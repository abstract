/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsIGenericFactory.h"
#include "nsIStringEnumerator.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaTestSqlDocument.h"
#include "aaTestSqlChannel.h"

#define AA_SQL_BASE_TEST_CID \
{0x256df941, 0x0922, 0x4f4b, {0x9b, 0xd6, 0x63, 0x07, 0x4a, 0x04, 0x5f, 0xe6}}
#define AA_SQL_BASE_TEST_CONTRACT "@aasii.org/sql/unit-base;1"

class aaModuleSqlBaseTest: public nsITest,
                           public nsIUTF8StringEnumerator
{
public:
  aaModuleSqlBaseTest() :mSubtest(0) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  virtual ~aaModuleSqlBaseTest() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaModuleSqlBaseTest,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaModuleSqlBaseTest::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
} 

/* nsIUTF8StringEnumerator */
static const char* sqlSubtests[] =
{
  "@aasii.org/sql/unit/document;1",
  "@aasii.org/sql/unit/sql-channel;1"
};
#define sqlSubtestCount (sizeof(sqlSubtests) / sizeof(char*))

NS_IMETHODIMP
aaModuleSqlBaseTest::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < sqlSubtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaModuleSqlBaseTest::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < sqlSubtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(sqlSubtests[mSubtest++]);
  return NS_OK;
}

NS_GENERIC_FACTORY_CONSTRUCTOR(aaModuleSqlBaseTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTestSqlDocument)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTestSqlChannel)

static const nsModuleComponentInfo components[] =
{
  { "Module for SQL Base Unit Test",
    AA_SQL_BASE_TEST_CID,
    AA_SQL_BASE_TEST_CONTRACT,
    aaModuleSqlBaseTestConstructor }
  ,{"Unit Test for SQL Document",
    AA_TEST_SQL_DOCUMENT_CID,
    AA_TEST_SQL_DOCUMENT_CONTRACT,
    aaTestSqlDocumentConstructor }
  ,{"Unit Test for SQL Channel",
    AA_TEST_SQL_CHANNEL_CID,
    AA_TEST_SQL_CHANNEL_CONTRACT,
    aaTestSqlChannelConstructor }
};

NS_IMPL_NSGETMODULE(aaModuleSqlBaseTest, components)
