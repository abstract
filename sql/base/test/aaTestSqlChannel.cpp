/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsNetCID.h"
#include "nsIAsyncInputStream.h"
#include "nsIAsyncOutputStream.h"
#include "nsIPipe.h"

/* Unfrozen API */
#include "nsThreadUtils.h"
#include "nsIRunnable.h"
#include "nsIThread.h"
#include "nsTestUtils.h"

#include "nsIBinaryInputStream.h"
#include "nsIBinaryOutputStream.h"

#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaTestSqlChannel.h"

class aaSqlChannelObserver : public nsISupports
{
public:
  aaSqlChannelObserver(nsITestRunner *testRunner, nsITest *parent);
  NS_DECL_ISUPPORTS

  nsresult Notify(nsIInputStream *stream, PRUint32 offset);
private:
  nsITestRunner *cxxUnitTestRunner;
  nsCOMPtr<nsITest> mParent;
  ~aaSqlChannelObserver() {}
  nsresult consume(nsIInputStream *stream, PRUint32 count);
};

aaSqlChannelObserver:: aaSqlChannelObserver(nsITestRunner *testRunner, nsITest
    *parent)
  : cxxUnitTestRunner(testRunner), mParent(parent) {}

NS_IMPL_THREADSAFE_ISUPPORTS0(aaSqlChannelObserver)

nsresult
aaSqlChannelObserver::Notify(nsIInputStream *stream, PRUint32 offset)
{
  return NS_OK;
}

nsresult
aaSqlChannelObserver::consume(nsIInputStream *stream, PRUint32 count)
{
  return NS_OK;
}

class aaSqlChannelEvent : public nsIRunnable
{
public:
  aaSqlChannelEvent(aaSqlChannelObserver *target, nsIInputStream *stream,
      PRUint32 offset);
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsRefPtr<aaSqlChannelObserver> mTarget;
  nsCOMPtr<nsIInputStream> mStream;
  PRUint32 mOffset;

  ~aaSqlChannelEvent() {}
};

aaSqlChannelEvent::aaSqlChannelEvent(aaSqlChannelObserver *target,
    nsIInputStream *stream, PRUint32 offset)
  : mTarget(target), mStream(stream), mOffset(offset) {}

NS_IMPL_THREADSAFE_ISUPPORTS1(aaSqlChannelEvent,
                              nsIRunnable)

NS_IMETHODIMP
aaSqlChannelEvent::Run()
{
  NS_ENSURE_STATE(mTarget);
  return mTarget->Notify(mStream, mOffset);
}

class aaSqlChannelRequest : public nsIRunnable
{
public:
  aaSqlChannelRequest();
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE

  nsresult AsyncOpen(aaSqlChannelObserver *observer);
private:
  nsCOMPtr<nsIThread> mTarget;
  nsRefPtr<aaSqlChannelObserver> mObserver;

  ~aaSqlChannelRequest() {}
  nsresult feed(PRUint32 offset, PRUint32 count);
};

aaSqlChannelRequest::aaSqlChannelRequest() {}

NS_IMPL_THREADSAFE_ISUPPORTS1(aaSqlChannelRequest,
                              nsIRunnable)

nsresult
aaSqlChannelRequest::AsyncOpen(aaSqlChannelObserver *observer)
{
  return NS_OK;
}

NS_IMETHODIMP
aaSqlChannelRequest::Run()
{
  return NS_OK;
}

nsresult
aaSqlChannelRequest::feed(PRUint32 offset, PRUint32 count)
{
  return NS_OK;
}


NS_IMPL_ISUPPORTS1(aaTestSqlChannel,
                   nsITest)

/* nsITest */
NS_IMETHODIMP
aaTestSqlChannel::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}
