/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATESTSQLDOCUMENT_H
#define AATESTSQLDOCUMENT_H 1

#define AA_TEST_SQL_DOCUMENT_CID \
{0x2426a017, 0xb232, 0x4e54, {0xad, 0x2a, 0xb3, 0x1e, 0xfe, 0x77, 0x82, 0xfa}}
#define AA_TEST_SQL_DOCUMENT_CONTRACT "@aasii.org/sql/unit/document;1"

#include "nsITest.h"

class aaTestSqlDocument: public nsITest
{
public:
  aaTestSqlDocument() {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaTestSqlDocument() {}
};

#endif /* AATESTSQLDOCUMENT_H */
