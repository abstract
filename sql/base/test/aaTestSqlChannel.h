/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATESTSQLCHANNEL_H
#define AATESTSQLCHANNEL_H 1

#define AA_TEST_SQL_CHANNEL_CID \
{0x4f2a1a3d, 0xdb22, 0x4caa, {0x80, 0x12, 0xc5, 0x9f, 0xac, 0xcc, 0x18, 0xfe}}
#define AA_TEST_SQL_CHANNEL_CONTRACT "@aasii.org/sql/unit/sql-channel;1"

#include "nsITest.h"

class aaTestSqlChannel: public nsITest
{
public:
  aaTestSqlChannel() {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaTestSqlChannel() {}
};

#endif /* AATESTSQLCHANNEL_H */
