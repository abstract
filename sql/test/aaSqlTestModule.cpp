/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaSqliteTest.h"
#include "aaEntitySqlTest.h"

#define AA_SQL_TEST_MODULE_CID \
{0xd1809dce, 0x036f, 0x44b8, {0xb7, 0x45, 0x1e, 0x63, 0x17, 0x9e, 0xa4, 0xf4}}
#define AA_SQL_TEST_MODULE_CONTRACT "@aasii.org/sql/unit;1"

class aaSqlTestModule: public nsITest,
                       public nsIUTF8StringEnumerator
{
public:
  aaSqlTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  ~aaSqlTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaSqlTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaSqlTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* sqlSubtests[] =
{
  "@aasii.org/sql/unit-base;1"
  ,"@aasii.org/sql/unit-sqlite;1"
};
#define sqlSubtestCount (sizeof(sqlSubtests) / sizeof(char*))

NS_IMETHODIMP
aaSqlTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < sqlSubtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaSqlTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < sqlSubtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(sqlSubtests[mSubtest++]);
  return NS_OK;
}

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqlTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqliteTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaEntitySqlTest)

static const nsModuleComponentInfo kComponents[] =
{
  { "Sql Module Test Container",
    AA_SQL_TEST_MODULE_CID,
    AA_SQL_TEST_MODULE_CONTRACT,
    aaSqlTestModuleConstructor}
  ,{"Sqlite Test Container",
    AA_SQLITE_TEST_CID,
    AA_SQLITE_TEST_CONTRACT,
    aaSqliteTestConstructor}
  ,{"Sql Test for Entity",
    AA_SQL_TEST_ENTITY_CID,
    AA_SQL_TEST_ENTITY_CONTRACT,
    aaEntitySqlTestConstructor}
};
NS_IMPL_NSGETMODULE(aaSqlTest, kComponents)
