/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsITestRunner.h"

/* Project includes */
#include "aaSqliteTest.h"

aaSqliteTest::aaSqliteTest()
  :mSubtest(0)
{
}

NS_IMPL_ISUPPORTS2(aaSqliteTest,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaSqliteTest::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* sqliteSubtests[] =
{
  "@aasii.org/storage/unit;1"
  ,"@aasii.org/transport/sqlite/test/load-entity;1"
  ,"@aasii.org/sql/unit/entity;1"
};
#define sqliteSubtestCount (sizeof(sqliteSubtests) / sizeof(char*))

NS_IMETHODIMP
aaSqliteTest::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < sqliteSubtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteTest::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < sqliteSubtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(sqliteSubtests[mSubtest++]);
  return NS_OK;
}
