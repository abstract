/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAENTITYSQLTEST_H
#define AAENTITYSQLTEST_H 1

#include "nsITest.h"

#define AA_SQL_TEST_ENTITY_CID \
{0x7e7a0f18, 0xc42a, 0x423e, {0xb0, 0xed, 0x9d, 0x26, 0xab, 0xa1, 0xd0, 0x2f}}
#define AA_SQL_TEST_ENTITY_CONTRACT "@aasii.org/sql/unit/entity;1"

class nsITestRunner;

class aaEntitySqlTest: public nsITest
{
public:
  aaEntitySqlTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaEntitySqlTest() {;}
};

#endif /* AAENTITYSQLTEST_H */
