/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLITETEST_H
#define AASQLITETEST_H

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"

class nsITestRunner;

#define AA_SQLITE_TEST_CID \
{0x7d8c6fae, 0x8ceb, 0x4c67, {0x8e, 0x14, 0xe2, 0xac, 0xc4, 0x48, 0x93, 0xdb}}
#define AA_SQLITE_TEST_CONTRACT "@aasii.org/sql/unit-sqlite;1"

class aaSqliteTest: public nsITest,
                    public nsIUTF8StringEnumerator
{
public:
  aaSqliteTest();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  ~aaSqliteTest() {;}
  PRUint32 mSubtest;
};

#endif /* AASQLITETEST_H */
