/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=idl: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include "nsISupports.idl"

interface nsIDOMWindow;
interface nsITest;
interface nsIChannel;
interface nsIURI;

/*
 * The nsITestRunner interface
 * @status UNSTABLE
 */

[scriptable, uuid(81db3a76-28c9-4b17-aee5-879e175adeb1)]
interface nsITestRunner : nsISupports
{
  void markTestStart();
  void addFailure(in string aFile, in unsigned long aLine, in string aText);
  void addJSFailure(in string aText);

  const unsigned long errorNoError = 0x0;
  const unsigned long errorLoad = 0x1;
  const unsigned long errorJS = 0x2;
  const unsigned long errorWindow = 0x3;
  const unsigned long errorTimeout = 0x4;
  void addError(in unsigned long aCode, in string aComment);

  void armTimer(in unsigned long aDelay);
  void markTestEnd(in nsITest aTest);

  readonly attribute nsIDOMWindow testWindow;
  readonly attribute boolean hasWindow;
  attribute nsIDOMWindow watchWindow;
  void doCommand(in DOMString aCommand);

  void CompareWithFile(in string aFile, in unsigned long aLine,
      in nsIChannel dataChannel, in nsIURI reference);
};

