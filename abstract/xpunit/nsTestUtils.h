/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef NSTESTUTILS_H
#define NSTESTUTILS_H

#define NS_TESTRUNNER_CONTRACT "@aasii.org/cxxunit/testrunner"

#define NS_TEST_BEGIN(testrunner) \
  nsITestRunner *cxxUnitTestRunner = testrunner; \
  cxxUnitTestRunner->MarkTestStart();

#define NS_TEST_ASSERT(condition) \
  if ( NS_UNLIKELY(!(condition)) ) { \
    cxxUnitTestRunner->AddFailure(__FILE__,__LINE__,0); \
  }

#define NS_TEST_ASSERT_OK(condition) \
  if ( NS_FAILED(condition) ) { \
    cxxUnitTestRunner->AddFailure(__FILE__,__LINE__,"xpcom error"); \
  }

#define NS_TEST_ASSERT_MSG(condition, message) \
  if ( NS_UNLIKELY(!(condition)) ) { \
    cxxUnitTestRunner->AddFailure(__FILE__,__LINE__,(message)); \
  }

#define NS_TEST_IGETTER_INIT(unit_class, unit_IID, unit_object) \
{ \
  nsresult rv; \
  unit_class *testee; \
  unit_IID *iObject; \
  unit_object *object;\
  nsAutoRefCnt refCnt; \
  testee = new unit_class(); \
  object = new unit_object(); \
  object->AddRef();

#define NS_TEST_IGETTER_ARG(unit_class, unit_getter) \
  rv = testee->unit_getter( nsnull ); \
  NS_TEST_ASSERT( rv == NS_ERROR_INVALID_POINTER );

#define NS_TEST_IGETTER_NOVALUE(unit_class, unit_getter) \
  rv = testee->unit_getter( &iObject ); \
  NS_TEST_ASSERT( rv == NS_ERROR_NOT_INITIALIZED ); \
  NS_TEST_ASSERT( !iObject );

#define NS_TEST_IGETTER_RETURN(unit_class, unit_getter, \
    unit_member, unit_IID) \
  object->QueryInterface(NS_GET_IID(unit_IID), \
      getter_AddRefs(testee->unit_member)); \
  refCnt = object->mRefCnt; \
  rv = testee->unit_getter( &iObject ); \
  NS_TEST_ASSERT( rv == NS_OK ); \
  NS_TEST_ASSERT( iObject == object );

#define NS_TEST_IGETTER_REFCNT \
  NS_TEST_ASSERT( refCnt +1 == object->mRefCnt ); \
  delete testee; \
  delete object; \
}

#define NS_TEST_IGETTER(unit_class, \
    unit_getter, unit_member, unit_IID, unit_object) \
  NS_TEST_IGETTER_INIT(unit_class, unit_IID, unit_object) \
  NS_TEST_IGETTER_ARG(unit_class, unit_getter) \
  NS_TEST_IGETTER_NOVALUE(unit_class, unit_getter) \
  NS_TEST_IGETTER_RETURN(unit_class, unit_getter, \
      unit_member, unit_IID) \
  NS_TEST_IGETTER_REFCNT

#endif /* NSTESTUTILS_H */
