/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATESTCONST_H
#define AATESTCONST_H 1

#define AA_TXN_COUNT 6

#define AA_ENTITY_TAG_1 "abstract"
#define AA_ENTITY_TAG_2 "Sergey"
#define AA_ENTITY_TAG_3 "J. Dow"
#define AA_ENTITY_TAG_4 "SB RF Bank"

#define AA_MONEY_CODE_1 643
#define AA_RESOURCE_TAG_1 "RUB"
#define AA_RESOURCE_TAG_2 "AASI share"

#define AA_FLOW_TAG_1 "equity share 1"
#define AA_FLOW_TAG_3 "equity share 2"
#define AA_FLOW_TAG_5 "bank account 1"

#define AA_FLOW_SHARE_RATE 10000.0
#define AA_EVENT_AMOUNT_1 900000.0
#define AA_EVENT_AMOUNT_2 100000.0

#define AA_EVENT_AMOUNT_3 142000.0

#define AA_RESOURCE_TAG_3 "Sony VAIO"
#define AA_ENTITY_TAG_5 "Equipment supplier"
#define AA_FLOW_TAG_6 "purchase contract 1"
#define AA_EVENT_AMOUNT_4 70000.0
#define AA_EVENT_AMOUNT_5 1.0

#define AA_MONEY_CODE_2 978
#define AA_RESOURCE_TAG_4 "EUR"
#define AA_FLOW_TAG_7 "forex deal 1"
#define AA_EVENT_AMOUNT_6 1000.0
#define AA_EVENT_RATE_2 34.95

#define AA_FLOW_TAG_8 "bank account 2"

#define AA_FLOW_TAG_9 "forex deal 2"
#define AA_EVENT_RATE_3 35.0

#define AA_FLOW_TAG_10 "forex deal 3"
#define AA_EVENT_AMOUNT_7 5000.0
#define AA_EVENT_RATE_4 34.2

#define AA_FLOW_TAG_11 "rented office 1"
#define AA_ENTITY_TAG_6 "Audit service"
#define AA_RESOURCE_TAG_5 "office space"
#define AA_EVENT_RATE_6 2000.0

#define AA_FLOW_TAG_12 "forex deal 4"
#define AA_EVENT_AMOUNT_8 3000.0
#define AA_EVENT_RATE_5 34.95
#define AA_EVENT_AMOUNT_9 2000.0
#define AA_EVENT_AMOUNT_10 2500.0
#define AA_EVENT_AMOUNT_11 600.0
#define AA_EVENT_AMOUNT_12 100.0

#define AA_EVENT_AMOUNT_13 50.0
#define AA_EVENT_AMOUNT_14 50.0

#define AA_EVENT_AMOUNT_15 400.0

#endif /* AATESTCONST_H */
