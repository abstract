/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEKEYS_H
#define AASAVEKEYS_H 1

#define AA_INSERT_ENTITY_CONTRACT \
  "@aasii.org/storage/entity-insert;1"
#define AA_UPDATE_ENTITY_CONTRACT \
  "@aasii.org/storage/entity-update;1"
#define AA_SAVEENTITY_CONTRACT "@aasii.org/storage/save-entity;1"

#define AA_INSERT_RESOURCE_CONTRACT \
  "@aasii.org/storage/resource-insert;1"

#define AA_INSERT_ASSET_CONTRACT \
  "@aasii.org/storage/asset-insert;1"
#define AA_UPDATE_ASSET_CONTRACT \
  "@aasii.org/storage/asset-update;1"
#define AA_SAVEASSET_CONTRACT "@aasii.org/storage/save-asset;1"

#define AA_INSERT_MONEY_CONTRACT \
  "@aasii.org/storage/money-insert;1"
#define AA_SAVEMONEY_CONTRACT "@aasii.org/storage/save-money;1"

#define AA_INSERT_FLOW_CONTRACT \
  "@aasii.org/storage/flow-insert;1"
#define AA_INSERT_TERM_CONTRACT \
  "@aasii.org/storage/term-insert;1"
#define AA_SAVEFLOW_CONTRACT "@aasii.org/storage/save-flow;1"

#define AA_CLAUSE_INSERT_CONTRACT \
  "@aasii.org/storage/clause-insert;1"
#define AA_NORM_INSERT_CONTRACT \
  "@aasii.org/storage/norm-insert;1"
#define AA_SAVENORM_CONTRACT "@aasii.org/storage/save-norm;1"

#define AA_STATE_INSERT_CONTRACT \
  "@aasii.org/storage/state-insert;1"
#define AA_STATE_UPDATE_CONTRACT \
  "@aasii.org/storage/state-update;1"
#define AA_STATE_CLOSE_CONTRACT \
  "@aasii.org/storage/state-close;1"
#define AA_STATE_DELETE_CONTRACT \
  "@aasii.org/storage/state-delete;1"
#define AA_SAVESTATE_CONTRACT "@aasii.org/storage/save-state;1"

#define AA_EVENT_INSERT_CONTRACT \
  "@aasii.org/storage/event-insert;1"
#define AA_EVENT_DELETE_CONTRACT \
  "@aasii.org/storage/event-delete;1"
#define AA_TRANSFER_INSERT_CONTRACT \
  "@aasii.org/storage/transfer-insert;1"
#define AA_TRANSFER_DELETE_CONTRACT \
  "@aasii.org/storage/transfer-delete;1"
#define AA_FACT_SIDE_INSERT_CONTRACT \
  "@aasii.org/storage/fact-side-insert;1"
#define AA_FACT_SIDE_DELETE_CONTRACT \
  "@aasii.org/storage/fact-side-delete;1"
#define AA_SAVEEVENT_CONTRACT "@aasii.org/storage/save-event;1"

#define AA_INSERT_QUOTE_CONTRACT \
  "@aasii.org/storage/quote-insert;1"
#define AA_SAVEQUOTE_CONTRACT "@aasii.org/storage/save-quote;1"

#define AA_TRANSACTION_INSERT_CONTRACT \
  "@aasii.org/storage/transaction-insert;1"
#define AA_SAVETRANSACTION_CONTRACT "@aasii.org/storage/save-transaction;1"

#define AA_INSERT_CHART_CONTRACT \
  "@aasii.org/storage/chart-insert;1"
#define AA_SAVECHART_CONTRACT "@aasii.org/storage/save-chart;1"

#define AA_BALANCE_INSERT_CONTRACT \
  "@aasii.org/storage/balance-insert;1"
#define AA_BALANCE_UPDATE_CONTRACT \
  "@aasii.org/storage/balance-update;1"
#define AA_BALANCE_CLOSE_CONTRACT \
  "@aasii.org/storage/balance-close;1"
#define AA_BALANCE_DELETE_CONTRACT \
  "@aasii.org/storage/balance-delete;1"
#define AA_SAVEBALANCE_CONTRACT "@aasii.org/storage/save-balance;1"

#define AA_INCOME_INSERT_CONTRACT \
  "@aasii.org/storage/income-insert;1"
#define AA_INCOME_UPDATE_CONTRACT \
  "@aasii.org/storage/income-update;1"
#define AA_INCOME_CLOSE_CONTRACT \
  "@aasii.org/storage/income-close;1"
#define AA_INCOME_DELETE_CONTRACT \
  "@aasii.org/storage/income-delete;1"
#define AA_SAVEINCOME_CONTRACT "@aasii.org/storage/save-income;1"

#endif /* AASAVEKEYS_H */
