/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABASELOADERS_H
#define AABASELOADERS_H 1

#define AA_SQLSIMPLELOADER_CONTRACT "@aasii.org/sql/loader;1"
#define AA_SQLSIMPLELISTENER_CONTRACT "@aasii.org/sql/listener;1"
#define AA_SQLFILTER_CONTRACT "@aasii.org/sql/filter;1"

#define AA_SQLIDLISTENER_CONTRACT "@aasii.org/sql/id-listener;1"
#define AA_LOADLASTID_CONTRACT "@aasii.org/storage/load-last-id;1"

#define AA_LOADENTITY_CONTRACT "@aasii.org/storage/load-entity;1"

#define AA_LOADRESOURCE_CONTRACT "@aasii.org/storage/load-resource;1"

#define AA_LOADFLOW_CONTRACT "@aasii.org/storage/load-flow;1"

#define AA_LOADFLOWSTATES_CONTRACT "@aasii.org/storage/load-flow-states;1"

#define AA_RULE_GET_CONTRACT "@aasii.org/storage/load-get;1"
#define AA_RULE_LISTENER_CONTRACT "@aasii.org/storage/load-listener;1"
#define AA_LOADRULE_CONTRACT "@aasii.org/storage/load-rule;1"

#endif /* AABASELOADERS_H */
