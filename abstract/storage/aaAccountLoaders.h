/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAACCOUNTLOADERS_H
#define AAACCOUNTLOADERS_H 1

#define AA_LOADBALANCE_CONTRACT "@aasii.org/storage/load-balance;1"
#define AA_LOADPENDINGFACTS_CONTRACT \
  "@aasii.org/storage/load-pending-facts;1"
#define AA_CALCDIFF_CONTRACT "@aasii.org/storage/calc-quote-diff;1"
#define AA_LOADQUOTE_CONTRACT "@aasii.org/storage/load-quote;1"

#define AA_LOADCHART_CONTRACT "@aasii.org/storage/load-chart;1"

#define AA_LOADFACTLIST_CONTRACT \
  "@aasii.org/storage/load-fact-list;1"

#define AA_LOADINCOME_CONTRACT "@aasii.org/storage/load-income;1"

#endif /* AAACCOUNTLOADERS_H */

