/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_NOTIFY_H
#define AA_NOTIFY_H

#include "nsIObserverService.h"
#include "nsServiceManagerUtils.h"

#define AA_NOTIFY_SERVICE_CONTRACT "@mozilla.org/observer-service;1"

#define AA_NOTIFY_OBSERVE_TYPE_INFORMATION "ABSTRACT_INFO"
#define AA_NOTIFY_OBSERVE_TYPE_WARNING "ABSTRACT_WARNING"
#define AA_NOTIFY_OBSERVE_TYPE_ERROR "ABSTRACT_CRITICAL"
#define AA_NOTIFY_OBSERVE_TYPE_BLOCK "ABSTRACT_BLOCK"

/*
 * Notify base macros contains 3 params
 *  @notifier - pointer to nsISupports of object that notify observers
 *  @observeType - const char* - data contains type of observers
 *  @msgID - const PRUnichar* - data contains message ID
 */
#define AA_NOTIFY_USER_BASE(notifier, observeType, msgID)                                     \
  PR_BEGIN_MACRO                                                                              \
    nsCOMPtr<nsIObserverService> _notify_serv = do_GetService(AA_NOTIFY_SERVICE_CONTRACT);    \
    if (_notify_serv) {                                                                       \
      _notify_serv->NotifyObservers(notifier, observeType, msgID);                            \
    }                                                                                         \
  PR_END_MACRO

/*
 * All notify_user_* macroses have 2 params
 *  @notifier - pointer to nsISupports of object that notify observers
 *  @msgID - const PRUnichar* - data contains message ID
 */
#define AA_NOTIFY_USER_INFORMATION(notifier, msgID)                                           \
  AA_NOTIFY_USER_BASE(notifier, AA_NOTIFY_OBSERVE_TYPE_INFORMATION, msgID)

#define AA_NOTIFY_USER_WARNING(notifier, msgID)                                               \
  AA_NOTIFY_USER_BASE(notifier, AA_NOTIFY_OBSERVE_TYPE_WARNING, msgID)

#define AA_NOTIFY_USER_ERROR(notifier, msgID)                                                 \
  AA_NOTIFY_USER_BASE(notifier, AA_NOTIFY_OBSERVE_TYPE_ERROR, msgID)

#define AA_NOTIFY_USER_BLOCK(notifier, msgID)                                                 \
  AA_NOTIFY_USER_BASE(notifier, AA_NOTIFY_OBSERVE_TYPE_BLOCK, msgID)

/*
 * All notify_user_this_* macroses have 1 param
 *  @msgID - const PRUnichar* - data contains message ID
 *  notifier will be current class that wil be converted to nsISupports
 */
#define AA_NOTIFY_USER_THIS_INFORMATION(msgID)                                                          \
  AA_NOTIFY_USER_BASE(reinterpret_cast<nsISupports*>(this), AA_NOTIFY_OBSERVE_TYPE_INFORMATION, msgID)

#define AA_NOTIFY_USER_THIS_WARNING(msgID)                                                              \
  AA_NOTIFY_USER_BASE(reinterpret_cast<nsISupports*>(this), AA_NOTIFY_OBSERVE_TYPE_WARNING, msgID)

#define AA_NOTIFY_USER_THIS_ERROR(msgID)                                                                \
  AA_NOTIFY_USER_BASE(reinterpret_cast<nsISupports*>(this), AA_NOTIFY_OBSERVE_TYPE_ERROR, msgID)

#define AA_NOTIFY_USER_THIS_BLOCK(msgID)                                                                \
  AA_NOTIFY_USER_BASE(reinterpret_cast<nsISupports*>(this), AA_NOTIFY_OBSERVE_TYPE_BLOCK, msgID)

#endif //AA_NOTIFY_H
