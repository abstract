/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABASEKEYS_H
#define AABASEKEYS_H 1

#define AA_TIMEFRAME_CONTRACT "@aasii.org/base/timeframe;1"
#define AA_ENTITY_CONTRACT "@aasii.org/base/entity;1"
#define AA_MONEY_CONTRACT "@aasii.org/base/money;1"
#define AA_ASSET_CONTRACT "@aasii.org/base/asset;1"
#define AA_FLOW_CONTRACT "@aasii.org/base/flow;1"
#define AA_STATE_CONTRACT "@aasii.org/base/state;1"
#define AA_FACT_CONTRACT "@aasii.org/base/fact;1"
#define AA_EVENT_CONTRACT "@aasii.org/base/event;1"
#define AA_RULE_CONTRACT "@aasii.org/base/rule;1"

#define AA_INCOMEFLOW_CONTRACT "@aasii.org/base/income-flow;1"
#define AA_QUOTE_CONTRACT "@aasii.org/base/quote;1"
#define AA_TRANSACTION_CONTRACT "@aasii.org/base/transaction;1"
#define AA_INCOME_CONTRACT "@aasii.org/base/income;1"

#endif /* AABASEKEYS_H */
