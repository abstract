/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsXPCOM.h"
#include "nsMemory.h"
#include "nsIGenericFactory.h"
#include "nsIServiceManager.h"
#include "nsICategoryManager.h"
#include "nsComponentManagerUtils.h"
#include "nsIWebProgressListener.h"
#include "nsWeakReference.h"

/* Unfrozen interfaces */
#include "nsIConsoleListener.h"
#include "nsITimer.h"
#include "nsICommandLineHandler.h"
#include "nsDeque.h"

/* Project includes */
#include "nsITestRunner.h"
#include "nsITestResult.h"
#include "nsITest.h"
#include "nsTestUtils.h"
#include "nsTestRunner.h"
#include "nsTestResult.h"

#define NS_TESTRUNNER_CLH "m-aatest"

NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsTestRunner,
    nsTestRunner::GetTestRunner)
NS_GENERIC_FACTORY_CONSTRUCTOR(nsTestResult)

static NS_METHOD ModuleRegistration(nsIComponentManager *aCompMgr,
    nsIFile *aPath, const char *registryLocation, const char *componentType,
    const nsModuleComponentInfo *info)
{
  nsresult rv;

  nsCOMPtr<nsIServiceManager> servman;
  servman = do_QueryInterface((nsISupports*)aCompMgr, &rv);
  NS_ENSURE_SUCCESS(rv,rv);

  nsCOMPtr<nsICategoryManager> catman;
  rv = servman->GetServiceByContractID(NS_CATEGORYMANAGER_CONTRACTID,
      NS_GET_IID(nsICategoryManager), getter_AddRefs(catman));
  NS_ENSURE_SUCCESS(rv,rv);

  char* previous = nsnull;
  rv = catman->AddCategoryEntry("command-line-handler",
      NS_TESTRUNNER_CLH, NS_TESTRUNNER_CONTRACT, PR_TRUE, PR_TRUE,
      &previous);
  if (previous)
    nsMemory::Free(previous);
  return rv;
}

static NS_METHOD ModuleUnregistration(nsIComponentManager *aCompMgr,
    nsIFile *aPath, const char *registryLocation,
    const nsModuleComponentInfo *info)
{
  nsresult rv;

  nsCOMPtr<nsIServiceManager> servman;
  servman = do_QueryInterface((nsISupports*)aCompMgr, &rv);
  NS_ENSURE_SUCCESS(rv,rv);

  nsCOMPtr<nsICategoryManager> catman;
  rv = servman->GetServiceByContractID(NS_CATEGORYMANAGER_CONTRACTID,
      NS_GET_IID(nsICategoryManager), getter_AddRefs(catman));
  NS_ENSURE_SUCCESS(rv,rv);

  rv = catman->DeleteCategoryEntry("command-line-handler",
      NS_TESTRUNNER_CLH, PR_TRUE);
  return rv;
}

static const nsModuleComponentInfo components[] =
{
  { "XPUnit Test Runner",
    NS_TESTRUNNER_CID,
    NS_TESTRUNNER_CONTRACT,
    nsTestRunnerConstructor,
    ModuleRegistration,
    ModuleUnregistration },
  { "XPUnit Test Result",
    NS_TESTRESULT_CID,
    NS_TESTRESULT_CONTRACT,
    nsTestResultConstructor }
};
NS_IMPL_NSGETMODULE(nsTest, components)

