/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// addMethod - By John Resig (MIT Licensed)
function addMethod(object, name, fn){
  var old = object[ name ];
  if ( old )
    object[ name ] = function(){
      if ( fn.length == arguments.length )
        return fn.apply(this, arguments);
      else if ( typeof old == 'function' )
        return old.apply(this, arguments);
    };
  else
    object[ name ] = fn;
}

/* Base object for javascript tests  */
function JSTest()
{
}
JSTest.prototype = {
    get name() { return this._name; },
    set name() { throw "readonly property"; },
    _name : null,

    get CID() { return this._CID; },
    set CID() { throw "readonly property"; },
    _CID : null,

    get contractID() { return this._contractID; },
    set contractID() { throw "readonly property"; },
    _contractID : null,
    _test : null,
    _check : null,
    
    addJSFailure: function(runner, msg) {
        runner.addJSFailure("Test cid: " + this.CID + "; Message: " + msg);
    },
    
    /* nsISupports */
    QueryInterface : function (iid)
    {
        if (iid.equals(nsCI.nsITest) ||
            iid.equals(nsCI.nsITimerCallback) ||
            iid.equals(nsCI.nsIWebProgressListener) ||
            iid.equals(nsCI.nsIFactory) ||
            iid.equals(nsCI.nsIModule) ||
            iid.equals(nsCI.nsISupports))
            return this;

        throw Components.results.NS_ERROR_NO_INTERFACE;
    },
    /* nsITest */
    test : function (runner)
    {
        runner.markTestStart();
        this._test(runner);
    },
    set async() { throw "readonly property"; },
    get async() { return false; },
    /* nsITimerCallback */
    notify : function (timer)
    {
        var runner = Components.classes["@aasii.org/cxxunit/testrunner"].getService(nsCI.nsITestRunner);
        try {
            this._check(runner);
        } finally {
            runner.markTestEnd(this);
        }
    },
    /* nsIWebProgressListener */
    onStateChange : function( aWebProgress, aRequest, aStateFlags, aStatus)
    {
        var timer = Components.classes["@mozilla.org/timer;1"].getService(nsCI.nsITimer);
        timer.initWithCallback(this, 0, nsCI.nsITimer.TYPE_ONE_SHOT);
    },
    /* nsIFactory */
    createInstance : function (outer, iid)
    {
        if (outer != null)
            throw Components.results.NS_ERROR_NO_AGGREGATION;
        return this.QueryInterface(iid);
    },
    lockFactory : function (lock)
    {
        /* no-op */
    },
    /* nsIModule */
    getClassObject : function mod_gch(compMgr, cid, iid)
    {
        if (cid.equals(this.CID)){
            return this.QueryInterface(iid);
        }
        throw Components.results.NS_ERROR_NOT_REGISTERED;
    },
    registerSelf : function mod_regself(compMgr, fileSpec, location, type)
    {
        compMgr.QueryInterface(nsCI.nsIComponentRegistrar);
        compMgr.registerFactoryLocation(this._CID, this._name, this._contractID, fileSpec, location, type);
    },
    unregisterSelf : function mod_unreg(compMgr, location, type)
    {
        compMgr.QueryInterface(nsCI.nsIComponentRegistrar);
        compMgr.unregisterFactoryLocation(this.CID, location);
    },
    canUnload : function(compMgr)
    {
        return true;
    }
};


/* XPCOM module */
function JSTestModule()
{
    var me = new JSTest();
    //get old functions
    me.base_unregisterSelf = me.unregisterSelf;
    me.base_QueryInterface = me.QueryInterface;
    me.base_getClassObject = me.getClassObject;
    me.base_registerSelf = me.registerSelf;
    //variables
    me._tests = [];
    me._map = {};
    me._child = 0;
    me._parse_counter = 0;
    //functions
    me._test = function(runner) {
        /* no - op */
    }
    me._check = function(runner) {
        /* no - op */
    }
    //you must call _add methods with appropriate arguments count
    addMethod(me, '_add', function(_jsTest){
        var i = this._tests.length;
        this._tests[i] = _jsTest;
        this._map[_jsTest.CID] = i;
    });
    addMethod(me, '_add', function(cid, contractID, name, testF, checkF){
        var test = new JSTest();
        test._CID = cid;
        test._name = name;
        test._contractID = contractID;
        test._test = testF;
        test._check = checkF;
        this._add(test);
    });
    /*me._add = function(cid, contractID, name, testF, checkF){
        var test = new JSTest();
        test._CID = cid;
        test._name = name;
        test._contractID = contractID;
        test._test = testF;
        test._check = checkF;
        var i = this._tests.length;
        this._tests[i] = test;
        this._map[test.CID] = i;
    }*/
    me._repeat = function(idx) {
        var i = this._tests.length;
        this._tests[i] = this._tests[idx];
    }
    //test flow
    me.parseAction = null;
    me.createTestsFlow = function(actions) {
        for (this._parse_counter = 0; this._parse_counter < actions.length; this._parse_counter++) {
            var action = actions[this._parse_counter];
            if (!this.parseAction) {
                //runner.addJSFailure("JSTestModule - cann't parse action, parseAction function is not defined");
            } else {
                if (!this.parseAction(action)) {
                    throw "cann't parse action";
                }
            }
        }
    }
    //end test flow
    /* nsISupports */
    me.QueryInterface = function (iid)
    {
        if (iid.equals(nsCI.nsIUTF8StringEnumerator)) return this;
        return this.base_QueryInterface(iid);
    }
    /* nsIStringEnumerator */
    me.hasMore = function ()
    {
        if (this._child < this._tests.length)
            return true;
        else
            return false;
    }
    me.getNext = function ()
    {
        if (this._child >= this._tests.length)
            throw Components.results.NS_ERROR_NOT_AVAILABLE;
        return this._tests[this._child++].contractID;
    }
    /* nsIModule */
    me.getClassObject = function mod_gch(compMgr, cid, iid)
    {
        if (!cid.equals(this.CID)) {
            if (cid in this._map) {
                return this._tests[this._map[cid]].QueryInterface(iid);
            } else {
                var i, count = this._tests.length;
                var obj = null;
                for (i = 0; i < count ; i++) {
                    var o = this._tests[i];
                    try {
                        obj = o.getClassObject(compMgr, cid, iid);
                        if (null != obj) {
                            return obj;
                        }
                    } catch (e) {
                        obj = null;
                    }
                }
                if (null == obj) throw Components.results.NS_ERROR_NOT_REGISTERED;
                return obj;
            }
        }
        return this.base_getClassObject(compMgr, cid, iid);
    }
    me.registerSelf = function mod_regself(compMgr, fileSpec, location, type)
    {
        this.base_registerSelf(compMgr, fileSpec, location, type);
        var i, count = this._tests.length;
        for (i = 0; i < count ; i++) {
            var o = this._tests[i];
            try {
                o.registerSelf(compMgr, fileSpec, location, type);
            } catch (e) {
            }
        }
    }
    me.unregisterSelf = function mod_unreg(compMgr, location, type)
    {
        this.base_unregisterSelf(compMgr, location, type);
        var i, count = this._tests.length;
        for (i = 0; i < count ; i++) {
            var o = this._tests[i];
            try {
                o.unregisterSelf(compMgr, location, type);
            } catch (e) {
            }
        }
    }
    return me;
}

