/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NSTESTRUNNER_H
#define NSTESTRUNNER_H 1

#include "nsStringAPI.h"

#define NS_TESTRUNNER_CID \
{0xda0c516d, 0x0531, 0x4429, {0xb8, 0x28, 0xa7, 0xaf, 0xbf, 0xfc, 0x6e, 0x35}}

#include "nsIConsoleListener.h"
#include "nsIWebProgressListener.h"
#include "nsWeakReference.h"
#include "nsITimer.h"
#include "nsICommandLineHandler.h"
#include "nsDeque.h"
#include "nsITestRunner.h"
#include "nsICompareListener.h"

class nsITestResult;
class nsIDOMWindow;
class nsITimer;
class nsIRDFDataSource;
class nsIThreadJSContextStack;

class nsTestRunner: public nsITestRunner,
                    public nsIConsoleListener,
                    public nsIWebProgressListener,
                    public nsSupportsWeakReference,
                    public nsITimerCallback,
                    public nsICommandLineHandler,
                    public nsICompareListener
{
public:
  virtual ~nsTestRunner();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITESTRUNNER
  NS_DECL_NSICONSOLELISTENER
  NS_DECL_NSIWEBPROGRESSLISTENER
  NS_DECL_NSITIMERCALLBACK
  NS_DECL_NSICOMMANDLINEHANDLER
  NS_DECL_NSICOMPARELISTENER
  NS_DECLARE_STATIC_IID_ACCESSOR(NS_TESTRUNNER_CID)

  operator nsISupports * () { return (nsITestRunner*) this; }
  static nsTestRunner* GetTestRunner();
  nsresult resolve(nsACString &aURL, const PRUnichar* *result);
private:
  nsTestRunner();
  static nsTestRunner* gTestRunner;
  friend class nsSelfTest;

  nsresult openMainWindow();
  void run();
  nsresult init();
  nsresult pushTest(const char *aContractID);
  nsresult getNextTest(nsACString &aContractID);
  void traverse();
  nsresult closeMainWindow();
  void done();
  nsresult registerXmlTests();

  nsCOMPtr<nsITestResult> mTestResult;
  nsCAutoString mTestID;
  nsDeque mTestTree;
  nsCOMPtr<nsIDOMWindow> mTestWindow;
  nsCOMPtr<nsIDOMWindow> mWatchWindow;
  nsCOMPtr<nsITimer> mTimer;
  nsCOMPtr<nsIRDFDataSource> mFileMap;
  nsCOMPtr<nsIThreadJSContextStack> mJSStack;
  nsCOMPtr<nsIURI> mCompareURI;
  const char* mCompareFile;
  PRUint32 mCompareLine;
  PRPackedBool mArmed;
  PRPackedBool mHasWindow;
}; 

NS_DEFINE_STATIC_IID_ACCESSOR(nsTestRunner, NS_TESTRUNNER_CID)
#endif /* ! NSTESTRUNNER_H */
