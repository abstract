/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NS_TEST_XML_FACTORY_H
#define NS_TEST_XML_FACTORY_H

#include "nsIFactory.h"
#include "nsAutoPtr.h"

class nsTestXmlParser;
class nsIURI;

#define NS_TEST_XML_FACTORY_CID                         \
  { 0x7a37534d, 0x3f4, 0x4922,                          \
    { 0xa5, 0x36, 0xed, 0x8b, 0xb6, 0x66, 0x96, 0x6f }}

#define NS_TEST_XML_FACTORY_CONTRACT \
  "@aasii.org/xpunit/xml/factory;1"


class nsTestXmlFactory : public nsIFactory {
public:
  NS_DECLARE_STATIC_IID_ACCESSOR(NS_TEST_XML_FACTORY_CID)

  nsTestXmlFactory();
  NS_DECL_ISUPPORTS
  NS_DECL_NSIFACTORY
public:
  NS_IMETHOD RegisterSelf(nsIURI *aURI, nsIComponentRegistrar *aRegistrar);
private:
  ~nsTestXmlFactory();

  nsRefPtr<nsTestXmlParser> mTestParser;
};

NS_DEFINE_STATIC_IID_ACCESSOR(nsTestXmlFactory, NS_TEST_XML_FACTORY_CID)

#endif //NS_TEST_XML_FACTORY_H

