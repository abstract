/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NS_TEST_XML_PARSER_H
#define NS_TEST_XML_PARSER_H

#include "nsIContentSink.h"
#include "nsIExpatSink.h"
#include "nsTArray.h"
#include "nsCOMPtr.h"

class nsTestXmlQuery
{
public:
  nsTestXmlQuery()
    : mReferenceLine(-1) {}
  void Clear();
public:
  nsCAutoString mUrl;
  nsCAutoString mReference;
  PRInt32 mReferenceLine;
};

class nsTestXmlParser
  : public nsIContentSink
  , public nsIExpatSink
{
public:
  nsTestXmlParser()
    : mTestElementStarted(PR_FALSE)
    , mNodeType(XML_NODE_UNKNOWN)
    , mIsChildContract(PR_FALSE)
    , mIsQueryUrl(PR_FALSE)
    , mIsQueryReference(PR_FALSE) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIEXPATSINK

  nsresult Init(nsIURI* aFileUri);
  nsresult GetFileURI(nsIURI** aRetFileURI);

  const nsACString& getContract() const
  {
    return mContract;
  } 
  const nsACString& getName() const
  {
    return mName;
  } 
  const nsID& getCID() const
  { 
    return mCID;
  }
  const nsTArray<nsCAutoString>& getChildrens() const
  {
    return mChildrens;
  }
  const nsTArray<nsTestXmlQuery>& getQueries() const
  {
    return mQueries;
  }
  //nsIContentSink
  NS_IMETHOD WillParse(void)
  {
    return NS_OK;
  }
  NS_IMETHOD WillBuildModel(nsDTDMode aDTDMode) {
    return NS_OK;
  }
  NS_IMETHOD DidBuildModel(PRBool aTerminated) {
    return NS_OK;
  }
  NS_IMETHOD WillInterrupt(void)
  {
    return NS_OK;
  }
  NS_IMETHOD WillResume(void)
  {
    return NS_OK;
  }
  NS_IMETHOD SetParser(nsIParser* aParser)
  {
    return NS_OK;
  }
  virtual void FlushPendingNotifications(mozFlushType aType)
  {
  }
  NS_IMETHOD SetDocumentCharset(nsACString& aCharset)
  {
    return NS_OK;
  }
  virtual nsISupports *GetTarget()
  {
    return nsnull;
  }
private:
  ~nsTestXmlParser() {;}

  nsCOMPtr<nsIURI> mFileUri;

  nsCAutoString mContract;
  nsCAutoString mName;
  nsID mCID;

  PRBool mTestElementStarted;
  enum XmlNode
  {
    XML_NODE_CID = 0x00,
    XML_NODE_CONTRACT,
    XML_NODE_NAME,
    XML_NODE_CHILDREN,
    XML_NODE_QUERY,

    XML_NODE_UNKNOWN = 0xFF
  };
  XmlNode mNodeType;

  PRBool mIsChildContract;
  nsTArray<nsCAutoString> mChildrens;

  PRBool mIsQueryUrl;
  PRBool mIsQueryReference;
  nsTestXmlQuery mCurrentQuery;
  nsTArray<nsTestXmlQuery> mQueries;
};

#endif //NS_TEST_XML_PARSER_H

