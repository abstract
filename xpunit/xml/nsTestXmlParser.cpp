/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "nsTestXmlParser.h"
#include "nsNetUtil.h"
#include "nsCOMPtr.h"
#include "nsParserCIID.h"

#include "nsIParser.h"

void
nsTestXmlQuery::Clear()
{
  mUrl.Truncate();
  mReference.Truncate();
  mReferenceLine = -1;
}

static NS_DEFINE_CID(kParserCID, NS_PARSER_CID);

static NS_METHOD
TestInputStream(nsIInputStream *inStr, void *closure, const char *buffer,
    PRUint32 offset, PRUint32 count, PRUint32 *countWritten)
{
  PRBool *result = static_cast<PRBool *>(closure);
  *result = PR_TRUE;
  return NS_ERROR_ABORT;  // don't call me anymore
}

PRBool
NS_InputStreamIsBuffered(nsIInputStream *stream)
{
  PRBool result = PR_FALSE;
  PRUint32 n;
  nsresult rv = stream->ReadSegments(TestInputStream,
      &result, 1, &n);
  return result || NS_SUCCEEDED(rv);
}

NS_IMPL_ISUPPORTS2(nsTestXmlParser,
    nsIContentSink,
    nsIExpatSink)

nsresult
nsTestXmlParser::Init(nsIURI* aFileUri)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFileUri);
  NS_ENSURE_STATE(!mFileUri);

  mFileUri = aFileUri;

  nsCOMPtr<nsIChannel> parserChannel;
  rv = NS_NewChannel(getter_AddRefs(parserChannel), mFileUri);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = parserChannel->SetContentType(nsDependentCString("text/xml"));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIParser> parser = do_CreateInstance(kParserCID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  parser->SetContentSink(this);

  rv = parser->Parse(mFileUri, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIInputStream> stream;
  rv = parserChannel->Open(getter_AddRefs(stream));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIInputStream> bufferedStream;
  if (!NS_InputStreamIsBuffered(stream)) {
    rv = NS_NewBufferedInputStream(getter_AddRefs(bufferedStream),
        stream, 4096);
    NS_ENSURE_SUCCESS(rv, rv);
    stream = bufferedStream;
  }

  nsCOMPtr<nsIStreamListener> listener = do_QueryInterface(parser, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = listener->OnStartRequest(parserChannel, nsnull);
  if (NS_FAILED(rv))
    parserChannel->Cancel(rv);

  nsresult status;
  parserChannel->GetStatus(&status);

  PRUint32 offset = 0;
  while (NS_SUCCEEDED(rv) && NS_SUCCEEDED(status)) {
    PRUint32 available;
    rv = stream->Available(&available);
    if (rv == NS_BASE_STREAM_CLOSED)
    {
      rv = NS_OK;
      available = 0;
    }
    if (NS_FAILED(rv))
    {
      parserChannel->Cancel(rv);
      break;
    }
    if (!available)
      break;
    rv = listener->OnDataAvailable(parserChannel, nsnull, stream,
          offset, available);
    if (NS_SUCCEEDED(rv))
      offset += available;
    else {
      parserChannel->Cancel(rv);
    }
    parserChannel->GetStatus(&status);
  }
  rv = listener->OnStopRequest(parserChannel, nsnull, status);

  return NS_OK;
}

nsresult
nsTestXmlParser::GetFileURI(nsIURI** aRetFileURI)
{
  NS_ENSURE_ARG_POINTER(aRetFileURI);
  NS_IF_ADDREF(*aRetFileURI = mFileUri);
  return NS_OK;
}

//nsIExpatSink
NS_IMETHODIMP
nsTestXmlParser::HandleStartElement(const PRUnichar *aName, const
    PRUnichar **aAtts, PRUint32 aAttsCount, PRInt32 aIndex, PRUint32
    aLineNumber)
{
  nsAutoString name(aName);
  if (!mTestElementStarted)
  {
    NS_ENSURE_TRUE(name.EqualsLiteral("test"), NS_ERROR_INVALID_ARG);
    mTestElementStarted = PR_TRUE;
  }
  else if (XML_NODE_CHILDREN == mNodeType)
  {
    NS_ENSURE_STATE(!mIsChildContract);
    if (name.EqualsLiteral("contract"))
    {
      mIsChildContract = PR_TRUE;
    }
  }
  else if (XML_NODE_QUERY == mNodeType)
  {
    NS_ENSURE_STATE(!mIsQueryUrl && !mIsQueryReference);
    if (name.EqualsLiteral("url"))
    {
      mIsQueryUrl = PR_TRUE;
    }
    else if (name.EqualsLiteral("reference"))
    {
      mIsQueryReference = PR_TRUE;
      mCurrentQuery.mReferenceLine = (PRInt32)aLineNumber;
    }
  }
  else
  {
    NS_ENSURE_STATE(XML_NODE_UNKNOWN == mNodeType);
    if (name.EqualsLiteral("cid"))
    {
      mNodeType = XML_NODE_CID;
    }
    else if (name.EqualsLiteral("contract"))
    {
      mNodeType = XML_NODE_CONTRACT;
    }
    else if (name.EqualsLiteral("name"))
    {
      mNodeType = XML_NODE_NAME;
    }
    else if (name.EqualsLiteral("children"))
    {
      mNodeType = XML_NODE_CHILDREN;
    }
    else if (name.EqualsLiteral("query"))
    {
      mNodeType = XML_NODE_QUERY;
      mCurrentQuery.Clear();
    }
  }
  return NS_OK;
}

NS_IMETHODIMP
nsTestXmlParser::HandleEndElement(const PRUnichar *aName)
{
  NS_ENSURE_STATE(mTestElementStarted);
  if (XML_NODE_CHILDREN == mNodeType)
  {
    nsAutoString name(aName);
    if (name.EqualsLiteral("children"))
    {
      mNodeType = XML_NODE_UNKNOWN;
    }
    else
    {
      NS_ENSURE_STATE(mIsChildContract);
      mIsChildContract = PR_FALSE;
    }
  }
  else if (XML_NODE_QUERY == mNodeType)
  {
    nsAutoString name(aName);
    if (name.EqualsLiteral("query"))
    {
      NS_ENSURE_STATE(!mIsQueryUrl && !mIsQueryReference);
      mQueries.AppendElement(mCurrentQuery);
      mCurrentQuery.Clear();
      mNodeType = XML_NODE_UNKNOWN;
    }
    else if (name.EqualsLiteral("url"))
    {
      NS_ENSURE_STATE(mIsQueryUrl && !mIsQueryReference);
      mIsQueryUrl = PR_FALSE;
    }
    else
    {
      NS_ENSURE_STATE(mIsQueryReference && !mIsQueryUrl);
      mIsQueryReference = PR_FALSE;
    }
  }
  else
  {
    mNodeType = XML_NODE_UNKNOWN;
  }
  return NS_OK;
}

NS_IMETHODIMP
nsTestXmlParser::HandleComment(const PRUnichar *aCommentText)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
nsTestXmlParser::HandleCDataSection(const PRUnichar *aData, PRUint32
    aLength)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
nsTestXmlParser::HandleDoctypeDecl(const nsAString & aSubset, const
    nsAString & aName, const nsAString & aSystemId, const nsAString &
    aPublicId, nsISupports *aCatalogData)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
nsTestXmlParser::HandleCharacterData(const PRUnichar *aData, PRUint32
    aLength)
{
  if (XML_NODE_UNKNOWN != mNodeType)
  {
    nsCAutoString value = NS_ConvertUTF16toUTF8(aData, aLength);
    value.Trim("\n\r\t ");
    if (value.IsEmpty()) return NS_OK;
    switch (mNodeType)
    {
      case XML_NODE_CID:
        {
          mCID.Parse(value.get());
        }
        break;
      case XML_NODE_CONTRACT:
        mContract = value;
        break;
      case XML_NODE_NAME:
        mName = value;
        break;
      case XML_NODE_CHILDREN:
        {
          NS_ENSURE_STATE(mIsChildContract);
          mChildrens.AppendElement(value);
        }
        break;
      case XML_NODE_QUERY:
        {
          NS_ENSURE_STATE(mIsQueryUrl || mIsQueryReference);
          if (mIsQueryUrl)
          {
            mCurrentQuery.mUrl.Append(value);
          }
          else if (mIsQueryReference)
          {
            mCurrentQuery.mReference = value;
          }
        }
        break;
      default:
        NS_ENSURE_TRUE(PR_FALSE, NS_ERROR_INVALID_ARG);
        break;
    }
  }
  return NS_OK;
}

NS_IMETHODIMP
nsTestXmlParser::HandleProcessingInstruction(const PRUnichar
    *aTarget, const PRUnichar *aData)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
nsTestXmlParser::HandleXMLDeclaration(const PRUnichar *aVersion,
    const PRUnichar *aEncoding, PRInt32 aStandalone)
{
  return NS_OK;
}

NS_IMETHODIMP
nsTestXmlParser::ReportError(const PRUnichar *aErrorText, const
    PRUnichar *aSourceText, nsIScriptError *aError, PRBool *_retval
    NS_OUTPARAM)
{
  nsAutoString error(aErrorText);
  printf_stderr("\nReportError: %s\n", NS_ConvertUTF16toUTF8(error).get());
  return NS_OK;
}

