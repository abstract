/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "nsTestXml.h"
#include "nsTestXmlParser.h"
#include "nsTestUtils.h"
#include "nsITestRunner.h"

#include "nsNetUtil.h"

NS_IMPL_ISUPPORTS2(nsTestXml,
                  nsITest,
                  nsIUTF8StringEnumerator)

NS_IMETHODIMP
nsTestXml::Test(nsITestRunner *aRunner)
{
  nsresult rv = NS_OK;
  NS_ENSURE_STATE(mParser);
  NS_TEST_BEGIN(aRunner);

  PRUint32 length = mParser->getQueries().Length();
  for (PRUint32 i = 0; i < length; ++ i)
  {
    const nsTestXmlQuery& query = mParser->getQueries()[i];

    nsCOMPtr<nsIURI> ref;
    rv = NS_NewURI(getter_AddRefs(ref), query.mReference.get());
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIURI> data;
    rv = NS_NewURI(getter_AddRefs(data), query.mUrl.get());
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIChannel> channel;
    rv = NS_NewChannel(getter_AddRefs(channel), data);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIURI> url;
    rv = mParser->GetFileURI(getter_AddRefs(url));
    NS_ENSURE_SUCCESS(rv, rv);

    nsCAutoString spec;
    rv = url->GetSpec(spec);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = aRunner->CompareWithFile(spec.get(), query.mReferenceLine,
        channel, ref);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
nsTestXml::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_STATE(mParser);
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = PR_FALSE;
  ++ mCurrent;
  if (mCurrent < (PRInt32)mParser->getChildrens().Length())
  {
    *aHasMore = PR_TRUE;
  }
  return NS_OK;
}

NS_IMETHODIMP
nsTestXml::GetNext(nsACString& aNext)
{
  NS_ENSURE_STATE(mParser);
  aNext.Truncate();
  aNext.Assign(mParser->getChildrens()[mCurrent]);
  return NS_OK;
}

