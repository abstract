/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "nsTestXmlFactory.h"
#include "nsCOMPtr.h"
#include "nsNetUtil.h"
#include "nsStringAPI.h"
#include "nsTestXml.h"
#include "nsTestXmlParser.h"

#include "nsIComponentRegistrar.h"

nsTestXmlFactory::nsTestXmlFactory() {
}

nsTestXmlFactory::~nsTestXmlFactory() {
}

NS_IMPL_ISUPPORTS2(nsTestXmlFactory, nsIFactory, nsTestXmlFactory)

NS_IMETHODIMP
nsTestXmlFactory::RegisterSelf(nsIURI *aURI, nsIComponentRegistrar *aRegistrar)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aRegistrar);
  NS_ENSURE_ARG_POINTER(aURI);
  NS_ENSURE_STATE(!mTestParser);

  nsCOMPtr<nsIURI> url(aURI);

  mTestParser = new nsTestXmlParser();
  NS_ENSURE_TRUE(mTestParser, NS_ERROR_OUT_OF_MEMORY);

  rv = mTestParser->Init(url);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIComponentRegistrar> registrar(aRegistrar);
  rv = registrar->RegisterFactory(
    mTestParser->getCID(),
    mTestParser->getName().BeginReading(),
    mTestParser->getContract().BeginReading(), this);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
nsTestXmlFactory::CreateInstance(nsISupports *aOuter, const nsIID & iid,
		  void * *result NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(result);
  if (iid.Equals(nsITest::GetIID()))
  {
    NS_ENSURE_STATE(mTestParser);
    nsTestXml* pTest = new nsTestXml(mTestParser);
    NS_ENSURE_TRUE(pTest, NS_ERROR_OUT_OF_MEMORY);
    NS_ADDREF(pTest);

    *result = (void*)pTest;
    return NS_OK;
  }
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void lockFactory (in PRBool lock); */
NS_IMETHODIMP
nsTestXmlFactory::LockFactory(PRBool lock)
{
  return NS_OK;
}

