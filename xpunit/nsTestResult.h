/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NSTESTRESULT_H
#define NSTESTRESULT_H 1

#define NS_TESTRESULT_CID \
{0x51961034, 0x7b41, 0x4e4d, {0x94, 0x9f, 0x3f, 0xa6, 0xc3, 0xe5, 0xe6, 0x2f}}
#define NS_TESTRESULT_CONTRACT "@aasii.org/cxxunit/testresult"

#include "nsITestResult.h"
#ifdef DEBUG
#define AA_HAS_DEBUG
#undef DEBUG
#endif
#include "mozilla/Mutex.h"
#ifdef AA_HAS_DEBUG
#undef AA_HAS_DEBUG
#define DEBUG
#endif

class nsTestResult: public nsITestResult
{ 
public:
  nsTestResult();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITESTRESULT

private:
  ~nsTestResult();
  friend class nsSelfTest;

  void printTime();
  void printFailures();
  void printHeadline();

  PRUint32 mCount;
  PRUint32 mFailureCount;
  PRUint32 mErrorCount;
  PRIntervalTime mStartTime;
  nsDeque mFailureList;
  mozilla::Mutex mLock;
  PRBool mSilent;
};

#endif /* ! NSTESTRESULT_H */
