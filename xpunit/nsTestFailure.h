/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NSTESTFAILURE_H
#define NSTESTFAILURE_H

#include "nsStringAPI.h"

/******************** nsTestFailure ***********************************/
class nsTestFailure
{
public:
  nsTestFailure(const char *aFile, PRUint32 aLine, const char *aText,
      PRBool aError)
    : mFile(aFile), mLine(aLine), mText(aText), mError(aError)  {;}
  ~nsTestFailure() {;}
  const char* getFile() const {return mFile.get();}
  PRUint32 getLine() const {return mLine;}
  const char* getText() const {return mText.get();}
  PRBool isError() const {return mError;}
protected:
  nsTestFailure() {;}
  nsCAutoString mFile;
  PRUint32 mLine;
  nsCAutoString mText;
  PRBool mError;
};

#endif /* NSTESTFAILURE_H */
