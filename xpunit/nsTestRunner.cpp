/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsXPCOM.h"
#include "nsStringAPI.h"
#include "nsNetUtil.h"
#include "nsIComponentManager.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsIPrefBranch.h"
#include "nsIWindowWatcher.h"
#include "nsIDOMWindow.h"
#include "nsIDOMWindowCollection.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsIWebProgress.h"
#include "nsIDOMDocument.h"
#include "nsIDOMDocumentView.h"
#include "nsIDOMElement.h"
#include "nsIDOMEventTarget.h"
#include "nsIDOMDocumentEvent.h"
#include "nsIDOMEvent.h"
#include "nsIDOMAbstractView.h"
#include "nsIThreadManager.h"
#include "nsIThread.h"
#include "nsILineInputStream.h"
#include "nsIComponentRegistrar.h"
#include "nsDirectoryServiceDefs.h"

/* Unfrozen interfaces */
#include "nsAutoPtr.h"
#include "nsICommandLine.h"
#include "nsIWebNavigation.h"
#include "nsIAppStartup.h"
#include "nsIConsoleService.h"
#include "nsIConsoleMessage.h"
#include "nsIScriptError.h"
#include "jspubtd.h"
#include "nsIJSContextStack.h"
#include "nsIRDFService.h"
#include "nsIRDFDataSource.h"
#include "nsIRDFResource.h"
#include "nsIDOMXULDocument.h"
#include "nsIDOMXULCommandDispatcher.h"
#include "nsIDOMXULCommandEvent.h"
#include "nsIStringEnumerator.h"
#include "nsIURI.h"

/* Project includes */
#include "nsITestRunner.h"
#include "nsITestResult.h"
#include "nsITest.h"
#include "nstestjs.h"
#include "nsTestRunner.h"
#include "nsTestFailure.h"
#include "nsOneLineDiff.h"
#include "nsTestXmlFactory.h"

#include "nsTestUtils.h"

class nsFormatJSMessage: public nsTestFailure
{
public:
  nsFormatJSMessage(nsIConsoleMessage *aMessage);
  nsFormatJSMessage(const char *aFile);
  ~nsFormatJSMessage() {;}
private:
  nsresult parseScriptError(nsIConsoleMessage *aMessage);
  nsresult formatErrorMessage(nsIScriptError *error);
  PRBool parseException(const nsACString &aMessage);
  nsresult getSourceFile();
  nsresult parseLocalFile();
  nsresult parseChrome();

  nsRefPtr<nsTestRunner> mResolver;
};


/******************** nsTestRunner ***********************************/
nsTestRunner::nsTestRunner()
:mTestTree(nsnull), mArmed(PR_FALSE), mHasWindow(PR_FALSE)
{ 
}

nsTestRunner::~nsTestRunner()
{
}

nsTestRunner* nsTestRunner::gTestRunner = nsnull;

NS_IMPL_THREADSAFE_ISUPPORTS8(nsTestRunner,
                              nsITestRunner,
                              nsIConsoleListener,
                              nsIWebProgressListener,
                              nsISupportsWeakReference,
                              nsITimerCallback,
                              nsICommandLineHandler,
                              nsICompareListener,
                              nsTestRunner)

NS_IMETHODIMP
nsTestRunner::MarkTestStart()
{
  return mTestResult->MarkTestStart();
}

NS_IMETHODIMP
nsTestRunner::AddFailure(const char *aFile, PRUint32 aLine, const char *aText)
{
  return mTestResult->AddFailure(aFile, aLine, aText, PR_FALSE);
}

NS_IMETHODIMP
nsTestRunner::AddJSFailure(const char *aText)
{
  nsresult rv;
  JSContext *cx = nsnull;
  nsCAutoString file;

  if ( ! mJSStack ) {
    mJSStack = do_GetService("@mozilla.org/js/xpc/ContextStack;1");
    NS_ENSURE_TRUE(mJSStack, NS_ERROR_FAILURE);
  }
  rv = mJSStack->Peek( &cx );
  NS_ENSURE_TRUE(cx, NS_ERROR_FAILURE);
  
  nsFormatJSMessage m(ns_test_js_caller_filename(cx));

  return mTestResult->AddFailure(m.getFile(),
      ns_test_js_caller_lineno(cx), aText, PR_FALSE);
}

NS_IMETHODIMP
nsTestRunner::AddError(PRUint32 aCode, const char *aComment)
{
  return mTestResult->AddFailure(nsnull, aCode, aComment, PR_TRUE);
}

NS_IMETHODIMP
nsTestRunner::MarkTestEnd(nsITest *aTest)
{
  if (aTest != (nsITest *) mTestTree.Peek())
    return NS_ERROR_ILLEGAL_VALUE;
  if (mArmed) {
    mArmed = PR_FALSE;
    mTimer->Cancel();
    if(NS_FAILED(mTimer->InitWithCallback(this, 0, nsITimer::TYPE_ONE_SHOT)))
      traverse();
  }
  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::GetHasWindow(PRBool *aHasWindow)
{
  NS_ENSURE_ARG_POINTER(aHasWindow);
  *aHasWindow = mHasWindow;
  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::GetTestWindow(nsIDOMWindow* *aTestWindow)
{
  NS_ENSURE_ARG_POINTER( aTestWindow );
  if ( !mTestWindow ) {
    *aTestWindow = nsnull;
    return NS_ERROR_NOT_INITIALIZED;
  }
  *aTestWindow = mTestWindow;
  NS_ADDREF( *aTestWindow );

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::GetWatchWindow(nsIDOMWindow* *aWatchWindow)
{
  NS_ENSURE_ARG_POINTER( aWatchWindow );
  if ( !mWatchWindow ) {
    *aWatchWindow = nsnull;
    return NS_ERROR_NOT_INITIALIZED;
  }
  *aWatchWindow = mWatchWindow;
  NS_ADDREF( *aWatchWindow );

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::SetWatchWindow(nsIDOMWindow *aWatchWindow)
{
  nsresult rv;

  if (mWatchWindow == aWatchWindow)
  {
    if (mWatchWindow && !mHasWindow)
    {
      rv = ArmTimer(1000);
      NS_ENSURE_SUCCESS(rv, rv);
    }
    return NS_OK;
  }

  if (mWatchWindow) {
    nsCOMPtr<nsIWebNavigation> web(do_GetInterface(mWatchWindow, &rv ));
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIWebProgress> webProgress(do_QueryInterface(web, &rv));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = webProgress->RemoveProgressListener(this);
    NS_ENSURE_SUCCESS(rv, rv);

    mWatchWindow = nsnull;
  }

  if (aWatchWindow) {
    nsCOMPtr<nsIWebNavigation> web(do_GetInterface(aWatchWindow, &rv ));
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIWebProgress> webProgress(do_QueryInterface(web, &rv));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = webProgress->AddProgressListener(this,
        nsIWebProgress::NOTIFY_STATE_WINDOW);
    NS_ENSURE_SUCCESS(rv, rv);

    if (mHasWindow) {
      rv = ArmTimer(1000);
      NS_ENSURE_SUCCESS(rv, rv);
    }
  }
  mWatchWindow = aWatchWindow;

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::ArmTimer(PRUint32 aDelay)
{
  NS_ENSURE_TRUE(!mArmed, NS_ERROR_ALREADY_INITIALIZED);

  nsresult rv;
  rv = mTimer->InitWithCallback(this, aDelay << 1, nsITimer::TYPE_ONE_SHOT);
  NS_ENSURE_SUCCESS(rv, rv);

  mArmed = PR_TRUE;
  return NS_OK;
}

nsresult
findElement(const nsAString& aCommand, nsIDOMWindow *wnd,
    nsIDOMElement **element)
{
  nsresult rv;
  PRUint32 count, i;
  nsCOMPtr<nsIDOMWindowCollection> list;
  nsCOMPtr<nsIDOMWindow> next;
  nsCOMPtr<nsIDOMDocument> doc;

  while (wnd) {
    rv = wnd->GetDocument(getter_AddRefs(doc));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = doc->GetElementById(aCommand, element);
    NS_ENSURE_SUCCESS(rv, rv);
    if (*element)
      return NS_OK;

    rv = wnd->GetFrames(getter_AddRefs(list));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = list->GetLength(&count);
    NS_ENSURE_SUCCESS(rv, rv);

    for (i = 0; i < count; i++) {
      rv = list->Item(i, getter_AddRefs(next));
      NS_ENSURE_SUCCESS(rv, rv); NS_ENSURE_STATE(next);
      
      rv = findElement(aCommand, next, element);
      NS_ENSURE_SUCCESS(rv, rv);
      if (*element)
        return NS_OK;
    }
  }
  return NS_ERROR_ILLEGAL_VALUE;
}


NS_IMETHODIMP
nsTestRunner::DoCommand(const nsAString& aCommand)
{
  nsresult rv;
  nsCOMPtr<nsIDOMElement> element;

  rv = findElement(aCommand, mTestWindow, getter_AddRefs(element));
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool isDisabled;
  rv = element->HasAttribute(NS_LITERAL_STRING("disabled"), &isDisabled);
  NS_ENSURE_SUCCESS(rv, rv);
  if ( isDisabled )
    return NS_ERROR_NOT_AVAILABLE;

  nsCOMPtr<nsIDOMEventTarget> evtTgt(do_QueryInterface(element, &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIDOMDocument> doc;
  rv = element->GetOwnerDocument(getter_AddRefs(doc));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIDOMDocumentEvent> docEvent(do_QueryInterface(doc, &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIDOMEvent> evt;
  rv = docEvent->CreateEvent(NS_LITERAL_STRING("XULCommandEvent"),
      getter_AddRefs(evt));
  NS_ENSURE_SUCCESS(rv, rv);
  
  nsCOMPtr<nsIDOMXULCommandEvent> cmdEvt(do_QueryInterface(evt, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  
  nsCOMPtr<nsIDOMDocumentView> docView(do_QueryInterface(doc, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsIDOMAbstractView> view;
  rv = docView->GetDefaultView(getter_AddRefs(view));
  NS_ENSURE_SUCCESS(rv, rv);
  
  rv = cmdEvt->InitCommandEvent(NS_LITERAL_STRING("command"), PR_TRUE, PR_TRUE,
      view, 0, 0, 0, 0, 0, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);
  
  PRBool isDefault;
  rv = evtTgt->DispatchEvent( evt, &isDefault );
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::CompareWithFile(const char* aFile, PRUint32 aLine,
    nsIChannel *dataChannel, nsIURI *reference)
{
  nsresult rv = NS_OK;
  NS_TEST_BEGIN(this);

  nsCOMPtr<nsOneLineDiff> differ = new nsOneLineDiff();
  NS_ENSURE_TRUE(differ, NS_ERROR_OUT_OF_MEMORY);

  nsCOMPtr<nsIChannel> refChannel;
  rv = NS_NewChannel(getter_AddRefs(refChannel), reference);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIThreadManager> threadMngr =
    do_GetService("@mozilla.org/thread-manager;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIThread> thread;
  rv = threadMngr->GetCurrentThread(getter_AddRefs(thread));
  NS_ENSURE_SUCCESS(rv, rv);

  mCompareURI = reference;
  mCompareFile = aFile;
  mCompareLine = aLine;
  rv = differ->Compare(refChannel, dataChannel, this);
  NS_ENSURE_SUCCESS(rv, rv);

  while (mCompareURI)
  {
    PRBool isProcessed = PR_FALSE;
    rv = thread->ProcessNextEvent(PR_TRUE, &isProcessed);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::Observe(nsIConsoleMessage *aMessage)
{
  NS_ENSURE_TRUE(mTestResult, NS_ERROR_UNEXPECTED);

  nsFormatJSMessage m(aMessage);
  mTestResult->AddFailure(m.getFile(), m.getLine(), m.getText(), PR_TRUE);
  return NS_OK;
}

nsresult
nsTestRunner::openMainWindow()
{
  nsresult rv;
  nsCOMPtr<nsIConsoleService> jsConsole;
  nsCOMPtr<nsIPrefBranch> pref;
  nsCAutoString chromeURI;
  nsCOMPtr<nsIWindowWatcher> ww;
  nsCOMPtr<nsIDOMWindow> wnd;

  pref = do_GetService("@mozilla.org/preferences-service;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = pref->GetCharPref("toolkit.defaultChromeURI",getter_Copies(chromeURI));
  NS_ENSURE_SUCCESS(rv, rv);
  ww = do_GetService("@mozilla.org/embedcomp/window-watcher;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = ww->OpenWindow(0, chromeURI.get(), "_blank", "chrome,dialog=no,all",
      0, getter_AddRefs(wnd) );
  NS_ENSURE_SUCCESS(rv, rv);

  rv = SetWatchWindow(wnd);
  NS_ENSURE_SUCCESS(rv, rv);

  mTestWindow = wnd;
  jsConsole = do_GetService("@mozilla.org/consoleservice;1", &rv);
  NS_ENSURE_TRUE(jsConsole, NS_OK);
  jsConsole->RegisterListener( this );
  
  return NS_OK;
}

nsresult
nsTestRunner::init()
{ 
  nsresult rv;
  nsCOMPtr<nsIPrefBranch> pref;

  pref = do_GetService("@mozilla.org/preferences-service;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  (void) pref->GetCharPref("xpunit.firstTest",getter_Copies(mTestID));
  if (!mTestID.get() || !mTestID.get()[0]) {
    printf_stderr("Please provide first test contractID in any pref file:\n");
    printf_stderr("pref(\"xpunit.firstTest\",\"@test.org/contract/id\");\n");
    return NS_ERROR_NOT_AVAILABLE;
  }

  mTimer = do_CreateInstance("@mozilla.org/timer;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  mTestResult = do_CreateInstance("@aasii.org/cxxunit/testresult", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = openMainWindow();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = registerXmlTests();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = pushTest(mTestID.get());
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
nsTestRunner::pushTest(const char * aContractID)
{
  nsresult rv;
  nsCOMPtr<nsITest> test = do_CreateInstance(aContractID, &rv);

  if (NS_FAILED(rv)) {
    AddError(nsITestRunner::errorLoad, aContractID);
    return rv;
  }

  mTestTree.Push( (void *) test.get() );

  rv = test->Test(this);
  if (NS_FAILED(rv)) return rv;

  test.get()->AddRef();
  return NS_OK;
}

nsresult
nsTestRunner::getNextTest(nsACString &aContractID)
{
  nsresult rv;
  PRBool hasMore;

  nsITest *test = (nsITest *)  mTestTree.Peek();
  NS_ENSURE_TRUE(test, NS_ERROR_UNEXPECTED);

  nsCOMPtr<nsIUTF8StringEnumerator> testNode(do_QueryInterface(test, &rv));
  if (rv == NS_NOINTERFACE)
    return NS_ERROR_NOT_AVAILABLE;
  NS_ENSURE_SUCCESS(rv, rv);

  rv = testNode->HasMore( &hasMore );
  NS_ENSURE_SUCCESS(rv, rv);
  if (! hasMore)
    return NS_ERROR_NOT_AVAILABLE;

  rv = testNode->GetNext( aContractID );
  NS_ENSURE_SUCCESS(rv, rv);
  
  return NS_OK;
}

void
nsTestRunner::traverse()
{
  nsresult rv;
  nsITest *test;

  while (mTestTree.GetSize()) {

    rv = getNextTest( mTestID );

    if (NS_FAILED(rv)) {
      test = (nsITest *) mTestTree.Pop();
      NS_RELEASE(test);
    } else {
      rv = pushTest(mTestID.get());
      if (NS_FAILED(rv))
        mTestTree.Empty();
      if (mArmed)
        return;
    }
  }

  if (mTestWindow) {
    mTestWindow = nsnull;
    rv = mTimer->InitWithCallback(this, 0, nsITimer::TYPE_ONE_SHOT);
    if (NS_SUCCEEDED(rv))
      return;
  }

  done();
}

nsresult
nsTestRunner::closeMainWindow()
{
  nsresult rv;

  nsCOMPtr<nsIAppStartup> app;
  app = do_GetService("@mozilla.org/toolkit/app-startup;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = app->Quit(app->eForceQuit);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

void
nsTestRunner::done()
{ 
  nsCOMPtr<nsIConsoleService> jsConsole;

  if (!mHasWindow)
  {
    mTestWindow = nsnull;
    return;
  }
  jsConsole = do_GetService("@mozilla.org/consoleservice;1");
  if (jsConsole )
    (void) jsConsole->UnregisterListener(this);
  
  (void) closeMainWindow();
  (void) mTestResult->Done();
  mTestResult = nsnull;
  mTimer = nsnull;
}

NS_IMETHODIMP
nsTestRunner::Handle(nsICommandLine *aCommandLine)
{
  nsresult rv;
  PRBool isTest;

  rv = aCommandLine->HandleFlag(NS_LITERAL_STRING("test"), PR_TRUE, &isTest);
  NS_ENSURE_SUCCESS(rv,rv);
  if (!isTest)
    return NS_OK;

  aCommandLine->SetPreventDefault(PR_TRUE);

  rv = init();
  if (NS_FAILED(rv)) {
    printf_stderr("Failed to initialize test framework\n");
    return NS_ERROR_ABORT;
  }

  traverse();
  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::GetHelpInfo(nsACString & aHelpInfo)
{
  aHelpInfo.Assign("  --test               Run unit test collection\n");
  return NS_OK;
}

nsresult
nsTestRunner::resolve(nsACString &aURL, const PRUnichar* *result)
{
  nsresult rv;
  nsCOMPtr<nsIRDFService> rdfSrv;
  nsCOMPtr<nsIRDFResource> src, prop;
  nsCOMPtr<nsIRDFNode> target;
  nsCOMPtr<nsIRDFLiteral> targetLiteral;

  NS_ENSURE_ARG_POINTER(result);
  *result = nsnull;
  rdfSrv = do_GetService("@mozilla.org/rdf/rdf-service;1", &rv);
  NS_ENSURE_SUCCESS( rv, rv );

  if ( ! mFileMap ) {
    nsCOMPtr<nsIPrefBranch> pref;
    nsCAutoString url; 

    pref = do_GetService("@mozilla.org/preferences-service;1", &rv);
    NS_ENSURE_SUCCESS(rv, rv );
    rv = pref->GetCharPref("xpunit.fileMap",getter_Copies(url));
    NS_ENSURE_SUCCESS(rv, rv );

    rv = rdfSrv->GetDataSourceBlocking(url.get(),
        getter_AddRefs( mFileMap ));
    NS_ENSURE_TRUE( mFileMap, rv );
  }

  rv = rdfSrv->GetResource( aURL, getter_AddRefs( src ));
  NS_ENSURE_SUCCESS(rv, rv );
  rv = rdfSrv->GetResource(
      NS_LITERAL_CSTRING("http://www.aasii.org/AA-rdf#File"),
      getter_AddRefs( prop ));
  NS_ENSURE_SUCCESS(rv, rv );

  rv = mFileMap->GetTarget(src, prop, PR_TRUE, getter_AddRefs( target ));
  if (! target)
    return rv;
  targetLiteral = do_QueryInterface( target, &rv );
  NS_ENSURE_TRUE( target, rv );
  targetLiteral->GetValueConst( result );
  NS_ENSURE_TRUE( target, rv );

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::Notify(nsITimer *timer)
{
  if (mArmed) {
    mArmed = PR_FALSE;
    AddError(nsITestRunner::errorTimeout, mTestID.get());
  }
  traverse();
  return NS_OK;
}

NS_IMETHODIMP 
nsTestRunner::OnStateChange(nsIWebProgress *aWebProgress,
    nsIRequest *aRequest, PRUint32 aStateFlags, nsresult aStatus)
{
  if (! (aStateFlags & nsIWebProgressListener::STATE_STOP) )
    return NS_OK;

  nsCOMPtr<nsIDOMWindow> wnd;
  aWebProgress->GetDOMWindow(getter_AddRefs( wnd ));
  if ( wnd != mWatchWindow  ) {
    return NS_OK;
  }

  mWatchWindow = nsnull;
  aWebProgress->RemoveProgressListener( this );

  if (!mHasWindow)
  {
    mHasWindow = PR_TRUE;
    if (!mTestWindow)
      done();

    return NS_OK;
  }
  NS_ENSURE_TRUE(mArmed, NS_ERROR_UNEXPECTED);

  nsITest *test = (nsITest *)  mTestTree.Peek();
  NS_ENSURE_TRUE(test, NS_ERROR_UNEXPECTED);

  nsCOMPtr<nsIWebProgressListener> listener(do_QueryInterface(test));
  NS_ENSURE_TRUE(listener, NS_ERROR_UNEXPECTED);

  listener->OnStateChange(aWebProgress, aRequest, aStateFlags, aStatus);

  return NS_OK;
}

NS_IMETHODIMP 
nsTestRunner::OnProgressChange(nsIWebProgress *aWebProgress,
    nsIRequest *aRequest, PRInt32 aCurSelfProgress, PRInt32 aMaxSelfProgress,
    PRInt32 aCurTotalProgress, PRInt32 aMaxTotalProgress)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP 
nsTestRunner::OnLocationChange(nsIWebProgress *aWebProgress,
    nsIRequest *aRequest, nsIURI *aLocation)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP 
nsTestRunner::OnStatusChange(nsIWebProgress *aWebProgress,
    nsIRequest *aRequest, nsresult aStatus, const PRUnichar *aMessage)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP 
nsTestRunner::OnSecurityChange(nsIWebProgress *aWebProgress,
    nsIRequest *aRequest, PRUint32 aState)
{
      return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
nsTestRunner::OnMismatch(const nsACString& aText, PRUint32 aLine)
{
  nsresult rv = NS_OK;
  nsCOMPtr<nsIURI> reference = mCompareURI;

  mCompareURI = nsnull;

  nsCAutoString spec;
  rv = reference->GetSpec(spec);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString message;
  message.AssignLiteral("\"");
  message.Append(aText);
  message.AppendLiteral("\"");
  rv = AddFailure(spec.get(), aLine, message.get());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = AddFailure(mCompareFile, mCompareLine, "included from here");
  NS_ENSURE_SUCCESS(rv, rv);
  mCompareFile = nsnull;
  mCompareLine = 0;

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::OnError(const nsACString& aText, PRUint32 aLine)
{
  nsresult rv = NS_OK;
  nsCOMPtr<nsIURI> reference = mCompareURI;

  mCompareURI = nsnull;

  nsCAutoString spec;
  rv = reference->GetSpec(spec);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString message;
  message.Append(aText);
  rv = AddFailure(spec.get(), aLine, message.get());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = AddFailure(mCompareFile, mCompareLine, "included from here");
  NS_ENSURE_SUCCESS(rv, rv);
  mCompareFile = nsnull;
  mCompareLine = 0;

  return NS_OK;
}

NS_IMETHODIMP
nsTestRunner::OnSuccess()
{
  mCompareURI = nsnull;
  mCompareFile = nsnull;
  mCompareLine = 0;
  return NS_OK;
}

nsresult
nsTestRunner::registerXmlTests()
{
  nsresult rv = NS_OK;
  
  nsCOMPtr<nsIComponentRegistrar> compReg;
  rv = NS_GetComponentRegistrar(getter_AddRefs(compReg));
  NS_ENSURE_SUCCESS(rv, rv);
  
  nsCOMPtr<nsIProperties> procDirProp(
      do_CreateInstance("@mozilla.org/file/directory_service;1", &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> testDir;
  rv = procDirProp->Get(NS_OS_CURRENT_PROCESS_DIR, NS_GET_IID(nsIFile),
      getter_AddRefs(testDir));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = testDir->Append(NS_LITERAL_STRING("tests"));
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool dirExist = PR_FALSE;
  rv = testDir->Exists(&dirExist);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool testIsDir = PR_FALSE;
  rv = testDir->IsDirectory(&testIsDir);
  NS_ENSURE_SUCCESS(rv, rv);

  if (dirExist && testIsDir)
  {
    rv = testDir->Append(NS_LITERAL_STRING("xml"));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = testDir->Exists(&dirExist);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = testDir->IsDirectory(&testIsDir);
    NS_ENSURE_SUCCESS(rv, rv);

    if (dirExist && testIsDir)
    {
      nsCOMPtr<nsISimpleEnumerator> entries;
      rv = testDir->GetDirectoryEntries(getter_AddRefs(entries));
      NS_ENSURE_SUCCESS(rv, rv);

      PRBool hasMore = PR_FALSE;
      while (NS_SUCCEEDED(rv = entries->HasMoreElements(&hasMore)) && hasMore)
      {
        nsCOMPtr<nsISupports> item;
        rv = entries->GetNext(getter_AddRefs(item));
        NS_ENSURE_SUCCESS(rv, rv);

        nsCOMPtr<nsILocalFile> file(do_QueryInterface(item, &rv));
        NS_ENSURE_SUCCESS(rv, rv);

        PRBool fileIsFile = PR_FALSE;
        rv = file->IsFile(&fileIsFile);
        NS_ENSURE_SUCCESS(rv, rv);

        if (fileIsFile)
        {
          nsCAutoString leafName;
          rv = file->GetNativeLeafName(leafName);
          NS_ENSURE_SUCCESS(rv, rv);

          nsCAutoString baseUri;
          baseUri.AssignLiteral("resource:///tests/xml/");
          baseUri.Append(leafName);

          nsCOMPtr<nsIURI> uri;
          rv = NS_NewURI(getter_AddRefs(uri), baseUri.get());
          NS_ENSURE_SUCCESS(rv, rv);

          nsCOMPtr<nsTestXmlFactory> xmlFactory =
            do_CreateInstance(NS_TEST_XML_FACTORY_CONTRACT, &rv);
          NS_ENSURE_SUCCESS(rv, rv);
          NS_ENSURE_TRUE(!!(xmlFactory), NS_ERROR_OUT_OF_MEMORY);

          rv = xmlFactory->RegisterSelf(uri, compReg);
          NS_ENSURE_SUCCESS(rv, rv);
        }
      }
    }
  }

  return NS_OK;
}

nsTestRunner*
nsTestRunner::GetTestRunner()
{
  if ( nsTestRunner::gTestRunner == nsnull ) {
    nsTestRunner::gTestRunner = new nsTestRunner;
  }

  if ( nsTestRunner::gTestRunner != nsnull ) {
    NS_ADDREF( nsTestRunner::gTestRunner );
  }

  return nsTestRunner::gTestRunner;
}

/******************** nsFormatJSMessage ***********************************/
nsFormatJSMessage::nsFormatJSMessage(nsIConsoleMessage *aMessage)
{
  if (NS_SUCCEEDED( parseScriptError( aMessage ) )) {
    return;
  }
  nsAutoString wstr;
  aMessage->GetMessageMoz(getter_Copies( wstr ));
  mFile.Assign(nsnull,0);
  mLine = nsITestRunner::errorNoError;
  mText.Assign( NS_ConvertUTF16toUTF8(wstr));
}

nsFormatJSMessage::nsFormatJSMessage(const char *aFile)
{
  mFile.Assign(aFile);
  getSourceFile();
}

nsresult
nsFormatJSMessage::parseScriptError(nsIConsoleMessage *aMessage)
{
  nsresult rv;
  nsCOMPtr<nsIScriptError> error;
  nsAutoString wstr;

  error = do_QueryInterface( aMessage, &rv );
  if (NS_FAILED(rv))
    return rv;

  rv = formatErrorMessage(error);
  NS_ENSURE_SUCCESS(rv, rv);
  
  if (NS_FAILED( getSourceFile() )) {
    mFile.Assign(nsnull, 0);
    mLine = nsITestRunner::errorJS;
  }

  return NS_OK;
}

nsresult
nsFormatJSMessage::formatErrorMessage(nsIScriptError *error)
{
  nsresult rv;
  nsAutoString wstr;
  
  rv = error->GetErrorMessage( wstr );
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString text = NS_ConvertUTF16toUTF8( wstr );

  rv = error->GetLineNumber( &mLine );
  if (NS_SUCCEEDED( rv  ))
    rv = error->GetSourceName( wstr );
  if (NS_SUCCEEDED( rv  ))
    mFile = NS_ConvertUTF16toUTF8(wstr);

  if ( parseException(text) )
    return NS_OK;

  PRUint32 flags;
  rv = error->GetFlags( &flags );
  if (NS_FAILED( rv ))
    flags = 0;

  if ( flags & nsIScriptError::warningFlag ) {
    mText.Assign("warning: ");
  } else {
    mText.Assign("error: ");
  }
  mText.Append( text );
  return NS_OK;
}

PRBool
nsFormatJSMessage::parseException(const nsACString &aMessage)
{
  const char *head, *pos, *res, *end;
  nsresult rv;

  head = aMessage.BeginReading();
  pos = strstr(head, "[Exception... \"");
  if (! pos)
    return PR_FALSE;
  
  mText.Assign("exception: ");

  if (pos[15] == '\'') {
    end = strchr(&pos[16], '\'');
    if (end) {
      mText.Append(&pos[15], end - pos - 14);
    }
  } else if ( (res = strstr(&pos[15], "nsresult: \"0x")) && strlen(res) > 23) {
      end = strchr(&res[23], ')');
      if (end) {
        mText.Append(&res[23], end - res - 23);
      }
  } else {
    mText.Append(&pos[15],strlen(pos) - 16);
  }

  if ( ! mFile.Length() ) {
    res = strstr(&pos[15],"location: \"JS frame :: ");
    if (!res || strlen(res) < 25)
      return PR_TRUE;
    end = strstr(&res[23], " :: ");
    if (! end)
      return PR_TRUE;
    mFile.Assign(Substring(&res[23], end - res - 23));
    res = strstr(&end[4], ":: line ");
    if (!res && strlen(res) < 10)
      return PR_TRUE;
    end = strchr(&res[8], '"');
    if (! end)
      return PR_TRUE;
    mLine = Substring(&res[8], end - res - 8).ToInteger(&rv);
  }
  return PR_TRUE;
}

nsresult
nsFormatJSMessage::getSourceFile()
{
  if ( StringHead(mFile, 7).Equals(NS_LITERAL_CSTRING("file://")) ) {
    return parseLocalFile();
  } else if (! StringHead(mFile, 9).Equals(NS_LITERAL_CSTRING("chrome://")) ) {
    return NS_ERROR_FILE_UNRECOGNIZED_PATH;
  }
  return parseChrome();
}

nsresult
nsFormatJSMessage::parseLocalFile()
{
  nsCAutoString leafName;
  const PRUnichar* resolved;

  leafName = "file://";
  leafName += strrchr(mFile.BeginReading(), '/');
  if (! mResolver) {
    mResolver = nsTestRunner::GetTestRunner();
    NS_ENSURE_TRUE( mResolver, NS_OK );
  }
  mResolver->resolve( leafName, &resolved );
  if (! resolved )
    return NS_OK;
  if ( resolved[0] )
    mFile.Assign(NS_ConvertUTF16toUTF8( resolved ));

  return NS_OK;
}

nsresult
nsFormatJSMessage::parseChrome()
{ 
  nsCAutoString head;
  const char *tail, *ptr = 0;
  PRUint32 len;
  const PRUnichar* resolved;

  len = mFile.Length();
  tail = strrchr(mFile.BeginReading(), '/');
  head = StringHead(mFile, len - strlen(tail) );
  if (! mResolver) {
    mResolver = nsTestRunner::GetTestRunner();
    NS_ENSURE_TRUE( mResolver, NS_OK );
  }
  mResolver->resolve( head, &resolved);
  while( ! resolved && (head.Length() > 9) ) {
    ptr = strrchr(head.BeginReading(), '/');
    tail -= strlen(ptr);
    head = StringHead(mFile, len - strlen(tail) );
    mResolver->resolve( head, &resolved);
  }
  if (! resolved )
    return NS_OK;

  head = NS_ConvertUTF16toUTF8(resolved);
  head += tail;
  mFile.Assign( head );

  return NS_OK;
}
