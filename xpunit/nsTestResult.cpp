/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include <stdio.h>
#include "nsCOMPtr.h"

/* Unfrozen interfaces */
#include "nsDeque.h"
#include "nsIConsoleService.h"
#include "nsIConsoleMessage.h"
#include "nsServiceManagerUtils.h"

/* Project includes */
#include "nsTestResult.h"
#include "nsTestFailure.h"

/******************** nsTestResult ************************************/
nsTestResult::nsTestResult()
  : mFailureList(0)
  , mLock("nsTestResult Lock")
{
  mCount = 0;
  mFailureCount = 0;
  mErrorCount = 0;
  mStartTime = PR_IntervalNow();
  mSilent = PR_FALSE;
}

nsTestResult::~nsTestResult()
{
  nsTestFailure *failure;

  while ( mFailureList.GetSize() ) {
    if ( (failure = (nsTestFailure *) mFailureList.PopFront()) ) {
      delete failure;
    }
  }
}

NS_IMPL_ISUPPORTS1(nsTestResult,
                   nsITestResult)

NS_IMETHODIMP
nsTestResult::MarkTestStart()
{
  mozilla::MutexAutoLock lock(mLock);
  mCount++;
  if (mSilent)
    return NS_OK;
  printf_stderr(".");
  if (! (mCount % 50))
    printf_stderr("\n");
  return NS_OK;
}

NS_IMETHODIMP
nsTestResult::AddFailure(const char *aFile, PRUint32 aLine, const char *aText,
    PRBool aError)
{
  mozilla::MutexAutoLock lock(mLock);
  nsTestFailure *failure = new nsTestFailure(aFile, aLine, aText, aError);
  NS_ENSURE_TRUE(failure, NS_ERROR_OUT_OF_MEMORY);
  mFailureList.Push( (void *) failure );
  if (mSilent)
    return NS_OK;

  if ( ! aError ) {
    printf_stderr("F");
    mFailureCount++;
  } else if ( aLine ) {
    printf_stderr("E");
    mErrorCount++;
  }
  
  return NS_OK;
}

NS_IMETHODIMP
nsTestResult::Done()
{
  if (mSilent)
    return NS_OK;

  printFailures();
  printTime();
  printHeadline();

  return NS_OK;
}

void
nsTestResult::printFailures()
{
  nsTestFailure *failure;
  const static char *kFailureMessage = "test failed";
  const static char *kFailureHeader = " failure:";
  const static char *kErrorMessages[] = {
    "cxxunit: message: %s\n"
    ,"cxxunit: error: component \"%s\" failed to load\n"
    ,"cxxunit: %s\n"
    ,"cxxunit: error: failed to open main window\n"
    ,"cxxunit: error: component \"%s\" failed to complete async test\n"
  };

  printf_stderr("\n");
  while ( mFailureList.GetSize() ) {
    if ( ! (failure = (nsTestFailure *) mFailureList.PopFront()) )
      continue;
    if (  failure->getFile()[0] ){
      const char *msg = kFailureMessage;
      if ( failure->getText()[0] ) {
        msg = failure->getText();
      }
      printf_stderr("%s:%u:%s %s\n", failure->getFile(), failure->getLine(),
          failure->isError() ? "" : kFailureHeader, msg);
    } else {
      printf_stderr( kErrorMessages[ failure->getLine() ],
          failure->getText() );
    }
    delete failure;
  }
}

void
nsTestResult::printTime()
{
  PRInt32 runTime, msec, sec;

  runTime = PR_IntervalToMilliseconds(PR_IntervalNow() - mStartTime);
  sec = runTime / 1000;
  msec = runTime % 1000;
  printf_stderr("Time: %u.%03u\n\n", sec, msec);
}

void
nsTestResult::printHeadline()
{
  if (!mFailureCount && !mErrorCount) {
    printf_stderr("OK (%u tests)\n", mCount);
    return;
  }
  printf_stderr("Failures!!!\n Test run: %u Failures: %u Errors: %u\n",
      mCount, mFailureCount, mErrorCount);
}

