/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"
#include "nsServiceManagerUtils.h"
#include "nsAutoPtr.h"
#include "nsNetUtil.h"

/* Unfrozen interfaces */
#include "nsICompareListener.h"
#include "nsIStringEnumerator.h"

/* Project includes */
#include "nsResultGuard.h"
#include "nsSelfTest.h"
#include "nsTestUtils.h"
#include "nsTestRunner.h"
#include "nsTestResult.h"
#include "nsTestFailure.h"

NS_IMPL_ISUPPORTS1(nsSelfTest, nsITest)

NS_IMETHODIMP
nsSelfTest::Test(nsITestRunner *aTestRunner)
{
  nsresult rv;
  nsResultGuard guard(this, aTestRunner);
  NS_TEST_BEGIN(aTestRunner);

  NS_TEST_ASSERT(PR_TRUE);

  rv = testResultGuard(aTestRunner);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = testFileComp(aTestRunner);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = testXml(aTestRunner);
  NS_ENSURE_SUCCESS(rv, rv);

  return guard.FixResult(__FILE__, __LINE__);
}

nsresult
nsSelfTest::testResultGuard(nsITestRunner *aTestRunner)
{
  nsresult rv;
  nsResultGuard guard(this);
  NS_TEST_BEGIN(aTestRunner);

  nsRefPtr<nsTestRunner> runner;
  rv =  aTestRunner->QueryInterface(NS_GET_IID(nsTestRunner),
      getter_AddRefs(runner));
  NS_ENSURE_SUCCESS(rv, rv);

  runner->mTestResult = nsnull;
  return NS_OK;
}

#define NS_TEST_FCMP_REF_URL "resource:///tests/1.txt"
#define NS_TEST_FCMP_DATA_URL "resource:///tests/2.txt"

nsresult
nsSelfTest::testFileComp(nsITestRunner *aTestRunner)
{
  nsresult rv;
  nsResultGuard guard(this);
  NS_TEST_BEGIN(aTestRunner);

  nsRefPtr<nsTestRunner> runner;
  rv =  aTestRunner->QueryInterface(NS_GET_IID(nsTestRunner),
      getter_AddRefs(runner));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsICompareListener> listener = do_QueryInterface(aTestRunner, &rv);
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<nsTestResult> silent = new nsTestResult();
  NS_ENSURE_TRUE(silent, NS_ERROR_OUT_OF_MEMORY);
  silent->mSilent = PR_TRUE;

  runner->mTestResult = silent;
  
  nsCOMPtr<nsIURI> reference;
  nsCOMPtr<nsIChannel> dataChannel;
  nsCOMPtr<nsIIOService> io = do_GetIOService(&rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = io->NewURI(NS_LITERAL_CSTRING(NS_TEST_FCMP_REF_URL), nsnull, nsnull,
      getter_AddRefs(reference));

  NS_ENSURE_SUCCESS(rv, rv);

  rv = io->NewChannel(NS_LITERAL_CSTRING(NS_TEST_FCMP_DATA_URL), nsnull, nsnull,
      getter_AddRefs(dataChannel));
  NS_ENSURE_SUCCESS(rv, rv);

  const PRUint32 line = __LINE__;
  rv = aTestRunner->CompareWithFile(__FILE__, line, dataChannel, reference);
  if (NS_FAILED(rv)) return rv;

  guard.FixResult(nsnull, 0);
  NS_TEST_ASSERT_MSG(silent->mFailureList.GetSize() == 2,
      "compare test: bad failure count");

  nsTestFailure* failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(NS_TEST_FCMP_REF_URL), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == 2,
      "compare test: line is not equal to 2");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("\"3\""), failure->getText());

  delete failure;

  failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(__FILE__), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == line,
      "compare test: bad test origin line");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("included from here"), failure->getText());

  delete failure;

  return NS_OK;
}

#define NS_XPUNIT_XML_TEST_URL "resource:///tests/xml/xpunit.xml"
#define NS_XPUNIT_XML_TEST_CONTRACT "@aasii.org/xpunit/xml;1"
#define NS_XPUNIT_XML_SUBTEST1_CONTRACT "@aasii.org/xpunit/xml/child1;1"
#define NS_XPUNIT_XML_SUBTEST2_CONTRACT "@aasii.org/xpunit/xml/child2;1"

nsresult
nsSelfTest::testXml(nsITestRunner *aTestRunner)
{
  nsresult rv = NS_OK;
  nsResultGuard guard(this);
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<nsITest> xmlTest = do_CreateInstance(NS_XPUNIT_XML_TEST_CONTRACT);
  NS_TEST_ASSERT_MSG(xmlTest, "xml test is not registered");

  nsCOMPtr<nsIUTF8StringEnumerator> subTests = do_QueryInterface(xmlTest, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(subTests, NS_ERROR_UNEXPECTED);

  PRBool hasMore = PR_FALSE;
  PRInt32 childCount = 0;
  while (NS_SUCCEEDED(rv = subTests->HasMore(&hasMore)) && hasMore)
  {
    ++ childCount;
    nsCAutoString child;
    rv = subTests->GetNext(child);
    NS_ENSURE_SUCCESS(rv, rv);

    NS_TEST_ASSERT_MSG(child.EqualsLiteral(NS_XPUNIT_XML_SUBTEST1_CONTRACT) ||
                       child.EqualsLiteral(NS_XPUNIT_XML_SUBTEST2_CONTRACT),
                       "xml test: invalid child contract");
  }
  NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(2 == childCount,
      "xml test: count of child contracts is not equal to 2");

  nsRefPtr<nsTestRunner> runner;
  rv =  aTestRunner->QueryInterface(NS_GET_IID(nsTestRunner),
      getter_AddRefs(runner));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsTestResult> silent = new nsTestResult();
  NS_ENSURE_TRUE(silent, NS_ERROR_OUT_OF_MEMORY);
  silent->mSilent = PR_TRUE;

  runner->mTestResult = silent;

  rv = xmlTest->Test(aTestRunner);
  NS_ENSURE_SUCCESS(rv, rv);

  guard.FixResult(nsnull, 0);
  NS_TEST_ASSERT_MSG(silent->mFailureList.GetSize() == 4,
      "compare test: bad failure count");

  nsTestFailure* failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(NS_TEST_FCMP_REF_URL), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == 2,
      "compare test: line is not equal to 2");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("\"3\""), failure->getText());

  delete failure;

  failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(NS_XPUNIT_XML_TEST_URL), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == 8,
      "compare test: bad test origin line");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("included from here"), failure->getText());

  delete failure;

  failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(NS_TEST_FCMP_DATA_URL), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == 2,
      "compare test: line is not equal to 2");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("\"2\""), failure->getText());

  delete failure;

  failure = (nsTestFailure *) silent->mFailureList.PopFront();
  NS_ENSURE_TRUE(failure, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getFile())
      .EqualsLiteral(NS_XPUNIT_XML_TEST_URL), failure->getFile());

  NS_TEST_ASSERT_MSG(!failure->isError(), "compare test: failure is error");

  NS_TEST_ASSERT_MSG(failure->getLine() == 12,
      "compare test: bad test origin line");

  NS_TEST_ASSERT_MSG(nsDependentCString(failure->getText())
      .EqualsLiteral("included from here"), failure->getText());

  delete failure;

  return NS_OK;
}

nsITestResult*
nsSelfTest::GetResult()
{
  nsresult rv;
  nsCOMPtr<nsITestRunner> iRunner = do_GetService(NS_TESTRUNNER_CONTRACT);
  NS_ENSURE_TRUE(iRunner, nsnull);

  nsRefPtr<nsTestRunner> runner;
  rv = iRunner->QueryInterface(NS_GET_IID(nsTestRunner),
      getter_AddRefs(runner));
  NS_ENSURE_SUCCESS(rv, nsnull);

  return runner->mTestResult;
}

void
nsSelfTest::RestoreResult(nsITestResult *testResult)
{
  nsresult rv;
  nsCOMPtr<nsITestRunner> iRunner = do_GetService(NS_TESTRUNNER_CONTRACT);
  NS_ENSURE_TRUE(iRunner, (void) nsnull);

  nsRefPtr<nsTestRunner> runner;
  rv = iRunner->QueryInterface(NS_GET_IID(nsTestRunner),
      getter_AddRefs(runner));
  NS_ENSURE_SUCCESS(rv, (void) nsnull);

  runner->mTestResult = testResult;
}
