/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"
#include "nsServiceManagerUtils.h"

/* Unfrozen interfaces */

/* Project includes */
#include "nsITestResult.h"
#include "nsITestRunner.h"
#include "nsTestUtils.h"
#include "nsResultGuard.h"
#include "nsSelfTest.h"

nsResultGuard::nsResultGuard(nsSelfTest *test, nsITestRunner *runner)
  :mTest(test), mRunner(runner)
{
  if (NS_UNLIKELY(!mTest))
    return;
  mResult = mTest->GetResult();
}

nsresult
nsResultGuard::FixResult(const char *file, PRUint32 line)
{
  nsresult rv;

  NS_ENSURE_STATE(mTest);

  if (mResult == mTest->GetResult())
    return NS_OK;

  mTest->RestoreResult(mResult);

  if (!file)
    return NS_ERROR_FAILURE;

  NS_ENSURE_STATE(mRunner);
  rv = mRunner->AddFailure(file, line, "xpunit: wrong test result processor");
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_ERROR_FAILURE;
}

nsResultGuard::~nsResultGuard()
{
  FixResult(nsnull, 0);
}
