/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef NSSELFTEST_H
#define NSSELFTEST_H

#include "xpcom-config.h"

/* Unfrozen interfaces */

/* Project includes */
#include "nsITest.h"

#define NS_TEST_XPUNIT_CID \
{0xf7497445, 0x2747, 0x4468, {0x8f, 0xfc, 0x5e, 0x82, 0x6d, 0xc4, 0x53, 0x15}}

#define NS_TEST_XPUNIT_CONTRACT "@aasii.org/xpunit/unit;1"

class nsITestResult;
class nsTestRunner;

class nsSelfTest: public nsITest {
public:
  nsSelfTest() {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST

  nsITestResult* GetResult();
  void RestoreResult(nsITestResult *testResult);
private:
  virtual ~nsSelfTest() {}

  nsresult testResultGuard(nsITestRunner *aRunner);
  nsresult testFileComp(nsITestRunner *aRunner);
  nsresult testXml(nsITestRunner *aRunner);
};

#endif
