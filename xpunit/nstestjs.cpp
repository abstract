/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=c: */
/*
 * Copyright (C) 2007,2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include "jscntxt.h"
#include "jsinterp.h"
#include "jsscript.h"
#include "jsdbgapi.h"

#include "nstestjs.h"

const char *
ns_test_js_caller_filename(JSContext *cx)
{
  if (! cx->fp || ! cx->fp->down || ! cx->fp->down->script)
    return (const char *)0;
  return cx->fp->down->script->filename;
}

unsigned long
ns_test_js_caller_lineno(JSContext *cx)
{
  if (! cx->fp || ! cx->fp->down || ! cx->fp->down->script
      || ! cx->fp->down->regs)
    return 0;
  return JS_PCToLineNumber(cx, cx->fp->down->script,
        cx->fp->down->regs->pc);
}

