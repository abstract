/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsOneLineDiff.h"
#include "nsICompareListener.h"
#include "nsIChannel.h"
#include "nsILineInputStream.h"

#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

NS_IMPL_ISUPPORTS2(nsOneLineDiff,
		   nsIStreamListener,
		   nsIRequestObserver)

NS_IMETHODIMP
nsOneLineDiff::Compare(nsIChannel* aReference, nsIChannel* aTest,
    nsICompareListener* aListener)
{
  nsresult rv = NS_OK;

  NS_ENSURE_ARG_POINTER(aListener);

  mListener = aListener;

  nsCOMPtr<nsIInputStream> iStream;
  rv = aReference->Open(getter_AddRefs(iStream));
  NS_ENSURE_SUCCESS(rv, rv);

  mReference = do_QueryInterface(iStream, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  mLine = 0;
  rv = NS_InitLineBuffer(&mBuffer);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aTest->AsyncOpen(this, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);
  return NS_OK;
}

NS_IMETHODIMP
nsOneLineDiff::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext,
		nsIInputStream *aInputStream, PRUint32 aOffset,
		PRUint32 aCount)
{
  nsresult rv = NS_OK;
  nsCAutoString data;
  nsCAutoString ref;
  PRBool hasMore = PR_FALSE;
  do {
    rv = NS_ReadLine(aInputStream, mBuffer, data, &hasMore);
    if (NS_FAILED(rv))
     hasMore = PR_FALSE;

    if (!data.Length())
      return NS_OK;

    if (!mRefHasMore)
    {
      mIsErrorCalled = PR_TRUE;
      nsCAutoString error("text after EOF:");
      error.Append(ref);
      mListener->OnError(error, mLine);
      return NS_ERROR_FAILURE;
    }

    rv = mReference->ReadLine(ref, &mRefHasMore);
    NS_ENSURE_SUCCESS(rv, rv);

    ++mLine;
    
    if (!ref.Equals(data))
    {
      mIsErrorCalled = PR_TRUE;
      mListener->OnMismatch(data, mLine);
      return NS_ERROR_FAILURE;
    }
  } while(hasMore);
  return NS_OK;
}

NS_IMETHODIMP
nsOneLineDiff::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  return NS_OK;
}

NS_IMETHODIMP
nsOneLineDiff::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext,
		nsresult aStatusCode)
{
  nsresult rv = NS_OK;
  PR_Free(mBuffer);
  mBuffer = nsnull;
  mReference = nsnull;

  if (!mIsErrorCalled && mRefHasMore)
  {
    rv = mListener->OnError(NS_LITERAL_CSTRING("unexpected end of file"),
        mLine++);
    NS_ENSURE_SUCCESS(rv, rv);
    mIsErrorCalled = PR_TRUE;
  }
  if (!mIsErrorCalled)
  {
    rv = mListener->OnSuccess();
    NS_ENSURE_SUCCESS(rv, rv);
  }
  return NS_OK;
}

