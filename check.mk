#!/usr/bin/make -f 
# check.mk -- Run make -f check.mk for integrated unit testing
# Copyright (C) 2007 Sergey Yanovich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

top_srcdir := $(shell cd .. && pwd)
HOST_OS_GUESS=$(shell ../build/autoconf/config.guess)
HOST_OS=$(shell ../build/autoconf/config.sub $(HOST_OS_GUESS))

ifneq (,$(findstring mingw,$(HOST_OS)))
OS_ARCH=w32
LIBXULPATH=/src/xulrunner-build/dist/
else
ifneq (,$(findstring linux,$(HOST_OS)))
OS_ARCH=linux
LIBXULPATH=$(shell PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig pkg-config --variable=sdkdir libxul)
else
$(error "platform not supported, yet")
endif
endif

all: build

setup: objdir/Makefile

mozconfig: app/test/unit/mozconfig.$(OS_ARCH)
	cp $< $@

objdir/Makefile: mozconfig $(top_srcdir)/configure $(top_srcdir)/nsprpub/configure
	test -d objdir || mkdir objdir
	echo $(LIBXULPATH)
	cd objdir && \
	MOZCONFIG=$(top_srcdir)/abstract/mozconfig $(top_srcdir)/configure \
	--with-libxul-sdk=$(LIBXULPATH)
	touch $@

$(top_srcdir)/configure: $(top_srcdir)/configure.in
	cd $(top_srcdir) && autoconf

$(top_srcdir)/nsprpub/configure: $(top_srcdir)/nsprpub/configure.in
	cd $(top_srcdir)/nsprpub && autoconf

objdir/nsprpub/config/nspr-config: objdir/Makefile
	test -d objdir/nsprpub/config || mkdir -p objdir/nsprpub/config
	cp nspr-config $@

build: objdir/Makefile objdir/nsprpub/config/nspr-config
	$(MAKE) -C objdir

clean: objdir/Makefile
	$(MAKE) -C objdir clean

install: objdir/Makefile
	$(MAKE) -C objdir install

objpkg/Makefile: $(top_srcdir)/configure
	test -d objpkg || mkdir objpkg
	cd objpkg && \
	MOZCONFIG=$(top_srcdir)/abstract/installer/mozconfig $(top_srcdir)/configure \
	--with-libxul-sdk=$(LIBXULPATH)

installer: objpkg/Makefile
	$(MAKE) -C objpkg
	$(MAKE) -C objpkg installer

package: objpkg/Makefile
	$(MAKE) -C objpkg
	$(MAKE) -C objpkg package

.PHONY: all setup clean build install package installer
