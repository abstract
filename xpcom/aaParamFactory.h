/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#define NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(_InstanceClass)                  \
static NS_IMETHODIMP                                                          \
_InstanceClass##Constructor(nsISupports *aOuter, REFNSIID aIID,               \
                            void **aResult)                                   \
{                                                                             \
  nsresult rv;                                                                \
                                                                              \
  _InstanceClass * inst;                                                      \
                                                                              \
  *aResult = NULL;                                                            \
                                                                              \
  inst = new _InstanceClass();                                                \
  if (NULL == inst) {                                                         \
    rv = NS_ERROR_OUT_OF_MEMORY;                                              \
    return rv;                                                                \
  }                                                                           \
  NS_ADDREF(inst);                                                            \
  rv = inst->Init(aOuter);                                                    \
  if (NS_SUCCEEDED(rv))                                                       \
    rv = inst->QueryInterface(aIID, aResult);                                 \
  NS_RELEASE(inst);                                                           \
                                                                              \
  return rv;                                                                  \
}                                                                             \

