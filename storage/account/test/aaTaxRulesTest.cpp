/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsIMutableArray.h"
#include "nsArrayUtils.h"

#include "nsDirectoryServiceDefs.h"
#include "nsServiceManagerUtils.h"
#include "nsIProperties.h"
#include "nsIFile.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIRule.h"
#include "aaIFact.h"
#include "aaIEvent.h"
#include "aaITransaction.h"
#include "aaIBalance.h"
#include "aaBaseKeys.h"
#include "aaIChart.h"
#include "aaISession.h"
#include "aaIManager.h"
#include "aaISqlRequest.h"
#include "aaISqlTransaction.h"
#include "aaSessionUtils.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"

#include "aaBaseTestUtils.h"
#include "aaTaxRulesTest.h"

NS_IMPL_ISUPPORTS1(aaTaxRulesTest,
                   nsITest)
/* nsITest */
NS_IMETHODIMP
aaTaxRulesTest::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED( res.status )) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_TAX_RULES_TEST_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }

  testSaveRule(aTestRunner);
  return NS_OK;
}

/* Helpers */
aaTaxRulesTest::RAII::RAII(aaTaxRulesTest *t)
  :test(t)
{
  status = init();
}

aaTaxRulesTest::RAII::~RAII()
{
  if (!test)
    return;

  test->mSession = nsnull;
  test = nsnull;
}

nsresult
aaTaxRulesTest::RAII::init()
{
  NS_ENSURE_ARG_POINTER(test);
  nsresult rv;

  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file, dir;
  rv = manager->TmpFileFromString("rule.sqlite", PR_TRUE,
      getter_AddRefs( file ));
  NS_ENSURE_SUCCESS(rv, rv);

  test->mSession = do_CreateInstance(AA_SESSION_CONTRACT, file, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* private methods */
nsresult
aaTaxRulesTest::testSaveRule(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);

  nsresult rv;
  nsCOMPtr<aaIMoney> rub;
  nsCOMPtr<aaIAsset> x, y;
  nsCOMPtr<aaIAsset> shipment;
  nsCOMPtr<aaIEntity> keeper;
  nsCOMPtr<aaIEntity> supplier, buyer, bank;
  nsCOMPtr<aaIFlow> bankAccount, purchaseX, purchaseY, storageX, storageY,
    shipmentFlow, saleX, saleY;

  /* Create currency and init chart */
  {
    rub = do_CreateInstance("@aasii.org/base/money;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = rub->SetAlphaCode(NS_LITERAL_STRING("RUB"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = rub->SetNumCode(643);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(rub, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<aaIChart> chart = 
      do_CreateInstance("@aasii.org/accounting/chart;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    chart->SetCurrency(rub);

    rv = mSession->Save(chart, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Create resources*/
  {
    shipment = do_CreateInstance("@aasii.org/base/asset;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = shipment->SetTag(NS_LITERAL_STRING("shipment"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(shipment, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    x = do_CreateInstance("@aasii.org/base/asset;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = x->SetTag(NS_LITERAL_STRING("resource x"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(x, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    y = do_CreateInstance("@aasii.org/base/asset;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = y->SetTag(NS_LITERAL_STRING("y"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(y, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Create entities*/
  {
    keeper = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = keeper->SetTag(NS_LITERAL_STRING("keeper"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(keeper, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    bank = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = bank->SetTag(NS_LITERAL_STRING("bank"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(bank, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    supplier = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = supplier->SetTag(NS_LITERAL_STRING("supplier"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(supplier, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    buyer = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = buyer->SetTag(NS_LITERAL_STRING("buyer"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(buyer, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Create accounts */
  {
    bankAccount = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    bankAccount->SetEntity(bank);
    bankAccount->SetGiveResource(rub);
    bankAccount->SetTakeResource(rub);
    bankAccount->SetRate(1.0);
    bankAccount->SetTag(NS_LITERAL_STRING("bank account"));
    rv = mSession->Save(bankAccount, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    purchaseX = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    purchaseX->SetEntity(supplier);
    purchaseX->SetGiveResource(rub);
    purchaseX->SetTakeResource(x);
    purchaseX->SetRate(1/100.0);
    purchaseX->SetTag(NS_LITERAL_STRING("purchase 1"));
    rv = mSession->Save(purchaseX, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    purchaseY = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    purchaseY->SetEntity(supplier);
    purchaseY->SetGiveResource(rub);
    purchaseY->SetTakeResource(y);
    purchaseY->SetRate(1/150.0);
    purchaseY->SetTag(NS_LITERAL_STRING("purchase 2"));
    rv = mSession->Save(purchaseY, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    storageX = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    storageX->SetEntity(keeper);
    storageX->SetGiveResource(x);
    storageX->SetTakeResource(x);
    storageX->SetRate(1.0);
    storageX->SetTag(NS_LITERAL_STRING("storage 1"));
    rv = mSession->Save(storageX, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    storageY = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    storageY->SetEntity(keeper);
    storageY->SetGiveResource(y);
    storageY->SetTakeResource(y);
    storageY->SetRate(1.0);
    storageY->SetTag(NS_LITERAL_STRING("storage 2"));
    rv = mSession->Save(storageY, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    saleX = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    saleX->SetEntity(supplier);
    saleX->SetGiveResource(x);
    saleX->SetTakeResource(rub);
    saleX->SetRate(120.0);
    saleX->SetTag(NS_LITERAL_STRING("sale 1"));
    rv = mSession->Save(saleX, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    saleY = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    saleY->SetEntity(supplier);
    saleY->SetGiveResource(y);
    saleY->SetTakeResource(rub);
    saleY->SetRate(160.0);
    saleY->SetTag(NS_LITERAL_STRING("sale 2"));
    rv = mSession->Save(saleY, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  PRExplodedTime tm = {0,0,0,12,16,8,2008};
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  /* Register and process preparing transactions */
  {
    /* Fact 1 */
    event = do_CreateInstance(AA_EVENT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance(AA_FACT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(purchaseX);
    fact->SetGiveTo(storageX);
    fact->SetAmount(50.0);
    block->AppendElement(fact, PR_FALSE);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    txn = do_CreateInstance(AA_TRANSACTION_CONTRACT, &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    /* Fact 2 */
    event = do_CreateInstance(AA_EVENT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance(AA_FACT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(purchaseY);
    fact->SetGiveTo(storageY);
    fact->SetAmount(50.0);
    block->AppendElement(fact, PR_FALSE);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    txn = do_CreateInstance(AA_TRANSACTION_CONTRACT, &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Test balances before rules */
  nsCOMPtr<aaISqlRequest> balanceLoader;
  nsCOMPtr<nsIArray> set;
  PRUint32 count;
  nsCOMPtr<aaIBalance> balance;
  {
    balanceLoader  = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(balanceLoader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    rv = set->GetLength(&count);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(count == 4, "rule: wrong balance count after txn2");

    balance = do_QueryElementAt(set, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 643, 5000.0,
          5000.0, 0),
        "rule: purchaseX balance is wrong after txn1");
    balance = do_QueryElementAt(set, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 643, 7500.0,
          7500.0, 0),
        "rule: purchaseY balance is wrong after txn1");
    balance = (do_QueryElementAt(set, 2));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, 50.0,
          5000.0, 1),
        "rule: storageX balance is wrong after txn1");
    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 5, 3, 50.0,
          7500.0, 1),
        "rule: storageY balance is wrong after txn1");
  }

  nsCOMPtr<aaIRule> rule;
  /* Create shipment flow and rules */
  {
    shipmentFlow = do_CreateInstance(AA_FLOW_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    shipmentFlow->SetEntity(supplier);
    shipmentFlow->SetGiveResource(shipment);
    shipmentFlow->SetTakeResource(shipment);
    shipmentFlow->SetRate(1.0);
    shipmentFlow->SetTag(NS_LITERAL_STRING("shipment 1"));
    shipmentFlow->SetIsOffBalance(PR_TRUE);

    /* shipment1.rule1 */
    rule = do_CreateInstance(AA_RULE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetTag(NS_LITERAL_STRING("shipment1.rule1"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetTakeFrom(storageX);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetGiveTo(saleX);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetFactSide(0);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetChangeSide(1);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetRate(27.0);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = shipmentFlow->StoreRule(rule);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    NS_TEST_ASSERT_MSG(rule->PickId() == 1, "rule: wrong id");

    /* shipment1.rule2 */
    rule = do_CreateInstance(AA_RULE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetTag(NS_LITERAL_STRING("shipment1.rule2"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->SetTakeFrom(storageY); NS_TEST_ASSERT_OK(rv);
    rv = rule->SetGiveTo(saleY); NS_TEST_ASSERT_OK(rv);
    rv = rule->SetFactSide(0); NS_TEST_ASSERT_OK(rv);
    rv = rule->SetChangeSide(1); NS_TEST_ASSERT_OK(rv);
    rv = rule->SetRate(42.0); NS_TEST_ASSERT_OK(rv);
    rv = shipmentFlow->StoreRule(rule); NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(rule->PickId() == 2, "rule: wrong id");

    rv = mSession->Save(shipmentFlow, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<aaISqlTransaction> ruleLoader;
  nsCOMPtr<aaISqlRequest> flowLoader;
  nsCOMPtr<aaIFlow> testFlow;
  /* Test saved rules */
  {
    flowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    ruleLoader = do_CreateInstance(AA_LOADRULE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(flowLoader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    testFlow = do_QueryElementAt(set, 7, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    NS_TEST_ASSERT_MSG(testFlow->PickId() == 8,
        "rule: wrong shipment1 flow in database");
    PRBool isOffBalance;
    testFlow->GetIsOffBalance(&isOffBalance);
    NS_TEST_ASSERT_MSG(isOffBalance, "rule: shipment1 not OffBalance");

    rv = ruleLoader->SetParam(testFlow);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<aaIFlow> resultFlow;
    rv = mSession->Execute(ruleLoader, getter_AddRefs(resultFlow));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    set = do_QueryInterface(resultFlow, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = set->GetLength(&count);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(count == 2, "rule: wrong norm count for shipment1");

    rule = do_QueryElementAt(set, 0);
    NS_TEST_ASSERT_MSG(testRule(aTestRunner, rule, 4, 6, 0, 1, 27.0,
          NS_LITERAL_STRING("shipment1.rule1")),
        "rule: shipment1.rule1 is wrong");

    rule = do_QueryElementAt(set, 1);
    NS_TEST_ASSERT_MSG(testRule(aTestRunner, rule, 5, 7, 0, 1, 42.0,
          NS_LITERAL_STRING("shipment1.rule2")),
        "rule: shipment1.rule2 is wrong");
  }

  /* Register the rule invoking transaction */
  tm.tm_mday = 22;
  nsCOMPtr<aaISqlRequest> stateLoader;
  nsCOMPtr<aaIState> state;
  {
    event = do_CreateInstance(AA_EVENT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance(AA_FACT_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(nsnull);
    fact->SetGiveTo(shipmentFlow);
    fact->SetAmount(1.0);
    block->AppendElement(fact, PR_FALSE);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    stateLoader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(stateLoader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "rule: wrong state count after event3");

    state = do_QueryElementAt(set, 0);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 2, 643, 5000.0),
        "rule: wrong state of purchaseX after event3");

    state = do_QueryElementAt(set, 1);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, 643, 7500.0),
        "rule: wrong state of purchaseY after event3");

    state = do_QueryElementAt(set, 2);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 4, 2, 23.0),
        "rule: wrong state of storageX after event3");

    state = do_QueryElementAt(set, 3);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 5, 3, 8.0),
        "rule: wrong state of storageY after event3");

    state = do_QueryElementAt(set, 4);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 6, 643, 120.0 * 27.0),
        "rule: wrong state of saleX after event3");

    state = do_QueryElementAt(set, 5);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 7, 643, 160.0 * 42.0),
        "rule: wrong state of saleY after event3");

    state = do_QueryElementAt(set, 6);
    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 8, 1, 1.0),
        "rule: wrong state of shipment1 after event3");
  }

  /* Process the rule transaction, balances shouldn't change */
  {
    txn = do_CreateInstance(AA_TRANSACTION_CONTRACT, &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(balanceLoader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    rv = set->GetLength(&count);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(count == 4, "rule: wrong balance count after txn2");

    balance = do_QueryElementAt(set, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 643, 5000.0,
          5000.0, 0),
        "rule: purchaseX balance is wrong after txn1");
    balance = do_QueryElementAt(set, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 643, 7500.0,
          7500.0, 0),
        "rule: purchaseY balance is wrong after txn1");
    balance = (do_QueryElementAt(set, 2));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, 50.0,
          5000.0, 1),
        "rule: storageX balance is wrong after txn1");
    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 5, 3, 50.0,
          7500.0, 1),
        "rule: storageY balance is wrong after txn1");
  }

  /* Complete event3 processing and check the resulting balances*/
  {
    PRUint32 i;
    block->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 3, "rule: wrong fact count in event3");

    for (i = 1; i < count; i++) {
      fact = do_QueryElementAt(block, i, &rv);
      NS_TEST_ASSERT_OK(rv);
      NS_ENSURE_SUCCESS(rv, rv);

      txn = do_CreateInstance(AA_TRANSACTION_CONTRACT, &rv);
      txn->SetFact(fact);
      rv = mSession->Save(txn, nsnull);
      NS_TEST_ASSERT_OK(rv);
      NS_ENSURE_SUCCESS(rv, rv);
    }

    rv = mSession->Load(balanceLoader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    rv = set->GetLength(&count);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(count == 6, "rule: wrong balance count after event3");

    balance = (do_QueryElementAt(set, 2));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, 23.0,
          2300.0, 1),
        "rule: storageX balance is wrong after event3");
    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 5, 3, 8.0,
          1200.0, 1),
        "rule: storageY balance is wrong after event3");
    balance = (do_QueryElementAt(set, 4));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, 643, 3240.0,
          3240.0, 1),
        "rule: storageX balance is wrong after event3");
    balance = (do_QueryElementAt(set, 5));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 7, 643, 6720.0,
          6720.0, 1),
        "rule: storageY balance is wrong after event3");
  }

  return NS_OK;
}
