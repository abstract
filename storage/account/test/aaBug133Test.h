/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABUG133TEST_H
#define AABUG133TEST_H 1

#include "nsCOMPtr.h"
#include "nsITest.h"

#define AA_BUG133_TEST_CID \
{0xefcaa261, 0x8273, 0x419d, {0xb0, 0xb0, 0xb2, 0xc7, 0x84, 0x24, 0xad, 0x34}}
#define AA_BUG133_TEST_CONTRACT "@aasii.org/storage/unit-bug133;1"

class nsITestRunner;
class aaISession;
class aaILoadQuery;
class aaIFact;

class aaBug133Test: public nsITest
{
public:
  aaBug133Test() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  virtual ~aaBug133Test() {;}
  class RAII
  {
    public:
      RAII(aaBug133Test *t);
      ~RAII();

      PRBool status;
    private:
      aaBug133Test* test;
      nsresult init();
  };

  //friend class RAII;
  nsCOMPtr<aaISession> mSession;

  nsresult testRounding(nsITestRunner *aTestRunner);
};

#endif /* AABUG133TEST_H */
