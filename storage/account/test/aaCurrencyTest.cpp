/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsArrayUtils.h"
#include "nsDirectoryServiceDefs.h"
#include "nsServiceManagerUtils.h"
#include "nsIProperties.h"
#include "nsIFile.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIQuote.h"
#include "aaIFact.h"
#include "aaIEvent.h"
#include "aaITransaction.h"
#include "aaBaseKeys.h"
#include "aaISqlRequest.h"
#include "aaISqlFilter.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaIBalance.h"
#include "aaAmountUtils.h"

#include "aaISession.h"
#include "aaSessionUtils.h"
#include "aaIManager.h"

#include "aaBaseTestUtils.h"
#include "aaCurrencyTest.h"

/* nsITest */
NS_IMETHODIMP
aaCurrencyTest::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED( res.status )) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_CURRENCY_TEST_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }
  testPurchase(aTestRunner);
  testRateChangeBeforeIncome(aTestRunner);
  testSaleAdvance(aTestRunner);
  testForexSale(aTestRunner);
  testRateChange(aTestRunner);
  testForexSaleAfterChange(aTestRunner);
  testTransferRollback(aTestRunner);
  return NS_OK;
}

NS_IMPL_ISUPPORTS1(aaCurrencyTest, nsITest)

/* Helpers */
aaCurrencyTest::RAII::RAII(aaCurrencyTest *t)
  :test(t)
{
  status = init();
}

nsresult
aaCurrencyTest::RAII::init()
{
  NS_ENSURE_ARG_POINTER(test);
  nsresult rv;

  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = manager->TmpFileFromString("currency.sqlite", PR_TRUE,
      getter_AddRefs(file));

  test->mSession = do_CreateInstance(AA_SESSION_CONTRACT, file, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mFlowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mResourceLoader = do_CreateInstance(AA_LOADRESOURCE_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mEntityLoader = do_CreateInstance(AA_LOADENTITY_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

aaCurrencyTest::RAII::~RAII()
{
  if (!test)
    return;
  test->mFlowLoader = nsnull;
  test->mResourceLoader = nsnull;
  test->mEntityLoader = nsnull;
  test->mBank = nsnull;
  test->mSession = nsnull;
  test = nsnull;
}

/* Private methods */
nsresult
aaCurrencyTest::testPurchase(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIMoney> cf, c1, c2;
  nsCOMPtr<aaIAsset> x, y;

  nsCOMPtr<aaIEntity> B, S1, S2, P1, P2, K;

  nsCOMPtr<aaIFlow> af, a1, a2, dx, dy, bx1, by2, sx1, sy2;

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  PRExplodedTime tm = {0,0,0,12,24,2,2008};


  /* Create resources and entities */
  {
    cf = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = cf->SetAlphaCode(NS_LITERAL_STRING("cf"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = cf->SetNumCode(1);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(cf, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    c1 = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c1->SetAlphaCode(NS_LITERAL_STRING("c1"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c1->SetNumCode(2);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(c1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    c2 = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c2->SetAlphaCode(NS_LITERAL_STRING("c2"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c2->SetNumCode(3);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(c2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    x = do_CreateInstance(AA_ASSET_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = x->SetTag(NS_LITERAL_STRING("x"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(x, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    y = do_CreateInstance(AA_ASSET_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = y->SetTag(NS_LITERAL_STRING("y"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(y, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    B = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = B->SetTag(NS_LITERAL_STRING("B"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(B, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    S1 = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = S1->SetTag(NS_LITERAL_STRING("S1"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(S1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    S2 = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = S2->SetTag(NS_LITERAL_STRING("S2"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(S2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    P1 = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = P1->SetTag(NS_LITERAL_STRING("P1"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(P1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    P2 = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = P2->SetTag(NS_LITERAL_STRING("P2"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(P2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    K = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = K->SetTag(NS_LITERAL_STRING("K"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(K, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mBank = B;
  /* Create accounts and init chart */
  {
    af = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    af->SetEntity(B);
    af->SetGiveResource(cf);
    af->SetTakeResource(cf);
    af->SetRate(1.0);
    af->SetTag(NS_LITERAL_STRING("af"));
    mSession->Save(af, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    a1 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    a1->SetEntity(B);
    a1->SetGiveResource(c1);
    a1->SetTakeResource(c1);
    a1->SetRate(1.0);
    a1->SetTag(NS_LITERAL_STRING("a1"));
    mSession->Save(a1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    a2 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    a2->SetEntity(B);
    a2->SetGiveResource(c2);
    a2->SetTakeResource(c2);
    a2->SetRate(1.0);
    a2->SetTag(NS_LITERAL_STRING("a2"));
    mSession->Save(a2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    dx = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    dx->SetEntity(K);
    dx->SetGiveResource(x);
    dx->SetTakeResource(x);
    dx->SetRate(1.0);
    dx->SetTag(NS_LITERAL_STRING("dx"));
    mSession->Save(dx, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    dy = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    dy->SetEntity(K);
    dy->SetGiveResource(y);
    dy->SetTakeResource(y);
    dy->SetRate(1.0);
    dy->SetTag(NS_LITERAL_STRING("dy"));
    mSession->Save(dy, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    bx1 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    bx1->SetEntity(S1);
    bx1->SetGiveResource(c1);
    bx1->SetTakeResource(x);
    bx1->SetRate(1.0/100.0);
    bx1->SetTag(NS_LITERAL_STRING("bx1"));
    mSession->Save(bx1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    by2 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    by2->SetEntity(S2);
    by2->SetGiveResource(cf);
    by2->SetTakeResource(y);
    by2->SetRate(1.0/200.0);
    by2->SetTag(NS_LITERAL_STRING("by2"));
    mSession->Save(by2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    sx1 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    sx1->SetEntity(P1);
    sx1->SetGiveResource(x);
    sx1->SetTakeResource(cf);
    sx1->SetRate(200.0);
    sx1->SetTag(NS_LITERAL_STRING("sx1"));
    mSession->Save(sx1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    sy2 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    sy2->SetEntity(P2);
    sy2->SetGiveResource(y);
    sy2->SetTakeResource(c2);
    sy2->SetRate(150.0);
    sy2->SetTag(NS_LITERAL_STRING("sy2"));
    mSession->Save(sy2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(cf);
    quote->SetRate(1.0);
    quote->SetTime(PR_ImplodeTime(&tm));
    mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Add a quote for c1, and accept delivery under bx1 */
  {
    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(c1);
    quote->SetRate(1.5);
    quote->SetTime(PR_ImplodeTime(&tm));
    rv = mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(bx1);
    fact->SetGiveTo(dx);
    fact->SetAmount(300.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

  }

  /* Test results */
  {
    rv = mSession->Load(mResourceLoader, getter_AddRefs(mResources));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIArray> set;
    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    nsCOMPtr<aaIBalance> balance;
    balance = (do_QueryElementAt(set, 1));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, 2, 30000.0,
          45000.0, 0),
        "[currency] flow 'bx1' is wrong");
    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 1, 300.0,
          45000.0, 1),
        "[currency] flow 'dx' is wrong");
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testRateChangeBeforeIncome(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIResource> c1 = do_QueryElementAt(mResources, 1);
  nsCOMPtr<aaIResource> c2 = do_QueryElementAt(mResources, 2);
  PRExplodedTime tm = {0,0,0,12,25,2,2008};
  nsCOMPtr<aaIBalance> balance;
  nsCOMPtr<aaISqlRequest> incomeLoader
    = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<nsIArray> incomeSet;
  nsCOMPtr<aaISqlRequest> quoteLoader
    = do_CreateInstance(AA_LOADQUOTE_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<nsIArray> quoteSet;
  {
    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(c1);
    quote->SetRate(1.6);
    quote->SetTime(PR_ImplodeTime(&tm));
    rv = mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_MSG(NS_FAILED(rv), "[quote save] date constrain");

    rv = mSession->Load(incomeLoader, getter_AddRefs(incomeSet));
    NS_TEST_ASSERT_MSG(incomeSet, "[rate change] income set not created" );
    NS_ENSURE_TRUE(incomeSet, rv);

    balance = do_QueryElementAt(incomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[rate change] income not loaded" );
    if (NS_LIKELY(balance)) {
      NS_TEST_ASSERT_MSG(balance->PickSide() == 1, 
          "[rate change] wrong income sign");
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() - 3000.0), 
          "[rate change] wrong income amount");
    }

    rv = quoteLoader->SetFilter(c1);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Load(quoteLoader, getter_AddRefs(quoteSet));
    NS_TEST_ASSERT_MSG(quoteSet, "[rate change] query instance creation" );
    NS_ENSURE_TRUE(quoteSet, rv);

    PRUint32 count;
    quoteSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[rate change] wrong quote count");

    nsCOMPtr<aaIQuote> quote = do_QueryElementAt(quoteSet, 0, &rv);
    NS_TEST_ASSERT_MSG(quote, "[rate change] quote not loaded" );
    if (quote) {
      NS_TEST_ASSERT_MSG(quote->PickResource() &&
          quote->PickResource()->PickId() == 2,
          "[rate change] wrong resource in the quote");
      NS_TEST_ASSERT_MSG(isZero(quote->PickDiff() + 3000.0),
          "[rate change] wrong difference in the quote");
    }
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testSaleAdvance(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlows));
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIResource> c2 = do_QueryElementAt(mResources, 2);

  nsCOMPtr<aaIFlow> a2 = do_QueryElementAt(mFlows, 2);
  nsCOMPtr<aaIFlow> sy2 = do_QueryElementAt(mFlows, 8);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  PRExplodedTime tm = {0,0,0,12,25,2,2008};


  /* Add a quote for c2, and accept advance payment under sy2 */
  {
    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(c2);
    quote->SetRate(2.0);
    quote->SetTime(PR_ImplodeTime(&tm));
    mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(sy2);
    fact->SetGiveTo(a2);
    fact->SetAmount(60000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Test results */
  {
    nsCOMPtr<nsIArray> set;
    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    nsCOMPtr<aaIBalance> balance;
    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 2, 400.0,
          120000.0, 0),
        "[currency] flow 'bx1' is wrong");
    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 3, 60000.0,
          120000.0, 1),
        "[currency] flow 'dx' is wrong");
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testForexSale(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIResource> cf = do_QueryElementAt(mResources, 0);
  nsCOMPtr<aaIResource> c2 = do_QueryElementAt(mResources, 2);

  nsCOMPtr<aaIFlow> f1, f2, f3;
  nsCOMPtr<aaIFlow> a2 = do_QueryElementAt(mFlows, 2);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  PRExplodedTime tm = {0,0,0,12,25,2,2008};
  nsCOMPtr<aaIBalance> balance;

  /* Pay for forex deal 1 */
  {
    f1 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    f1->SetEntity(mBank);
    f1->SetGiveResource(c2);
    f1->SetTakeResource(cf);
    f1->SetRate(2.1);
    f1->SetTag(NS_LITERAL_STRING("f1"));
    mSession->Save(f1, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(a2);
    fact->SetGiveTo(f1);
    fact->SetAmount(10000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIArray> set;
    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 2, 400.0,
          120000.0, 0),
        "[currency] flow 'sy2' is wrong");
    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 3, 50000.0,
          100000.0, 1),
        "[currency] flow 'a2' is wrong");
    balance = (do_QueryElementAt(set, 4));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, 1, 21000.0,
          21000.0, 1),
        "[currency] flow 'f1' is wrong");
  }

  /* Pay for forex deal 2 */
  {
    f2 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    f2->SetEntity(mBank);
    f2->SetGiveResource(c2);
    f2->SetTakeResource(cf);
    f2->SetRate(2.0);
    f2->SetTag(NS_LITERAL_STRING("f2"));
    mSession->Save(f2, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(a2);
    fact->SetGiveTo(f2);
    fact->SetAmount(10000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIArray> set;
    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 2, 400.0,
          120000.0, 0),
        "[currency] flow 'sy2' is wrong");
    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 3, 40000.0,
          80000.0, 1),
        "[currency] flow 'a2' is wrong");
    balance = (do_QueryElementAt(set, 5));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 11, 1, 20000.0,
          20000.0, 1),
        "[currency] flow 'f2' is wrong");
  }

  /* Pay for forex deal 3 */
  {
    f3 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    f3->SetEntity(mBank);
    f3->SetGiveResource(c2);
    f3->SetTakeResource(cf);
    f3->SetRate(1.95);
    f3->SetTag(NS_LITERAL_STRING("f3"));
    mSession->Save(f3, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(a2);
    fact->SetGiveTo(f3);
    fact->SetAmount(10000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIArray> set;
    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);

    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 3, 30000.0,
          60000.0, 1),
        "[currency] flow 'a2' is wrong");
    balance = (do_QueryElementAt(set, 6));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 12, 1, 19500.0,
          19500.0, 1),
        "[currency] flow 'f3' is wrong");
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testRateChange(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIResource> c1 = do_QueryElementAt(mResources, 1);
  nsCOMPtr<aaIResource> c2 = do_QueryElementAt(mResources, 2);
  PRExplodedTime tm = {0,0,0,12,31,2,2008};
  nsCOMPtr<aaIBalance> balance;
  nsCOMPtr<aaISqlRequest> incomeLoader
    = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<nsIArray> incomeSet;
  nsCOMPtr<aaISqlRequest> quoteLoader
    = do_CreateInstance(AA_LOADQUOTE_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<nsIArray> quoteSet;

  {
    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(c2);
    quote->SetRate(2.1);
    quote->SetTime(PR_ImplodeTime(&tm));
    mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(incomeLoader, getter_AddRefs(incomeSet));
    NS_TEST_ASSERT_MSG(incomeSet, "[rate change] income set not created" );
    NS_ENSURE_TRUE(incomeSet, rv);

    balance = do_QueryElementAt(incomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[rate change] income not loaded" );
    if (NS_LIKELY(balance)) {
      NS_TEST_ASSERT_MSG(balance->PickSide() == 0, 
          "[rate change] wrong income sign");
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() - 500.0), 
          "[rate change] wrong income amount");
    }

    rv = quoteLoader->SetFilter(c2);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Load(quoteLoader, getter_AddRefs(quoteSet));
    NS_TEST_ASSERT_MSG(quoteSet, "[rate change] query instance creation" );
    NS_ENSURE_TRUE(quoteSet, rv);

    PRUint32 count;
    quoteSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[rate change] wrong quote count");

    nsCOMPtr<aaIQuote> quote = do_QueryElementAt(quoteSet, 0, &rv);
    NS_TEST_ASSERT_MSG(quote, "[rate change] quote not loaded" );
    NS_ENSURE_TRUE(quote, NS_ERROR_FAILURE);
    NS_TEST_ASSERT_MSG(quote->PickResource() &&
        quote->PickResource()->PickId() == 3,
        "[rate change] wrong resource in the quote");
    NS_TEST_ASSERT_MSG(isZero(quote->PickDiff() - 3000.0),
        "[rate change] wrong difference in the quote");
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testForexSaleAfterChange(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIResource> c1 = do_QueryElementAt(mResources, 1);
  nsCOMPtr<aaIResource> c2 = do_QueryElementAt(mResources, 2);

  nsCOMPtr<aaIFlow> f4;
  nsCOMPtr<aaIFlow> a2 = do_QueryElementAt(mFlows, 2);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  PRExplodedTime tm = {0,0,0,12,31,2,2008};
  nsCOMPtr<aaIBalance> balance;

  nsCOMPtr<aaISqlRequest> factLoader
    = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<nsIArray> factSet;
  nsCOMPtr<aaISqlFilter> filter;
  /* Pay for forex deal 4 */
  {
    f4 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    f4->SetEntity(mBank);
    f4->SetGiveResource(c2);
    f4->SetTakeResource(c1);
    f4->SetRate(2.1/1.6);
    f4->SetTag(NS_LITERAL_STRING("f4"));
    mSession->Save(f4, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(a2);
    fact->SetGiveTo(f4);
    fact->SetAmount(10000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    nsCOMPtr<nsIArray> set;
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(set, rv);

    balance = (do_QueryElementAt(set, 3));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 2, 400.0,
          120000.0, 0),
        "[currency] flow 'sy2' is wrong");
    balance = (do_QueryElementAt(set, 0));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 3, 20000.0,
          42000.0, 1),
        "[currency] flow 'a2' is wrong");
    balance = (do_QueryElementAt(set, 7));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 13, 2, 13125.0,
          21000.0, 1),
        "[currency] flow 'f4' is wrong");

    filter = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(filter, rv);
    rv = filter->SetInterface(NS_GET_IID(aaIFact), fact);
    NS_TEST_ASSERT_OK(rv);

    rv = factLoader->SetFilter(filter);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Load(factLoader, getter_AddRefs(factSet));
    NS_TEST_ASSERT_MSG(factSet, "[rate change] query instance creation" );
    NS_ENSURE_TRUE(factSet, rv);

    PRUint32 count;
    factSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[change effect] wrong transaction count");

    txn = do_QueryElementAt(factSet, 0, &rv);
    NS_TEST_ASSERT_MSG(testTxn(aTestRunner, txn, 3, 13, 10000,
          21000, 1, 0.0), "[change effect] transaction is wrong");
  }

  return NS_OK;
}

nsresult
aaCurrencyTest::testTransferRollback(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  rv = mSession->Load(mEntityLoader, getter_AddRefs(mEntities));
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIMoney> c3;
  nsCOMPtr<aaIAsset> y;
  y = do_QueryElementAt(mResources, 4);

  nsCOMPtr<aaIEntity> S2;
  S2 = do_QueryElementAt(mEntities, 2);

  nsCOMPtr<aaIFlow> dy, by3;
  dy = do_QueryElementAt(mFlows, 4);

  nsCOMPtr<aaIQuote> quote;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  nsCOMPtr<aaITransaction> txn;
  PRExplodedTime tm = {0,0,0,12,14,3,2008};

  /* */
  {
    c3 = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c3->SetAlphaCode(NS_LITERAL_STRING("c3"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = c3->SetNumCode(4);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(c3, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    by3 = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    by3->SetEntity(S2);
    by3->SetGiveResource(c3);
    by3->SetTakeResource(y);
    by3->SetRate(1.0/200.0);
    by3->SetTag(NS_LITERAL_STRING("by3"));
    rv = mSession->Save(by3, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    quote = do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(c3);
    quote->SetRate(0.8);
    quote->SetTime(PR_ImplodeTime(&tm));
    rv = mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {  
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(by3);
    fact->SetGiveTo(dy);
    fact->SetAmount(100.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    tm.tm_mday = 11;
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<aaISqlRequest> loader
      = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    nsCOMPtr<aaISqlRequest> factLoader
      = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    nsCOMPtr<nsIArray> set, factSet;

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_MSG(set, "[rollback] creating loader" );
    NS_ENSURE_TRUE(set, rv);

    PRUint32 count;
    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 10, "[rollback] wrong flow count");

    rv = mSession->Load(factLoader, getter_AddRefs(factSet));
    NS_TEST_ASSERT_MSG(factSet, "[rollback] query instance creation" );
    NS_ENSURE_TRUE(factSet, rv);

    factSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[rollback] wrong fact count");

    rv = mSession->Save(event, event);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_MSG(set, "[rollback] creating loader" );
    NS_ENSURE_TRUE(set, rv);

    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 8, "[rollback] wrong flow count");

    rv = mSession->Load(factLoader, getter_AddRefs(factSet));
    NS_TEST_ASSERT_MSG(factSet, "[rollback] query instance creation" );
    NS_ENSURE_TRUE(factSet, rv);

    factSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[rollback] wrong fact count");
  }

  return NS_OK;
}
