/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsArrayUtils.h"
#include "nsDirectoryServiceDefs.h"
#include "nsServiceManagerUtils.h"
#include "nsIProperties.h"
#include "nsIFile.h"


/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "nsTestUtils.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIQuote.h"
#include "aaIFact.h"
#include "aaIEvent.h"
#include "aaITransaction.h"
#include "aaIBalance.h"
#include "aaBaseKeys.h"
#include "aaISession.h"
#include "aaSessionUtils.h"
#include "aaIManager.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"

#include "aaBaseTestUtils.h"
#include "aaBug133Test.h"

/* nsITest */
NS_IMETHODIMP
aaBug133Test::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED(res.status) || !mSession) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_BUG133_TEST_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }
  testRounding(aTestRunner);
  return NS_OK;
}

NS_IMPL_ISUPPORTS1(aaBug133Test, nsITest)

/* Helpers */
aaBug133Test::RAII::RAII(aaBug133Test *t)
  :status(PR_FALSE), test(t)
{
  status = init();
}

aaBug133Test::RAII::~RAII()
{
  if (!test)
    return;
  test->mSession = nsnull;
  test = nsnull;
}

/* Private methods */
nsresult
aaBug133Test::RAII::init()
{
  NS_ENSURE_ARG_POINTER(test);
  nsresult rv;

  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = manager->TmpFileFromString("bug133.sqlite", PR_TRUE,
      getter_AddRefs(file));

  nsCOMPtr<aaISession> session(do_CreateInstance(AA_SESSION_CONTRACT, file,
        &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  status = PR_TRUE;
  test->mSession = session;

  return NS_OK;
} 

nsresult
aaBug133Test::testRounding(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIMoney> x;
  nsCOMPtr<aaIAsset> y;

  nsCOMPtr<aaIEntity> K, L, M, N;

  nsCOMPtr<aaIFlow> a, b, c, d;

  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<nsIMutableArray> block;
  PRExplodedTime tm = {0,0,0,12,26,1,2008};

  nsCOMPtr<aaITransaction> txn;

  /* Create resources and entities */
  {
    x = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = x->SetAlphaCode(NS_LITERAL_STRING("x"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = x->SetNumCode(1);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(x, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    y = do_CreateInstance(AA_ASSET_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = y->SetTag(NS_LITERAL_STRING("y"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(y, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    K = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = K->SetTag(NS_LITERAL_STRING("K"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(K, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    L = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = L->SetTag(NS_LITERAL_STRING("L"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(L, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    M = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = M->SetTag(NS_LITERAL_STRING("M"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(M, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    N = do_CreateInstance("@aasii.org/base/entity;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = N->SetTag(NS_LITERAL_STRING("N"));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->Save(N, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Create accounts and init chart */
  {
    a = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    a->SetEntity(K);
    a->SetGiveResource(x);
    a->SetTakeResource(y);
    a->SetRate(1.0/100.0);
    a->SetTag(NS_LITERAL_STRING("a"));
    rv = mSession->Save(a, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    b = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    b->SetEntity(L);
    b->SetGiveResource(y);
    b->SetTakeResource(y);
    b->SetRate(1.0);
    b->SetTag(NS_LITERAL_STRING("b"));
    rv = mSession->Save(b, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    c = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    c->SetEntity(M);
    c->SetGiveResource(y);
    c->SetTakeResource(x);
    c->SetRate(150.0);
    c->SetTag(NS_LITERAL_STRING("c"));
    rv = mSession->Save(c, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    d = do_CreateInstance("@aasii.org/base/flow;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    d->SetEntity(N);
    d->SetGiveResource(x);
    d->SetTakeResource(x);
    d->SetRate(1.0);
    d->SetTag(NS_LITERAL_STRING("d"));
    rv = mSession->Save(d, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<aaIQuote> quote =
      do_CreateInstance("@aasii.org/base/quote;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    quote->SetResource(x);
    quote->SetRate(1.0);
    quote->SetTime(PR_ImplodeTime(&tm));
    rv = mSession->Save(quote, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Execute transfers and measurement */
  {
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(a);
    fact->SetGiveTo(b);
    fact->SetAmount(300.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(b);
    fact->SetGiveTo(c);
    fact->SetAmount(200.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    fact->SetTakeFrom(c);
    fact->SetGiveTo(d);
    fact->SetAmount(20000.0);
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    event->SetTime(PR_ImplodeTime(&tm));
    block = do_QueryInterface(event, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    block->AppendElement(fact, PR_FALSE);
    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    txn->SetFact(fact);
    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  /* Test error condition */
  {
    nsCOMPtr<aaISqlRequest> loader;
    nsCOMPtr<nsIArray> set;
    loader = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(set, rv);

    nsCOMPtr<aaIBalance> balance(do_QueryElementAt(set, 2));
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, 1, 10000.0,
          10000.0, 1),
        "[bug 133] flow 'c' is wrong");
  }

  return NS_OK;
}
