/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsIGenericFactory.h"

/* Unfrozen interfaces */
#include "nsIStringEnumerator.h"

/* Project includes */
#include "nsTestUtils.h"
#include "nsITestRunner.h"
#include "nsITest.h"

#include "aaAccountTest.h"
#include "aaBug133Test.h"
#include "aaCurrencyTest.h"
#include "aaTaxRulesTest.h"

#define AA_ACCOUNT_TEST_MODULE_CID \
{0x0eaf81ee, 0xa6bf, 0x4757, {0x8a, 0xa3, 0x5f, 0xbd, 0xc8, 0x7d, 0x0e, 0x3c}}
#define AA_ACCOUNT_TEST_MODULE_CONTRACT "@aasii.org/storage/unit-account;1"

class aaAccountTestModule: public nsITest,
                           public nsIUTF8StringEnumerator
{
public:
  aaAccountTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  virtual ~aaAccountTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaAccountTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

/* nsITest */
NS_IMETHODIMP
aaAccountTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  "@aasii.org/storage/unit-account-core;1"
  ,"@aasii.org/storage/unit-bug133;1"
  ,"@aasii.org/storage/unit-account-currency;1"
  ,"@aasii.org/storage/unit-tax-rules;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaAccountTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaAccountTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign( subtests[mSubtest++] );
  return NS_OK;
}

/* Boilerplate - factory & module */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaAccountTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaAccountTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBug133Test)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaCurrencyTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTaxRulesTest)

static const nsModuleComponentInfo kComponents[] =
{
  {
    "Account Module Test Container",
    AA_ACCOUNT_TEST_MODULE_CID,
    AA_ACCOUNT_TEST_MODULE_CONTRACT,
    aaAccountTestModuleConstructor
  }
  ,{
    "Account Core Unit Test",
    AA_ACCOUNT_TEST_CID,
    AA_ACCOUNT_TEST_CONTRACT,
    aaAccountTestConstructor
  }
  ,{
    "Bug133 Unit Test",
    AA_BUG133_TEST_CID,
    AA_BUG133_TEST_CONTRACT,
    aaBug133TestConstructor
  }
  ,{
    "Multiple Currency Unit Test",
    AA_CURRENCY_TEST_CID,
    AA_CURRENCY_TEST_CONTRACT,
    aaCurrencyTestConstructor
  }
  ,{
    "Tax Rules Unit Test",
    AA_TAX_RULES_TEST_CID,
    AA_TAX_RULES_TEST_CONTRACT,
    aaTaxRulesTestConstructor
  }
};
NS_IMPL_NSGETMODULE(aaAccountTest, kComponents)
