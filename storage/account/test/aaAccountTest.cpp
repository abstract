/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"
#include "nsArrayUtils.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsIFile.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "nsStringUtils.h"
#include "aaAmountUtils.h"
#include "aaITimeFrame.h"
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIEntity.h"
#include "aaIFact.h"
#include "aaIFlow.h"
#include "aaIEvent.h"
#include "aaIQuote.h"
#include "aaITransaction.h"
#include "aaIBalance.h"
#include "aaBaseKeys.h"
#include "aaIChart.h"
#include "aaISession.h"
#include "aaIManager.h"
#include "aaSessionUtils.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaISqlFilter.h"
#include "aaISqlRequest.h"
#include "aaSaveKeys.h"
#include "aaSaveTransaction.h"
/* XXX _acc This is a temporary hack to acquire connection
 * It has to be replaced with a 'chart'
 */
#include "aaSession.h"
#include "aaTestConsts.h"

#include "aaBaseTestUtils.h"
#include "aaAccountTest.h"

/* nsITest */
NS_IMETHODIMP
aaAccountTest::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED(res.status) || !mSession) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_ACCOUNT_TEST_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }
  testEmptyBalance(aTestRunner);
  testPendingFacts(aTestRunner);
  testChart(aTestRunner);
  testTransaction(aTestRunner);
  testUpdateTransaction(aTestRunner);
  testReplaceTransaction(aTestRunner);
  testStateFilter(aTestRunner);
  testCreditTransaction(aTestRunner);
  testDeleteTransaction(aTestRunner);
  testFlowChart(aTestRunner);
  testLoadIncome(aTestRunner);
  testStopTransaction(aTestRunner);
  testLossTransaction(aTestRunner);
  testSplitTransaction(aTestRunner);
  testGainTransaction(aTestRunner);
  testDirectGainsLosses(aTestRunner);
  return NS_OK;
}

NS_IMPL_ISUPPORTS1(aaAccountTest, nsITest)

/* Helpers */
aaAccountTest::RAII::RAII(aaAccountTest *t)
  :test(t)
{
  status = init();
}

nsresult
aaAccountTest::RAII::init()
{
  NS_ENSURE_ARG_POINTER(test);
  nsresult rv;
  
  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = manager->TmpFileFromString("storage.sqlite", PR_FALSE,
      getter_AddRefs(file));

  test->mSession = do_CreateInstance(AA_SESSION_CONTRACT, file, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mFlowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = test->mSession->Load(test->mFlowLoader, getter_AddRefs(test->mFlows));
  NS_ENSURE_SUCCESS(rv, rv);

  test->mResourceLoader = do_CreateInstance(AA_LOADRESOURCE_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = test->mSession->Load(test->mResourceLoader,
      getter_AddRefs(test->mResources));
  NS_ENSURE_SUCCESS(rv, rv);

  test->mEntityLoader = do_CreateInstance(AA_LOADENTITY_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = test->mSession->Load(test->mEntityLoader,
      getter_AddRefs(test->mEntities));
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

aaAccountTest::RAII::~RAII()
{
  if (!test)
    return;
  test->mPendingFact = nsnull;
  test->mResources = nsnull;
  test->mFlows = nsnull;
  test->mEntities = nsnull;
  test->mSession = nsnull;
  test = nsnull;
}

nsresult
aaAccountTest::loadBalanceSet()
{
  mBalanceSet = nsnull;
  NS_ENSURE_TRUE(mSession, NS_ERROR_NOT_INITIALIZED);
  return mSession->Load(mBalanceLoader, getter_AddRefs(mBalanceSet));
}

nsresult
aaAccountTest::loadIncomeSet()
{
  mIncomeSet = nsnull;
  NS_ENSURE_TRUE(mSession, NS_ERROR_NOT_INITIALIZED);
  return mSession->Load(mIncomeLoader, getter_AddRefs(mIncomeSet));
}

nsresult
aaAccountTest::loadPendingFact(nsITestRunner *cxxUnitTestRunner)
{
  nsresult rv;
  mPendingFact = nsnull;
  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mPendingLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(set, "[pending facts] result set not loaded" );
  NS_ENSURE_TRUE(set, rv);

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == mPendingCount, "[pending facts] wrong fact count");
  NS_ENSURE_TRUE(count == mPendingCount, NS_ERROR_UNEXPECTED);

  if (!count)
    return NS_OK;

  mPendingFact = do_QueryElementAt(set, 0, &rv);
  return rv;
}

nsresult
aaAccountTest::loadLastTxn()
{
  nsresult rv;
  mTxnSet = nsnull;
  if (!mTxnLoader) {
    mTxnLoader = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_ENSURE_TRUE(mTxnLoader, rv);
  }

  nsCOMPtr<aaISqlFilter> filter
    = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = filter->SetInterface(NS_GET_IID(aaIFact), mPendingFact);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = mTxnLoader->SetFilter(filter);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mSession->Load(mTxnLoader, getter_AddRefs(mTxnSet));
  NS_ENSURE_TRUE(mTxnSet, rv);

  PRUint32 count;
  mTxnSet->GetLength(&count);
  if (count != 1)
    return NS_ERROR_UNEXPECTED;

  return NS_OK;
}

/* Private methods */
nsresult
aaAccountTest::testEmptyBalance(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  aaSession *session = static_cast<aaSession *>(mSession.get());
  NS_TEST_ASSERT_MSG(session, "[empty balance] 'session' cast" );
  NS_ENSURE_TRUE(session, NS_ERROR_UNEXPECTED);

  mBalanceLoader  = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mBalanceLoader, "[empty balance] query instance creation");
  NS_ENSURE_TRUE(mBalanceLoader, rv);

  rv = loadBalanceSet();
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[empty balance] loading");
  NS_TEST_ASSERT_MSG(mBalanceSet, "[empty balance] result set not loaded" );
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 0, "[empty balance] wrong flow count");

  return NS_OK;
}

nsresult
aaAccountTest::testPendingFacts(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  mPendingLoader = do_CreateInstance(AA_LOADPENDINGFACTS_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mPendingLoader, "[pending facts] instance creation");
  NS_ENSURE_TRUE(mPendingLoader, rv);

  mPendingCount = AA_TXN_COUNT;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);

  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 2, 3,
        AA_EVENT_AMOUNT_2), "[pending facts] 1st fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testChart(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  nsCOMPtr<aaIChart> node = 
    do_CreateInstance("@aasii.org/accounting/chart;1", &rv);
  NS_TEST_ASSERT_MSG(node, "[chart] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  nsCOMPtr<aaIMoney> rub = do_QueryElementAt(mResources, 0, &rv);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(rub, rv);
  node->SetCurrency( rub );

  rv = mSession->Save( node, nsnull );
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[chart] saving");

  aaSession *session = static_cast<aaSession *>(mSession.get());
  NS_TEST_ASSERT_MSG(session, "[chart] 'session' cast" );
  NS_ENSURE_TRUE(session, NS_ERROR_UNEXPECTED);

  nsCOMPtr<nsIArray> set;
  nsCOMPtr<aaISqlRequest> loader 
    = do_CreateInstance(AA_LOADCHART_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[chart] query instance creation" );
  NS_ENSURE_TRUE(loader, rv);

  rv = mSession->Load(loader, getter_AddRefs(set));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(set, "[chart] result set not loaded" );
  NS_ENSURE_TRUE(set, rv);

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 1, "[chart] wrong chart count");

  nsCOMPtr<aaIChart> chart = do_QueryElementAt(set, 0, &rv);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[chart] quering 1st chart");
  NS_TEST_ASSERT_MSG(chart, "[chart] 1st chart not loaded" );
  NS_ENSURE_TRUE(chart, NS_ERROR_FAILURE);

  nsCOMPtr<aaIMoney> obj;
  PRInt64 objId;

  chart->GetCurrency(getter_AddRefs( obj ));
  if (NS_LIKELY( obj )) {
    obj->GetId( &objId );
    NS_TEST_ASSERT_MSG(objId == AA_MONEY_CODE_1, "[chart] wrong resource id");
  } else {
    NS_TEST_ASSERT_MSG(obj, "[chart] resource is null");
  }

  return NS_OK;
}

nsresult
aaAccountTest::testTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaISqlTransaction> saver
    = do_CreateInstance(AA_SAVETRANSACTION_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(saver, "[transaction] query instance creation" );
  NS_ENSURE_TRUE(saver, rv);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[transaction] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[transaction] saving" );

  nsCOMPtr<aaIBalance> balance;
  rv = node->GetBalanceFrom(getter_AddRefs(balance));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[transaction] flow2 is wrong");

  rv = node->GetBalanceTo(getter_AddRefs(balance));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2, AA_EVENT_AMOUNT_2, 1),
      "[transaction] flow3 is wrong");

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 2, "[transaction] wrong flow count");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[transaction] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 1);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2, AA_EVENT_AMOUNT_2, 1),
      "[transaction] flow3 is wrong");

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 1, 3,
        AA_EVENT_AMOUNT_3), "[transaction] pending fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testUpdateTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[update txn] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[update txn] saving" );

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 3, "[update txn] wrong flow count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 1));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[update txn] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3, 1),
      "[update txn] flow3 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[update txn] flow1 is wrong");

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 3, 4,
        AA_EVENT_AMOUNT_4), "[update txn] pending fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testReplaceTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[replace txn] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[replace txn] saving" );

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 4, "[replace txn] wrong flow count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 1));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[replace txn] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[replace txn] flow1 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4, 1),
      "[replace txn] flow3 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 3);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[replace txn] flow4 is wrong");

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 5, 6,
        AA_EVENT_AMOUNT_6), "[replace txn] pending fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testStateFilter(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  NS_TEST_ASSERT_MSG(mBalanceLoader, "[sql filter] query instance not created" );
  NS_ENSURE_TRUE(mBalanceLoader, NS_ERROR_NOT_INITIALIZED);

  nsCOMPtr<aaISqlFilter> filter
    = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(filter, "[sql filter] sql filter instance" );
  NS_ENSURE_TRUE(filter, rv);

  nsCOMPtr<aaIFlow> bank = do_QueryElementAt(mFlows, 2);
  rv = filter->SetInterface(NS_GET_IID(aaIFlow), bank);
  NS_TEST_ASSERT_OK(rv);

  /* Set date to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  nsCOMPtr<aaITimeFrame> frame
    = do_CreateInstance(AA_TIMEFRAME_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  rv = frame->SetEnd(PR_ImplodeTime(&tm));
  NS_TEST_ASSERT_OK(rv);
  rv = filter->SetInterface(NS_GET_IID(aaITimeFrame), frame);
  NS_TEST_ASSERT_OK(rv);

  rv = mBalanceLoader->SetFilter(filter);
  NS_TEST_ASSERT_OK(rv);

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  mBalanceLoader->SetFilter(nsnull);

  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 1, "[sql filter] wrong count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 0));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3, 1),
      "[sql filter] flow3 is wrong");

  return NS_OK;
}

nsresult
aaAccountTest::testCreditTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[credit txn] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[credit txn] saving" );

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 6, "[credit txn] wrong flow count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 1));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[credit txn] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[credit txn] flow1 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4, 1),
      "[credit txn] flow3 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 3);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[credit txn] flow4 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 4);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 5, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 0),
      "[credit txn] flow5 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 5);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
        AA_EVENT_AMOUNT_6, AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[credit txn] flow6 is wrong");

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 3, 5,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
      "[transaction] pending fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testDeleteTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[delete txn] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete txn] saving" );

  rv = loadLastTxn();
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<aaITransaction> txn = do_QueryElementAt(mTxnSet, 0);
  NS_TEST_ASSERT_MSG(txn, "[delete txn] transaction not loaded");
  if (NS_LIKELY(txn)) {
    NS_TEST_ASSERT_MSG(testFact(aTestRunner, txn->PickFact(), 3, 5,
          AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
        "[delete txn] new fact is wrong");
    NS_TEST_ASSERT_MSG(txn->PickValue() == AA_EVENT_AMOUNT_6
        * AA_EVENT_RATE_2, "[delete txn] wrong value stored");
  }

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  count = 0;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 5, "[delete txn] wrong flow count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 1));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[delete txn] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[delete txn] flow1 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, AA_EVENT_AMOUNT_2
        + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[delete txn] flow3 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 3);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[delete txn] flow4 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 4);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
        AA_EVENT_AMOUNT_6, AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[delete txn] flow6 is wrong");

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testFact(aTestRunner, mPendingFact, 6, 7,
        AA_EVENT_AMOUNT_6),
      "[transaction] pending fact is wrong");
  return NS_OK;
}

nsresult
aaAccountTest::testFlowChart(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<nsIArray> list;
  nsCOMPtr<aaISqlRequest> listLoader
    = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(listLoader, "[flow chart] loader not created" );
  NS_ENSURE_TRUE(listLoader, rv);
  rv = mSession->Load(listLoader, getter_AddRefs(list));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[flow chart] list not loaded" );
  NS_ENSURE_TRUE(list, rv);

  nsCOMPtr<aaILoadObserver> listM = do_QueryInterface(list, &rv);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(listM, rv);
  nsCOMPtr<aaIFlow> pnl = do_CreateInstance(AA_INCOMEFLOW_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  listM->ObserveLoad(pnl);

  PRUint32 count = 0;
  rv = list->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIFlow> flow = do_QueryElementAt(list, count - 1, &rv);
  NS_TEST_ASSERT_MSG(flow, "[flow chart] tested flow not loaded");
  NS_ENSURE_TRUE(flow, rv);

  nsAutoString wstr;
  rv = flow->GetTag(wstr);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! flow->PickId(),
      "[flow chart] wrong flow tag");
  return NS_OK;
}

nsresult
aaAccountTest::testLoadIncome(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  mIncomeLoader = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mIncomeLoader, "[load income] loader not created" );

  rv = loadIncomeSet();
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load income] results not loaded" );
  NS_ENSURE_TRUE(mIncomeSet, rv);

  PRUint32 count = 0;
  rv = mIncomeSet->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 0, "[load income] wrong line count");

  return NS_OK;
}

nsresult
aaAccountTest::testStopTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaITransaction> node(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_TEST_ASSERT_MSG(node, "[stop txn] instance not created" );
  NS_ENSURE_TRUE(node, rv);

  node->SetFact(mPendingFact);

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[stop txn] saving" );

  rv = loadLastTxn();
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<aaITransaction> txn = do_QueryElementAt(mTxnSet, 0);
  NS_TEST_ASSERT_MSG(txn, "[stop txn] transaction not loaded");
  if (NS_LIKELY(txn)) {
    NS_TEST_ASSERT_MSG(testFact(aTestRunner, txn->PickFact(), 6, 7,
          AA_EVENT_AMOUNT_6),
        "[stop txn] new fact is wrong");
    NS_TEST_ASSERT_MSG(txn->PickValue() == AA_EVENT_AMOUNT_6
        * AA_EVENT_RATE_2, "[stop txn] wrong value stored");
    NS_TEST_ASSERT_MSG(txn->PickHasEarnings(),
        "[stop txn] wrong earnings status");
    NS_TEST_ASSERT_MSG(isZero(txn->PickEarnings() - AA_EVENT_AMOUNT_6
        * (AA_EVENT_RATE_3 - AA_EVENT_RATE_2)),
        "[stop txn] wrong earnings stored");
  }

  rv = loadBalanceSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mBalanceSet, rv);

  PRUint32 count;
  count = 0;
  mBalanceSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 5, "[stop txn] wrong flow count");

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mBalanceSet, 1));
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[stop txn] flow2 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[stop txn] flow1 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, AA_EVENT_AMOUNT_2
        + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[stop txn] flow3 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 3);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[stop txn] flow4 is wrong");

  balance = do_QueryElementAt(mBalanceSet, 4);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 7, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_3,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_3, 1),
      "[stop txn] flow7 is wrong");

  rv = loadIncomeSet();
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mIncomeSet, rv);

  count = 0;
  mIncomeSet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 1, "[stop txn] wrong income count");

  mProfit = 0.0;
  balance = do_QueryElementAt(mIncomeSet, 0);
  NS_TEST_ASSERT_MSG(balance, "[stop txn] income not loaded" );
  if (NS_LIKELY(balance)) {
    mProfit = AA_EVENT_AMOUNT_6 * (AA_EVENT_RATE_3 - AA_EVENT_RATE_2);
    NS_TEST_ASSERT_MSG(isZero(balance->PickValue() - mProfit), 
      "[stop txn] wrong income saved");
  }

  mPendingCount--;
  rv = loadPendingFact(aTestRunner);
  NS_TEST_ASSERT_OK(rv);
  return NS_OK;
}

nsresult
aaAccountTest::testLossTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  nsCOMPtr<aaIEntity> bank = do_QueryElementAt(mEntities, 3, &rv);
  NS_ENSURE_TRUE(bank, rv);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResources, 0, &rv));
  nsCOMPtr<aaIResource> eur(do_QueryElementAt(mResources, 3, &rv));

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlows,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlows,5));

  nsCOMPtr<aaIFlow> forex(do_QueryElementAt(mFlows, 6));
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1", &rv));
  nsCOMPtr<aaIEvent> event(do_CreateInstance("@aasii.org/base/event;1"));
  nsCOMPtr<aaITransaction> txn(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));

  PRUint32 count = 0;
  PRInt64 id = 0;
  nsCOMPtr<aaISqlRequest> loader;
  nsCOMPtr<nsIArray> set;
  nsCOMPtr<aaIBalance> balance;
  /* Settle forex deal #2 */
  {
    /* Set time to 2007-09-03 */
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    PRExplodedTime tm = {0,0,0,12,3,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 7 */
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_3);
    fact->SetTakeFrom(forex);
    fact->SetGiveTo(bankAccRub);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 4, "[loss txn] [fact 7] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[loss txn] [fact 7] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[loss txn] [fact 7] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[loss txn] [fact 7] flow4 is wrong");

    mRubs = AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
      + AA_EVENT_AMOUNT_6 * (AA_EVENT_RATE_3 - AA_EVENT_RATE_2);
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[loss txn] [fact 7] flow3 is wrong");
  }

  /* Create flow for forex deal #3 */
  {
    forex = do_CreateInstance("@aasii.org/base/flow;1");
    NS_ENSURE_TRUE(forex, NS_ERROR_FAILURE);
    forex->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_10));
    forex->SetEntity(bank);
    forex->SetGiveResource(rub);
    forex->SetTakeResource(eur);
    forex->SetRate(1 / AA_EVENT_RATE_4);
    forex->SetLimit(AA_EVENT_AMOUNT_7);
    rv = mSession->Save(forex, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[loss event] [deal 3] saving flow");
    NS_ENSURE_SUCCESS(rv, rv);

    forex->GetId( &id );
    NS_TEST_ASSERT_MSG(id == 8, "[loss event] [deal 3] unexpected 'flow.id'");
    NS_ENSURE_TRUE(id == 8, NS_ERROR_UNEXPECTED);
  }

  /* Pay forex deal #3 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-03 */
    PRExplodedTime tm = {0,0,0,12,3,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 8 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_7 * AA_EVENT_RATE_4);
    fact->SetTakeFrom(bankAccRub);
    fact->SetGiveTo(forex);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 5, "[loss txn] [fact 8] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[loss txn] [fact 8] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[loss txn] [fact 8] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[loss txn] [fact 8] flow4 is wrong");

    mRubs -= AA_EVENT_AMOUNT_7 * AA_EVENT_RATE_4;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[loss txn] [fact 8] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 8, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_7, AA_EVENT_AMOUNT_7 * AA_EVENT_RATE_4, 1),
        "[loss txn] [fact 8] flow8 is wrong");
  }

  /* Recieve settlement from forex deal #3 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-04 */
    PRExplodedTime tm = {0,0,0,12,4,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 9 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_7);
    fact->SetTakeFrom(forex);
    fact->SetGiveTo(bankAccEur);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 5, "[loss txn] [fact 9] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[loss txn] [fact 9] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[loss txn] [fact 9] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[loss txn] [fact 9] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[loss txn] [fact 9] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_7, AA_EVENT_AMOUNT_7 * AA_EVENT_RATE_4, 1),
        "[loss txn] [fact 9] flow8 is wrong");
  }

  nsCOMPtr<aaIFlow> office = do_CreateInstance("@aasii.org/base/flow;1");
  nsCOMPtr<aaIEntity> landlord = do_CreateInstance("@aasii.org/base/entity;1");
  nsCOMPtr<aaIAsset> rent = do_CreateInstance(AA_ASSET_CONTRACT);
  /* Create flow for office rent */
  {
    NS_ENSURE_TRUE(office, NS_ERROR_FAILURE);
    NS_ENSURE_TRUE(landlord, NS_ERROR_FAILURE);
    NS_ENSURE_TRUE(rent, NS_ERROR_FAILURE);
    landlord->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_6));
    rv = mSession->Save(landlord, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rent->SetTag(NS_LITERAL_STRING(AA_RESOURCE_TAG_5));
    rv = mSession->Save(rent, nsnull);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    office->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_11));
    office->SetEntity(landlord);
    office->SetGiveResource(rub);
    office->SetTakeResource(rent);
    office->SetRate(1 / AA_EVENT_RATE_6);
    office->SetLimit(12);
    rv = mSession->Save(office, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[loss event] [office] saving flow");
    NS_ENSURE_SUCCESS(rv, rv);

    office->GetId( &id );
    NS_TEST_ASSERT_MSG(id == 9, "[loss event] [office] unexpected 'flow.id'");
    NS_ENSURE_TRUE(id == 9, NS_ERROR_UNEXPECTED);
  }

  nsCOMPtr<aaIFlow> finres = do_CreateInstance("@aasii.org/base/income-flow;1");
  nsCOMPtr<aaIState> state;
  /* Accrue office rent for august */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-08-31 */
    PRExplodedTime tm = {0,0,0,12,31,7,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 10 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(1);
    fact->SetTakeFrom(office);
    fact->SetGiveTo(finres);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    loader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[loss event] wrong flow count");

    state = do_QueryElementAt(set, 2, &rv);
    NS_TEST_ASSERT_MSG(state, "[loss event] 3nd item not read" );
    NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
          mRubs), "[loss event] flow3 is wrong");

    state = do_QueryElementAt(set, 5, &rv);
    NS_TEST_ASSERT_MSG(state, "[loss event] 5th item not read" );
    NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6), "[loss event] flow7 is wrong");
  }

  /* Reflect office rent for august */
  {
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[loss txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[loss txn] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[loss txn] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[loss txn] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[loss txn] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[loss txn] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_7, AA_EVENT_AMOUNT_7 * AA_EVENT_RATE_4, 1),
        "[loss txn] flow8 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6, AA_EVENT_RATE_6, 0),
        "[loss txn] flow9 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[loss txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[loss txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      mProfit -= AA_EVENT_RATE_6;
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() + mProfit),
          "[loss txn] wrong income saved");
    }
  }

  return NS_OK;
}

nsresult
aaAccountTest::testSplitTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  nsCOMPtr<aaIEntity> bank = do_QueryElementAt(mEntities, 3, &rv);
  NS_ENSURE_TRUE(bank, rv);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResources, 0, &rv));
  nsCOMPtr<aaIResource> eur(do_QueryElementAt(mResources, 3, &rv));

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlows,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlows,5));

  nsCOMPtr<aaIFlow> forex;
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1", &rv));
  nsCOMPtr<aaIEvent> event(do_CreateInstance("@aasii.org/base/event;1"));
  nsCOMPtr<aaITransaction> txn(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));

  PRUint32 count = 0;
  PRInt64 id = 0;
  nsCOMPtr<aaISqlRequest> loader;
  nsCOMPtr<nsIArray> set;
  nsCOMPtr<aaIBalance> balance;
  /* Create flow for forex deal #4 */
  {
    forex = do_CreateInstance("@aasii.org/base/flow;1");
    NS_ENSURE_TRUE(forex, NS_ERROR_FAILURE);
    forex->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_12));
    forex->SetEntity(bank);
    forex->SetGiveResource(eur);
    forex->SetTakeResource(rub);
    forex->SetRate(AA_EVENT_RATE_5);
    forex->SetLimit(AA_EVENT_AMOUNT_8);
    rv = mSession->Save(forex, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[split event] [deal 4] saving flow");
    NS_ENSURE_SUCCESS(rv, rv);

    forex->GetId( &id );
    NS_TEST_ASSERT_MSG(id == 10, "[split event] [deal 4] unexpected 'flow.id'");
    NS_ENSURE_TRUE(id == 10, NS_ERROR_UNEXPECTED);
  }

  nsCOMPtr<aaIState> state;
  /* Partially pay forex deal #4 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-05 */
    PRExplodedTime tm = {0,0,0,12,5,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 11 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_9);
    fact->SetTakeFrom(bankAccEur);
    fact->SetGiveTo(forex);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[split txn] [fact 11] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[split txn] [fact 11] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[split txn] [fact 11] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[split txn] [fact 11] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[split txn] [fact 11] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6, AA_EVENT_RATE_6, 0),
        "[loss txn] flow9 is wrong");

    mEuros = AA_EVENT_AMOUNT_7 - AA_EVENT_AMOUNT_9;
    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[split txn] [fact 11] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 6);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, AA_MONEY_CODE_1,
          AA_EVENT_AMOUNT_9 * AA_EVENT_RATE_5,
          AA_EVENT_AMOUNT_9 * AA_EVENT_RATE_5, 1),
        "[split txn] [fact 11] flow10 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[split txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[split txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      mProfit += (AA_EVENT_RATE_5 - AA_EVENT_RATE_4) * AA_EVENT_AMOUNT_9;
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() + mProfit),
          "[split txn] wrong income saved");
    }
  }

  /* Partially receive from forex deal #4 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-05 */
    PRExplodedTime tm = {0,0,0,12,5,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 12 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_10 * AA_EVENT_RATE_5);
    fact->SetTakeFrom(forex);
    fact->SetGiveTo(bankAccRub);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    loader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[split event] wrong flow count");

    state = do_QueryElementAt(set, 6, &rv);
    NS_TEST_ASSERT_MSG(state, "[split event] 5th item not read" );
    NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 10, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_10 - AA_EVENT_AMOUNT_9),
        "[split event] flow7 is wrong");
  }

  /* Reflect partial revenue from forex deal #4 */
  {
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[split txn] [fact 11] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[split txn] [fact 11] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[split txn] [fact 11] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[split txn] [fact 11] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6, AA_EVENT_RATE_6, 0),
        "[split txn] flow9 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[split txn] [fact 11] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 6);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, AA_MONEY_CODE_2,
          (AA_EVENT_AMOUNT_10 - AA_EVENT_AMOUNT_9),
          (AA_EVENT_AMOUNT_10 - AA_EVENT_AMOUNT_9) * AA_EVENT_RATE_5, 0),
        "[split txn] [fact 11] flow10 is wrong");

    mRubs += AA_EVENT_AMOUNT_10 * AA_EVENT_RATE_5;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[split txn] [fact 11] flow3 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[split txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[split txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() + mProfit),
          "[split txn] wrong income saved");
    }
  }

  /* Fully pay forex deal #4 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-05 */
    PRExplodedTime tm = {0,0,0,12,5,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 13 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_11);
    fact->SetTakeFrom(bankAccEur);
    fact->SetGiveTo(forex);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    loader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[split event] wrong flow count");

    state = do_QueryElementAt(set, 6, &rv);
    NS_TEST_ASSERT_MSG(state, "[split event] 5th item not read" );
    NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 10, AA_MONEY_CODE_1,
          AA_EVENT_AMOUNT_12 * AA_EVENT_RATE_5),
        "[split event] flow7 is wrong");
  }

  /* Reflect full payment of deal #4 */
  {
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[split txn] [fact 13] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[split txn] [fact 13] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[split txn] [fact 13] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[split txn] [fact 13] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6, AA_EVENT_RATE_6, 0),
        "[split txn] flow9 is wrong");

    mEuros -= AA_EVENT_AMOUNT_11;
    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[split txn] [fact 13] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 6);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, AA_MONEY_CODE_1,
          AA_EVENT_AMOUNT_12 * AA_EVENT_RATE_5,
          AA_EVENT_AMOUNT_12 * AA_EVENT_RATE_5, 1),
        "[split txn] [fact 13] flow10 is wrong");
    NS_TEST_ASSERT_MSG(balance && balance->PickAmount() == 3495.0,
        "[split txn] [fact 13] inprecise calculation");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[split txn] [fact 13] flow3 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 0, "[split txn] wrong income count");
    mProfit += (AA_EVENT_RATE_5 - AA_EVENT_RATE_4) * AA_EVENT_AMOUNT_11;

  }

  return NS_OK;
}

nsresult
aaAccountTest::testGainTransaction(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  nsCOMPtr<aaIEntity> bank = do_QueryElementAt(mEntities, 3, &rv);
  NS_ENSURE_TRUE(bank, rv);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResources, 0, &rv));
  nsCOMPtr<aaIResource> eur(do_QueryElementAt(mResources, 3, &rv));

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlows));
  NS_TEST_ASSERT_OK(rv);
  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlows,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlows,5));
  nsCOMPtr<aaIFlow> forex(do_QueryElementAt(mFlows,9));
  nsCOMPtr<aaIFlow> office(do_QueryElementAt(mFlows,8));

  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1", &rv));
  nsCOMPtr<aaIEvent> event(do_CreateInstance("@aasii.org/base/event;1"));
  nsCOMPtr<aaITransaction> txn(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));

  PRUint32 count = 0;
  nsCOMPtr<aaISqlRequest> loader;
  nsCOMPtr<nsIArray> set;
  nsCOMPtr<aaIBalance> balance;

  /* Recieve settlement from forex deal #4 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-05 */
    PRExplodedTime tm = {0,0,0,12,5,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 14 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_12 * AA_EVENT_RATE_5);
    fact->SetTakeFrom(forex);
    fact->SetGiveTo(bankAccRub);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[gain txn] [fact 14] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[gain txn] [fact 14] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[gain txn] [fact 14] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[gain txn] [fact 14] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, AA_MONEY_CODE_1,
          AA_EVENT_RATE_6, AA_EVENT_RATE_6, 0),
        "[gain txn] flow9 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[gain txn] [fact 14] flow6 is wrong");

    mRubs += AA_EVENT_AMOUNT_12 * AA_EVENT_RATE_5;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[gain txn] [fact 14] flow3 is wrong");
  }

  nsCOMPtr<aaIState> state;
  /* Settle office rent for Aug and prepay for Sep*/
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-05 */
    PRExplodedTime tm = {0,0,0,12,5,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 15 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(2 * AA_EVENT_RATE_6);
    fact->SetTakeFrom(bankAccRub);
    fact->SetGiveTo(office);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    loader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(loader, getter_AddRefs(set));
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);

    set->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[gain event] wrong flow count");

    state = do_QueryElementAt(set, 5, &rv);
    NS_TEST_ASSERT_MSG(state, "[gain event] 5th item not read" );
    NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

    NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 9, 3, 1),
        "[gain event] flow7 is wrong");
  }

  /* Reflect payment for office */
  {
    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[gain txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[gain txn] [fact 15] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[gain txn] [fact 15] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[gain txn] [fact 15] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[gain txn] [fact 15] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[gain txn] [fact 15] flow6 is wrong");

    mRubs -= 2 * AA_EVENT_RATE_6;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[gain txn] [fact 15] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[gain txn] flow9 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 0, "[gain txn] wrong income count");
  }

  nsCOMPtr<aaIFlow> finres = do_CreateInstance("@aasii.org/base/income-flow;1");
  /* Direct monetary loss */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-06 */
    PRExplodedTime tm = {0,0,0,12,6,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 16 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_13);
    fact->SetTakeFrom(bankAccRub);
    fact->SetGiveTo(finres);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[gain txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[gain txn] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[gain txn] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[gain txn] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[gain txn] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[gain txn] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[gain txn] flow9 is wrong");

    mRubs -= AA_EVENT_AMOUNT_13;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[gain txn] flow3 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[gain txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[gain txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      mProfit -= AA_EVENT_AMOUNT_13;
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() + mProfit),
          "[gain txn] wrong income saved");
    }
  }

  /* Direct monetary gain */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-07 */
    PRExplodedTime tm = {0,0,0,12,7,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 17 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_14);
    fact->SetTakeFrom(finres);
    fact->SetGiveTo(bankAccRub);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[gain txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[gain txn] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[gain txn] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[gain txn] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[gain txn] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[gain txn] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[gain txn] flow9 is wrong");

    mRubs += AA_EVENT_AMOUNT_14;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[gain txn] flow3 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 0, "[gain txn] wrong income count");
    mProfit = 0.0;
  }
  return NS_OK;
}

nsresult
aaAccountTest::testDirectGainsLosses(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResources, 0, &rv));
  nsCOMPtr<aaIResource> eur(do_QueryElementAt(mResources, 3, &rv));

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlows,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlows,5));
  nsCOMPtr<aaIFlow> forex(do_QueryElementAt(mFlows,9));

  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1", &rv));
  nsCOMPtr<aaIEvent> event(do_CreateInstance("@aasii.org/base/event;1"));
  nsCOMPtr<aaITransaction> txn(do_CreateInstance(
        "@aasii.org/base/transaction;1", &rv));
  NS_ENSURE_TRUE(txn, rv);

  PRUint32 count = 0;
  nsCOMPtr<aaIBalance> balance;

  /* Receive prepayment from forex deal #4 */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-10 */
    PRExplodedTime tm = {0,0,0,12,10,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 18 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5);
    fact->SetTakeFrom(forex);
    fact->SetGiveTo(bankAccRub);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_OK(rv);

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[gain txn] [fact 18] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[gain txn] [fact 18] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
          / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[gain txn] [fact 18] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
          AA_EVENT_AMOUNT_4, 1), "[gain txn] [fact 18] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[gain txn] [fact 18] flow6 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[gain txn] [fact 18] flow9 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 6);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_15, AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5, 0),
        "[split txn] [fact 18] flow10 is wrong");

    mRubs += AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5;
    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[gain txn] [fact 18] flow3 is wrong");
  } 

  nsCOMPtr<aaIFlow> finres = do_CreateInstance("@aasii.org/base/income-flow;1");
  /* Direct write-off - loss */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-10 */
    PRExplodedTime tm = {0,0,0,12,10,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 19 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_15);
    fact->SetTakeFrom(bankAccEur);
    fact->SetGiveTo(finres);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[write-off txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 7, "[write-off txn] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1,
          AA_EVENT_AMOUNT_2 / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[write-off txn] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1,
          AA_EVENT_AMOUNT_3 / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[write-off txn] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2,
          AA_EVENT_AMOUNT_5, AA_EVENT_AMOUNT_4, 1),
        "[write-off txn] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[write-off txn] flow9 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 6);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 10, AA_MONEY_CODE_2,
          AA_EVENT_AMOUNT_15, AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5, 0),
        "[split txn] [fact 18] flow10 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[write-off txn] flow3 is wrong");

    mEuros -= AA_EVENT_AMOUNT_15;
    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[write-off txn] flow6 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[write-off txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[write-off txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      mProfit -= AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_4;
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() + mProfit),
          "[write-off txn] wrong income saved");
    }
  }

  /* Direct write-in - gain */
  {
    event = do_CreateInstance("@aasii.org/base/event;1", &rv);
    NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);
    /* Set time to 2007-09-10 */
    PRExplodedTime tm = {0,0,0,12,10,8,2007};
    event->SetTime(PR_ImplodeTime(&tm));

    nsCOMPtr<nsIMutableArray> block(do_QueryInterface(event, &rv ));
    NS_ENSURE_TRUE(block, rv);

    /* Fact 20 */
    fact = do_CreateInstance("@aasii.org/base/fact;1", &rv);
    NS_ENSURE_TRUE(fact, rv);
    fact->SetAmount(AA_EVENT_AMOUNT_15);
    fact->SetTakeFrom(finres);
    fact->SetGiveTo(forex);

    rv = block->AppendElement(fact, PR_FALSE);
    NS_TEST_ASSERT_OK(rv);

    rv = mSession->Save(event, nsnull);
    NS_TEST_ASSERT_OK(rv);

    txn = do_CreateInstance("@aasii.org/base/transaction;1", &rv);
    NS_ENSURE_TRUE(txn, rv);
    txn->SetFact(fact);

    rv = mSession->Save(txn, nsnull);
    NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[write-in txn] not saved");

    rv = loadBalanceSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mBalanceSet, rv);

    mBalanceSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 6, "[write-in txn] wrong flow count");

    balance = do_QueryElementAt(mBalanceSet, 1);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1,
          AA_EVENT_AMOUNT_2 / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
        "[write-in txn] flow2 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 0);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1,
          AA_EVENT_AMOUNT_3 / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
        "[write-in txn] flow1 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 3);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2,
          AA_EVENT_AMOUNT_5, AA_EVENT_AMOUNT_4, 1),
        "[write-in txn] flow4 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 5);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
          1, AA_EVENT_RATE_6, 1),
        "[write-in txn] flow9 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 2);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
          mRubs, mRubs, 1), "[write-in txn] flow3 is wrong");

    balance = do_QueryElementAt(mBalanceSet, 4);
    NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
          mEuros, mEuros * AA_EVENT_RATE_4, 1),
        "[write-in txn] flow6 is wrong");

    rv = loadIncomeSet();
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_TRUE(mIncomeSet, rv);

    count = 0;
    mIncomeSet->GetLength(&count);
    NS_TEST_ASSERT_MSG(count == 1, "[write-in txn] wrong income count");

    balance = do_QueryElementAt(mIncomeSet, 0);
    NS_TEST_ASSERT_MSG(balance, "[write-in txn] income not loaded" );
    if (NS_LIKELY(balance)) {
      mProfit += AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5;
      NS_TEST_ASSERT_MSG(isZero(balance->PickValue() - mProfit),
          "[write-in txn] wrong income saved");
    }
  }

  return NS_OK;
}
