/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAACCOUNTTEST_H
#define AAACCOUNTTEST_H

#define AA_ACCOUNT_TEST_CID \
{0x9f3b3e19, 0x2f06, 0x4a75, {0xbc, 0xb8, 0xd7, 0xd3, 0x5c, 0xaf, 0x9d, 0x36}}
#define AA_ACCOUNT_TEST_CONTRACT "@aasii.org/storage/unit-account-core;1"

#include "nsCOMPtr.h"

class nsIArray;
class aaISqlRequest;
class aaISession;
class aaIFact;
#ifdef DEBUG
#include "nsIArray.h"
#include "aaISession.h"
#include "aaISqlRequest.h"
#include "aaIFact.h"
#endif

class aaAccountTest: public nsITest
{
public:
  aaAccountTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaAccountTest() {;}
  class RAII
  {
    public:
      RAII(aaAccountTest *t);
      ~RAII();

      PRBool status;
    private:
      aaAccountTest* test;
      nsresult init();
  };

  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<aaISqlRequest> mFlowLoader;
  nsCOMPtr<aaISqlRequest> mResourceLoader;
  nsCOMPtr<aaISqlRequest> mEntityLoader;
  nsCOMPtr<aaISqlRequest> mBalanceLoader;
  nsCOMPtr<aaISqlRequest> mPendingLoader;
  nsCOMPtr<aaISqlRequest> mIncomeLoader;
  nsCOMPtr<aaISqlRequest> mTxnLoader;
  nsCOMPtr<nsIArray> mFlows;
  nsCOMPtr<nsIArray> mResources;
  nsCOMPtr<nsIArray> mEntities;
  nsCOMPtr<nsIArray> mBalanceSet;
  nsCOMPtr<nsIArray> mIncomeSet;
  nsCOMPtr<nsIArray> mTxnSet;
  nsCOMPtr<aaIFact> mPendingFact;

  PRUint32 mPendingCount;
  double mRubs;
  double mEuros;
  double mProfit;

  nsresult loadBalanceSet();
  nsresult loadIncomeSet();
  nsresult loadPendingFact(nsITestRunner *aTestRunner);
  nsresult loadLastTxn();

  nsresult testEmptyBalance(nsITestRunner *aTestRunner);
  nsresult testPendingFacts(nsITestRunner *aTestRunner);
  nsresult testChart(nsITestRunner *aTestRunner);
  nsresult testTransaction(nsITestRunner *aTestRunner);
  nsresult testUpdateTransaction(nsITestRunner *aTestRunner);
  nsresult testReplaceTransaction(nsITestRunner *aTestRunner);
  nsresult testStateFilter(nsITestRunner *aTestRunner);
  nsresult testCreditTransaction(nsITestRunner *aTestRunner);
  nsresult testDeleteTransaction(nsITestRunner *aTestRunner);
  nsresult testFlowChart(nsITestRunner *aTestRunner);
  nsresult testLoadIncome(nsITestRunner *aTestRunner);
  nsresult testStopTransaction(nsITestRunner *aTestRunner);
  nsresult testLossTransaction(nsITestRunner *aTestRunner);
  nsresult testSplitTransaction(nsITestRunner *aTestRunner);
  nsresult testGainTransaction(nsITestRunner *aTestRunner);
  nsresult testDirectGainsLosses(nsITestRunner *aTestRunner);
};

#endif /* AAACCOUNTTEST_H */
