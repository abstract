/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATAXRULESTEST_H
#define AATAXRULESTEST_H 1

#define AA_TAX_RULES_TEST_CID \
{0x96b2d894, 0x5b8d, 0x4b7b, {0x87, 0x1b, 0xda, 0xe2, 0xeb, 0x0b, 0xc1, 0xbd}}
#define AA_TAX_RULES_TEST_CONTRACT "@aasii.org/storage/unit-tax-rules;1"

#include "nsCOMPtr.h"
#include "nsITest.h"

#ifndef DEBUG
class aaISession;
#else
#include "aaISession.h"
#endif

class aaTaxRulesTest: public nsITest
{
public:
  aaTaxRulesTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaTaxRulesTest() {;}
  class RAII
  {
    public:
      RAII(aaTaxRulesTest *t);
      ~RAII();

      PRBool status;
    private:
      aaTaxRulesTest* test;
      nsresult init();
  };

  nsresult testSaveRule(nsITestRunner *aTestRunner);
  
  nsCOMPtr<aaISession> mSession;
};

#endif /* AATAXRULESTEST_H */
