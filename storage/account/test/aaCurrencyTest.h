/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AACURRENCYTEST_H
#define AACURRENCYTEST_H 1

#include "nsCOMPtr.h"
#include "nsITest.h"
#include "aaIEntity.h"

#define AA_CURRENCY_TEST_CID \
{0x0a0806cb, 0xb496, 0x4df7, {0xa4, 0xa2, 0x4a, 0x62, 0x7f, 0x7e, 0xaa, 0x75}}
#define AA_CURRENCY_TEST_CONTRACT \
  "@aasii.org/storage/unit-account-currency;1"

class nsITestRunner;
class aaISession;
class aaISqlRequest;
#ifdef DEBUG
#include "aaISession.h"
#include "aaISqlRequest.h"
#endif

class aaCurrencyTest: public nsITest
{
public:
  aaCurrencyTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaCurrencyTest() {;}
  class RAII
  {
    public:
      RAII(aaCurrencyTest *t);
      ~RAII();

      PRBool status;
    private:
      aaCurrencyTest* test;
      nsresult init();
  };

  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<aaISqlRequest> mFlowLoader;
  nsCOMPtr<aaISqlRequest> mResourceLoader;
  nsCOMPtr<aaISqlRequest> mEntityLoader;
  nsCOMPtr<nsIArray> mFlows;
  nsCOMPtr<nsIArray> mResources;
  nsCOMPtr<nsIArray> mEntities;
  nsCOMPtr<aaIEntity> mBank;

  nsresult testPurchase(nsITestRunner *aTestRunner);
  nsresult testRateChangeBeforeIncome(nsITestRunner *aTestRunner);
  nsresult testSaleAdvance(nsITestRunner *aTestRunner);
  nsresult testForexSale(nsITestRunner *aTestRunner);
  nsresult testRateChange(nsITestRunner *aTestRunner);
  nsresult testForexSaleAfterChange(nsITestRunner *aTestRunner);
  nsresult testTransferRollback(nsITestRunner *aTestRunner);
};

#endif /* AACURRENCYTEST_H */
