/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsISupportsPrimitives.h"
#include "nsIMutableArray.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaInsertMoney.h"

static const char *sql = "INSERT INTO money (id,alpha_code) VALUES (?,?)";

aaInsertMoney::aaInsertMoney()
{
  mSql = sql;
}

NS_IMPL_ISUPPORTS_INHERITED0(aaInsertMoney,
                             aaSaveRequest)

/* aaSaveRequest */
NS_IMETHODIMP
aaInsertMoney::SetFilter(nsISupports * aFilter)
{
  nsresult rv;
  mMoney = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaInsertMoney::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  NS_ENSURE_TRUE(mMoney, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsAutoString wTag;
  rv = mMoney->GetTag(wTag);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64
    = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(mMoney->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsCString> valCString
    = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valCString->SetData(NS_ConvertUTF16toUTF8(wTag));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valCString, 1, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  *aParams = result;
  result.forget();

  return NS_OK;
}
