/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSFERDELETE_H
#define AATRANSFERDELETE_H 1

#define AA_TRANSFER_DELETE_CID \
{0x95acfd9c, 0x9263, 0x4507, {0xb4, 0xdf, 0x95, 0x89, 0xfa, 0xf4, 0x78, 0x8f}}

#include "aaSaveRequest.h"

class aaIEvent;
#ifdef DEBUG
#include "aaIEvent.h"
#endif

class aaTransferDelete : public aaSaveRequest
{
public:
  aaTransferDelete();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaTransferDelete() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIEvent> mEvent;
};

#endif /* AATRANSFERDELETE_H */
