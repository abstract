/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTRESOURCE_H
#define AAINSERTRESOURCE_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_RESOURCE_CID \
{0xd4c81e37, 0x086a, 0x4b27, {0xba, 0xdc, 0x11, 0xfa, 0x6c, 0x7a, 0xc6, 0x70}}

class aaIResource;
#ifdef DEBUG
#include "aaIResource.h"
#endif

class aaInsertResource : public aaSaveRequest
{
public:
  aaInsertResource();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertResource() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIResource> mResource;
};

#endif /* AAINSERTRESOURCE_H */
