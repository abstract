/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "pldhash.h"

/* Project includes */
#include "nsStringUtils.h"
#include "aaIFlow.h"
#include "aaStorageUtils.h"
#include "aaSqlFilter.h"

aaSqlFilter::aaSqlFilter()
{
  mParams.Init();
}

aaSqlFilter::~aaSqlFilter()
{
}

NS_IMPL_ISUPPORTS2(aaSqlFilter,
                   nsIInterfaceRequestor,
                   aaISqlFilter)
/* nsIInterfaceRequestor */
NS_IMETHODIMP
aaSqlFilter::GetInterface(const nsIID & uuid, void * *result)
{ 
  NS_ENSURE_ARG_POINTER(result);
  void *tmp = 0;
  PRInt32 i;
  for (i = 0; i < mElements.Count(); i++) {
    mElements[i]->QueryInterface(uuid, &tmp);
    if (tmp) {
      *result = tmp;
      return NS_OK;
    }
  }

  return NS_NOINTERFACE;
}

/* aaISqlFilter */
NS_IMETHODIMP
aaSqlFilter::SetInterface(const nsIID & uuid, nsISupports *aInstance)
{
  NS_ENSURE_ARG_POINTER(aInstance);
  void *tmp = 0;
  PRInt32 i;
  for (i = 0; i < mElements.Count(); i++) {
    mElements[i]->QueryInterface(uuid, &tmp);
    if (tmp) {
      mElements.ReplaceObjectAt(aInstance, i);
      return NS_OK;
    }
  }
  mElements.AppendObject(aInstance);

  return NS_OK;
}

NS_IMETHODIMP
aaSqlFilter::Clear(void)
{
  mElements.Clear();
  mParams.Clear();
  return NS_OK;
}

NS_IMETHODIMP
  aaSqlFilter::AddParam(const nsACString& name, nsISupports* aParamInst)
{
  NS_ENSURE_ARG_POINTER(aParamInst);
  NS_ENSURE_TRUE(name.Length() > 0, NS_ERROR_INVALID_ARG);
  if (mParams.Put(name, aParamInst)) {
    return NS_OK;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
  aaSqlFilter::GetParam(const nsACString& name, nsISupports** _retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(name.Length() > 0, NS_ERROR_INVALID_ARG);
  if (mParams.Get(name, _retval)) {
    return NS_OK;
  }
  return NS_ERROR_FAILURE;
}

PLDHashOperator HashTableEnumerator(const nsACString& name, nsCOMPtr<nsISupports>& ptr, void* userArg)
{
  aaIParamObserver* pObserver = static_cast<aaIParamObserver*>(userArg);
  if (NS_SUCCEEDED(pObserver->Param(name, ptr.get()))) {
    return PL_DHASH_STOP;
  }
  return PL_DHASH_NEXT;//PLDHashOperator::PL_DHASH_NEXT;
}

NS_IMETHODIMP
  aaSqlFilter::Enumerate(aaIParamObserver* pObserver)
{
  NS_ENSURE_ARG_POINTER(pObserver);
  if (mParams.Enumerate(&HashTableEnumerator, static_cast<void*>(pObserver)) > 0) {
    return NS_OK;
  }
  return NS_ERROR_FAILURE;
}
