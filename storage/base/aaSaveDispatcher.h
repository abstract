/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEDISPATCHER_H
#define AASAVEDISPATCHER_H 1

#define AA_SAVEDISPATCHER_CID \
{0xd657bc39, 0xf9b7, 0x41b9, {0xad, 0xd3, 0x0f, 0x31, 0xae, 0xcf, 0xc3, 0xb2}}

#include "aaISaveQuery.h"
#include "aaIChartHandler.h"

class aaIEntity;
class aaIObject;
class aaIFlow;
class aaISession;
class aaISqlTransaction;

class aaSaveDispatcher : public aaISaveQuery,
                         public aaIChartHandler
{
public:
  aaSaveDispatcher() {}
  NS_DECL_ISUPPORTS
  NS_DECL_AAISAVEQUERY
  NS_DECL_AAIHANDLER
  NS_DECL_AAICHARTHANDLER

  nsresult Init(nsISupports *aSession);
private:
  ~aaSaveDispatcher() {}
  friend class aaStorageTest;

  aaISession* mParentSession;
  aaIDataNode *mOldNode;

  nsCOMPtr<aaISqlTransaction> mEntitySync;
  nsCOMPtr<aaISqlTransaction> mAssetSync;
  nsCOMPtr<aaISqlTransaction> mMoneySync;
  nsCOMPtr<aaISqlTransaction> mFlowSync;
  nsCOMPtr<aaISqlTransaction> mRuleSync;
  nsCOMPtr<aaISqlTransaction> mEventSync;
  nsCOMPtr<aaISqlTransaction> mQuoteSync;
  nsCOMPtr<aaISqlTransaction> mTransactionSync;
  nsCOMPtr<aaISqlTransaction> mChartSync;
};

#endif /* AASAVEDISPATCHER_H */
