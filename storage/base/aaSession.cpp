/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsIIOService.h"
#include "nsIFile.h"
#include "nsNetCID.h"
#include "nsStringAPI.h"
#include "nsIArray.h"
#include "nsDebugUtils.h"

/* Unfrozen API */
#include "nsAppDirectoryServiceDefs.h"
#include "mozStorageCID.h"
#include "mozIStorageService.h"
#include "mozIStorageConnection.h"

/* Project includes */
#include "aaIManager.h"
#include "aaISqlRequest.h"
#include "aaISqlTransaction.h"
#include "aaSessionUtils.h"
#include "aaBaseLoaders.h"
#include "aaSqlChannel.h"
#include "aaSession.h"

/******** Transaction Guard ********/
class aaGuard
{
public:
  aaGuard(mozIStorageConnection *aConnection)
    :mConnection(aConnection) {;}
  ~aaGuard() { if (mConnection) mConnection->RollbackTransaction(); }
  nsresult Dismiss();
private:
  mozIStorageConnection *mConnection;
};

inline nsresult
aaGuard::Dismiss()
{
  mozIStorageConnection *tmp = mConnection;
  mConnection = nsnull;
  return tmp->CommitTransaction();
}

/******** aaSession ********/
aaSession::aaSession()
{
}

aaSession::~aaSession()
{
}

NS_IMPL_ISUPPORTS2(aaSession,
                   aaISaveQuery,
                   aaISession)

/* aaISession */
NS_IMETHODIMP
aaSession::GetConnectionURI(nsIURI * * _retval)
{ 
  NS_ENSURE_ARG_POINTER(_retval);
  nsresult rv;

  nsCOMPtr<nsIIOService> ios = do_GetService(NS_IOSERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = mConnection->GetDatabaseFile(getter_AddRefs(file));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = ios->NewFileURI(file, _retval);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSession::Load(aaISqlRequest *aRequest, nsIArray * * _retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  nsresult rv;
  if (! mLoader) {
    mLoader = do_CreateInstance(AA_SQLSIMPLELOADER_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  nsCOMPtr<nsISupports> param = do_QueryInterface(aRequest);
  rv = mLoader->SetParam(param);
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<nsISupports> retval;
  rv =  Execute(mLoader, getter_AddRefs(retval));
  NS_ENSURE_SUCCESS(rv, rv);
  return CallQueryInterface(retval, _retval);
}

NS_IMETHODIMP
aaSession::Execute(aaISqlTransaction *aTransaction, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aTransaction);
  NS_ENSURE_STATE(mConnection);
  nsresult rv;

  nsRefPtr<aaSqlChannel> channel = new aaSqlChannel(this);
  NS_ENSURE_TRUE(channel, NS_ERROR_OUT_OF_MEMORY);

  rv = mConnection->BeginTransaction();
  NS_ENSURE_SUCCESS(rv, rv);

  aaGuard guard(mConnection);
  rv = aTransaction->Execute(channel, _retval);
  if (NS_FAILED(rv)) return rv;

  return guard.Dismiss();
} 

/* aaISaveQuery */
NS_IMETHODIMP
aaSession::Save(aaIDataNode *aNode, aaIDataNode *aOldNode)
{
  nsresult rv;
  if (! mDispatcher) {
    mDispatcher = do_CreateInstance("@aasii.org/storage/save-dispatcher;1",
        this, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  return mDispatcher->Save(aNode, aOldNode);
}

/* Public methods */

/* Private methods */
struct aaTable {
  const char *name;
  const char *sql;
};

const struct aaTable tables[] =
{
  {"entity",
    "id INTEGER PRIMARY KEY AUTOINCREMENT,\
      tag CHAR (47) UNIQUE"}

  ,{"resource",
    "type INTEGER NOT NULL, id INTEGER NOT NULL,\
      PRIMARY KEY (type, id)"}

  ,{"asset",
    "id INTEGER PRIMARY KEY AUTOINCREMENT,\
      tag CHAR (47) UNIQUE"}

  ,{"money",
    "id INTEGER PRIMARY KEY NOT NULL,\
      alpha_code CHAR (3) UNIQUE NOT NULL"}

  ,{"flow",
    "id INTEGER PRIMARY KEY AUTOINCREMENT,\
      tag CHAR (47) UNIQUE, entity_id INTEGER NOT NULL, rate DOUBLE NOT NULL,\
      off_balance BOOLEAN NOT NULL"}

  ,{"term",
    "flow_id INTEGER NOT NULL, side BOOLEAN NOT\
      NULL, resource_type INTEGER NOT NULL, resource_id INTEGER NOT NULL,\
      PRIMARY KEY (flow_id, side)"}

  ,{"fact",
    "day INTEGER NOT NULL, event_id INTEGER NOT NULL,\
      transfer_id INTEGER NOT NULL, side BOOLEAN NOT NULL, flow_id INTEGER\
      NOT NULL, PRIMARY KEY (day, event_id, transfer_id, side)"}

  ,{"norm",
    "flow_id INTEGER NOT NULL,\
      id INTEGER NOT NULL,\
      fact_side BOOLEAN NOT NULL,\
      change_side BOOLEAN NOT NULL,\
      rate DOUBLE NOT NULL,\
      tag CHAR (47),\
      PRIMARY KEY (flow_id, id)"}

  ,{"clause",
    "flow_id INTEGER NOT NULL, norm_id INTEGER NOT NULL,\
      side BOOLEAN NOT NULL, link_flow_id BOOLEAN NOT NULL,\
      PRIMARY KEY (flow_id, norm_id, side)"}

  ,{"state",
    "id INTEGER PRIMARY KEY AUTOINCREMENT,\
      flow_id INTEGER NOT NULL, side INTEGER NOT NULL,\
      amount DOUBLE NOT NULL, start INTEGER NOT NULL, paid INTEGER"}

  ,{"event",
    "day INTEGER NOT NULL, id INTEGER NOT NULL,\
      PRIMARY KEY (day, id)"}

  ,{"transfer",
    "day INTEGER NOT NULL, event_id INTEGER\
      NOT NULL, id INTEGER NOT NULL, amount DOUBLE NOT NULL,\
      PRIMARY KEY (day, event_id, id)"}

  /* XXX _acc This belongs to a separate module */
  ,{"txn",
    "day INTEGER NOT NULL, event_id INTEGER\
      NOT NULL, transfer_id INTEGER NOT NULL, value DOUBLE NOT NULL,\
      status INTEGER NOT NULL DEFAULT 0, earnings DOUBLE,\
      PRIMARY KEY (day, event_id, transfer_id)"}

  ,{"quote",
    "resource_id INTEGER NOT NULL,\
      day INTEGER NOT NULL, rate DOUBLE NOT NULL,\
      diff DOUBLE NOT NULL, PRIMARY KEY (resource_id, day)"}

  ,{"balance",
    "id INTEGER PRIMARY KEY AUTOINCREMENT,\
      flow_id INTEGER NOT NULL, side INTEGER NOT NULL,\
      amount DOUBLE NOT NULL, value DOUBLE NOT NULL, start INTEGER NOT NULL,\
      paid INTEGER, UNIQUE (flow_id, start)"}

  ,{"income",
    "start INTEGER PRIMARY KEY NOT NULL,\
      side INTEGER NOT NULL,\
      value DOUBLE NOT NULL,\
      paid INTEGER"}

  ,{"chart",
    "id INTEGER PRIMARY KEY NOT NULL,\
      resource_id INTEGER NOT NULL"}
};

  nsresult
aaSession::getDatabase(nsISupports *dbFile)
{
  nsresult rv;

  nsCOMPtr<nsIFile> file = do_QueryInterface(dbFile);

  if (!file) { 
    nsCOMPtr<aaIManager> manager = 
      do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsIFile> file;
    rv = manager->TmpFileFromString("default.sqlite", PR_FALSE,
        getter_AddRefs(file));
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<mozIStorageService> storageSvc;
  storageSvc = do_GetService(MOZ_STORAGE_SERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<mozIStorageConnection> connection;
  rv = storageSvc->OpenDatabase(file, getter_AddRefs( connection ));
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 i;
  for (i = 0; i < sizeof(tables) / sizeof(struct aaTable); i++) {
    rv = checkTable(connection, tables[i].name, tables[i].sql);
    NS_ENSURE_SUCCESS(rv, rv);
  } 
  mConnection = connection;

  return NS_OK;
}

  nsresult
aaSession::checkTable(mozIStorageConnection *aConnection,
    const char *tableName, const char *createString)
{
  nsresult rv;
  PRBool hasTable;

  rv = aConnection->TableExists(nsDependentCString(tableName), &hasTable);
  NS_ENSURE_SUCCESS(rv, rv);
  if (! hasTable ) {
    rv = aConnection->CreateTable(tableName, createString);
    NS_ENSURE_SUCCESS(rv, rv);
   }

  return NS_OK;
} 

