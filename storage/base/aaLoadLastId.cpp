/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsISupportsPrimitives.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "mozIStorageStatement.h"

/* Project includes */
#include "aaIListNode.h"
#include "aaIHandler.h"
#include "aaLoadLastId.h"

#define AA_LOAD_LAST_ID_BEGIN "SELECT id FROM "
#define AA_LOAD_LAST_ID_END " WHERE ROWID == last_insert_rowid()"

aaLoadLastId::aaLoadLastId()
{
}

NS_IMPL_ISUPPORTS2(aaLoadLastId,
                   aaISqlRequest,
                   aaIDataNode)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadLastId::GetSql(nsACString & aSql)
{
  aSql.Assign(mSql);
  return NS_OK;
}

NS_IMETHODIMP
aaLoadLastId::GetColumn(const char *aPrefix, PRUint32 *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  PRUint32 i =  PRUint32 (-1);
  if (!strcmp("id", aPrefix)) {
    i = 0;
  }
  *_retval = i;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadLastId::GetStatement(mozIStorageStatement * *aStatement)
{
  NS_ENSURE_ARG_POINTER(aStatement);
  NS_IF_ADDREF(*aStatement = mStatement);
  return NS_OK;
}
NS_IMETHODIMP
aaLoadLastId::SetStatement(mozIStorageStatement * aStatement)
{
  mStatement = aStatement;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadLastId::SetFilter(nsISupports * aFilter)
{
  nsresult rv;
  nsCOMPtr<nsISupportsCString> filter = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString table;
  rv = filter->GetData(table);
  NS_ENSURE_SUCCESS(rv, rv);

  mStatement = nsnull;
  mSql.Assign(AA_LOAD_LAST_ID_BEGIN);
  mSql.Append(table);
  mSql.Append(AA_LOAD_LAST_ID_END);
  return NS_OK;
}

NS_IMETHODIMP
aaLoadLastId::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  *aParams = nsnull;

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadLastId::GetEdited(PRBool *aEdited)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaLoadLastId::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleNode(nsnull);
}

NS_IMETHODIMP
aaLoadLastId::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
