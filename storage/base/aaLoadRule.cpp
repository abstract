/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */
#include "nsIInterfaceRequestorUtils.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIState.h"
#include "aaBaseKeys.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"

#include "aaRuleListener.h"
#include "aaLoadRule.h"

aaLoadRule::aaLoadRule()
{
}

NS_IMPL_ISUPPORTS1(aaLoadRule,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaLoadRule::SetParam(nsISupports *aParam)
{
  nsresult rv;
  NS_ENSURE_ARG_POINTER(aParam);
  mFlow = do_QueryInterface(aParam, &rv);
  if (!mFlow)
    mFlow = do_GetInterface(aParam, &rv);
  return rv;
}

NS_IMETHODIMP
aaLoadRule::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  rv = checkParam();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = getRules(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  if (_retval)
    NS_IF_ADDREF(*_retval = mFlow);

  return NS_OK;
}

/* Private methods */
nsresult
aaLoadRule::checkParam()
{
  NS_ENSURE_TRUE(mFlow->PickId(), NS_ERROR_INVALID_ARG);
  return NS_OK;
}

nsresult
aaLoadRule::getRules(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mRuleGetter) {
    mRuleGetter = do_CreateInstance(AA_RULE_GET_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mRuleGetter->SetFilter(mFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> listener
    = new aaRuleListener(mFlow);
  NS_ENSURE_TRUE(listener, NS_ERROR_OUT_OF_MEMORY);

  rv = aChannel->Open(mRuleGetter, listener);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
