/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsISupportsPrimitives.h"
#include "nsXPCOMCID.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIRule.h"
#include "aaIState.h"
#include "aaBaseKeys.h"
#include "aaISqlFilter.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"
#include "aaSaveNorm.h"

aaSaveNorm::aaSaveNorm()
{
}

NS_IMPL_ISUPPORTS1(aaSaveNorm,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveNorm::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mNorm = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveNorm::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mNorm, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  rv = checkParam();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertNorm(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertClauses(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* Private methods */
nsresult
aaSaveNorm::checkParam()
{
  return NS_OK;
}

nsresult
aaSaveNorm::insertNorm(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mNormInserter) {
    mNormInserter = do_CreateInstance(AA_NORM_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mNormInserter->SetFilter(mNorm);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mNormInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveNorm::insertClauses(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mClauseInserter) {
    mClauseFilter = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    mSide = do_CreateInstance(NS_SUPPORTS_PRBOOL_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mClauseFilter->SetInterface(NS_GET_IID(nsISupportsPRBool), mSide);
    NS_ENSURE_SUCCESS(rv, rv);

    mClauseInserter = do_CreateInstance(AA_CLAUSE_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mClauseInserter->SetFilter(mClauseFilter);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mClauseFilter->SetInterface(NS_GET_IID(aaIRule), mNorm);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIFlow> flow;
  rv = mNorm->GetTakeFrom(getter_AddRefs(flow));
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool side = PR_TRUE;
  if (flow && flow->PickId()) {
    rv = mNorm->GetGiveTo(getter_AddRefs(flow));
    NS_ENSURE_SUCCESS(rv, rv);

    if (flow && flow->PickId()) {
      rv = mSide->SetData(PR_FALSE);
      NS_ENSURE_SUCCESS(rv, rv);

      rv = aChannel->Open(mClauseInserter, nsnull);
      NS_ENSURE_SUCCESS(rv, rv);
    }
  }
  else {
    side = PR_FALSE;
  }

  rv = mSide->SetData(side);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mClauseInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
