/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIResource.h"
#include "aaIQuote.h"
#include "aaIBalance.h"
#include "aaAmountUtils.h"
#include "aaStorageUtils.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveQuote.h"

aaSaveQuote::aaSaveQuote()
{
}

NS_IMPL_ISUPPORTS1(aaSaveQuote,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveQuote::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mQuote = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveQuote::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mQuote, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  rv = checkState();
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIQuote> diffQuote;
  rv = loadDiff(aChannel, getter_AddRefs(diffQuote));
  NS_ENSURE_SUCCESS(rv, rv);

  if (diffQuote) {
    double diff;
    rv = diffQuote->GetDiff(&diff);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mQuote->SetDiff(diff);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = updateIncome(aChannel);
    if (NS_FAILED(rv)) return rv;
  }

  rv = mQuoteInserter->SetFilter(mQuote);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mQuoteInserter, nsnull);
  if (NS_FAILED(rv)) return rv;

  return NS_OK;
}

/* Private methods */
nsresult
aaSaveQuote::checkState()
{
  nsresult rv;

  NS_ENSURE_TRUE(mQuote->PickResource(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(mQuote->PickResource()->PickType() == aaIResource::TYPE_MONEY,
      NS_ERROR_INVALID_ARG);

  if(!mDiffLoader) {
    mIncomeLoader = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    mIncomeSaver = do_CreateInstance(AA_SAVEINCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    mQuoteInserter = do_CreateInstance(AA_INSERT_QUOTE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    mDiffLoader = do_CreateInstance(AA_CALCDIFF_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSaveQuote::loadDiff(aaISqliteChannel *aChannel, aaIQuote * *_retval)
{
  nsresult rv;

  nsCOMPtr<aaILoadObserver> diffHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mDiffLoader->SetFilter(mQuote);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mDiffLoader, diffHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> diffResult = do_QueryInterface(diffHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  rv = diffResult->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  if (count) {
    rv = diffResult->QueryElementAt(0, NS_GET_IID(aaIQuote), (void **) _retval);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSaveQuote::loadIncome(aaISqliteChannel *aChannel, aaIBalance * *_retval)
{
  nsresult rv;

  nsCOMPtr<aaILoadObserver> incomeHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeLoader, incomeHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> incomeResult = do_QueryInterface(incomeHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  rv = incomeResult->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  if (count) {
    rv = incomeResult->QueryElementAt(0, NS_GET_IID(aaIBalance),
        (void **) _retval);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSaveQuote::updateIncome(aaISqliteChannel *aChannel)
{
  nsresult rv;

  nsCOMPtr<aaIBalance> income;
  rv = loadIncome(aChannel, getter_AddRefs(income));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mQuote->InitIncome(income);
  if (NS_FAILED(rv)) return rv;

  rv = mQuote->GetIncome(getter_AddRefs(income));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mIncomeSaver->SetParam(income);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mIncomeSaver->Execute(aChannel, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
