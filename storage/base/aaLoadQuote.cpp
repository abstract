/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsStringAPI.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */
#include "nsIInterfaceRequestorUtils.h"

/* Project includes */
#include "aaIMoney.h"
#include "aaIHandler.h"

#include "aaLoadQuote.h"

#include "aaISqlFilter.h"
#include "aaIColumnNameConverter.h"
#include "aaOrderCondition.h"

/**********************************************************/
class CQuoteNameConverter : public aaIColumnNameConverter
{
public:
  CQuoteNameConverter() {}
  ~CQuoteNameConverter() {}
public:
  NS_IMETHOD_(nsCAutoString) convertName(const nsACString& name) const {
    if (name.Equals(NS_LITERAL_CSTRING("quote.tag"))) {
      return NS_LITERAL_CSTRING("money.alpha_code");
    } else if (name.Equals(NS_LITERAL_CSTRING("quote.date"))) {
      return NS_LITERAL_CSTRING("quote.day");
    } else if (name.Equals(NS_LITERAL_CSTRING("quote.rate"))) {
      return NS_LITERAL_CSTRING("quote.rate");
    }
    return nsCAutoString();
  }
};
/**********************************************************/

#ifndef AA_LOADQUOTE_SELECT_QUERY
#define AA_LOADQUOTE_SELECT_QUERY "SELECT\
    money.id,\
    money.alpha_code,\
    strftime('%s',quote.day),\
    quote.rate,\
    quote.diff\
  FROM (SELECT\
          MAX(quote.day) AS latest_day,\
          quote.resource_id AS q_resource_id\
        FROM\
          quote\
        WHERE\
          IFNULL(quote.resource_id = ?, 1)\
        GROUP BY\
          quote.resource_id)\
  LEFT JOIN quote ON\
    q_resource_id=quote.resource_id\
    AND quote.day=latest_day\
  LEFT JOIN money ON\
    money.id=quote.resource_id"
#endif

#define AA_LOADQUOTE_SELECT_QUERY_FILTERED \
  AA_LOADQUOTE_SELECT_QUERY AA_SQL_WHERE_FILTER_MARK AA_SQL_ORDER_MARK

static struct aaLoadRequest::ColumnMap map[] = {
  { "money.id", 0}
  ,{"money.alpha_code", 1}
  ,{"time", 2}
  ,{"rate", 3}
  ,{"diff", 4}
  ,{0, 0}
};

aaLoadQuote::aaLoadQuote()
{
  mSql = AA_LOADQUOTE_SELECT_QUERY;
  mSqlFiltered = AA_LOADQUOTE_SELECT_QUERY_FILTERED;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadQuote,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadQuote::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;

  if (mFilter) {
    nsCOMPtr<aaIMoney> money = do_QueryInterface(mFilter, &rv);
    if (!money) {
      money = do_GetInterface(mFilter, &rv);
      if (NS_NOINTERFACE == rv) {
        rv = result->InsertElementAt(nsnull, 0, PR_FALSE);
        NS_ENSURE_SUCCESS(rv, rv);
        *aParams = result;
        result.forget();

        return NS_OK;
      }
    }
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(money->PickId());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
  NS_IMETHODIMP
aaLoadQuote::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleQuote(nsnull);
}

NS_IMETHODIMP
  aaLoadQuote::GetCondition(aaICondition* pCondition, nsACString& aSql)
{
  nsresult rc = NS_OK;
  CQuoteNameConverter cnvrt;
  NS_ENSURE_ARG_POINTER(pCondition);

  aSql.Truncate();

  pCondition->ToString(&cnvrt, aSql);

  return rc;
}
