/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEMONEY_H
#define AASAVEMONEY_H 1

#define AA_SAVEMONEY_CID \
{0x26a19a6f, 0x2097, 0x413a, {0x8f, 0x93, 0x68, 0x77, 0x0b, 0xfc, 0x68, 0x2c}}

#include "aaISqlTransaction.h"

class aaIMoney;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIMoney.h"
#include "aaISqlRequest.h"
#endif

class aaSaveMoney : public aaISqlTransaction
{
public:
  aaSaveMoney();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveMoney() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIMoney> mMoney;
  nsCOMPtr<aaISqlRequest> mResourceInserter;
  nsCOMPtr<aaISqlRequest> mInserter;

  nsresult insert(aaISqliteChannel *aChannel);
};

#endif /* AASAVEMONEY_H */

