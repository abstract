/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaIHandler.h"
#include "aaIFlow.h"
#include "aaRuleGet.h"

#ifndef AA_NORM_GET_SELECT_QUERY
#define AA_NORM_GET_SELECT_QUERY "SELECT\
    norm.fact_side,\
    norm.change_side,\
    norm.id,\
    norm.rate,\
    flowF.id,\
    flowF.tag,\
    flowF.rate,\
    entityF.id,\
    entityF.tag,\
    term.resource_type,\
    term.resource_id,\
    money.alpha_code,\
    asset.tag,\
    termF.resource_type,\
    termF.resource_id,\
    moneyF.alpha_code,\
    assetF.tag,\
    flowT.id,\
    flowT.tag,\
    flowT.rate,\
    entityT.id,\
    entityT.tag,\
    termT.resource_type,\
    termT.resource_id,\
    moneyT.alpha_code,\
    assetT.tag,\
    norm.tag\
  FROM norm\
    LEFT JOIN clause AS clauseA ON\
      clauseA.flow_id = norm.flow_id\
      AND clauseA.norm_id = norm.id\
    LEFT JOIN clause AS clauseB ON\
      clauseB.flow_id = clauseA.flow_id\
      AND clauseB.norm_id = clauseA.norm_id\
      AND clauseB.side = NOT clauseA.side\
    LEFT JOIN flow AS flowF ON\
      (flowF.id = clauseA.link_flow_id AND clauseA.side = 1)\
      OR (flowF.id = clauseB.link_flow_id AND clauseA.side = 0)\
    LEFT JOIN flow AS flowT ON\
      (flowT.id = clauseA.link_flow_id AND clauseA.side = 0)\
      OR (flowT.id = clauseB.link_flow_id AND clauseA.side = 1)\
    LEFT JOIN entity AS entityF ON\
      entityF.id=flowF.entity_id\
    LEFT JOIN entity AS entityT ON\
      entityT.id=flowT.entity_id\
    LEFT JOIN term AS termF ON\
      termF.flow_id=flowF.id\
      AND termF.side=0\
    LEFT JOIN money AS moneyF ON\
      termF.resource_type=1\
      AND moneyF.id=termF.resource_id\
    LEFT JOIN asset AS assetF ON\
      termF.resource_type=2\
      AND assetF.id=termF.resource_id\
    LEFT JOIN term ON\
      term.flow_id=clauseA.link_flow_id\
      AND term.side=clauseA.side\
    LEFT JOIN money ON\
      term.resource_type=1\
      AND money.id=term.resource_id\
    LEFT JOIN asset ON\
      term.resource_type=2\
      AND asset.id=term.resource_id\
    LEFT JOIN term AS termT ON\
      termT.flow_id=flowT.id\
      AND termT.side=1\
    LEFT JOIN money AS moneyT ON\
      termT.resource_type=1\
      AND moneyT.id=termT.resource_id\
    LEFT JOIN asset AS assetT ON\
      termT.resource_type=2\
      AND assetT.id=termT.resource_id\
  WHERE norm.flow_id = ?\
    AND (clauseA.side = 1 OR (clauseA.side = 0 AND clauseB.side IS NULL))\
  ORDER BY norm.id"
#endif

static struct aaLoadRequest::ColumnMap map[] = {
  { "fact_side", 0}
  ,{"change_side", 1}
  ,{"id", 2}
  ,{"rate", 3}
  ,{"take_flow.id", 4}
  ,{"take_flow.tag", 5}
  ,{"take_flow.rate", 6}
  ,{"take_flow.entity.id", 7}
  ,{"take_flow.entity.tag", 8}
  ,{"take_flow.give_resource.type", 9}
  ,{"take_flow.give_resource.id", 10}
  ,{"take_flow.give_resource.alpha_code", 11}
  ,{"take_flow.give_resource.tag", 12}
  ,{"take_flow.take_resource.type", 13}
  ,{"take_flow.take_resource.id", 14}
  ,{"take_flow.take_resource.alpha_code", 15}
  ,{"take_flow.take_resource.tag", 16}
  ,{"give_flow.id", 17}
  ,{"give_flow.tag", 18}
  ,{"give_flow.rate", 19}
  ,{"give_flow.entity.id", 20}
  ,{"give_flow.entity.tag", 21}
  ,{"give_flow.give_resource.type", 22}
  ,{"give_flow.give_resource.id", 23}
  ,{"give_flow.give_resource.alpha_code", 24}
  ,{"give_flow.give_resource.tag", 25}
  ,{"give_flow.take_resource.type", 9}
  ,{"give_flow.take_resource.id", 10}
  ,{"give_flow.take_resource.alpha_code", 11}
  ,{"give_flow.take_resource.tag", 12}
  ,{"tag", 26}
  ,{0, 0}
};

aaRuleGet::aaRuleGet()
{
  mSql = AA_NORM_GET_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaRuleGet,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaRuleGet::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<aaIFlow> flow = do_QueryInterface(mFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  
  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(flow->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaRuleGet::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleRule(nsnull);
}
