/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEENTITY_H
#define AASAVEENTITY_H 1

#define AA_SAVEENTITY_CID \
{0x94bf571c, 0x8ba1, 0x4442, {0xbf, 0xbe, 0x46, 0x48, 0x4f, 0x20, 0x27, 0x66}}

#include "aaISqlTransaction.h"

class aaIEntity;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIEntity.h"
#include "aaISqlRequest.h"
#endif

class aaSaveEntity : public aaISqlTransaction
{
public:
  aaSaveEntity();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

private:
  ~aaSaveEntity() {}
  friend class aaStorageTest;

  nsCOMPtr<aaIEntity> mEntity;
  nsCOMPtr<aaISqlRequest> mInserter;
  nsCOMPtr<aaISqlRequest> mIdLoader;
  nsCOMPtr<aaISqlRequest> mUpdater;

  nsresult insert(aaISqliteChannel *aChannel);
  nsresult update(aaISqliteChannel *aChannel);
};

#endif /* AASAVEENTITY_H */
