/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIHandler.h"
#include "aaLoadEntity.h"
#include "aaISqlFilter.h"

#include "aaIColumnNameConverter.h"
#include "aaOrderCondition.h"

/************************************************************************/
class CEntityNameConverter : public aaIColumnNameConverter
{
public:
  CEntityNameConverter() {}
  ~CEntityNameConverter() {}
public:
  NS_IMETHOD_(nsCAutoString) convertName(const nsACString& name) const {
    if (name.Equals(NS_LITERAL_CSTRING("entity.tag"))) {
      return NS_LITERAL_CSTRING("tag");
    }
    return nsCAutoString();
  }
};
/************************************************************************/

#define AA_LOADENTITY_SELECT_QUERY "SELECT id,tag FROM entity"
#define AA_LOADENTITY_SELECT_QUERY_FILTERED \
  "SELECT id,tag FROM entity" AA_SQL_WHERE_FILTER_MARK AA_SQL_ORDER_MARK

static struct aaLoadRequest::ColumnMap map[] = {
  { "id", 0 }
  ,{"tag", 1}
  ,{0, 0}
};

aaLoadEntity::aaLoadEntity()
{
  mSql = AA_LOADENTITY_SELECT_QUERY;
  mSqlFiltered = AA_LOADENTITY_SELECT_QUERY_FILTERED;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadEntity,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadEntity::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  *aParams = nsnull;

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadEntity::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleEntity(nsnull);
}

NS_IMETHODIMP
  aaLoadEntity::GetCondition(aaICondition* pCondition, nsACString& aSql)
{
  nsresult rc = NS_OK;
  CEntityNameConverter cnvrt;
  NS_ENSURE_ARG_POINTER(pCondition);

  aSql.Truncate();

  pCondition->ToString(&cnvrt, aSql);

  return rc;
}
