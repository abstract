/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADPENDINGFACTS_H
#define AALOADPENDINGFACTS_H 1

#include "aaLoadRequest.h"

#define AA_LOADPENDINGFACTS_CID \
{0xf41eaab7, 0x4f62, 0x4da3, {0xa9, 0x36, 0x90, 0x9d, 0x6a, 0x90, 0xec, 0x09}}

class aaLoadPendingFacts : public aaLoadRequest
{
public:
  aaLoadPendingFacts();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AALOADREQUEST

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
protected:
  ~aaLoadPendingFacts() {}

private:
  friend class aaStorageTest;
};

#endif /* AALOADPENDINGFACTS_H */

