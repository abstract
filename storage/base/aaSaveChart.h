/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVECHART_H
#define AASAVECHART_H 1

#define AA_SAVECHART_CID \
{0xefbabf55, 0xc26f, 0x431a, {0xb4, 0x03, 0x11, 0x85, 0x1b, 0x8c, 0x17, 0xf5}}

#include "aaISqlTransaction.h"

class aaIChart;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIChart.h"
#include "aaISqlRequest.h"
#endif

class aaSaveChart : public aaISqlTransaction
{
public:
  aaSaveChart();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveChart() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIChart> mChart;
  nsCOMPtr<aaISqlRequest> mChartInserter;

  nsresult insertChart(aaISqliteChannel *aChannel);
};

#endif /* AASAVECHART_H */
