/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_ORDERCONDITION_H
#define AA_ORDERCONDITION_H

#include "aaIOrderCondition.h"
#include "nsTArray.h"
#include "nsStringAPI.h"

#define ORDER_CONDITION_NAME "order"

#define AA_ORDER_CONDITION_ID \
  { 0x2371379f, 0xac36, 0x4721, { 0x98, 0xab, 0xfc, 0xda, 0xf4, 0xc5, 0x86, 0x9b } }

class aaOrderCondition
  : public aaIOrderCondition
{
public:
  aaOrderCondition();
  NS_DECL_ISUPPORTS
  NS_DECL_AAICONDITION
  NS_DECL_AAIORDERCONDITION
protected:
  ~aaOrderCondition(){}
protected:
  struct OrderStruct {
    nsCAutoString column;
    PRInt16 mDirection;
  };
  nsCAutoString OrderStructToString(const OrderStruct& os, const aaIColumnNameConverter* pConverter) const;
private:
  nsTArray<OrderStruct> mOrders;
};

#endif //AA_ORDERCONDITION_H
