/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_FILTERCONDITION_H
#define AA_FILTERCONDITION_H

#include "aaIFilterCondition.h"
#include "nsTArray.h"
#include "nsStringAPI.h"

#define FILTER_CONDITION_NAME "filter"

#define AA_FILTER_CONDITION_ID \
  { 0x4ec57b66, 0x4165, 0x4931, { 0x89, 0x6b, 0xc9, 0x13, 0x25, 0xd1, 0x29, 0x38 } }

class aaFilterCondition
  : public aaIFilterCondition
{
public:
  aaFilterCondition();
  NS_DECL_ISUPPORTS
  NS_DECL_AAICONDITION
  NS_DECL_AAIFILTERCONDITION
protected:
  ~aaFilterCondition(){}
protected:
  struct FilterStruct {
    nsCAutoString mColumn;
    nsCAutoString mCompareStr;
  };
  nsCAutoString FilterStructToString(const FilterStruct& fs, const aaIColumnNameConverter* pConverter) const;
private:
  nsTArray<FilterStruct> mFilters;
};

#endif //AA_FILTERCONDITION_H
