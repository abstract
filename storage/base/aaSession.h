/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASESSION_H
#define AASESSION_H 1

#define AA_SESSION_CID \
{0xc7eb3168, 0x85f9, 0x4fa2, {0x9e, 0x0f, 0xec, 0xd4, 0x21, 0xf3, 0x6e, 0x98}}

#include "nsAutoPtr.h"
#include "aaISession.h"

class mozIStorageConnection;
#ifdef DEBUG
#include "mozIStorageConnection.h"
#endif

class aaSession : public aaISession
{
public:
  aaSession();
  virtual ~aaSession();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISESSION
  NS_DECL_AAISAVEQUERY

  nsresult Init(nsISupports *dbFile = nsnull) { return getDatabase(dbFile); }

  mozIStorageConnection* GetConnection() const { return mConnection; }
private:
  friend class aaStorageTest;
  /* XXX _acc This has to be removed after accounting is separated*/
  friend class aaAccountTest;

  nsCOMPtr<mozIStorageConnection> mConnection;
  nsCOMPtr<aaISaveQuery> mDispatcher;
  nsCOMPtr<aaISqlTransaction> mLoader;

  nsresult getDatabase(nsISupports *dbFile);
  nsresult checkTable(mozIStorageConnection *aConnection,
      const char *tableName, const char *createString);
};

#endif /* AASESSION_H */
