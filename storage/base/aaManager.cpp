/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsDirectoryServiceDefs.h"
#include "nsIProperties.h"
#include "nsIFile.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsAppDirectoryServiceDefs.h"

/* Project includes */
#include "aaManager.h"

aaManager::aaManager()
{
}

aaManager::~aaManager()
{
}

NS_IMPL_ISUPPORTS1(aaManager,
                   aaIManager)

/* aaIManager */
NS_IMETHODIMP
aaManager::TmpFileFromString(const char *aName, PRBool aReplace,
    nsIFile * *_retval)
{
  return TmpFileFromCString(nsDependentCString(aName), aReplace, _retval);
}

NS_IMETHODIMP
aaManager::TmpFileFromCString(const nsACString& aName, PRBool aReplace,
    nsIFile * *_retval)
{
  nsresult rv;

  nsCOMPtr<nsIProperties> dsp;
  dsp = do_GetService("@mozilla.org/file/directory_service;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file, dir;
  rv = dsp->Get(NS_OS_TEMP_DIR, NS_GET_IID(nsIFile), getter_AddRefs(file));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = file->Clone(getter_AddRefs( dir ));
  NS_ENSURE_SUCCESS(rv, rv);
                   
  rv = file->AppendNative(aName);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool exists;
  rv = file->Exists(&exists);
  NS_ENSURE_SUCCESS(rv, rv);

  if (exists && aReplace) {
    rv = file->Remove(PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  NS_IF_ADDREF(*_retval = file);
  return NS_OK;
}

