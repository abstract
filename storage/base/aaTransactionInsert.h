/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSACTIONINSERT_H
#define AATRANSACTIONINSERT_H 1

#define AA_TRANSACTION_INSERT_CID \
{0x5d9f1d4f, 0xca66, 0x47a6, {0x91, 0x5d, 0x00, 0xc2, 0x5a, 0xde, 0x0c, 0x00}}

#include "aaSaveRequest.h"

class aaITransaction;
#ifdef DEBUG
#include "aaITransaction.h"
#endif

class aaTransactionInsert : public aaSaveRequest
{
public:
  aaTransactionInsert();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaTransactionInsert() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaITransaction> mTransaction;
};

#endif /* AATRANSACTIONINSERT_H */
