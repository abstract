/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIBalance.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveState.h"

aaSaveState::aaSaveState()
{
}

NS_IMPL_ISUPPORTS2(aaSaveState,
                   aaISqlTransaction,
                   aaIStorager)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveState::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mState = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveState::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mState, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER(mState->PickFlow());

  nsresult rv;
  rv = mState->Sync(this, aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* aaIStorager */
NS_IMETHODIMP
aaSaveState::Insert(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mStateInserter) {
    mStateInserter = do_CreateInstance(AA_STATE_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mStateInserter->SetFilter(mState);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mStateInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveState::Update(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mStateUpdater) {
    mStateUpdater = do_CreateInstance(AA_STATE_UPDATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mStateUpdater->SetFilter(mState);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mStateUpdater, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveState::Close(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mStateCloser) {
    mStateCloser = do_CreateInstance(AA_STATE_CLOSE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mStateCloser->SetFilter(mState);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mStateCloser, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveState::Delete(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mStateDeleter) {
    mStateDeleter = do_CreateInstance(AA_STATE_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mStateDeleter->SetFilter(mState);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mStateDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
