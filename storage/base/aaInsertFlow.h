/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTFLOW_H
#define AAINSERTFLOW_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_FLOW_CID \
{0xf3526e4f, 0xa7ae, 0x4d15, {0xb3, 0x45, 0x87, 0x1a, 0xa0, 0x13, 0xd9, 0xa7}}

class aaIFlow;
#ifdef DEBUG
#include "aaIFlow.h"
#endif

class aaInsertFlow : public aaSaveRequest
{
public:
  aaInsertFlow();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertFlow() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIFlow> mFlow;
};

#endif /* AAINSERTFLOW_H */
