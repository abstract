/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEBALANCE_H
#define AASAVEBALANCE_H 1

#define AA_SAVEBALANCE_CID \
{0x1740fb02, 0x990a, 0x4cd0, {0xbc, 0xcc, 0x38, 0x3c, 0xbe, 0x35, 0x30, 0x9f}}

#include "aaISqlTransaction.h"
#include "aaIStorager.h"

class aaIBalance;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIBalance.h"
#include "aaISqlRequest.h"
#endif

class aaSaveBalance : public aaISqlTransaction,
                    public aaIStorager
{
public:
  aaSaveBalance();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION
  NS_DECL_AAISTORAGER

protected:
  ~aaSaveBalance() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIBalance> mBalance;
  nsCOMPtr<aaISqlRequest> mBalanceInserter;
  nsCOMPtr<aaISqlRequest> mBalanceUpdater;
  nsCOMPtr<aaISqlRequest> mBalanceCloser;
  nsCOMPtr<aaISqlRequest> mBalanceDeleter;
};

#endif /* AASAVEBALANCE_H */
