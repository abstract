/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AACLAUSEINSERT_H
#define AACLAUSEINSERT_H 1

#include "aaSaveRequest.h"

#define AA_CLAUSE_INSERT_CID \
{0x7de509d6, 0x7d28, 0x4f7e, {0xb4, 0x26, 0x90, 0x10, 0x6d, 0xfc, 0x00, 0xfe}}

class nsIInterfaceRequestor;
#ifdef DEBUG
#include "nsIInterfaceRequestor.h"
#endif

class aaClauseInsert : public aaSaveRequest
{
public:
  aaClauseInsert();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaClauseInsert() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<nsIInterfaceRequestor> mFilter;
};

#endif /* AACLAUSEINSERT_H */
