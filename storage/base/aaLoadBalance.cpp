/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaITimeFrame.h"
#include "aaIFlow.h"
#include "aaIChartHandler.h"
#include "aaLoadBalance.h"

#ifndef AA_LOADBALANCE_SELECT_QUERY
#define AA_LOADBALANCE_SELECT_QUERY "SELECT link.id, link.tag,\
    entity.id, entity.tag, giveT.resource_type, giveT.resource_id,\
    giveM.alpha_code, giveA.tag, takeT.resource_type, takeT.resource_id,\
    takeM.alpha_code, takeA.tag, link.rate, balance.amount, balance.side,\
    balance.value, takeQ.rate, giveQ.rate, strftime('%s',balance.start),\
    strftime('%s',balance.paid), strftime('%s',takeQ.day),\
    strftime('%s',giveQ.day), NULL, takeC.id, giveC.id, balance.id\
  FROM \
    (SELECT flow.id AS id, flow.tag AS tag, flow.rate AS rate,\
     flow.entity_id AS entity_id, balance.start AS start\
     FROM flow\
     LEFT JOIN balance ON balance.flow_id==flow.id AND IFNULL(\
       balance.start <= (replace(round(julianday(?1,'unixepoch')),'.0','')),1)\
     AND (balance.paid > (replace(round(julianday(?2,'unixepoch')),'.0','')) \
         OR balance.paid IS NULL) WHERE IFNULL(flow.id=?3,balance.start IS NOT NULL))\
    AS link\
    LEFT JOIN balance ON link.id=balance.flow_id AND balance.start=link.start\
    LEFT JOIN entity ON link.entity_id==entity.id\
    LEFT JOIN term AS giveT ON giveT.flow_id = link.id AND giveT.side = 0\
    LEFT JOIN money AS giveM ON giveT.resource_type = 1 AND giveM.id = giveT.resource_id\
    LEFT JOIN asset AS giveA ON giveT.resource_type = 2 AND giveA.id = giveT.resource_id\
    LEFT JOIN term AS takeT ON takeT.flow_id = link.id AND takeT.side = 1\
    LEFT JOIN money AS takeM ON takeT.resource_type = 1 AND takeM.id = takeT.resource_id\
    LEFT JOIN asset AS takeA ON takeT.resource_type = 2 AND takeA.id = takeT.resource_id\
    LEFT JOIN chart AS giveC ON giveC.resource_id=giveT.resource_id AND giveC.id = 1\
    LEFT JOIN chart AS takeC ON takeC.resource_id=takeT.resource_id AND takeC.id = 1\
    LEFT JOIN quote AS giveQ ON giveQ.resource_id=giveT.resource_id AND giveQ.day=(\
        SELECT MAX(quote.day) FROM quote\
        WHERE quote.resource_id=giveT.resource_id\
          AND 1=giveT.resource_type\
          AND IFNULL(quote.day<=IFNULL(balance.paid,?1),1))\
    LEFT JOIN quote AS takeQ ON takeQ.resource_id=takeT.resource_id AND takeQ.day=(\
        SELECT MAX(quote.day) FROM quote\
        WHERE quote.resource_id=takeT.resource_id\
          AND 1=takeT.resource_type\
          AND IFNULL(quote.day<=IFNULL(balance.paid,?1),1))"
#endif

static struct aaLoadRequest::ColumnMap map[] = {
  { "flow.id", 0 }
  ,{"flow.tag", 1}
  ,{"flow.entity.id", 2}
  ,{"flow.entity.tag", 3}
  ,{"flow.give_resource.type", 4}
  ,{"flow.give_resource.id", 5}
  ,{"quote0.money.id", 5}
  ,{"flow.give_resource.alpha_code", 6}
  ,{"quote0.money.alpha_code", 6}
  ,{"flow.give_resource.tag", 7}
  ,{"flow.take_resource.type", 8}
  ,{"flow.take_resource.id", 9}
  ,{"quote1.money.id", 9}
  ,{"flow.take_resource.alpha_code", 10}
  ,{"quote1.money.alpha_code", 10}
  ,{"flow.take_resource.tag", 11}
  ,{"flow.rate", 12}
  ,{"amount", 13}
  ,{"side", 14}
  ,{"value", 15}
  ,{"quote1.rate", 16}
  ,{"quote0.rate", 17}
  ,{"start", 18}
  ,{"paid", 19}
  ,{"quote1.time", 20}
  ,{"quote0.time", 21}
  ,{"quote1.diff", 22}
  ,{"quote0.diff", 22}
  ,{"quote1.is_chart", 23}
  ,{"quote0.is_chart", 24}
  ,{"id", 25}
  ,{0, 0}
};


aaLoadBalance::aaLoadBalance()
{
  mSql = AA_LOADBALANCE_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadBalance,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadBalance::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<aaIFlow> flow = do_QueryInterface(mFilter);
  if (!flow)
    flow = do_GetInterface(mFilter);

  nsCOMPtr<aaITimeFrame> frame = do_GetInterface(mFilter);

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;

  if (frame) {
    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickEnd() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickStart() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = result->InsertElementAt(nsnull, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (flow) {
    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(flow->PickId());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 2, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 2, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
  NS_IMETHODIMP
aaLoadBalance::Accept(aaIHandler *aQuery)
{
  nsresult rv;
  nsCOMPtr<aaIChartHandler> chartQuery = do_QueryInterface(aQuery, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  return chartQuery->HandleBalance(nsnull);
}
