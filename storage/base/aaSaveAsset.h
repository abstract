/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEASSET_H
#define AASAVEASSET_H 1

#define AA_SAVEASSET_CID \
{0x2f790daf, 0xc155, 0x4018, {0x8a, 0x49, 0x0e, 0xf3, 0xc7, 0x44, 0x65, 0x36}}

#include "aaISqlTransaction.h"

class aaIAsset;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIResource.h"
#include "aaISqlRequest.h"
#endif

class aaSaveAsset : public aaISqlTransaction
{
public:
  aaSaveAsset();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveAsset() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIAsset> mAsset;
  nsCOMPtr<aaISqlRequest> mResourceInserter;
  nsCOMPtr<aaISqlRequest> mInserter;
  nsCOMPtr<aaISqlRequest> mIdLoader;
  nsCOMPtr<aaISqlRequest> mUpdater;

  nsresult insert(aaISqliteChannel *aChannel);
  nsresult update(aaISqliteChannel *aChannel);
};

#endif /* AASAVEASSET_H */
