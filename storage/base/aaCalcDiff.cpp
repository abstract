/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaIResource.h"
#include "aaIQuote.h"
#include "aaIHandler.h"

#include "aaCalcDiff.h"

#ifndef AA_CALCDIFF_SELECT_QUERY
#define AA_CALCDIFF_SELECT_QUERY "SELECT\
    money.id,\
    money.alpha_code,\
    diff.time,\
    diff.new_rate,\
    diff.newSumF - diff.oldSumF - diff.newSumT + diff.oldSumT,\
    diff.constrain\
  FROM (\
    SELECT\
      ? AS money_id,\
      strftime('%s',quote.day) AS time,\
      ?2 AS new_rate,\
      COUNT(balance.paid) AS constrain,\
      SUM(IFNULL(NULLIF(balance.side,1),ROUND(balance.amount * ?2, 2)))\
        AS newSumF,\
      SUM(IFNULL(NULLIF(balance.side,1),ROUND(balance.amount * quote.rate, 2)))\
        AS oldSumF,\
      SUM(IFNULL(NULLIF(balance.side,0),ROUND(balance.amount * ?2, 2)))\
        AS newSumT,\
      SUM(IFNULL(NULLIF(balance.side,0),ROUND(balance.amount * quote.rate, 2)))\
        AS oldSumT\
    FROM  term\
    INNER JOIN balance ON\
      term.side=balance.side\
      AND term.flow_id=balance.flow_id\
    LEFT JOIN quote ON\
      term.resource_id=quote.resource_id\
      AND quote.day=(SELECT MAX(day) FROM quote\
          WHERE quote.resource_id=term.resource_id)\
    WHERE\
      term.resource_id=money_id\
      AND term.resource_type=1\
      AND (balance.paid IS NULL\
          OR balance.paid>=replace(round(julianday(?,'unixepoch')),'.0',''))\
    GROUP BY term.resource_id) AS diff\
  LEFT JOIN money ON\
    money.id = diff.money_id"
#endif

static struct aaLoadRequest::ColumnMap map[] = {
  { "money.id", 0}
  ,{"money.alpha_code", 1}
  ,{"time", 2}
  ,{"rate", 3}
  ,{"diff", 4}
  ,{"constrain", 5}
  ,{0, 0}
};

aaCalcDiff::aaCalcDiff()
{
  mSql = AA_CALCDIFF_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaCalcDiff,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaCalcDiff::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<aaIQuote> quote = do_QueryInterface(mFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_ARG_POINTER(quote->PickResource());

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;
  nsCOMPtr<nsISupportsDouble> valDouble;

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(quote->PickResource()->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valDouble = do_CreateInstance(NS_SUPPORTS_DOUBLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valDouble->SetData(quote->PickRate());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valDouble, 1, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(quote->PickTime() / 1000000);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 2, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaCalcDiff::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleQuote(nsnull);
}
