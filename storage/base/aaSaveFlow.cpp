/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIState.h"
#include "aaBaseKeys.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"
#include "aaSaveFlow.h"

aaSaveFlow::aaSaveFlow()
{
}

NS_IMPL_ISUPPORTS1(aaSaveFlow,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveFlow::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mFlow = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveFlow::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  rv = checkParam();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertFlow(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertTerms(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* Private methods */
nsresult
aaSaveFlow::checkParam()
{
  nsresult rv;
  double rate;
  rv = mFlow->GetRate( &rate );
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(rate != 0.0, NS_ERROR_INVALID_ARG);

  nsCOMPtr<aaIEntity> entity;
  rv = mFlow->GetEntity(getter_AddRefs( entity ));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(entity, NS_ERROR_INVALID_ARG);

  nsCOMPtr<aaIResource> resource;

  rv = mFlow->GetGiveResource(getter_AddRefs( resource ));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(resource, NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(resource->PickId(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(resource->PickType() == aaIResource::TYPE_MONEY ||
      resource->PickType() == aaIResource::TYPE_ASSET, NS_ERROR_INVALID_ARG);

  rv = mFlow->GetTakeResource(getter_AddRefs( resource ));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(resource, NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(resource->PickId(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(resource->PickType() == aaIResource::TYPE_MONEY ||
      resource->PickType() == aaIResource::TYPE_ASSET, NS_ERROR_INVALID_ARG);

  return NS_OK;
}

nsresult
aaSaveFlow::insertFlow(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mFlowInserter) {
    mIdLoader = do_CreateInstance(AA_LOADLASTID_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsISupportsCString> filter
      = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = filter->SetData(NS_LITERAL_CSTRING("flow"));
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = mIdLoader->SetFilter(filter);
    NS_ENSURE_SUCCESS(rv, rv);

    mFlowInserter = do_CreateInstance(AA_INSERT_FLOW_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mFlowInserter->SetFilter(mFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mFlowInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> id
    = do_CreateInstance(AA_SQLIDLISTENER_CONTRACT, mFlow, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIdLoader, id);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveFlow::insertTerms(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mTermInserter) {
    mTermInserter = do_CreateInstance(AA_INSERT_TERM_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  nsCOMPtr<aaIState> filter = do_CreateInstance(AA_STATE_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = filter->SetFlow(mFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mTermInserter->SetFilter(filter);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTermInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = filter->SetSide(1);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTermInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
