/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIState.h"

#include "aaStateUpdate.h"

static const char *sql =
"UPDATE state SET side = ?, amount = ? WHERE id == ?";

aaStateUpdate::aaStateUpdate()
{
  mSql = sql;
}

NS_IMPL_ISUPPORTS_INHERITED0(aaStateUpdate,
                             aaSaveRequest)

/* aaSaveRequest */
NS_IMETHODIMP
aaStateUpdate::SetFilter(nsISupports * aFilter)
{
  nsresult rv;
  mState = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaStateUpdate::GetParams(nsIArray * * aParams)
{ 
  NS_ENSURE_TRUE(mState, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  PRInt64 id;
  rv = mState->GetId(&id);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;
  nsCOMPtr<nsISupportsPRInt32> valInt32;
  nsCOMPtr<nsISupportsDouble> valDouble;

  valInt32 = do_CreateInstance(NS_SUPPORTS_PRINT32_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt32->SetData(mState->PickSide());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt32, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valDouble = do_CreateInstance(NS_SUPPORTS_DOUBLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valDouble->SetData(normVal(mState->PickAmount()));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valDouble, 1, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(id);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 2, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  *aParams = result;
  result.forget();

  return NS_OK;
}
