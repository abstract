/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTNORM_H
#define AAINSERTNORM_H 1

#include "aaSaveRequest.h"

#define AA_NORM_INSERT_CID \
{0x8dbca2f6, 0xb07f, 0x4d90, {0xa1, 0xa2, 0x64, 0xe9, 0x4a, 0x24, 0x76, 0xb2}}

class aaIRule;
#ifdef DEBUG
#include "aaIRule.h"
#endif

class aaNormInsert : public aaSaveRequest
{
public:
  aaNormInsert();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaNormInsert() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIRule> mNorm;
};

#endif /* AAINSERTNORM_H */
