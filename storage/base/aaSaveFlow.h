/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEFLOW_H
#define AASAVEFLOW_H 1

#define AA_SAVEFLOW_CID \
{0xf55fca12, 0x3ce3, 0x4b1e, {0x82, 0x41, 0x9f, 0x28, 0xf2, 0x8f, 0x14, 0x42}}

#include "aaISqlTransaction.h"

class aaIFlow;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIFlow.h"
#include "aaISqlRequest.h"
#include "mozIStorageConnection.h"
#endif

class aaSaveFlow : public aaISqlTransaction
{
public:
  aaSaveFlow();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveFlow() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIFlow> mFlow;
  nsCOMPtr<aaISqlRequest> mFlowInserter;
  nsCOMPtr<aaISqlRequest> mIdLoader;
  nsCOMPtr<aaISqlRequest> mTermInserter;

  nsresult checkParam();
  nsresult insertFlow(aaISqliteChannel *aChannel);
  nsresult insertTerms(aaISqliteChannel *aChannel);
};

#endif /* AASAVEFLOW_H */
