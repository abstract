/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsISupportsPrimitives.h"
#include "nsXPCOMCID.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"
#include "aaSaveEntity.h"

aaSaveEntity::aaSaveEntity()
{
}

NS_IMPL_ISUPPORTS1(aaSaveEntity,
                   aaISqlTransaction)

/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveEntity::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mEntity = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveEntity::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mEntity, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  PRInt64 id;
  rv = mEntity->GetId(&id);
  NS_ENSURE_SUCCESS(rv, rv);

  if (!id) {
    rv = insert(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = update(aChannel);
    if (NS_FAILED(rv)) return rv;
  }

  if (_retval)
    *_retval = nsnull;

  return NS_OK;
}

/* private methods */
nsresult
aaSaveEntity::insert(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mInserter) {
    mInserter = do_CreateInstance(AA_INSERT_ENTITY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mInserter->SetFilter(mEntity);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  if (!mIdLoader) {
    mIdLoader = do_CreateInstance(AA_LOADLASTID_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsISupportsCString> filter
      = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = filter->SetData(NS_LITERAL_CSTRING("entity"));
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = mIdLoader->SetFilter(filter);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<aaILoadObserver> id
    = do_CreateInstance(AA_SQLIDLISTENER_CONTRACT, mEntity, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIdLoader, id);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEntity::update(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mUpdater) {
    mUpdater = do_CreateInstance(AA_UPDATE_ENTITY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mUpdater->SetFilter(mEntity);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mUpdater, nsnull);
  if (NS_FAILED(rv)) return rv;

  return NS_OK;
}
