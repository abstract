/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLSIMPLELISTENER_H
#define AASQLSIMPLELISTENER_H 1

#include "nsIMutableArray.h"
#include "aaISqlRequest.h"

#define AA_SQLSIMPLELISTENER_CID \
{0x5a2d1c53, 0x8c45, 0x4a27, {0xa1, 0x34, 0x60, 0xb5, 0x3c, 0x64, 0xc2, 0xa7}}

class aaSqlSimpleListener : public nsIArray,
                            public aaILoadObserver
{ 
public:
  aaSqlSimpleListener();
  NS_DECL_ISUPPORTS
  NS_FORWARD_NSIARRAY(mData->);
  NS_DECL_AAILOADOBSERVER
  
private:
  ~aaSqlSimpleListener() {;}
  friend class aaAccountTest;
  
  nsCOMPtr<nsIMutableArray> mData;
};

#endif /* AASQLSIMPLELISTENER_H */
