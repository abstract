/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTMONEY_H
#define AAINSERTMONEY_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_MONEY_CID \
{0xb5ce1cd8, 0xdf34, 0x4bab, {0xbf, 0xbd, 0x61, 0xf1, 0xe7, 0x1d, 0x22, 0x5c}}

class aaIMoney;
#ifdef DEBUG
#include "aaIMoney.h"
#endif

class aaInsertMoney : public aaSaveRequest
{
public:
  aaInsertMoney();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertMoney() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIMoney> mMoney;
};

#endif /* AAINSERTMONEY_H */
