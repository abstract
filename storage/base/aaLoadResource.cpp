/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIHandler.h"
#include "aaLoadResource.h"

#include "aaOrderCondition.h"
#include "aaIColumnNameConverter.h"
#include "aaISqlFilter.h"

#define AA_LOADRESOURCE_SELECT_QUERY "SELECT resource.type, resource.id,\
    IFNULL(money.alpha_code, asset.tag)\
  FROM resource\
    LEFT JOIN money ON resource.type = 1 AND resource.id = money.id \
    LEFT JOIN asset ON resource.type = 2 AND resource.id = asset.id"

#define AA_LOADRESOURCE_SELECT_QUERY_FILTERED \
  AA_LOADRESOURCE_SELECT_QUERY AA_SQL_WHERE_FILTER_MARK AA_SQL_ORDER_MARK

static struct aaLoadRequest::ColumnMap map[] = {
  { "type", 0 }
  ,{"id", 1}
  ,{"alpha_code", 2}
  ,{"tag", 2}
  ,{0, 0}
};

/**********************************************************/
class CResourceNameConverter : public aaIColumnNameConverter
{
public:
  CResourceNameConverter() {}
  ~CResourceNameConverter() {}
public:
  NS_IMETHOD_(nsCAutoString) convertName(const nsACString& name) const
  {
    nsCAutoString ret;
    if (name.Compare("resource.type") == 0) {
      ret = "resource.type";
    } else if (name.Compare("resource.tag") == 0) {
      ret = "IFNULL(money.alpha_code, asset.tag)";
    }
    return ret;
  }
};
/**********************************************************/

aaLoadResource::aaLoadResource()
{
  mSql = AA_LOADRESOURCE_SELECT_QUERY;
  mSqlFiltered = AA_LOADRESOURCE_SELECT_QUERY_FILTERED;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadResource,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadResource::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  *aParams = nsnull;

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadResource::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleResource(nsnull);
}

NS_IMETHODIMP
  aaLoadResource::GetCondition(aaICondition* pCondition, nsACString& aCondition)
{
  CResourceNameConverter cnvrt;

  NS_ENSURE_ARG_POINTER(pCondition);

  aCondition.Truncate();
  pCondition->ToString(&cnvrt, aCondition);
  return NS_OK;
}
