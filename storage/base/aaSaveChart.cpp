/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaIChart.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveChart.h"

aaSaveChart::aaSaveChart()
{
}

NS_IMPL_ISUPPORTS1(aaSaveChart,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveChart::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mChart = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveChart::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mChart, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  rv = insertChart(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveChart::insertChart(aaISqliteChannel *aChannel)
{
  nsresult rv;

  NS_ENSURE_TRUE(mChart->PickCurrency() && mChart->PickCurrency()->PickId(),
      NS_ERROR_INVALID_ARG);

  if (!mChartInserter) {
    mChartInserter = do_CreateInstance(AA_INSERT_CHART_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mChartInserter->SetFilter(mChart);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mChartInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
