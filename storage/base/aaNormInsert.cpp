/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIRule.h"
#include "aaNormInsert.h"

static const char *sql =
  "INSERT INTO norm (flow_id, id, fact_side, change_side, rate, tag)\
    VALUES (?,?,?,?,?,?)";

aaNormInsert::aaNormInsert()
{
  mSql = sql;
}

NS_IMPL_ISUPPORTS_INHERITED0(aaNormInsert,
                             aaSaveRequest)

/* aaSaveRequest */
NS_IMETHODIMP
aaNormInsert::SetFilter(nsISupports * aFilter)
{
  nsresult rv;
  mNorm = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mNorm->Validate();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaNormInsert::GetParams(nsIArray * * aParams)
{ 
  NS_ENSURE_TRUE(mNorm, NS_ERROR_NOT_INITIALIZED);

  nsresult rv;
  rv = mNorm->Validate();
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIFlow> flow;
  rv = mNorm->GetFlow(getter_AddRefs(flow));
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool factSide;
  rv = mNorm->GetFactSide(&factSide);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool changeSide;
  rv = mNorm->GetChangeSide(&changeSide);
  NS_ENSURE_SUCCESS(rv, rv);

  double rate;
  rv = mNorm->GetRate(&rate);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;
  nsCOMPtr<nsISupportsPRInt32> valInt32;
  nsCOMPtr<nsISupportsDouble> valDouble;
  nsCOMPtr<nsISupportsCString> valString;

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(flow->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(mNorm->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt32 = do_CreateInstance(NS_SUPPORTS_PRINT32_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt32->SetData(factSide);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt32, 2, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt32 = do_CreateInstance(NS_SUPPORTS_PRINT32_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt32->SetData(changeSide);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt32, 3, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valDouble = do_CreateInstance(NS_SUPPORTS_DOUBLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valDouble->SetData(rate);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valDouble, 4, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  nsAutoString strTag;
  rv = mNorm->GetTag(strTag);
  NS_ENSURE_SUCCESS(rv, rv);
  valString = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valString->SetData(NS_ConvertUTF16toUTF8(strTag));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valString, 5, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  *aParams = result;
  result.forget();

  return NS_OK;
}
