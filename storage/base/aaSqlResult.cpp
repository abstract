/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsIVariant.h"

/* Unfrozen API */
#include "mozIStorageStatement.h"

/* Project includes */
#include "aaISqlRequest.h"

#include "aaSqlResult.h"

aaSqlResult::aaSqlResult(aaISqlRequest *request)
{
  nsresult rv = initialize(request);
  if (NS_FAILED(rv)) return;

  mRequest = request;
}

aaSqlResult::~aaSqlResult()
{}

NS_IMPL_ISUPPORTS2(aaSqlResult,
                   aaIDataNode,
                   aaISqlResult)

/* aaISqlResult */
NS_IMETHODIMP
aaSqlResult::GetColumn(const char *aPrefix, PRUint32 *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  return mRequest->GetColumn(aPrefix, _retval);
}

NS_IMETHODIMP
aaSqlResult::GetInt32(PRUint32 aIndex, PRInt32 *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aIndex < (PRUint32) mData.Count(), NS_ERROR_INVALID_ARG);

  if (!mData[aIndex])
    return NS_OK;

  return mData[aIndex]->GetAsInt32(_retval);
}

NS_IMETHODIMP
aaSqlResult::GetInt64(PRUint32 aIndex, PRInt64 *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aIndex < (PRUint32) mData.Count(), NS_ERROR_INVALID_ARG);

  if (!mData[aIndex])
    return NS_OK;

  return mData[aIndex]->GetAsInt64(_retval);
}

NS_IMETHODIMP
aaSqlResult::GetDouble(PRUint32 aIndex, double *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aIndex < (PRUint32) mData.Count(), NS_ERROR_INVALID_ARG);

  if (!mData[aIndex])
    return NS_OK;

  return mData[aIndex]->GetAsDouble(_retval);
}

NS_IMETHODIMP
aaSqlResult::GetUTF8String(PRUint32 aIndex, nsACString & _retval NS_OUTPARAM)
{
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aIndex < (PRUint32) mData.Count(), NS_ERROR_INVALID_ARG);

  if (!mData[aIndex]) {
    _retval.Truncate();
    _retval.SetIsVoid(PR_TRUE);
    return NS_OK;
  }
  return mData[aIndex]->GetAsACString(_retval);
}

NS_IMETHODIMP
aaSqlResult::GetIsNull(PRUint32 aIndex, PRBool *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mRequest, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aIndex < (PRUint32) mData.Count(), NS_ERROR_INVALID_ARG);

  *_retval = mData[aIndex] == nsnull;
  return NS_OK;
}

/* private methods */
nsresult
aaSqlResult::initialize(aaISqlRequest *request)
{
  NS_ENSURE_ARG_POINTER(request);
  nsresult rv;

  nsCOMPtr<mozIStorageStatement> statement;
  rv = request->GetStatement(getter_AddRefs(statement));
  NS_ENSURE_SUCCESS(rv, rv); NS_ENSURE_ARG_POINTER(statement);

  PRUint32 count = 0;
  rv = statement->GetNumEntries(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 i;
  for (i = 0; i < count; i++) {
    PRInt32 type;
    nsCOMPtr<nsIWritableVariant>
      item = do_CreateInstance(NS_VARIANT_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = statement->GetTypeOfIndex(i, &type);
    NS_ENSURE_SUCCESS(rv, rv);

    switch (type) {
    case mozIStorageStatement::VALUE_TYPE_INTEGER:
      {
        PRInt64 val64;
        rv = statement->GetInt64(i, &val64);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = item->SetAsInt64(val64);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case mozIStorageStatement::VALUE_TYPE_FLOAT:
      {
        double valDouble;
        rv = statement->GetDouble(i, &valDouble);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = item->SetAsDouble(valDouble);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case mozIStorageStatement::VALUE_TYPE_TEXT:
      {
        nsCAutoString valCString;
        rv = statement->GetUTF8String(i, valCString);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = item->SetAsACString(valCString);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case mozIStorageStatement::VALUE_TYPE_BLOB:
      return NS_ERROR_UNEXPECTED;
    case mozIStorageStatement::VALUE_TYPE_NULL:
      item = nsnull;
      break;
    default:
      return NS_ERROR_UNEXPECTED;
    };

    mData.AppendObject(item);
  }

  NS_ASSERTION((PRUint32) mData.Count() == count, "wrong column count");
  mNode = do_QueryInterface(request, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
