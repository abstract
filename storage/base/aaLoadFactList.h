/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADFACTLIST_H
#define AALOADFACTLIST_H 1

#include "aaLoadRequest.h"

#define AA_LOADFACTLIST_CID \
{0xb08c70ff, 0x9f32, 0x4962, {0xae, 0xc6, 0x95, 0xed, 0x67, 0x5d, 0x6d, 0xbe}}

class aaLoadFactList : public aaLoadRequest
{
public:
  aaLoadFactList();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AALOADREQUEST

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
protected:
  ~aaLoadFactList() {}

private:
  friend class aaStorageTest;
};

#endif /* AALOADFACTLIST_H */

