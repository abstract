/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAUPDATEASSET_H
#define AAUPDATEASSET_H 1

#include "aaSaveRequest.h"

#define AA_UPDATE_ASSET_CID \
{0x18cf0f31, 0xff63, 0x4488, {0xa6, 0xaf, 0x21, 0x7e, 0xfa, 0x68, 0x3d, 0x5e}}

class aaIAsset;
#ifdef DEBUG
#include "aaIResource.h"
#endif

class aaUpdateAsset : public aaSaveRequest
{
public:
  aaUpdateAsset();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaUpdateAsset() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIAsset> mAsset;
};

#endif /* AAUPDATEASSET_H */
