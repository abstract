/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIMutableArray.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIRule.h"
#include "aaIState.h"
#include "aaIEvent.h"
#include "aaIFact.h"
#include "aaIQuote.h"
#include "aaIIncome.h"
#include "aaITransaction.h"
#include "aaIChart.h"
#include "aaBaseKeys.h"
#include "aaChartKeys.h"
#include "aaStorageUtils.h"
#include "aaISqlRequest.h"
#include "aaISqlResult.h"
#include "aaLoadBuilder.h"


NS_IMPL_ISUPPORTS2(aaLoadBuilder,
                   aaIHandler,
                   aaIChartHandler)

nsresult
aaLoadBuilder::Load(aaISqlResult *result, aaIDataNode **_retval)
{
  NS_ENSURE_ARG_POINTER(result);
  nsresult rv;
  mData = result;

  nsCOMPtr<aaIDataNode> iid = do_QueryInterface(mData, &rv);
  rv = iid->Accept(this);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*_retval = mResult);
  return NS_OK;
}

/* aaIHandler */
NS_IMETHODIMP
aaLoadBuilder::HandleNode(aaIListNode *aNode)
{
  nsresult rv;
  nsCOMPtr<aaIListNode> item = aNode;
  if (!aNode) {
    item = do_CreateInstance(AA_ENTITY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  PRInt64 id;
  rv = mData->GetInt64(getColumn("id"), &id); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetId(id);
  NS_ENSURE_SUCCESS(rv, rv);

  if (!aNode)
    mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleEntity(aaIEntity *aEntity)
{
  nsresult rv;
  nsCOMPtr<aaIEntity> item = do_CreateInstance(AA_ENTITY_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = HandleNode(item);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString tag;
  rv = mData->GetUTF8String(getColumn("tag"), tag); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTag(NS_ConvertUTF8toUTF16(tag));
  NS_ENSURE_SUCCESS(rv, rv);

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleResource(aaIResource *aResource)
{
  nsresult rv;
  PRInt32 type;
  rv = mData->GetInt32(getColumn("type"), &type);
  NS_ENSURE_SUCCESS(rv, rv);
  switch (type) {
  case aaIResource::TYPE_MONEY:
    rv = HandleMoney(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    break;
  case aaIResource::TYPE_ASSET:
    rv = HandleAsset(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    break;
  default:
    NS_ASSERTION(PR_FALSE, "Wrong resource type.");
    return NS_ERROR_UNEXPECTED;
  };
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleMoney(aaIMoney *aMoney)
{
  nsresult rv;
  nsCOMPtr<aaIMoney> item = do_CreateInstance(AA_MONEY_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt64 id;
  rv = mData->GetInt64(getColumn("id"), &id);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetNumCode(id);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString alphaCode;
  rv = mData->GetUTF8String(getColumn("alpha_code"), alphaCode);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetAlphaCode(NS_ConvertUTF8toUTF16(alphaCode));
  NS_ENSURE_SUCCESS(rv, rv);

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleAsset(aaIAsset *aAsset)
{
  nsresult rv;
  nsCOMPtr<aaIAsset> item = do_CreateInstance(AA_ASSET_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt64 id;
  rv = mData->GetInt64(getColumn("id"), &id);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetId(id);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString tag;
  rv = mData->GetUTF8String(getColumn("tag"), tag);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTag(NS_ConvertUTF8toUTF16(tag));
  NS_ENSURE_SUCCESS(rv, rv);

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleFlow(aaIFlow *aFlow)
{
  nsresult rv;

  PRUint32 colId = getColumn("id");
  
  PRBool isNull = PR_TRUE;
  PRInt64 id = 0;
  if (colId != (PRUint32) -1) {
    rv = mData->GetIsNull(colId, &isNull);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!isNull) {
    rv = mData->GetInt64(colId, &id); 
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!id) {
    mResult = do_CreateInstance(AA_INCOMEFLOW_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    return NS_OK;
  }

  nsCOMPtr<aaIFlow> item = do_CreateInstance(AA_FLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetId(id);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString tag;
  rv = mData->GetUTF8String(getColumn("tag"), tag); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTag(NS_ConvertUTF8toUTF16(tag));
  NS_ENSURE_SUCCESS(rv, rv);

  double rate;
  rv = mData->GetDouble(getColumn("rate"), &rate); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetRate(rate);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 colOffBalance = getColumn("off_balance");
  if (colOffBalance != (PRUint32) -1) {
    PRBool isOffBalance;
    rv = mData->GetInt32(colOffBalance, &isOffBalance); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetIsOffBalance(isOffBalance);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("entity.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleEntity(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIEntity> entity = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetEntity(entity);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("give_resource.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleResource(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIResource> resource = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetGiveResource(resource);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("take_resource.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleResource(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIResource> resource = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetTakeResource(resource);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleRule(aaIRule *aRule)
{
  nsresult rv;
  nsCOMPtr<aaIRule> item = do_CreateInstance(AA_RULE_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = HandleNode(item);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool changeSide, factSide;
  rv = mData->GetInt32(getColumn("fact_side"), &factSide); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetFactSide(factSide);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mData->GetInt32(getColumn("change_side"), &changeSide); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetChangeSide(changeSide);
  NS_ENSURE_SUCCESS(rv, rv);

  double rate;
  rv = mData->GetDouble(getColumn("rate"), &rate); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetRate(rate);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString tag;
  rv = mData->GetUTF8String(getColumn("tag"), tag);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTag(NS_ConvertUTF8toUTF16(tag));
  NS_ENSURE_SUCCESS(rv, rv);

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("take_flow.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFlow(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIFlow> flow = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetTakeFrom(flow);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("give_flow.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFlow(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIFlow> flow = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetGiveTo(flow);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleState(aaIState *aState)
{
  nsresult rv;
  nsCOMPtr<aaIState> item = aState;

  if (!item) {
    item = do_CreateInstance(AA_STATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("flow.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFlow(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIFlow> flow = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    if (flow->PickId()) {
      rv = item->SetFlow(flow);
      NS_ENSURE_SUCCESS(rv, rv);
    }
  }

  PRUint32 col = getColumn("id");
  PRBool isNull = PR_TRUE;
  if (col != (PRUint32) -1) {
    rv = mData->GetIsNull(col, &isNull);
    NS_ENSURE_SUCCESS(rv, rv);
    if (isNull) {
      if (!aState)
        mResult = item;
      return NS_OK;
    }
  }
  if (!isNull) {
    PRInt64 id;
    rv = mData->GetInt64(col, &id); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetId(id);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  PRBool side;
  rv = mData->GetInt32(getColumn("side"), &side); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetSide(side);
  NS_ENSURE_SUCCESS(rv, rv);

  double amount;
  rv = mData->GetDouble(getColumn("amount"), &amount); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetAmount(amount);
  NS_ENSURE_SUCCESS(rv, rv);

  PRTime start;
  rv = mData->GetInt64(getColumn("start"), &start); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetStart(start * 1000000);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 colPaid = getColumn("paid");
  NS_ENSURE_TRUE(colPaid != (PRUint32) -1, NS_ERROR_INVALID_ARG);
  rv = mData->GetIsNull(colPaid, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (!isNull) {
    PRTime end;
    rv = mData->GetInt64(colPaid, &end); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetEnd(end * 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (!aState)
    mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleFact(aaIFact *aFact)
{
  nsresult rv;
  nsCOMPtr<aaIFact> item = do_CreateInstance(AA_FACT_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt64 id;
  rv = mData->GetInt64(getColumn("id"), &id); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetId(id);
  NS_ENSURE_SUCCESS(rv, rv);

  double amount;
  rv = mData->GetDouble(getColumn("amount"), &amount); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetAmount(amount);
  NS_ENSURE_SUCCESS(rv, rv);

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("event.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleEvent(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIEvent> event = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetEvent(event);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("giveToFlow.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFlow(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIFlow> flow = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetGiveTo(flow);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("takeFromFlow.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFlow(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIFlow> flow = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetTakeFrom(flow);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleEvent(aaIEvent *aEvent)
{
  nsresult rv;
  nsCOMPtr<aaIEvent> item = do_CreateInstance(AA_EVENT_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRTime time;
  rv = mData->GetInt64(getColumn("time"), &time); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTime(time * 1000000);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt64 id;
  rv = mData->GetInt64(getColumn("id"), &id); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetId(id);
  NS_ENSURE_SUCCESS(rv, rv);

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleQuote(aaIQuote *aQuote)
{
  nsresult rv;
  PRUint32 colRate = getColumn("rate");
  if (colRate == (PRUint32) -1) {
    mResult = nsnull;
    return NS_OK;
  }

  nsCOMPtr<aaIQuote> item = do_CreateInstance(AA_QUOTE_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("money.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleMoney(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIResource> resource = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetResource(resource);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  PRUint32 colChart = getColumn("is_chart");
  PRBool isNull = PR_TRUE;
  if (colChart != (PRUint32) -1) {
    rv = mData->GetIsNull(colChart, &isNull);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!isNull) {
    rv = item->SetRate(1.0);
    NS_ENSURE_SUCCESS(rv, rv);
    mResult = item;
    return NS_OK;
  }

  double rate;
  isNull = PR_TRUE;
  rv = mData->GetIsNull(colRate, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (isNull) {
    mResult = nsnull;
    return NS_OK;
  }
  rv = mData->GetDouble(colRate, &rate); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetRate(rate);
  NS_ENSURE_SUCCESS(rv, rv);

  PRTime time;
  rv = mData->GetInt64(getColumn("time"), &time); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetTime(time * 1000000);
  NS_ENSURE_SUCCESS(rv, rv);

  double diff;
  rv = mData->GetDouble(getColumn("diff"), &diff); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetDiff(diff);
  NS_ENSURE_SUCCESS(rv, rv);

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleTransaction(aaITransaction *aTransaction)
{
  nsresult rv;
  nsCOMPtr<aaIFact> fact;
  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("fact.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleFact(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    fact = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  PRUint32 colValue = getColumn("value");
  NS_ENSURE_TRUE(colValue != (PRUint32) -1, NS_ERROR_INVALID_ARG);

  PRBool isNull = PR_FALSE;
  rv = mData->GetIsNull(colValue, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);

  if (isNull) {
    mResult = fact;
    return NS_OK;
  }

  nsCOMPtr<aaITransaction> item
    = do_CreateInstance(AA_TRANSACTION_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = item->SetFact(fact);
  NS_ENSURE_SUCCESS(rv, rv);

  double value;
  rv = mData->GetDouble(colValue, &value); 
  NS_ENSURE_SUCCESS(rv, rv);
  rv = item->SetValue(value);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 colEarnings = getColumn("earnings");
  NS_ENSURE_TRUE(colEarnings != (PRUint32) -1, NS_ERROR_INVALID_ARG);
  rv = mData->GetIsNull(colEarnings, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (!isNull) {
    double earnings;
    rv = mData->GetDouble(colEarnings, &earnings); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetEarnings(earnings);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mResult = item;
  return NS_OK;
}

/* aaIChartHandler */
NS_IMETHODIMP
aaLoadBuilder::HandleChart(aaIChart *aChart)
{
  nsresult rv;
  nsCOMPtr<aaIChart> item = do_CreateInstance(AA_CHART_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("resource.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleMoney(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    nsCOMPtr<aaIMoney> resource = do_QueryInterface(mResult, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetCurrency(resource);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleBalance(aaIBalance *aBalance)
{
  nsresult rv;
  nsCOMPtr<aaIBalance> item = aBalance;;
  if (!item) {
    item = do_CreateInstance(AA_STATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = HandleState(item);
  NS_ENSURE_SUCCESS(rv, rv);

  double value;
  PRUint32 colVal = getColumn("value");
  NS_ENSURE_TRUE(colVal != (PRUint32) -1, NS_ERROR_UNEXPECTED);
  PRBool isNull = PR_FALSE;
  rv = mData->GetIsNull(colVal, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (!isNull) {
    rv = mData->GetDouble(colVal, &value); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetInitValue(value);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("quote1.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleQuote(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    if (mResult) {
      nsCOMPtr<aaIQuote> quote = do_QueryInterface(mResult, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      rv = item->ApplyQuote(quote);
      NS_ENSURE_SUCCESS(rv, rv);
    }
  }

  {
    aaLoadBuilder::PrefixStacker stacker(this);
    rv = appendPrefix("quote0.");
    NS_ENSURE_SUCCESS(rv, rv);
    rv = HandleQuote(nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
    if (mResult) {
      nsCOMPtr<aaIQuote> quote = do_QueryInterface(mResult, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      rv = item->ApplyQuote(quote);
      NS_ENSURE_SUCCESS(rv, rv);
    }
  }

  if (!aBalance)
    mResult = item;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadBuilder::HandleIncome(aaIBalance *aIncome)
{
  nsresult rv;
  nsCOMPtr<aaIIncome> item = do_CreateInstance(AA_INCOME_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = HandleBalance(item);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool isNull = PR_TRUE;
  PRUint32 colIndex = getColumn("credit_diff");
  NS_ENSURE_TRUE(colIndex != (PRUint32) -1, NS_ERROR_INVALID_ARG);
  rv = mData->GetIsNull(colIndex, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (!isNull) {
    double credit_diff;
    rv = mData->GetDouble(colIndex, &credit_diff); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetCreditDiff(credit_diff);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  isNull = PR_TRUE;
  colIndex = getColumn("debit_diff");
  NS_ENSURE_TRUE(colIndex != (PRUint32) -1, NS_ERROR_INVALID_ARG);
  rv = mData->GetIsNull(colIndex, &isNull);
  NS_ENSURE_SUCCESS(rv, rv);
  if (!isNull) {
    double debit_diff;
    rv = mData->GetDouble(colIndex, &debit_diff); 
    NS_ENSURE_SUCCESS(rv, rv);
    rv = item->SetDebitDiff(debit_diff);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  mResult = item;
  return NS_OK;
}

/* Private methods */
nsresult
aaLoadBuilder::appendPrefix(const char *aElement)
{
  PRUint32 len = strlen(aElement);
  NS_ENSURE_TRUE(mIndex + len < AA_BUILD_BUFFER_SIZE, NS_ERROR_UNEXPECTED);
  strcpy(&mPrefix[mIndex], aElement);
  mIndex += len;
  return NS_OK;
}

PRUint32
aaLoadBuilder::getColumn(const char *aElement)
{
  aaLoadBuilder::PrefixStacker stacker(this);
  nsresult rv;
  PRUint32 column = -1;
  rv = appendPrefix(aElement);
  NS_ENSURE_SUCCESS(rv, -1);
  rv = mData->GetColumn(mPrefix, &column);
  NS_ENSURE_SUCCESS(rv, -1);
  return column;
}
