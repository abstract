/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADQUOTE_H
#define AALOADQUOTE_H 1

#include "aaLoadRequest.h"

#define AA_LOADQUOTE_CID \
{0xcea04403, 0x4b2d, 0x40e4, {0x9e, 0x45, 0x5a, 0xba, 0x23, 0x92, 0xff, 0x1b}}

class aaLoadQuote : public aaLoadRequest
{
public:
  aaLoadQuote();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AALOADREQUEST

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition);
protected:
  ~aaLoadQuote() {}

private:
  friend class aaStorageTest;
};

#endif /* AALOADQUOTE_H */
