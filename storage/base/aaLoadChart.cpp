/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIChartHandler.h"
#include "aaLoadChart.h"

#define AA_LOADCHART_SELECT_QUERY "SELECT chart.resource_id, money.alpha_code\
  FROM chart LEFT JOIN money ON chart.resource_id == money.id"

static struct aaLoadRequest::ColumnMap map[] = {
  { "resource.id", 0 }
  ,{"resource.alpha_code", 1}
  ,{0, 0}
};

aaLoadChart::aaLoadChart()
{
  mSql = AA_LOADCHART_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadChart,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadChart::GetParams(nsIArray * * aParams)
{
  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadChart::Accept(aaIHandler *aQuery)
{
  nsresult rv;
  nsCOMPtr<aaIChartHandler> chartQuery = do_QueryInterface(aQuery, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  return chartQuery->HandleChart(nsnull);
}
