/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAMANAGER_H
#define AAMANAGER_H 1

#define AA_MANAGER_CID \
{0x59b402d1, 0x765a, 0x4e97, {0xbb, 0x54, 0x21, 0x22, 0x93, 0x0a, 0x12, 0xfd}}

#include "aaIManager.h"

class aaManager : public aaIManager
{
public:
  aaManager();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIMANAGER

private:
  virtual ~aaManager();

  friend class aaStorageTest;
  /* XXX _acc This has to be removed after accounting is separated*/
  friend class aaAccountTest;
};

#endif /* AAMANAGER_H */

