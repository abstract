/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"
#include <string.h>

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsIArray.h"

/* Unfrozen API */

/* Project includes */
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIBalance.h"
#include "aaITransaction.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveTransaction.h"

/*************** aaSaveTransaction **********************/
aaSaveTransaction::aaSaveTransaction()
{
}

NS_IMPL_ISUPPORTS1(aaSaveTransaction,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveTransaction::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mTransaction = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveTransaction::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mTransaction, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(mTransaction->PickFact(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_ARG(mTransaction->PickFact()->PickEventId());
  NS_ENSURE_TRUE(mTransaction->PickFact()->PickId(), NS_ERROR_INVALID_ARG);
  nsresult rv;

  rv = setBalances(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertTransaction(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* Private methods */
nsresult
aaSaveTransaction::loadBalance(aaISqliteChannel *aChannel, aaIFlow *aFlow,
    aaIBalance * * _retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  nsresult rv;
  if (!mBalanceLoader) {
    mBalanceLoader = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (!aFlow || aFlow->PickIsOffBalance()) {
    *_retval = nsnull;
    return NS_OK;
  }

  nsCOMPtr<aaILoadObserver> balanceHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mBalanceLoader->SetFilter(aFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mBalanceLoader, balanceHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> balanceSet = do_QueryInterface(balanceHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  rv = balanceSet->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(count <= 1, NS_ERROR_UNEXPECTED);

  if (count == 0) {
    *_retval = nsnull;
    return NS_OK;
  }

  rv = balanceSet->QueryElementAt(0, NS_GET_IID(aaIBalance),
      (void **) _retval);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveTransaction::loadIncome(aaISqliteChannel *aChannel, aaIBalance * *_retval)
{
  nsresult rv;

  if(!mIncomeLoader) {
    mIncomeLoader = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<aaILoadObserver> incomeHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeLoader, incomeHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> incomeResult = do_QueryInterface(incomeHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  rv = incomeResult->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  if (count) {
    rv = incomeResult->QueryElementAt(0, NS_GET_IID(aaIBalance),
        (void **) _retval);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSaveTransaction::syncBalance(aaISqliteChannel *aChannel, aaIBalance *aBalance)
{
  nsresult rv;

  if (!aBalance)
    return NS_OK;
  if (!mBalanceSaver) {
    mBalanceSaver = do_CreateInstance(AA_SAVEBALANCE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mBalanceSaver->SetParam(aBalance);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mBalanceSaver->Execute(aChannel, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveTransaction::syncIncome(aaISqliteChannel *aChannel, aaIBalance *aIncome)
{
  nsresult rv;

  if (!mIncomeSaver) {
    mIncomeSaver = do_CreateInstance(AA_SAVEINCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncomeSaver->SetParam(aIncome);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mIncomeSaver->Execute(aChannel, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveTransaction::setBalances(aaISqliteChannel *aChannel)
{
  nsresult rv;

  nsCOMPtr<aaIBalance> balanceFrom, balanceTo;

  rv = loadBalance(aChannel, mTransaction->PickFact()->PickTakeFrom(),
      getter_AddRefs(balanceFrom));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mTransaction->InitBalanceFrom(balanceFrom);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = loadBalance(aChannel, mTransaction->PickFact()->PickGiveTo(),
      getter_AddRefs(balanceTo));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mTransaction->InitBalanceTo(balanceTo);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mTransaction->PickHasEarnings()) {
    nsCOMPtr<aaIBalance> income;
    rv = loadIncome(aChannel, getter_AddRefs(income));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mTransaction->InitIncome(income);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mTransaction->GetIncome(getter_AddRefs(income));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = syncIncome(aChannel, income);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mTransaction->GetBalanceFrom(getter_AddRefs(balanceFrom));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = syncBalance(aChannel, balanceFrom);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mTransaction->GetBalanceTo(getter_AddRefs(balanceTo));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = syncBalance(aChannel, balanceTo);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveTransaction::insertTransaction(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mTransactionInserter) {
    mTransactionInserter =
      do_CreateInstance(AA_TRANSACTION_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mTransactionInserter->SetFilter(mTransaction);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTransactionInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
