/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVENORM_H
#define AASAVENORM_H 1

#define AA_SAVENORM_CID \
{0x6ef4f2c2, 0xefe8, 0x4e03, {0xbe, 0x83, 0xcc, 0xe5, 0x4b, 0xb7, 0x54, 0x87}}

#include "aaISqlTransaction.h"

class aaIRule;
class aaISqlRequest;
class nsISupportsPRBool;
class aaISqlFilter;
#ifdef DEBUG
#include "aaIRule.h"
#include "aaISqlRequest.h"
#include "nsISupportsPrimitives.h"
#include "aaISqlFilter.h"
#endif

class aaSaveNorm : public aaISqlTransaction
{
public:
  aaSaveNorm();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveNorm() {}

private:
  nsCOMPtr<aaIRule> mNorm;
  nsCOMPtr<nsISupportsPRBool> mSide;
  nsCOMPtr<aaISqlFilter> mClauseFilter;
  nsCOMPtr<aaISqlRequest> mNormInserter;
  nsCOMPtr<aaISqlRequest> mClauseInserter;

  nsresult checkParam();
  nsresult insertNorm(aaISqliteChannel *aChannel);
  nsresult insertClauses(aaISqliteChannel *aChannel);
};

#endif /* AASAVENORM_H */
