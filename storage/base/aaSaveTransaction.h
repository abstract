/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVETRANSACTION_H
#define AASAVETRANSACTION_H 1

#define AA_SAVETRANSACTION_CID \
{0x72516923, 0x1133, 0x4dab, {0xbb, 0x10, 0xe4, 0x47, 0xed, 0xa5, 0x03, 0xf2}}

#include "aaISqlTransaction.h"

class aaISqliteChannel;

class aaITransaction;
class aaISqlRequest;
#ifdef DEBUG
#include "aaITransaction.h"
#include "aaISqlRequest.h"
#endif

class aaSaveTransaction : public aaISqlTransaction
{
public:
  aaSaveTransaction();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveTransaction() {}

private:
  friend class aaAccountTest;

  nsCOMPtr<aaITransaction> mTransaction;
  nsCOMPtr<aaISqlRequest> mBalanceLoader;
  nsCOMPtr<aaISqlRequest> mIncomeLoader;
  nsCOMPtr<aaISqlTransaction> mBalanceSaver;
  nsCOMPtr<aaISqlTransaction> mIncomeSaver;
  nsCOMPtr<aaISqlRequest> mTransactionInserter;

  nsresult setBalances(aaISqliteChannel *aChannel);
  nsresult loadBalance(aaISqliteChannel *aChannel, aaIFlow *aFlow,
      aaIBalance * * _retval);
  nsresult loadIncome(aaISqliteChannel *aChannel, aaIBalance * *_retval);
  nsresult insertTransaction(aaISqliteChannel *aChannel);
  nsresult syncBalance(aaISqliteChannel *aChannel, aaIBalance *aBalance);
  nsresult syncIncome(aaISqliteChannel *aChannel, aaIBalance *aIncome);
};

#endif /* AASAVETRANSACTION_H */
