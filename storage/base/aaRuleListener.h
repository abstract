/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AARULELISTENER_H
#define AARULELISTENER_H 1

#include "aaISqlRequest.h"

class aaIFlow;
#ifdef DEBUG
#include "aaIFlow.h"
#endif

#define AA_RULELISTENER_CID \
{0xdf8e7847, 0x16ed, 0x433e, {0xb0, 0x9f, 0xe8, 0x7b, 0xa3, 0x0f, 0x1c, 0x32}}

class aaRuleListener : public aaILoadObserver
{ 
public:
  aaRuleListener(aaIFlow *aFlow);
  NS_DECL_ISUPPORTS
  NS_DECL_AAILOADOBSERVER
  
private:
  ~aaRuleListener() {;}
  
  nsCOMPtr<aaIFlow> mData;
};

#endif /* AARULELISTENER_H */
