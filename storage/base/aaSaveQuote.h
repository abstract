/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEQUOTE_H
#define AASAVEQUOTE_H 1

#define AA_SAVEQUOTE_CID \
{0x85840a0f, 0x41fb, 0x4b4d, {0x9f, 0xf8, 0xb0, 0x74, 0x56, 0x46, 0x31, 0xd1}}

#include "aaISqlTransaction.h"

class aaIBalance;
class aaIQuote;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIQuote.h"
#include "aaISqlRequest.h"
#endif

class aaSaveQuote : public aaISqlTransaction
{
public:
  aaSaveQuote();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

protected:
  ~aaSaveQuote() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIQuote> mQuote;
  nsCOMPtr<aaISqlRequest> mDiffLoader;
  nsCOMPtr<aaISqlRequest> mQuoteInserter;
  nsCOMPtr<aaISqlRequest> mIncomeLoader;
  nsCOMPtr<aaISqlTransaction> mIncomeSaver;

  PRBool mOK;

  nsresult checkState();
  nsresult loadDiff(aaISqliteChannel *aChannel, aaIQuote * *_retval);
  nsresult loadIncome(aaISqliteChannel *aChannel, aaIBalance * *_retval);
  nsresult updateIncome(aaISqliteChannel *aChannel);
  nsresult insertQuote(aaISqliteChannel *aChannel, aaIQuote *aQuote);
};

#endif /* AASAVEQUOTE_H */
