/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich "ynvich@gmail.com"
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIHandler.h"
#include "aaLoadFlow.h"

#include "aaIColumnNameConverter.h"
#include "aaICondition.h"

#define AA_LOADFLOW_SELECT_QUERY "SELECT flow.id, flow.tag, flow.entity_id,\
    entity.tag, giveT.resource_type, giveT.resource_id, giveM.alpha_code,\
    giveA.tag, takeT.resource_type, takeT.resource_id, takeM.alpha_code,\
    takeA.tag, flow.rate, flow.off_balance\
  FROM flow\
  LEFT JOIN entity ON flow.entity_id == entity.id\
  LEFT JOIN term AS giveT ON giveT.flow_id = flow.id AND giveT.side = 0\
  LEFT JOIN money AS giveM ON giveT.resource_type = 1 AND giveM.id = giveT.resource_id\
  LEFT JOIN asset AS giveA ON giveT.resource_type = 2 AND giveA.id = giveT.resource_id\
  LEFT JOIN term AS takeT ON takeT.flow_id = flow.id AND takeT.side = 1\
  LEFT JOIN money AS takeM ON takeT.resource_type = 1 AND takeM.id = takeT.resource_id\
  LEFT JOIN asset AS takeA ON takeT.resource_type = 2 AND takeA.id = takeT.resource_id"

#define AA_LOADFLOW_SELECT_QUERY_FILTERED \
  AA_LOADFLOW_SELECT_QUERY AA_SQL_WHERE_FILTER_MARK AA_SQL_ORDER_MARK

static struct aaLoadRequest::ColumnMap map[] = {
  { "id", 0 }
  ,{"tag", 1}
  ,{"entity.id", 2}
  ,{"entity.tag", 3}
  ,{"give_resource.type", 4}
  ,{"give_resource.id", 5}
  ,{"give_resource.alpha_code", 6}
  ,{"give_resource.tag", 7}
  ,{"take_resource.type", 8}
  ,{"take_resource.id", 9}
  ,{"take_resource.alpha_code", 10}
  ,{"take_resource.tag", 11}
  ,{"rate", 12}
  ,{"off_balance", 13}
  ,{0, 0}
};

/******************************************************************/
class CFlowNameConverter : public aaIColumnNameConverter
{
public:
  CFlowNameConverter() {}
  ~CFlowNameConverter() {}
public:
  NS_IMETHOD_(nsCAutoString) convertName(const nsACString& name) const
  {
    nsCAutoString ret;
    if (name.Compare("flow.tag") == 0) {
      ret = "flow.tag";
    } else if(name.Compare("flow.entity.tag") == 0) {
      ret = "entity.tag";
    }
    return ret;
  }
};
/******************************************************************/

aaLoadFlow::aaLoadFlow()
{
  mSql = AA_LOADFLOW_SELECT_QUERY;
  mSqlFiltered = AA_LOADFLOW_SELECT_QUERY_FILTERED;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadFlow,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadFlow::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  *aParams = nsnull;

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadFlow::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleFlow(nsnull);
}

NS_IMETHODIMP
  aaLoadFlow::GetCondition(aaICondition* pCondition, nsACString& aCondition)
{
  CFlowNameConverter cnvrt;
  NS_ENSURE_ARG_POINTER(pCondition);
  aCondition.Truncate();
  pCondition->ToString(&cnvrt, aCondition);
  return NS_OK;
}
