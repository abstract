/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "aaFilterCondition.h"
#include "aaIColumnNameConverter.h"

NS_IMPL_ISUPPORTS2(aaFilterCondition, aaIFilterCondition, aaICondition)

aaFilterCondition::aaFilterCondition() {
}


NS_IMETHODIMP
  aaFilterCondition::AddFilter(const nsACString & column, const nsACString& compare)
{
  NS_ENSURE_ARG(!column.IsEmpty() && !compare.IsEmpty());
  aaFilterCondition::FilterStruct fs;
  fs.mColumn = column;
  fs.mCompareStr = compare;
  if (mFilters.AppendElement(fs) != nsnull) {
    return NS_OK;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
  aaFilterCondition::GetConditionName(nsACString& condition)
{
  condition.Append(FILTER_CONDITION_NAME);
  return NS_OK;
}

nsCAutoString
  aaFilterCondition::FilterStructToString(const aaFilterCondition::FilterStruct& fs, const aaIColumnNameConverter* pConverter) const
{
  nsCAutoString ret_str;
  ret_str.Append(pConverter->convertName(fs.mColumn));
  ret_str.Append(" LIKE '");
  ret_str.Append(fs.mCompareStr);
  ret_str.Append("'");
  return ret_str;
}

void aaFilterCondition::ToString(const aaIColumnNameConverter * pConverter, nsACString & cond)
{
  if (nsnull == pConverter || mFilters.IsEmpty()) return;
  cond.Append(this->FilterStructToString(mFilters[0], pConverter));
  for(nsTArray<aaFilterCondition::FilterStruct>::size_type i = 1; i < mFilters.Length(); ++ i)
  {
    cond.Append(" AND ");
    cond.Append(this->FilterStructToString(mFilters[i], pConverter));
  }
}
