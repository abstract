/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEREQUEST_H
#define AASAVEREQUEST_H 1

#include "aaISqlRequest.h"

class mozIStorageStatement;
#ifdef DEBUG
#include "mozIStorageStatement.h"
#endif

class aaSaveRequest : public aaISqlRequest
{
public:
  aaSaveRequest();
  NS_DECL_ISUPPORTS
  /* NS_DECL_AAISQLREQUEST */
  NS_SCRIPTABLE NS_IMETHOD GetSql(nsACString & aSql);
  NS_SCRIPTABLE NS_IMETHOD GetColumn(const char *aPrefix, PRUint32 *_retval);
  NS_SCRIPTABLE NS_IMETHOD GetStatement(mozIStorageStatement * *aStatement);
  NS_SCRIPTABLE NS_IMETHOD SetStatement(mozIStorageStatement * aStatement);

  NS_SCRIPTABLE NS_IMETHOD SetFilter(nsISupports *aFilter) = 0;
  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams) = 0;

protected:
  ~aaSaveRequest() {}

  const char *mSql;
  nsCOMPtr<mozIStorageStatement> mStatement;

private:
  friend class aaStorageTest;
};

#define NS_DECL_AASAVEREQUEST                                                 \
  NS_SCRIPTABLE NS_IMETHOD SetFilter(nsISupports *aFilter);                   \
  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams);

#endif /* AASAVEREQUEST_H */
