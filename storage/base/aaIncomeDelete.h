/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINCOMEDELETE_H
#define AAINCOMEDELETE_H 1

#define AA_INCOME_DELETE_CID \
{0xd71f81ac, 0xeeaa, 0x4d85, {0x9e, 0xc2, 0x79, 0xb1, 0xcc, 0x23, 0xd4, 0x21}}

#include "aaSaveRequest.h"

class aaIBalance;
#ifdef DEBUG
#include "aaIBalance.h"
#endif

class aaIncomeDelete : public aaSaveRequest
{
public:
  aaIncomeDelete();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaIncomeDelete() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIBalance> mIncome;
};

#endif /* AAINCOMEDELETE_H */
