/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADREQUEST_H
#define AALOADREQUEST_H 1

#include "aaIDataNode.h"
#include "aaISqlRequest.h"

class nsIInterfaceRequestor;
class mozIStorageStatement;
class aaICondition;
#ifdef DEBUG
#include "nsIInterfaceRequestor.h"
#include "mozIStorageStatement.h"
#endif

#define AA_SQL_ORDER_MARK "%AA_ORDER%"
#define AA_SQL_FILTER_MARK "%AA_FILTER%"
#define AA_SQL_WHERE_FILTER_MARK "%AA_WFILTER%"

class aaLoadRequest : public aaISqlRequest,
                      public aaIDataNode
{
public:
  aaLoadRequest();
  NS_DECL_ISUPPORTS
  /* NS_DECL_AAISQLREQUEST */
  NS_SCRIPTABLE NS_IMETHOD GetSql(nsACString & aSql);
  NS_SCRIPTABLE NS_IMETHOD GetColumn(const char *aPrefix, PRUint32 *_retval);
  NS_SCRIPTABLE NS_IMETHOD GetStatement(mozIStorageStatement * *aStatement);
  NS_SCRIPTABLE NS_IMETHOD SetStatement(mozIStorageStatement * aStatement);
  NS_SCRIPTABLE NS_IMETHOD SetFilter(nsISupports *aFilter);

  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams) = 0;

  /* NS_DECL_AAIDATANODE */
  NS_SCRIPTABLE NS_IMETHOD GetEdited(PRBool *aEdited);
  NS_SCRIPTABLE NS_IMETHOD Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel);

  NS_SCRIPTABLE NS_IMETHOD Accept(aaIHandler *aQuery) = 0;

  struct ColumnMap {
    const char *prefix;
    PRUint32 index;
  };

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition) = 0;

protected:
  ~aaLoadRequest() {}

  const char *mSql;
  const char *mSqlFiltered;
  struct aaLoadRequest::ColumnMap *mMap;
  nsCOMPtr<mozIStorageStatement> mStatement;
  nsCOMPtr<nsISupports> mFilter;

private:
  friend class aaStorageTest;
};

#define NS_DECL_AALOADREQUEST                                                 \
  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams);                   \
  NS_SCRIPTABLE NS_IMETHOD Accept(aaIHandler *aQuery);

#endif /* AALOADREQUEST_H */
