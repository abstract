/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADFLOWSTATES_H
#define AALOADFLOWSTATES_H 1

#include "aaLoadRequest.h"

#define AA_LOADFLOWSTATES_CID \
{0xdb66a553, 0x4d27, 0x4020, {0xa8, 0x36, 0x8d, 0x86, 0xcd, 0x73, 0xa5, 0x21}}

class aaLoadFlowStates : public aaLoadRequest
{
public:
  aaLoadFlowStates();
  NS_DECL_ISUPPORTS_INHERITED

  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams);
  NS_SCRIPTABLE NS_IMETHOD Accept(aaIHandler *aQuery);

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
protected:
  ~aaLoadFlowStates() {}

private:
  friend class aaStorageTest;
};

#endif /* AALOADFLOWSTATES_H */
