/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsIGenericFactory.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaStorageTest.h"

#define AA_STORAGE_TEST_MODULE_CID \
{0x96705450, 0x908f, 0x496d, {0xa9, 0xf5, 0x3d, 0x67, 0xfa, 0xa4, 0x55, 0x6d}}
#define AA_STORAGE_TEST_MODULE_CONTRACT "@aasii.org/storage/unit;1"

class aaStorageTestModule: public nsITest,
                           public nsIUTF8StringEnumerator
{
public:
  aaStorageTestModule():mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  virtual ~aaStorageTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaStorageTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

/* nsITest */
NS_IMETHODIMP
aaStorageTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  "@aasii.org/storage/unit-core;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaStorageTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaStorageTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign( subtests[mSubtest++] );
  return NS_OK;
}

NS_GENERIC_FACTORY_CONSTRUCTOR(aaStorageTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaStorageTest)

static const nsModuleComponentInfo kComponents[] =
{
  {
    "Storage Module Test Container",
    AA_STORAGE_TEST_MODULE_CID,
    AA_STORAGE_TEST_MODULE_CONTRACT,
    aaStorageTestModuleConstructor
  }
  ,{
    "Storage Core Unit Test",
    AA_STORAGE_TEST_CID,
    AA_STORAGE_TEST_CONTRACT,
    aaStorageTestConstructor
  }
};
NS_IMPL_NSGETMODULE(aabase, kComponents)

