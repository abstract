/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"
#include "nsIFile.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"
#include "nsArrayUtils.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsAutoPtr.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"
#include "mozIStorageConnection.h"

/* Project includes */
#include "aaAmountUtils.h"
#include "aaITimeFrame.h"
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIState.h"
#include "aaIEvent.h"
#include "aaBaseKeys.h"
#include "aaSessionUtils.h"
#include "aaSaveKeys.h"
#include "aaSession.h"
#include "aaManager.h"
#include "aaBaseLoaders.h"
#include "aaSaveDispatcher.h"
#include "aaSqlFilter.h"
#include "aaSaveEntity.h"
#include "aaLoadEntity.h"
#include "aaSaveAsset.h"
#include "aaSaveMoney.h"
#include "aaLoadResource.h"
#include "aaSaveFlow.h"
#include "aaLoadFlow.h"
#include "aaSaveEvent.h"
#include "aaLoadFlowStates.h"

#include "aaBaseTestUtils.h"
#include "aaTestConsts.h"

#include "aaStorageTest.h"

NS_IMPL_ISUPPORTS1(aaStorageTest,
                   nsITest)

/* nsITest */
NS_IMETHODIMP
aaStorageTest::Test(nsITestRunner *aTestRunner)
{
  testSession(aTestRunner);
  testSaveDispatcher(aTestRunner);
  testFilter(aTestRunner);
  testSaveEntity(aTestRunner);
  testLoadEntity(aTestRunner);
  testSaveResource(aTestRunner);
  testLoadResource(aTestRunner);
  testSaveFlow(aTestRunner);
  testLoadFlow(aTestRunner);
  testSaveEvent(aTestRunner);
  testLoadFlowStates(aTestRunner);
  testUpdateEvent(aTestRunner);
  testReplaceEvent(aTestRunner);
  testDeleteEvent(aTestRunner);
  testStopEvent(aTestRunner);
  testJunkEvent(aTestRunner);
  mStateLoader = nsnull;
  mSession = nsnull;
  return NS_OK;
}

/* Private methods */
nsresult
aaStorageTest::testSession(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;
  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = manager->TmpFileFromString("storage.sqlite", PR_TRUE,
      getter_AddRefs(file));

  mSession = do_CreateInstance(AA_SESSION_CONTRACT, file);
  NS_TEST_ASSERT_MSG(mSession, "aaSession instance creation" );

  aaSession *session = static_cast<aaSession *>(mSession.get());
  NS_ENSURE_TRUE(session, NS_ERROR_UNEXPECTED);

  NS_TEST_ASSERT_MSG( session->mConnection, "session.connection failed" );

  return NS_OK;
}

nsresult
aaStorageTest::testSaveDispatcher(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaIHandler> saver(do_CreateInstance(
        AA_SAVEDISPATCHER_CONTRACT, mSession, &rv));
  NS_TEST_ASSERT_MSG(saver, "'saveDispatcher' instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaStorageTest::testFilter(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaISqlFilter> filter(do_CreateInstance(
        AA_SQLFILTER_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(filter, "[filter] instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaStorageTest::testSaveEntity(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaISqlTransaction> saver
    = do_CreateInstance(AA_SAVEENTITY_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(saver, "'saveEntity' instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIEntity> node(do_CreateInstance("@aasii.org/base/entity;1"));
  NS_ENSURE_TRUE(node, NS_ERROR_FAILURE);

  node->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_1));
  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveEntity' insertion" );

  node->SetId(0);
  node->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_2));
  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveEntity' second insertion" );

  node->SetId(0);
  node->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_3));
  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveEntity' third insertion" );

  node->SetId(0);
  node->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_4));
  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveEntity' forth insertion" );

  return NS_OK;
}

nsresult
aaStorageTest::testLoadEntity(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  mEntityLoader = do_CreateInstance(AA_LOADENTITY_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mEntityLoader, "[load entity] instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mSession->Load(mEntityLoader, getter_AddRefs(mEntitySet));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load entity] load" );
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  mEntitySet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 4, "[load entity] flow count is wrong");

  nsCOMPtr<aaIEntity> node(do_QueryElementAt(mEntitySet, 0, &rv));
  NS_TEST_ASSERT_MSG(node, "[load entity] 1st item not read" );
  NS_ENSURE_TRUE(node, rv);

  nsAutoString tag;
  node->GetTag(tag);
  NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_ENTITY_TAG_1)),
        "[load entity] 1st item read differ from inserted" );

  PRInt64 id;
  node->GetId(&id);
  NS_TEST_ASSERT_MSG(id == 1, "[load entity] wrong 1st item id");

  return NS_OK;
}

nsresult
aaStorageTest::testSaveResource(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaISqlTransaction> saver1(do_CreateInstance(
        AA_SAVEMONEY_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(saver1, "'saveMoney' instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> saver2(do_CreateInstance(
        AA_SAVEASSET_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(saver2, "'saveAsset' instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIMoney> node1 = do_CreateInstance(AA_MONEY_CONTRACT);
  NS_ENSURE_TRUE(node1, NS_ERROR_FAILURE);

  nsCOMPtr<aaIAsset> node2 = do_CreateInstance(AA_ASSET_CONTRACT);
  NS_ENSURE_TRUE(node2, NS_ERROR_FAILURE);

  node1->SetNumCode(AA_MONEY_CODE_1);
  node1->SetAlphaCode(NS_LITERAL_STRING(AA_RESOURCE_TAG_1));
  rv = saver1->SetParam(node1);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver1, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveResource' insertion" );

  node2->SetTag(NS_LITERAL_STRING(AA_RESOURCE_TAG_2));
  rv = saver2->SetParam(node2);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver2, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveResource' second insertion" );

  return NS_OK;
}

nsresult
aaStorageTest::testLoadResource(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  mResourceLoader = do_CreateInstance(AA_LOADRESOURCE_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mResourceLoader, "[load resource] instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mSession->Load(mResourceLoader, getter_AddRefs(mResourceSet));
  NS_TEST_ASSERT_MSG(mResourceSet, "[load resource] load" );
  NS_ENSURE_TRUE(mResourceSet, rv);

  PRUint32 count = 0;
  rv = mResourceSet->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 2, "[load resource] wrong item count");

  nsCOMPtr<aaIResource> node = do_QueryElementAt(mResourceSet, 0, &rv);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load resource] 1st item not read" );

  if (node) {
    NS_TEST_ASSERT_MSG(node->PickType() == aaIResource::TYPE_MONEY,
        "[load resource] 1st item type differs from inserted" );
    NS_TEST_ASSERT_MSG(node->PickId() == AA_MONEY_CODE_1,
        "[load resource] 1st item id differs from inserted" );
    nsAutoString tag;
    node->GetTag(tag);
    NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_RESOURCE_TAG_1)),
        "[load resource] 1st item tag differs from inserted" );
  }

  node = do_QueryElementAt(mResourceSet, 1, &rv);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load resource] 2nd item not read" );
  if (node) {
    NS_TEST_ASSERT_MSG(node->PickType() == aaIResource::TYPE_ASSET,
        "[load resource] 2nd item type differs from inserted" );
    NS_TEST_ASSERT_MSG(node->PickId() == 1,
        "[load resource] 2nd item id differs from inserted" );
    nsAutoString tag;
    node->GetTag(tag);
    NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_RESOURCE_TAG_2)),
        "[load resource] 2nd item tag differs from inserted" );
  }

  return NS_OK;
}

nsresult
aaStorageTest::testSaveFlow(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaISqlTransaction> saver
    = do_CreateInstance(AA_SAVEFLOW_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(saver, "'saveflow' instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIFlow> node(do_CreateInstance("@aasii.org/base/flow;1"));
  NS_ENSURE_TRUE(node, NS_ERROR_FAILURE);

  nsCOMPtr<aaIResource> share(do_QueryElementAt(mResourceSet,1));
  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResourceSet,0));

  nsCOMPtr<aaIEntity> entity(do_QueryElementAt(mEntitySet, 1));
  node->SetEntity(entity);
  node->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_1));
  node->SetGiveResource(share);
  node->SetTakeResource(rub);
  node->SetRate(AA_FLOW_SHARE_RATE);
  node->SetLimit(AA_EVENT_AMOUNT_1 / AA_FLOW_SHARE_RATE);
  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveFlow' 1st insertion" );

  entity = do_QueryElementAt(mEntitySet, 2);
  node->SetEntity(entity);
  node->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_3));
  node->SetGiveResource(share);
  node->SetTakeResource(rub);
  node->SetRate(AA_FLOW_SHARE_RATE);
  node->SetLimit(AA_EVENT_AMOUNT_2 / AA_FLOW_SHARE_RATE);
  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveFlow' 2nd insertion" );

  entity = do_QueryElementAt(mEntitySet, 3);
  node->SetEntity(entity);
  node->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_5));
  node->SetGiveResource(rub);
  node->SetTakeResource(rub);
  node->SetRate(1.0);
  node->SetLimit(0.0);
  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'saveFlow' 3rd insertion" );

  return NS_OK;
}

nsresult
aaStorageTest::testLoadFlow(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  mFlowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mFlowLoader, "[load flow] instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlowSet));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load flow] load" );

  nsCOMPtr<aaIFlow> node(do_QueryElementAt(mFlowSet, 0, &rv));
  NS_TEST_ASSERT_MSG(node, "[load flow] first item not read" );
  NS_ENSURE_TRUE(node, NS_ERROR_FAILURE);

  nsAutoString tag;
  node->GetTag(tag);
  nsCAutoString msg("[load flow] first item read differs from inserted: ");
  nsCAutoString tag8 = NS_ConvertUTF16toUTF8(tag);
  msg.Append(tag8);
  NS_TEST_ASSERT_MSG(tag8.Equals(AA_FLOW_TAG_1), msg.get());

  nsCOMPtr<aaIEntity> entity;
  rv = node->GetEntity(getter_AddRefs( entity ));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load flow] flow.entity read" );
  NS_TEST_ASSERT_MSG(entity, "[load flow] flow.entity read is null" );
  if (entity) {
    entity->GetTag(tag);
    NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_ENTITY_TAG_2)),
        "[load flow] flow.entity differs from inserted" );
  }

  nsCOMPtr<aaIResource> res;
  rv = node->GetGiveResource(getter_AddRefs( res ));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv),
      "[load flow] flow.giveResource not read" );
  NS_TEST_ASSERT_MSG(res, "[load flow] flow.giveResource read is null" );
  if (res) {
    res->GetTag(tag);
    NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_RESOURCE_TAG_2)),
        "[load flow] flow.giveResource differs from inserted" );
  }

  rv = node->GetTakeResource(getter_AddRefs( res ));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv),
      "[load flow] flow.takeResource not read" );
  NS_TEST_ASSERT_MSG(res, "[load flow] flow.takeResource read is null" );
  if (res) {
    res->GetTag(tag);
    NS_TEST_ASSERT_MSG(tag.Equals(NS_ConvertUTF8toUTF16(AA_RESOURCE_TAG_1)),
        "[load flow] flow.takeResource differs from inserted" );
  }

  double rate;
  rv = node->GetRate( &rate );
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "'loadFlow' flow.rate not read" );
  NS_TEST_ASSERT_MSG(rate == AA_FLOW_SHARE_RATE,
        "'loadFlow' flow.rate differs from inserted" );

  return NS_OK;
}

nsresult
aaStorageTest::testSaveEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaISqlTransaction> saver
    = do_CreateInstance(AA_SAVEEVENT_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(saver, "'saveEvent' instance creation" );
  NS_ENSURE_TRUE(saver, rv);

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 1 */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(AA_EVENT_AMOUNT_2);
  nsCOMPtr<aaIFlow> flow(do_QueryElementAt(mFlowSet,1));
  fact->SetTakeFrom(flow);
  flow = do_QueryElementAt(mFlowSet,2);
  fact->SetGiveTo(flow);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[insert event] appendElement" );

  rv = saver->SetParam(node);
  NS_TEST_ASSERT_OK(rv);
  rv = mSession->Execute(saver, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[insert event] saving" );

  return NS_OK;
}

nsresult
aaStorageTest::testLoadFlowStates(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<nsIArray> set;
  mStateLoader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(mStateLoader, "[load states] instance creation" );
  NS_ENSURE_TRUE(mStateLoader, rv);

  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[load states] loading" );
  NS_ENSURE_TRUE(set, NS_ERROR_FAILURE);

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 2, "[load states] wrong flow count");

  nsCOMPtr<aaIState> node(do_QueryElementAt(set, 0, &rv));
  NS_TEST_ASSERT_MSG(node, "[load states 1st item not read" );
  NS_ENSURE_TRUE(node, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, node, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE), "[load states] flow2 is wrong");

  nsCOMPtr<aaIFlow> flow;
  nsAutoString tag;
  node->GetFlow(getter_AddRefs( flow ));
  if (flow) {
    flow->GetTag(tag);
    NS_TEST_ASSERT_MSG( tag.Equals(NS_ConvertUTF8toUTF16(AA_FLOW_TAG_3)),
      "[load states] 'fact.flow.tag' is wrong" );
  }

  nsCOMPtr<aaIResource> resource;
  node->GetResource(getter_AddRefs( resource ));
  if (resource) {
    resource->GetTag(tag);
    NS_TEST_ASSERT_MSG( tag.Equals(NS_ConvertUTF8toUTF16(AA_RESOURCE_TAG_2)),
        "[load states] 'state.resource.tag' is wrong" );
  }

  node = do_QueryElementAt(set, 1, &rv);
  NS_TEST_ASSERT_MSG(node, "[load states] 2nd item not read" );
  NS_ENSURE_TRUE(node, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, node, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2), "[load states] flow3 is wrong");

  return NS_OK;
}

nsresult
aaStorageTest::testUpdateEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 2 */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(AA_EVENT_AMOUNT_3);
  nsCOMPtr<aaIFlow> flow(do_QueryElementAt(mFlowSet,0));
  fact->SetTakeFrom(flow);
  flow = do_QueryElementAt(mFlowSet,2);
  fact->SetGiveTo(flow);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[update event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[update event] saving" );

  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[update event] loading states" );

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 3, "[update event] wrong flow count");

  nsCOMPtr<aaIState> state;
  state = do_QueryElementAt(set, 0, &rv);
  NS_TEST_ASSERT_MSG(state, "[update event] 3rd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE), "[update event] flow1 is wrong");

  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[update event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3),
      "[update event] flow3 is wrong");
  return NS_OK;
}

nsresult
aaStorageTest::testReplaceEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<aaIAsset> vaio(do_CreateInstance(AA_ASSET_CONTRACT));
  NS_ENSURE_TRUE(vaio, NS_ERROR_FAILURE);
  vaio->SetTag(NS_LITERAL_STRING(AA_RESOURCE_TAG_3));
  rv = mSession->Save(vaio, nsnull);
  NS_TEST_ASSERT_OK(rv);

  PRInt64 id;
  vaio->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 2, "[replace event] unexpected 'resource.id'");
  NS_ENSURE_TRUE(id == 2, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIEntity> shop(do_CreateInstance(AA_ENTITY_CONTRACT));
  NS_ENSURE_TRUE(shop, NS_ERROR_FAILURE);
  shop->SetTag(NS_LITERAL_STRING(AA_ENTITY_TAG_5));
  rv = mSession->Save(shop, nsnull);
  NS_TEST_ASSERT_OK(rv);

  shop->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 5, "[replace event] unexpected 'entity.id'");
  NS_ENSURE_TRUE(id == 5, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResourceSet,0));

  nsCOMPtr<aaIFlow> purchase(do_CreateInstance("@aasii.org/base/flow;1"));
  NS_ENSURE_TRUE(purchase, NS_ERROR_FAILURE);
  purchase->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_6));
  purchase->SetEntity(shop);
  purchase->SetGiveResource(rub);
  purchase->SetTakeResource(vaio);
  purchase->SetRate(1.0 / AA_EVENT_AMOUNT_4);
  purchase->SetLimit(AA_EVENT_AMOUNT_5);
  rv = mSession->Save(purchase, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[replace event] saving flow");
  NS_ENSURE_SUCCESS(rv, rv);

  purchase->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 4, "[replace event] unexpected 'flow.id'");
  NS_ENSURE_TRUE(id == 4, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-30 */
  PRExplodedTime tm = {0,0,0,12,30,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 3 */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(AA_EVENT_AMOUNT_4);
  nsCOMPtr<aaIFlow> flow(do_QueryElementAt(mFlowSet,2));
  fact->SetTakeFrom(flow);
  fact->SetGiveTo(purchase);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[replace event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[replace event] saving" );

  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[replace event] loading states" );

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 4, "[replace event] wrong flow count");

  nsCOMPtr<aaIState> state;
  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[replace event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4),
      "[replace event] flow3 is wrong");

  state = do_QueryElementAt(set, 3, &rv);
  NS_TEST_ASSERT_MSG(state, "[replace event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 4, 2, AA_EVENT_AMOUNT_5),
      "[replace event] flow4 is wrong");
  return NS_OK;
}

nsresult
aaStorageTest::testDeleteEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  rv = mSession->Load(mResourceLoader, getter_AddRefs(mResourceSet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResourceSet, 0, &rv));
  NS_ENSURE_TRUE(rub, rv);

  PRInt64 id;
  rub->GetId( &id );
  NS_TEST_ASSERT_MSG(id == AA_MONEY_CODE_1,
      "[delete event] unexpected 'resource.id'");
  NS_ENSURE_TRUE(id == AA_MONEY_CODE_1, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIMoney> eur(do_CreateInstance(AA_MONEY_CONTRACT));
  NS_ENSURE_TRUE(eur, NS_ERROR_FAILURE);
  eur->SetAlphaCode(NS_LITERAL_STRING(AA_RESOURCE_TAG_4));
  eur->SetNumCode(AA_MONEY_CODE_2);
  mSession->Save(eur, nsnull);

  eur->GetId( &id );
  NS_TEST_ASSERT_MSG(id == AA_MONEY_CODE_2,
      "[delete event] unexpected 'resource.id'");
  NS_ENSURE_TRUE(id == AA_MONEY_CODE_2, NS_ERROR_UNEXPECTED);

  rv = mSession->Load(mEntityLoader, getter_AddRefs(mEntitySet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIEntity> bank(do_QueryElementAt(mEntitySet, 3, &rv));
  NS_ENSURE_TRUE(bank, rv);

  bank->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 4, "[delete event] unexpected 'entity.id'");
  NS_ENSURE_TRUE(id == 4, NS_ERROR_UNEXPECTED);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlowSet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlowSet,2));

  /* Flow for a forex deal */
  nsCOMPtr<aaIFlow> forex(do_CreateInstance(AA_FLOW_CONTRACT));
  NS_ENSURE_TRUE(forex, NS_ERROR_FAILURE);
  forex->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_7));
  forex->SetEntity(bank);
  forex->SetGiveResource(rub);
  forex->SetTakeResource(eur);
  forex->SetRate(1 / AA_EVENT_RATE_2);
  forex->SetLimit(AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2);
  rv = mSession->Save(forex, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] saving flow");
  NS_ENSURE_SUCCESS(rv, rv);

  forex->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 5, "[delete event] unexpected 'flow.id'");
  NS_ENSURE_TRUE(id == 5, NS_ERROR_UNEXPECTED);

  /* Flow for EUR in the bank */
  nsCOMPtr<aaIFlow> bankAccEur(do_CreateInstance("@aasii.org/base/flow;1"));
  NS_ENSURE_TRUE(bankAccEur, NS_ERROR_FAILURE);
  bankAccEur->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_8));
  bankAccEur->SetEntity(bank);
  bankAccEur->SetGiveResource(eur);
  bankAccEur->SetTakeResource(eur);
  bankAccEur->SetRate(1);
  bankAccEur->SetLimit(0.0);
  rv = mSession->Save(bankAccEur, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] saving flow");
  NS_ENSURE_SUCCESS(rv, rv);

  bankAccEur->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 6, "[delete event] unexpected 'flow.id'");
  NS_ENSURE_TRUE(id == 6, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-30 */
  PRExplodedTime tm = {0,0,0,12,30,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 4 */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(AA_EVENT_AMOUNT_6);
  fact->SetTakeFrom(forex);
  fact->SetGiveTo(bankAccEur);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] saving [2]" );

  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] loading states [2]" );

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 6, "[delete event] wrong flow count");

  nsCOMPtr<aaIState> state;
  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[delete event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4),
      "[delete event] flow3 is wrong");

  state = do_QueryElementAt(set, 4, &rv);
  NS_TEST_ASSERT_MSG(state, "[delete event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 5, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
      "[delete event] flow5 is wrong");

  state = do_QueryElementAt(set, 5, &rv);
  NS_TEST_ASSERT_MSG(state, "[delete event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 6, AA_MONEY_CODE_2,
        AA_EVENT_AMOUNT_6), "[delete event] flow6 is wrong");
  /* Fact 4 is processed correctly */

  node = do_CreateInstance("@aasii.org/base/event;1");
  /* Set time to 2007-08-30 */
  node->SetTime(PR_ImplodeTime(&tm));

  block = do_QueryInterface( node );
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 5 */
  fact = do_CreateInstance("@aasii.org/base/fact;1");
  /* Forex deal is paid correctly */
  fact->SetAmount(AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2);
  fact->SetTakeFrom(bankAccRub);
  fact->SetGiveTo(forex);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] saving" );

  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[delete event] loading states" );

  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 5, "[delete event] wrong flow count");

  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[delete event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
      "[delete event] flow3 is wrong");

  state = do_QueryElementAt(set, 4, &rv);
  NS_TEST_ASSERT_MSG(state, "[delete event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 6, AA_MONEY_CODE_2,
        AA_EVENT_AMOUNT_6), "[delete event] flow6 is wrong");
  return NS_OK;
}

nsresult
aaStorageTest::testStopEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  rv = mSession->Load(mResourceLoader, getter_AddRefs(mResourceSet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIResource> rub(do_QueryElementAt(mResourceSet, 0, &rv));
  NS_ENSURE_TRUE(rub, rv);

  PRInt64 id;
  rub->GetId( &id );
  NS_TEST_ASSERT_MSG(id == AA_MONEY_CODE_1,
      "[stop event] unexpected 'resource.id'");
  NS_ENSURE_TRUE(id == AA_MONEY_CODE_1, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIResource> eur(do_QueryElementAt(mResourceSet, 3, &rv));
  NS_ENSURE_TRUE(eur, NS_ERROR_FAILURE);

  eur->GetId( &id );
  NS_TEST_ASSERT_MSG(id == AA_MONEY_CODE_2,
      "[stop event] unexpected 'resource.id'");
  NS_ENSURE_TRUE(id == AA_MONEY_CODE_2, NS_ERROR_UNEXPECTED);

  rv = mSession->Load(mEntityLoader, getter_AddRefs(mEntitySet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIEntity> bank(do_QueryElementAt(mEntitySet, 3, &rv));
  NS_ENSURE_TRUE(bank, rv);

  bank->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 4, "[stop event] unexpected 'entity.id'");
  NS_ENSURE_TRUE(id == 4, NS_ERROR_UNEXPECTED);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlowSet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlowSet,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlowSet,5));

  /* Flow for a forex deal #2*/
  nsCOMPtr<aaIFlow> forex(do_CreateInstance("@aasii.org/base/flow;1"));
  NS_ENSURE_TRUE(forex, NS_ERROR_FAILURE);
  forex->SetTag(NS_LITERAL_STRING(AA_FLOW_TAG_9));
  forex->SetEntity(bank);
  forex->SetGiveResource(eur);
  forex->SetTakeResource(rub);
  forex->SetRate(AA_EVENT_RATE_3);
  forex->SetLimit(AA_EVENT_AMOUNT_6);
  rv = mSession->Save(forex, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[stop event] saving flow");
  NS_ENSURE_SUCCESS(rv, rv);

  forex->GetId( &id );
  NS_TEST_ASSERT_MSG(id == 7, "[stop event] unexpected 'flow.id'");
  NS_ENSURE_TRUE(id == 7, NS_ERROR_UNEXPECTED);

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-31 */
  PRExplodedTime tm = {0,0,0,12,31,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Fact 6 */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(AA_EVENT_AMOUNT_6);
  fact->SetTakeFrom(bankAccEur);
  fact->SetGiveTo(forex);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[stop event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[stop event] saving" );

  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(set, "[stop event] loading states" );
  NS_ENSURE_TRUE(set, rv);

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 5, "[stop event] wrong flow count");

  nsCOMPtr<aaIState> state;
  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[stop event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
      "[stop event] flow3 is wrong");

  state = do_QueryElementAt(set, 4, &rv);
  NS_TEST_ASSERT_MSG(state, "[stop event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 7, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_3),
      "[stop event] flow7 is wrong");
  /* Forex deal #2 is paid correctly */
  nsCOMPtr<aaISqlFilter> filter
    = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(filter, "[stop event] sql filter instance" );
  NS_ENSURE_TRUE(filter, rv);

  nsCOMPtr<aaIFlow> flow
    = do_CreateInstance(AA_FLOW_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  rv = flow->SetId(6);
  NS_TEST_ASSERT_OK(rv);
  rv = filter->SetInterface(NS_GET_IID(aaIFlow), flow);
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaITimeFrame> frame
    = do_CreateInstance(AA_TIMEFRAME_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  tm.tm_mday = 30;
  rv = frame->SetEnd(PR_ImplodeTime(&tm));
  NS_TEST_ASSERT_OK(rv);
  rv = filter->SetInterface(NS_GET_IID(aaITimeFrame), frame);
  NS_TEST_ASSERT_OK(rv);

  rv = mStateLoader->SetFilter(filter);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(set, "[stop event] loading states" );
  NS_ENSURE_TRUE(set, rv);

  rv = mStateLoader->SetFilter(nsnull);
  NS_TEST_ASSERT_OK(rv);

  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 1, "[stop event] wrong flow count");

  state = do_QueryElementAt(set, 0, &rv);
  NS_TEST_ASSERT_MSG(state, "[stop event] item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 6, AA_MONEY_CODE_2,
        AA_EVENT_AMOUNT_6), "[stop event] flow6 is wrong");
  return NS_OK;
}

nsresult
aaStorageTest::testJunkEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlowSet));
  NS_TEST_ASSERT_OK(rv);

  nsCOMPtr<aaIFlow> bankAccRub(do_QueryElementAt(mFlowSet,2));
  nsCOMPtr<aaIFlow> bankAccEur(do_QueryElementAt(mFlowSet,5));

  nsCOMPtr<aaIEvent> node(do_CreateInstance("@aasii.org/base/event;1"));
  /* Set time to 2007-08-31 */
  PRExplodedTime tm = {0,0,0,12,31,7,2007};
  node->SetTime(PR_ImplodeTime(&tm));

  nsCOMPtr<nsIMutableArray> block(do_QueryInterface( node ));
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  /* Junk Fact */
  nsCOMPtr<aaIFact> fact(do_CreateInstance("@aasii.org/base/fact;1"));
  fact->SetAmount(1);
  fact->SetTakeFrom(bankAccEur);
  fact->SetGiveTo(bankAccRub);

  rv = block->AppendElement(fact, PR_FALSE);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[junk event] appending element" );

  rv = mSession->Save(node, nsnull);
  NS_TEST_ASSERT_MSG(NS_FAILED(rv), "[junk event] saving" );

  nsCOMPtr<nsIArray> set;
  rv = mSession->Load(mStateLoader, getter_AddRefs(set));
  NS_TEST_ASSERT_MSG(set, "[stop event] loading states" );
  NS_ENSURE_TRUE(set, rv);

  PRUint32 count;
  set->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 5, "[stop event] wrong flow count");

  nsCOMPtr<aaIState> state;
  state = do_QueryElementAt(set, 2, &rv);
  NS_TEST_ASSERT_MSG(state, "[stop event] 2nd item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2),
      "[stop event] flow3 is wrong");

  state = do_QueryElementAt(set, 4, &rv);
  NS_TEST_ASSERT_MSG(state, "[stop event] 4th item not read" );
  NS_ENSURE_TRUE(state, NS_ERROR_FAILURE);

  NS_TEST_ASSERT_MSG(testState(aTestRunner, state, 7, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_3),
      "[stop event] flow7 is wrong");

  return NS_OK;
}
