/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASTORAGETEST_H
#define AASTORAGETEST_H 1

#define AA_STORAGE_TEST_CID \
{0x1e7c924e, 0x872d, 0x4c4e, {0xb8, 0x1c, 0x58, 0xd9, 0x66, 0x4a, 0xe9, 0xd1}}
#define AA_STORAGE_TEST_CONTRACT "@aasii.org/storage/unit-core;1"

#include "nsCOMPtr.h"
#include "nsITest.h"

class aaISession;
class aaISqlRequest;
class nsIArray;
#ifdef DEBUG
#include "aaISession.h"
#include "aaISqlRequest.h"
#include "nsIArray.h"
#endif

class aaStorageTest: public nsITest
{
public:
  aaStorageTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaStorageTest() {;}
  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<aaISqlRequest> mEntityLoader;
  nsCOMPtr<nsIArray> mEntitySet;
  nsCOMPtr<aaISqlRequest> mResourceLoader;
  nsCOMPtr<nsIArray> mResourceSet;
  nsCOMPtr<aaISqlRequest> mFlowLoader;
  nsCOMPtr<nsIArray> mFlowSet;
  nsCOMPtr<aaISqlRequest> mStateLoader;

  nsresult testSession(nsITestRunner *aTestRunner);
  nsresult testSaveDispatcher(nsITestRunner *aTestRunner);
  nsresult testFilter(nsITestRunner *aTestRunner);
  nsresult testSaveEntity(nsITestRunner *aTestRunner);
  nsresult testLoadEntity(nsITestRunner *aTestRunner);
  nsresult testSaveResource(nsITestRunner *aTestRunner);
  nsresult testLoadResource(nsITestRunner *aTestRunner);
  nsresult testSaveFlow(nsITestRunner *aTestRunner);
  nsresult testLoadFlow(nsITestRunner *aTestRunner);
  nsresult testSaveEvent(nsITestRunner *aTestRunner);
  nsresult testLoadFlowStates(nsITestRunner *aTestRunner);
  nsresult testUpdateEvent(nsITestRunner *aTestRunner);
  nsresult testReplaceEvent(nsITestRunner *aTestRunner);
  nsresult testDeleteEvent(nsITestRunner *aTestRunner);
  nsresult testStopEvent(nsITestRunner *aTestRunner);
  nsresult testJunkEvent(nsITestRunner *aTestRunner);
};

#endif /* AASTORAGETEST_H */
