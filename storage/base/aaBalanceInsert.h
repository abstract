/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABALANCEINSERT_H
#define AABALANCEINSERT_H 1

#define AA_BALANCE_INSERT_CID \
{0xcaf1eac0, 0x318b, 0x495e, {0x82, 0x1b, 0x2b, 0xc2, 0x44, 0xd7, 0xcb, 0xf2}}

#include "aaSaveRequest.h"

class aaIBalance;
#ifdef DEBUG
#include "aaIBalance.h"
#endif

class aaBalanceInsert : public aaSaveRequest
{
public:
  aaBalanceInsert();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaBalanceInsert() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIBalance> mBalance;
};

#endif /* AABALANCEINSERT_H */
