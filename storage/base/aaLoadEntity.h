/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADENTITY_H
#define AALOADENTITY_H 1

#define AA_LOADENTITY_CID \
{0xc4bf8150, 0x5fe4, 0x409c, {0xbb, 0x30, 0x7b, 0x0f, 0x77, 0xeb, 0x04, 0x0e}}

#include "aaLoadRequest.h"

class aaLoadEntity : public aaLoadRequest
{
public:
  aaLoadEntity();
  NS_DECL_ISUPPORTS_INHERITED

  NS_SCRIPTABLE NS_IMETHOD GetParams(nsIArray * * aParams);
  NS_SCRIPTABLE NS_IMETHOD Accept(aaIHandler *aQuery);

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition);
protected:
  ~aaLoadEntity() {}

private:
  friend class aaStorageTest;
};

#endif /* AALOADENTITY_H */

