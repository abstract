/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaSaveKeys.h"
#include "aaSaveMoney.h"

aaSaveMoney::aaSaveMoney()
{
}

NS_IMPL_ISUPPORTS1(aaSaveMoney,
                   aaISqlTransaction)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveMoney::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mMoney = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveMoney::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mMoney, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;

  PRInt64 id;
  rv = mMoney->GetId(&id);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(id, NS_ERROR_INVALID_ARG);

  rv = insert(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  if (_retval)
    *_retval = nsnull;

  return NS_OK;
}

/* private methods */
nsresult
aaSaveMoney::insert(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mInserter) {
    mResourceInserter = do_CreateInstance(AA_INSERT_RESOURCE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    mInserter = do_CreateInstance(AA_INSERT_MONEY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mInserter->SetFilter(mMoney);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mResourceInserter->SetFilter(mMoney);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mResourceInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
