/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTQUOTE_H
#define AAINSERTQUOTE_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_QUOTE_CID \
{0x68f511a5, 0xb947, 0x40a5, {0xa8, 0xc4, 0x24, 0x2d, 0x21, 0xb9, 0xb8, 0x5f}}

class aaIQuote;
#ifdef DEBUG
#include "aaIQuote.h"
#endif

class aaInsertQuote : public aaSaveRequest
{
public:
  aaInsertQuote();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertQuote() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIQuote> mQuote;
};

#endif /* AAINSERTQUOTE_H */
