/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "aaOrderCondition.h"
#include "aaIColumnNameConverter.h"

NS_IMPL_ISUPPORTS2(aaOrderCondition, aaIOrderCondition, aaICondition)

aaOrderCondition::aaOrderCondition() {
}


NS_IMETHODIMP
  aaOrderCondition::AddOrder(const nsACString & column, PRInt16 direction)
{
  const char* columnData;
  NS_CStringGetData(column, &columnData);
  NS_ENSURE_ARG(direction == aaIOrderCondition::SORT_ASC || direction == aaIOrderCondition::SORT_DESC);
  aaOrderCondition::OrderStruct os;
  os.mDirection = direction;
  os.column = column;
  if (mOrders.AppendElement(os) != nsnull) {
    return NS_OK;
  }
  return NS_ERROR_FAILURE;
}

NS_IMETHODIMP
  aaOrderCondition::GetConditionName(nsACString& condition)
{
  condition.Append(ORDER_CONDITION_NAME);
  return NS_OK;
}

nsCAutoString
  aaOrderCondition::OrderStructToString(const OrderStruct& os, const aaIColumnNameConverter* pConverter) const
{
  nsCAutoString ret_str;
  ret_str.Append(pConverter->convertName(os.column));
  ret_str.Append(" ");
  ret_str.Append(os.mDirection == aaIOrderCondition::SORT_ASC ? "ASC" : "DESC");
  return ret_str;
}

void aaOrderCondition::ToString(const aaIColumnNameConverter * pConverter, nsACString & cond)
{
  if (nsnull == pConverter || mOrders.IsEmpty()) return;
  cond.Assign("ORDER BY ");
  cond.Append(this->OrderStructToString(mOrders[0], pConverter));
  for(nsTArray<OrderStruct>::size_type i = 1; i < mOrders.Length(); ++ i)
  {
    cond.Append(", ");
    cond.Append(this->OrderStructToString(mOrders[i], pConverter));
  }
}
