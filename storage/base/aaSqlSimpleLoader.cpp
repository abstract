/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIArray.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaISqlRequest.h"
#include "aaISqliteChannel.h"
#include "aaSqlSimpleListener.h"
#include "aaSqlSimpleLoader.h"

aaSqlSimpleLoader::aaSqlSimpleLoader()
{
}

NS_IMPL_ISUPPORTS1(aaSqlSimpleLoader,
                   aaISqlTransaction)

/* aaISqlRequest */
NS_IMETHODIMP
aaSqlSimpleLoader::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mRequest = do_QueryInterface(aParam, &rv);
  return rv;
}

NS_IMETHODIMP
aaSqlSimpleLoader::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_ARG_POINTER(aChannel);
  nsresult rv;
  nsCOMPtr<aaILoadObserver> retval = new aaSqlSimpleListener();
  rv = aChannel->Open(mRequest, retval);
  NS_ENSURE_SUCCESS(rv, rv);
  return CallQueryInterface(retval, _retval);
}
