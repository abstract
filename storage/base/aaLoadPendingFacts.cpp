/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIHandler.h"
#include "aaLoadPendingFacts.h"

#define AA_LOADPENDINGFACTS_SELECT_QUERY "SELECT\
    strftime('%s',transfer.day),\
    transfer.event_id,\
    transfer.id,\
    transfer.amount,\
    flowF.id,\
    flowF.tag,\
    flowF.rate,\
    entityF.id,\
    entityF.tag,\
    term.resource_type,\
    term.resource_id,\
    money.alpha_code,\
    asset.tag,\
    termF.resource_type,\
    termF.resource_id,\
    moneyF.alpha_code,\
    assetF.tag,\
    flowT.id,\
    flowT.tag,\
    flowT.rate,\
    entityT.id,\
    entityT.tag,\
    termT.resource_type,\
    termT.resource_id,\
    moneyT.alpha_code,\
    assetT.tag\
  FROM transfer\
    LEFT JOIN txn ON \
      transfer.day == txn.day\
      AND transfer.event_id == txn.event_id\
      AND transfer.id == txn.transfer_id\
    LEFT JOIN fact AS factF ON\
      transfer.day == factF.day\
      AND transfer.event_id  == factF.event_id\
      AND transfer.id == factF.transfer_id\
      AND factF.side == 1\
    LEFT JOIN fact AS factT ON\
      transfer.day == factT.day\
      AND transfer.event_id == factT.event_id\
      AND transfer.id == factT.transfer_id\
      AND factT.side == 0\
    LEFT JOIN flow AS flowF ON\
      factF.flow_id=flowF.id\
    LEFT JOIN flow AS flowT ON\
      factT.flow_id=flowT.id\
    LEFT JOIN entity AS entityF ON\
      entityF.id=flowF.entity_id\
    LEFT JOIN entity AS entityT ON\
      entityT.id=flowT.entity_id\
    LEFT JOIN term AS termF ON\
      termF.flow_id=flowF.id\
      AND termF.side=0\
    LEFT JOIN money AS moneyF ON\
      termF.resource_type=1\
      AND moneyF.id=termF.resource_id\
    LEFT JOIN asset AS assetF ON\
      termF.resource_type=2\
      AND assetF.id=termF.resource_id\
    LEFT JOIN term ON\
      term.flow_id=IFNULL(factF.flow_id,factT.flow_id)\
      AND term.side=IFNULL(factF.side,factT.side)\
    LEFT JOIN money ON\
      term.resource_type=1\
      AND money.id=term.resource_id\
    LEFT JOIN asset ON\
      term.resource_type=2\
      AND asset.id=term.resource_id\
    LEFT JOIN term AS termT ON\
      termT.flow_id=flowT.id\
      AND termT.side=1\
    LEFT JOIN money AS moneyT ON\
      termT.resource_type=1\
      AND moneyT.id=termT.resource_id\
    LEFT JOIN asset AS assetT ON\
      termT.resource_type=2\
      AND assetT.id=termT.resource_id\
    WHERE txn.value IS NULL"

static struct aaLoadRequest::ColumnMap map[] = {
  { "event.time", 0}
  ,{"event.id", 1}
  ,{"id", 2}
  ,{"amount", 3}
  ,{"takeFromFlow.id", 4}
  ,{"takeFromFlow.tag", 5}
  ,{"takeFromFlow.rate", 6}
  ,{"takeFromFlow.entity.id", 7}
  ,{"takeFromFlow.entity.tag", 8}
  ,{"takeFromFlow.give_resource.type", 9}
  ,{"takeFromFlow.give_resource.id", 10}
  ,{"takeFromFlow.give_resource.alpha_code", 11}
  ,{"takeFromFlow.give_resource.tag", 12}
  ,{"takeFromFlow.take_resource.type", 13}
  ,{"takeFromFlow.take_resource.id", 14}
  ,{"takeFromFlow.take_resource.alpha_code", 15}
  ,{"takeFromFlow.take_resource.tag", 16}
  ,{"giveToFlow.id", 17}
  ,{"giveToFlow.tag", 18}
  ,{"giveToFlow.rate", 19}
  ,{"giveToFlow.entity.id", 20}
  ,{"giveToFlow.entity.tag", 21}
  ,{"giveToFlow.give_resource.type", 22}
  ,{"giveToFlow.give_resource.id", 23}
  ,{"giveToFlow.give_resource.alpha_code", 24}
  ,{"giveToFlow.give_resource.tag", 25}
  ,{"giveToFlow.take_resource.type", 9}
  ,{"giveToFlow.take_resource.id", 10}
  ,{"giveToFlow.take_resource.alpha_code", 11}
  ,{"giveToFlow.take_resource.tag", 12}
  ,{0, 0}
};

aaLoadPendingFacts::aaLoadPendingFacts()
{
  mSql = AA_LOADPENDINGFACTS_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadPendingFacts,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadPendingFacts::GetParams(nsIArray * * aParams)
{
  NS_ENSURE_ARG_POINTER(aParams);
  *aParams = nsnull;
  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadPendingFacts::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleFact(nsnull);
}
