/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEINCOME_H
#define AASAVEINCOME_H 1

#define AA_SAVEINCOME_CID \
{0x34b20951, 0xa2b3, 0x4082, {0x8a, 0xef, 0xcc, 0xd7, 0x89, 0x9b, 0x95, 0xab}}

#include "aaISqlTransaction.h"
#include "aaIStorager.h"

class aaIBalance;
class aaISqlRequest;
#ifdef DEBUG
#include "mozIStorageConnection.h"
#include "aaIBalance.h"
#include "aaISqlRequest.h"
#endif

class aaSaveIncome : public aaISqlTransaction,
                     public aaIStorager
{
public:
  aaSaveIncome();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION
  NS_DECL_AAISTORAGER

protected:
  ~aaSaveIncome() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIBalance> mIncome;
  nsCOMPtr<aaISqlRequest> mIncomeInserter;
  nsCOMPtr<aaISqlRequest> mIncomeUpdater;
  nsCOMPtr<aaISqlRequest> mIncomeCloser;
  nsCOMPtr<aaISqlRequest> mIncomeDeleter;
};

#endif /* AASAVEINCOME_H */
