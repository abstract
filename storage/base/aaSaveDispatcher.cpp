/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsISupportsUtils.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIRule.h"
#include "aaIEvent.h"
#include "aaIQuote.h"
#include "aaITransaction.h"
#include "aaIChart.h"
#include "aaISession.h"
#include "aaISqlTransaction.h"
#include "aaSaveKeys.h"
#include "aaSaveDispatcher.h"

nsresult
aaSaveDispatcher::Init(nsISupports *aSession)
{
  nsresult rv;
  rv = CallQueryInterface(aSession, &mParentSession);
  NS_ENSURE_SUCCESS(rv, rv);
  mParentSession->Release();
  return NS_OK;
}

NS_IMPL_ISUPPORTS3(aaSaveDispatcher,
                   aaISaveQuery,
                   aaIHandler,
                   aaIChartHandler)

/* aaISaveQuery */
NS_IMETHODIMP
aaSaveDispatcher::Save(aaIDataNode *aNode, aaIDataNode *aOldNode)
{ 
  NS_ENSURE_ARG_POINTER(aNode);
  mOldNode = aOldNode;
  return aNode->Accept(this);
}

/* aaIHandler */
NS_IMETHODIMP
aaSaveDispatcher::HandleNode(aaIListNode *aNode)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSaveDispatcher::HandleEntity(aaIEntity *aEntity)
{
  nsresult rv;
  if (!mEntitySync) {
    mEntitySync = do_CreateInstance(AA_SAVEENTITY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mEntitySync->SetParam(aEntity);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mEntitySync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleResource(aaIResource *aResource)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSaveDispatcher::HandleMoney(aaIMoney *aMoney)
{
  nsresult rv;
  if (!mMoneySync) {
    mMoneySync = do_CreateInstance(AA_SAVEMONEY_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mMoneySync->SetParam(aMoney);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mMoneySync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleAsset(aaIAsset *aAsset)
{
  nsresult rv;
  if (!mAssetSync) {
    mAssetSync = do_CreateInstance(AA_SAVEASSET_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mAssetSync->SetParam(aAsset);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mAssetSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleFlow(aaIFlow *aFlow)
{
  nsresult rv;
  if (!mFlowSync) {
    mFlowSync = do_CreateInstance(AA_SAVEFLOW_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mFlowSync->SetParam(aFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mFlowSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleRule(aaIRule *aRule)
{
  nsresult rv;
  if (!mRuleSync) {
    mRuleSync = do_CreateInstance(AA_SAVENORM_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mRuleSync->SetParam(aRule);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mRuleSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleState(aaIState *aState)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSaveDispatcher::HandleFact(aaIFact *aFact)
{
  return NS_OK;
}

NS_IMETHODIMP
aaSaveDispatcher::HandleEvent(aaIEvent *aEvent)
{
  nsresult rv;
  if (!mEventSync) {
    mEventSync = do_CreateInstance(AA_SAVEEVENT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mEventSync->SetParam(aEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mEventSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleQuote(aaIQuote *aQuote)
{
  nsresult rv;
  if (!mQuoteSync) {
    mQuoteSync = do_CreateInstance(AA_SAVEQUOTE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mQuoteSync->SetParam(aQuote);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mQuoteSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleTransaction(aaITransaction *aTransaction)
{
  nsresult rv;
  if (!mTransactionSync) {
    mTransactionSync = do_CreateInstance(AA_SAVETRANSACTION_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mTransactionSync->SetParam(aTransaction);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mTransactionSync, nsnull);
}

/* aaIHandler */
NS_IMETHODIMP
aaSaveDispatcher::HandleChart(aaIChart *aChart)
{
  nsresult rv;
  if (!mChartSync) {
    mChartSync = do_CreateInstance(AA_SAVECHART_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mChartSync->SetParam(aChart);
  NS_ENSURE_SUCCESS(rv, rv);

  return mParentSession->Execute(mChartSync, nsnull);
}

NS_IMETHODIMP
aaSaveDispatcher::HandleBalance(aaIBalance *aBalance)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSaveDispatcher::HandleIncome(aaIBalance *aIncome)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
