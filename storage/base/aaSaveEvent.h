/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVEEVENT_H
#define AASAVEEVENT_H 1

#define AA_SAVEEVENT_CID \
{0xc1b3d0a4, 0x9286, 0x42aa, {0xa1, 0x0f, 0x5c, 0x72, 0xa9, 0x7a, 0x04, 0x90}}

#include "aaISqlTransaction.h"
#include "aaIStorager.h"

class aaIEvent;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIEvent.h"
#include "aaISqlRequest.h"
#include "mozIStorageConnection.h"
#endif

class aaSaveEvent : public aaISqlTransaction,
                    public aaIStorager
{
public:
  aaSaveEvent();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION
  NS_DECL_AAISTORAGER

private:
  ~aaSaveEvent() {}
  friend class aaStorageTest;

  nsCOMPtr<aaIEvent> mEvent;
  nsCOMPtr<aaISqlRequest> mEventInserter;
  nsCOMPtr<aaISqlRequest> mEventIdLoader;
  nsCOMPtr<aaISqlRequest> mStateLoader;
  nsCOMPtr<aaISqlRequest> mTransferInserter;
  nsCOMPtr<aaISqlRequest> mTransferIdLoader;
  nsCOMPtr<aaISqlRequest> mSideInserter;
  nsCOMPtr<aaISqlTransaction> mStateInserter;
  nsCOMPtr<aaISqlRequest> mFactLoader;
  nsCOMPtr<aaISqlTransaction> mRuleLoader;
  nsCOMPtr<aaISqlRequest> mEventDeleter;
  nsCOMPtr<aaISqlRequest> mTransferDeleter;
  nsCOMPtr<aaISqlRequest> mSideDeleter;

  nsresult insertEvent(aaISqliteChannel *aChannel);
  nsresult processFact(aaISqliteChannel *aChannel, PRUint32 aIndex);
  nsresult loadFlowState(aaISqliteChannel *aChannel, aaIFlow *aFlow,
      aaIState * * aState);
  nsresult insertTransfer(aaISqliteChannel *aChannel, aaIFact *aFact);
  nsresult loadRules(aaISqliteChannel *aChannel, aaIFact *aFact);
  nsresult insertSide(aaISqliteChannel *aChannel, aaIState *aState);
  nsresult insertState(aaISqliteChannel *aChannel, aaIState *aState);
  nsresult setStates(aaISqliteChannel *aChannel, aaIFact *aFact, PRBool insert);

  nsresult cleanEvent(aaISqliteChannel *aChannel);
  nsresult deleteSide(aaISqliteChannel *aChannel);
  nsresult deleteTransfer(aaISqliteChannel *aChannel);
  nsresult deleteEvent(aaISqliteChannel *aChannel);
};

#endif /* AASAVEEVENT_H */
