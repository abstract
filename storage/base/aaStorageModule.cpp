/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIGenericFactory.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"

/* Project includes */
#include "aaParamFactory.h"
#include "aaIFlow.h"
#include "aaSessionUtils.h"
#include "aaSession.h"
#include "aaManager.h"
#include "aaSaveKeys.h"
#include "aaBaseLoaders.h"
#include "aaSaveDispatcher.h"
#include "aaLoadBuilder.h"
#include "aaSqlSimpleLoader.h"
#include "aaSqlSimpleListener.h"
#include "aaSqlIdListener.h"
#include "aaLoadLastId.h"
#include "aaSqlFilter.h"
#include "aaSaveEntity.h"
#include "aaInsertEntity.h"
#include "aaUpdateEntity.h"
#include "aaLoadEntity.h"
#include "aaSaveAsset.h"
#include "aaInsertResource.h"
#include "aaInsertAsset.h"
#include "aaUpdateAsset.h"
#include "aaInsertMoney.h"
#include "aaSaveMoney.h"
#include "aaLoadResource.h"
#include "aaInsertFlow.h"
#include "aaInsertTerm.h"
#include "aaSaveFlow.h"
#include "aaLoadFlow.h"
#include "aaNormInsert.h"
#include "aaClauseInsert.h"
#include "aaSaveNorm.h"
#include "aaRuleGet.h"
#include "aaLoadRule.h"
#include "aaStateInsert.h"
#include "aaStateUpdate.h"
#include "aaStateDelete.h"
#include "aaStateClose.h"
#include "aaSaveState.h"
#include "aaEventInsert.h"
#include "aaEventDelete.h"
#include "aaTransferInsert.h"
#include "aaTransferDelete.h"
#include "aaFactSideInsert.h"
#include "aaFactSideDelete.h"
#include "aaSaveEvent.h"
#include "aaLoadFlowStates.h"
#include "aaBalanceInsert.h"
#include "aaBalanceUpdate.h"
#include "aaBalanceDelete.h"
#include "aaBalanceClose.h"
#include "aaSaveBalance.h"
#include "aaLoadBalance.h"
#include "aaLoadPendingFacts.h"
#include "aaLoadFactList.h"
#include "aaIncomeInsert.h"
#include "aaIncomeUpdate.h"
#include "aaIncomeClose.h"
#include "aaIncomeDelete.h"
#include "aaSaveIncome.h"
#include "aaLoadIncome.h"
#include "aaCalcDiff.h"
#include "aaInsertQuote.h"
#include "aaSaveQuote.h"
#include "aaLoadQuote.h"
#include "aaInsertChart.h"
#include "aaSaveChart.h"
#include "aaLoadChart.h"
#include "aaTransactionInsert.h"
#include "aaSaveTransaction.h"

///Conditions
#include "aaConditionKeys.h"
#include "aaOrderCondition.h"
#include "aaFilterCondition.h"

/* XXX _acc This belongs to a separate module */
#include "aaAccountLoaders.h"

NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaSession)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaManager)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaSaveDispatcher)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadBuilder)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqlSimpleLoader)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqlSimpleListener)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaSqlIdListener)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadLastId)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqlFilter)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaUpdateEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertResource)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertAsset)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaUpdateAsset)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveAsset)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertMoney)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveMoney)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadResource)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertTerm)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaNormInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaClauseInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveNorm)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaRuleGet)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadRule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaStateInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaStateUpdate)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaStateDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaStateClose)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveState)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaEventInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaEventDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTransferInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTransferDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaFactSideInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaFactSideDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveEvent)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadFlowStates)

/********* Conditions *************************/
NS_GENERIC_FACTORY_CONSTRUCTOR(aaOrderCondition)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaFilterCondition)

/* XXX _acc This belongs to a separate module */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceUpdate)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceClose)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveBalance)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadBalance)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadPendingFacts)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadFactList)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaCalcDiff)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertQuote)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveQuote)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadQuote)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaInsertChart)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveChart)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadChart)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTransactionInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveTransaction)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncomeInsert)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncomeUpdate)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncomeClose)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncomeDelete)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSaveIncome)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaLoadIncome)

static const nsModuleComponentInfo kComponents[] =
{
  { "Session",
    AA_SESSION_CID,
    AA_SESSION_CONTRACT,
    aaSessionConstructor }
  ,{"Session Manager",
    AA_MANAGER_CID,
    AA_MANAGER_CONTRACT,
    aaManagerConstructor }
  ,{"SaveDispatcher",
    AA_SAVEDISPATCHER_CID,
    AA_SAVEDISPATCHER_CONTRACT,
    aaSaveDispatcherConstructor }
  ,{"LoadBuilder",
    AA_LOADBUILDER_CID,
    AA_LOADBUILDER_CONTRACT,
    aaLoadBuilderConstructor }
  ,{"Simple SQL Load Transaction",
    AA_SQLSIMPLELOADER_CID,
    AA_SQLSIMPLELOADER_CONTRACT,
    aaSqlSimpleLoaderConstructor }
  ,{"Simple SQL Request Listener",
    AA_SQLSIMPLELISTENER_CID,
    AA_SQLSIMPLELISTENER_CONTRACT,
    aaSqlSimpleListenerConstructor }
  ,{"ListNode Id SQL Request Listener",
    AA_SQLIDLISTENER_CID,
    AA_SQLIDLISTENER_CONTRACT,
    aaSqlIdListenerConstructor }
  ,{"Load Last Insert Id Query",
    AA_LOADNODE_CID,
    AA_LOADLASTID_CONTRACT,
    aaLoadLastIdConstructor }
  ,{"Basic SQL Filter",
    AA_SQLFILTER_CID,
    AA_SQLFILTER_CONTRACT,
    aaSqlFilterConstructor }
  ,{"Save Entity Query",
    AA_SAVEENTITY_CID,
    AA_SAVEENTITY_CONTRACT,
    aaSaveEntityConstructor }
  ,{"Load Entity Query",
    AA_LOADENTITY_CID,
    AA_LOADENTITY_CONTRACT,
    aaLoadEntityConstructor }
  ,{"Save Asset Query",
    AA_SAVEASSET_CID,
    AA_SAVEASSET_CONTRACT,
    aaSaveAssetConstructor }
  ,{"Save Money Query",
    AA_SAVEMONEY_CID,
    AA_SAVEMONEY_CONTRACT,
    aaSaveMoneyConstructor }
  ,{"Load Resource Query",
    AA_LOADRESOURCE_CID,
    AA_LOADRESOURCE_CONTRACT,
    aaLoadResourceConstructor }
  ,{"Save Flow Query",
    AA_SAVEFLOW_CID,
    AA_SAVEFLOW_CONTRACT,
    aaSaveFlowConstructor }
  ,{"Load Flow Query",
    AA_LOADFLOW_CID,
    AA_LOADFLOW_CONTRACT,
    aaLoadFlowConstructor }
  ,{"Save Norm Query",
    AA_SAVENORM_CID,
    AA_SAVENORM_CONTRACT,
    aaSaveNormConstructor }
  ,{"Load Rule Query",
    AA_LOADRULE_CID,
    AA_LOADRULE_CONTRACT,
    aaLoadRuleConstructor }
  ,{"Save State Query",
    AA_SAVESTATE_CID,
    AA_SAVESTATE_CONTRACT,
    aaSaveStateConstructor }
  ,{"Save Event Query",
    AA_SAVEEVENT_CID,
    AA_SAVEEVENT_CONTRACT,
    aaSaveEventConstructor }
  ,{"Load Flow States Query",
    AA_LOADFLOWSTATES_CID,
    AA_LOADFLOWSTATES_CONTRACT,
    aaLoadFlowStatesConstructor }

/* XXX _acc This belongs to a separate module */
  ,{"Insert Entity Request",
    AA_INSERT_ENTITY_CID,
    AA_INSERT_ENTITY_CONTRACT,
    aaInsertEntityConstructor }
  ,{"Update Entity Request",
    AA_UPDATE_ENTITY_CID,
    AA_UPDATE_ENTITY_CONTRACT,
    aaUpdateEntityConstructor }
  ,{"Insert Resource Request",
    AA_INSERT_RESOURCE_CID,
    AA_INSERT_RESOURCE_CONTRACT,
    aaInsertResourceConstructor }
  ,{"Insert Asset Request",
    AA_INSERT_ASSET_CID,
    AA_INSERT_ASSET_CONTRACT,
    aaInsertAssetConstructor }
  ,{"Update Asset Request",
    AA_UPDATE_ASSET_CID,
    AA_UPDATE_ASSET_CONTRACT,
    aaUpdateAssetConstructor }
  ,{"Insert Money Request",
    AA_INSERT_MONEY_CID,
    AA_INSERT_MONEY_CONTRACT,
    aaInsertMoneyConstructor }
  ,{"Insert Flow Request",
    AA_INSERT_FLOW_CID,
    AA_INSERT_FLOW_CONTRACT,
    aaInsertFlowConstructor }
  ,{"Insert Term Request",
    AA_INSERT_TERM_CID,
    AA_INSERT_TERM_CONTRACT,
    aaInsertTermConstructor }
  ,{"Insert Norm Request",
    AA_NORM_INSERT_CID,
    AA_NORM_INSERT_CONTRACT,
    aaNormInsertConstructor }
  ,{"Insert Clause Request",
    AA_CLAUSE_INSERT_CID,
    AA_CLAUSE_INSERT_CONTRACT,
    aaClauseInsertConstructor }
  ,{"Get Rule Request",
    AA_RULE_GET_CID,
    AA_RULE_GET_CONTRACT,
    aaRuleGetConstructor }
  ,{"Insert State Request",
    AA_STATE_INSERT_CID,
    AA_STATE_INSERT_CONTRACT,
    aaStateInsertConstructor }
  ,{"Update State Request",
    AA_STATE_UPDATE_CID,
    AA_STATE_UPDATE_CONTRACT,
    aaStateUpdateConstructor }
  ,{"Delete State Request",
    AA_STATE_DELETE_CID,
    AA_STATE_DELETE_CONTRACT,
    aaStateDeleteConstructor }
  ,{"Close State Request",
    AA_STATE_CLOSE_CID,
    AA_STATE_CLOSE_CONTRACT,
    aaStateCloseConstructor }
  ,{"Insert Event Request",
    AA_EVENT_INSERT_CID,
    AA_EVENT_INSERT_CONTRACT,
    aaEventInsertConstructor }
  ,{"Delete Event Request",
    AA_EVENT_DELETE_CID,
    AA_EVENT_DELETE_CONTRACT,
    aaEventDeleteConstructor }
  ,{"Insert Transfer Request",
    AA_TRANSFER_INSERT_CID,
    AA_TRANSFER_INSERT_CONTRACT,
    aaTransferInsertConstructor }
  ,{"Delete Transfer Request",
    AA_TRANSFER_DELETE_CID,
    AA_TRANSFER_DELETE_CONTRACT,
    aaTransferDeleteConstructor }
  ,{"Insert Fact Side Request",
    AA_FACT_SIDE_INSERT_CID,
    AA_FACT_SIDE_INSERT_CONTRACT,
    aaFactSideInsertConstructor }
  ,{"Delete Fact Side Request",
    AA_FACT_SIDE_DELETE_CID,
    AA_FACT_SIDE_DELETE_CONTRACT,
    aaFactSideDeleteConstructor }
  ,{"Insert Chart Request",
    AA_INSERT_CHART_CID,
    AA_INSERT_CHART_CONTRACT,
    aaInsertChartConstructor }
  ,{"Insert Quote Request",
    AA_INSERT_QUOTE_CID,
    AA_INSERT_QUOTE_CONTRACT,
    aaInsertQuoteConstructor }
  ,{"Insert Income Request",
    AA_INCOME_INSERT_CID,
    AA_INCOME_INSERT_CONTRACT,
    aaIncomeInsertConstructor }
  ,{"Update Income Request",
    AA_INCOME_UPDATE_CID,
    AA_INCOME_UPDATE_CONTRACT,
    aaIncomeUpdateConstructor }
  ,{"Close Income Request",
    AA_INCOME_CLOSE_CID,
    AA_INCOME_CLOSE_CONTRACT,
    aaIncomeCloseConstructor }
  ,{"Delete Income Request",
    AA_INCOME_DELETE_CID,
    AA_INCOME_DELETE_CONTRACT,
    aaIncomeDeleteConstructor }
  ,{"Insert Balance Request",
    AA_BALANCE_INSERT_CID,
    AA_BALANCE_INSERT_CONTRACT,
    aaBalanceInsertConstructor }
  ,{"Update Balance Request",
    AA_BALANCE_UPDATE_CID,
    AA_BALANCE_UPDATE_CONTRACT,
    aaBalanceUpdateConstructor }
  ,{"Delete Balance Request",
    AA_BALANCE_DELETE_CID,
    AA_BALANCE_DELETE_CONTRACT,
    aaBalanceDeleteConstructor }
  ,{"Close Balance Request",
    AA_BALANCE_CLOSE_CID,
    AA_BALANCE_CLOSE_CONTRACT,
    aaBalanceCloseConstructor }
  ,{"Insert Transaction Request",
    AA_TRANSACTION_INSERT_CID,
    AA_TRANSACTION_INSERT_CONTRACT,
    aaTransactionInsertConstructor }

/* XXX _acc This belongs to a separate module */
  ,{"Save Balance Query",
    AA_SAVEBALANCE_CID,
    AA_SAVEBALANCE_CONTRACT,
    aaSaveBalanceConstructor }
  ,{"Load Balance Query",
    AA_LOADBALANCE_CID,
    AA_LOADBALANCE_CONTRACT,
    aaLoadBalanceConstructor }
  ,{"Load Pending Facts Query",
    AA_LOADPENDINGFACTS_CID,
    AA_LOADPENDINGFACTS_CONTRACT,
    aaLoadPendingFactsConstructor }
  ,{"Load Facts Query",
    AA_LOADFACTLIST_CID,
    AA_LOADFACTLIST_CONTRACT,
    aaLoadFactListConstructor }
  ,{"Calculate Quote Diff Query",
    AA_CALCDIFF_CID,
    AA_CALCDIFF_CONTRACT,
    aaCalcDiffConstructor }
  ,{"Save Quote Query",
    AA_SAVEQUOTE_CID,
    AA_SAVEQUOTE_CONTRACT,
    aaSaveQuoteConstructor }
  ,{"Load Quote Query",
    AA_LOADQUOTE_CID,
    AA_LOADQUOTE_CONTRACT,
    aaLoadQuoteConstructor }
  ,{"Save Chart Query",
    AA_SAVECHART_CID,
    AA_SAVECHART_CONTRACT,
    aaSaveChartConstructor }
  ,{"Load Chart Query",
    AA_LOADCHART_CID,
    AA_LOADCHART_CONTRACT,
    aaLoadChartConstructor }
  ,{"Save Transaction Query",
    AA_SAVETRANSACTION_CID,
    AA_SAVETRANSACTION_CONTRACT,
    aaSaveTransactionConstructor }
  ,{"Save Income Query",
    AA_SAVEINCOME_CID,
    AA_SAVEINCOME_CONTRACT,
    aaSaveIncomeConstructor }
  ,{"Load Income Query",
    AA_LOADINCOME_CID,
    AA_LOADINCOME_CONTRACT,
    aaLoadIncomeConstructor }

  /******** Conditions *********/
  ,{"Order Condition",
    AA_ORDER_CONDITION_ID,
    AA_ORDER_CONDITION_CONTRACTID,
    aaOrderConditionConstructor}
  ,{"Filter Condition",
    AA_FILTER_CONDITION_ID,
    AA_FILTER_CONDITION_CONTRACTID,
    aaFilterConditionConstructor}
};
NS_IMPL_NSGETMODULE(aastorage, kComponents)

