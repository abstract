/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADLASTID_H
#define AALOADLASTID_H 1

#include "aaIDataNode.h"
#include "aaISqlRequest.h"

#define AA_LOADNODE_CID \
{0x67ee9859, 0xed94, 0x4468, {0x98, 0x03, 0xac, 0x5d, 0x83, 0x78, 0x4e, 0x32}}

class nsIInterfaceRequestor;
class mozIStorageStatement;
#ifdef DEBUG
#include "nsIInterfaceRequestor.h"
#include "mozIStorageStatement.h"
#endif

class aaLoadLastId : public aaISqlRequest,
                     public aaIDataNode
{
public:
  aaLoadLastId();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLREQUEST
  NS_DECL_AAIDATANODE

protected:
  ~aaLoadLastId() {}

  nsCAutoString mSql;
  nsCOMPtr<mozIStorageStatement> mStatement;

private:
  friend class aaStorageTest;
};

#endif /* AALOADLASTID */
