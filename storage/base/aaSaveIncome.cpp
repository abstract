/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIBalance.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveIncome.h"

aaSaveIncome::aaSaveIncome()
{
}

NS_IMPL_ISUPPORTS2(aaSaveIncome,
                   aaISqlTransaction,
                   aaIStorager)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveIncome::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mIncome = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveIncome::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mIncome, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER(mIncome->PickFlow());

  return mIncome->Sync(this, aChannel);
}

/* aaIStorager */
NS_IMETHODIMP
aaSaveIncome::Insert(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mIncomeInserter) {
    mIncomeInserter = do_CreateInstance(AA_INCOME_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncomeInserter->SetFilter(mIncome);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveIncome::Update(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mIncomeUpdater) {
    mIncomeUpdater = do_CreateInstance(AA_INCOME_UPDATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncomeUpdater->SetFilter(mIncome);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeUpdater, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveIncome::Close(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mIncomeCloser) {
    mIncomeCloser = do_CreateInstance(AA_INCOME_CLOSE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncomeCloser->SetFilter(mIncome);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeCloser, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveIncome::Delete(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mIncomeDeleter) {
    mIncomeDeleter = do_CreateInstance(AA_INCOME_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncomeDeleter->SetFilter(mIncome);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mIncomeDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
