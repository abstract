/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADBUILDER_H
#define AALOADBUILDER_H 1

#include "aaIChartHandler.h"

class aaSession;
class aaISqlResult;
class aaILoadObserver;

class aaIDataNode;
class mozIStorageValueArray;
#ifdef DEBUG
#include "aaIDataNode.h"
#include "mozIStorageValueArray.h"
#endif

#define AA_LOADBUILDER_CID \
{0xb1be92d1, 0xdd70, 0x496a, {0xaa, 0xa9, 0xa4, 0x0f, 0x7e, 0x5b, 0x94, 0x12}}

#define AA_BUILD_BUFFER_SIZE 1024

class aaLoadBuilder : public aaIChartHandler
{
public:
  aaLoadBuilder() :mIndex(0) {}
  NS_DECL_ISUPPORTS
  NS_DECL_AAIHANDLER
  NS_DECL_AAICHARTHANDLER

  nsresult Load(aaISqlResult *result, aaIDataNode **_retval);
private:
  virtual ~aaLoadBuilder() {}
  aaISqlResult *mData;
  char mPrefix[AA_BUILD_BUFFER_SIZE];
  PRUint32 mIndex;
  nsCOMPtr<aaIDataNode> mResult;

  class PrefixStacker {
  public:
    PrefixStacker(aaLoadBuilder *parent)
      :mParent(parent), mIndex(parent ? parent->mIndex : 0) {}
    ~PrefixStacker()
    { ResetPrefix(); mParent = nsnull; }
    PRUint32 GetIndex() const { return mIndex; }
    void ResetPrefix()
    {
      if (mParent) {
        mParent->mPrefix[mIndex] = 0;
        mParent->mIndex = mIndex;
        mParent->mResult = nsnull;
      }
    }
  private:
    aaLoadBuilder *mParent;
    PRUint32 mIndex;
  };

  nsresult appendPrefix(const char *aElement);
  PRUint32 getColumn(const char *aElement);
};

#endif /* AALOADBUILDER_H */

