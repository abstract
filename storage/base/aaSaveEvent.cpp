/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsArrayUtils.h"
#include "nsComponentManagerUtils.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIEvent.h"
#include "aaIState.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveEvent.h"

/*************** aaSaveEvent **********************/
aaSaveEvent::aaSaveEvent()
{
}

NS_IMPL_ISUPPORTS2(aaSaveEvent,
                   aaISqlTransaction,
                   aaIStorager)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveEvent::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mEvent = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveEvent::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mEvent, NS_ERROR_NOT_INITIALIZED);

  return mEvent->Sync(this, aChannel);
}

/* aaIStorager */
NS_IMETHODIMP
aaSaveEvent::Insert(aaISqliteChannel *aChannel)
{
  nsresult rv;

  rv = insertEvent(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> block(do_QueryInterface(mEvent));
  NS_ENSURE_ARG_POINTER(block);

  PRUint32 i = 0, count = 0;
  rv = block->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(count, NS_ERROR_INVALID_ARG);

  while (i < count) {
    rv = processFact(aChannel, i);
    if (NS_FAILED(rv)) return rv;
    i++;
    rv = block->GetLength(&count);
    NS_ENSURE_SUCCESS(rv, rv);
    NS_ENSURE_TRUE(count, NS_ERROR_INVALID_ARG);
  }

  return NS_OK;
}

NS_IMETHODIMP
aaSaveEvent::Update(aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaSaveEvent::Close(aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaSaveEvent::Delete(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mFactLoader) {
    mFactLoader = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<aaILoadObserver> factHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mFactLoader->SetFilter(mEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mFactLoader, factHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> factSet = do_QueryInterface(factHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 i, count = 0;
  rv = factSet->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  for (i = 0; i < count; i++) {
    nsCOMPtr<aaIFact> fact = do_QueryElementAt(factSet, i, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = fact->SetAmount(-fact->PickAmount());
    NS_ENSURE_SUCCESS(rv, rv);

    rv = setStates(aChannel, fact, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = cleanEvent(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* Private methods */
nsresult
aaSaveEvent::insertEvent(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mEventInserter) {
    mEventIdLoader = do_CreateInstance(AA_LOADLASTID_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsISupportsCString> filter
      = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = filter->SetData(NS_LITERAL_CSTRING("event"));
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = mEventIdLoader->SetFilter(filter);
    NS_ENSURE_SUCCESS(rv, rv);

    mEventInserter = do_CreateInstance(AA_EVENT_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mEventInserter->SetFilter(mEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mEventInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> id
    = do_CreateInstance(AA_SQLIDLISTENER_CONTRACT, mEvent, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mEventIdLoader, id);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::processFact(aaISqliteChannel *aChannel, PRUint32 aIndex)
{
  nsresult rv;

  nsCOMPtr<nsIArray> block(do_QueryInterface(mEvent));
  NS_ENSURE_ARG_POINTER(block);

  nsCOMPtr<aaIFact> fact = do_QueryElementAt(block, aIndex, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(fact, NS_ERROR_INVALID_ARG);

  rv = insertTransfer(aChannel, fact);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = loadRules(aChannel, fact);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = setStates(aChannel, fact, PR_TRUE);
  if (NS_FAILED(rv)) return rv;

  return NS_OK;
}

nsresult
aaSaveEvent::setStates(aaISqliteChannel *aChannel, aaIFact *aFact, PRBool insert)
{
  nsresult rv;
  nsCOMPtr<aaIState> stateFrom, stateTo;

  rv = loadFlowState(aChannel, aFact->PickTakeFrom(),
      getter_AddRefs(stateFrom));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aFact->InitStateFrom(stateFrom);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = loadFlowState(aChannel, aFact->PickGiveTo(), getter_AddRefs(stateTo));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aFact->InitStateTo(stateTo);
  if (NS_FAILED(rv)) return rv;

  rv = aFact->GetStateFrom(getter_AddRefs(stateFrom));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aFact->GetStateTo(getter_AddRefs(stateTo));
  NS_ENSURE_SUCCESS(rv, rv);

  if (insert) {
    rv = insertSide(aChannel, stateFrom);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = insertSide(aChannel, stateTo);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = insertState(aChannel, stateFrom);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = insertState(aChannel, stateTo);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::loadFlowState(aaISqliteChannel *aChannel,
    aaIFlow *aFlow, aaIState * * aState)
{
  NS_ENSURE_ARG_POINTER(aState);
  if (!aFlow) {
    *aState = nsnull;
    return NS_OK;
  }

  nsresult rv;
  if (!mStateLoader) {
    mStateLoader = do_CreateInstance(AA_LOADFLOWSTATES_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  nsCOMPtr<aaILoadObserver> stateHolder
    = do_CreateInstance(AA_SQLSIMPLELISTENER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mStateLoader->SetFilter(aFlow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mStateLoader, stateHolder);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIArray> stateSet = do_QueryInterface(stateHolder, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 count;
  rv = stateSet->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_ENSURE_TRUE(count <= 1, NS_ERROR_UNEXPECTED);

  if (!count) {
    *aState = nsnull;
    return NS_OK;
  }

  rv = stateSet->QueryElementAt(0, NS_GET_IID(aaIState), (void **) aState);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::loadRules(aaISqliteChannel *aChannel, aaIFact *aFact)
{
  nsresult rv;
  if (!mRuleLoader) {
    mRuleLoader = do_CreateInstance(AA_LOADRULE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsCOMPtr<aaIFlow> flowFrom = aFact->PickTakeFrom();
  nsCOMPtr<aaIFlow> flowTo = aFact->PickGiveTo();

  if (flowFrom && flowFrom->PickId()) {
    rv = mRuleLoader->SetParam(flowFrom);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mRuleLoader->Execute(aChannel, nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (flowTo && flowTo->PickId()) {
    rv = mRuleLoader->SetParam(flowTo);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mRuleLoader->Execute(aChannel, nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSaveEvent::insertTransfer(aaISqliteChannel *aChannel, aaIFact *aFact)
{
  nsresult rv;
  if (!mTransferInserter) {
    mTransferIdLoader = do_CreateInstance(AA_LOADLASTID_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsISupportsCString> filter
      = do_CreateInstance(NS_SUPPORTS_CSTRING_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = filter->SetData(NS_LITERAL_CSTRING("transfer"));
    NS_ENSURE_SUCCESS(rv, rv);
    
    rv = mTransferIdLoader->SetFilter(filter);
    NS_ENSURE_SUCCESS(rv, rv);

    mTransferInserter = do_CreateInstance(AA_TRANSFER_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mTransferInserter->SetFilter(aFact);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTransferInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> id
    = do_CreateInstance(AA_SQLIDLISTENER_CONTRACT, aFact, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTransferIdLoader, id);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::insertSide(aaISqliteChannel *aChannel, aaIState *aState)
{
  nsresult rv;
  if (!aState)
    return NS_OK;

  if (!mSideInserter) {
    mSideInserter = do_CreateInstance(AA_FACT_SIDE_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mSideInserter->SetFilter(aState);
  NS_ENSURE_SUCCESS(rv, rv);
  
  rv = aChannel->Open(mSideInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);
  
  return NS_OK;
}

nsresult
aaSaveEvent::insertState(aaISqliteChannel *aChannel, aaIState *aState)
{
  nsresult rv;
  if (!aState)
    return NS_OK;

  if (!mStateInserter) {
    mStateInserter = do_CreateInstance(AA_SAVESTATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mStateInserter->SetParam(aState);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mStateInserter->Execute(aChannel, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::cleanEvent(aaISqliteChannel *aChannel)
{
  nsresult rv;

  rv = deleteSide(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = deleteTransfer(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = deleteEvent(aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMutableArray> block = do_QueryInterface(mEvent, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(block, NS_ERROR_UNEXPECTED);

  rv = mEvent->SetId(0);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = block->Clear();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::deleteSide(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mSideDeleter) {
    mSideDeleter = do_CreateInstance(AA_FACT_SIDE_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mSideDeleter->SetFilter(mEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mSideDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::deleteTransfer(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mTransferDeleter) {
    mTransferDeleter = do_CreateInstance(AA_TRANSFER_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mTransferDeleter->SetFilter(mEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mTransferDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSaveEvent::deleteEvent(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mEventDeleter) {
    mEventDeleter = do_CreateInstance(AA_EVENT_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mEventDeleter->SetFilter(mEvent);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mEventDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
