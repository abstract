/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTTERM_H
#define AAINSERTTERM_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_TERM_CID \
{0x205a97ce, 0xe278, 0x43b2, {0xaf, 0x66, 0x52, 0x98, 0xfb, 0xf6, 0x18, 0x60}}

class aaIState;
#ifdef DEBUG
#include "aaIState.h"
#endif

class aaInsertTerm : public aaSaveRequest
{
public:
  aaInsertTerm();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertTerm() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIState> mTerm;
};

#endif /* AAINSERTTERM_H */
