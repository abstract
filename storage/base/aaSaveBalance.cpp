/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaIMoney.h"
#include "aaIFlow.h"
#include "aaIBalance.h"
#include "aaISqliteChannel.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaSaveKeys.h"

#include "aaSaveBalance.h"

aaSaveBalance::aaSaveBalance()
{
}

NS_IMPL_ISUPPORTS2(aaSaveBalance,
                   aaISqlTransaction,
                   aaIStorager)
/* aaISqlTransaction */
NS_IMETHODIMP
aaSaveBalance::SetParam(nsISupports *aParam)
{
  nsresult rv;
  mBalance = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveBalance::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mBalance, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER(mBalance->PickFlow());

  nsresult rv;
  rv = mBalance->Sync(this, aChannel);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* aaIStorager */
NS_IMETHODIMP
aaSaveBalance::Insert(aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (!mBalanceInserter) {
    mBalanceInserter = do_CreateInstance(AA_BALANCE_INSERT_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mBalanceInserter->SetFilter(mBalance);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mBalanceInserter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveBalance::Update(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mBalanceUpdater) {
    mBalanceUpdater = do_CreateInstance(AA_BALANCE_UPDATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mBalanceUpdater->SetFilter(mBalance);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mBalanceUpdater, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveBalance::Close(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mBalanceCloser) {
    mBalanceCloser = do_CreateInstance(AA_BALANCE_CLOSE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mBalanceCloser->SetFilter(mBalance);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mBalanceCloser, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaSaveBalance::Delete(aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (!mBalanceDeleter) {
    mBalanceDeleter = do_CreateInstance(AA_BALANCE_DELETE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mBalanceDeleter->SetFilter(mBalance);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aChannel->Open(mBalanceDeleter, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
