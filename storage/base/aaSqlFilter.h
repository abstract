/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLFILTER_H
#define AASQLFILTER_H 1

#define AA_SQLFILTER_CID \
{0x1cd54f75, 0xc88b, 0x4354, {0xb7, 0x1b, 0x95, 0x56, 0xf7, 0x78, 0x0a, 0xfc}}

#include "nsCOMArray.h"
#include "nsInterfaceHashtable.h"
#include "aaISqlFilter.h"

class aaSqlFilter : public aaISqlFilter
{
public:
  aaSqlFilter();
  NS_DECL_ISUPPORTS
  NS_DECL_NSIINTERFACEREQUESTOR
  NS_DECL_AAISQLFILTER

private:
  ~aaSqlFilter();
  friend class aaStorageTest;

  nsCOMArray<nsISupports> mElements;
  nsInterfaceHashtable<nsCStringHashKey, nsISupports> mParams;
};

#endif /* AASQLFILTER_H */

