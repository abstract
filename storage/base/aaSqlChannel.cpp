/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsISupportsPrimitives.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "mozIStorageStatement.h"
#include "mozStorageHelper.h"

/* Project includes */
#include "aaISqlRequest.h"

#include "aaSession.h"
#include "aaLoadBuilder.h"
#include "aaSqlResult.h"
#include "aaSqlChannel.h"

aaSqlChannel::aaSqlChannel(aaSession *aSession)
  :mSession(aSession)
{}

NS_IMPL_ISUPPORTS1(aaSqlChannel,
                   aaISqliteChannel)

/* aaISqliteChannel */
NS_IMETHODIMP
aaSqlChannel::Open(aaISqlRequest *aRequest, aaILoadObserver *aObserver)
{
  nsresult rv;

  nsRefPtr<aaLoadBuilder> builder = new aaLoadBuilder();
  NS_ENSURE_TRUE(builder, NS_ERROR_OUT_OF_MEMORY);

  nsCOMPtr<mozIStorageStatement> statement;
  rv = aRequest->GetStatement(getter_AddRefs(statement));
  NS_ENSURE_SUCCESS(rv, rv);

  if (!statement) {
    nsCAutoString sql;
    rv = aRequest->GetSql(sql);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mSession->GetConnection()->CreateStatement(sql,
        getter_AddRefs(statement));
    NS_ENSURE_SUCCESS(rv, rv);
    rv = aRequest->SetStatement(statement);
    NS_ENSURE_SUCCESS(rv, rv);
  }

#ifdef DEBUG_LOG
  {
    nsCAutoString sql;
    rv = aRequest->GetSql(sql);
    NS_ENSURE_SUCCESS(rv, rv);
    printf_stderr(sql.get());
    printf_stderr("\n");
  }
#endif

  nsCOMPtr<nsIArray> rqParams;
  rv = aRequest->GetParams(getter_AddRefs(rqParams));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = bindParams(statement, rqParams);
  NS_ENSURE_SUCCESS(rv, rv);

  mozStorageStatementScoper scoper(statement);
  PRBool hasMore;
  PRInt32 state;
  do {
    rv = statement->ExecuteStep(&hasMore);
    if (NS_FAILED(rv)) return rv;
    rv = statement->GetState(&state);
    NS_ENSURE_SUCCESS(rv, rv);
    if (!aObserver)
      return NS_OK;

    if (state != mozIStorageStatement::MOZ_STORAGE_STATEMENT_EXECUTING)
      continue;

    nsCOMPtr<aaIDataNode> node;
    nsCOMPtr<aaISqlResult> result = new aaSqlResult(aRequest);
    rv = builder->Load(result, getter_AddRefs(node));
    NS_ENSURE_SUCCESS(rv, rv);
    aObserver->ObserveLoad(node);

  } while (hasMore);

  aObserver->ObserveResults();

  return NS_OK;
}

nsresult
aaSqlChannel::bindParams(mozIStorageStatement *statement, nsIArray *params)
{
  NS_ASSERTION(statement, "empty statement");

  nsresult rv;
  PRUint32 paramCount;
  rv = statement->GetParameterCount(&paramCount);
  NS_ENSURE_SUCCESS(rv, rv);

  if (!paramCount && !params)
    return NS_OK;

  NS_ENSURE_ARG_POINTER(params);
  PRUint32 count;
  rv = params->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(count == paramCount, NS_ERROR_INVALID_ARG);
  
  PRUint32 i;
  for (i = 0; i < count; i++) {
    nsCOMPtr<nsISupportsPrimitive> param = do_QueryElementAt(params, i);

    if (!param) {
      rv = statement->BindNullParameter(i);
      NS_ENSURE_SUCCESS(rv, rv);
      continue;
    }

    PRUint16 paramType;
    rv = param->GetType(&paramType);
    NS_ENSURE_SUCCESS(rv, rv);
    switch (paramType) {
    case nsISupportsPrimitive::TYPE_PRINT32:
      {
        nsCOMPtr<nsISupportsPRInt32> paramInt32 = do_QueryInterface(param, &rv);
        NS_ENSURE_SUCCESS(rv, rv);

        PRInt32 valInt32;
        rv = paramInt32->GetData(&valInt32);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = statement->BindInt32Parameter(i, valInt32);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case nsISupportsPrimitive::TYPE_PRINT64:
      {
        nsCOMPtr<nsISupportsPRInt64> paramInt64 = do_QueryInterface(param, &rv);
        NS_ENSURE_SUCCESS(rv, rv);

        PRInt64 valInt64;
        rv = paramInt64->GetData(&valInt64);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = statement->BindInt64Parameter(i, valInt64);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case nsISupportsPrimitive::TYPE_DOUBLE:
      {
        nsCOMPtr<nsISupportsDouble> paramDouble
          = do_QueryInterface(param, &rv);
        NS_ENSURE_SUCCESS(rv, rv);

        double valDouble;
        rv = paramDouble->GetData(&valDouble);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = statement->BindDoubleParameter(i, valDouble);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    case nsISupportsPrimitive::TYPE_CSTRING:
      {
        nsCOMPtr<nsISupportsCString> paramCString
          = do_QueryInterface(param, &rv);
        NS_ENSURE_SUCCESS(rv, rv);

        nsCAutoString valCString;
        rv = paramCString->GetData(valCString);
        NS_ENSURE_SUCCESS(rv, rv);

        rv = statement->BindUTF8StringParameter(i, valCString);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      break;
    default:
      return NS_ERROR_INVALID_ARG;
    }
  }
  return NS_OK;
}
