/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsStringAPI.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaITimeFrame.h"
#include "aaIFlow.h"
#include "aaIChartHandler.h"
#include "aaLoadIncome.h"

#define AA_LOADINCOME_SELECT_QUERY "SELECT\
    0.0,\
    income.side,\
    income.value,\
    SUM(-takeQ.diff),\
    SUM( giveQ.diff),\
    strftime('%s',income.start),\
    strftime('%s',income.paid)\
  FROM income\
  LEFT JOIN quote AS takeQ ON\
    takeQ.day = income.start\
    AND takeQ.diff<0.0\
  LEFT JOIN quote AS giveQ ON\
    giveQ.day = income.start\
    AND giveQ.diff>0.0\
  WHERE IFNULL(start<=round(julianday(?,'unixepoch')),1)\
    AND (IFNULL(paid>round(julianday(?,'unixepoch')),0)\
        OR paid IS NULL)\
  GROUP BY income.start"

static struct aaLoadRequest::ColumnMap map[] = {
  {"amount", 0}
  ,{"side", 1}
  ,{"value", 2}
  ,{"credit_diff", 3}
  ,{"debit_diff", 4}
  ,{"start", 5}
  ,{"paid", 6}
  ,{0, 0}
};

aaLoadIncome::aaLoadIncome()
{
  mSql = AA_LOADINCOME_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadIncome,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadIncome::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<aaIFlow> flow = do_GetInterface(mFilter);
  NS_ENSURE_TRUE(!flow || !(flow->PickId()), NS_ERROR_INVALID_ARG);

  nsCOMPtr<aaITimeFrame> frame = do_GetInterface(mFilter);
  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;

  if (frame) {
    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickEnd() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickStart() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = result->InsertElementAt(nsnull, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
NS_IMETHODIMP
aaLoadIncome::Accept(aaIHandler *aQuery)
{
  nsresult rv;
  nsCOMPtr<aaIChartHandler> chartQuery = do_QueryInterface(aQuery, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  return chartQuery->HandleIncome(nsnull);
}
