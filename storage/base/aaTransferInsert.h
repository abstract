/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSFERINSERT_H
#define AATRANSFERINSERT_H 1

#define AA_TRANSFER_INSERT_CID \
{0xc5382267, 0x8459, 0x4cd5, {0xb7, 0x79, 0x5e, 0x9b, 0xab, 0xea, 0x71, 0xed}}

#include "aaSaveRequest.h"

class aaIFact;
#ifdef DEBUG
#include "aaIFact.h"
#endif

class aaTransferInsert : public aaSaveRequest
{
public:
  aaTransferInsert();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaTransferInsert() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIFact> mFact;
};

#endif /* AATRANSFERINSERT_H */
