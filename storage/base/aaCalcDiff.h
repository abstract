/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AACALCDIFF_H
#define AACALCDIFF_H 1

#include "aaLoadRequest.h"

#define AA_CALCDIFF_CID \
{0x71df8a2a, 0xc356, 0x448d, {0x83, 0x15, 0x2e, 0xa6, 0xbd, 0x18, 0xe9, 0xf8}}

class aaCalcDiff : public aaLoadRequest
{
public:
  aaCalcDiff();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AALOADREQUEST

protected:
  NS_IMETHOD GetCondition(aaICondition* pCondition, nsACString& aCondition) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
protected:
  ~aaCalcDiff() {}

private:
  friend class aaStorageTest;
};

#endif /* AACALCDIFF_H */
