/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIFact.h"
#include "aaITransaction.h"

#include "aaTransactionInsert.h"

static const char *sql =
"INSERT INTO txn (day, event_id, transfer_id, value, status, earnings)\
  VALUES ((replace(round(julianday(?,'unixepoch')),'.0','')), ?, ?, ?, ?, ?)";


aaTransactionInsert::aaTransactionInsert()
{
  mSql = sql;
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTransactionInsert,
                             aaSaveRequest)

/* aaSaveRequest */
NS_IMETHODIMP
aaTransactionInsert::SetFilter(nsISupports * aFilter)
{
  nsresult rv;
  mTransaction = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaTransactionInsert::GetParams(nsIArray * * aParams)
{ 
  NS_ENSURE_TRUE(mTransaction, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(mTransaction->PickFact(), NS_ERROR_INVALID_ARG);
  nsresult rv;

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;
  nsCOMPtr<nsISupportsPRInt32> valInt32;
  nsCOMPtr<nsISupportsDouble> valDouble;

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(mTransaction->PickFact()->PickTime() / 1000000);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(mTransaction->PickFact()->PickEventId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt64->SetData(mTransaction->PickFact()->PickId());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt64, 2, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valDouble = do_CreateInstance(NS_SUPPORTS_DOUBLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valDouble->SetData(mTransaction->PickValue());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valDouble, 3, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  valInt32 = do_CreateInstance(NS_SUPPORTS_PRINT32_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = valInt32->SetData(mTransaction->PickHasEarnings());
  NS_ENSURE_SUCCESS(rv, rv);
  rv = result->InsertElementAt(valInt32, 4, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mTransaction->PickHasEarnings()) {
    valDouble = do_CreateInstance(NS_SUPPORTS_DOUBLE_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valDouble->SetData(mTransaction->PickEarnings());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valDouble, 5, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 5, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

*aParams = result;
result.forget();

return NS_OK;
}
