/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASAVESTATE_H
#define AASAVESTATE_H 1

#define AA_SAVESTATE_CID \
{0x02a584f6, 0xfb1b, 0x4984, {0xa6, 0x19, 0x4f, 0xcc, 0xe5, 0x07, 0xe1, 0x3a}}

#include "aaISqlTransaction.h"
#include "aaIStorager.h"

class aaIState;
class aaISqlRequest;
#ifdef DEBUG
#include "aaIState.h"
#include "aaISqlRequest.h"
#endif

class aaSaveState : public aaISqlTransaction,
                    public aaIStorager
{
public:
  aaSaveState();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION
  NS_DECL_AAISTORAGER

protected:
  ~aaSaveState() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIState> mState;
  nsCOMPtr<aaISqlRequest> mStateInserter;
  nsCOMPtr<aaISqlRequest> mStateUpdater;
  nsCOMPtr<aaISqlRequest> mStateCloser;
  nsCOMPtr<aaISqlRequest> mStateDeleter;
};

#endif /* AASAVESTATE_H */
