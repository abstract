/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINSERTENTITY_H
#define AAINSERTENTITY_H 1

#include "aaSaveRequest.h"

#define AA_INSERT_ENTITY_CID \
{0xfc464264, 0xa5d8, 0x4832, {0xbc, 0x6e, 0x68, 0xd0, 0xec, 0xd1, 0x66, 0x4d}}

class aaIEntity;
#ifdef DEBUG
#include "aaIEntity.h"
#endif

class aaInsertEntity : public aaSaveRequest
{
public:
  aaInsertEntity();
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_AASAVEREQUEST

protected:
  ~aaInsertEntity() {}

private:
  friend class aaStorageTest;

  nsCOMPtr<aaIEntity> mEntity;
};

#endif /* AAINSERTENTITY_H */
