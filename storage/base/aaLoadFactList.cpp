/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsComponentManagerUtils.h"
#include "nsXPCOMCID.h"
#include "nsIMutableArray.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */

/* Project includes */
#include "aaITimeFrame.h"
#include "aaIFlow.h"
#include "aaIEvent.h"
#include "aaIFact.h"
#include "aaIHandler.h"
#include "aaLoadFactList.h"

#ifndef AA_LOADFACTLIST_SELECT_QUERY
#define AA_LOADFACTLIST_SELECT_QUERY "SELECT\
    strftime('%s',transfer.day),\
    transfer.event_id,\
    transfer.id,\
    transfer.amount,\
    flowF.id,\
    flowF.tag,\
    flowF.rate,\
    entityF.id,\
    entityF.tag,\
    term.resource_type,\
    term.resource_id,\
    money.alpha_code,\
    asset.tag,\
    termF.resource_type,\
    termF.resource_id,\
    moneyF.alpha_code,\
    assetF.tag,\
    flowT.id,\
    flowT.tag,\
    flowT.rate,\
    entityT.id,\
    entityT.tag,\
    termT.resource_type,\
    termT.resource_id,\
    moneyT.alpha_code,\
    assetT.tag,\
    txn.value,\
    txn.earnings\
  FROM fact AS factA\
    LEFT JOIN fact AS factB ON\
      factB.day = factA.day\
      AND factB.event_id = factA.event_id\
      AND factB.transfer_id = factA.transfer_id\
      AND factB.side = NOT factA.side\
    LEFT JOIN transfer ON\
      transfer.day == factA.day\
      AND transfer.event_id  == factA.event_id\
      AND transfer.id == factA.transfer_id\
    LEFT JOIN txn ON \
      transfer.day == txn.day\
      AND transfer.event_id == txn.event_id\
      AND transfer.id == txn.transfer_id\
    LEFT JOIN flow AS flowF ON\
      (flowF.id = factA.flow_id AND factA.side = 1)\
      OR (flowF.id = factB.flow_id AND factA.side = 0)\
    LEFT JOIN flow AS flowT ON\
      (flowT.id = factA.flow_id AND factA.side = 0)\
      OR (flowT.id = factB.flow_id AND factA.side = 1)\
    LEFT JOIN entity AS entityF ON\
      entityF.id=flowF.entity_id\
    LEFT JOIN entity AS entityT ON\
      entityT.id=flowT.entity_id\
    LEFT JOIN term AS termF ON\
      termF.flow_id=flowF.id\
      AND termF.side=1\
    LEFT JOIN money AS moneyF ON\
      termF.resource_type=1\
      AND moneyF.id=termF.resource_id\
    LEFT JOIN asset AS assetF ON\
      termF.resource_type=2\
      AND assetF.id=termF.resource_id\
    LEFT JOIN term ON\
      term.flow_id=factA.flow_id\
      AND term.side=1\
    LEFT JOIN money ON\
      term.resource_type=1\
      AND money.id=term.resource_id\
    LEFT JOIN asset ON\
      term.resource_type=2\
      AND asset.id=term.resource_id\
    LEFT JOIN term AS termT ON\
      termT.flow_id=flowT.id\
      AND termT.side=0\
    LEFT JOIN money AS moneyT ON\
      termT.resource_type=1\
      AND moneyT.id=termT.resource_id\
    LEFT JOIN asset AS assetT ON\
      termT.resource_type=2\
      AND assetT.id=termT.resource_id\
  WHERE\
    (IFNULL(factA.day=round(julianday(?,'unixepoch')),1)\
     AND IFNULL(factA.event_id=?,1)\
     AND IFNULL(factA.transfer_id=?,1))\
    AND IFNULL(factA.flow_id=?4 AND (factB.flow_id != ?4 OR factA.side = 1),\
        (factA.side = 1 OR (factA.side = 0 AND factB.flow_id IS NULL)))\
    AND IFNULL(txn.status = ?,1)\
    AND (IFNULL(factA.day>=round(julianday(?,'unixepoch')),1)\
        AND IFNULL(factA.day<=round(julianday(?,'unixepoch')),1))"
#endif

static struct aaLoadRequest::ColumnMap map[] = {
  { "fact.event.time", 0}
  ,{"fact.event.id", 1}
  ,{"fact.id", 2}
  ,{"fact.amount", 3}
  ,{"fact.takeFromFlow.id", 4}
  ,{"fact.takeFromFlow.tag", 5}
  ,{"fact.takeFromFlow.rate", 6}
  ,{"fact.takeFromFlow.entity.id", 7}
  ,{"fact.takeFromFlow.entity.tag", 8}
  ,{"fact.takeFromFlow.give_resource.type", 9}
  ,{"fact.takeFromFlow.give_resource.id", 10}
  ,{"fact.takeFromFlow.give_resource.alpha_code", 11}
  ,{"fact.takeFromFlow.give_resource.tag", 12}
  ,{"fact.takeFromFlow.take_resource.type", 13}
  ,{"fact.takeFromFlow.take_resource.id", 14}
  ,{"fact.takeFromFlow.take_resource.alpha_code", 15}
  ,{"fact.takeFromFlow.take_resource.tag", 16}
  ,{"fact.giveToFlow.id", 17}
  ,{"fact.giveToFlow.tag", 18}
  ,{"fact.giveToFlow.rate", 19}
  ,{"fact.giveToFlow.entity.id", 20}
  ,{"fact.giveToFlow.entity.tag", 21}
  ,{"fact.giveToFlow.give_resource.type", 22}
  ,{"fact.giveToFlow.give_resource.id", 23}
  ,{"fact.giveToFlow.give_resource.alpha_code", 24}
  ,{"fact.giveToFlow.give_resource.tag", 25}
  ,{"fact.giveToFlow.take_resource.type", 9}
  ,{"fact.giveToFlow.take_resource.id", 10}
  ,{"fact.giveToFlow.take_resource.alpha_code", 11}
  ,{"fact.giveToFlow.take_resource.tag", 12}
  ,{"value", 26}
  ,{"earnings", 27}
  ,{0, 0}
};

aaLoadFactList::aaLoadFactList()
{
  mSql = AA_LOADFACTLIST_SELECT_QUERY;
  mMap = &map[0];
}

NS_IMPL_ISUPPORTS_INHERITED0(aaLoadFactList,
                             aaLoadRequest)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadFactList::GetParams(nsIArray * * aParams)
{
  nsresult rv;
  nsCOMPtr<aaIEvent> event;
  nsCOMPtr<aaIFact> fact;
  nsCOMPtr<aaIFlow> flow;
  nsCOMPtr<aaITimeFrame> frame;

  event = do_QueryInterface(mFilter);
  if (!event) {
    fact = do_GetInterface(mFilter);
    if (!fact) {
      flow = do_GetInterface(mFilter);
      frame= do_GetInterface(mFilter);
    }
  }

  nsCOMPtr<nsIMutableArray> result
    = do_CreateInstance(NS_ARRAY_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupportsPRInt64> valInt64;
  nsCOMPtr<nsISupportsPRInt32> valInt32;

  if (event) {
    PRTime time;
    rv = event->GetTime(&time);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(time / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(event->PickId());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = result->InsertElementAt(nsnull, 2, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else if (fact) {
    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(fact->PickTime() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(fact->PickEventId());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(fact->PickId());
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 2, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

  }
  else {
    rv = result->InsertElementAt(nsnull, 0, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(nsnull, 1, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(nsnull, 2, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (flow) {
    if (flow->PickId()) {
      valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      rv = valInt64->SetData(flow->PickId());
      NS_ENSURE_SUCCESS(rv, rv);
      rv = result->InsertElementAt(valInt64, 3, PR_FALSE);
      NS_ENSURE_SUCCESS(rv, rv);

      rv = result->InsertElementAt(nsnull, 4, PR_FALSE);
      NS_ENSURE_SUCCESS(rv, rv);
    }
    else {
      rv = result->InsertElementAt(nsnull, 3, PR_FALSE);
      NS_ENSURE_SUCCESS(rv, rv);

      valInt32 = do_CreateInstance(NS_SUPPORTS_PRINT32_CONTRACTID, &rv);
      NS_ENSURE_SUCCESS(rv, rv);
      rv = valInt32->SetData(1);
      NS_ENSURE_SUCCESS(rv, rv);
      rv = result->InsertElementAt(valInt32, 4, PR_FALSE);

      NS_ENSURE_SUCCESS(rv, rv);
    }
  }
  else {
    rv = result->InsertElementAt(nsnull, 3, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(nsnull, 4, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (frame) {
    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickStart() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 5, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);

    valInt64 = do_CreateInstance(NS_SUPPORTS_PRINT64_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = valInt64->SetData(frame->PickEnd() / 1000000);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(valInt64, 6, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = result->InsertElementAt(nsnull, 5, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = result->InsertElementAt(nsnull, 6, PR_FALSE);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  *aParams = result;
  result.forget();

  return NS_OK;
}

/* aaIDataNode */
  NS_IMETHODIMP
aaLoadFactList::Accept(aaIHandler *aQuery)
{
  return aQuery->HandleTransaction(nsnull);
}
