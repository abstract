/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIMutableArray.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "mozIStorageStatement.h"

/* Project includes */
#include "aaIDataNode.h"
#include "aaIHandler.h"
#include "aaLoadRequest.h"

#include "aaISqlFilter.h"
#include "aaOrderCondition.h"
#include "aaFilterCondition.h"

aaLoadRequest::aaLoadRequest()
  :mSql(0), mSqlFiltered(0), mMap(0)
{
}

NS_IMPL_ISUPPORTS2(aaLoadRequest,
                   aaISqlRequest,
                   aaIDataNode)

/* aaISqlRequest */
NS_IMETHODIMP
aaLoadRequest::GetSql(nsACString & aSql)
{
  nsresult rc = NS_OK;
  if (mFilter && mSqlFiltered) {
    nsCOMPtr<aaISqlFilter> flter;
    //get filter
    flter = do_QueryInterface(mFilter, &rc);
    if (NS_NOINTERFACE == rc) {
      aSql.Assign(mSql);
    } else {
      NS_ENSURE_SUCCESS(rc, rc);
      nsCOMPtr<nsISupports> pTmp;
      nsCOMPtr<aaICondition> pCondition;
      nsCAutoString str;
      PRInt32 offset = -1;
      //do assign
      aSql.Assign(mSqlFiltered);
      //check for order
      offset = aSql.Find(AA_SQL_ORDER_MARK, sizeof(AA_SQL_ORDER_MARK) - 1);
      if (-1 != offset) {
        str.Truncate();
        rc = flter->GetParam(NS_LITERAL_CSTRING(ORDER_CONDITION_NAME), getter_AddRefs(pTmp));
        if (NS_SUCCEEDED(rc)) {
          pCondition = do_QueryInterface(pTmp, &rc);
          NS_ENSURE_SUCCESS(rc, rc);

          rc = GetCondition(pCondition, str);
          NS_ENSURE_SUCCESS(rc, rc);

          if (!str.IsEmpty()) {
            str.Insert(" ", 0);
          }
        } else {
          rc = NS_OK;
        }
        aSql.Replace(offset, sizeof(AA_SQL_ORDER_MARK) - 1, str);
      }
      //check for filter
      nsACString::size_type curLength = sizeof(AA_SQL_FILTER_MARK) - 1;
      bool fWhereCondition = false;
      offset = aSql.Find(AA_SQL_FILTER_MARK, curLength);
      if (-1 == offset) {
        curLength = sizeof(AA_SQL_WHERE_FILTER_MARK) - 1;
        offset = aSql.Find(AA_SQL_WHERE_FILTER_MARK, curLength);
        fWhereCondition = true;
      }
      if (-1 != offset) {
        str.Truncate();
        rc = flter->GetParam(NS_LITERAL_CSTRING(FILTER_CONDITION_NAME), getter_AddRefs(pTmp));
        if (NS_SUCCEEDED(rc)) {
          pCondition = do_QueryInterface(pTmp, &rc);
          NS_ENSURE_SUCCESS(rc, rc);

          rc = GetCondition(pCondition, str);
          NS_ENSURE_SUCCESS(rc, rc);

          if (!str.IsEmpty()) {
            str.Insert(fWhereCondition ? " WHERE " : " ", 0);
          }
        } else {
          rc = NS_OK;
        }
        aSql.Replace(offset, curLength, str);
      }
    }
  } else {
    aSql.Assign(mSql);
  }
  return NS_OK;
}

NS_IMETHODIMP
aaLoadRequest::GetColumn(const char *aPrefix, PRUint32 *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mMap, NS_ERROR_NOT_INITIALIZED);
  
  PRUint32 i = 0;
  while (mMap[i].prefix) {
    if (!strcmp(mMap[i].prefix, aPrefix)) {
      *_retval = mMap[i].index;
      return NS_OK;
    }
    i++;
  }
  *_retval = PRUint32 (-1);
  return NS_OK;
}

NS_IMETHODIMP
aaLoadRequest::GetStatement(mozIStorageStatement * *aStatement)
{
  NS_ENSURE_ARG_POINTER(aStatement);
  NS_IF_ADDREF(*aStatement = mStatement);
  return NS_OK;
}
NS_IMETHODIMP
aaLoadRequest::SetStatement(mozIStorageStatement * aStatement)
{
  mStatement = aStatement;
  return NS_OK;
}

NS_IMETHODIMP
aaLoadRequest::SetFilter(nsISupports * aFilter)
{
  if (!aFilter) {
    mFilter = nsnull;
    return NS_OK;
  }

  nsresult rv;
  mFilter = do_QueryInterface(aFilter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mStatement) {
    mStatement = nsnull;
  }
  return NS_OK;
}

/* aaIDataNode: PARTIAL */
NS_IMETHODIMP
aaLoadRequest::GetEdited(PRBool *aEdited)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaLoadRequest::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
