# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1/GPL 2.0/LGPL 2.1
#
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.
#
# The Original Code is the Mozilla build system.
#
# The Initial Developer of the Original Code is
# the Mozilla Foundation <http://www.mozilla.org/>.
# Portions created by the Initial Developer are Copyright (C) 2007
# the Initial Developer. All Rights Reserved.
#
# Contributor(s):
#   Henrik Skupin <hskupin@gmail.com>
#
# Alternatively, the contents of this file may be used under the terms of
# either the GNU General Public License Version 2 or later (the "GPL"), or
# the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# in which case the provisions of the GPL or the LGPL are applicable instead
# of those above. If you wish to allow use of your version of this file only
# under the terms of either the GPL or the LGPL, and not to allow others to
# use your version of this file under the terms of the MPL, indicate your
# decision by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL or the LGPL. If you do not delete
# the provisions above, a recipient may use your version of this file under
# the terms of any one of the MPL, the GPL or the LGPL.
#
# ***** END LICENSE BLOCK *****

add_makefiles "
abstract/Makefile
abstract/xpcom/Makefile
abstract/abstract/Makefile
abstract/abstract/xpunit/Makefile
abstract/abstract/base/Makefile
abstract/abstract/storage/Makefile
abstract/abstract/view/Makefile
abstract/xpunit/Makefile
abstract/base/Makefile
abstract/base/test/Makefile
abstract/base/utils/Makefile
abstract/accounting/Makefile
abstract/accounting/public/Makefile
abstract/accounting/src/Makefile
abstract/accounting/test/Makefile
abstract/storage/Makefile
abstract/storage/base/Makefile
abstract/storage/base/test/Makefile
abstract/storage/account/Makefile
abstract/storage/account/test/Makefile
abstract/report/Makefile
abstract/report/public/Makefile
abstract/report/test/Makefile
abstract/view/Makefile
abstract/view/test/Makefile
abstract/app/Makefile
abstract/app/chrome/Makefile
abstract/app/test/Makefile
abstract/app/test/unit/Makefile
abstract/installer/Makefile
abstract/installer/windows/Makefile
abstract/transport/Makefile
abstract/transport/base/Makefile
abstract/transport/base/public/Makefile
abstract/transport/base/src/Makefile
abstract/transport/base/test/Makefile
abstract/transport/sqlite/Makefile
abstract/transport/sqlite/src/Makefile
abstract/transport/sqlite/test/Makefile
abstract/transport/test/Makefile
abstract/sql/Makefile
abstract/sql/base/Makefile
abstract/sql/base/src/Makefile
abstract/sql/base/test/Makefile
abstract/sql/sqlite/Makefile
abstract/sql/sqlite/src/Makefile
abstract/sql/sqlite/test/Makefile
abstract/sql/test/Makefile
"
