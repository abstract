/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSCRIPTLOADER_H
#define AATRANSCRIPTLOADER_H 1

#include "aaISqlTransaction.h"

#define AA_TRANSCRIPT_CID \
{0xeb8151f9, 0x38be, 0x43ac, {0x8b, 0x08, 0x26, 0x6f, 0xdc, 0x33, 0x54, 0x54}}

class aaTranscript;

class aaITranscriptParam;
class aaISqlRequest;
class aaISqlFilter;
#ifdef DEBUG
#include "aaITranscript.h"
#include "aaISqlRequest.h"
#include "aaISqlFilter.h"
#endif /* DEBUG */

class aaTranscriptLoader : public aaISqlTransaction
{
public:
  aaTranscriptLoader();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

private:
  ~aaTranscriptLoader() {}
  friend class aaReportTest;

  nsCOMPtr<aaITranscriptParam> mParam;
  nsCOMPtr<aaISqlRequest> mBalanceLoader;
  nsCOMPtr<aaISqlRequest> mIncomeLoader;
  nsCOMPtr<aaISqlFilter> mBalanceFilter;
  nsCOMPtr<aaISqlRequest> mListLoader;
  nsCOMPtr<aaISqlFilter> mListFilter;

  nsresult checkState();
  nsresult loadList(aaISqliteChannel *aChannel, aaTranscript *retval);
  nsresult loadDiffs(aaISqliteChannel *aChannel, aaTranscript *retval);
};

#endif /* AATRANSCRIPTLOADER_H */
