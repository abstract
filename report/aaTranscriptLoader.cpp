/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIArray.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIInterfaceRequestorUtils.h"

/* Project includes */
#include "aaITimeFrame.h"
#include "aaIFlow.h"
#include "aaBaseKeys.h"
#include "aaISqlRequest.h"
#include "aaISqlFilter.h"
#include "aaISqliteChannel.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaTranscript.h"
#include "aaTranscriptCounter.h"
#include "aaTranscriptDiffCounter.h"
#include "aaTranscriptLoader.h"

aaTranscriptLoader::aaTranscriptLoader()
{
}

NS_IMPL_ISUPPORTS1(aaTranscriptLoader,
                   aaISqlTransaction)

/* aaISqlTransaction */
NS_IMETHODIMP
aaTranscriptLoader::SetParam(nsISupports *aParam)
{
  nsresult rv;
  NS_ENSURE_ARG_POINTER(aParam);
  mParam = do_QueryInterface(aParam, &rv);
  if (!mParam)
    mParam = do_GetInterface(aParam, &rv);
  return rv;
}

NS_IMETHODIMP
aaTranscriptLoader::Execute(aaISqliteChannel *aChannel, nsISupports * * _retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_ARG_POINTER(aChannel);
  NS_ENSURE_TRUE(mParam, NS_ERROR_NOT_INITIALIZED);

  nsresult rv;
  rv = checkState();
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaTranscript> retval = new aaTranscript(mParam);
/*
  rv = loadOpening(aChannel, retval);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = loadClosing(aChannel, retval);
  NS_ENSURE_SUCCESS(rv, rv);
*/
  rv = loadList(aChannel, retval);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = loadDiffs(aChannel, retval);
  NS_ENSURE_SUCCESS(rv, rv);

  return CallQueryInterface(retval, _retval);
}

/* private methods */
nsresult
aaTranscriptLoader::checkState()
{
  nsresult rv;
  if (!mBalanceFilter){
    mBalanceFilter = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!mBalanceLoader) {
    mBalanceLoader = do_CreateInstance(AA_LOADBALANCE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mBalanceLoader->SetFilter(mBalanceFilter);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!mIncomeLoader) {
    mIncomeLoader = do_CreateInstance(AA_LOADINCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mIncomeLoader->SetFilter(mBalanceFilter);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!mListFilter){
    mListFilter = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  if (!mListLoader) {
    mListLoader = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
    rv = mListLoader->SetFilter(mListFilter);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  return NS_OK;
}

nsresult
aaTranscriptLoader::loadList(aaISqliteChannel *aChannel, aaTranscript *retval)
{
  nsresult rv;

  nsCOMPtr<aaIFlow> flow;
  rv = mParam->GetFlow(getter_AddRefs(flow));
  NS_ENSURE_SUCCESS(rv, rv);

  PRTime start, end;
  rv = mParam->GetStart(&start);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = mParam->GetEnd(&end);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaITimeFrame> frame
    = do_CreateInstance(AA_TIMEFRAME_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = frame->SetStart(start);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = frame->SetEnd(end);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mListFilter->SetInterface(NS_GET_IID(aaIFlow), flow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mListFilter->SetInterface(NS_GET_IID(aaITimeFrame), frame);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> counter = new aaTranscriptCounter(retval);
  rv = aChannel->Open(mListLoader, counter);
  NS_ENSURE_SUCCESS(rv, rv);
  return NS_OK;
}

nsresult
aaTranscriptLoader::loadDiffs(aaISqliteChannel *aChannel, aaTranscript *retval)
{
  nsresult rv;

  nsCOMPtr<aaIFlow> flow;
  rv = mParam->GetFlow(getter_AddRefs(flow));
  NS_ENSURE_SUCCESS(rv, rv);

  PRTime start, end;
  rv = mParam->GetStart(&start);
  NS_ENSURE_SUCCESS(rv, rv);
  rv = mParam->GetEnd(&end);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaITimeFrame> frame
    = do_CreateInstance(AA_TIMEFRAME_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  PRExplodedTime tm;
  PR_ExplodeTime(start, &PR_GMTParameters, &tm);
  tm.tm_mday -= 1;
  PR_NormalizeTime(&tm, &PR_GMTParameters);
  rv = frame->SetStart(PR_ImplodeTime(&tm));
  NS_ENSURE_SUCCESS(rv, rv);
  rv = frame->SetEnd(end);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mBalanceFilter->SetInterface(NS_GET_IID(aaIFlow), flow);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = mBalanceFilter->SetInterface(NS_GET_IID(aaITimeFrame), frame);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaILoadObserver> result = new aaTranscriptDiffCounter(retval);
  rv = aChannel->Open(flow->PickId() ? mBalanceLoader : mIncomeLoader, result);
  NS_ENSURE_SUCCESS(rv, rv);
  return NS_OK;
}
