/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSCRIPTPARAM_H
#define AATRANSCRIPTPARAM_H 1

#define AA_TRANSCRIPTPARAM_CID \
{0x5dd899ac, 0x9fc8, 0x4fcc, {0xb8, 0x0f, 0x5b, 0x81, 0x2d, 0x79, 0x55, 0x4f}}

#include "aaITranscript.h"

class aaIFlow;

#ifdef DEBUG
#include "aaIFlow.h"
#endif

class aaTranscriptParam : public aaITranscriptParam
{
public:
  aaTranscriptParam() {}
  NS_DECL_ISUPPORTS
  NS_DECL_AAITRANSCRIPTPARAM

private:
  ~aaTranscriptParam() {}
  friend class aaReportTest;

  nsCOMPtr<aaIFlow> mFlow;
  PRTime mStart;
  PRTime mEnd;
};

#endif /* AATRANSCRIPTPARAM_H */

