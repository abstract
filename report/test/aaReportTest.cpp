/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsIFile.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "nsStringUtils.h"
#include "aaAmountUtils.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIBalance.h"
#include "aaITransaction.h"
#include "aaBaseTestUtils.h"
#include "aaBaseLoaders.h"
#include "aaISqlTransaction.h"
#include "aaISession.h"
#include "aaSessionUtils.h"
#include "aaIManager.h"
#include "aaReportKeys.h"
#include "aaTranscript.h"
#include "aaBalanceSheet.h"
#include "aaGeneralLedger.h"
#include "aaTestConsts.h"

#include "aaReportTest.h"

NS_IMPL_ISUPPORTS1(aaReportTest, nsITest)

NS_IMETHODIMP
aaReportTest::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED( res.status )) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_REPORT_TEST_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }
  testTranscript( aTestRunner );
  testPnLTranscript(aTestRunner);
  testBalanceSheet(aTestRunner);
  testGeneralLedger(aTestRunner);
  return NS_OK;
}

/* Helpers */
aaReportTest::RAII::RAII(aaReportTest *t)
  :test(t)
{
  status = init();
}

nsresult
aaReportTest::RAII::init()
{
  nsresult rv;
  NS_ENSURE_ARG_POINTER(test);

  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file, dir;
  rv = manager->TmpFileFromString("storage.sqlite", PR_FALSE,
      getter_AddRefs( file ));
  NS_ENSURE_SUCCESS(rv, rv);

  test->mSession = do_CreateInstance(AA_SESSION_CONTRACT, file, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mFlowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

aaReportTest::RAII::~RAII()
{
  if (!test)
    return;
  test->mFlowLoader = nsnull;
  test->mFlows = nsnull;
  test->mSession = nsnull;
  test = nsnull;
}

/* Private methods */
nsresult
aaReportTest::testTranscript(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<aaITranscript> transcript;
  nsCOMPtr<aaITranscriptParam> transcriptParam
    = do_CreateInstance(AA_TRANSCRIPTPARAM_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_TRANSCRIPT_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[transcript] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlows));
  NS_TEST_ASSERT_MSG(mFlows, "[transcript] creating flow loader");
  NS_ENSURE_TRUE(mFlows, rv);
  
  /* Set flow to bank */
  nsCOMPtr<aaIFlow> bank = do_QueryElementAt(mFlows, 2);
  transcriptParam->SetFlow(bank);

  /* *** Transcript [1] *** */

  /* Set start time to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  transcriptParam->SetStart(PR_ImplodeTime(&tm));
  /* Set end time to 2007-08-29 */
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  rv = loader->SetParam(transcriptParam);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [1] loading");
  NS_ENSURE_TRUE(transcript, rv);

  nsCOMPtr<aaIBalance> balance;
  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [1] opening must be null");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3, 1),
      "[transcript] [1] closing is wrong");

  PRUint32 count = 0;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 2, "[transcript] [1] wrong fact count");

  nsCOMPtr<aaITransaction> txn = do_QueryElementAt(transcript, 0);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 2, 3,
        AA_EVENT_AMOUNT_2), "[transcript] [1] 1st fact is wrong");

  txn =  do_QueryElementAt(transcript, 1);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 1, 3,
        AA_EVENT_AMOUNT_3), "[transcript] [1] 2nd fact is wrong");

  /* *** Transcript [2] *** */

  /* Set end time to 2007-08-30 */
  tm.tm_mday = 30;
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [2] loading");
  NS_ENSURE_TRUE(transcript, rv);

  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [2] opening must be null");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4 
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[transcript] [2] closing is wrong");

  count = 0;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 4, "[transcript] [2] wrong fact count");

  txn = do_QueryElementAt(transcript, 0);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 2, 3,
        AA_EVENT_AMOUNT_2), "[transcript] [2] 1st fact is wrong");

  txn =  do_QueryElementAt(transcript, 1);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 1, 3,
        AA_EVENT_AMOUNT_3), "[transcript] [2] 2nd fact is wrong");

  txn =  do_QueryElementAt(transcript, 2);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 3, 4,
        AA_EVENT_AMOUNT_4), "[transcript] pending fact is wrong");

  double val = 0.0;
  rv = transcript->GetTotalDebits(&val);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(val == AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        "[transcript] [3] wrong debit sum");

  rv = transcript->GetTotalDebitsValue(&val);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(val == AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        "[transcript] [3] wrong debit sum");

  rv = transcript->GetTotalCredits(&val);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(val == AA_EVENT_AMOUNT_4 + AA_EVENT_AMOUNT_6 *
      AA_EVENT_RATE_2, "[transcript] [3] wrong credit sum");

  rv = transcript->GetTotalCreditsValue(&val);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(val == AA_EVENT_AMOUNT_4 + AA_EVENT_AMOUNT_6 *
      AA_EVENT_RATE_2, "[transcript] [3] wrong credit sum");

  /* *** Transcript [3] *** */

  /* Set start time to 2007-08-30 */
  tm.tm_mday = 30;
  transcriptParam->SetStart(PR_ImplodeTime(&tm));
  /* Set end time to 2007-08-31 */
  tm.tm_mday = 31;
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [2] loading");
  NS_ENSURE_TRUE(transcript, rv);

  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3, 1),
      "[transcript] [3] opening is wrong");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2,
        AA_EVENT_AMOUNT_2 + AA_EVENT_AMOUNT_3 - AA_EVENT_AMOUNT_4
        - AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1),
      "[transcript] [3] closing is wrong");

  count = 0;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 2, "[transcript] [3] wrong fact count");

  txn = do_QueryElementAt(transcript, 0);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 3, 4,
        AA_EVENT_AMOUNT_4), "[transcript] [3] pending fact is wrong");

  /* *** Transcript [4] *** */

  /* Set start time to 2007-08-28 */
  tm.tm_mday = 28;
  transcriptParam->SetStart(PR_ImplodeTime(&tm));
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [2] loading");
  NS_ENSURE_TRUE(transcript, rv);

  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [4] opening must be null");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [4] closing must be null");

  count = 1;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 0, "[transcript] [4] wrong fact count");

  /* *** Transcript [5] *** */

  /* Set start time to 2007-08-29 */
  tm.tm_mday = 29;
  transcriptParam->SetStart(PR_ImplodeTime(&tm));
  /* Set end time to 2007-08-30 */
  tm.tm_mday = 30;
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  nsCOMPtr<aaIFlow> purchase = do_QueryElementAt(mFlows, 3);
  transcriptParam->SetFlow(purchase);

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [2] loading");
  NS_ENSURE_TRUE(transcript, rv);

  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [5] opening must be null");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[transcript] [5] closing is wrong");

  count = 0;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 1, "[transcript] [5] wrong fact count");

  txn = do_QueryElementAt(transcript, 0);
  NS_TEST_ASSERT_MSG(txn && testFact(aTestRunner, txn->PickFact(), 3, 4,
        AA_EVENT_AMOUNT_4), "[update txn] pending fact is wrong");

  return NS_OK;
}

nsresult
aaReportTest::testPnLTranscript(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<aaITranscript> transcript;
  nsCOMPtr<aaITranscriptParam> transcriptParam
    = do_CreateInstance(AA_TRANSCRIPTPARAM_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_TRANSCRIPT_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[transcript] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  /* Set flow to PnL */
  nsCOMPtr<aaIFlow> finres = do_CreateInstance("@aasii.org/base/income-flow;1");
  transcriptParam->SetFlow(finres);

  /* *** Transcript [6] *** */

  /* Set start time to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  transcriptParam->SetStart(PR_ImplodeTime(&tm));
  /* Set end time to 2007-09-10 */
  tm.tm_month = 8;
  tm.tm_mday = 10;
  transcriptParam->SetEnd(PR_ImplodeTime(&tm));

  rv = loader->SetParam(transcriptParam);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Execute(loader, getter_AddRefs(transcript));
  NS_TEST_ASSERT_MSG(transcript, "[transcript] [6] loading");
  NS_ENSURE_TRUE(transcript, rv);

  nsCOMPtr<aaIBalance> balance;
  rv = transcript->GetOpening(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! balance, "[transcript] [6] opening must be null");

  rv = transcript->GetClosing(getter_AddRefs( balance ));
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(testIncomeState(aTestRunner, balance, AA_EVENT_AMOUNT_15
        * (AA_EVENT_RATE_5 - AA_EVENT_RATE_4), 0),
      "[transcript] [6] closing is wrong");

  PRUint32 count = 0;
  rv = transcript->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 8, "[transcript] [8] wrong txn count");

  nsCOMPtr<aaITransaction> txn = do_QueryElementAt(transcript, 0);
  NS_TEST_ASSERT_MSG(testTxn(aTestRunner, txn, 6, 7, AA_EVENT_AMOUNT_6,
        AA_EVENT_AMOUNT_6 * AA_EVENT_RATE_2, 1, AA_EVENT_AMOUNT_6
        * (AA_EVENT_RATE_3 - AA_EVENT_RATE_2)), "[transcript] [8] 1st txn is wrong");

  txn = do_QueryElementAt(transcript, 7);
  NS_TEST_ASSERT_MSG(testTxn(aTestRunner, txn, 0, 10, AA_EVENT_AMOUNT_15, 0.0,
        1, AA_EVENT_AMOUNT_15 * AA_EVENT_RATE_5),
      "[transcript] [8] 8th txn is wrong");

  return NS_OK;
}

nsresult
aaReportTest::testBalanceSheet(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<aaIBalanceSheet> sheet;
  nsCOMPtr<aaIBalanceSheetParam> param
    = do_CreateInstance(AA_BALANCESHEETPARAM_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(param, "[balance sheet] param creation");
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_BALANCESHEET_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[balance sheet] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  rv = loader->SetParam(param);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Execute(loader, getter_AddRefs(sheet));
  NS_TEST_ASSERT_MSG(sheet, "[balance sheet] [1] loading");
  NS_ENSURE_TRUE(sheet, rv);

  PRUint32 count = 0;
  rv = sheet->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 7, "[balance sheet] [1] wrong line count");

  nsCOMPtr<aaIBalance> balance = do_QueryElementAt(sheet, 6);
  NS_TEST_ASSERT_MSG(balance, "[balance sheet] income not loaded" );

  NS_TEST_ASSERT_MSG(testIncomeState(aTestRunner, balance, AA_EVENT_AMOUNT_15
        * (AA_EVENT_RATE_5 - AA_EVENT_RATE_4), 0),
      "[balance sheet] income is wrong");

  double assets, liabilities;
  rv = sheet->GetTotalAssets(&assets);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(isZero(assets - 242300),
      "[balance sheet] [1] wrong total assets");

  rv = sheet->GetTotalLiabilities(&liabilities);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(isZero(liabilities - 242300),
      "[balance sheet] [1] wrong total liabilities");

  /* Set date to 2007-09-07 */
  PRExplodedTime tm = {0,0,0,12,7,8,2007};
  rv = param->SetDate(PR_ImplodeTime(&tm));

  rv = mSession->Execute(loader, getter_AddRefs(sheet));
  NS_TEST_ASSERT_MSG(sheet, "[balance sheet] [2] loading");
  NS_ENSURE_TRUE(sheet, rv);

  sheet->GetLength(&count);
  NS_TEST_ASSERT_MSG(count == 6, "[balance sheet] [2] wrong flow count");

  balance = do_QueryElementAt(sheet, 1);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 2, 1, AA_EVENT_AMOUNT_2
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_2, 0),
      "[balance sheet] flow2 is wrong");

  balance = do_QueryElementAt(sheet, 0);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 1, 1, AA_EVENT_AMOUNT_3
        / AA_FLOW_SHARE_RATE, AA_EVENT_AMOUNT_3, 0),
      "[balance sheet] flow1 is wrong");

  balance = do_QueryElementAt(sheet, 3);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 4, 2, AA_EVENT_AMOUNT_5,
        AA_EVENT_AMOUNT_4, 1), "[balance sheet] flow4 is wrong");

  balance = do_QueryElementAt(sheet, 4);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, AA_MONEY_CODE_2,
        2400, 2400 * AA_EVENT_RATE_4, 1),
      "[balance sheet] flow6 is wrong");

  balance = do_QueryElementAt(sheet, 5);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 9, 3,
        1, AA_EVENT_RATE_6, 1),
      "[balance sheet] flow9 is wrong");

  balance = do_QueryElementAt(sheet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 3, AA_MONEY_CODE_1,
        87920, 87920, 1), "[balance sheet] flow3 is wrong");

  rv = sheet->GetTotalAssets(&assets);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(isZero(assets - 242000),
      "[balance sheet] [2] wrong total assets");

  rv = sheet->GetTotalLiabilities(&liabilities);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(isZero(liabilities - 242000),
      "[balance sheet] [2] wrong total liabilities");

  return NS_OK;
}

nsresult
aaReportTest::testGeneralLedger(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<aaIGeneralLedger> ledger;
  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_GENERALLEDGER_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[general ledger] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  rv = mSession->Execute(loader, getter_AddRefs(ledger));
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(ledger, rv);

  PRUint32 count;
  rv = ledger->GetLength(&count);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 20, "[general ledger] wrong transfer count");

  return NS_OK;
}
