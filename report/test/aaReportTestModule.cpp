/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaReportTest.h"
#include "aaCurrencyReport.h"

#define AA_REPORT_TESTMODULE_CID \
{0xca303dca, 0xd6d4, 0x4049, {0xba, 0x4c, 0x51, 0x4b, 0xf2, 0x3c, 0x82, 0x98}}
#define AA_REPORT_TESTMODULE_CONTRACT "@aasii.org/report/unit;1"

class aaReportTestModule: public nsITest,
                          public nsIUTF8StringEnumerator
{
public:
  aaReportTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  virtual ~aaReportTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaReportTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaReportTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  "@aasii.org/report/unit-core;1"
  ,"@aasii.org/report/unit-currency;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaReportTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaReportTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign( subtests[mSubtest++] );
  return NS_OK;
}

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaReportTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaReportTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaCurrencyReport)

static const nsModuleComponentInfo kComponents[] =
{
  {
    "Report Module Test Container",
    AA_REPORT_TESTMODULE_CID,
    AA_REPORT_TESTMODULE_CONTRACT,
    aaReportTestModuleConstructor
  }
  ,{
    "Core Report Module Unit Test",
    AA_REPORT_TEST_CID,
    AA_REPORT_TEST_CONTRACT,
    aaReportTestConstructor
  }
  ,{
    "Report Unit Test with Multiple Currencies",
    AA_CURRENCY_REPORT_CID,
    AA_CURRENCY_REPORT_CONTRACT,
    aaCurrencyReportConstructor
  }
};
NS_IMPL_NSGETMODULE(aaReportTest, kComponents)
