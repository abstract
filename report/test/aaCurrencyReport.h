/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AACURRENCYREPORT_H
#define AACURRENCYREPORT_H 1

#include "nsCOMPtr.h"


class aaISession;
class aaISqlRequest;

#ifdef DEBUF
#include "aaISession.h"
#include "aaISqlRequest.h"
#endif

#define AA_CURRENCY_REPORT_CID \
{0xccad739c, 0xe13a, 0x4ce0, {0x80, 0xa1, 0xdd, 0xa1, 0x75, 0x86, 0x64, 0x77}}
#define AA_CURRENCY_REPORT_CONTRACT "@aasii.org/report/unit-currency;1"

class aaCurrencyReport: public nsITest
{
public:
  aaCurrencyReport() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST

private:
  ~aaCurrencyReport() {;}
  class RAII
  {
    public:
      RAII(aaCurrencyReport *t);
      ~RAII();

      PRBool status;
    private:
      aaCurrencyReport* test;
      nsresult init();
  };
  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<aaISqlRequest> mFlowLoader;
  nsCOMPtr<nsIArray> mFlows;

  nsresult testBalanceSheet(nsITestRunner *aTestRunner);
  nsresult testTranscript(nsITestRunner *aTestRunner);
};

#endif /* AACURRENCYREPORT_H */
