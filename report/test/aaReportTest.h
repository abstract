/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAREPORTTEST_H
#define AAREPORTTEST_H 1

#include "nsCOMPtr.h"

/* Unfrozen API */

/* Project includes */
#include "aaISession.h"
#include "nsIArray.h"
#include "aaISqlRequest.h"

#define AA_REPORT_TEST_CID \
{0x7f2114c5, 0x823a, 0x4f3c, {0x94, 0xb2, 0xd8, 0x54, 0x3b, 0xb9, 0x0c, 0x71}}
#define AA_REPORT_TEST_CONTRACT "@aasii.org/report/unit-core;1"

class aaReportTest: public nsITest
{
public:
  aaReportTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST

private:
  ~aaReportTest() {;}

  class RAII
  {
    public:
      RAII(aaReportTest *t);
      ~RAII();

      PRBool status;
    private:
      aaReportTest* test;
      nsresult init();
  };
  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<aaISqlRequest> mFlowLoader;
  nsCOMPtr<nsIArray> mFlows;

  nsresult testTranscript(nsITestRunner *aTestRunner);
  nsresult testPnLTranscript(nsITestRunner *aTestRunner);
  nsresult testBalanceSheet(nsITestRunner *aTestRunner);
  nsresult testGeneralLedger(nsITestRunner *aTestRunner);
};

#endif /* AAREPORTTEST_H */
