/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsIFile.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIBalance.h"
#include "aaISqlTransaction.h"
#include "aaISession.h"
#include "aaSessionUtils.h"
#include "aaIManager.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaReportKeys.h"
#include "aaBalanceSheet.h"
#include "aaTranscript.h"

#include "nsStringUtils.h"
#include "aaBaseTestUtils.h"
#include "aaCurrencyReport.h"

NS_IMPL_ISUPPORTS1(aaCurrencyReport, nsITest)

NS_IMETHODIMP
aaCurrencyReport::Test(nsITestRunner *aTestRunner)
{
  RAII res(this);
  if (NS_FAILED( res.status )) {
    aTestRunner->AddError(nsITestRunner::errorJS, \
        AA_CURRENCY_REPORT_CONTRACT " not initialized");
    return NS_ERROR_NOT_INITIALIZED;
  }
  testBalanceSheet(aTestRunner);
  testTranscript(aTestRunner);
  return NS_OK;
}

/* Helpers */
aaCurrencyReport::RAII::RAII(aaCurrencyReport *t)
  :test(t)
{
  status = init();
}

nsresult
aaCurrencyReport::RAII::init()
{
  NS_ENSURE_ARG_POINTER(test);
  nsresult rv;
  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file, dir;
  rv = manager->TmpFileFromString("currency.sqlite", PR_FALSE,
      getter_AddRefs( file ));
  NS_ENSURE_SUCCESS(rv, rv);

  test->mSession = do_CreateInstance(AA_SESSION_CONTRACT, file, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  test->mFlowLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

aaCurrencyReport::RAII::~RAII()
{
  if (!test)
    return;
  test->mFlowLoader = nsnull;
  test->mSession = nsnull;
  test = nsnull;
}

/* Private methods */
nsresult
aaCurrencyReport::testBalanceSheet(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<aaIBalanceSheet> sheet;
  nsCOMPtr<aaIBalanceSheetParam> param
    = do_CreateInstance(AA_BALANCESHEETPARAM_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(param, "[balance sheet] param creation");
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_BALANCESHEET_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[balance sheet] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  rv = loader->SetParam(param);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Execute(loader, getter_AddRefs(sheet));
  NS_TEST_ASSERT_MSG(sheet, "[balance sheet] [1] loading");
  NS_ENSURE_TRUE(sheet, rv);

  nsCOMPtr<aaIBalance> balance = do_QueryElementAt(sheet, 2);
  NS_TEST_ASSERT_MSG(testBalance(aTestRunner, balance, 6, 2, 30000, 48000, 0),
      "[balance sheet] flow 'bx1' is wrong");

  return NS_OK;
}

nsresult
aaCurrencyReport::testTranscript(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlows));
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mFlows, rv);

  nsCOMPtr<aaITranscript> transcript;
  nsCOMPtr<aaITranscriptParam> transcriptParam
    = do_CreateInstance(AA_TRANSCRIPTPARAM_CONTRACT, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaISqlTransaction> loader
    = do_CreateInstance(AA_TRANSCRIPT_CONTRACT, &rv);
  NS_TEST_ASSERT_MSG(loader, "[transcript] intance creation");
  NS_ENSURE_TRUE(loader, rv);

  rv = loader->SetParam(transcriptParam);
  NS_TEST_ASSERT_OK(rv);

  rv = mSession->Load(mFlowLoader, getter_AddRefs(mFlows));
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_TRUE(mFlows, rv);

  nsCOMPtr<aaIFlow> a2 = do_QueryElementAt(mFlows, 2);
  nsCOMPtr<aaIFlow> bx1 = do_QueryElementAt(mFlows, 5);

  PRExplodedTime tm = {0,0,0,12,25,2,2008};
  double val = 0.0;

  {
    transcriptParam->SetStart(PR_ImplodeTime(&tm));
    tm.tm_mday = 31;
    transcriptParam->SetEnd(PR_ImplodeTime(&tm));
    transcriptParam->SetFlow(a2);

    rv = mSession->Execute(loader, getter_AddRefs(transcript));
    NS_TEST_ASSERT_MSG(transcript, "[transcript] [6] loading");
    NS_ENSURE_TRUE(transcript, rv);

    rv = transcript->GetTotalDebits(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 60000.0, "[transcript] [3] wrong debit sum");

    rv = transcript->GetTotalDebitsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 120000.0, "[transcript] [3] wrong debit sum");

    rv = transcript->GetTotalDebitsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 3000.0, "[transcript] [3] wrong debit diff");

    rv = transcript->GetTotalCredits(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 40000.0, "[transcript] [a2] wrong credit sum");

    rv = transcript->GetTotalCreditsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 81000.0, "[transcript] [a2] wrong credit sum");

    rv = transcript->GetTotalCreditsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 0.0, "[transcript] [a2] wrong credit diff");
  }

  {
    tm.tm_mday = 24;
    transcriptParam->SetStart(PR_ImplodeTime(&tm));
    tm.tm_mday = 31;
    transcriptParam->SetEnd(PR_ImplodeTime(&tm));
    transcriptParam->SetFlow(bx1);

    rv = mSession->Execute(loader, getter_AddRefs(transcript));
    NS_TEST_ASSERT_MSG(transcript, "[transcript] [6] loading");
    NS_ENSURE_TRUE(transcript, rv);

    rv = transcript->GetTotalDebits(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 0,
        "[transcript] [3] wrong debit sum");

    rv = transcript->GetTotalDebitsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 0,
        "[transcript] [3] wrong debit sum");

    rv = transcript->GetTotalDebitsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 0,
        "[transcript] [3] wrong debit diff");

    rv = transcript->GetTotalCredits(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 300, "[transcript] [bx1] wrong credit sum");

    rv = transcript->GetTotalCreditsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 45000.0, "[transcript] [bx1] wrong credit sum");

    rv = transcript->GetTotalCreditsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 3000.0, "[transcript] [bx1] wrong credit diff");
  }

  nsCOMPtr<aaIFlow> finres = do_CreateInstance("@aasii.org/base/income-flow;1");
  {
    tm.tm_mday = 24;
    transcriptParam->SetStart(PR_ImplodeTime(&tm));
    tm.tm_mday = 31;
    transcriptParam->SetEnd(PR_ImplodeTime(&tm));
    transcriptParam->SetFlow(finres);

    rv = mSession->Execute(loader, getter_AddRefs(transcript));
    NS_TEST_ASSERT_MSG(transcript, "[transcript] [6] loading");
    NS_ENSURE_TRUE(transcript, rv);

    rv = transcript->GetTotalDebitsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 500.0, "[transcript] [PnL] wrong debit sum");

    rv = transcript->GetTotalDebitsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    if (NS_UNLIKELY(val != 3000.0)) {
      nsCAutoString msg("[transcript] [PnL] wrong debit diff: ");
      AppendDouble(msg, val);
      NS_TEST_ASSERT_MSG(0, msg.get());
    }

    rv = transcript->GetTotalCreditsValue(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 1000.0, "[transcript] [PnL] wrong credit sum");

    rv = transcript->GetTotalCreditsDiff(&val);
    NS_TEST_ASSERT_OK(rv);
    NS_TEST_ASSERT_MSG(val == 3000.0, "[transcript] [PnL] wrong credit diff");
  }

  return NS_OK;
}
