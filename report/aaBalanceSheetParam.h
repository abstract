/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABALANCESHEETPARAM_H
#define AABALANCESHEETPARAM_H 1

#define AA_BALANCESHEETPARAM_CID \
{0xfbbe75cd, 0x7961, 0x454f, {0x92, 0x96, 0x90, 0x7e, 0x2b, 0x41, 0x8a, 0x36}}

#include "aaIBalanceSheet.h"

class aaBalanceSheetParam : public aaIBalanceSheetParam
{
public:
  aaBalanceSheetParam();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIBALANCESHEETPARAM

private:
  ~aaBalanceSheetParam() {}
  friend class aaReportTest;

  PRTime mDate;
};

#endif /* AABALANCESHEETPARAM_H */

