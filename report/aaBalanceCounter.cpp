/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"

/* Unfrozen API */

/* Project includes */
#include "aaIBalance.h"
#include "aaBalanceSheet.h"
#include "aaBalanceCounter.h"

aaBalanceCounter::aaBalanceCounter(aaBalanceSheet *aTarget)
  :mTarget(aTarget)
{
}

NS_IMPL_ISUPPORTS1(aaBalanceCounter,
                   aaILoadObserver)

NS_IMETHODIMP
aaBalanceCounter::ObserveLoad(aaIDataNode *aNode)
{
  NS_ENSURE_ARG_POINTER(mTarget);
  nsresult rv;
  nsCOMPtr<aaIBalance> balance = do_QueryInterface(aNode, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  if (balance->PickSide())
    mTarget->mAssets += balance->PickValue();
  else
    mTarget->mLiabilities += balance->PickValue();

  mTarget->mList.AppendObject(balance);
  return NS_OK;
}

NS_IMETHODIMP
aaBalanceCounter::ObserveResults()
{
  return NS_OK;
}
