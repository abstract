/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaITransaction.h"
#include "aaGeneralLedger.h"

aaGeneralLedger::aaGeneralLedger()
{
}

NS_IMPL_ISUPPORTS2(aaGeneralLedger,
                   nsIArray,
                   aaIGeneralLedger)

/* nsIArray */
NS_IMETHODIMP
aaGeneralLedger::GetLength(PRUint32 *aLength)
{
  NS_ENSURE_ARG_POINTER(aLength);
  *aLength = mList.Count();
  return NS_OK;
}

NS_IMETHODIMP
aaGeneralLedger::QueryElementAt(PRUint32 index, const nsIID & uuid,
    void * *result)
{
  NS_ENSURE_ARG_POINTER(result);
  NS_ENSURE_TRUE(index < (PRUint32) mList.Count(), NS_ERROR_INVALID_ARG);
  return mList[index]->QueryInterface(uuid, result);
}

NS_IMETHODIMP
aaGeneralLedger::IndexOf(PRUint32 startIndex, nsISupports *element,
    PRUint32 *_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaGeneralLedger::Enumerate(nsISimpleEnumerator **_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
