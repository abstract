/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIGenericFactory.h"
#include "nsIArray.h"

/* Unfrozen API */
#include "mozIStorageConnection.h"

/* Project includes */
#include "aaParamFactory.h"
#include "aaReportKeys.h"
#include "aaTranscriptParam.h"
#include "aaTranscriptLoader.h"
#include "aaBalanceSheetParam.h"
#include "aaBalanceSheetLoader.h"
#include "aaGeneralLedgerLoader.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(aaTranscriptParam)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTranscriptLoader)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceSheetParam)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaBalanceSheetLoader)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaGeneralLedgerLoader)

static const nsModuleComponentInfo kComponents[] =
{
  { "Transcript Request",
    AA_TRANSCRIPTPARAM_CID,
    AA_TRANSCRIPTPARAM_CONTRACT,
    aaTranscriptParamConstructor}
  ,{"Transcript SQL Loader",
    AA_TRANSCRIPT_CID,
    AA_TRANSCRIPT_CONTRACT,
    aaTranscriptLoaderConstructor}
  ,{"Balance Sheet Request",
    AA_BALANCESHEETPARAM_CID,
    AA_BALANCESHEETPARAM_CONTRACT,
    aaBalanceSheetParamConstructor}
  ,{"Balance Sheet SQL Loader",
    AA_BALANCESHEET_CID,
    AA_BALANCESHEET_CONTRACT,
    aaBalanceSheetLoaderConstructor}
  ,{"General Ledger SQL Loader",
    AA_GENERALLEDGER_CID,
    AA_GENERALLEDGER_CONTRACT,
    aaGeneralLedgerLoaderConstructor}
};
NS_IMPL_NSGETMODULE(aareport, kComponents)

