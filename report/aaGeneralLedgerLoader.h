/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAGENERALLEDGERLOADER_H
#define AAGENERALLEDGERLOADER_H 1

#define AA_GENERALLEDGER_CID \
{0xf68135a8, 0xdbb9, 0x4afb, {0xb3, 0x10, 0x3d, 0x5e, 0xd4, 0xc4, 0x4e, 0xac}}

#include "nsCOMPtr.h"
#include "aaISqlTransaction.h"

class aaISqlRequest;
class aaGeneralLedger;

#ifdef DEBUG
#include "aaISqlRequest.h"
#endif

class aaGeneralLedgerLoader : public aaISqlTransaction
{
public:
  aaGeneralLedgerLoader();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

private:
  ~aaGeneralLedgerLoader() {}
  friend class aaReportTest;

  nsCOMPtr<aaISqlRequest> mListLoader;

  nsresult checkState();
  nsresult loadList(aaISqliteChannel *aChannel, aaGeneralLedger *rval);
};

#endif /* AAGENERALLEDGERLOADER_H */
