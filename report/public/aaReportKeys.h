/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAREPORTKEYS_H
#define AAREPORTKEYS_H 1

#define AA_TRANSCRIPTPARAM_CONTRACT "@aasii.org/report/transcript-param;1"
#define AA_TRANSCRIPT_CONTRACT "@aasii.org/report/transcript;1"

#define AA_BALANCESHEETPARAM_CONTRACT "@aasii.org/report/balance-param;1"
#define AA_BALANCESHEET_CONTRACT "@aasii.org/report/balance;1"

#define AA_GENERALLEDGER_CONTRACT "@aasii.org/report/general-ledger;1"

#endif /* AAREPORTKEYS_H */
