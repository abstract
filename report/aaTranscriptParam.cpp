/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "aaIFlow.h"

#include "aaTranscriptParam.h"

NS_IMPL_ISUPPORTS1(aaTranscriptParam,
                   aaITranscriptParam)

/* aaITranscriptParam */
NS_IMETHODIMP
aaTranscriptParam::GetFlow(aaIFlow * *aFlow)
{
  NS_ENSURE_ARG_POINTER(aFlow);
  NS_IF_ADDREF(*aFlow = mFlow);
  return NS_OK;
}
NS_IMETHODIMP
aaTranscriptParam::SetFlow(aaIFlow * aFlow)
{
  mFlow = aFlow;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscriptParam::GetStart(PRTime *aStart)
{
  NS_ENSURE_ARG_POINTER(aStart);
  *aStart = mStart;
  return NS_OK;
}
NS_IMETHODIMP
aaTranscriptParam::SetStart(PRTime aStart)
{
  mStart = aStart;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscriptParam::GetEnd(PRTime *aEnd)
{
  NS_ENSURE_ARG_POINTER(aEnd);
  *aEnd = mEnd;
  return NS_OK;
}
NS_IMETHODIMP
aaTranscriptParam::SetEnd(PRTime aEnd)
{
  mEnd = aEnd;
  return NS_OK;
}
