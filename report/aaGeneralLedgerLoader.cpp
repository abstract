/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */

/* Project includes */
#include "aaISqliteChannel.h"
#include "aaISqlFilter.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaAccountLoaders.h"
#include "aaGeneralLedger.h"
#include "aaGeneralLedgerCounter.h"
#include "aaGeneralLedgerLoader.h"

aaGeneralLedgerLoader::aaGeneralLedgerLoader()
{
}

NS_IMPL_ISUPPORTS1(aaGeneralLedgerLoader,
                   aaISqlTransaction)

nsresult
aaGeneralLedgerLoader::checkState()
{
  nsresult rv;
  if (!mListLoader) {
    mListLoader = do_CreateInstance(AA_LOADFACTLIST_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  return NS_OK;
}

nsresult
aaGeneralLedgerLoader::loadList(aaISqliteChannel *aChannel, aaGeneralLedger *rval)
{
  nsresult rv;
  rv = checkState();
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaGeneralLedgerCounter> result = new aaGeneralLedgerCounter(rval);
  rv = aChannel->Open(mListLoader, result);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* aaISqlTransaction */
NS_IMETHODIMP
aaGeneralLedgerLoader::SetParam(nsISupports *aParam)
{
  return NS_OK;
}

NS_IMETHODIMP
aaGeneralLedgerLoader::Execute(aaISqliteChannel *aChannel, nsISupports * *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_ARG_POINTER(aChannel);

  nsresult rv;
  rv = checkState();
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaGeneralLedger> retval = new aaGeneralLedger();

  rv = loadList(aChannel, retval);
  NS_ENSURE_SUCCESS(rv, rv);

  return CallQueryInterface(retval, _retval);
}
