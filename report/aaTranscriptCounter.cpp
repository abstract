/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"

/* Unfrozen API */

/* Project includes */
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaITransaction.h"
#include "aaTranscript.h"
#include "aaTranscriptCounter.h"

aaTranscriptCounter::aaTranscriptCounter(aaTranscript *aTarget)
  :mTarget(aTarget)
{
}

NS_IMPL_ISUPPORTS1(aaTranscriptCounter,
                   aaILoadObserver)

NS_IMETHODIMP
aaTranscriptCounter::ObserveLoad(aaIDataNode *aNode)
{
  NS_ENSURE_ARG_POINTER(mTarget);
  nsresult rv;
  nsCOMPtr<aaITransaction> txn = do_QueryInterface(aNode, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(txn->PickFact(), NS_ERROR_UNEXPECTED);

  if (!mTarget->mFlow->PickId()) {
    if (txn->PickEarnings() > 0.0)
      mTarget->mTotalCreditsValue += txn->PickEarnings();
    else
      mTarget->mTotalDebitsValue -= txn->PickEarnings();

    mTarget->mList.AppendObject(txn);
    return NS_OK;
  }

  if (txn->PickFact()->PickGiveTo()
      && txn->PickFact()->PickGiveTo()->PickId() == mTarget->mFlow->PickId())
  {
    mTarget->mTotalDebits += txn->PickFact()->PickAmount();
    mTarget->mTotalDebitsValue += txn->PickValue() + txn->PickEarnings();
  }
  else if (txn->PickFact()->PickTakeFrom()
      && txn->PickFact()->PickTakeFrom()->PickId() == mTarget->mFlow->PickId())
  {
    mTarget->mTotalCredits += txn->PickFact()->PickAmount();
    mTarget->mTotalCreditsValue += txn->PickValue();
  }
  else {
    return NS_ERROR_UNEXPECTED;
  }

  mTarget->mList.AppendObject(txn);
  return NS_OK;
}

NS_IMETHODIMP
aaTranscriptCounter::ObserveResults()
{
  return NS_OK;
}
