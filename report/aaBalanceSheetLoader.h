/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABALANCESHEETLOADER_H
#define AABALANCESHEETLOADER_H 1

#define AA_BALANCESHEET_CID \
{0xb9e95d64, 0xb9f9, 0x4a80, {0x86, 0xdb, 0x9b, 0x59, 0x9e, 0x8d, 0x7a, 0x2a}}

#include "aaISqlTransaction.h"

class aaISqlRequest;
class aaISqlFilter;
class aaBalanceSheet;

#ifdef DEBUG
#include "aaISqlRequest.h"
#include "aaISqlFilter.h"
#endif

class aaBalanceSheetLoader : public aaISqlTransaction
{
public:
  aaBalanceSheetLoader();
  NS_DECL_ISUPPORTS
  NS_DECL_AAISQLTRANSACTION

private:
  ~aaBalanceSheetLoader() {}
  friend class aaReportTest;

  nsCOMPtr<aaIBalanceSheetParam> mParam;
  nsCOMPtr<aaISqlRequest> mBalanceLoader;
  nsCOMPtr<aaISqlFilter> mBalanceFilter;
  nsCOMPtr<aaISqlRequest> mIncomeLoader;
  nsCOMPtr<aaISqlFilter> mIncomeFilter;

  nsresult checkState();
  nsresult loadList(aaISqliteChannel *aChannel, aaBalanceSheet *retval);
};

#endif /* AABALANCESHEETLOADER_H */
