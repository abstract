/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "nsStringUtils.h"
#include "aaIFlow.h"
#include "aaIBalance.h"
#include "aaITransaction.h"

#include "aaTranscript.h"

aaTranscript::aaTranscript(aaITranscriptParam *aParam)
  :mTotalDebits(0.0), mTotalCredits(0.0), mTotalDebitsValue(0.0),
  mTotalCreditsValue(0.0), mTotalDebitsDiff(0.0), mTotalCreditsDiff(0.0)
{
  mStatus = init(aParam);
}

nsresult
aaTranscript::init(aaITranscriptParam *aParam)
{
  NS_ENSURE_ARG_POINTER(aParam);
  nsresult rv;

  rv = aParam->GetFlow(getter_AddRefs(mFlow));
  NS_ENSURE_TRUE(mFlow, NS_ERROR_INVALID_ARG);

  rv = aParam->GetStart(&mStart);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aParam->GetEnd(&mEnd);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMPL_ISUPPORTS2(aaTranscript,
                   nsIArray,
                   aaITranscript)

/* nsIArray */
NS_IMETHODIMP
aaTranscript::GetLength(PRUint32 *aLength)
{
  NS_ENSURE_ARG_POINTER(aLength);
  *aLength = mList.Count();
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::QueryElementAt(PRUint32 index, const nsIID & uuid, void * *result)
{
  NS_ENSURE_ARG_POINTER(result);
  NS_ENSURE_TRUE(index < (PRUint32) mList.Count(), NS_ERROR_INVALID_ARG);
  return mList[index]->QueryInterface(uuid, result);
}

NS_IMETHODIMP
aaTranscript::IndexOf(PRUint32 startIndex, nsISupports *element, PRUint32 *_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTranscript::Enumerate(nsISimpleEnumerator **_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}


/* aaITranscript */
NS_IMETHODIMP
aaTranscript::GetFlow(aaIFlow * *aFlow)
{
  NS_ENSURE_ARG_POINTER(aFlow);
  NS_IF_ADDREF(*aFlow = mFlow);
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetOpening(aaIBalance * *aOpen)
{
  NS_ENSURE_ARG_POINTER(aOpen);
  NS_IF_ADDREF(*aOpen = mOpen);
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetClosing(aaIBalance * *aClose)
{
  NS_ENSURE_ARG_POINTER(aClose);
  NS_IF_ADDREF(*aClose = mClose);
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalDebits(double *aTotalDebits)
{
  NS_ENSURE_ARG_POINTER(aTotalDebits);

  *aTotalDebits = mTotalDebits;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalCredits(double *aTotalCredits)
{
  NS_ENSURE_ARG_POINTER(aTotalCredits);

  *aTotalCredits = mTotalCredits;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalDebitsValue(double *aTotalDebitsValue)
{
  NS_ENSURE_ARG_POINTER(aTotalDebitsValue);

  *aTotalDebitsValue = mTotalDebitsValue;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalCreditsValue(double *aTotalCreditsValue)
{
  NS_ENSURE_ARG_POINTER(aTotalCreditsValue);

  *aTotalCreditsValue = mTotalCreditsValue;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalDebitsDiff(double *aTotalDebitsDiff)
{
  NS_ENSURE_ARG_POINTER(aTotalDebitsDiff);

  *aTotalDebitsDiff = mTotalDebitsDiff;
  return NS_OK;
}

NS_IMETHODIMP
aaTranscript::GetTotalCreditsDiff(double *aTotalCreditsDiff)
{
  NS_ENSURE_ARG_POINTER(aTotalCreditsDiff);

  *aTotalCreditsDiff = mTotalCreditsDiff;
  return NS_OK;
}
