/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include <stdio.h>
#include "nsStringAPI.h"
#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaChartKeys.h"
#include "aaChart.h"

#define AA_ACCOUNTING_TEST_CID \
{0x59694add, 0xeec8, 0x43df, {0xb0, 0x85, 0xc8, 0x7b, 0x64, 0x71, 0xea, 0x7d}}
#define AA_ACCOUNTING_TEST_CONTRACT "@aasii.org/accounting/unit;1"

class aaAccountingTest: public nsITest
{
public:
  aaAccountingTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  virtual ~aaAccountingTest() {;}
  nsresult testChart(nsITestRunner *aTestRunner);
};

NS_IMETHODIMP
aaAccountingTest::Test(nsITestRunner *aTestRunner)
{
  nsITestRunner *cxxUnitTestRunner = aTestRunner;
  NS_TEST_ASSERT_OK(testChart(aTestRunner));
  return NS_OK;
} 

NS_IMPL_ISUPPORTS1(aaAccountingTest, nsITest)

NS_GENERIC_FACTORY_CONSTRUCTOR(aaAccountingTest)

static const nsModuleComponentInfo components[] =
{
  { "Accounting Module Unit Test",
    AA_ACCOUNTING_TEST_CID,
    AA_ACCOUNTING_TEST_CONTRACT,
    aaAccountingTestConstructor }
};

NS_IMPL_NSGETMODULE(aaAccountingTest, components)

/* Private methods */
nsresult
aaAccountingTest::testChart(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIChart> chart(do_CreateInstance(AA_CHART_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(chart, "aaChart instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(chart, &rv));
  NS_TEST_ASSERT_MSG(item, "aaChart aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
