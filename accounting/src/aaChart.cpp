/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsServiceManagerUtils.h"
#include "nsIObserverService.h"

/* Project includes */
#include "aaIMoney.h"
#include "aaIChartHandler.h"
#include "aaChart.h"

aaChart::aaChart()
{
}

aaChart::~aaChart()
{
}

NS_IMPL_ISUPPORTS2(aaChart,
                   aaIDataNode,
                   aaIChart)
/* aaIDataNode */
NS_IMETHODIMP
aaChart::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaChart::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}

NS_IMETHODIMP
aaChart::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  nsCOMPtr<aaIChartHandler> chartQuery = do_QueryInterface(aQuery);
  NS_ENSURE_TRUE(chartQuery, NS_ERROR_INVALID_ARG);

  return chartQuery->HandleChart(this);
}

NS_IMETHODIMP
aaChart::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaChart::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIChart */
NS_IMETHODIMP
aaChart::GetCurrency(aaIMoney* *aCurrency)
{
  NS_ENSURE_ARG_POINTER(aCurrency);
  NS_IF_ADDREF(*aCurrency = mCurrency);
  return NS_OK;
}
NS_IMETHODIMP
aaChart::SetCurrency(aaIMoney* aCurrency)
{
  mCurrency = aCurrency;
  nsCOMPtr<nsIObserverService> broadcaster = do_GetService(
      "@mozilla.org/observer-service;1");
  NS_ENSURE_TRUE(broadcaster, NS_OK);
  broadcaster->NotifyObservers(this, "chart-currency-change", nsnull);
  return NS_OK;
}
aaIMoney*
aaChart::PickCurrency()
{
  return mCurrency;
}
