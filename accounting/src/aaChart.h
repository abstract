/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AACHART_H
#define AACHART_H 1

#include "nsCOMPtr.h"
#include "aaIChart.h"

#define AA_CHART_CID \
{0x8e0d3c46, 0x4838, 0x4fff, {0xaf, 0xde, 0xa9, 0xbe, 0x6d, 0x33, 0x1f, 0x05}}

class aaIResource;

class aaChart : public aaIChart
{
public:
  aaChart();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAICHART

private:
  friend class aaAccountingTest;

  ~aaChart();
  PRInt64 mId;
  nsCOMPtr<aaIMoney> mCurrency;
};

#endif /* AACHART_H */


