/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  jsdump(obj);
  for (var prop in obj) {
    try {
      jsdump(prop + ": " + obj[prop]);
    } catch (e) {
      jsdump(prop + ": error: " + e.message);
    }
  }
}

var view = null;
var gTree = null;
var gEditing = false;
var gBrowseSet = null;
var gBrowseBar = null;
var gEditSet = null;
var gEditWnd = null;
var gEditBar = null;
var gNewMode = null;
var gCanSave = null;
var gWORM = false;
var gCanChange = null;
var gCanDiscard = true;
var gInsertOffset = 0;

function session()
{
  return window.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
}

function loadURI(uri)
{ 
  session().QueryInterface(nsCI.nsIWebNavigation)
    .loadURI(uri,nsCI.nsIWebNavigation.LOAD_FLAGS_NONE,null,null,null);
}

function goBack()
{ 
  let wn = window.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation);
  wn.goBack();
}

function createTree(flags)
{
  view.tree = document.getElementById("page1.tree");
  view.tree.view = session().createTreeView(treeViewContract);
  view.tree.view.QueryInterface(nsCI.aaIDataTreeView).flags = flags;
  view.tree.addEventListener("select", page1TreeSelect, false);
  view.tree.addEventListener("dblclick", page1TreeDblClick, false);
}

function page1LoadData(param)
{
  view.tree.view.QueryInterface(nsCI.aaIDataTreeView).update(param);
}

function page1Load(flags)
{
  createTree(flags);
  view._loadDataWithFilters();
  view.load();

  //fill filter control subfilters
  var cond = view.conditionBuilder();
  if (cond) {
    var viewer = document.getElementById("viewer");
    if (viewer) {
      var filters = cond.getFilters();
      for (var i = 0; i < filters.length; i ++) {
        viewer.addSubFilter(filters[i].label, filters[i].value);
      }
    }
  }
  //end fill filter control subfilters

  gBrowseSet = document.getElementById("page1.browseSet");
  gBrowseBar = document.getElementById("page1.browseBar");
  gEditSet = document.getElementById("page1.editSet");
  gEditWnd = document.getElementById("page1.editWnd");
  gEditBar = document.getElementById("page1.editBar");
  gNewMode = document.getElementById("page1.newMode");
  gCanSave = document.getElementById("page1.canSave");
  gCanChange = document.getElementById("page1.canChange");
  if (session().choosing) {
    document.getElementById("page1.header").removeAttribute("collapsed");
    document.getElementById("page1.header").removeAttribute("disabled");
    if (gCanDiscard) {
      document.getElementById("page1.headerDiscard").removeAttribute("disabled");
    }
  }
}

function page1Update(aSync)
{
  if (aSync)
    view.tree.boxObject.invalidate();
  if (view.buffer)
    page1UpdateStatus(true);
  else if (view.tree.view.selection.currentIndex == view.index ||
      (! view.tree.view.selection.isSelected(view.tree.view.selection.currentIndex) &&
      view.index == -1))
    page1UpdateStatus(true);
  else
    view.tree.view.selection.select(view.index);
}

function page1UpdateStatus(aSync)
{
  if (aSync)
    view.load();

  if (view.index == -1 || view.buffer || gWORM)
    gCanChange.setAttribute("disabled","true");
  else
    gCanChange.removeAttribute("disabled");

  var editing = (view.buffer != null);
  if (gEditing == editing)
    return;
  gEditing = editing;
  if (gEditing) {
    view.focus();
    gBrowseSet.setAttribute("disabled","true");
    gBrowseBar.setAttribute("collapsed","true");
    gEditSet.removeAttribute("disabled");
    gEditWnd.removeAttribute("readonly");
    gEditBar.removeAttribute("collapsed");
    if (view.tree.view.selection.currentIndex == -1) {
      gNewMode.removeAttribute("disabled");
      gNewMode.removeAttribute("readonly");
    }
    else {
      gNewMode.setAttribute("disabled","true");
      gNewMode.setAttribute("readonly","true");
    }
  } else {
    gBrowseSet.removeAttribute("disabled");
    gBrowseBar.removeAttribute("collapsed");
    gEditSet.setAttribute("disabled","true");
    gEditWnd.setAttribute("readonly","true");
    gEditBar.setAttribute("collapsed","true");
    gNewMode.setAttribute("disabled","true");
    gNewMode.setAttribute("readonly","true");
    gCanSave.setAttribute("disabled","true");
    view.tree.body.focus();
  }
  view.update();
}

function page1TreeSelect()
{
  if (! view.buffer) {
    var i = view.tree.view.selection.currentIndex;
    if (! view.tree.view.selection.isSelected(i))
      view.index = -1;
    else
      view.index = i;
  }
  page1UpdateStatus(true);
}

function page1TreeDblClick()
{
  if (document.getElementById("page1.header").hasAttribute("disabled"))
    return;
  page1Submit();
}

function page1SaveUpdate()
{
  if (view.buffer && view.buffer.edited)
    gCanSave.removeAttribute("disabled");
  else
    gCanSave.setAttribute("disabled","true");
}

function page1New()
{
  view.buffer = view.createNewNode(true);
  view.tree.view.selection.select(-1);
  page1Update();
}

function page1Change()
{
  view.copyNode();
  page1UpdateStatus(true);
}

function page1Cancel()
{
  if (gEditSet.getAttribute("disabled") == "true") {
    view.tree.view.selection.select(-1);
  } else {
    view.buffer = null;
    page1Update();
  }
}

function page1Reset()
{
  if (view.tree.view.selection.currentIndex >= 0) {
    page1Cancel();
    page1Change();
  } else {
    page1Cancel();
    page1New();
  }
}

function page1Save()
{
  try {
    view._save();
  } catch (e) {
    return;
  }
  try {
    view._loadDataWithFilters();
    view.index = view._getSavedObjectIndex();
    view._clearBuffer();
  } catch (e) {
    page1Reset();
    return;
  }
  page1Update(true);
}

function page1Discard()
{
  session().choice = null;
  goBack();
}

function page1Submit()
{
  view._submit();
  goBack();
}

function page1DoSort(column)
{
	var order = view.tree.getAttribute("sortDirection") == "natural"
              ? "ascending"
              : (view.tree.getAttribute("sortDirection") == "ascending" ? "descending" : "ascending");
  if (view.tree.getAttribute("sortResource") != column.id) {
    order = "ascending";
  }
	//setting these will make the sort option persist
	view.tree.setAttribute("sortDirection", order);
  view.tree.setAttribute("sortResource", column.id);
  //add condition to condition builder
  if (view.conditionBuilder()) {
    view.conditionBuilder().setCondition(nsCI.aaITreeViewFilters.FILTER_SORT,
                                         { sortDirection: order, sortResource:  column.id});
  }
  //do update
  view._loadDataWithFilters();
}

function page1Filter()
{
  //get viewer element
  var viewer = document.getElementById("viewer");
  if (viewer) {
    //add condition to condition builder
    if (view.conditionBuilder()) {
      var fType = viewer.getFilterType();
      if (!fType) {
        //get filters
        var filters = view.conditionBuilder().getFilters();
        if (filters && filters.length == 1) {
          fType = filters[0].value;
        }
      }
      view.conditionBuilder().setCondition(nsCI.aaITreeViewFilters.FILTER_FILTER,
                                           { type: fType, value: viewer.getFilterValue() });
    }
    //do update
    view._loadDataWithFilters();
  }
}

function page1ClearFilter()
{
  //add condition to condition builder
  if (view.conditionBuilder()) {
    view.conditionBuilder().setCondition(nsCI.aaITreeViewFilters.FILTER_FILTER, null);
  }
  //do update
  view._loadDataWithFilters();
}

/* View object */
function aaDetailView()
{
}

aaDetailView.prototype = {
  get history() {
    return document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
      .getInterface(nsCI.nsIWebNavigation)
      .sessionHistory;
  },
  get array() {
    return view.tree.view.QueryInterface(nsCI.aaIDataTreeView).dataSet;
  },
  get buffer() {
    return this.history.getEntryAtIndex(this.history.index, false)
      .QueryInterface(nsCI.aaIPageView).buffer;
  },
  set buffer(node) {
    this.history.getEntryAtIndex(this.history.index, false)
      .QueryInterface(nsCI.aaIPageView).buffer = node;
    this._onSetBuffer();
  },
  get index () {
    return this.history.getEntryAtIndex(this.history.index, false)
      .QueryInterface(nsCI.aaIPageView).index;
  },
  set index (i) {
    this.history.getEntryAtIndex(this.history.index, false)
      .QueryInterface(nsCI.aaIPageView).index = i;
    this._onSetIndex();
  },
  get _item () {
    if (this.index >= 0)
      return this.array.queryElementAt(this.index,nsCI.aaIDataNode);
    else
      return null;
  },
  tag : null,
  _top : null,
  get selection () {
    if (this.buffer)
      return this.buffer;
    else
      return this._item;
  },
  load : function () {
    if (this.selection)
      return this.read(this.selection);
    else
      this.reset();
  },

  _onSetIndex : function aaDetailView_OnSetIndex() {
  },

  _onSetBuffer : function aaDetailView_OnSetBuffer() {
  },

  createNewNode : function () {
    return Components.classes[nodeContract].createInstance(nsCI.aaIDataNode);
  },

  copyNode : function () {
    this.buffer = this.createNewNode();
    this.copy();
  },

  focus : function () {
    if (this._top)
      this._top.focus();
  },

  update : function () {
  },

  //manipulation functions
  _save : function aaDetailView_Save() {
    var prev = (this.tree.view.selection.currentIndex >= 0);
    this.history.save(this.buffer, prev ? this._item : null);
  },
  _clearBuffer: function aaDetailView_ClearBuffer() {
    this.buffer = null;
  },
  _submit : function aaDetailView_Submit() {
    this.history.choice = this.array.queryElementAt(this.index, nsCI.nsISupports);
  },
  _loadData : function aaDetailView_LoadData() {
    page1LoadData();
  },
  _buildFilter: function() {
    if (this.conditionBuilder()) {
      this.conditionBuilder().buildConditions(this.tree.view.QueryInterface(nsCI.aaIDataTreeView).filters);
    }
  },
  _loadDataWithFilters: function aaDetailView_LoadDataWithFilters() {
    this._buildFilter();
    this._loadData();
  },
  _getSavedObjectIndex: function aaDetailView_getSavedObjectIndex()
  {
    if (this.tree.getAttribute("sortDirection") == "" || this.tree.getAttribute("sortDirection") == "natural") {
      return this.array.length - 1 + gInsertOffset;
    } else if (this.buffer) {
      for (var i = 0; i < this.tree.view.rowCount; ++ i) {
        if (this.rowBufferComparator(i)) {
          return i;
        }
      }
    }
    return -1;
  },
  //end manipulation functions
  conditionBuilder: function() {
    if (!this._conditionBuilder)
      this._conditionBuilder = new aaPageConditionBuilder();
    return this._conditionBuilder;
  }
};

/* condition builder */
function aaPageConditionBuilder()
{
  this._fmap = new Array();
}

aaPageConditionBuilder.prototype = {
  getFilters: function() {
    return [];
  },
  convertFilterTypeToColumnID: function(type) {
    return type;
  },
  buildConditions: function(tvFilters) {
    tvFilters.clear();
    //do build filter
    if (!this.buildFilter(tvFilters)) return false;
    if (!this.buildSort(tvFilters)) return false;
    return true;
  },
  buildFilter: function(tvFilters) {
    var fl = this._fmap[nsCI.aaITreeViewFilters.FILTER_FILTER];
    if (fl) {
      if (!fl.value || !fl.type || !tvFilters) {
        return false;
      }
      var tree = document.getElementById("page1.tree");
      tvFilters.addFilter(nsCI.aaITreeViewFilters.FILTER_FILTER,
                          tree.columns.getNamedColumn(this.convertFilterTypeToColumnID(fl.type)),
                          fl.value);
    }
    return true;
  },
  buildSort: function(tvFilters) {
    var fl = this._fmap[nsCI.aaITreeViewFilters.FILTER_SORT];
    if (fl) {
      if (!fl.sortResource || !fl.sortDirection || !tvFilters) {
        return false;
      }
      var tree = document.getElementById("page1.tree");
      tvFilters.addFilter(tvFilters.FILTER_SORT,
                          tree.columns.getNamedColumn(fl.sortResource),
                          fl.sortDirection);
    }
    return true;
  },
  setCondition: function(cType, data) {
    this._fmap[cType] = data;
  }
}
