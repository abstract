/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

var gAbstract = null;

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function doLoadURI(uri)
{
  document.getElementById("content").docShell.
    QueryInterface(nsCI.nsIWebNavigation).
    loadURI(uri, nsCI.nsIWebNavigation.LOAD_FLAGS_NONE, null, null, null);
}

function AbstractLoad()
{
  if (!document.documentElement.hasAttribute("width")) {
    document.documentElement.setAttribute("width",500);
    document.documentElement.setAttribute("height",450);
  }

  var session = Components.classes["@aasii.org/view/session-history;1"]
              .createInstance(nsCI.nsISHistory);
  var web = document.getElementById("content").docShell
    .QueryInterface(nsCI.nsIWebNavigation);
  web.sessionHistory = session;

  gAbstract = new aaAbstractObserver();
  //register observer
  gAbstract.register();
}

function AbstractUnload() {
  //unregister observer
  gAbstract.unregister();
  //delete object
  gAbstract = null;
}

function ViewEntity()
{
  doLoadURI("chrome://abstract/content/entity.xul");
}

function ViewResource()
{
  doLoadURI("chrome://abstract/content/resource.xul");
}

function ManageChart()
{
  doLoadURI("chrome://abstract/content/chart.xul");
}

function ManageRate()
{
  doLoadURI("chrome://abstract/content/rate.xul");
}

function Show3()
{
  setTimeout(DoShow3,0);
}

function DoShow3()
{
  var evnt = document.createEvent("KeyEvents");
  var view = window.frames[0];
  var cmd = view.document.documentElement;

  evnt.initKeyEvent("keypress",true,true,view,false,true,false,false,
      0,"49");
  cmd.dispatchEvent(evnt);

  evnt = document.createEvent("KeyEvents");
  view = window.frames[0];
  cmd = view.document.documentElement;
  evnt.initKeyEvent("keypress",true,true,view,false,true,false,false,
      0,"50");
  cmd.dispatchEvent(evnt);
}

function ViewFlow()
{
  doLoadURI("chrome://abstract/content/flow.xul");
}

function ReportGeneralLedger()
{
  doLoadURI("chrome://abstract/content/ledger.xul");
}

function ReportBalances()
{
  doLoadURI("chrome://abstract/content/balances.xul");
}

function ReportTranscript()
{
  doLoadURI("chrome://abstract/content/transcript.xul");
}

function ProcessTransfer()
{
  doLoadURI("chrome://abstract/content/transfer.xul");
}

function HelpAbout()
{
  doLoadURI("chrome://abstract/content/test.xhtml");
}

function StartPage()
{
  doLoadURI("chrome://abstract/content/start.xul");
}

function CloseFile()
{
  //storageFile.remove(false);
  StartPage();
}

function aaAbstractObserver() {
  this.msgCount = 0;
}
aaAbstractObserver.prototype = {
  register: function() {
    var service = Components.classes["@mozilla.org/observer-service;1"]
                      .getService(Components.interfaces.nsIObserverService);
    service.addObserver(this, "ABSTRACT_INFO", false);
    service.addObserver(this, "ABSTRACT_WARNING", false);
    service.addObserver(this, "ABSTRACT_CRITICAL", false);
    service.addObserver(this, "ABSTRACT_BLOCK", false);
  },
  unregister: function() {
    var service = Components.classes["@mozilla.org/observer-service;1"]
                      .getService(Components.interfaces.nsIObserverService);
    service.removeObserver(this, "ABSTRACT_INFO");
    service.removeObserver(this, "ABSTRACT_WARNING");
    service.removeObserver(this, "ABSTRACT_CRITICAL");
    service.removeObserver(this, "ABSTRACT_BLOCK");
  },
  observe: function(subject, topic, data) {
    try {
      var notify = document.getElementById("abstract.msgBox");
      if (notify) {
        var priority;
        var sbID = 'msg.';
        switch(topic) {
          case 'ABSTRACT_INFO':
            priority = notify.PRIORITY_INFO_HIGH;
            sbID += 'info';
            break;
          case 'ABSTRACT_WARNING':
            priority = notify.PRIORITY_WARNING_HIGH;
            sbID += 'warning';
            break;
          case 'ABSTRACT_CRITICAL':
            priority = notify.PRIORITY_CRITICAL_HIGH;
            sbID += 'critical';
            break;
          case 'ABSTRACT_BLOCK':
            priority = notify.PRIORITY_CRITICAL_BLOCK;
            sbID += 'block';
            break;
        }
        //get data from current local file
        var sbElem = document.getElementById(sbID);
        if (sbElem) {
          //append message for temporary
          notify.appendNotification(sbElem.getString(data), topic + this.msgCount, null, priority, null);
          this.msgCount += 1;
        }
      }
    } catch(e) {
    }
  }
};
