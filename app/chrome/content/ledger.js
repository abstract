/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

const treeViewContract = "@aasii.org/view/tree-general-ledger;1";
const dataSetContract = "@aasii.org/report/general-ledger;1";

var gView = null;
var gTree = null;
var gReport = null;

function aaView()
{
  this.session = document.defaultView
    .QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
  this.tree = document.getElementById("ledger.tree");
  this.tree.view = this.session.createTreeView(treeViewContract);
}

aaView.prototype =
{
  tree : null,
  param : null,

  update: function aaViewUpdate()
  {
    this.tree.view.QueryInterface(nsCI.aaIDataTreeView).update(null);
    this.result = this.tree.view.QueryInterface(nsCI.aaIDataTreeView).dataSet
      .QueryInterface(nsCI.aaIGeneralLedger);
  }
}

function ledgerPageLoad()
{
  gView = new aaView();
  gView.update();
}
