/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

const treeViewContract = "@aasii.org/view/tree-transcript;1";
const reportContract = "@aasii.org/report/transcript;1";

var gView = null;
var gTree = null;
var gReport = null;

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  dump(obj + "\n");
  for (var prop in obj) {
    try {
      dump(prop + ": " + obj[prop] + "\n");
    } catch (e) {
      dump(prop + ": error: " + e.message + "\n");
    }
  }
}

function session()
{
  return document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
}

function loadURI(uri)
{ 
  document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation)
    .loadURI(uri,nsCI.nsIWebNavigation.LOAD_FLAGS_NONE,null,null,null);
}

function aaView()
{
  this.session = document.defaultView
    .QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
  this.param = Components.classes["@aasii.org/report/transcript-param;1"]
    .createInstance(nsCI.aaITranscriptParam);
  this.tree = document.getElementById("transcript.tree");
  this.tree.view = this.session.createTreeView(treeViewContract);
}

aaView.prototype =
{
  QueryInterface : function aaViewQI(iid)
  {
    if (iid.equals(nsCI.nsIObserver) ||
        iid.equals(nsCI.nsISupportsWeakReference) ||
        iid.equals(nsCI.nsISupports))
      return this;

    throw Components.results.NS_ERROR_NO_INTERFACE;
  },

  tree : null,
  param : null,
  result : null,

  observe: function aaViewObserve(aSubject, aTopic, aData)
  {
    if (this.tree && aSubject == this.tree.view) {
      if (aTopic == "tree-flags-change")
        updateBalances();
    }
  },

  update: function aaViewUpdate()
  {
    this.tree.view.QueryInterface(nsCI.aaIDataTreeView).update(this.param);
    this.result = this.tree.view.QueryInterface(nsCI.aaIDataTreeView).dataSet
      .QueryInterface(nsCI.aaITranscript);
  }
}

function transcriptPageLoad()
{
  document.getElementById("filter.dateFrom").addEventListener("change", dateFromChange, false);
  document.getElementById("header.dateFrom").value = document.
    getElementById("filter.dateFrom").value;
  document.getElementById("header.dateTo").value = document.
    getElementById("filter.dateTo").value;
  gView = new aaView();
  if (! gView.param.flow)
    setMode(true);
  addEventListener("resize", transcriptResize, true);
  alignLabels();
  var broadcaster = Components.classes["@mozilla.org/observer-service;1"].
    getService(nsCI.nsIObserverService);
  broadcaster.addObserver(gView, "tree-flags-change", true);
}

function transcriptResize()
{
  alignLabels();
}

function alignLabels()
{
  var width = gView.tree.columns[3].width;
  document.getElementById("header.sumCreditFrom").setAttribute('width',width);
  document.getElementById("header.sumCredits").setAttribute('width',width);
  document.getElementById("header.sumCreditDiff").setAttribute('width',width);
  document.getElementById("header.sumCreditTo").setAttribute('width',width);
}

function openFlow()
{
  if (document.getElementById("viewMode").hasAttribute("disabled")) {
    session().beginQuery();
    addEventListener("pageshow", choosenFlow, true);
  }
  loadURI("chrome://abstract/content/flow.xul");
}

function choosenFlow()
{
  removeEventListener("pageshow", choosenFlow, true);
  var buffer = session().queryBuffer;
  gView.param.flow = buffer
    ? buffer.QueryInterface(nsCI.aaIFlow) : null;
  
  updateFlowLink("filter", gView.param.flow);
}

function dateFromChange(event)
{
}

function dateToChange(event)
{
}

function transcriptLoad()
{
  var date = document.getElementById("filter.dateFrom").dateValue;
  date.setHours(12);
  date.setMinutes(0); date.setSeconds(0); date.setMilliseconds(0);
  gView.param.start = (date.getTime() - 1000 * 60 * 60 * 24) * 1000;

  date = document.getElementById("filter.dateTo").dateValue;
  date.setHours(12);
  date.setMinutes(0); date.setSeconds(0); date.setMilliseconds(0);
  gView.param.end = date.getTime() * 1000;

  gView.update();
  updateFlowLink("header", gView.result.flow);

  var startDate = new Date();
  var endDate = new Date();
  startDate.setTime(gView.param.start/1000 + 1000 * 60 * 60 * 24);
  endDate.setTime(gView.param.end/1000);

  document.getElementById("filter.dateFrom").dateValue = startDate;
  document.getElementById("header.dateFrom").value =
    startDate.toLocaleFormat('%Y-%m-%d');
  document.getElementById("filter.dateTo").dateValue =  endDate;
  document.getElementById("header.dateTo").value =
    endDate.toLocaleFormat('%Y-%m-%d');

  updateBalances();

  setMode(false);
}

function updateBalances()
{
  var sumDebitFrom, sumCreditFrom, sumDebits, sumCredits,
      sumDebitDiff, sumCreditDiff, sumDebitTo, sumCreditTo;
  if (gView.tree && gView.tree.view && gView.tree.view.flags) {
    sumDebitFrom = gView.result.opening &&
      gView.result.opening.side ? gView.result.opening.value.toFixed(2) : "0.00" ;
    sumCreditFrom = gView.result.opening &&
      ! gView.result.opening.side ? gView.result.opening.value.toFixed(2) : "0.00" ;
    sumDebits = gView.result.totalDebitsValue
      ? gView.result.totalDebitsValue.toFixed(2) : "0.00";
    sumCredits = gView.result.totalCreditsValue
      ? gView.result.totalCreditsValue.toFixed(2) : "0.00";
    sumDebitDiff = gView.result.totalDebitsValue
      ? gView.result.totalDebitsDiff.toFixed(2) : "0.00";
    sumCreditDiff = gView.result.totalCreditsValue
      ? gView.result.totalCreditsDiff.toFixed(2) : "0.00";
    sumDebitTo = gView.result.closing &&
      gView.result.closing.side ? gView.result.closing.value.toFixed(2) : "0.00";
    sumCreditTo = gView.result.closing &&
      ! gView.result.closing.side ? gView.result.closing.value.toFixed(2) : "0.00";
  } else {
    sumDebitFrom = gView.result.opening &&
      gView.result.opening.side ? gView.result.opening.amount.toFixed(2) : "0.00" ;
    sumCreditFrom = gView.result.opening &&
      ! gView.result.opening.side ? gView.result.opening.amount.toFixed(2) : "0.00" ;
    sumDebits = gView.result.totalDebits ? gView.result.totalDebits.toFixed(2) : "0.00";
    sumCredits = gView.result.totalCredits ? gView.result.totalCredits.toFixed(2) : "0.00";
    sumDebitDiff = "";
    sumCreditDiff = "";
    sumDebitTo = gView.result.closing &&
      gView.result.closing.side ? gView.result.closing.amount.toFixed(2) : "0.00";
    sumCreditTo = gView.result.closing &&
      ! gView.result.closing.side ? gView.result.closing.amount.toFixed(2) : "0.00";
  }

  document.getElementById("header.sumDebitFrom").value = sumDebitFrom;
  document.getElementById("header.sumCreditFrom").value = sumCreditFrom;
  document.getElementById("header.sumDebits").value = sumDebits;
  document.getElementById("header.sumCredits").value = sumCredits;
  document.getElementById("header.sumDebitDiff").value = sumDebitDiff;
  document.getElementById("header.sumCreditDiff").value = sumCreditDiff;
  document.getElementById("header.sumDebitTo").value = sumDebitTo;
  document.getElementById("header.sumCreditTo").value = sumCreditTo;

}

function updateFlowLink(part, flow)
{
  if (flow)
    document.getElementById(part + ".flow").value = flow.tag;
  else
    document.getElementById(part + ".flow").value = document.
      getElementById("bundle_abstract").getString("link.choose");
}

function setMode(isChange)
{
  var newMode, oldMode;

  if (isChange) {
    newMode = document.getElementById("changeMode");
    oldMode = document.getElementById("viewMode");

    updateFlowLink("filter", gView.param.flow);
  } else {
    newMode = document.getElementById("viewMode");
    oldMode = document.getElementById("changeMode");
  }

  oldMode.setAttribute("disabled","true");
  oldMode.setAttribute("collapsed","true");

  newMode.removeAttribute("disabled");
  newMode.removeAttribute("collapsed");
}

function filterChange()
{
  setMode(true);
}

function filterReset()
{
  setMode(false);
}

function unitSwitch()
{
  if (gView && gView.tree && gView.tree.view)
    gView.tree.view.QueryInterface(nsCI.aaIDataTreeView).flags =
      document.getElementById("units.mode").selectedIndex;
}

function Act()
{
  if (document.getElementById("changeMode").hasAttribute("disabled")) {
    filterChange();
  } else {
    transcriptLoad();
  }
}

function Switch()
{
  document.getElementById("units.mode").selectedIndex =  (document
    .getElementById("units.mode").selectedIndex) ? 0 : 1;
}
