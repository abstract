/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const treeViewContract = "@aasii.org/view/tree-resource;1";
const assetContract = "@aasii.org/base/asset;1";
const moneyContract = "@aasii.org/base/money;1";

function resourcePageLoad()
{
  view = new aaResourceView();
  page1Load();
}

function resourceTagType(event)
{
  var resource = view.buffer.QueryInterface(nsCI.aaIResource);
  if (resource.type == nsCI.aaIResource.TYPE_ASSET)
    resource.setTag(view.tag.value);
  else
    resource.setAlphaCode(view.tag.value);
  page1SaveUpdate(resource);
}

function resourceCodeType(event)
{
  view.buffer.setNumCode(view.code.value);
  page1SaveUpdate();
}

function resourceTypeSwitch()
{
  view.buffer = view.createNewNode(true);
  page1Update();
}

function aaResourceView()
{
  this.tag = document.getElementById("resource.tag");
  this.type = document.getElementById("resource.isMoney");
  this.isMoney = document.getElementById("resource.moneyMode");
  this.code = document.getElementById("resource.code");
  this._top = this.tag;
}

aaResourceView.prototype = new aaDetailView;
aaResourceView.prototype.read = function (node) {
    var resource = node.QueryInterface(  nsCI.aaIResource);
    this.tag.value = resource.tag;
    this.setType(resource);
};
aaResourceView.prototype.reset = function () {
    this.tag.value = "";
    this.setType(null);
};
aaResourceView.prototype.copy = function () { 
  var resource = this.array.queryElementAt(this.index, nsCI.aaIResource);
  if (resource.type == nsCI.aaIResource.TYPE_ASSET) {
    this.buffer.id = resource.id;
    this.buffer.setTag(resource.tag);
  }
  else {
    this.buffer.setAlphaCode(resource.tag);
    this.buffer.setNumCode(resource.id);
  }
};
aaResourceView.prototype.createNewNode = function (isNew) {
  let nodeContract = assetContract;
  if (isNew) {
    if (this.buffer && this.buffer.type == nsCI.aaIResource.TYPE_ASSET)
      nodeContract = moneyContract;
  }
  else {
    var resource = this.array.queryElementAt(this.index, nsCI.aaIResource);
    if (resource.type == nsCI.aaIResource.TYPE_MONEY)
      nodeContract = moneyContract;
  }
  return Components.classes[nodeContract].createInstance(nsCI.aaIDataNode);
};
aaResourceView.prototype.setType = function (resource) { 
  var isMoney = resource ? (resource.type == nsCI.aaIResource.TYPE_MONEY) : 0;
  if (isMoney)
    this.isMoney.removeAttribute("collapsed");
  else
    this.isMoney.setAttribute("collapsed","true");
  this.type.checked = isMoney;
  this.code.value = isMoney ? resource.id : "";
  if (isMoney)
    ;
};
aaResourceView.prototype.rowBufferComparator = function(i) {
  return this.tree.view.getCellText(i, this.tree.columns[1]) == this.buffer.QueryInterface(nsCI.aaIResource).tag;
};
aaResourceView.prototype.conditionBuilder = function() {
  if (!this._conditionBuilder)
    this._conditionBuilder = new aaResourceConditionBuilder();
  return this._conditionBuilder;
}

function aaResourceConditionBuilder()
{
}

aaResourceConditionBuilder.prototype = new aaPageConditionBuilder();
aaResourceConditionBuilder.prototype.getFilters = function() {
  //get element bundle
  var sbElem = document.getElementById("resource.bundle");
  if (sbElem) {
    return [
      { label: sbElem.getString("resource.filter.name"), value: "tree.name" }
    ];
  }
  return [];
}
