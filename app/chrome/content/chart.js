/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;
const dataSetContract = "@aasii.org/storage/load-chart;1";

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  dump(obj + "\n");
  for (var prop in obj) {
    try {
      dump(prop + ": " + obj[prop] + "\n");
    } catch (e) {
      dump(prop + ": error: " + e.message + "\n");
    }
  }
}

function session()
{
  return document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
}

function loadURI(uri)
{ 
  document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation)
    .loadURI(uri,nsCI.nsIWebNavigation.LOAD_FLAGS_NONE,null,null,null);
}

var gView = null;
var gLoaded = false;

function chartPageLoad()
{
  var loader = Components.classes[dataSetContract]
    .createInstance(nsCI.aaISqlRequest);
  var charts = session().load(loader);
  gView = new aaView();
  if (charts.length > 0) {
    gView.chart = charts.queryElementAt(0, nsCI.aaIChart);
    document.getElementById("chart.resource").value =
      gView.chart.currency.tag;
    notifyReady();
  } else {
    gView.chart = Components.classes["@aasii.org/accounting/chart;1"]
      .createInstance(nsCI.aaIChart);
  }

  var broadcaster = Components.classes["@mozilla.org/observer-service;1"].
    getService(nsCI.nsIObserverService);
  broadcaster.addObserver(gView, "chart-currency-change", true);
}

function openResource(side)
{
  if (! document.getElementById("canSet").hasAttribute("disabled")) {
    session().beginQuery();
    addEventListener("pageshow", chosenResource, true);
  }
  loadURI("chrome://abstract/content/resource.xul");
}

function chosenResource()
{ 
  removeEventListener("pageshow", chosenResource, true);
  var buffer = session().queryBuffer;
  if (buffer && gView && gView.chart)
    gView.chart.currency = buffer;
}

function updateResource()
{
  if (gView.chart.currency)
    document.getElementById("chart.resource").value
      = gView.chart.currency.tag;
  else
    document.getElementById("chart.resource").value = "";
}

function setResource()
{
  session().save(gView.chart, null);
  notifyReady();
}

function notifyReady()
{
  gLoaded = true;
  document.getElementById("canSet").setAttribute("disabled", "true");
  document.getElementById("canSet").setAttribute("collapsed", "true");
  document.getElementById("isSet").removeAttribute("disabled");
  document.getElementById("isSet").removeAttribute("collapsed");
}

function aaView()
{
}

aaView.prototype =
{
  QueryInterface : function aaViewQI(iid)
  {
    if (iid.equals(nsCI.nsIObserver) ||
        iid.equals(nsCI.nsISupportsWeakReference) ||
        iid.equals(nsCI.nsISupports))
      return this;

    throw Components.results.NS_ERROR_NO_INTERFACE;
  },

  chart : null,

  observe: function aaViewObserve(aSubject, aTopic, aData)
  {
    if (this.chart && aSubject == this.chart) {
      if (aTopic == "chart-currency-change")
        updateResource();
    }
  }
}
