/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const treeViewContract = "@aasii.org/view/tree-rule;1";
const nodeContract = "@aasii.org/base/rule;1";
const dataSetContract = "@aasii.org/storage/load-flow;1";

function rulePageLoad()
{
  view = new aaRuleView();
  gCanDiscard = false;
  page1Load(session().choosing);
}

function ruleTagInsert()
{
  var rule = view.buffer.QueryInterface(nsCI.aaIRule);
  rule.tag = view.tag.value;
  page1SaveUpdate();
}

function ruleRateInsert()
{
  var rule = view.buffer.QueryInterface(nsCI.aaIRule);
  rule.rate = view._rate.value;
  page1SaveUpdate();
}

function ruleSelectTakeFrom()
{
  session().beginQuery();
  addEventListener("pageshow", chosenTakeFrom, true);
  loadURI("chrome://abstract/content/flow.xul");
}

function chosenTakeFrom() {
  removeEventListener("pageshow", chosenTakeFrom, true);
  var buffer = session().queryBuffer;
  try {
    view.buffer.QueryInterface(nsCI.aaIRule).takeFrom = buffer.QueryInterface(nsCI.aaIFlow)
  } catch (e) {
    view.buffer.QueryInterface(nsCI.aaIRule).takeFrom = null;
  }
  view.load();
  page1SaveUpdate();
}

function ruleSelectGiveTo()
{
  session().beginQuery();
  addEventListener("pageshow", chosenGiveTo, true);
  loadURI("chrome://abstract/content/flow.xul");
}

function chosenGiveTo() {
  removeEventListener("pageshow", chosenGiveTo, true);
  var buffer = session().queryBuffer;
  try {
    view.buffer.QueryInterface(nsCI.aaIRule).giveTo = buffer.QueryInterface(nsCI.aaIFlow)
  } catch (e) {
    view.buffer.QueryInterface(nsCI.aaIRule).giveTo = null;
  }
  view.load();
  page1SaveUpdate();
}

function ruleChangeFactSelect()
{
  if (view && view.buffer) {
    var rule = view.buffer.QueryInterface(nsCI.aaIRule);
    var idx = view._changeFact.selectedIndex;
    if (0 == idx) {
      rule.changeSide = true;
      rule.factSide = false;
    } else if(1 == idx) {
      rule.changeSide = true;
      rule.factSide = true;
    } else if(2 == idx) {
      rule.changeSide = false;
      rule.factSide = false;
    } else {
      rule.changeSide = false;
      rule.factSide = true;
    }
    page1SaveUpdate();
  }
}

function aaRule_Save() {
  try {
    var pBuffer = this.prevBuffer();
    if (pBuffer) {
      pBuffer.QueryInterface(nsCI.aaIFlow).storeRule(view.buffer);
      this.buffer = null;
    }
  } catch(anError) {
  }
}

function aaRule_Submit() {
  this.history.choice = this._container_buffer;
}

function aaRule_prevBuffer() {
  var idx = this.history.index;
  if (!this._container_buffer && idx > 0) {
    this._container_buffer = this.history.getEntryAtIndex(idx - 1, false).QueryInterface(nsCI.aaIPageView).buffer;
  }
  return this._container_buffer;
}

function aaRule_LoadData() {
  page1LoadData(this.prevBuffer());
}

/* DetailView subclassing */
function aaRuleView()
{
  this.tag = document.getElementById('rule.tag');
  this._rate = document.getElementById('rule.rate');
  this._giveTo = document.getElementById('rule.tb.giveto');
  this._takeFrom = document.getElementById('rule.tb.takefrom');
  this._changeFact = document.getElementById('rule.change.fact.side');
  this._top = this.tag;

  this.prevBuffer = aaRule_prevBuffer;
  this._save = aaRule_Save;
  this._submit = aaRule_Submit;
  this._loadData = aaRule_LoadData;

  this._container_buffer = null;
}

aaRuleView.prototype = new aaDetailView();
aaRuleView.prototype.reset = function ()
{
  this.tag.value = '';
  this._rate.value = '';
  this._giveTo.value = '';
  this._takeFrom.value = '';
  this._changeFact.selectedIndex = 0;
};
aaRuleView.prototype.read = function(node) {
  var rule = node.QueryInterface(nsCI.aaIRule);
  var idx = 0;

  this.tag.value = rule.tag;
  this._rate.value = rule.rate;
  if (rule.changeSide == true && rule.factSide == true) {
    idx = 1;
  } else if (rule.changeSide == true && rule.factSide == false) {
    idx = 0;
  } else if (rule.changeSide == false && rule.factSide == true) {
    idx = 3;
  } else {
    idx = 2;
  }
  this._changeFact.selectedIndex = idx;
  //find combobox item
  if (rule.giveTo) {
    this._giveTo.value = rule.giveTo.tag;
  } else {
    this._giveTo.value = "";
  }
  if (rule.takeFrom) {
    this._takeFrom.value = rule.takeFrom.tag;
  } else {
    this._takeFrom.value = "";
  }
  //for set attribute after selection
  gCanSave.setAttribute("disabled","true");
};
aaRuleView.prototype.copy = function() {
  var rule = this.array.queryElementAt(this.index, nsCI.aaIRule);
  var bufRule = this.buffer.QueryInterface(nsCI.aaIRule);
  //do copy
  bufRule.id = rule.id;
  bufRule.tag = rule.tag;
  bufRule.flow = rule.flow;
  bufRule.changeSide = rule.changeSide;
  bufRule.factSide = rule.factSide;
  bufRule.takeFrom = rule.takeFrom;
  bufRule.giveTo = rule.giveTo;
  bufRule.rate = rule.rate;
};
aaRuleView.prototype.rowBufferComparator = function(i) {
  return this.tree.view.getCellText(i, this.tree.columns[0]) == this.buffer.QueryInterface(nsCI.aaIRule).tag;
};
aaRuleView.prototype.conditionBuilder = function() {
  if (!this._conditionBuilder)
    this._conditionBuilder = new aaRuleConditionBuilder();
  return this._conditionBuilder;
}

function aaRuleConditionBuilder()
{
}

aaRuleConditionBuilder.prototype = new aaPageConditionBuilder();
aaRuleConditionBuilder.prototype.getFilters = function() {
  //get element bundle
  var sbElem = document.getElementById("rule.bundle");
  if (sbElem) {
    return [
      { label: sbElem.getString("rule.filter.tag"), value: "tree.tag" }
    ];
  }
  return [];
}
