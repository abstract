/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  jsdump(obj);
  for (var prop in obj) {
    try {
      jsdump(prop + ": " + obj[prop]);
    } catch (e) {
      jsdump(prop + ": error: " + e.message);
    }
  }
}

const treeViewContract = "@aasii.org/view/tree-balance;1";
const dataSetContract = "@aasii.org/report/balance-sheet;1";

var view = null;
var gTree = null;

function aaView()
{
  this.session = document.defaultView
    .QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
  this.param = Components.classes["@aasii.org/report/balance-param;1"]
    .createInstance(nsCI.aaIBalanceSheetParam);
}

aaView.prototype =
{
  QueryInterface : function aaViewQI(iid)
  {
    if (iid.equals(nsCI.nsIObserver) ||
        iid.equals(nsCI.nsISupportsWeakReference) ||
        iid.equals(nsCI.nsISupports))
      return this;

    throw Components.results.NS_ERROR_NO_INTERFACE;
  },

  tree : null,

  session : null,

  param : null,

  observe: function aaViewObserve(aSubject, aTopic, aData)
  {
    if (this.tree && aSubject == this.tree.view) {
      if (aTopic == "tree-view-load")
        updateTotals();
    }
  },

  update: function aaViewUpdate()
  {
    this.tree.view.QueryInterface(nsCI.aaIDataTreeView).update(this.param);
  }
}

function balancesPageLoad()
{
  view = new aaView();
  view.tree = document.getElementById("balances.tree");
  view.tree.view = view.session.createTreeView(treeViewContract);
  var broadcaster = Components.classes["@mozilla.org/observer-service;1"].
    getService(nsCI.nsIObserverService);
  broadcaster.addObserver(view, "tree-view-load", true);
  addEventListener("resize", balancesResize, true);
  document.getElementById("filter.date")
    .addEventListener("change", dateChange, false);
  alignLabels();
  view.update();
}

function balancesResize()
{
  alignLabels();
}

function alignLabels()
{
  var width = view.tree.columns[4].width;
  document.getElementById("footer.sumCredit").setAttribute('width',width);
  width = view.tree.columns[3].width;
  document.getElementById("footer.sumDebit").setAttribute('width',width);
}

function dateChange()
{
  var date = document.getElementById("filter.date").dateValue;
  date.setHours(12); date.setMinutes(0); date.setSeconds(0); date.setMilliseconds(0);
  view.param.date =  date.getTime() * 1000;
  view.update();
}

function updateTotals()
{
  var sumDebit, sumCredit;
  var balance = view.tree.view.QueryInterface(nsCI.aaIDataTreeView).dataSet
    .QueryInterface(nsCI.aaIBalanceSheet);
  if (balance) {
    sumDebit = balance.totalAssets.toFixed(2);
    sumCredit = balance.totalLiabilities.toFixed(2);
  } else {
    sumDebit = "0.00";
    sumCredit = "0.00";
  }
  document.getElementById("footer.sumDebit").value = sumDebit;
  document.getElementById("footer.sumCredit").value = sumCredit;
}

function unitSwitch()
{
  if (view && view.tree && view.tree.view)
    view.tree.view.QueryInterface(nsCI.aaIDataTreeView).flags =
      document.getElementById("units.mode").selectedIndex;
}

