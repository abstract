/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


const nsCI = Components.interfaces;
var storageFile = null;

function openStorDialog() { 
  var nsIFilePicker = Components.interfaces.nsIFilePicker;
  var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
  fp.init(window, "Select File", nsIFilePicker.modeOpen);
  fp.appendFilter("SQLite Files","*.sqlite");
  fp.appendFilter("All Files","*.*");
  var res = fp.show();
  if (res == nsIFilePicker.returnOK){
    storageFile = fp.file;
    return true;
  }
  return false;
}

function setExistingStorage() {
  if (openStorDialog())
    doSetNewStorage(false);
}

function createStorDialog() { 
  var nsIFilePicker = Components.interfaces.nsIFilePicker;
  var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
  fp.init(window, "Create File", nsIFilePicker.modeSave);
  fp.appendFilter("SQLite Files","*.sqlite");
  fp.appendFilter("All Files","*.*");
  fp.defaultString = "new.sqlite";
  var res = fp.show();
  if (res == nsIFilePicker.returnCancel) {
    return false;
  }
  else if (res == nsIFilePicker.returnReplace) {
    fp.file.remove(false);
  }

  storageFile = fp.file;

  return true;
}

function setNewStorage() {
  if (createStorDialog())
    doSetNewStorage(true);
}

function doSetNewStorage(isNew)
{
  var session = Components.classes["@aasii.org/view/session-history;1"]
            .createInstance(nsCI.nsISHistory);
  var web = document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation)
  web.sessionHistory = session;
  session.setFile(storageFile);

  web.loadURI(isNew ? "chrome://abstract/content/chart.xul" : 
      "chrome://abstract/content/balances.xul",
      nsCI.nsIWebNavigation.LOAD_FLAGS_NONE, null, null, null);
}
