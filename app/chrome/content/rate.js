/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const treeViewContract = "@aasii.org/view/tree-quote;1";
const nodeContract = "@aasii.org/base/quote;1";
const dataSetContract = "@aasii.org/storage/load-quote;1";

function session()
{
  return document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
}

function loadURI(uri)
{ 
  document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation)
    .loadURI(uri,nsCI.nsIWebNavigation.LOAD_FLAGS_NONE,null,null,null);
}

function aaRateView()
{
  this._resource = document.getElementById("quote.resource");
  this._date = document.getElementById("quote.date");
  this._rate = document.getElementById("quote.rate");
}

aaRateView.prototype = new aaDetailView();
aaRateView.prototype.read = function (node) {
  this._rate.value = node.rate;
  updateResource(node);
}; 
aaRateView.prototype.reset = function () {
  document.getElementById("quote.resource").value = document.
    getElementById("bundle_abstract").getString("link.choose");
  document.getElementById("quote.rate").value = "";
};
aaRateView.prototype.copy = function () { 
};
aaRateView.prototype.createNewNode = function () {
  var quote = Components.classes[nodeContract].createInstance(nsCI.aaIQuote);
  var time = this._date.dateValue;
  time.setHours(12);
  quote.time = time.getTime() * 1000;
  return quote;
}
aaRateView.prototype.QueryInterface = function (iid)
{
  if (iid.equals(nsCI.nsIObserver) ||
      iid.equals(nsCI.nsISupportsWeakReference) ||
      iid.equals(nsCI.nsISupports))
    return this;

  throw Components.results.NS_ERROR_NO_INTERFACE;
}
aaRateView.prototype.observe =  function (aSubject, aTopic, aData)
{
  if (this.buffer && aSubject == this.buffer) {
    if (aTopic == "quote-resource-change") {
      updateResource(this.buffer);
      page1SaveUpdate();
    }
  }
}
aaRateView.prototype.rowBufferComparator = function(i) {
  return this.tree.view.getCellText(i, this.tree.columns[0]) == this.buffer.QueryInterface(nsCI.aaIQuote).resource.tag;
};
aaRateView.prototype.conditionBuilder = function() {
  if (!this._conditionBuilder)
    this._conditionBuilder = new aaRateConditionBuilder();
  return this._conditionBuilder;
}

function aaRateConditionBuilder()
{
}

aaRateConditionBuilder.prototype = new aaPageConditionBuilder();
aaRateConditionBuilder.prototype.getFilters = function() {
  //get element bundle
  var sbElem = document.getElementById("rate.bundle");
  if (sbElem) {
    return [
      { label: sbElem.getString("rate.filter.resource"), value: "tree.resource" }
    ];
  }
  return [];
}

function ratePageLoad()
{
  view = new aaRateView();
  gWORM = true;
  document.getElementById("quote.date").addEventListener("change",
      dateChange, false);
  var broadcaster = Components.classes["@mozilla.org/observer-service;1"].
    getService(nsCI.nsIObserverService);
  broadcaster.addObserver(view, "quote-resource-change", true);
  page1Load();
}

function dateChange()
{
  var date = document.getElementById("quote.date").dateValue;
  date.setHours(12);
  view.buffer.time = date.getTime()  * 1000;
  page1SaveUpdate();
}

function rateType()
{
  view.buffer.rate = document.getElementById("quote.rate").value;
  page1SaveUpdate();
}

function chooseResource()
{
  session().beginQuery();
  addEventListener("pageshow", chosenResource, true);
  loadURI("chrome://abstract/content/resource.xul");
}

function chosenResource()
{
  removeEventListener("pageshow", chosenResource, true);
  var buffer = session().queryBuffer;
  if (!buffer)
    return;
  view.buffer.resource = buffer;
}

function updateResource(quote)
{
  view._resource.value = quote.resource
    ? quote.resource.tag
    : document.getElementById("bundle_abstract").getString("link.choose");
}
