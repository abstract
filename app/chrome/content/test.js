/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

var nsCI = Components.interfaces;

function objdump(obj)
{
  jsdump(obj);
  for (var prop in obj) {
    try {
      jsdump(prop + ": " + obj[prop]);
    } catch (e) {
      jsdump(prop + ": error: " + e.message);
    }
  }
}

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function goBack()
{ 
  var wn = document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation);
  wn.goBack();
}
