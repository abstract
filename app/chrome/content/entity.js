/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const treeViewContract = "@aasii.org/view/tree-entity;1";
const nodeContract = "@aasii.org/base/entity;1";

function entityPageLoad()
{
  view = new aaEntityView();
  page1Load();
}

function entityTagType(event)
{
  var entity = view.buffer.QueryInterface(nsCI.aaIEntity);
  entity.tag = view.tag.value;
  page1SaveUpdate();
}

function aaEntityView()
{
  this.tag = document.getElementById("entity.tag");
  this._top = this.tag;
}

aaEntityView.prototype = new aaDetailView();
aaEntityView.prototype.read = function (node) { 
  var entity = node.QueryInterface(nsCI.aaIEntity);
  this.tag.value = entity.tag;
};
aaEntityView.prototype.reset = function () {
  this.tag.value = "";
};
aaEntityView.prototype.copy = function () { 
  var source = this.array.queryElementAt(this.index,
      nsCI.aaIEntity);
  var target = this.buffer.QueryInterface(nsCI.aaIEntity);
  target.id = source.id;
  target.tag = source.tag;
};
aaEntityView.prototype.rowBufferComparator = function(i) {
  return this.tree.view.getCellText(i, this.tree.columns[0]) == this.buffer.QueryInterface(nsCI.aaIEntity).tag;
};
aaEntityView.prototype.conditionBuilder = function() {
  if (!this._conditionBuilder)
    this._conditionBuilder = new aaEntityConditionBuilder();
  return this._conditionBuilder;
}

function aaEntityConditionBuilder()
{
}

aaEntityConditionBuilder.prototype = new aaPageConditionBuilder();
aaEntityConditionBuilder.prototype.getFilters = function() {
  //get element bundle
  var sbElem = document.getElementById("entity.bundle");
  if (sbElem) {
    return [
      { label: sbElem.getString("entity.filter.name"), value: "tree.name" }
    ];
  }
  return [];
}

