/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI = Components.interfaces;

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  dump(obj + "\n");
  for (var prop in obj) {
    try {
      dump(prop + ": " + obj[prop] + "\n");
    } catch (e) {
      dump(prop + ": error: " + e.message + "\n");
    }
  }
}

function session()
{
  return document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation).sessionHistory;
}

function loadURI(uri)
{ 
  document.defaultView.QueryInterface(nsCI.nsIInterfaceRequestor)
    .getInterface(nsCI.nsIWebNavigation)
    .loadURI(uri,nsCI.nsIWebNavigation.LOAD_FLAGS_NONE,null,null,null);
}

var gView = null;
var gEvent = null;
var gFact = null;
var gTransaction = null;
var gResource = null;

function transferPageLoad()
{
  gFact = Components.classes["@aasii.org/base/fact;1"].
    createInstance(nsCI.aaIFact);
  gEvent = Components.classes["@aasii.org/base/event;1"].
    createInstance(nsCI.aaIEvent);
  dateChange();
  gView = new aaView();
  var broadcaster = Components.classes["@mozilla.org/observer-service;1"].
    getService(nsCI.nsIObserverService);
  broadcaster.addObserver(gView, "fact-from-flow-change", true);
  broadcaster.addObserver(gView, "fact-to-flow-change", true);
  document.getElementById("transfer.date").addEventListener("change",
      dateChange, false);
  updateAll();
  gFact.broadcasting = true;
}

function dateChange()
{
  var date = document.getElementById("transfer.date").dateValue;
  date.setHours(12);
  date.setMinutes(0); date.setSeconds(0); date.setMilliseconds(0);
  gEvent.time = date.getTime()  * 1000;
}

function openFlow(side)
{
  session().beginQuery();
  if (! side)
    addEventListener("pageshow", chosenFrom, true);
  else
    addEventListener("pageshow", chosenTo, true);
  loadURI("chrome://abstract/content/flow.xul");
}

function amountType()
{
  gFact.amount = document.getElementById("transfer.amount").value;
}

function transferCommit()
{
  if (gTransaction)
    return;
  var transaction = Components.classes["@aasii.org/base/transaction;1"].
    createInstance(nsCI.aaITransaction);
  gEvent.appendElement(gFact, false);
  session().save(gEvent, null);
  transaction.fact = gFact;
  try {
    session().save(transaction, null);
    gTransaction = transaction;
    document.getElementById("canCommit").setAttribute("disabled","true");
    document.getElementById("canReset").removeAttribute("disabled");
    document.getElementById("canReset").removeAttribute("collapsed");
    updateValue();
  } catch(e) {
    session().save(gEvent, gEvent);
  }
}

function transferReset()
{
  var time = gEvent.time;
  gTransaction = null;
  gFact = Components.classes["@aasii.org/base/fact;1"].
    createInstance(nsCI.aaIFact);
  gEvent = Components.classes["@aasii.org/base/event;1"].
    createInstance(nsCI.aaIEvent);
  gEvent.time = time;
  document.getElementById("canCommit").removeAttribute("disabled");
  document.getElementById("canReset").setAttribute("disabled","true");
  document.getElementById("canReset").setAttribute("collapsed","true");
  updateAll();
  gFact.broadcasting = true;
}

function chosenFrom()
{
  removeEventListener("pageshow", chosenFrom, true);
  var buffer = session().queryBuffer;
  if (buffer)
    gFact.takeFrom = buffer.QueryInterface(nsCI.aaIFlow);
}

function chosenTo()
{
  removeEventListener("pageshow", chosenTo, true);
  var buffer = session().queryBuffer;
  if (buffer)
    gFact.giveTo = buffer.QueryInterface(nsCI.aaIFlow);
}

function updateAll()
{
  updateFromFlow();
  updateToFlow();
  updateAmount();
  updateValue();
}

function loadBalance(flow)
{
  if (! flow)
    return null;

  let loader = Components.classes["@aasii.org/storage/load-balance;1"]
    .createInstance(nsCI.aaISqlRequest);
  loader.setFilter(flow);
  let result = session().load(loader);
  if (result && result.length)
    return result.queryElementAt(0, nsCI.aaIBalance);
}

function updateFromFlow()
{
  let balance = null;

  if (gFact && gFact.takeFrom) {
    document.getElementById("transfer.fromFlow").value = gFact.takeFrom.tag;
    balance = loadBalance(gFact.takeFrom);
  }
  else {
    document.getElementById("transfer.fromFlow").value = document.
      getElementById("bundle_abstract").getString("link.choose");
  }

  if (balance) {
    if (balance.side) {
      document.getElementById("transfer.fromDebitAmount").value =
        balance.amount.toFixed(2);
      document.getElementById("transfer.fromCreditAmount").value = "-";
    }
    else {
      document.getElementById("transfer.fromDebitAmount").value = "-";
      document.getElementById("transfer.fromCreditAmount").value =
        balance.amount.toFixed(2);
    }
  }
  else {
    document.getElementById("transfer.fromDebitAmount").value = "-";
    document.getElementById("transfer.fromCreditAmount").value = "-";
  }
}

function updateToFlow()
{
  let balance;
  if (gFact && gFact.giveTo) {
    document.getElementById("transfer.toFlow").value = gFact.giveTo.tag;
    balance = loadBalance(gFact.giveTo);
  }
  else
    document.getElementById("transfer.toFlow").value = document.
      getElementById("bundle_abstract").getString("link.choose");

  if (balance) {
    if (balance.side) {
      document.getElementById("transfer.toDebitAmount").value =
        balance.amount.toFixed(2);
      document.getElementById("transfer.toCreditAmount").value = "-";
    }
    else {
      document.getElementById("transfer.toDebitAmount").value = "-";
      document.getElementById("transfer.toCreditAmount").value =
        balance.amount.toFixed(2);
    }
  }
  else {
    document.getElementById("transfer.toDebitAmount").value = "-";
    document.getElementById("transfer.toCreditAmount").value = "-";
  }
}

function updateAmount()
{
  if (gFact)
    document.getElementById("transfer.amount").value = gFact.amount;
  else
    document.getElementById("transfer.amount").value = "0";
}

function updateValue()
{
  if (gTransaction) {
    document.getElementById("transfer.value").value = gTransaction.value.toFixed(2);
    document.getElementById("transfer.earnings").value = gTransaction.hasEarnings ? 
      gTransaction.earnings.toFixed(2) : "";
  } else {
    document.getElementById("transfer.value").value = "";
    document.getElementById("transfer.earnings").value = "";
  }
}

function aaView()
{
}

aaView.prototype =
{
  QueryInterface : function aaViewQI(iid)
  {
    if (iid.equals(nsCI.nsIObserver) ||
        iid.equals(nsCI.nsISupportsWeakReference) ||
        iid.equals(nsCI.nsISupports))
      return this;

    throw Components.results.NS_ERROR_NO_INTERFACE;
  },

  observe: function aaViewObserve(aSubject, aTopic, aData)
  {
    if (aSubject == gFact) {
      if (aTopic == "fact-from-flow-change")
        updateFromFlow();
      else if (aTopic == "fact-to-flow-change")
        updateToFlow();
    }
  }
}
