/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const treeViewContract = "@aasii.org/view/tree-flow;1";
const nodeContract = "@aasii.org/base/flow;1";
var dataSetContract = "@aasii.org/storage/load-flow;1";

var gIndirectRate = 0;

function flowPageLoad()
{
  view = new aaFlowView();
  gWORM = true;
  var isChoosing = session().choosing;
  if (isChoosing) {
    dataSetContract += "?type=ext";
    gInsertOffset = -1;
  }
  page1Load(isChoosing);
}

function flowTagType()
{
  var flow = view.buffer.QueryInterface(nsCI.aaIFlow);
  flow.tag = view._tag.value;
  page1SaveUpdate();
}

function rateType()
{
  var flow = view.buffer.QueryInterface(nsCI.aaIFlow);
  var rate = gIndirectRate ? 1 / view._rate.value : view._rate.value;
  flow.rate = rate;
  page1SaveUpdate();
}

function chooseEntity()
{
  session().beginQuery();
  addEventListener("pageshow", chosenEntity, true);
  loadURI("chrome://abstract/content/entity.xul");
}

function chosenEntity()
{
  removeEventListener("pageshow", chosenEntity, true);
  var buffer = session().queryBuffer;
  try {
    view.buffer.QueryInterface(nsCI.aaIFlow).entity =
      buffer.QueryInterface(nsCI.aaIEntity)
  } catch (e) {
    view.buffer.QueryInterface(nsCI.aaIFlow).entity = null;
  }
  view.load();
  page1SaveUpdate();
}

function checkIsOffBalance() {
  var flow = view.buffer.QueryInterface(nsCI.aaIFlow);
  flow.isOffBalance = view._offBalance.checked;
  page1SaveUpdate();
}

function chooseGiveResource()
{
  session().beginQuery();
  addEventListener("pageshow", chosenGiveResource, true);
  loadURI("chrome://abstract/content/resource.xul");
}

function chosenGiveResource()
{
  removeEventListener("pageshow", chosenGiveResource, true);
  var buffer = session().queryBuffer;
  try {
    view.buffer.QueryInterface(nsCI.aaIFlow).giveResource =
      buffer.QueryInterface(nsCI.aaIResource)
  } catch (e) {
    view.buffer.QueryInterface(nsCI.aaIFlow).giveResource = null;
  }
  view.load();
  page1SaveUpdate();
}

function chooseTakeResource()
{
  session().beginQuery();
  addEventListener("pageshow", chosenTakeResource, true);
  loadURI("chrome://abstract/content/resource.xul");
}

function chosenTakeResource()
{
  removeEventListener("pageshow", chosenTakeResource, true);
  var buffer = session().queryBuffer;
  try {
    view.buffer.QueryInterface(nsCI.aaIFlow).takeResource =
      buffer.QueryInterface(nsCI.aaIResource)
  } catch (e) {
    view.buffer.QueryInterface(nsCI.aaIFlow).takeResource = null;
  }
  view.load();
  page1SaveUpdate();
}

function rateSwitch()
{
  var indirect = document.getElementById("rate.direction").selectedIndex;
  if (indirect == gIndirectRate)
    return;
  gIndirectRate = indirect;
  page1UpdateStatus(true);
}

function flowOnSetIndex()
{
  var indirect = calcBestDirection();
  if (indirect == gIndirectRate)
    return;
  gIndirectRate = indirect;
  document.getElementById("rate.direction").selectedIndex = gIndirectRate;
}

function calcBestDirection()
{
  return 0;
}

function showRules()
{
  session().beginQuery();
  addEventListener("pageshow", showRulesAdd, true);
  loadURI("chrome://abstract/content/rule.xul");
}

function showRulesAdd() {
  removeEventListener("pageshow", showRulesAdd, true);
  var buffer = session().queryBuffer;
  try {
    for (var i = 0; i < buffer.QueryInterface(nsCI.nsIArray).length; i++) {
      var rule = buffer.queryElementAt(i, nsCI.aaIRule);
      rule.flow = view.buffer;
      view.buffer.QueryInterface(nsCI.aaIFlow).storeRule(rule);
    }
  } catch (e) {
  }
  view.load();
  page1SaveUpdate();
}

/* DetailView subclassing */

function aaFlowView()
{
  this._tag = document.getElementById("flow.tag");
  this._offBalance = document.getElementById("flow.isoffbalance");
  this._entity = document.getElementById("entity.tag");
  this._give = document.getElementById("give.tag");
  this._take = document.getElementById("take.tag");
  this._rate = document.getElementById("flow.rate");
  this._rateDirection = document.getElementById("rate.direction");
  this._top = this._tag;
  this._onSetIndex = flowOnSetIndex;
  this._onSetBuffer = flowOnSetIndex;
}

aaFlowView.prototype = new aaDetailView();
aaFlowView.prototype.read = function (node) {
  var flow = node.QueryInterface(nsCI.aaIFlow);
  this._tag.value = flow.tag;
  this._offBalance.checked = flow.isOffBalance;
  this._entity.value = flow.entity ? flow.entity.tag : "";
  this._give.value = flow.giveResource ? flow.giveResource.tag : "";
  this._take.value = flow.takeResource ? flow.takeResource.tag : "";
  if (gIndirectRate) {
    this._rate.value = (1 / flow.rate);
  } else {
    this._rate.value = flow.rate;
  }
};
aaFlowView.prototype.reset = function () {
  this._tag.value = "";
  this._entity.value = "";
  this._offBalance.checked = false;
  this._give.value = "";
  this._take.value = "";
  this._rate.value = "";
  this._rateDirection.selectedIndex = 0;
};
aaFlowView.prototype.copy = function () { 
  var flow = this.array.queryElementAt(this.index,
      nsCI.aaIFlow);
  this.buffer.QueryInterface(nsCI.aaIFlow).tag = flow.tag;
};
aaFlowView.prototype.update = function () { 
  if (gEditing) {
    document.getElementById("flow.canShowRules").removeAttribute("disabled");
    return;
  }

  document.getElementById("flow.canShowRules").setAttribute("disabled","true");
};
aaFlowView.prototype.rowBufferComparator = function(i) {
  return this.tree.view.getCellText(i, this.tree.columns[0]) == this.buffer.QueryInterface(nsCI.aaIFlow).tag;
};
aaFlowView.prototype.conditionBuilder = function() {
  if (!this._conditionBuilder)
    this._conditionBuilder = new aaFlowConditionBuilder();
  return this._conditionBuilder;
}

function aaFlowConditionBuilder()
{
}

aaFlowConditionBuilder.prototype = new aaPageConditionBuilder();
aaFlowConditionBuilder.prototype.getFilters = function() {
  //get element bundle
  var sbElem = document.getElementById("flow.bundle");
  if (sbElem) {
    return [
      { label: sbElem.getString("flow.filter.tag"), value: "tree.tag" }
    ];
  }
  return [];
}
