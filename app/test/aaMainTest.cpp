/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsIGenericFactory.h"

/* Unfrozen interfaces */
#include "nsIStringEnumerator.h"

/* Project includes */
#include "nsTestUtils.h"
#include "nsITestRunner.h"
#include "nsITest.h"

#define MODULE_TEST_CID \
{0xfefc6b55, 0xe3a9, 0x4c5f, {0x87, 0x01, 0x8f, 0xcc, 0xa9, 0x6c, 0xdc, 0xfa}}
#define MODULE_TEST_CONTRACT "@aasii.org/abstract/test"

class aaAbstractTest: public nsITest,
                      public nsIUTF8StringEnumerator
{
public:
  aaAbstractTest() :mSubtest(0) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  virtual ~aaAbstractTest() {}
  PRUint32 mSubtest;
};

NS_GENERIC_FACTORY_CONSTRUCTOR(aaAbstractTest)

static const nsModuleComponentInfo components[] =
{
  { "Abstract Accounting Test",
    MODULE_TEST_CID,
    MODULE_TEST_CONTRACT,
    aaAbstractTestConstructor
  }
};
NS_IMPL_NSGETMODULE(aaMainTest, components)

static const char* subtests[] =
{
  "@aasii.org/xpunit/unit;1"
  ,"@aasii.org/base/unit;1"
  ,"@aasii.org/accounting/unit;1"
  ,"@aasii.org/transport/unit;1"
  ,"@aasii.org/sql/unit;1"
  ,"@aasii.org/storage/unit-account;1"
  ,"@aasii.org/report/unit;1"
  ,"@aasii.org/view/unit;1"
  ,"@aasii.org/abstract/test/vc-base;1"
  ,"@aasii.org/abstract/test/vc-accounts;1"
  ,"@aasii.org/abstract/test/vc-rules;1"
  ,"@aasii.org/abstract/test/vc-docs;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMPL_ISUPPORTS2(aaAbstractTest,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaAbstractTest::Test(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  return NS_OK;
}

NS_IMETHODIMP
aaAbstractTest::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaAbstractTest::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign( subtests[mSubtest++] );
  return NS_OK;
}
