/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
* Copyright (C) 2009 Sergey Yanovich <ynvich@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of the
* License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*/
Components.utils.import("resource:///modules/aaUITestUtils.jsm");
Components.utils.import("resource:///modules/aaTestVC.jsm");

var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
  .getService(Components.interfaces.mozIJSSubScriptLoader);
loader.loadSubScript("resource:///modules/nsTestFrame.jsm");

/* Page specific class  */
/*
example for actions on page
[
    {
        name: "open",
        data: null //some data for special page 
    },
    {
        name: "close",
        data: null //some data for special page 
    },
]
*/
function JSTestPageModuleUI() {
    var base = JSTestModule();
    base.addOpenPage = null;
    base.addClosePage = null;
    base.parseAction = function(action) {
        switch(action.name) {
            case 'open':
                this.addOpenPage(action.data);
                return true;
                break;
            case 'close':
                this.addClosePage(action.data);
                return true;
                break;
        }
        return false;
    }
    return base;
}
/* End page specific class */

/*
example
{
    cid: Components.ID(),
    contractID: "",
    name: "",
    object: {
        command: ''
        checkF: null //function to check
        checkInTest: true or false
    }
}
*/
function aaJSDoCommandTest(input) {
    var base = new JSTest();
    
    base._name = input.name;
    base._contractID = input.contractID;
    base._CID = input.cid;
    
    base._command = input.object.command;
    base._checkInTest = false;
    if (input.object.checkInTest) {
        base._checkInTest = input.object.checkInTest;
    }

    base._test = function (runner) {
      if (!this._command)
        this.addJSFailure(runner, "JSDoCommandTest::_test - command is not defined");
      else {
        try {
          runner.doCommand(this._command);
          if (this._checkInTest && this._check) {
            this._check(runner);
          }
        } catch (e) {
          this.addJSFailure(runner, "[JSDoCommandTest::_test] - cann't rise command: " + this._command);
        }
      }
    }
    base._check = function (runner) {
      if (input.object.checkF)
        return input.object.checkF(runner, this);
      else {
        this.addJSFailure(runner, "[JSDoCommandTest] check function is not defined");
      }
    }
    return base;
}

/*
example
{
    cid: Components.ID(),
    contractID: "",
    name: "",
    object: {
        command: '' || click: ''
        checkF: null //function to check
    }
}
*/
function aaJSCommandOpenPageTest(input) {
    var base = null;
    if (input.object.command) {
        base = aaJSDoCommandTest(input);
        base.aaJSDoCommandTest_test = base._test;
        base._test = function(runner) {
            try {
                this.aaJSDoCommandTest_test(runner);
            } catch(e) {
                this.addJSFailure(runner, "[aaJSCommandOpenPageTest] cann't do command. Exception: " + e);
            }
            runner.watchWindow = getFrame(runner).contentWindow;
        }
    } else if(input.object.click) {
        base = new JSTest();
        base._CID = input.cid;
        base._contractID = input.contractID;
        base._name = input.name;
        
        base._clickID = input.object.click;
        
        base._test = function(runner) {
            if (getElement(runner, this._clickID).getAttribute("disabled") == 'true') {
              this.addJSFailure(runner, "[aaJSCommandOpenPageTest] cann't click to link, it is disabled");
            } else {
              sendOnClick(getElement(runner, this._clickID));
              runner.watchWindow = getFrame(runner).contentWindow;
            }
        }
        base._check = function (runner) {
          if (input.object.checkF)
            return input.object.checkF(runner, this);
          else {
            this.addJSFailure(runner, "[aaJSCommandOpenPageTest] check function is not defined");
          }
        }
    }
    return base;
}

/*
example
{
  cid: Components.ID(),
  contractID: "",
  name: "",
  object: {
    type: INFO || WARNING || CRITICAL || BLOCK
    textID: some user text
  }
}
*/
function aaSendNotificationTest(input) {
  var base = new JSTest();

  base._name = input.name;
  base._contractID = input.contractID;
  base._CID = input.cid;

  base._type = input.object.type;
  base._textID = input.object.textID;

  base._test = function (runner) {
    var service = Components.classes["@mozilla.org/observer-service;1"]
                      .getService(Components.interfaces.nsIObserverService);
    service.notifyObservers(this, 'ABSTRACT_' + this._type, this._textID);
    if (this._check) {
      this._check(runner);
    }
  }
  base._check = function (runner) {
    //find element
    var notify = runner.testWindow.document.getElementById("abstract.msgBox");
    if (!notify) {
      this.addJSFailure(runner, "Cann't find notification box element");
    } else {
      //check text
      var cur = notify.currentNotification;
      if (!cur) {
        this.addJSFailure(runner, "current notification is null");
      } else {
        //check notification type
        var tp = '';
        var sbID = 'msg.';
        switch (this._type) {
          case 'INFO':
            sbID += 'info';
            tp = 'info';
            break;
          case 'WARNING':
            sbID += 'warning';
            tp = 'warning';
            break;
          case 'CRITICAL':
            sbID += 'critical';
            tp = 'critical';
            break;
          case 'BLOCK':
            sbID += 'block';
            tp = 'critical';
            break;
        }
        if (cur.type != tp) {
          this.addJSFailure(runner, "Current message type(" + cur.type + ") is not equal to your type (" + tp + ")");
        }
        //get frame
        var sbundle = runner.testWindow.document.getElementById(sbID);
        if (!sbundle) {
          this.addJSFailure(runner, "String Bundle element is not found");
        } else {
          if (cur.label != sbundle.getString(this._textID)) {
            this.addJSFailure(runner, "Current message: " + cur.label + "; is not equal to entity from properties file: " + sbundle.getString(this._textID));
          }
        }
        //close message
        notify.removeCurrentNotification();
        //check count
      }
    }
  }
  return base;
}

/*
example for:
1. open
{
    cid: Components.ID(),
    object: { 
        command: "cmd_view_entity"
    }
}
2. close
{
    cid: Components.ID(),
    object: { 
        command: "cmd_page1_submit",
        checkF: null //function for check result page
    }
}
example for extended actions:
[
    {
        name: fill,
        data: null //page specific data
    },
]
*/
function aaJSTestPageModule(pgName) {
    var base = JSTestPageModuleUI();
        
    base._contractIDBase = "@aasii.org/abstract/test/";
    base._pageName = pgName;
    base._version = 1;
    
    base._contractID = base._contractIDBase + base._pageName + "/page;" + base._version;
    base._name = "aaPage_" + base._pageName;
    
    base.generateContractID = function(inner_str) {
        return this._contractIDBase + this._pageName + "/" + inner_str + ";" + this._version;
    }
    
    base.JSTestPage_parseAction = base.parseAction;
    base.parseAction = function (action) {
      switch (action.name) {
        case 'fill':
          this.addFillTest(action.data);
          return true;
          break;
        case 'msg':
          this.addMsgTest(action.data);
          return true;
          break;
      }
      return this.JSTestPage_parseAction(action);
    }
    base.addCommandPageOpenTest = function(data) {
        this._add(aaJSCommandOpenPageTest(data));
    }
    base.addCommandTest = function(data) {
        this._add(aaJSDoCommandTest(data));
    }
    base.addFillTest = null;
    base.checkPage = function(runner, errorHandler) {
      errorHandler.addJSFailure(runner, "aaJSTestPage.checkPage - not implemented");
    }
    base.addOpenPage = function(data) {
        data.contractID = this.generateContractID("open" + this._parse_counter + "/" + data.object.command);
        data.name = "aaPage_" + this._pageName + "_Open" + this._parse_counter + "_" + data.object.command;
        data.object.checkF = this.checkPage;
        this.addCommandPageOpenTest(data);
    }
    base.addClosePage = function(data) {
        data.contractID = this.generateContractID("close" + this._parse_counter + "/" + data.object.command);
        data.name = "aaPage_" + this._pageName + "_Close" + this._parse_counter + "_" + data.object.command;
        this.addCommandPageOpenTest(data);
    }
    base.addMsgTest = function (data) {
      data.contractID = this.generateContractID("message_" + data.object.type + this._parse_counter);
      data.name = "aaPage_" + this._pageName + "_message_" + data.object.type + this._parse_counter;
      this._add(aaSendNotificationTest(data));
    }
    
    //utils methods
    base.annoElemAttrIsEqual = function (runner, topElem, elem, attr, val) {
      return getAnnoElement(runner, topElem, elem).getAttribute(attr) == val;
    }
    base.elemAttrIsEqual = function(runner, elem, attr, val) {
      return getElement(runner, elem).getAttribute(attr) == val;
    }
    //end utils methods
    return base;
}

/*
{
  cid: Components.ID(),
  contractID: "",
  name:,
  getSortColumns: function() - return array of objects
    {
      id: "",
      compare: function(tree, prevLineIdx, curLineIdx, column)
      //if function is not defined, will be used default
      //return true if is right direction, false if not
    }
}
*/
function aaSortTest(input) {
  var base = new JSTest();

  base._CID = input.cid;
  base._contractID = input.contractID;
  base._name = input.name;

  base._getSortColumns = input.getSortColumns;

  base._defaultComparator = function (tree, prevLineIdx, curLineIdx, column) {
    var prevLine = tree.view.getCellText(prevLineIdx, column);
    var curLine = tree.view.getCellText(curLineIdx, column);
    return tree.getAttribute('sortDirection') == 'ascending' ? prevLine <= curLine :
          (tree.getAttribute('sortDirection') == 'descending' ? prevLine >= curLine : true);
  }

  base._test = function (runner) {
    if (this._getSortColumns) {
      var tree = getElement(runner, "page1.tree");
      if (tree) {
        var clmnIDs = this._getSortColumns();
        for (var i = 0; i < clmnIDs.length; i++) {
          var clmnID = clmnIDs[i].id;
          if (clmnID) {
            var column = tree.columns.getNamedColumn(clmnID);
            if (column) {
              sendOnClick(column.element);
              if (this._check) {
                this._check(runner, column, clmnIDs[i].compare ? clmnIDs[i].compare : this._defaultComparator);
              }
              sendOnClick(column.element);
              if (this._check) {
                this._check(runner, column, clmnIDs[i].compare ? clmnIDs[i].compare : this._defaultComparator);
              }
            } else {
              this.addJSFailure(runner, "[aaSortTest::_test] cann't find column with ID: " + clmnID);
            }
          } else {
            this.addJSFailure(runner, "[aaSortTest::_test] undefined column ID");
          }
        }
      } else {
        this.addJSFailure(runner, "[aaSortTest::_test] cann't find tree element");
      }
    } else {
      this.addJSFailure(runner, "[aaSortTest::_test] function getSortColumns is not defined");
    }
  }
  base._check = function (runner, clmn, comparator) {
    if (clmn) {
      var tree = getElement(runner, "page1.tree");
      if (tree && comparator) {
        for (var i = 1; i < tree.view.rowCount; i++) {
          if (!comparator(tree, i - 1, i, clmn)) {
            this.addJSFailure(runner, "[aaSortTest::_check] Column: " + clmn.id + "; sortDirection: " +
                                      tree.getAttribute('sortDirection') + "; line " + (i - 1) +
                                      " text (" + tree.view.getCellText(i - 1, clmn) + ") is " +
                                      (tree.getAttribute('sortDirection') == 'ascending' ? "great" : "less") +
                                      " then line " + i + " text (" + tree.view.getCellText(i, clmn) + ");");
          }
        }
      } else {
        this.addJSFailure(runner, "[aaSortTest::_check] cann't find tree element");
      }
    } else {
      this.addJSFailure(runner, "[aaSortTest::_check] column argument is empty");
    }
  }

  return base;
}

/*
{
  cid: Components.ID(),
  contractID: "",
  name:,
  object: {
    text:,
    type:,
    getFilterTypes: function() - return array of objects
    {
      clmnID: "",
      type: ""
    }
  }
}
*/
function aaFilterTest(input) {
  var base = new JSTest();

  base._CID = input.cid;
  base._contractID = input.contractID;
  base._name = input.name;

  base._getFilterTypes = input.object.getFilterTypes;
  base._text = input.object.text;
  base._type = input.object.type;

  base._defaultComparator = function (clmTxt, filterTxt) {
    return clmTxt.toLowerCase().match(filterTxt.toLowerCase()) != null;
  }

  base._test = function (runner) {
    if (this._getFilterTypes) {
      var filter = getAnnoElement(runner, "viewer", "page1.filter");
      if (filter) {
        //set filter type
        if (!filter.typesIsHidden && !filter.setType(this._type)) {
          this.addJSFailure(runner, "[aaFilterTest::_test] cann't set type to filter");
        }
        //set filter text
        filter.value = this._text;
        //do click
        runner.doCommand("cmd_do_filter");
        //do check
        if (this._check) {
          this._check(runner);
        }
        //clear filter
        filter.clear();
      } else {
        this.addJSFailure(runner, "[aaFilterTest::_test] cann't find filter element");
      }
    } else {
      this.addJSFailure(runner, "[aaFilterTest::_test] function _getFilterTypes is not defined");
    }
  }
  base._check = function (runner) {
    //find column id by type
    if (this._getFilterTypes) {
      var types = this._getFilterTypes();
      if (types.length > 0) {
        var clmnID = null;
        for (var i = 0; i < types.length; i++) {
          if (this._type == types[i].type) {
            clmnID = types[i].clmnID;
            break;
          }
        }
        if (clmnID) {
          var tree = getElement(runner, "page1.tree");
          if (tree) {
            var column = tree.columns.getNamedColumn(clmnID);
            if (column) {
              for (var i = 0; i < tree.view.rowCount; i++) {
                var rowTxt = tree.view.getCellText(i, column);
                if (!this._defaultComparator(rowTxt, this._text)) {
                  this.addJSFailure(runner, "[aaFilterTest::_check] at row " + i + " text " + rowTxt + " is not contain string " + this._text);
                }
              }
            } else {
              this.addJSFailure(runner, "[aaFilterTest::_check] cann't find column with ID: " + clmnID);
            }
          } else {
            this.addJSFailure(runner, "[aaFilterTest::_check] cann't find tree element");
          }
        } else {
          this.addJSFailure(runner, "[aaFilterTest::_check] cann't find clmn id by type: " + this._type);
        }
      } else {
        this.addJSFailure(runner, "[aaFilterTest::_check] returned types coun is 0");
      }
    } else {
      this.addJSFailure(runner, "[aaFilterTest::_check] cann't find function getFilterTypes");
    }
  }

  return base;
}

/*
example for cids
{
  name: 'page1_cids',
  data: {
    cid_new: Components.ID(),
    cid_change: Components.ID(),
    cid_cancel: Components.ID(),
    cid_reset: Components.ID(),
    cid_save: Components.ID()
  }
}
example for commands actions
{
  name: 'new_cmd',
  data: { //user specific command
    cid: Components.ID(),
    checkF: function(runner)
  }
}
{
  name: 'change_cmd',
  data: { //user specific command
    cid: Components.ID(),
    checkF: function(runner)
  }
}
{
  name: 'cancel_cmd',
  data: { //user specific command
    cid: Components.ID(),
    checkF: function(runner)
  }
}
{
  name: 'reset_cmd',
  data: { //user specific command
    cid: Components.ID(),
    checkF: function(runner)
  }
}
{
  name: 'save_cmd',
  data: { //user specific command
    cid: Components.ID(),
    checkF: function(runner)
  }
}
{
  name: 'sort',
  data: {
    cid: Components.ID()
  }
}
{
  name: 'filter',
  data: {
    cid: Components.ID(),
    object: {
      text: ""
      type: ""
    }
  }
}
*/
function aaJSTestPagePage1Module(pageName) {
    var base = aaJSTestPageModule(pageName);
    
    base.aaJSTestPageModule_parseAction = base.parseAction;
    
    base.addedCommandCounts = 0;
    
    base.newCID = null;
    base.changeCID = null;
    base.cancelCID = null;
    base.resetCID = null;
    base.saveCID = null;
    
    base._NewCommand = 'cmd_page1_new';
    base._ChangeCommand = 'cmd_page1_change';
    base._CancelCommand = 'cmd_page1_cancel';
    base._ResetCommand = 'cmd_page1_reset';
    base._SaveCommand = 'cmd_page1_save';
    
    base._SubmitCommand = 'cmd_page1_submit';
    base._DiscardCommand = 'cmd_page1_discard';

    base.parseAction = function (action) {
      switch (action.name) {
        case 'page1_cids':
          this.addCids(action.data);
          return true;
          break;
        case 'new_cmd':
          this.addCommandNewTest(action.data);
          return true;
          break;
        case 'change_cmd':
          this.addCommandChangeTest(action.data);
          return true;
          break;
        case 'cancel_cmd':
          this.addCommandCancelTest(action.data);
          return true;
          break;
        case 'reset_cmd':
          this.addCommandResetTest(action.data);
          return true;
          break;
        case 'save_cmd':
          this.addCommandSaveTest(action.data);
          return true;
          break;
        case 'sort':
          this.addSortTest(action.data);
          return true;
          break;
        case 'filter':
          this.addFilterTest(action.data);
          return true;
          break;
      }
      return this.aaJSTestPageModule_parseAction(action);
    }
    base.addCids = function(data) {
        if (data.cid_new) this.newCID = data.cid_new;
        if (data.cid_change) this.changeCID = data.cid_change;
        if (data.cid_cancel) this.cancelCID = data.cid_cancel;
        if (data.cid_reset) this.resetCID = data.cid_reset;
        if (data.cid_save) this.saveCID = data.cid_save;
    }
    base.addSortTest = function (data) {
      //sorting
      data.contractID = this.generateContractID("sort");
      data.name = "aaPage_" + this._pageName + "_Sort";
      data.getSortColumns = this.getSortColumns;
      this._add(aaSortTest(data));
    }
    base.addFilterTest = function (data) {
      //sorting
      data.contractID = this.generateContractID(data.object.type + "/filter");
      data.name = "aaPage_" + this._pageName + "_Filter_" + data.object.type;
      data.object.getFilterTypes = this.getFilterTypes;
      this._add(aaFilterTest(data));
    }
    base.getSortColumns = null;
    base.getFilterTypes = null;
    base.addPage1CommandTest = function(cid, command, checkF) {
        if (cid in this._map) {
            this._repeat(this._map[cid]);
        } else {
            var data = {};
            data.cid = cid;
            this.addedCommandCounts += 1;
            data.contractID = this.generateContractID("new_object/" + command + this.addedCommandCounts);
            data.name = "aaPage_" + this._pageName + "_" + command + this.addedCommandCounts;
            data.object = {};
            data.object.command = command;
            data.object.checkF = checkF;
            data.object.checkInTest = true;
            //add
            this.addCommandTest(data);
        }
    }
    
    base.addCommandNewTest = function(data) {
        if (!this.newCID) throw "cid for new button is not defined";
        this.addPage1CommandTest(data ? data.cid : this.newCID, this._NewCommand, data ? data.checkF : this.checkNew);
    }
    base.addCommandChangeTest = function(data) {
        if (!this.changeCID) throw "cid for change button is not defined";
        this.addPage1CommandTest(data ? data.cid : this.changeCID, this._ChangeCommand, data ? data.checkF : this.checkChange);
    }
    base.addCommandCancelTest = function(data) {
        if (!this.cancelCID) throw "cid for cancel button is not defined";
        this.addPage1CommandTest(data ? data.cid : this.cancelCID, this._CancelCommand, data ? data.checkF : this.checkCancel);
    }
    base.addCommandResetTest = function(data) {
        if (!this.resetCID) throw "cid for reset button is not defined";
        this.addPage1CommandTest(data ? data.cid : this.resetCID, this._ResetCommand, data ? data.checkF : this.checkReset);
    }
    base.addCommandSaveTest = function(data) {
        if (!this.saveCID) throw "cid for save button is not defined";
        this.addPage1CommandTest(data ? data.cid : this.saveCID, this._SaveCommand, data ? data.checkF : this.checkSave);
    }

    base.checkPage1CrossPageButtons = function (runner, errorHandler, submitted) {
      var elemEquals = base.annoElemAttrIsEqual(runner, "header", "header.discard", "collapsed", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPage1CrossPageButtons] header.discard button should not be visible");
      elemEquals = base.annoElemAttrIsEqual(runner, "header", "header.submit", "collapsed", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPage1CrossPageButtons] header.submit button should not be visible");
      elemEquals = base.elemAttrIsEqual(runner, "cmd_page1_discard", "disabled", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPage1CrossPageButtons] cmd_page1_discard command should be disabled");
      elemEquals = base.elemAttrIsEqual(runner, "cmd_page1_submit", "disabled", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPage1CrossPageButtons] entity_submit command should be disabled");
    }
    base.checkPageDefaultManipulationState = function (runner, errorHandler) {
      var tree = getElement(runner, "page1.tree");
      if (!tree)
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.tree not found");
      if (base.annoElemAttrIsEqual(runner, "viewer", "page1.browse", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.browse toolbar should be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.edit", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.edit toolbar should not be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.save button should be disabled");

      if (base.elemAttrIsEqual(runner, "cmd_page1_new", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_new command should not be disabled");
      if (base.elemAttrIsEqual(runner, "cmd_page1_cancel", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_cancel command should not be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_reset", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_new command should be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_save command should be disabled");


      if ((tree && tree.view.selection.currentIndex < 0) || !tree) {
        if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.change", "disabled", "true"))
          errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.change button should be disabled");
        if (!base.elemAttrIsEqual(runner, "cmd_page1_change", "disabled", "true"))
          errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_change command should be disabled");
      } else if (tree && tree.view.selection.currentIndex >= 0) {
        if (base.annoElemAttrIsEqual(runner, "viewer", "page1.change", "disabled", "true"))
          errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] page1.change button should not be disabled");
        if (base.elemAttrIsEqual(runner, "cmd_page1_change", "disabled", "true"))
          errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageDefaultManipulationState] cmd_page1_change command should not be disabled");
      }
    }
    base.checkPageNewManipulationState = function (runner, errorHandler) {
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.browse", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] page1.browse toolbar should not be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.change", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] page1.change button should be disabled");
      if (base.annoElemAttrIsEqual(runner, "viewer", "page1.edit", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] page1.edit toolbar should be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] page1.save button should be disabled");
      
      if (!base.elemAttrIsEqual(runner, "cmd_page1_new", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] cmd_page1_new command should be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_change", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] cmd_page1_change command should be disabled");
      if (base.elemAttrIsEqual(runner, "cmd_page1_cancel", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] cmd_page1_cancel command should not be disabled");
      if (base.elemAttrIsEqual(runner, "cmd_page1_reset", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] cmd_page1_new command should not be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageNewManipulationState] cmd_page1_save command should be disabled");
    }
    base.checkPage_Page1Buttons = function (runner, errorHandler) {
      base.checkPage1CrossPageButtons(runner, errorHandler, false);
      base.checkPageDefaultManipulationState(runner, errorHandler);
    }
    base.checkPage = function (runner, errorHandler) {
      base.checkPage_Page1Buttons(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "default");
    }
    base.checkNew = function (runner, errorHandler) {
      base.checkPageNewManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "new");
    }
    base.checkChange = function (runner, errorHandler) {
      base.checkPageNewManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "change");
    }
    base.checkCancel = function (runner, errorHandler) {
      base.checkPageDefaultManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "cancel");
    }
    base.checkReset = function (runner, errorHandler) {
      base.checkPageNewManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "reset");
    }
    base.checkSave = function (runner, errorHandler) {
      base.checkPageDefaultManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "save");
    }
    //state:
    //  'default' - state of page when opened
    //  'new' - new button pressed
    //  'change' - change button pressed
    //  'save' - save button pressed
    //  'cancel' - cancel button pressed
    //  'reset' - reset button pressed
    base.checkPageSpecificData = function (runner, errorHandler, state) {
      errorHandler.addJSFailure(runner, "[aaJSTestPagePage1Module.checkPageSpecificData] do not implemented");
    }
    return base;
}

/*
input data
{
    cid: Components.ID(),
    contractID: '',
    name: '',
    object: //specific object
    rowFunction: function(view, tree, i, object)
    checkF: if necessary
}
*/
function aaJSSelectObjectTest(input) {
    var base = new JSTest();
    base._CID = input.cid;
    base._contractID = input.contractID;
    base._name = input.name;
    
    base._checkFunction = input.rowFunction;
    base._object = input.object;
    
    base._test = function(runner) {
        var found = false;
        if (this._checkFunction) {
            var tree = getElement(runner, "page1.tree");
            var view = tree.view;
            for (var i = 0; i < tree.view.rowCount; ++ i) {
                if (this._checkFunction(view, tree, i, this._object)) {
                    tree.view.selection.select(i);
                    found = true;
                    break;
                }
            }
        } else {
            this.addJSFailure(runner, "[aaJSSelectObjectTest] rowFunction is not defined");
        }
        if (!found) {
            this.addJSFailure(runner, "[aaJSSelectObjectTest] cannot find appropriate row");
        }
        if (this._check) {
          this._check(runner);
        }
    }

    base._check = function (runner) {
      if (input.checkF) {
        input.checkF(runner, this);
      }
      if (input.extraCheck) {
        input.extraCheck(runner, this);
      }
    }
    return base;
}

/*
example for select
{
    name: 'select',
    data: {
        cid: Components.ID(),
        object: {
            //some child class specific object
        }
    }
}
example for extended open command
{
  cid: Components.ID(),
  selectable: true or false, if not exist selectable will be false
  object: specific object for aaJSDoCommandOpenPage
}

extended state for checkPageSpecificData
  state: 'selected'
*/
function aaJSTestSelectablePagePage1Module(pageName)
{
    var base = aaJSTestPagePage1Module(pageName);
    base._IsPageForSelect = false;
    
    base.checkAfterSelect = function (runner, errorHandler) {
      base.checkPageDefaultManipulationState(runner, errorHandler);
      base.checkPageSpecificData(runner, errorHandler, "selected");
    }
    base.selectCheckRow = function(view, tree, i, object) {
      return false;
    }
    //override base function
    base.checkPage_Page1Buttons = function (runner, errorHandler) {
      base.checkPage1CrossPageButtons(runner, errorHandler, base._IsPageForSelect);
      base.checkPageDefaultManipulationState(runner, errorHandler);
    }
    //override base function
    
    base.aaJSTestPagePage1Module_parseAction = base.parseAction;
    base.aaJSTestPagePage1Module_addOpenPage = base.addOpenPage;
    
    base.parseAction = function(action) {
        switch(action.name) {
            case 'select':
                this.addSelectTest(action.data);
                return true;
                break;
        }
        return this.aaJSTestPagePage1Module_parseAction(action);
    }
    base.addSelectTest = function(data) {
        data.contractID = this.generateContractID("select_object" + this._parse_counter);
        data.name = "aaPageSelectObject" + this._parse_counter;
        data.rowFunction = this.selectCheckRow;
        data.checkF = this.checkAfterSelect;
        this._add(aaJSSelectObjectTest(data));
    }
    base.addOpenPage = function(data) {
        if (data.selectable) {
          this._IsPageForSelect = data.selectable;
        }
        this.aaJSTestPagePage1Module_addOpenPage(data);
    }
    return base;
}

/*
example for fill
{
    name: "test name",
    contractID: "contract id",
    cid: Components.ID(""),
    object: {
        tag: 'asdsad'
    }
}
*/
function aaJSTestEntity(input) {
    var me = new JSTest();
    me._name = input.name;
    me._contractID = input.contractID;
    me._CID = input.cid;
    
    me._tagID = "entity.tag";
    me._tagValue = input.object.tag;
    
    me._test = function(runner) {
        //get element
        var box = getElement(runner, this._tagID);
        //set entity tag
        box.value = this._tagValue;
        sendOnInput(box);
        if (this._check) {
            this._check(runner);
        }
    }
    me._check = function(runner) {
        var box = getElement(runner, this._tagID);
        //check tag entity
        if (this._tagValue != box.value) {
            this.addJSFailure(runner, "[Entity page]: tag value is not equal tag textbox value");   
        }
    }
    return me;
}

/*
example for fill
{
    cid: Components.ID(""),
    object: {
        tag: 'asdsad'
    }
}
example for select
{
    cid: Components.ID(""),
    object: {
        tag: 'asdsad'
    }
}
*/
//add cid manually
function aaJSTestPageEntityModule(testNumber) {
    var base = aaJSTestSelectablePagePage1Module('entity' + testNumber);
    base.addFillTest = function(data) {
        //add call new
        this.addCommandNewTest();
        //add fill
        data.contractID = this.generateContractID("create_entity" + this._parse_counter);
        data.name = "aaEntityCreate_" + data.object.tag;
        this._add(aaJSTestEntity(data));
        //add call save
        this.addCommandSaveTest();
    }
    base.checkPageSpecificData = function(runner, errorHandler, state) {
        var tagBox = getElement(runner, 'entity.tag');
        if (!tagBox)
          errorHandler.addJSFailure(runner, "[aaJSTestPageEntityModule.checkPageSpecificData] element entity.tag is not found");
        switch(state) {
          case 'selected':
            //check for tagBox value state must be added
            var tree = getElement(runner, "page1.tree");
            var view = tree.view;
            if (tree.view.selection.currentIndex > -1) {
              if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[0]) != tagBox.value) {
                errorHandler.addJSFailure(runner, "[aaJSTestPageEntityModule.checkPageSpecificData] selected element is not sync with tagBox.");
              }
            } else {
              errorHandler.addJSFailure(runner, "[aaJSTestPageEntityModule.checkPageSpecificData] currentIndex less then 0.");
            }
          case 'cancel':
          case 'save':
          case 'default':
            if (tagBox.getAttribute("readonly") != 'true')
              errorHandler.addJSFailure(runner, "[aaJSTestPageEntityModule.checkPageSpecificData] element entity.tag is not readonly");
            break;
          case 'new':
            {
              if (runner.testWindow.document.commandDispatcher.focusedElement
                  != tagBox.inputField) {
                errorHandler.addJSFailure(runner, 
                  "[aaJSTestPageEntityModule.checkPageSpecificData] element" +
                  " entity.tag is not focused");
              }
            }
          case 'reset':
          case 'change':
            if (tagBox.getAttribute("readonly") == 'true')
              errorHandler.addJSFailure(runner, "[aaJSTestPageEntityModule.checkPageSpecificData] element entity.tag is readonly");
            break;
        }
    }
    base.selectCheckRow = function(view, tree, i, object) {
        return view.getCellText(i, tree.columns[0]) == object.tag;
    }
    base.getSortColumns = function () {
      return [
        { id:"tree.name" }
      ];
    }
    base.getFilterTypes = function () {
      return [
        { clmnID: "tree.name", type: "tree.name" }
      ];
    }
    return base;
}

/*
example for fill
{
    name: "test name",
    contractID: "contract id",
    cid: Components.ID(""),
    object: {
        isMoney: true or false
        tag: 'asdsad'
        code: //if isMoney set to true, that field will be used for set money code;
    }
}
*/
function aaJSTestNewResource(input) {
    var me = new JSTest();
    me._name = input.name;
    me._contractID = input.contractID;
    me._CID = input.cid;
    
    me.isMoneyID = 'resource.isMoney';
    me.tagID = 'resource.tag';
    me.codeID = 'resource.code';
    
    me.isMoney = false;
    me.code = null;
    me.tag = input.object.tag;
    if (input.object.isMoney) {
        me.isMoney = input.object.isMoney;
        me.code = input.object.code;
    }
    
    me._test = function(runner) {
        var _tag = getElement(runner, this.tagID);
        if (!_tag) this.addJSFailure(runner, "[aaJSTestNewResource] tag element not found");
        else {
            //if is money
            if (this.isMoney == true) {
                var _isMoney = getElement(runner, this.isMoneyID);
                var _code = getElement(runner, this.codeID);
                if (!_isMoney) this.addJSFailure(runner, "[aaJSTestNewResource] isMoney element not found");
                else {
                    _isMoney.checked = true;
                    _isMoney.doCommand();
                    if (!_code) this.addJSFailure(runner, "[aaJSTestNewResource] code element not found");
                    else if(_code.getAttribute('readonly') == 'true') {
                        this.addJSFailure(runner, "[aaJSTestNewResource] code element is readonly");
                    }
                    else {
                        _code.value = this.code;
                        sendOnInput(_code);
                    }
                }
            }
            _tag.value = this.tag;
            sendOnInput(_tag);
        }
        if (this._check) {
            this._check(runner);
        }
    }
    me._check = function(runner) {
        var _tag = getElement(runner, this.tagID);
        if (!_tag) this.addJSFailure(runner, "[aaJSTestNewResource] tag element not found");
        else {
            if (_tag.value != this.tag) {
                this.addJSFailure(runner, "[aaJSTestNewResource] tag element is not equal: " + this.tag);
            }
            //if is money
            if (this.isMoney == true) {
                var _isMoney = getElement(runner, this.isMoneyID);
                var _code = getElement(runner, this.codeID);
                if (!_isMoney) this.addJSFailure(runner, "[aaJSTestNewResource] isMoney element not found");
                else {
                    if (_isMoney.checked != true) {
                        this.addJSFailure(runner, "[aaJSTestNewResource] isMoney element is not checked");
                    }
                    if (!_code) this.addJSFailure(runner, "[aaJSTestNewResource] code element not found");
                    else if(_code.getAttribute('readonly') == 'true') {
                        this.addJSFailure(runner, "[aaJSTestNewResource] code element is readonly");
                    }
                    else if(_code.value != this.code){
                        this.addJSFailure(runner, "[aaJSTestNewResource] code element is not equal: " + this.code);
                    }
                }
            }
        }
    }
    return me;
}

/*
example for fill
{
    cid: Components.ID(""),
    object: {
        isMoney: true or false
        tag: 'asdsad'
        code: //if isMoney set to true, that field will be used for set money code;
    }
}
example for select
{
    cid: Components.ID(""),
    object: {
        isMoney: true or false
        tag: 'asdsad'
    }
}
*/
//add cid manually
function aaJSTestPageResourceModule(testNumber) {
    var base = aaJSTestSelectablePagePage1Module('resource' + testNumber);
    base.addFillTest = function(data) {
      //add call new
      this.addCommandNewTest();
      //add fill
      data.contractID = this.generateContractID("create_resource" + this._parse_counter);
      data.name = "aaResourceCreate" + this._parse_counter + "_" + data.object.tag;
      this._add(aaJSTestNewResource(data));
      //add call save
      this.addCommandSaveTest();
    }
    base.checkPageSpecificData = function (runner, errorHandler, state) {
      var _tag = getElement(runner, "resource.tag");
      var _isMoney = getElement(runner, "resource.isMoney");
      var _code = getElement(runner, "resource.code");
      var _tree = getElement(runner, "page1.tree");
      if (!_tag) errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element not found");
      if (!_isMoney) errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element not found");
      if (!_code) errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] code element not found");
      if (!_tree) errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tree element not found");
      switch (state) {
        case 'selected':
          if (_tree.view.selection.currentIndex > -1) {
            var _fIsMoney =
              _tree.view.getCellText(_tree.view.selection.currentIndex,
                  _tree.columns[0]) == chooseTypeSymbol(1);
            if (_fIsMoney) {
              if (_isMoney.checked != true) {
                errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element must be checked");
              }
            } else {
              if (_isMoney.checked == true) {
                errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element must not be checked");
                dump(_tree.view.getCellText(_tree.view.selection.currentIndex, _tree.columns[1]));
              }
            }
            if (_tree.view.getCellText(_tree.view.selection.currentIndex, _tree.columns[1]) != _tag.value) {
              errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element not synced");
            }
          } else {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] element is not selected");
          }
          if (_tag.getAttribute('readonly') != 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element is not readonly");
          }
          if (_code.getAttribute('readonly') != 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] code element is not readonly");
          }
          break;
        case 'default':
          if (_isMoney.checked == true) {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element is checked");
          }
        case 'cancel':
        case 'save':
          if (_tag.getAttribute('readonly') != 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element is not readonly");
          }
          if (_code.getAttribute('readonly') != 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] code element is not readonly");
          }
          break;
        case 'new':
          if (_isMoney.checked == true) {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element is checked");
          }
          if (_code.getAttribute('readonly') == 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] code element is readonly");
          }
        case 'reset':
          if (_tag.getAttribute('readonly') == 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element is readonly");
          }
          break;
        case 'change':
          if (_tag.getAttribute('readonly') == 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] tag element is readonly");
          }
          if (_code.getAttribute('readonly') != 'true') {
            errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] code element is not readonly");
          }
          if (_tree.view.selection.currentIndex > -1) {
            var _fIsMoney = _tree.view.getCellText(_tree.view.selection.currentIndex, _tree.columns[0]) == chooseTypeSymbol(1);
            if (_fIsMoney) {
              if (_isMoney.checked != true) {
                errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element must be checked");
              }
            } else {
              if (_isMoney.checked == true) {
                errorHandler.addJSFailure(runner, "[aaJSTestPageResourceModule.checkPageSpecificData] isMoney element must not be checked");
              }
            }
          }
          break;
      }
    }
    base.selectCheckRow = function(view, tree, i, object) {
        var type = chooseTypeSymbol(object.isMoney);

        return view.getCellText(i, tree.columns[0]) == type &&
                view.getCellText(i, tree.columns[1]) == object.tag;
    }
    base.getSortColumns = function () {
      return [
        { id: "tree.type",
          compare: function (tree, pIdx, cIdx, column) {
            var ds = tree.view.QueryInterface(nsCI.aaIDataTreeView).dataSet;
            var prev = ds.queryElementAt(pIdx, nsCI.aaIResource);
            var cur = ds.queryElementAt(cIdx, nsCI.aaIResource);
            return tree.getAttribute('sortDirection') == 'ascending' ? prev.type <= cur.type :
                   (tree.getAttribute('sortDirection') == 'descending' ? prev.type >= cur.type : true);
          }
        },
        { id: "tree.name" }
      ];
    }
    base.getFilterTypes = function () {
      return [
        { clmnID: "tree.name", type: "tree.name" }
      ];
    }
    return base;
}

/*
example for fill
{
    name: "test name",
    contractID: "contract id",
    cid: Components.ID(""),
    object: {
        isLight: false,
        tag: 'asdsad',
        direction: 0(-->) or 1(<--), 
        rate: 1.0, 
        isOffBalance: true or false
    }
}
second version
{
    cid: Components.ID(""),
    object: {
        isLight: true,
        tag: 'asdsad',
        direction: 0(-->) or 1(<--), 
        rate: 1.0, 
        isOffBalance: true or false,
        entity: {
            tag: ''
        },
        giveRes: {
            isMoney: true or false,
            tag: ''
        },
        takeRes: {
            isMoney: true or false,
            tag: ''
        }
    }
}
*/
function aaJSTestNewFlow(input) {
    var me = new JSTest();
    me._name = input.name;
    me._contractID = input.contractID;
    me._CID = input.cid;
    
    me._tagID = 'flow.tag';
    me._rateID = 'flow.rate';
    me._directionID = 'rate.direction';
    me._isOffBalanceID = 'flow.isoffbalance';
    
    me._tagValue = input.object.tag;
    me._rateValue = input.object.rate;
    me._directionValue = input.object.direction;
    me._isOffBalanceValue = input.object.isOffBalance;
    me._isLight = false;
    if (input.object.isLight) {
        me._isLight = input.object.isLight;
    }
    if (me._isLight == true) {
        me._entity = input.object.entity;
        me._give = input.object.giveRes;
        me._take = input.object.takeRes;
    }
    
    me._test = function(runner) {
        var tagInput = getElement(runner, this._tagID);
        var rateInput = getElement(runner, this._rateID);
        var flowIsOffBalance = getElement(runner, this._isOffBalanceID);
        //fill input values
        tagInput.value = this._tagValue;
        sendOnInput(tagInput);
        
        getElement(runner, this._directionID).selectedIndex = this._directionValue;
        
        rateInput.value = this._rateValue;
        sendOnInput(rateInput);
        
        //set is off balance
        if (true == this._isOffBalanceValue) {
            sendOnClick(flowIsOffBalance);
        }
        
        //if is light
        if (this._isLight) {
            this.addEntity(runner);
            this.addGiveResource(runner);
            this.addTakeResource(runner);
        }
        if (this._check) {
            this._check(runner);
        }
    }
    me.addEntity = function(runner) {
        var loader = Components.classes["@aasii.org/storage/load-entity;1"].createInstance(nsCI.aaISqlRequest);
        var entities = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation).sessionHistory.load(loader);
        var enumerator = entities.enumerate();
        while(enumerator.hasMoreElements()) {
            var entity = enumerator.getNext().QueryInterface(nsCI.aaIEntity);
            if (entity.tag == this._entity.tag) {
                var page = getFrame(runner).contentWindow.wrappedJSObject.view;
                page.buffer.QueryInterface(nsCI.aaIFlow).entity =
                        entity.QueryInterface(nsCI.aaIEntity);
                break;
            }
        }
    }
    me.addGiveResource = function(runner) {
        var loader = Components.classes["@aasii.org/storage/load-resource;1"].createInstance(nsCI.aaISqlRequest);
        var resources = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation).sessionHistory.load(loader);
        var enumerator = resources.enumerate();
        while(enumerator.hasMoreElements()) {
            var resource = enumerator.getNext().QueryInterface(nsCI.aaIResource);
            if (resource.tag == this._give.tag) {
                if ((this._give.isMoney && resource.type == nsCI.aaIResource.TYPE_MONEY) || 
                    (!this._give.isMoney && resource.type == nsCI.aaIResource.TYPE_ASSET)) {
                    var page = getFrame(runner).contentWindow.wrappedJSObject.view;
                    page.buffer.QueryInterface(nsCI.aaIFlow).giveResource =
                            resource.QueryInterface(nsCI.aaIResource);
                    break;
                }
            }
        }
    }
    me.addTakeResource = function(runner) {
        var loader = Components.classes["@aasii.org/storage/load-resource;1"].createInstance(nsCI.aaISqlRequest);
        var resources = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation).sessionHistory.load(loader);
        var enumerator = resources.enumerate();
        while(enumerator.hasMoreElements()) {
            var resource = enumerator.getNext().QueryInterface(nsCI.aaIResource);
            if (resource.tag == this._take.tag) {
                if ((this._take.isMoney && resource.type == nsCI.aaIResource.TYPE_MONEY) || 
                    (!this._take.isMoney && resource.type == nsCI.aaIResource.TYPE_ASSET)) {
                    var page = getFrame(runner).contentWindow.wrappedJSObject.view;
                    page.buffer.QueryInterface(nsCI.aaIFlow).takeResource =
                            resource.QueryInterface(nsCI.aaIResource);
                    break;
                }
            }
        }
    }
    me._check = function(runner) {
        var tagInput = getElement(runner, this._tagID);
        if (!tagInput) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] cann't find element " + this._tagID);
        }
        if (tagInput.value != this._tagValue) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] element " + this._tagID + " is not equal to " + this._tagValue);
        }
        var rateInput = getElement(runner, this._rateID);
        if (!rateInput) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] cann't find element " + this._rateID);
        }
        if (rateInput.value != this._rateValue) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] element " + this._rateID + " is not equal to " + this._rateValue);
        }
        var flowIsOffBalance = getElement(runner, this._isOffBalanceID);
        if (!flowIsOffBalance) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] cann't find element " + this._isOffBalanceID);
        }
        if (flowIsOffBalance.checked != this._isOffBalanceValue) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] element " + this._isOffBalanceID + " is not equal to " + this._isOffBalanceValue);
        }
        //getElement(runner, this._directionID).selectedIndex = this._directionValue;
        if (!getElement(runner, this._directionID)) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] cann't find element " + this._directionID);
        }
        if (getElement(runner, this._directionID).selectedIndex != this._directionValue) {
            this.addJSFailure(runner, "[aaJSTestNewFlow._check] element " + this._directionID + " is not equal to " + this._directionValue);
        }
    }
    return me;
}

/*
example for fill

first version
{
    cid: Components.ID(""),
    object: {
        isLight: false,
        tag: 'asdsad',
        direction: 0(-->) or 1(<--), 
        rate: 1.0, 
        isOffBalance: true or false,
        entity: {
            page: {
                cid: Components.ID(),
                open_cid: Components.ID(),
                close_cid: Components.ID()
            }
            flow: [
                {//specific flow for entity page
                }
            ]
        },
        giveRes: {
            page: {
                cid: Components.ID(),
                open_cid: Components.ID(),
                close_cid: Components.ID()
            }
            flow: [
                {//specific flow for resource page
                }
            ]
        },
        takeRes: {
            page: {
                cid: Components.ID(),
                open_cid: Components.ID(),
                close_cid: Components.ID()
            }
            flow: [
                {//specific flow for resource page
                }
            ]
        }
    }
}
second version
{
    cid: Components.ID(""),
    object: {
        isLight: true,
        tag: 'asdsad',
        direction: 0(-->) or 1(<--), 
        rate: 1.0, 
        isOffBalance: true or false,
        entity: {
            tag: ''
        },
        giveRes: {
            isMoney: true or false,
            tag: ''
        },
        takeRes: {
            isMoney: true or false,
            tag: ''
        }
    }
}
example for select
{
    cid: Components.ID(""),
    object: {
        tag: 'asdsad'
        entity: 'haha' //entity tag
    }
}
*/
//add cid manually
function aaJSTestPageFlowModule(testNumber) {
    var base = aaJSTestSelectablePagePage1Module('flow' + testNumber);
    base.addFillTest = function(data) {
        //add call new
        this.addCommandNewTest();
        //add fill
        data.contractID = this.generateContractID("create_flow" + this._parse_counter + "_" + data.object.tag);
        data.name = "aaFlowCreate" + this._parse_counter + "_" + data.object.tag;
        this._add(aaJSTestNewFlow(data));
        //add entity select
        if (!data.object.isLight) {
            this.addSelectEntity(data.object.entity);
            this.addSelectGiveResource(data.object.giveRes);
            this.addSelectTakeResource(data.object.takeRes, data);
        }
        if (data.object.rules) {
            this.addFillRules(data.object.rules);
        }
        //add call save
        this.addCommandSaveTest();
    }
    base.addSelectEntity = function(entity) {        
        var tmp = new Array();
        tmp[0] = {
            name: 'open',
            data: {
                cid: entity.page.open_cid,
                selectable: true,
                object: {
                    command: 'cmd_flow_entity'
                }
            }
        };
        var lng = tmp.length;
        for(var i = 0; i < entity.flow.length; i++) {
            tmp[lng++] = entity.flow[i];
        }
        var _entityModule = aaJSTestPageEntityModule(this._tests.length);
        _entityModule.createTestsFlow(tmp);
        for(var i = 0; i < _entityModule._tests.length; i++) {
            this._add(_entityModule._tests[i]);
        }
        ////////////////////////////////////////////////////
    }
    base.addSelectGiveResource = function(giveRes) {
        var tmp = new Array();
        tmp[0] = {
            name: 'open',
            data: {
                cid: giveRes.page.open_cid,
                selectable: true,
                object: {
                    command: 'cmd_flow_give'
                }
            }
        };
        var lng = tmp.length;
        for(var i = 0; i < giveRes.flow.length; i++) {
            tmp[lng++] = giveRes.flow[i];
        }
        var _resource = aaJSTestPageResourceModule(this._tests.length);
        _resource.createTestsFlow(tmp);
        for(var i = 0; i < _resource._tests.length; i++) {
            this._add(_resource._tests[i]);
        }
    }
    base.addSelectTakeResource = function(takeRes, data) {
        var tmp = new Array();
        tmp[0] = {
            name: 'open',
            data: {
                cid: takeRes.page.open_cid,
                selectable: true,
                object: {
                    command: 'cmd_flow_take'
                }
            }
        };
        var lng = tmp.length;
        for(var i = 0; i < takeRes.flow.length; i++) {
            tmp[lng++] = takeRes.flow[i];
        }
        var _resource = aaJSTestPageResourceModule(this._tests.length);
        _resource.createTestsFlow(tmp);
        for(var i = 0; i < _resource._tests.length; i++) {
            this._add(_resource._tests[i]);
        }
    }
    base.addFillRules = function(rules) {
        var tmp = new Array();
        tmp[0] = {
            name: 'open',
            data: {
                cid: rules.page.open_cid,
                selectable: true,
                object: {
                    click: 'flow.rules'
                }
            }
        };
        var lng = tmp.length;
        for(var i = 0; i < rules.flow.length; i++) {
            tmp[lng++] = rules.flow[i];
        }
        var _rule = aaJSTestPageRuleModule(this._tests.length);
        _rule.createTestsFlow(tmp);
        for(var i = 0; i < _rule._tests.length; i++) {
            this._add(_rule._tests[i]);
        }
    }

    base.checkPageSpecificData = function (runner, errorHandler, state) {
      var _tag = getElement(runner, "flow.tag");
      var _rate = getElement(runner, "flow.rate");
      var _IsOffBalance = getElement(runner, "flow.isoffbalance");
      var _direction = getElement(runner, "rate.direction");
      var _entity = getElement(runner, "entity.tag");
      var _give = getElement(runner, "give.tag");
      var _take = getElement(runner, "take.tag");
      var _rule = getElement(runner, "flow.rules");
      if (!_tag) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] tag element not found");
      if (!_rate) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] rate element not found");
      if (!_IsOffBalance) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] is off balance element not found");
      if (!_direction) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] direction element not found");
      if (!_entity) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] entity tag element not found");
      if (!_give) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] give element not found");
      if (!_take) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] take element not found");
      if (!_rule) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] rule link element not found");
      switch (state) {
        case 'selected':
          var _tree = getElement(runner, "page1.tree");
          if (!_tree) errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] tree element not found");
          if (_tree.view.selection.currentIndex >= 0 &&
                (_tree.view.getCellText(_tree.view.selection.currentIndex, _tree.columns[0]) != _tag.value ||
                  _tree.view.getCellText(_tree.view.selection.currentIndex, _tree.columns[1]) != _entity.value)) {
              errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] selected element is not equal to flow text boxes values");
          }
        case 'cancel':
        case 'save':
        case 'default':
          if (_tag.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] tag element is not readonly");
          if (_rate.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _rate element is not readonly");
          if (_IsOffBalance.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _IsOffBalance element is not readonly");
          if (_direction.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _direction element is not readonly");
          if (_entity.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _entity element is not readonly");
          if (_give.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _give element is not readonly");
          if (_take.getAttribute("readonly") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _take element is not readonly");
          if (_rule.getAttribute("disabled") != 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] rule element is not disabled");
          break;
        case 'new':
        case 'reset':
        case 'change':
          if (_tag.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] tag element is readonly");
          if (_rate.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _rate element is readonly");
          if (_IsOffBalance.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _IsOffBalance element is readonly");
          if (_direction.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _direction element is readonly");
          if (_entity.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _entity element is readonly");
          if (_give.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _give element is readonly");
          if (_take.getAttribute("readonly") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] _take element is readonly");
          if (_rule.getAttribute("disabled") == 'true') errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageSpecificData] rule element is disabled");
          break;
      }
    }
    base.checkPageDefaultManipulationState = function (runner, errorHandler) {
      var tree = getElement(runner, "page1.tree");
      if (!tree)
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] page1.tree not found");
      if (base.annoElemAttrIsEqual(runner, "viewer", "page1.browse", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] page1.browse toolbar should be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.edit", "collapsed", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] page1.edit toolbar should not be visible");
      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] page1.save button should be disabled");

      if (base.elemAttrIsEqual(runner, "cmd_page1_new", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] cmd_page1_new command should not be disabled");
      if (base.elemAttrIsEqual(runner, "cmd_page1_cancel", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] cmd_page1_cancel command should not be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_reset", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] cmd_page1_new command should be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_save", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] cmd_page1_save command should be disabled");

      if (!base.annoElemAttrIsEqual(runner, "viewer", "page1.change", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] page1.change button should be disabled");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_change", "disabled", "true"))
        errorHandler.addJSFailure(runner, "[aaJSTestPageFlowModule.checkPageDefaultManipulationState] cmd_page1_change command should be disabled");
    }
    base.selectCheckRow = function (view, tree, i, object) {
      return view.getCellText(i, tree.columns[0]) == object.tag &&
                view.getCellText(i, tree.columns[1]) == object.entity;
    }
    base.getSortColumns = function () {
      return [
        { id: "tree.tag" },
        { id: "tree.entity" }
      ];
    }
    base.getFilterTypes = function () {
      return [
        { clmnID: "tree.tag", type: "tree.tag" },
        { clmnID: "tree.entity", type: "tree.entity" }
      ];
    }
    return base;
}

/*
example for fill
{
    name: "test name",
    contractID: "contract id",
    cid: Components.ID(""),
    object: {
        tag: 'shipment1.rule1',
        rate: 27.0,
        change_fact_side: 0
    }
}
*/
function aaJSTestNewRule(input) {
    var me = new JSTest();
    me._name = input.name;
    me._contractID = input.contractID;
    me._CID = input.cid;
    
    me._tagID = 'rule.tag';
    me._rateID = 'rule.rate';
    me._change_fact_sideID = 'rule.change.fact.side';
    
    me._tagValue = input.object.tag;
    me._rateValue = input.object.rate;
    me._change_fact_sideValue = input.object.change_fact_side;
    
    me._test = function(runner) {
        var tag = getElement(runner, this._tagID);
        var _rate = getElement(runner, this._rateID);
        var _changeFact = getElement(runner, this._change_fact_sideID);
        
        tag.value = this._tagValue;
        sendOnInput(tag);
        
        _rate.value = this._rateValue;
        sendOnInput(_rate);
        
        _changeFact.selectedIndex = this._change_fact_sideValue;
        if (this._check) {
            this._check(runner);
        }
    }
    me._check = function(runner) {
        var tag = getElement(runner, this._tagID);
        if (tag.value != this._tagValue) {
            this.addJSFailure(runner, "[aaJSTestNewRule._check] tag value is not equal to set; Value: " + tag.value);
        }
        var _rate = getElement(runner, this._rateID);
        if (_rate.value != this._rateValue) {
            this.addJSFailure(runner, "[aaJSTestNewRule._check] rate value is not equal to set; Value: " + _rate.value);
        }
        var _changeFact = getElement(runner, this._change_fact_sideID);
        if (_changeFact.selectedIndex != this._change_fact_sideValue) {
            this.addJSFailure(runner, "[aaJSTestNewRule._check] change_fact_side value is not equal to set; Value: " + _changeFact.selectedIndex);
        }
    }
    return me;
}

/*
example for fill
{
    cid: Components.ID(""),
    object: {
        tag: 'shipment1.rule1',
        rate: 27.0,
        change_fact_side: 0,
        giveTo: {
            page: {
                cid: Components.ID(),
                open_cid: Components.ID(),
                close_cid: Components.ID()
            }
            flow: [
                {//specific flow for flow page
                }
            ]
        },
        takeFrom: {
            page: {
                cid: Components.ID(),
                open_cid: Components.ID(),
                close_cid: Components.ID()
            }
            flow: [
                {//specific flow for flow page
                }
            ]
        }
    }
}
example for select
{
    cid: Components.ID(""),
    object: {
        tag: 'shipment1.rule1'
    }
}
*/
//add cid manually
function aaJSTestPageRuleModule(testNumber) {
    var base = aaJSTestSelectablePagePage1Module('rule' + testNumber);
    base.addFillTest = function (data) {
      //add call new
      this.addCommandNewTest();
      //add fill
      data.contractID = this.generateContractID("create_rule" + this._parse_counter + "_" + data.object.tag);
      data.name = "aaRuleCreate" + this._parse_counter + "_" + data.object.tag;
      this._add(aaJSTestNewRule(data));
      this.addSelectFlow(data.object.giveTo, 'cmd_b_giveto');
      this.addSelectFlow(data.object.takeFrom, 'cmd_b_takefrom');
      //add call save
      this.addCommandSaveTest();
    }
    base.addSelectFlow = function (flow, bcommand) {
      var tmp = new Array();
      tmp[0] = {
        name: 'open',
        data: {
          cid: flow.page.open_cid,
          selectable: true,
          object: {
            command: bcommand
          }
        }
      };
      var lng = tmp.length;
      for (var i = 0; i < flow.flow.length; i++) {
        tmp[lng++] = flow.flow[i];
      }
      var _flowModule = aaJSTestPageFlowModule(this._tests.length);
      _flowModule.createTestsFlow(tmp);
      for (var i = 0; i < _flowModule._tests.length; i++) {
        this._add(_flowModule._tests[i]);
      }
      ////////////////////////////////////////////////////
    }
    base.checkPageSpecificData = function (runner, errorHandler, state) {
      var tag = getElement(runner, "rule.tag");
      var _rate = getElement(runner, "rule.rate");
      var _changeFact = getElement(runner, "rule.change.fact.side");
      var _giveToTB = getElement(runner, "rule.tb.giveto");
      var _giveToB = getElement(runner, "rule.b.giveto");
      var _takeFromTB = getElement(runner, "rule.tb.takefrom");
      var _takeFromB = getElement(runner, "rule.b.takefrom");
      if (!tag) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] tag element not found");
      if (!_rate) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] rate element not found");
      if (!_changeFact) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] change fact side element not found");
      if (!_giveToTB) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] give to textbox element not found");
      if (!_giveToB) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] give to button element not found");
      if (!_takeFromTB) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] take from textbox element not found");
      if (!_takeFromB) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] take from button element not found");
      switch (state) {
        case 'selected':
          var tree = getElement(runner, "page1.tree");
          if (!tree) errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] tree element not found");
          else {
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[0]) != tag.value) {
              errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] selected row column 0 is not equal to tag");
            }
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[1]) != _giveToTB.value) {
              errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] selected row column 1 is not equal to give to element");
            }
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[2]) != _takeFromTB.value) {
              errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] selected row column 2 is not equal to take from element");
            }
          }
        case 'cancel':
        case 'save':
        case 'default':
          if (tag.getAttribute("readonly") != 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] tag element is not readonly");
          if (_rate.getAttribute("readonly") != 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _rate element is not readonly");
          if (_changeFact.getAttribute("readonly") != 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _changeFact element is not readonly");
          if (_giveToB.getAttribute("readonly") != 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _giveTo button element is not readonly");
          if (_takeFromB.getAttribute("readonly") != 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _takeFrom button element is not readonly");
          break;
        case 'new':
        case 'reset':
        case 'change':
          if (tag.getAttribute("readonly") == 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] tag element is readonly");
          if (_rate.getAttribute("readonly") == 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _rate element is readonly");
          if (_changeFact.getAttribute("readonly") == 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _changeFact element is readonly");
          if (_giveToB.getAttribute("readonly") == 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _giveTo button element is readonly");
          if (_takeFromB.getAttribute("readonly") == 'true')
            errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _takeFrom button element is readonly");
          break;
      }
      if (_giveToTB.getAttribute("readonly") != 'true')
        errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _giveTo textbox element is not readonly");
      if (_takeFromTB.getAttribute("readonly") != 'true')
        errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPageSpecificData] _takeFrom textbox element is not readonly");
    }
    base.checkPage1CrossPageButtons = function (runner, errorHandler, submitted) {
      var elemEquals = base.annoElemAttrIsEqual(runner, "header", "header.discard", "collapsed", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPage1CrossPageButtons] header.discard button should " + (submitted ? "not " : "") + "be visible");
      elemEquals = base.annoElemAttrIsEqual(runner, "header", "header.submit", "collapsed", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
         errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPage1CrossPageButtons] header.submit button should " + (submitted ? "not " : "") + "be visible");
      if (!base.elemAttrIsEqual(runner, "cmd_page1_discard", "disabled", "true"))
         errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPage1CrossPageButtons] cmd_page1_discard command should be disabled");
      elemEquals = base.elemAttrIsEqual(runner, "cmd_page1_submit", "disabled", "true");
      if ((submitted && elemEquals) || (!submitted && !elemEquals))
        errorHandler.addJSFailure(runner, "[aaJSTestPageRuleModule.checkPage1CrossPageButtons] cmd_page1_submit command should " + (submitted ? "not " : "") + "be disabled");
    }
    base.selectCheckRow = function (view, tree, i, object) {
      return view.getCellText(i, tree.columns[0]) == object.tag;
    }
    base.getSortColumns = function () {
      return [
        { id: "tree.tag" },
        { id: "tree.giveto" },
        { id: "tree.takefrom" },
        { id: "tree.rate" }
      ];
    }
    base.getFilterTypes = function () {
      return [
        { clmnID: "tree.tag", type: "tree.tag" },
        { clmnID: "tree.giveto", type: "tree.giveto" },
        { clmnID: "tree.takefrom", type: "tree.takefrom" },
        { clmnID: "tree.rate", type: "tree.rate" }
      ];
    }
    return base;
}

/*
select and fill not implemented
*/
//add cid manually
function aaJSTestPageRateModule(testNumber) {
  var base = aaJSTestSelectablePagePage1Module('rate' + testNumber);
  base.addFillTest = function (data) {
  }
  base.checkPage1CrossPageButtons = function (runner, errorHandler, submitted) {
  }
  base.checkPageSpecificData = function (runner, errorHandler, state) {
    var tag = getElement(runner, "quote.resource");
    var _rate = getElement(runner, "quote.rate");
    var _date = getElement(runner, "quote.date");
    if (!tag) errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] tag element not found");
    if (!_rate) errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] rate element not found");
    if (!_date) errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] date element not found");
    switch (state) {
      case 'selected':
        return;
      case 'cancel':
      case 'save':
      case 'default':
        if (tag.getAttribute("readonly") != 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] tag element is not readonly");
        if (_rate.getAttribute("readonly") != 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] _rate element is not readonly");
        if (_date.getAttribute("readonly") != 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] _date element is not readonly");
        break;
      case 'new':
      case 'reset':
      case 'change':
        if (tag.getAttribute("readonly") == 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] tag element is readonly");
        if (_rate.getAttribute("readonly") == 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] _rate element is readonly");
        if (_date.getAttribute("readonly") == 'true')
          errorHandler.addJSFailure(runner, "[aaJSTestPageRateModule.checkPageSpecificData] _date element is readonly");
        break;
    }
  }
  base.selectCheckRow = function (view, tree, i, object) {
    return false;
  }
  base.getSortColumns = function () {
    return [
        { id: "tree.resource" },
        { id: "tree.date" },
        { id: "tree.rate" }
      ];
  }
  base.getFilterTypes = function () {
    return [
        { clmnID: "tree.resource", type: "tree.resource" },
        { clmnID: "tree.date", type: "tree.date" },
        { clmnID: "tree.rate", type: "tree.rate" }
      ];
  }
  return base;
}

