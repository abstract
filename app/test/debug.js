/* debugging prefs */
pref("browser.dom.window.dump.enabled", true);
pref("javascript.options.showInConsole", true);
pref("javascript.options.strict", true);
pref("dom.report_all_js_exceptions", true);
pref("nglayout.debug.disable_xul_cache", true);
pref("nglayout.debug.disable_xul_fastload", true);

/* cxxunit prefs */
pref("xpunit.firstTest","@aasii.org/abstract/test");
pref("xpunit.fileMap","resource:///fileMap.rdf");

