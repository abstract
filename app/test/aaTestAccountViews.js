/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI                   = Components.interfaces
const moduleName             = "aaTestAccountViews";
const moduleCID              = "{afd38c04-819b-4823-ad6a-22c6d05b3774}";
const moduleContractID       = "@aasii.org/abstract/test/vc-accounts;1";

Components.utils.import("resource:///modules/aaTestVC.jsm");
Components.utils.import("resource:///modules/aaUITestUtils.jsm");

var clickToChoose = "";
var profitAndLoss = "";
var debitTerm = "";
var creditTerm = "";

/*
 * Module entry point
 * The NSGetModule function is the magic entry point that XPCOM uses to find
 * what XPCOM components this module provides
 */
function NSGetModule(comMgr, fileSpec)
{
  var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Components.interfaces.mozIJSSubScriptLoader);
  loader.loadSubScript("resource:///modules/nsTestFrame.jsm");
  loader.loadSubScript("resource:///modules/aaTestNames.jsm");

  var aaVCTestModule = JSTestModule();
  aaVCTestModule.init = ModuleInit;
  aaVCTestModule.init();
  return aaVCTestModule;
}

function ModuleInit()
{
  this._name = moduleName;
  this._CID = Components.ID(moduleCID);
  this._contractID = moduleContractID;

  /*  0 */
  this._add(chart_page_CID, chart_page_contractID,
      chart_page_name, chart_page_test, chart_page_check);
  /*  1 */
  this._add(chart_resource_CID, chart_resource_contractID, chart_resource_name,
      chart_resource_test, resource_select_check);
  /*  2 */
  this._add(chart_resource_submit_CID, chart_resource_submit_contractID,
      chart_resource_submit_name, resource_submit_test,
      chart_resource_submit_check);
  this._tests[2].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "money" &&
           view.getCellText(i, tree.columns[1]) == "USD";
  };
  /*  3 */
  this._repeat(0);
  /*  4 */
  this._add(transfer_page_CID, transfer_page_contractID,
      transfer_page_name, transfer_page_test, transfer_page_check);
  /*  5 */
  this._add(transfer_to_CID, transfer_to_contractID, transfer_to_name,
      transfer_to_test, flow_select_check);
  /*  6 */
  this._add(transfer_to_discard_CID, transfer_to_discard_contractID,
      transfer_to_discard_name, flow_discard_test,
      transfer_to_discard_check);
  this._tests[6].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "purchase 1";
  };
  /*  7 */
  this._repeat(5);
  /*  8 */
  this._add(transfer_to_create_CID, transfer_to_create_contractID,
      transfer_to_create_name, transfer_to_create_test, resource_select_check);
  /*  9 */
  this._add(subflow_give_submit_CID, subflow_give_submit_contractID,
      subflow_give_submit_name, resource_submit_test,
      flow_check);
  this._tests[9].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "asset" &&
           view.getCellText(i, tree.columns[1]) == "steel, tn";
  };
  /* 10 */
  this._add(subflow_take_select_CID, subflow_take_select_contractID,
      subflow_take_select_name, flow_take_test, resource_select_check);
  /* 11 */
  this._add(subflow_take_submit_CID, subflow_take_submit_contractID,
      subflow_take_submit_name, resource_submit_test,
      flow_check);
  this._tests[11].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "asset" &&
           view.getCellText(i, tree.columns[1]) == "steel, tn";
  };
  /* 12 */
  this._add(subflow_entity_select_CID, subflow_entity_select_contractID,
      subflow_entity_select_name, flow_entity_test, entity_select_check);
  /* 13 */
  this._add(subflow_entity_submit_CID, subflow_entity_submit_contractID,
      subflow_entity_submit_name, resource_submit_test,
      flow_check);
  this._tests[13].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "Smith, John";
  };
  /* 14 */
  this._add(transfer_to_submit_CID, transfer_to_submit_contractID,
      transfer_to_submit_name,  transfer_to_submit_test,
      transfer_check);
  /* 15 */
  this._add(transfer_from_CID, transfer_from_contractID, transfer_from_name,
      transfer_from_test, flow_select_check);
  /* 16 */
  this._add(transfer_from_discard_CID, transfer_from_discard_contractID,
      transfer_from_discard_name, flow_discard_test,
      transfer_from_discard_check);
  /* 17 */
  this._repeat(15);
  /* 18 */
  this._add(transfer_from_submit_CID, transfer_from_submit_contractID,
      transfer_from_submit_name, flow_submit_test, transfer_from_submit_check);
  this._tests[18].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "purchase 1";
  };
  /* 19 */
  this._add(transfer_save_CID, transfer_save_contractID,
      transfer_save_name, transfer_save_test, flow_select_check);
  /* 20 */
  this._add(submit_after_reset_CID, submit_after_reset_contractID,
      submit_after_reset_name,  flow_submit_test,
      submit_after_reset_check);
  this._tests[20].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "metall storage";
  };
  /* 21 */
  this._add(account_page_CID, account_page_contractID, account_page_name,
      account_page_test, account_page_check);
  /* 22 */
  this._add(transcript_page_CID, transcript_page_contractID,
      transcript_page_name, transcript_page_test, transcript_page_check);
  /* 23 */
  this._add(transcript_flow_CID, transcript_flow_contractID, transcript_flow_name,
      transcript_flow_test, flow_select_check);
  /* 24 */
  this._add(tf_discard_CID, tf_discard_contractID, tf_discard_name,
      flow_discard_test, transcript_flow_discard_check);
  this._tests[24].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "purchase 1";
  };
  /* 25 */
  this._repeat(23);
  /* 26 */
  this._add(tf_submit_CID, tf_submit_contractID, tf_submit_name,
      flow_submit_test, transcript_flow_submit_check);
  this._tests[26].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "metall storage";
  };
  /* 27 */
  this._add(transcript_load_CID, transcript_load_contractID,
      transcript_load_name, transcript_load_test, null);
  /* 28 */
  this._add(transcript_after_flow_CID, transcript_after_flow_contractID,
      transcript_after_flow_name, transcript_after_flow_test, null);
  /* 29 */
  this._add(transcript_income_flow_CID, transcript_income_flow_contractID,
      transcript_income_flow_name, transcript_income_flow_test, null);
  /* 30 */
  this._add(rate_page_CID, rate_page_contractID,
      rate_page_name, rate_page_test, rate_page_check);
  /* 31 */
  this._add(rate_resource_CID, rate_resource_contractID, rate_resource_name,
      rate_resource_test, resource_select_check);
  /* 32 */
  this._add(rate_resource_discard_CID, rate_resource_discard_contractID,
      rate_resource_discard_name, resource_discard_test,
      rate_resource_discard_check);
  /* 33 */
  this._repeat(31);
  /* 34 */
  this._add(rate_resource_submit_CID, rate_resource_submit_contractID,
      rate_resource_submit_name, resource_submit_test,
      rate_resource_submit_check);
  this._tests[34].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "money" &&
           view.getCellText(i, tree.columns[1]) == "EUR";
  };
  /* 35 */
  this._add(rate_save_CID, rate_save_contractID,
      rate_save_name, rate_save_test, null);
  /* 36 */
  this._add(rate_digits_CID, rate_digits_contractID,
      rate_digits_name, rate_digits_test, null);
  /* 37 */
  this._add(ledger_page_CID, ledger_page_contractID,
      ledger_page_name, ledger_page_test, ledger_page_check);
  /* 38 */
  this._add(tasp_CID, tasp_contractID, tasp_name,
      transfer_page_test, tasp_select_from);
  /* 39 */
  this._repeat(15);
  /* 40 */
  this._add(tasff_CID, tasff_contractID,
      tasff_name, flow_submit_test, tasff_check);
  this._tests[this._tests.length - 1].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "d";
  };
  /* 41 */
  this._repeat(5);
  this._add(tastf_CID, tastf_contractID,
      tastf_name, flow_submit_test, tastf_check);
  this._tests[this._tests.length - 1].item = function(view, tree, i) {
    return view.getCellText(i, tree.columns[0]) == "purchase 1";
  };
}

function initLocaleSymbols()
{
  var heap = Components.classes["@mozilla.org/intl/stringbundle;1"]
    .createInstance(nsCI.nsIStringBundleService);
  var bundle = heap.createBundle(
      "chrome://abstract/locale/abstract.properties");
  clickToChoose = bundle.GetStringFromName("link.choose");
  profitAndLoss = bundle.GetStringFromName("terms.profits");
  debitTerm = bundle.GetStringFromName("terms.debit");
  creditTerm = bundle.GetStringFromName("terms.credit");
}

/* Module-specific Defines and Utilities */
const contentId = "content";

/* Account Page Test */
const account_page_contractID = "@aasii.org/abstract/test/account-balance;1";
const account_page_name = "aaAccountPageTest";
const account_page_CID = Components.ID("{3391fcd8-e122-4e31-9162-f114fe2d843d}");

function account_page_test(runner)
{
  runner.doCommand("cmd_report_balances");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function account_page_check(runner)
{
  var tree = getElement(runner, "balances.tree");
  var view = tree.view;
  var date = getElement(runner, "filter.date");
  date.value = "2008-02-06";
  sendOnChange(date);
  if (!tree)
    runner.addJSFailure("[balances page] failed");

  if (view.getCellText(0,tree.columns[3]) != "")
    runner.addJSFailure("transcript.tree.line1.col4 failed");
  if (view.getCellText(1,tree.columns[3]) != "100.00")
    runner.addJSFailure("transcript.tree.line2.col4 failed");
  if (view.getCellText(2,tree.columns[3]) != "10000.00")
    runner.addJSFailure("transcript.tree.line3.col4 failed");
  if (view.getCellText(3,tree.columns[3]) != "20000.00")
    runner.addJSFailure("transcript.tree.line4.col4 failed");
  if (view.getCellText(4,tree.columns[3]) != "")
    runner.addJSFailure("transcript.tree.line5.col4 failed");

  if (view.getCellText(0,tree.columns[4]) != "30000.00")
    runner.addJSFailure("transcript.tree.line1.col4 failed");
  if (view.getCellText(1,tree.columns[4]) != "")
    runner.addJSFailure("transcript.tree.line2.col4 failed");
  if (view.getCellText(2,tree.columns[4]) != "")
    runner.addJSFailure("transcript.tree.line3.col4 failed");
  if (view.getCellText(3,tree.columns[4]) != "")
    runner.addJSFailure("transcript.tree.line4.col4 failed");
  if (view.getCellText(4,tree.columns[4]) != "0.00")
    runner.addJSFailure("transcript.tree.line5.col4 failed");

  getElement(runner, "units.mode").selectedIndex = 1;

  if (tree.view.getCellText(0,tree.columns[0]) != aaTestNames.FlowTag[0])
    runner.addJSFailure("balance.tree.line1.col1 failed");
  if (tree.view.getCellText(0,tree.columns[1]) != aaTestNames.EntityTag[0])
    runner.addJSFailure("balance.tree.line1.col2 failed");
  if (tree.view.getCellText(0,tree.columns[2]) != aaTestNames.ResourceTag[0])
    runner.addJSFailure("balance.tree.line1.col3 failed");
  if (tree.view.getCellText(0,tree.columns[3]) != "")
    runner.addJSFailure("balance.tree.line3.col4 failed");
  if (tree.view.getCellText(0,tree.columns[4]) != "30000.00")
    runner.addJSFailure("balance.tree.line1.col5 failed");
  if (tree.view.getCellText(3,tree.columns[3]) != "20000.00")
    runner.addJSFailure("balance.tree.line3.col4 failed");

  if (getElement(runner, "footer.sumDebit").value != "40000.00")
    runner.addJSFailure("balance.totalAssets is wrong: "
        + getElement(runner, "footer.sumDebit").value);
  if (getElement(runner, "footer.sumCredit").value != "40000.00")
    runner.addJSFailure("balance.totalLiabilities is wrong: "
        + getElement(runner, "footer.sumCredit").value);
}

/* Transcript Page Test */
const transcript_page_contractID = "@aasii.org/abstract/test/account-transcript;1";
const transcript_page_name = "aaTranscriptPageTest";
const transcript_page_CID = Components.ID("{d7a1753e-7d74-4f8a-940d-317d37c369ee}");

function transcript_page_test(runner)
{
  runner.doCommand("cmd_report_transcript");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function transcript_check(runner)
{
  if (! getElement(runner, "transcript.tree"))
    runner.addJSFailure("[transcript page] tree failed");
  if (! getElement(runner, "filter.flow"))
    runner.addJSFailure("[transcript page] flow link failed");
}

const transcript1DateFrom = "2008-02-04";
const transcript1DateTo = "2008-02-06";

function transcript_page_check(runner)
{
  transcript_check(runner);
  var tree = getElement(runner, "transcript.tree");
  // XXX 403202 'Unflexing' the tree speeds up reflow significantly
  if (tree && tree.hasAttribute("flex"))
    tree.removeAttribute("flex");
 
  getElement(runner, "filter.dateFrom").value = transcript1DateFrom;
  sendOnChange(getElement(runner, "filter.dateFrom"));
  getElement(runner, "filter.dateTo").value = transcript1DateTo;
  sendOnChange(getElement(runner, "filter.dateTo"));
  if (getElement(runner, "filter.cancel").hasAttribute("collapsed"))
    runner.addJSFailure("[transcript page] 'change' command");
}

/* ask for flow from transcript page */
const transcript_flow_contractID = "@aasii.org/test/transcript-flow;1";
const transcript_flow_name = "aaVCTranscriptFlowTest";
const transcript_flow_CID = Components.ID("{b4fe65f6-c55e-491c-a039-27c0beff12cc}");

function transcript_flow_test(runner)
{
  sendOnClick(getElement(runner, "filter.flow"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

const tf_discard_contractID = "@aasii.org/test/transcript-flow-discard;1";
const tf_discard_name = "aaVCTranscriptFlowDiscardTest";
const tf_discard_CID = Components.ID("{a122d947-3fb2-4796-87fe-22c67706bd9a}");

function transcript_flow_discard_check(runner)
{
  transcript_check(runner);
  if (getElement(runner, "filter.flow").value != clickToChoose)
    runner.addJSFailure("[transcript->flow] discard failed");
}

const tf_submit_contractID = "@aasii.org/test/transcript-flow-submit;1";
const tf_submit_name = "aaVCTranscriptFlowSubmitTest";
const tf_submit_CID = Components.ID("{3197a8bb-1fd1-453e-98d8-6bc7996552a5}");

function transcript_flow_submit_check(runner)
{
  transcript_check(runner);
  if (getElement(runner, "filter.flow").value != aaTestNames.FlowTag[1])
    runner.addJSFailure("[transcript->flow] submit failed: "
        + getElement(runner, "filter.flow").value + "!=" + aaTestNames.FlowTag[1]);
}

/* submit/discard from flow page */
function flow_select_check(runner)
{
  flow_check(runner);
  var tree = getElement(runner, "page1.tree");
  // XXX 403202 'Unflexing' the tree speeds up reflow significantly
  if (tree && tree.hasAttribute("flex"))
    tree.removeAttribute("flex");
 
  if (getAnnoElement(runner, "header", "header.discard")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("flow.discard button should be visible");
  if (getAnnoElement(runner, "header", "header.submit")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("flow.submit button should be visible");
  if (getElement(runner, "cmd_page1_discard")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("flow_discard command should be enabled");
  if (getElement(runner, "cmd_page1_submit")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("flow_submit command should be enabled");
}

function flow_discard_test(runner)
{
  if (this.item) {
    var tree = getElement(runner, "page1.tree");
    var view = tree.view;
    for (var i = 0; i < tree.view.rowCount; ++ i) {
        if (this.item(view, tree, i)) {
            tree.view.selection.select(i);
        }
    }
  }
  runner.doCommand("cmd_page1_discard");
  runner.watchWindow = getFrame(runner).contentWindow;
} 

function flow_submit_test(runner)
{
  if (this.item) {
    var tree = getElement(runner, "page1.tree");
    var view = tree.view;
    for (var i = 0; i < tree.view.rowCount; ++ i) {
        if (this.item(view, tree, i)) {
            tree.view.selection.select(i);
        }
    }
  }
  runner.doCommand("cmd_page1_submit");
  runner.watchWindow = getFrame(runner).contentWindow;
} 

function flow_last_line_check(runner)
{
  var tree = getElement(runner, "page1.tree");
  var view = tree.view;
  if (view.getCellText(view.rowCount - 1, tree.columns[0])
      != profitAndLoss)
    runner.addJSFailure("[flow page] wrong tag on the last line");
}

/* Transcript Load Test */
const transcript_load_contractID = "@aasii.org/abstract/test/transcript-load;1";
const transcript_load_name = "aaTranscriptLoadTest";
const transcript_load_CID = Components.ID("{335e1a8e-f8a0-4a9d-850b-0404cd2db9e5}");

function transcript_load_test(runner)
{
  var loadLnk = getElement(runner, "transcript.load");
  var startDate = getElement(runner, "filter.dateFrom");
  var endDate = getElement(runner, "filter.dateTo");
  var tree = getElement(runner, "transcript.tree");
  var view = tree.view.QueryInterface(nsCI.nsITreeView);
  
  sendOnClick(loadLnk);

  if (getElement(runner, "filter.group").getAttribute("collapsed") != "true")
    runner.addJSFailure("[transcript load] filter not hidden");

  if (getElement(runner, "header.flow").value != aaTestNames.FlowTag[1])
    runner.addJSFailure("[transcript load] wrong flow: "
        + getElement(runner, "header.flow").value);
  if (getElement(runner, "header.dateFrom").value != transcript1DateFrom)
    runner.addJSFailure("[transcript load] [1] wrong 'from' date");
  if (getElement(runner, "header.sumDebitFrom").value != "0.00")
    runner.addJSFailure("[transcript load] [1] wrong 'from' debit");
  if (getElement(runner, "header.sumCreditFrom").value != "0.00")
    runner.addJSFailure("[transcript load] [1] wrong 'from' credit");
  if (getElement(runner, "header.dateTo").value != transcript1DateTo)
    runner.addJSFailure("[transcript load] [1] wrong 'to' date");
  if (getElement(runner, "header.sumDebits").value != "300.00")
    runner.addJSFailure("[transcript load] [1] wrong total debit");
  if (getElement(runner, "header.sumCredits").value != "200.00")
    runner.addJSFailure("[transcript load] [1] wrong total credit");
  if (getElement(runner, "header.sumDebitTo").value != "100.00")
    runner.addJSFailure("[transcript load] [1] wrong 'to' debit");
  if (getElement(runner, "header.sumCreditTo").value != "0.00")
    runner.addJSFailure("[transcript load] [1] wrong 'to' credit");

  if (view.getCellText(0,tree.columns[0]) != "2008-02-04")
    runner.addJSFailure("transcript.tree.line1.col1 failed");
  if (view.getCellText(1,tree.columns[0]) != "2008-02-05")
    runner.addJSFailure("transcript.tree.line2.col1 failed");

  if (view.getCellText(0,tree.columns[1]) != aaTestNames.FlowTag[0])
    runner.addJSFailure("transcript.tree.line1.col2 failed");
  if (view.getCellText(1,tree.columns[1]) != "c")
    runner.addJSFailure("transcript.tree.line2.col2 failed");

  if (view.getCellText(0,tree.columns[2]) != "300.00")
    runner.addJSFailure("transcript.tree.line1.col3 failed");
  if (view.getCellText(1,tree.columns[2]) != "")
    runner.addJSFailure("transcript.tree.line2.col3 failed");

  if (view.getCellText(0,tree.columns[3]) != "")
    runner.addJSFailure("transcript.tree.line1.col4 failed");
  if (view.getCellText(1,tree.columns[3]) != "200.00")
    runner.addJSFailure("transcript.tree.line2.col4 failed");

  getElement(runner, "units.mode").selectedIndex = 1;

  if (view.getCellText(0,tree.columns[2]) != "30000.00")
    runner.addJSFailure("transcript.tree.line1.col3 failed");
  if (view.getCellText(1,tree.columns[2]) != "")
    runner.addJSFailure("transcript.tree.line2.col3 failed");

  if (view.getCellText(0,tree.columns[3]) != "")
    runner.addJSFailure("transcript.tree.line1.col4 failed");
  if (view.getCellText(1,tree.columns[3]) != "20000.00")
    runner.addJSFailure("transcript.tree.line2.col4 failed");

  /* Test for bug #133 */
  if (getElement(runner, "header.sumDebitTo").value != "10000.00")
    runner.addJSFailure("[transcript load] [1] wrong 'to' debit");
  if (getElement(runner, "header.sumCreditTo").value != "0.00")
    runner.addJSFailure("[transcript load] [1] wrong 'to' credit");

  if (getElement(runner, "header.sumDebits").value != "30000.00")
    runner.addJSFailure("[transcript load] [1] wrong total debit");
  if (getElement(runner, "header.sumCredits").value != "20000.00")
    runner.addJSFailure("[transcript load] [1] wrong total credit");
}

/* Tests for Flow-in-Transcript-Selection bug #122 */
const transcript_after_flow_contractID = "@aasii.org/abstract/test/trasncript-after-flow;1";
const transcript_after_flow_name = "aaTranscriptAfterFlowP1";
const transcript_after_flow_CID = Components.ID("{b9c0df9d-6036-43c7-b0b4-d4e2f3092601}");

function transcript_after_flow_test(runner)
{
  var loader = Components.classes["@aasii.org/storage/load-flow;1"].
    createInstance(nsCI.aaISqlRequest);
  var flows = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
    .sessionHistory.load(loader);
  getFrame(runner).contentWindow.wrappedJSObject.gView.param.flow = flows
    .queryElementAt(3, nsCI.aaIFlow);

  var tree = getElement(runner, "transcript.tree");
  var view = tree.view.QueryInterface(nsCI.nsITreeView);
  
  if (view.getCellText(0,tree.columns[1]) != aaTestNames.FlowTag[0])
    runner.addJSFailure("transcript.tree.line3.col2 failed: " +
        view.getCellText(0,tree.columns[1]) + "!=" + aaTestNames.FlowTag[0]);
}

/* Tests for income-flow display */
const transcript_income_flow_contractID = "@aasii.org/abstract/test/trasncript-income-flow;1";
const transcript_income_flow_name = "aaTranscriptIncomeFlowP1";
const transcript_income_flow_CID = Components.ID("{07f18646-a3be-4ab5-9edd-7b2b899f00b8}");

function transcript_income_flow_test(runner)
{
  var loadLnk = getElement(runner, "transcript.load");
  var startDate = getElement(runner, "filter.dateFrom");
  var endDate = getElement(runner, "filter.dateTo");
  var tree = getElement(runner, "transcript.tree");
  var view = tree.view.QueryInterface(nsCI.nsITreeView);
  
  getFrame(runner).contentWindow.wrappedJSObject.gView.param.flow =
    Components.classes["@aasii.org/base/income-flow;1"]
    .createInstance(nsCI.aaIFlow);

  startDate.value = "2008-02-04";
  endDate.value = "2008-02-06";

  sendOnClick(loadLnk);

  if (getElement(runner, "header.dateFrom").value != "2008-02-04")
    runner.addJSFailure("[transcript load] [2] wrong 'from' date");
  if (getElement(runner, "header.sumDebitFrom").value != "0.00")
    runner.addJSFailure("[transcript load] [2] wrong 'from' debit");
  if (getElement(runner, "header.sumCreditFrom").value != "0.00")
    runner.addJSFailure("[transcript load] [2] wrong 'from' credit");
  if (getElement(runner, "header.dateTo").value != "2008-02-06")
    runner.addJSFailure("[transcript load] [2] wrong 'to' date");
  if (getElement(runner, "header.sumDebitTo").value != "0.00")
    runner.addJSFailure("[transcript load] [2] wrong 'to' debit");
  if (getElement(runner, "header.sumCreditTo").value != "10000.00")
    runner.addJSFailure("[transcript load] [2] wrong 'to' credit");
  if (view.rowCount != 1)
    runner.addJSFailure("[transcript load] [2] line count");

  if (view.getCellText(0,tree.columns[0]) != "2008-02-05")
    runner.addJSFailure("transcript.tree.line1.col1 failed");

  if (view.getCellText(0,tree.columns[1]) != "c")
    runner.addJSFailure("transcript.tree.line1.col2 failed");

  if (view.getCellText(0,tree.columns[2]) != "")
    runner.addJSFailure("transcript.tree.line1.col3 failed");

  if (view.getCellText(0,tree.columns[3]) != "10000.00")
    runner.addJSFailure("transcript.tree.line1.col4 failed");
}

/* Chart Page Test */
const chart_page_contractID = "@aasii.org/abstract/test/account-chart;1";
const chart_page_name = "aaChartPageTest";
const chart_page_CID = Components.ID("{9166dd77-6add-4104-a75e-cbdc41c6d3df}");

function chart_page_test(runner)
{
  initLocaleSymbols();
  runner.doCommand("cmd_manage_chart");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function chart_sync_check(runner)
{
  var result = true;
  var wnd = getFrame(runner).contentWindow.wrappedJSObject;
  var chart = wnd.gView.chart;
  var loaded = wnd.gLoaded;
  if (! chart) {
    runner.addJSFailure(" no chart object");
    return false;
  }

  if (chart.resource && chart.resource.tag
      != getElement(runner, "chart.resource").value)
    runner.addJSFailure(" chart.resource is wrong");

  if ( loaded ^ getElement(runner, "chart.set").hasAttribute("collapsed")) {
    runner.addJSFailure(" chart.set command out of sync");
    result = false;
  }

  return result;
}

function chart_check(runner)
{ 
  var result = true;
  if (getFrame(runner).contentWindow.location.href !=
      "chrome://abstract/content/chart.xul") {
    runner.addJSFailure(" [chart] wrong xul document");
    return false;
  }

  if ( ! chart_sync_check(runner) )
    return false;

  return result;
}

function chart_page_check(runner)
{
  if (! chart_check(runner))
    runner.addJSFailure("[chart page]");
}

/* ask for resource from chart page */
const chart_resource_contractID = "@aasii.org/test/chart-resource;1";
const chart_resource_name = "aaVCChartResourceTest";
const chart_resource_CID = Components.ID("{0a83a675-ebbd-4796-9147-5a423b83d20b}");

function chart_resource_test(runner)
{
  sendOnClick(getElement(runner, "chart.resource"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

const chart_resource_submit_contractID = "@aasii.org/test/chart-resource-submit;1";
const chart_resource_submit_name = "aaVCChartResourceSubmitTest";
const chart_resource_submit_CID = Components.ID("{5b81d3f1-55a1-4f37-b608-992d8f094741}");

function chart_resource_submit_check(runner)
{
  if (! chart_check(runner))
    runner.addJSFailure("[chart->resource] wrong page init");

  if (getElement(runner, "chart.resource").value != aaTestNames.ResourceTag[0])
    runner.addJSFailure("[chart->resource] submit failed: "
        + getElement(runner, "chart.resource").value);

  sendOnClick(getElement(runner, "chart.set"));
  if ( ! chart_sync_check(runner))
    runner.addJSFailure("[chart->date] out of sync");
}

/* Transfer Page Test */
const transfer_page_contractID = "@aasii.org/abstract/test/account-transfer;1";
const transfer_page_name = "aaTransferPageTest";
const transfer_page_CID = Components.ID("{950daefb-95ba-4960-a11a-b88af6cf6cad}");

function transfer_page_test(runner)
{
  runner.doCommand("cmd_process_transfer");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function transfer_sync_check(runner)
{
  var wnd = getFrame(runner).contentWindow.wrappedJSObject;
  var result = true;
  if (! wnd) {
    runner.addJSFailure("[transfer sync] getting window");
    return false;
  }

  if (! wnd.gFact) {
    runner.addJSFailure("[transfer sync] getting fact object");
    return false;
  }

  if (getElement(runner, "transfer.fromFlow").value != (wnd.gFact.takeFrom ? 
        wnd.gFact.takeFrom.tag : clickToChoose)) {
    runner.addJSFailure("  'fromFlow.tag' is wrong:" + getElement(runner,
          "transfer.fromFlow").value);
    result = false;
  }

  if (getElement(runner, "transfer.toFlow").value != (wnd.gFact.giveTo ? 
        wnd.gFact.giveTo.tag : clickToChoose)) {
    runner.addJSFailure("  'toFlow.tag' is wrong: " + getElement(runner,
          "transfer.toFlow").value);
    result = false;
  }

  if (getElement(runner, "transfer.amount").value != wnd.gFact.amount) {
    runner.addJSFailure("  'amount' is wrong: " + getElement(runner,
          "transfer.amount").value);
    result = false;
  }

  var time = new Date(wnd.gEvent.time / 1000);
  if (getElement(runner, "transfer.date").value != time
      .toLocaleFormat('%Y-%m-%d')) {
    runner.addJSFailure("  'date' is wrong: " + getElement(runner,
          "transfer.date").value + " - " + time.toLocaleFormat('%Y-%m-%d'));
    result = false;
  }

  if ((wnd.gTransaction && (getElement(runner, "transfer.value").value
        != wnd.gTransaction.value.toFixed(2))) || !wnd.gTransaction
      && (getElement(runner, "transfer.value").value != "")) {
      runner.addJSFailure("  'value' is wrong: " + getElement(runner,
            "transfer.value").value);
      result = false;
  }

  return result;
}

function transfer_check(runner)
{
  var result = true;
  if (getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
      .currentURI.spec != "chrome://abstract/content/transfer.xul") {
    runner.addJSFailure(" [transfer] wrong xul document");
    return false;
  }
  if (! transfer_sync_check(runner)) {
    runner.addJSFailure(" [transfer] page out of sync");
    result = false;
  }

  return result;
}

function transfer_page_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer page] wrong initialization");

  if (getElement(runner, "transfer.amount").value != "0")
    runner.addJSFailure("  'amount' is wrong: " + getElement(runner,
          "transfer.amount").value);

  var junk = Components.classes["@aasii.org/base/flow;1"].createInstance(nsCI.aaIFlow);
  junk.tag = "junk";
  var wnd = getFrame(runner).contentWindow.wrappedJSObject;
  wnd.gFact.takeFrom = junk;
  wnd.gFact.giveTo = junk;
}

/* ask for 'from' flow from transfer page */
const transfer_from_contractID = "@aasii.org/test/transfer-from;1";
const transfer_from_name = "aaVCTransferFromTest";
const transfer_from_CID = Components.ID("{7ad5abf7-53b9-468d-b50c-4d9614a9dc14}");

function transfer_from_test(runner)
{
  sendOnClick(getElement(runner, "transfer.fromFlow"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

const transfer_from_discard_contractID = "@aasii.org/test/transfer-from-discard;1";
const transfer_from_discard_name = "aaVCTransferFromDiscardTest";
const transfer_from_discard_CID = Components.ID("{ca063e94-1b9c-4e53-bf25-9fa8af605def}");

function transfer_from_discard_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer->fromFlow] wrong init");

  if (getElement(runner, "transfer.fromFlow").value != "junk")
    runner.addJSFailure("[transfer->fromFlow] discard failed");
}

const transfer_from_submit_contractID = "@aasii.org/test/transfer-from-submit;1";
const transfer_from_submit_name = "aaVCTransferFromSubmitTest";
const transfer_from_submit_CID = Components.ID("{aaf7ec49-e9aa-45a8-b630-56b6659fa6ad}");

function transfer_from_submit_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer->fromFlow] wrong page init");

  if (getElement(runner, "transfer.fromFlow").value != aaTestNames.FlowTag[0])
    runner.addJSFailure("[transfer->fromFlow] submit failed:"
        + getElement(runner, "transfer.fromFlow").value);
}

/* ask for 'to' flow from transfer page */
const transfer_to_contractID = "@aasii.org/test/transfer-to;1";
const transfer_to_name = "aaVCTransferToTest";
const transfer_to_CID = Components.ID("{3c13bbbd-927b-4b67-be0a-2f80691bed1e}");

function transfer_to_test(runner)
{
  sendOnClick(getElement(runner, "transfer.toFlow"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

const transfer_to_discard_contractID = "@aasii.org/test/transfer-to-discard;1";
const transfer_to_discard_name = "aaVCTransferToDiscardTest";
const transfer_to_discard_CID = Components.ID("{a04d7093-663b-4c24-9c96-028c67b89df3}");

function transfer_to_discard_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer->toFlow] wrong init");

  if (getElement(runner, "transfer.toFlow").value != "junk")
    runner.addJSFailure("[transfer->toFlow] discard failed");
}

const transfer_to_create_contractID = "@aasii.org/test/transfer-to-create;1";
const transfer_to_create_name = "aaVCTransferToCreateTest";
const transfer_to_create_CID = Components.ID("{4c2eed92-0acd-4a60-9003-13e558ce961e}");

function transfer_to_create_test(runner)
{
  flow_last_line_check(runner);
  runner.doCommand("cmd_page1_new");
  runner.doCommand("cmd_flow_give");
  runner.watchWindow = getFrame(runner).contentWindow;
}

const subflow_give_submit_contractID = "@aasii.org/test/subflow-give-submit;1";
const subflow_give_submit_name = "aaVCSubflowGiveSubmitTest";
const subflow_give_submit_CID = Components.ID("{9daed633-4b08-4220-8931-4152f7bc5066}");

const subflow_take_submit_contractID = "@aasii.org/test/subflow-take-submit;1";
const subflow_take_submit_name = "aaVCSubflowtakeSubmitTest";
const subflow_take_submit_CID = Components.ID("{6b652c77-8368-42ca-9768-8376bad338ae}");

const subflow_take_select_contractID = "@aasii.org/test/subflow-take-select;1";
const subflow_take_select_name = "aaVCSubflowtakeSelectTest";
const subflow_take_select_CID = Components.ID("{479b48f0-a20f-488d-b219-dbfee74b15bd}");

const subflow_entity_submit_contractID = "@aasii.org/test/subflow-entity-submit;1";
const subflow_entity_submit_name = "aaVCSubflowGiveSubmitTest";
const subflow_entity_submit_CID = Components.ID("{316884a0-4623-4c49-b214-819bc7b98bc6}");

const subflow_entity_select_contractID = "@aasii.org/test/subflow-entity-select;1";
const subflow_entity_select_name = "aaVCSubflowGiveSelectTest";
const subflow_entity_select_CID = Components.ID("{63ecefa6-b153-4d83-a812-f23ae777d0f0}");

const transfer_to_submit_contractID = "@aasii.org/test/transfer-to-submit;1";
const transfer_to_submit_name = "aaVCTransferToSubmitTest";
const transfer_to_submit_CID = Components.ID("{dbd1150c-c80c-4a95-8a02-2906c0d06dd3}");

function transfer_to_submit_test(runner)
{
  getElement(runner, "flow.tag").value = aaTestNames.FlowTag[1];
  sendOnInput(getElement(runner, "flow.tag"));

  getElement(runner, "flow.rate").value = "1.0";
  sendOnInput(getElement(runner, "flow.rate"));

  runner.doCommand("cmd_page1_save");
  add_flow_when_selecting_check(runner);
  runner.doCommand("cmd_page1_submit");
  runner.watchWindow = getFrame(runner).contentWindow;
}

const transfer_save_contractID = "@aasii.org/test/transfer-save;1";
const transfer_save_name = "aaVCTransferSaveTest";
const transfer_save_CID = Components.ID("{bcb12042-4b82-4e71-949b-228b444e344c}");

function transfer_save_test(runner)
{
  var amount = getElement(runner, "transfer.amount");
  amount.value = "300";
  sendOnInput(amount);
  if (! transfer_sync_check(runner))
    runner.addJSFailure("[transfer save] page out of sync");
  var date = getElement(runner, "transfer.date");
  date.value = "2008-02-04";
  sendOnChange(date);
  if (! transfer_sync_check(runner))
    runner.addJSFailure("[transfer save] page out of sync");

  var commit = getElement(runner, "transfer.commit");
  sendOnClick(commit);
  if (! transfer_sync_check(runner))
    runner.addJSFailure("[transfer save] page out of sync");
  var value = getElement(runner, "transfer.value");
  if (value.value != "30000.00")
    runner.addJSFailure("[transfer save] wrong value:" + value.value);
  var earnings = getElement(runner, "transfer.earnings");
  if (earnings.value != "")
    runner.addJSFailure("[transfer save] wrong earnings:" + earnings.value);
  if (getElement(runner,"transfer.commit").getAttribute("disabled") != "true")
    runner.addJSFailure("[transfer save] 'commit' not disabled");
  var reset = getElement(runner, "transfer.reset");
  sendOnClick(reset);
  if (getElement(runner,"transfer.commit").hasAttribute("disabled"))
    runner.addJSFailure("[transfer save] 'commit' not enabled");

  sendOnClick(getElement(runner, "transfer.fromFlow"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

function submit_after_reset_check(runner)
{
  var amount = getElement(runner, "transfer.amount");
  var date = getElement(runner, "transfer.date");
  var commit = getElement(runner, "transfer.commit");
  var value = getElement(runner, "transfer.value");
  var earnings = getElement(runner, "transfer.earnings");
  var reset = getElement(runner, "transfer.reset");

  if (getElement(runner, "transfer.fromFlow").value != aaTestNames.FlowTag[1])
    runner.addJSFailure("submit after reset failed:"
        + getElement(runner, "transfer.fromFlow").value);
  /* Prepare direct database access */
  var wnd = getFrame(runner).contentWindow.wrappedJSObject;
  var session = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
    .sessionHistory;
  var entityLoader = Components.classes["@aasii.org/storage/load-entity;1"]
    .createInstance(nsCI.aaISqlRequest);
  var entities = session.load(entityLoader);
  var resourceLoader = Components.classes["@aasii.org/storage/load-resource;1"]
    .createInstance(nsCI.aaISqlRequest);
  var resources = session.load(resourceLoader);
  var flow = Components.classes["@aasii.org/base/flow;1"]
    .createInstance(nsCI.aaIFlow);

  /* Create flow 'c' */
  flow.tag = "c";
  flow.entity = entities.queryElementAt(2, nsCI.aaIEntity);
  flow.giveResource = resources.queryElementAt(1, nsCI.aaIResource);
  flow.takeResource = resources.queryElementAt(0, nsCI.aaIResource);
  flow.rate = 150;
  session.save(flow, null);

  /* Create flow 'd' */
  flow = Components.classes["@aasii.org/base/flow;1"]
    .createInstance(nsCI.aaIFlow);
  flow.tag = "d";
  flow.entity = entities.queryElementAt(3, nsCI.aaIEntity);
  flow.giveResource = resources.queryElementAt(0, nsCI.aaIResource);
  flow.takeResource = resources.queryElementAt(0, nsCI.aaIResource);
  flow.rate = 1;
  session.save(flow, null);

  /* Load flows */
  var flowLoader = Components.classes["@aasii.org/storage/load-flow;1"]
    .createInstance(nsCI.aaISqlRequest);
  var flows = session.load(flowLoader);

  /* Add transfer 'b'->'c' of 200 */
  //wnd.gFact.takeFrom = flows.queryElementAt(1, nsCI.aaIFlow);
  wnd.gFact.giveTo = flows.queryElementAt(2, nsCI.aaIFlow);
  amount.value = "200";
  sendOnInput(amount);
  date.value = "2008-02-05";
  sendOnChange(date);
  sendOnClick(commit);
  if (value.value != "20000.00")
    runner.addJSFailure("[transfer save] wrong value:" + value.value);
  if (earnings.value != "10000.00")
    runner.addJSFailure("[transfer save] wrong earnings:" + earnings.value);
  sendOnClick(reset);

  /* Add transfer 'c'->'d' of 20000 */
  wnd.gFact.takeFrom = flows.queryElementAt(2, nsCI.aaIFlow);
  wnd.gFact.giveTo = flows.queryElementAt(3, nsCI.aaIFlow);
  amount.value = "20000";
  sendOnInput(amount);
  date.value = "2008-02-06";
  sendOnChange(date);
  sendOnClick(commit);
  if (value.value != "20000.00")
    runner.addJSFailure("[transfer save] wrong value:" + value.value);
  if (earnings.value != "")
    runner.addJSFailure("[transfer save] wrong earnings:" + earnings.value);
}

const submit_after_reset_contractID = "@aasii.org/test/submit-after-reset;1";
const submit_after_reset_name = "aaVCTransferToSubmitAfterReset";
const submit_after_reset_CID = Components.ID("{412fc32b-1aed-4efa-8456-ae5a4c2ef29a}");

/* test for bug 128 */
function add_flow_when_selecting_check(runner)
{
  var tree = getElement(runner, "page1.tree");
  var view = tree.view;
  
  if (getElement(runner, "page1.browseSet").hasAttribute("disabled"))
    runner.addJSFailure("edit status - bug 128 regression");
  if (view.getCellText(view.selection.currentIndex,tree.columns[0])
      != aaTestNames.FlowTag[1])
    runner.addJSFailure("selection - bug 128 regression");
}

/* test for rate change page */
const rate_page_contractID = "@aasii.org/test/rate-page;1";
const rate_page_name = "aaVCRatePageTest";
const rate_page_CID = Components.ID("{cb01568d-1fb2-4ea9-a468-8d00b50b4c65}");

function rate_sync_check(runner)
{
  var wnd = getFrame(runner).contentWindow.wrappedJSObject;
  var result = true;
  if (! wnd) {
    runner.addJSFailure("[rate sync] getting window");
    return false;
  }

  var details = wnd.view;
  if (! details) {
    runner.addJSFailure("[rate sync] details object is null");
    return false;
  }

  var quote;
  if (details.buffer)
    quote = details.buffer;
  else if (details._item)
    quote = details._item;
  
  var resourceBox = details._resource;
  var dateBox = details._date;
  var rateBox = details._rate;
  if (!quote) {
    if (resourceBox.value != clickToChoose) {
      runner.addJSFailure("[rate sync] resource.tag is wrong: "
          + resourceBox.value);
      result = false;
    }
    if (rateBox.value != "") {
      runner.addJSFailure("[rate sync] rate is wrong: "
          + rateBox.value);
      result = false;
    }
    return result;
  }
  quote = quote.QueryInterface(nsCI.aaIQuote);
  var date = new Date(quote.time / 1000);
  if (dateBox.value != date.toLocaleFormat('%Y-%m-%d')) {
    runner.addJSFailure("[rate sync] date is wrong: "
        + date);
    result = false;
  }
  if (rateBox.value != quote.rate) {
    runner.addJSFailure("[rate sync] rate is wrong: "
        + quote.rate);
    result = false;
  }

  return result;
}

function rate_page_test(runner)
{
  runner.doCommand("cmd_manage_rate");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function rate_check(runner)
{
  var result = true;
  if (getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
      .currentURI.spec != "chrome://abstract/content/rate.xul") {
    runner.addJSFailure(" [rate page] wrong xul document");
    result = false;
  }
  var tree = getElement(runner, "page1.tree");
  if (! tree.view.QueryInterface(nsCI.aaIDataTreeView)) {
    runner.addJSFailure("[rate page] tree view assingment");
    result = false;
  }
  if (! rate_sync_check(runner))
    result = false;
  return result;
}

function rate_page_check(runner)
{
  if (!rate_check(runner))
    runner.addJSFailure("[rate page] page out of sync");
  var tree = getElement(runner, "page1.tree");
  if (tree.view.rowCount != 0)
    runner.addJSFailure("[rate page] tree not empty");
  runner.doCommand("cmd_page1_new");
  if (! rate_sync_check(runner))
    runner.addJSFailure("[rate page] page out of sync");
  var date = getElement(runner, "quote.date");
  date.value = "2008-02-06";
  sendOnChange(date);
  if (! rate_sync_check(runner))
    runner.addJSFailure("[rate page] page out of sync");
  var rate = getElement(runner, "quote.rate");
  rate.value = "1.4995";
  sendOnInput(rate);
  if (! rate_sync_check(runner))
    runner.addJSFailure("[rate page] page out of sync");
}

/* ask for resource from rate page */
const rate_resource_contractID = "@aasii.org/test/rate-resource;1";
const rate_resource_name = "aaVCRateResourceTest";
const rate_resource_CID = Components.ID("{89919005-f3fb-4d35-8f35-fa0603defbdb}");

function rate_resource_test(runner)
{
  sendOnClick(getElement(runner, "quote.resource"));
  runner.watchWindow = getFrame(runner).contentWindow;
}

const rate_resource_discard_contractID = "@aasii.org/test/rate-resource-discard;1";
const rate_resource_discard_name = "aaVCRateResourceDiscardTest";
const rate_resource_discard_CID = Components.ID("{fd2c7c83-2551-44d4-a06f-b6562b622f7e}");

function rate_resource_discard_check(runner)
{
  if (! rate_check(runner))
    runner.addJSFailure("[rate page] wrong init");

  if (getElement(runner, "quote.resource").value != clickToChoose)
    runner.addJSFailure("[rate page] discard failed");
}

const rate_resource_submit_contractID = "@aasii.org/test/rate-resource-submit;1";
const rate_resource_submit_name = "aaVCRateResourceSubmitTest";
const rate_resource_submit_CID = Components.ID("{3668794f-4e7a-44d6-bb37-949dc576fba1}");

function rate_resource_submit_check(runner)
{
  if (! rate_check(runner))
    runner.addJSFailure("[rate page] wrong page init");

  if (getElement(runner, "quote.resource").value != aaTestNames.ResourceTag[3])
    runner.addJSFailure("[rate page] submit failed:"
        + getElement(runner, "quote.resource").value);
}

const rate_save_contractID = "@aasii.org/test/rate-save;1";
const rate_save_name = "aaVCRateSaveTest";
const rate_save_CID = Components.ID("{cc30d198-e1ce-4d24-889d-d93ed4857569}");

function rate_save_test(runner)
{
  runner.doCommand("cmd_page1_save");
  if (! rate_check(runner))
    runner.addJSFailure("[rate save] page out of sync");
}

const rate_digits_contractID = "@aasii.org/test/rate-digits;1";
const rate_digits_name = "aaVCRateDigitsTest";
const rate_digits_CID = Components.ID("{76c0d5aa-b122-4322-b139-89ad95c88163}");

function rate_digits_test(runner)
{
  var tree = getElement(runner, "page1.tree");
  if (tree.view.getCellText(0,tree.columns[2]) != "1.4995")
    runner.addJSFailure("ledger.tree.line1.col1 failed:"
        + tree.view.getCellText(0,tree.columns[2]));
}

/* test for general ledger page */
const ledger_page_contractID = "@aasii.org/test/ledger-page;1";
const ledger_page_name = "aaVCLedgerPageTest";
const ledger_page_CID = Components.ID("{49b95893-a0e9-4e5e-9e8f-b2ab2398dccd}");

function ledger_page_test(runner)
{
  runner.doCommand("cmd_report_ledger");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function ledger_check(runner)
{
  var result = true;
  if (getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
      .currentURI.spec != "chrome://abstract/content/ledger.xul") {
    runner.addJSFailure(" [ledger page] wrong xul document");
    return false;
  }
  var tree = getElement(runner, "ledger.tree");
  if (! tree) {
    runner.addJSFailure("[ledger page] tree view assingment");
    result = false;
  }
  return result;
}

function ledger_page_check(runner)
{
  if (!ledger_check(runner)) {
    runner.addJSFailure("[ledger page] page out of sync");
    return;
  }
  var tree = getElement(runner, "ledger.tree");
  if (tree.view.rowCount != 6)
    runner.addJSFailure("[ledger page] wrong row count:" + tree.view.rowCount);
  if (tree.view.getCellText(0,tree.columns[0]) != "2008-02-04")
    runner.addJSFailure("ledger.tree.line1.col1 failed");
  if (tree.view.getCellText(1,tree.columns[0]) != "")
    runner.addJSFailure("ledger.tree.line2.col1 failed");
  if (tree.view.getCellText(2,tree.columns[0]) != "2008-02-05")
    runner.addJSFailure("ledger.tree.line3.col1 failed");
  if (tree.view.getCellText(3,tree.columns[0]) != "")
    runner.addJSFailure("ledger.tree.line4.col1 failed");
  if (tree.view.getCellText(4,tree.columns[0]) != "2008-02-06")
    runner.addJSFailure("ledger.tree.line5.col1 failed:"
        + tree.view.getCellText(4,tree.columns[0]));
  if (tree.view.getCellText(5,tree.columns[0]) != "")
    runner.addJSFailure("ledger.tree.line6.col1 failed");

  if (tree.view.getCellText(0,tree.columns[1]) != aaTestNames.ResourceTag[1])
    runner.addJSFailure("ledger.tree.line1.col2 failed: "
        + tree.view.getCellText(0,tree.columns[1]));
  if (tree.view.getCellText(1,tree.columns[1]) != "")
    runner.addJSFailure("ledger.tree.line2.col2 failed");
  if (tree.view.getCellText(2,tree.columns[1]) != aaTestNames.ResourceTag[1])
    runner.addJSFailure("ledger.tree.line3.col2 failed");
  if (tree.view.getCellText(3,tree.columns[1]) != "")
    runner.addJSFailure("ledger.tree.line4.col2 failed");
  if (tree.view.getCellText(4,tree.columns[1]) != aaTestNames.ResourceTag[0])
    runner.addJSFailure("ledger.tree.line5.col2 failed:"
        + tree.view.getCellText(4,tree.columns[1]));
  if (tree.view.getCellText(5,tree.columns[1]) != "")
    runner.addJSFailure("ledger.tree.line6.col2 failed");

  if (tree.view.getCellText(0,tree.columns[2]) != "300.00")
    runner.addJSFailure("ledger.tree.line1.col3 failed");
  if (tree.view.getCellText(1,tree.columns[2]) != "")
    runner.addJSFailure("ledger.tree.line2.col3 failed");
  if (tree.view.getCellText(2,tree.columns[2]) != "200.00")
    runner.addJSFailure("ledger.tree.line3.col3 failed");
  if (tree.view.getCellText(3,tree.columns[2]) != "")
    runner.addJSFailure("ledger.tree.line4.col3 failed");
  if (tree.view.getCellText(4,tree.columns[2]) != "20000.00")
    runner.addJSFailure("ledger.tree.line5.col3 failed:"
        + tree.view.getCellText(4,tree.columns[2]));
  if (tree.view.getCellText(5,tree.columns[2]) != "")
    runner.addJSFailure("ledger.tree.line6.col3 failed");

  if (tree.view.getCellText(0,tree.columns[3]) != creditTerm)
    runner.addJSFailure("ledger.tree.line1.col4 failed");
  if (tree.view.getCellText(1,tree.columns[3]) != debitTerm)
    runner.addJSFailure("ledger.tree.line2.col4 failed");
  if (tree.view.getCellText(2,tree.columns[3]) != creditTerm)
    runner.addJSFailure("ledger.tree.line3.col4 failed");
  if (tree.view.getCellText(3,tree.columns[3]) != debitTerm)
    runner.addJSFailure("ledger.tree.line4.col4 failed");
  if (tree.view.getCellText(4,tree.columns[3]) != creditTerm)
    runner.addJSFailure("ledger.tree.line5.col4 failed:"
        + tree.view.getCellText(4,tree.columns[3]));
  if (tree.view.getCellText(5,tree.columns[3]) != debitTerm)
    runner.addJSFailure("ledger.tree.line6.col3 failed");

  if (tree.view.getCellText(0,tree.columns[4]) != aaTestNames.FlowTag[0])
    runner.addJSFailure("ledger.tree.line1.col5 failed:"
        + tree.view.getCellText(0,tree.columns[4]));
  if (tree.view.getCellText(1,tree.columns[4]) != aaTestNames.FlowTag[1])
    runner.addJSFailure("ledger.tree.line2.col5 failed");
  if (tree.view.getCellText(2,tree.columns[4]) != aaTestNames.FlowTag[1])
    runner.addJSFailure("ledger.tree.line3.col5 failed");
  if (tree.view.getCellText(3,tree.columns[4]) != "c")
    runner.addJSFailure("ledger.tree.line4.col5 failed");
  if (tree.view.getCellText(4,tree.columns[4]) != "c")
    runner.addJSFailure("ledger.tree.line5.col5 failed:"
        + tree.view.getCellText(4,tree.columns[4]));
  if (tree.view.getCellText(5,tree.columns[4]) != "d")
    runner.addJSFailure("ledger.tree.line6.col5 failed");

  if (tree.view.getCellText(0,tree.columns[5]) != "100.00")
    runner.addJSFailure("ledger.tree.line1.col6 failed");
  if (tree.view.getCellText(1,tree.columns[5]) != "100.00")
    runner.addJSFailure("ledger.tree.line2.col6 failed");
  if (tree.view.getCellText(2,tree.columns[5]) != "100.00")
    runner.addJSFailure("ledger.tree.line3.col6 failed");
  if (tree.view.getCellText(3,tree.columns[5]) != "150.00")
    runner.addJSFailure("ledger.tree.line4.col6 failed");
  if (tree.view.getCellText(4,tree.columns[5]) != "1.00")
    runner.addJSFailure("ledger.tree.line5.col6 failed:"
        + tree.view.getCellText(4,tree.columns[5]));
  if (tree.view.getCellText(5,tree.columns[5]) != "1.00")
    runner.addJSFailure("ledger.tree.line6.col6 failed");

  if (tree.view.getCellText(0,tree.columns[6]) != "")
    runner.addJSFailure("ledger.tree.line1.col7 failed");
  if (tree.view.getCellText(1,tree.columns[6]) != "30000.00")
    runner.addJSFailure("ledger.tree.line2.col7 failed");
  if (tree.view.getCellText(2,tree.columns[6]) != "")
    runner.addJSFailure("ledger.tree.line3.col7 failed");
  if (tree.view.getCellText(3,tree.columns[6]) != "30000.00")
    runner.addJSFailure("ledger.tree.line4.col7 failed");
  if (tree.view.getCellText(4,tree.columns[6]) != "")
    runner.addJSFailure("ledger.tree.line5.col7 failed:"
        + tree.view.getCellText(4,tree.columns[6]));
  if (tree.view.getCellText(5,tree.columns[6]) != "20000.00")
    runner.addJSFailure("ledger.tree.line6.col7 failed");

  if (tree.view.getCellText(0,tree.columns[7]) != "30000.00")
    runner.addJSFailure("ledger.tree.line1.col8 failed");
  if (tree.view.getCellText(1,tree.columns[7]) != "")
    runner.addJSFailure("ledger.tree.line2.col8 failed");
  if (tree.view.getCellText(2,tree.columns[7]) != "20000.00")
    runner.addJSFailure("ledger.tree.line3.col8 failed");
  if (tree.view.getCellText(3,tree.columns[7]) != "")
    runner.addJSFailure("ledger.tree.line4.col8 failed");
  if (tree.view.getCellText(4,tree.columns[7]) != "20000.00")
    runner.addJSFailure("ledger.tree.line5.col8 failed:"
        + tree.view.getCellText(4,tree.columns[7]));
  if (tree.view.getCellText(5,tree.columns[7]) != "")
    runner.addJSFailure("ledger.tree.line6.col7 failed");
}

/* test for amounts on transfer page - get to the page */
const tasp_CID = Components.ID("{e5712fda-f8d6-47b2-a5ca-e4d9e3887c8f}");
const tasp_contractID  = "@aasii.org/test/transfer-page-amounts1;1";
const tasp_name = "aaVCTransferPageAmounts1Test";

function tasp_select_from(runner)
{
  let page = getFrame(runner).docShell.QueryInterface(nsCI.nsIWebNavigation)
      .currentURI.spec;
  if (page != "chrome://abstract/content/transfer.xul")
    runner.addJSFailure("[transfer page amounts] wrong xul document: " + page);
  if (getElement(runner, "transfer.fromDebitAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong from Debit amount"
        + getElement(runner, "transfer.fromDebitAmount").value);
  if (getElement(runner, "transfer.fromCreditAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong from credit amount"
        + getElement(runner, "transfer.fromCreditAmount").value);
  if (getElement(runner, "transfer.toDebitAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong to Debit amount"
        + getElement(runner, "transfer.toDebitAmount").value);
  if (getElement(runner, "transfer.toCreditAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong to credit amount"
        + getElement(runner, "transfer.toCreditAmount").value);
}

/* test for amounts on transfer page - select from flow */
const tasff_CID = Components.ID("{5f5eb176-b640-4fd6-af2b-915db0311f06}");
const tasff_contractID  = "@aasii.org/test/transfer-page-amounts2;1";
const tasff_name = "aaVCTransferPageAmounts2Test";

function tasff_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer page amounts] wrong page init");
  if (getElement(runner, "transfer.fromDebitAmount").value != "20000.00")
    runner.addJSFailure("[transfer page amounts] wrong from Debit amount: "
        + getElement(runner, "transfer.fromDebitAmount").value);
  if (getElement(runner, "transfer.fromCreditAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong from credit amount: "
        + getElement(runner, "transfer.fromCreditAmount").value);
}

/* test for amounts on transfer page - select to flow */
const tastf_CID = Components.ID("{408876cd-4988-49bb-9e86-2d40dd7af921}");
const tastf_contractID  = "@aasii.org/test/transfer-page-amounts3;1";
const tastf_name = "aaVCTransferPageAmounts3Test";

function tastf_check(runner)
{
  if (! transfer_check(runner))
    runner.addJSFailure("[transfer page amounts] wrong page init");
  if (getElement(runner, "transfer.toDebitAmount").value != "-")
    runner.addJSFailure("[transfer page amounts] wrong from Debit amount: "
        + getElement(runner, "transfer.toDebitAmount").value);
  if (getElement(runner, "transfer.toCreditAmount").value != "30000.00")
    runner.addJSFailure("[transfer page amounts] wrong from credit amount: "
        + getElement(runner, "transfer.toCreditAmount").value);
}
