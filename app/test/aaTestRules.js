/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI                   = Components.interfaces;
const moduleName             = "aaTestRules";
const moduleCID              = "{ea4a8e7c-fe3a-4388-9e46-493f3b504a67}";
const moduleContractID       = "@aasii.org/abstract/test/vc-rules;1";

/*
 * Module entry point
 * The NSGetModule function is the magic entry point that XPCOM uses to find
 * what XPCOM components this module provides
 */
function NSGetModule(comMgr, fileSpec)
{
  var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Components.interfaces.mozIJSSubScriptLoader);
  loader.loadSubScript("resource:///modules/aaTestNames.jsm");
  loader.loadSubScript("resource:///modules/nsTestFrame.jsm");
  loader.loadSubScript("resource:///modules/aaUITestFrame.jsm");

  var aaVCTestModule = JSTestModule();
  aaVCTestModule.init = ModuleInit;
  aaVCTestModule.init();
  return aaVCTestModule;
}

function ModuleInit()
{
  this._name = moduleName;
  this._CID = Components.ID(moduleCID);
  this._contractID = moduleContractID;
  
  var _jsFlow = aaJSTestPageFlowModule(0);
  _jsFlow.init = _jsFlow_init;
  _jsFlow.init();
  this._add(_jsFlow);

  var _jsRate = aaJSTestPageRateModule(1);
  _jsRate.init = _jsRate_init;
  _jsRate.init();
  this._add(_jsRate);
}

function _jsFlow_init() {
  this._CID = Components.ID("{66FDEF5B-E13E-40e3-83B8-7AC1F3089830}");
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{806B0881-FE44-4fb1-8E90-FA65A1F6A379}"),
        object: {
          command: 'cmd_process_flow'
        }
      }
    },
    {
      name: 'page1_cids',
      data: {
        cid_new: Components.ID("{C2BF4293-C665-40a3-9904-3F33CA8767A0}"),
        cid_cancel: Components.ID("{8EF4E6AC-EDEC-49e1-9BBD-F4E87D8F5037}")
      }
    },
    {
      name: 'new_cmd'
    }
  ];
  this.createTestsFlow(flow);

  var _jsRule = aaJSTestPageRuleModule(this._tests.length);
  _jsRule.init = _jsRule_init;
  _jsRule.init();
  this._add(_jsRule);

  flow = [
    {
      name: 'cancel_cmd'
    }
  ];
  flow[flow.length] = {
    name: 'sort',
    data: {
      cid: Components.ID("{89FDAA4B-A5ED-4b4d-A151-9AC109CC78D0}")
    }
  };
  flow[flow.length] = {
    name: 'filter',
    data: {
      cid: Components.ID("{CF577695-7266-4acc-B94C-FA9A5617DC51}"),
      object: {
        text: aaTestNames.FlowTag[0],
        type: "tree.tag"
      }
    }
  };
  this.createTestsFlow(flow);
}

function _jsRule_init() {
  this._CID = Components.ID("{98F8DCC9-9197-4342-B18E-02D968525EDB}");
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{77BFBE9D-8EAA-4145-BFC0-1E0392715B6F}"),
        selectable: true,
        object: {
          click: 'flow.rules'
        }
      }
    },
    {
      name: 'page1_cids',
      data: {
        cid_new: Components.ID("{C380E133-8BDC-4c4d-97C1-FE20B9F1FA4C}"),
        cid_change: Components.ID("{A40AB170-E921-4e99-8813-74F6A4E9C78E}"),
        cid_cancel: Components.ID("{4B0F4A98-EFF8-4254-8888-E62FFB82F316}"),
        cid_reset: Components.ID("{7249A9F5-0174-45c5-90C6-2593B786EBBD}"),
        cid_save: Components.ID("{710117FD-3A43-44b2-9686-50B47F1E466C}")
      }
    },
    {
      name: 'fill',
      data: {
        cid: Components.ID("{16CB632A-D3C5-4cd6-A5FC-4202B7B9EFDA}"),
        object: {
          tag: 'shipment1.rule1',
          rate: 32.0,
          change_fact_side: 1,
          takeFrom: {
            page: {
              open_cid: Components.ID("{F6C8D354-7247-4F7F-A257-DE16201F84A1}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{A4B279CA-8604-47EC-8A5F-2F6CC8CFAC32}"),
                  object: {
                    tag: aaTestNames.FlowTag[0],
                    entity: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{68AE2BFA-535F-4DD3-99CD-90FD27D5576D}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.takefrom").value != "")
                        errorHandler.addJSFailure(runner, "[flow.close] flow discard failed");
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{E2464642-C31C-4DA8-A664-172887DE6C40}"),
                  object: {
                    command: 'cmd_b_takefrom'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{4E186B6C-D776-492E-829F-203B43B27FCF}"),
                  object: {
                    tag: aaTestNames.FlowTag[0],
                    entity: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{2A3A06EA-06B5-4170-9410-A30DF3BFF195}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.takefrom").value != aaTestNames.FlowTag[0])
                        errorHandler.addJSFailure(runner, "[flow.close] flow submit failed");
                    }
                  }
                }
              }
            ]
          },
          giveTo: {
            page: {
              open_cid: Components.ID("{189A5B0D-A538-4982-B4D3-6B77FBAE7330}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{E966BFB2-DB8E-4749-ABDF-A3D159D24F87}"),
                  object: {
                    tag: aaTestNames.FlowTag[1],
                    entity: aaTestNames.EntityTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{5288E088-CF7F-4ADF-9A76-307DD5B9C736}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.giveto").value != "")
                        errorHandler.addJSFailure(runner, "[flow.close] flow discard failed");
                    }
                   }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{C417C0AF-DDDD-40D3-A9BE-A525C6238756}"),
                   object: {
                    command: 'cmd_b_giveto'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{D03CE76B-A4EA-434A-8D4C-B216A3A0D313}"),
                  object: {
                    tag: aaTestNames.FlowTag[1],
                    entity: aaTestNames.EntityTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{75182211-3891-4FD6-84F3-D0C451C2C521}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.giveto").value != aaTestNames.FlowTag[1])
                        errorHandler.addJSFailure(runner, "[flow.close] flow submit failed");
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{3D52FE95-E2F0-4b72-9932-9AD68DD1709C}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] page1.tree selection not cleared");
          }
        }
      }
    },
    {
      name: 'select',
      data: {
        cid: Components.ID("{2EEE67FD-C6FB-4214-BDBD-ECE3DA05B2BB}"),
        object: {
          tag: 'shipment1.rule1'
        }
      }
    },
    {
      name: 'new_cmd'
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{42C58370-F33C-4c96-8C82-A34152CC97AB}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[0]) != 'shipment1.rule1') {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cancel command do not restore selection");
            }
          } else {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] selection count is 0");
          }
        }
      }
    },
    {
      name: 'change_cmd'
    },
    {
      name: 'reset_cmd'
    },
    {
      name: 'cancel_cmd'
    },
    {
      name: 'change_cmd',
      data: {
        cid: Components.ID("{005E379F-E95A-438b-B3CF-1C7EEF6F288D}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "rule.tag");
          if (!_tag) errorHandler.addJSFailure(runner, "[change_cmd.checkF] tag element not found");
          _tag.value = 'temp';
          sendOnInput(_tag);
        }
      }
    },
    {
      name: 'reset_cmd',
      data: {
        cid: Components.ID("{D5FD9A9F-5077-470d-9CB1-BF3A4EE7F737}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "rule.tag");
          if (!_tag) errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element not found");
          if (_tag.value != 'shipment1.rule1') {
            errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element is not reseted");
          }
        }
      }
    },
    {
      name: 'cancel_cmd'
    },
    {
      name: 'fill',
      data: {
        cid: Components.ID("{652B0403-6570-4180-81D0-48AE27E5F5AF}"),
        object: {
          tag: 'shipment1.rule2',
          rate: 14.0,
          change_fact_side: 0,
          giveTo: {
            page: {
              open_cid: Components.ID("{2DE17F10-6DD6-4A7E-86DA-695AEDE660D2}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{DDD57D8F-B111-4385-82CB-BE3283AE3128}"),
                  object: {
                    tag: aaTestNames.FlowTag[0],
                    entity: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{B4D43C49-EDD9-4A70-8F64-1CEB89B280AC}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.giveto").value != "") {
                        errorHandler.addJSFailure(runner, "[flow.close] flow discard failed");
                      }
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{0B22863D-14D9-46BC-AC55-7FCB1C7EA568}"),
                  object: {
                    command: 'cmd_b_giveto'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{2B56B308-B9D3-429E-BB71-274EAA599F12}"),
                  object: {
                    tag: aaTestNames.FlowTag[0],
                    entity: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{AA12591D-6CF3-4DBE-A1FA-8835650E5A3C}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.giveto").value != aaTestNames.FlowTag[0])
                        errorHandler.addJSFailure(runner, "[flow.close] flow submit failed");
                    }
                  }
                }
              }
            ]
          },
          takeFrom: {
            page: {
              open_cid: Components.ID("{29F02156-C61C-41D4-947E-B79E9219C482}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{55E2A693-B0D2-4A24-AEC0-1AEB66CE9D94}"),
                  object: {
                    tag: aaTestNames.FlowTag[1],
                    entity: aaTestNames.EntityTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{93DCEFE5-E8AA-4841-8E5A-6C0F6440ED93}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.takefrom").value != "") {
                        errorHandler.addJSFailure(runner, "[flow.close] flow discard failed");
                      }
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{A6BA7CE4-135F-47D0-912A-A7B5429ADC0C}"),
                  object: {
                    command: 'cmd_b_takefrom'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{0ADA9A93-888B-492B-97CA-C26F894F2A3E}"),
                  object: {
                    tag: aaTestNames.FlowTag[1],
                    entity: aaTestNames.EntityTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{0D49426E-24A5-4829-A60E-C45271DA7CF2}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "rule.tb.takefrom").value != aaTestNames.FlowTag[1])
                        errorHandler.addJSFailure(runner, "[flow.close] flow submit failed");
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    {
      name: 'sort',
      data: {
        cid: Components.ID("{430E02EB-EEFB-4700-9CCE-D2C967997179}")
      }
    },
    {
      name: 'filter',
      data: {
        cid: Components.ID("{4159793E-A991-4c43-8117-9F2397A3FDD8}"),
        object: {
          text: "shipment1.rule2",
          type: "tree.tag"
        }
      }
    },
    {
      name: 'close',
      data: {
        cid: Components.ID("{FC4C530A-FB51-435d-A338-E53B60E8440F}"),
        object: {
          command: 'cmd_page1_submit',
          checkF: function(runner, errorHandler) {
            var page = getFrame(runner).contentWindow.wrappedJSObject.view;
            if (!page.buffer) {
              errorHandler.addJSFailure(runner, "[rules submit check] flow buffer is null");
            } else {
              if (page.buffer.QueryInterface(nsCI.nsIArray).length != 2) {
                errorHandler.addJSFailure(runner, "[rules submit check] flow buffer rules count != 2; Value: " + page.buffer.QueryInterface(nsCI.nsIArray).length);
              } else {
                var rule = page.buffer.QueryInterface(nsCI.nsIArray).queryElementAt(0, nsCI.aaIRule);
                if (rule.tag != "shipment1.rule1" ||
                    rule.rate != 32.0 ||
                    rule.changeSide != true ||
                    rule.factSide != true ||
                    rule.takeFrom.tag != aaTestNames.FlowTag[0] ||
                    rule.giveTo.tag != aaTestNames.FlowTag[1]) {
                  errorHandler.addJSFailure(runner, "[rules submit check] rule is not equal; rule tag: " + rule.tag);
                }
                rule = page.buffer.QueryInterface(nsCI.nsIArray).queryElementAt(1, nsCI.aaIRule);
                if (rule.tag != "shipment1.rule2" ||
                    rule.rate != 14.0 ||
                    rule.changeSide != true ||
                    rule.factSide != false ||
                    rule.takeFrom.tag != aaTestNames.FlowTag[1] ||
                    rule.giveTo.tag != aaTestNames.FlowTag[0]) {
                  errorHandler.addJSFailure(runner, "[rules submit check] rule is not equal; rule tag: " + rule.tag);
                }
              }
            }
          }
        }
      }
    }
  ];
  this.createTestsFlow(flow);
}

function _jsRate_init() {
  this._CID = Components.ID("{382CE6A3-5F81-4b2c-8C3D-F15BAFC3E76A}");
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{9BE2FE3E-0572-4fde-8819-509240277792}"),
        object: {
          command: 'cmd_manage_rate'
        }
      }
    },
    {
      name: "sort",
      data: {
        cid: Components.ID("{0A1CA067-3FAF-43ae-8969-3B3E7D179C3E}")
      }
    },
    {
      name: 'filter',
      data: {
        cid: Components.ID("{A706B83F-1877-45ac-86C8-4321146F12C3}"),
        object: {
          text: "EUR",
          type: "tree.resource"
        }
      }
    }
  ];
  this.createTestsFlow(flow);
}

