/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

//var testStorageFile = Components.interfaces.nsIFile;
var mFile = Components.interfaces.nsIFile;

const nsCI                   = Components.interfaces;
const moduleName             = "aaVCTest";
const moduleCID              = "{c23f7275-3b2f-432a-b2fe-c560b7b0a069}";
const moduleContractID       = "@aasii.org/abstract/test/vc-base;1";

Components.utils.import("resource:///modules/aaTestVC.jsm");
Components.utils.import("resource:///modules/aaUITestUtils.jsm");

/*
 * Module entry point
 * The NSGetModule function is the magic entry point that XPCOM uses to find
 * what XPCOM components this module provides
 */
function NSGetModule(comMgr, fileSpec)
{
  var uiFrame = {};
  var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Components.interfaces.mozIJSSubScriptLoader);
  loader.loadSubScript("resource:///modules/aaTestNames.jsm");
  loader.loadSubScript("resource:///modules/nsTestFrame.jsm");
  loader.loadSubScript("resource:///modules/aaUITestFrame.jsm");

  var aaVCTestModule = JSTestModule();
  aaVCTestModule.init = ModuleInit;
  aaVCTestModule.init();
  return aaVCTestModule;
}

function ModuleInit()
{
  var i = 0;
  this._name = moduleName;
  this._CID = Components.ID(moduleCID);
  this._contractID = moduleContractID;

  /*  0 */
  this._add(start_CID, start_contractID, start_name,
      start_page_test, start_page_check);
  /*  1 */ i++;
  this._add(open_storage_CID, open_storage_contractID, open_storage_name,
      open_storage_test, null);
  /*  2 */ i++;
  this._add(close_storage_CID, close_storage_contractID, close_storage_name,
      close_storage_test, null);
  /*  3 */ i++;
  this._add(top_CID, top_contractID, top_name, top_test, top_check);
  /*  4 */ i++;
  var _jsEntityPage = aaJSTestPageEntityModule(i);
  _jsEntityPage.init = _jsEntityPage_init;
  _jsEntityPage.init();
  this._add(_jsEntityPage);
  /*  5 */ i++;
  var _jsResourcePage = aaJSTestPageResourceModule(i);
  _jsResourcePage.init = _jsResourcePage_init;
  _jsResourcePage.init();
  this._add(_jsResourcePage);
  /*  6 */ i++;
  var _jsFlowPage = aaJSTestPageFlowModule(i);
  _jsFlowPage.init = _jsFlowPage_init;
  _jsFlowPage.init();
  this._add(_jsFlowPage);
}

/* Module-specific Defines and Utilities */
var contentId = "content";
var treeId = "page1.tree";
var gItem = -1;

/* Top Window Test */
const top_contractID = "@aasii.org/abstract/test/vc-top;1";
const top_name = "aaVCTopTest";
const top_CID = Components.ID("{5274ac84-fc9c-4bf9-9689-0a89f5638e1c}");

function top_test(runner)
{
  var manager = Components.classes["@aasii.org/storage/manager;1"]
    .createInstance(nsCI.aaIManager);

  if (!runner.hasWindow) 
    runner.watchWindow = runner.testWindow;
  else
    this.notify();
}

function top_check(runner)
{
}

/* 'cmd_show1' Test */
function _jsEntityPage_init() {
  this._CID = Components.ID("{75DB5E70-71A7-4dae-B2FF-855E8A8AB935}");
  var i = 0;
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{0284C590-7C98-4ad0-8A87-3A02B2924F75}"),
        object: {
          command: 'cmd_view_entity'
        }
      }
    },
    {
      name: 'open',
      data: {
        cid: Components.ID("{6FB3A1BF-0FB4-4aef-9C29-9DE356CF0B14}"),
        object: {
          command: 'cmd_view_entity'
        }
      }
    },
    {
      name: 'page1_cids',
      data: {
        cid_new: Components.ID("{774EE770-D1DD-4e9b-A19D-639A00CCA2BC}"),
        cid_change: Components.ID("{4D8DAF1D-E7A1-4757-815D-5AF9EB750C6C}"),
        cid_cancel: Components.ID("{63DE541A-9ADD-490e-B162-893C4F616582}"),
        cid_reset: Components.ID("{337D2D9B-3933-4551-8720-27ECC2493EFE}"),
        cid_save: Components.ID("{868BAE44-18D8-41c3-A015-B54D56B83F0F}")
      }
    },
    {
      name: 'fill',
      data: {
        cid: aaTestNames.EntityTagFillCid[i],
        object: {
          tag: aaTestNames.EntityTag[i]
        }
      }
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{4E2B1849-A5D5-420e-A19F-CEAA31B0C88E}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] page1.tree selection not cleared");
          }
        }
      }
    },
    {
      name: 'select',
      data: {
        cid: Components.ID("{C3FDC9A2-E916-4b39-9A1E-D9480903893B}"),
        object: {
          tag: aaTestNames.EntityTag[i]
        }
      }
    },
    {
      name: 'new_cmd'
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{73D241C8-9A1B-4ae3-B69B-FF952183D3B7}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[0]) != aaTestNames.EntityTag[i]) {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cancel command do not restore selection");
            }
          } else {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] selection count is 0");
          }
        }
      }
    },
    {
      name: 'change_cmd'
    },
    {
      name: 'reset_cmd'
    },
    {
      name: 'cancel_cmd'
    },
    {
      name: 'change_cmd',
      data: {
        cid: Components.ID("{9AB02AD5-FAEE-4ec5-A684-AD797C1B8EA3}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "entity.tag");
          if (!_tag) errorHandler.addJSFailure(runner, "[change_cmd.checkF] tag element not found");
          _tag.value = 'temp';
          sendOnInput(_tag);
        }
      }
    },
    {
      name: 'reset_cmd',
      data: {
        cid: Components.ID("{3DB4AE30-03B7-476d-B4B5-D68AB0BDB72C}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "entity.tag");
          if (!_tag) errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element not found");
          if (_tag.value != aaTestNames.EntityTag[i]) {
            errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element is not reseted");
          }
        }
      }
    },
    {
      name: 'cancel_cmd'
    },
    //message tests
    {
      name: 'msg',
      data: {
        cid: Components.ID("{DFA0976B-7760-475f-82AB-A2938C1F2508}"),
        object: {
          type: 'INFO',
          textID: 'info.test.message'
        }
      }
    },
    {
      name: 'msg',
      data: {
        cid: Components.ID("{682BB043-4424-4142-B261-BE5CB8E0BE0E}"),
        object: {
          type: 'WARNING',
          textID: 'warning.test.message'
        }
      }
    },
    {
      name: 'msg',
      data: {
        cid: Components.ID("{DFC81FA5-C9CA-4d78-BA44-4CC87627ACA9}"),
        object: {
          type: 'CRITICAL',
          textID: 'critical.test.message'
        }
      }
    },
    {
      name: 'msg',
      data: {
        cid: Components.ID("{B18381C3-58B2-42bf-A41F-29547EC2FF43}"),
        object: {
          type: 'BLOCK',
          textID: 'block.test.message'
        }
      }
    }
    //end messages tests
  ];
  var lng = flow.length;
  var b = i + 1;
  for (var l = lng; b < aaTestNames.EntityTag.length; b++, l++) {
    flow[l] = {
      name: 'fill',
      data: {
        cid: aaTestNames.EntityTagFillCid[b],
        object: {
          tag: aaTestNames.EntityTag[b]
        }
      }
    }
  }
  flow[flow.length] = {
    name: 'sort',
    data: {
      cid: Components.ID("{40A14580-3658-4bad-9F20-37BC3D4D62FF}")
    }
  }
  flow[flow.length] = {
    name: 'filter',
    data: {
      cid: Components.ID("{3B44C5C4-DC4C-4671-8423-989540928110}"),
      object: {
        text: aaTestNames.EntityTag[0],
        type: "tree.name"
      }
    }
  }
  this.createTestsFlow(flow);
}

/* 'cmd_show2' Test */
function _jsResourcePage_init() {
  this._CID = Components.ID("{BC1E6555-F98B-4914-B8DF-645D248A9327}");
  var i = 0;
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{7457678D-D601-4fb8-BC07-05D1E2BE9347}"),
        object: {
          command: 'cmd_view_resource'
        }
      }
    },
    {
      name: 'page1_cids',
      data: {
        cid_new: Components.ID("{B482F889-04CA-4fab-9277-7A414CA1F542}"),
        cid_change: Components.ID("{14D998DE-55C8-4d6e-9414-6BC25DF6CAE5}"),
        cid_cancel: Components.ID("{B29E2FF2-FBBC-4988-8C1F-845CE88B1A9E}"),
        cid_reset: Components.ID("{FA4A105A-EA26-439e-890D-B0840E28F6DA}"),
        cid_save: Components.ID("{70B20575-AC07-4d07-AFBA-C09426B095CA}")
      }
    },
    {
      name: 'fill',
      data: {
        cid: aaTestNames.ResourceFillCid[i],
        object: {
          isMoney: aaTestNames.ResourceType[i],
          tag: aaTestNames.ResourceTag[i],
          code: aaTestNames.ResourceCode[i]
        }
      }
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{8A9C9B9F-6230-4b7b-8C1B-8DAC2D3F3B5A}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] page1.tree selection not cleared");
          }
        }
      }
    },
    {
      name: 'select',
      data: {
        cid: Components.ID("{F96640F7-0005-4da7-B737-9D4A9B70403D}"),
        object: {
          isMoney: aaTestNames.ResourceType[i],
          tag: aaTestNames.ResourceTag[i]
        }
      }
    },
    {
      name: 'new_cmd'
    },
    {
      name: 'cancel_cmd',
      data: {
        cid: Components.ID("{007D2920-5195-48da-B60E-51E1E739B52D}"),
        checkF: function(runner, errorHandler) {
          var tree = getElement(runner, "page1.tree");
          if (!tree) {
            errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cann't find page1.tree");
          }
          if (tree.view.selection.currentIndex > -1) {
            if (tree.view.getCellText(tree.view.selection.currentIndex, tree.columns[1]) != aaTestNames.ResourceTag[i]) {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cancel command do not restore selection, tag is not equal to selected row value");
            }
            if (aaTestNames.ResourceType[i]) {
              if (tree.view.getCellText(tree.view.selection.currentIndex,
                    tree.columns[0]) != chooseTypeSymbol(1)) {
                errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cancel command do not restore selection; type is not a money");
              }
            } else {
              if (tree.view.getCellText(tree.view.selection.currentIndex,
                    tree.columns[0]) != chooseTypeSymbol(0)) {
                errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] cancel command do not restore selection; type is not a asset");
              }
            }
          } else {
              errorHandler.addJSFailure(runner, "[cancel_cmd.checkF] selection count is 0");
          }
        }
      }
    },
    {
      name: 'change_cmd'
    },
    {
      name: 'reset_cmd'
    },
    {
      name: 'cancel_cmd'
    },
    {
      name: 'change_cmd',
      data: {
        cid: Components.ID("{AD42A323-4910-4d7c-8FB5-1C4D8BBCAF80}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "resource.tag");
          if (!_tag) errorHandler.addJSFailure(runner, "[change_cmd.checkF] tag element not found");
          _tag.value = 'temp';
          sendOnInput(_tag);
          if (aaTestNames.ResourceType[i]) {
            var _code = getElement(runner, "resource.code");
            if (!_code) errorHandler.addJSFailure(runner, "[change_cmd.checkF] code element not found");
            _code.value = 333;
            sendOnInput(_code);
          }
        }
      }
    },
    {
      name: 'reset_cmd',
      data: {
        cid: Components.ID("{8E77E77C-DC9F-4236-A17B-0ECB86E74BDF}"),
        checkF: function(runner, errorHandler) {
          var _tag = getElement(runner, "resource.tag");
          var _isMoney = getElement(runner, "resource.isMoney");
          var _code = getElement(runner, "resource.code");
          if (!_tag) errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element not found");
          if (!_isMoney) errorHandler.addJSFailure(runner, "[reset_cmd.checkF] isMoney element not found");
          if (!_code) errorHandler.addJSFailure(runner, "[reset_cmd.checkF] code element not found");
          
          if (_tag.value != aaTestNames.ResourceTag[i]) {
            errorHandler.addJSFailure(runner, "[reset_cmd.checkF] tag element is not reseted");
          }
          
          if (aaTestNames.ResourceType[i]) {
            if (_isMoney.checked != true) {
              errorHandler.addJSFailure(runner, "[reset_cmd.checkF] isMoney element is not checked");
            }
            if (_code.value != aaTestNames.ResourceCode[i]) {
              errorHandler.addJSFailure(runner, "[reset_cmd.checkF] code element is not reseted");
            }
          }
        }
      }
    },
    {
      name: 'cancel_cmd'
    }
  ];
  var lng = flow.length;
  var b = i + 1;
  for (var l = lng; b < aaTestNames.ResourceTag.length; b++, l++) {
    flow[l] = {
      name: 'fill',
      data: {
        cid: aaTestNames.ResourceFillCid[b],
        object: {
          isMoney: aaTestNames.ResourceType[b],
          tag: aaTestNames.ResourceTag[b],
          code: aaTestNames.ResourceCode[b]
        }
      }
    };
  }
  flow[flow.length] = {
    name: 'sort',
    data: {
      cid: Components.ID("{5E2CE621-8928-4641-910F-9F9A40D5F03B}")
    }
  }
  flow[flow.length] = {
    name: 'filter',
    data: {
      cid: Components.ID("{15B5EBFA-B65F-40b3-8E5A-0D4DE2B1FF94}"),
      object: {
        text: aaTestNames.ResourceTag[0],
        type: "tree.name"
      }
    }
  }
  this.createTestsFlow(flow);
}

/* flow Test */
function _jsFlowPage_init() {
  this._CID = Components.ID("{314BDFD7-4576-48ab-8351-B62FB0AEB6F2}");
  var i = 0;
  var flow = [
    {
      name: 'open',
      data: {
        cid: Components.ID("{861A615A-9876-492b-B1BB-734E8EFB4E8C}"),
        object: {
          command: 'cmd_process_flow'
        }
      }
    },
    {
      name: 'page1_cids',
      data: {
        cid_new: Components.ID("{6D8C69E3-43F8-4c6e-BCCD-7DB18B65FB50}"),
        cid_change: Components.ID("{B0DD62D6-268C-4d38-81B6-D78E41C656FF}"),
        cid_cancel: Components.ID("{7E29E38E-A120-418b-983A-5392608C4C17}"),
        cid_reset: Components.ID("{F5EB3B64-4967-4193-9E5F-1D9778F30080}"),
        cid_save: Components.ID("{209446AE-8813-4f22-BF9F-7CF5B56769D1}")
      }
    },
    {
      name: 'fill',
      data: {
        cid: aaTestNames.FlowCid[i],
        object: {
          isLight: false,
          tag: aaTestNames.FlowTag[i],
          direction: 1,
          rate: 100,
          isOffBalance: false,
          entity: {
            page: {
              open_cid: Components.ID("{FE90237F-FF3B-49c9-9FD4-DEFA40D5B3BC}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{810E38F6-9983-4969-89B3-4BB2E3A1EAEF}"),
                  object: {
                    tag: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{616B463B-7A48-4eb6-84FA-CC8D229EB88A}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "entity.tag").value != "")
                        errorHandler.addJSFailure(runner, "[flow.close] entity discard failed");
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{EB84678B-BBBC-4523-A1D2-645422B096A5}"),
                  object: {
                    command: 'cmd_flow_entity'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{F17886EA-8FC7-4ea1-8DFE-549F7F034069}"),
                  object: {
                    tag: aaTestNames.EntityTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{B3CCD2C0-AA2F-4685-ACA2-CDB79197A8A8}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "entity.tag").value != aaTestNames.EntityTag[0])
                        errorHandler.addJSFailure(runner, "[flow.close] entity submit failed");
                    }
                  }
                }
              }
            ]
          },
          giveRes: {
            page: {
              open_cid: Components.ID("{8AB0F06D-DAB4-4096-BA2B-2CB037CE703E}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{112CD1EF-9BC0-477d-B6F9-278E6FF35A34}"),
                  object: {
                    isMoney: aaTestNames.ResourceType[0],
                    tag: aaTestNames.ResourceTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{3DD08D00-3B0E-4bd0-B239-0B6B8DE2ABFF}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "give.tag").value != "")
                        errorHandler.addJSFailure(runner, "[flow.close] give discard failed");
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{B842C738-8A23-44a2-8C4D-B76AD1DB8019}"),
                  object: {
                    command: 'cmd_flow_give'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{A60FEE0B-D0E1-499a-B890-2DD4D15210C2}"),
                  object: {
                    isMoney: aaTestNames.ResourceType[0],
                    tag: aaTestNames.ResourceTag[0]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{D86EE18F-5EBE-457b-9EDD-127492417553}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "give.tag").value != aaTestNames.ResourceTag[0])
                        errorHandler.addJSFailure(runner, "[flow.close] give submit failed");
                    }
                  }
                }
              }
            ]
          },
          takeRes: {
            page: {
              open_cid: Components.ID("{F36B7E54-894D-4654-905A-635844C790A6}")
            },
            flow: [
              {
                name: 'select',
                data: {
                  cid: Components.ID("{D051666F-0F6A-466f-9A93-84A5AB2AF970}"),
                  object: {
                    isMoney: aaTestNames.ResourceType[1],
                    tag: aaTestNames.ResourceTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{5E09C305-EF39-4bdd-84F7-4062AA461141}"),
                  object: {
                    command: 'cmd_page1_discard',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "take.tag").value != "")
                        errorHandler.addJSFailure(runner, "[flow.close] take discard failed");
                    }
                  }
                }
              },
              {
                name: 'open',
                data: {
                  cid: Components.ID("{CC9B3286-2283-46d2-8D5C-9DC9CBFCE575}"),
                  object: {
                    command: 'cmd_flow_take'
                  }
                }
              },
              {
                name: 'select',
                data: {
                  cid: Components.ID("{E9E65FAD-FDF1-4252-B258-115A982055FB}"),
                  object: {
                    isMoney: aaTestNames.ResourceType[1],
                    tag: aaTestNames.ResourceTag[1]
                  }
                }
              },
              {
                name: 'close',
                data: {
                  cid: Components.ID("{8C660F04-9021-4f3d-91DA-49858E286B5F}"),
                  object: {
                    command: 'cmd_page1_submit',
                    checkF: function(runner, errorHandler) {
                      if (getElement(runner, "take.tag").value != aaTestNames.ResourceTag[1])
                        errorHandler.addJSFailure(runner, "[flow.close] take submit failed");
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  ];
  this.createTestsFlow(flow);
}

const start_contractID = "@aasii.org/abstract/test/start-page;1";
const start_name = "aaVCStartPageTest";
const start_CID = Components.ID("{34d31427-05cd-4743-9f4b-6a4384e6337a}");

function start_page_check(runner)
{
  var result = false;
  
  var startPage = getElement(runner,"start_page");
  if (!startPage) {
    runner.addJSFailure("start page is null");
    result = false;
  }
  var setButton = getElement(runner, "start_page", "set_storage");
  if (!setButton)
  {
    runner.addJSFailure("start page: set_storage is null");
    result = false;
  }
  if (setButton.getAttribute("collapsed") == "true")
  {
    runner.addJSFailure("start page: set_storage button should be visible");
    result = false;
  }

  var getButton = getElement(runner, "start_page", "create_storage");
  if (!getButton)
  {
    runner.addJSFailure("start page: create_storage is null");
    result = false;
  }
  if (getButton.getAttribute("collapsed") == "true")
  {
    runner.addJSFailure("start page: create_storage button should be visible");
    result = false;
  }

  return result;
}

function start_page_test(runner)
{
  runner.doCommand("cmd_start");
  runner.watchWindow = getFrame(runner).contentWindow;
}

const open_storage_contractID = "@aasii.org/abstract/test/storage-open;1";
const open_storage_name = "aaVCOpenStorageTest";
const open_storage_CID = Components.ID("{6f8b5da2-a533-497a-bc5e-255bcc60f588}");

function open_storage_test(runner)
{
  mFile = Components.classes["@mozilla.org/file/directory_service;1"]
    .getService(Components.interfaces.nsIProperties)
    .get("CurProcD", Components.interfaces.nsIFile);
  mFile.append("myBase.sqlite");
  if (mFile.exists())
    mFile.remove(true);
  var doc = runner.testWindow.document;
  if ( doc.documentElement.tagName != "window" )
    runner.addJSFailure("main window not created");

  var web = doc.getElementById("content").docShell
    .QueryInterface(nsCI.nsIWebNavigation);

  var session = web.sessionHistory.QueryInterface(nsCI.aaISHistory);  
  if (!session)
    runner.addJSFailure("session not started");
  session.setFile(mFile);
  var ioService = Components.classes["@mozilla.org/network/io-service;1"]  
    .getService(Components.interfaces.nsIIOService);
  var uri = session.connectionURI.QueryInterface(nsCI.nsIURL);
  if (uri == null) 
    runner.addJSFailure("URI is null");
  if (uri.fileName != "myBase.sqlite")
    runner.addJSFailure("Filename is wrong: " + uri.fileName);
}

const close_storage_contractID = "@aasii.org/abstract/test/storage-close;1";
const close_storage_name = "aaVCCloseStorageTest";
const close_storage_CID = Components.ID("{dee507e5-5e1c-4550-9cf0-83b72215f09b}");

function close_storage_test(runner) 
{
  var doc = runner.testWindow.document;
  var web = doc.getElementById("content").docShell
    .QueryInterface(nsCI.nsIWebNavigation);

  var manager = Components.classes["@aasii.org/storage/manager;1"]
    .createInstance(nsCI.aaIManager);

  manager.tmpFileFromString("default.sqlite", true);

  var session = web.sessionHistory.QueryInterface(nsCI.aaISHistory);  
  if (!session)
    runner.addJSFailure("session not started");
  session.setFile(null);

  mFile.remove(true);
  if (mFile.exists())
    runner.addJSFailure("File exists yet");
}

