/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI                   = Components.interfaces;
const moduleName             = "aaTestDocs";
const moduleCID              = "{45608c56-9bab-4d9c-a034-ecaa194b35e9}";
const moduleContractID       = "@aasii.org/abstract/test/vc-docs;1";

Components.utils.import("resource:///modules/aaTestVC.jsm");
Components.utils.import("resource:///modules/aaUITestUtils.jsm");

/*
 * Module entry point
 * The NSGetModule function is the magic entry point that XPCOM uses to find
 * what XPCOM components this module provides
 */
function NSGetModule(comMgr, fileSpec)
{
  var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Components.interfaces.mozIJSSubScriptLoader);
  loader.loadSubScript("resource:///modules/nsTestFrame.jsm");

  var aaVCTestModule = JSTestModule();
  aaVCTestModule.init = ModuleInit;
  aaVCTestModule.init();
  return aaVCTestModule;
}

function ModuleInit()
{
  this._name = moduleName;
  this._CID = Components.ID(moduleCID);
  this._contractID = moduleContractID;

  /*  0 */
  this._add(about_page_CID, about_page_contractID,
      about_page_name, about_page_test, about_page_check);
}

/* Chart Page Test */
const about_page_contractID = "@aasii.org/abstract/test/about-page;1";
const about_page_name = "aaAboutPageTest";
const about_page_CID = Components.ID("{a2171bb7-e7f0-41cf-be2c-dfd10ef1f032}");

function about_page_test(runner)
{
  runner.doCommand("cmd_help_about");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function about_check(runner)
{ 
  var result = true;
  if (getFrame(runner).contentWindow.location.href !=
      "chrome://abstract/content/test.xhtml") {
    runner.addJSFailure(" [about] wrong xul document");
    return false;
  }

  return result;
}

function about_page_check(runner)
{
  if (! about_check(runner))
    runner.addJSFailure("[about page]");
}

