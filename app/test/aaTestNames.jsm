/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const aaTestNames = {
EntityTag : ["A Corp."
            ,"Smith, John"
            ,"Z Inc."
            ,"OK Bank."
            ,"X Corp."
            ,"Y Ltd."
            ],
            
EntityTagFillCid: [
  Components.ID("{B5DFC5FA-3C4A-4158-81E5-874D2D002C1C}"),
  Components.ID("{DD838937-D0E1-49fb-BFA2-B91F536D38D4}"),
  Components.ID("{AC2A0ADE-44F2-4502-89EB-AD9A51628CA1}"),
  Components.ID("{33F4C9DD-CB91-47cc-907A-740B638D19DE}"),
  Components.ID("{6C68F78B-F25E-4513-A17D-D72E1282FCCD}"),
  Components.ID("{B97C79A6-957B-4ec7-956E-BDF38B9E3E06}")
],

ResourceTag : ["USD"
              ,"steel, tn"
              ,"cast iron, tn"
              ,"EUR"
              ],

ResourceType : [true
               ,false
               ,false
               ,true
               ],

ResourceCode : [840
               ,0
               ,0
               ,978
               ],
ResourceFillCid : [
  Components.ID("{1BB11425-579D-4261-8279-F0072BC0C263}"),
  Components.ID("{E02CD796-3A6C-4253-B48C-2683046CA54C}"),
  Components.ID("{61D73788-F914-4b23-BF59-A63665E2735A}"),
  Components.ID("{A87F9F8E-2335-43ed-BCC8-5C7F5CBD0793}")
],

FlowTag : ["purchase 1"
          ,"metall storage"
          ],

FlowCid: [
  Components.ID("{FA084A04-E839-42a8-AAC3-64168935C07D}"),
  Components.ID("{B380D92C-06DC-4207-AEA2-7F0A10AB5BD8}")
]
};
