/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI                   = Components.interfaces

function jsdump(str)
{
    Components.classes['@mozilla.org/consoleservice;1']
      .getService(Components.interfaces.nsIConsoleService)
      .logStringMessage(str);
}

function objdump(obj)
{
  jsdump(obj);
  for (var prop in obj) {
    try {
      jsdump(prop + ": " + obj[prop]);
    } catch (e) {
      jsdump(prop + ": error: " + e.message);
    }
  }
}

function getFrame(runner)
{
  if (! runner)
    return null;
  return runner.testWindow.document.getElementById("content");
}

function getElement(runner, id)
{
  if (! id || ! runner)
    return null;
  var el = getFrame(runner).contentDocument.getElementById(id);
  if (! el)
    return null;
  if (el.wrappedJSObject)
    return el.wrappedJSObject;
  return el;
}

function getAnnoElement(runner, id, annoid)
{
  if (! annoid || ! id || ! runner)
    return null;
  var doc = getFrame(runner).contentDocument;
  var el = doc.getAnonymousElementByAttribute(doc.getElementById(id),
      "annoid", annoid);
  if (el && el.wrappedJSObject)
    return el.wrappedJSObject;
  return el;
}

function flow_check(runner)
{
  var details = getFrame(runner).contentWindow.wrappedJSObject.view;
  if (! details) {
    runner.addJSFailure("[flow page] detailView is null");
    return false;
  }
  var index = details.index;
  if (index == -1)
    return true;
  var result = true;
  var flow;
  try {
    flow = details._item.QueryInterface(nsCI.aaIFlow);
  } catch(e) {
    runner.addJSFailure("[flow page] detailView._item is null");
    return false;
  }
  var tree = details.tree;
  if (! tree) {
    runner.addJSFailure("[flow page] tree is null");
    return false;
  }
  if (tree.view.getCellText(index,tree.columns[0]) != flow.tag) {
    runner.addJSFailure("[flow page] tree.tag not synched for line " + index);
    result = false;
  }
  if (! flow.entity) {
    runner.addJSFailure("[flow page] flow.entity is null for line " + index);
    result = false;
  } else if (tree.view.getCellText(index,tree.columns[1]) != flow.entity.tag) {
    runner.addJSFailure("[flow page] tree.entity.tag not synched for line "
        + index);
    result = false;
  }

  var treeIndex = tree.view.selection.currentIndex;
  if (details.buffer ) {
    flow = details.buffer.QueryInterface(nsCI.aaIFlow);
    if (! flow) {
      runner.addJSFailure("[flow] detailView.buffer is not a flow");
      return false;
    }
  } else if (treeIndex == -1) {
    return result;
  }
  if (getElement(runner, "flow.tag").value != flow.tag) {
    runner.addJSFailure("[flow page] flow.tag not synched");
    result = false;
  }
  if ((flow.entity && getElement(runner,"entity.tag").value != flow.entity.tag)
      || (! flow.entity && getElement(runner,"entity.tag").value !=  "")) {
    runner.addJSFailure("[flow page] flow.entity.tag not synched");
    result = false;
  }
  if ((flow.giveResource && getElement(runner,"give.tag").value !=
        flow.giveResource.tag) || (! flow.giveResource &&
        getElement(runner,"give.tag").value !=  "")) {
    runner.addJSFailure("[flow page] flow.giveResource.tag not synched");
    result = false;
  }
  if ((flow.takeResource && getElement(runner,"take.tag").value !=
        flow.takeResource.tag) || (! flow.takeResource &&
        getElement(runner,"take.tag").value !=  "")) {
    runner.addJSFailure("[flow page] flow.takeResource.tag not synched");
    result = false;
  }
  var indirect = getFrame(runner).contentWindow.wrappedJSObject.gIndirectRate;
  if ((indirect ? (1 / flow.rate) : flow.rate)
      != getElement(runner,"flow.rate").value)
  {
    runner.addJSFailure("[flow page] flow.rate not synched");
    result = false;
  }
  return result;
}

const treeId = "page1.tree";
const flowTagId = "flow.tag";

/* submit/discard from resource page */
const resourceTagId = "resource.tag";
const resourceTypeId = "resource.isMoney";
const resourceCodeId = "resource.code";

var resourceType = null;
function loadResourceBundle()
{
  var heap = Components.classes["@mozilla.org/intl/stringbundle;1"]
    .createInstance(nsCI.nsIStringBundleService);
  var bundle = heap.createBundle(
      "chrome://abstract/locale/resource.properties");
  resourceType = {};
  resourceType.asset = bundle.GetStringFromName("resource.type.asset");
  resourceType.money = bundle.GetStringFromName("resource.type.money");
}

function chooseTypeSymbol(type)
{
  if (!resourceType)
    loadResourceBundle();

  if (type == nsCI.aaIResource.TYPE_MONEY)
      return resourceType.money;

  return resourceType.asset;
}

function resource_details_check(runner, page)
{
  var result = true;
  var view = page ? page : getFrame(runner).contentWindow.wrappedJSObject.view;
  var tree = view.tree;
  var row = view.tree.view.selection.currentIndex;

  var resource;
  if (row >= 0){
    resource = view.array.queryElementAt(row, nsCI.aaIResource);
    if (tree.view.getCellText(row, tree.columns[1]) != resource.tag) {
      result = false;
      runner.addJSFailure("-->resource tree.tag not synched");
    }
    if (!resourceType)
      loadResourceBundle();
    var typeSymbol = chooseTypeSymbol(resource.type);
    if (tree.view.getCellText(row, tree.columns[0]) != typeSymbol) {
      result = false;
      runner.addJSFailure("-->resource tree.type not synched:" +
          tree.view.getCellText(row, tree.columns[0]) + "!=" + typeSymbol);
    }
  }

  if (view.buffer)
    resource = view.buffer;

  if (!resource) {
    if ("" != view.tag.value) {
      result = false;
      runner.addJSFailure("-->resource tag not synched");
    }
    if (false != view.type.checked) {
      result = false;
      runner.addJSFailure("-->resource type not synched");
    }
    if (view.type.getAttribute("disabled") != "true")
      runner.addJSFailure("-->money checkbox should be disabled");
    if ( "" != view.code.value) {
      result = false;
      runner.addJSFailure("-->resource code not synched");
    }
    return result;
  }

  if (resource.tag != view.tag.value) {
    result = false;
    runner.addJSFailure("-->resource tag not synched");
  }

  var isMoney = (resource.type == nsCI.aaIResource.TYPE_MONEY);
  if (isMoney != view.type.checked) {
    dump(" " + resource.type + ":" + view.type.checked + " ");
    result = false;
    runner.addJSFailure("-->resource type not synched");
  }

  if ((row == -1 && view.type.getAttribute("disabled") == "true")
      || (row != -1 && view.type.getAttribute("disabled") != "true"))
  {
    result = false;
    runner.addJSFailure("-->money checkbox state not synched");
  }

  if ((isMoney && view.isMoney.getAttribute("collapsed") == "true")
      || (!isMoney && view.isMoney.getAttribute("collapsed") != "true"))
  {
    result = false;
    runner.addJSFailure("-->money code state not synched");
  }

  if ((isMoney ? resource.id : "") != view.code.value) {
    result = false;
    runner.addJSFailure("-->resource code not synched");
  }

  return result;
}

function resource_check(runner)
{
  var result = true;

  if (!getElement(runner, treeId)) {
    result = false;
    runner.addJSFailure("resource tree failed");
  }
  if (!getElement(runner, resourceTagId)) {
    result = false;
    runner.addJSFailure("resource tag failed");
  }
  if (!getElement(runner, resourceTypeId)) {
    result = false;
    runner.addJSFailure("resource type failed");
  }
  if (!getElement(runner, resourceCodeId)) {
    result = false;
    runner.addJSFailure("resource code failed");
  }
  if (!resource_details_check(runner)) {
    result = false;
    runner.addJSFailure("resource page not synched");
  }
  return result;
}

function resource_select_check(runner)
{
  if (!resource_check(runner))
    runner.addJSFailure("resource page state is wrong");

  if (getAnnoElement(runner, "header", "header.discard")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("resource.discard button should be visible");
  if (getAnnoElement(runner, "header", "header.submit")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("resource.submit button should be visible");
  if (getElement(runner, "cmd_page1_discard")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("resource_discard command should be enabled");
  if (getElement(runner, "cmd_page1_submit")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("resource_submit command should be enabled");
}

function resource_discard_test(runner)
{
  getElement(runner,"page1.tree").view.selection.select(1);
  runner.doCommand("cmd_page1_discard");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function resource_submit_test(runner)
{
  if (this.item) {
    var tree = getElement(runner, "page1.tree");
    var view = tree.view;
    for (var i = 0; i < tree.view.rowCount; ++ i) {
        if (this.item(view, tree, i)) {
            tree.view.selection.select(i);
        }
    }
  }
  runner.doCommand("cmd_page1_submit");
  runner.watchWindow = getFrame(runner).contentWindow;
}

/* submit/discard from entity page */
const entityTagId = "entity.tag";

function entity_details_check(tree, box)
{
  if (! tree || ! box)
    return null;
  var row = tree.view.selection.currentIndex;
  var text = box.value;
  if (row == -1 || ! tree.view.selection.isSelected(row)) {
    if (text == "")
      return true;
    else
      return false;
  } else {
    if (tree.view.getCellText(row,tree.columns[0]) == text)
      return true;
    else
      return false;
  }
}

function entity_check(runner)
{
  var tree = getElement(runner,treeId);
  var box = getElement(runner, entityTagId);

  if (! tree)
    runner.addJSFailure("entity page failed");
  if (! entity_details_check(tree, box))
    runner.addJSFailure("entity.tag failed");
  // XXX 403202 'Unflexing' the tree speeds up reflow significantly
  if (tree && tree.hasAttribute("flex"))
    tree.removeAttribute("flex");
}

function entity_select_check(runner)
{
  entity_check(runner);

  if (getAnnoElement(runner, "header", "header.discard")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("entity.discard button should be visible");
  if (getAnnoElement(runner, "header", "header.submit")
      .getAttribute("collapsed") == "true")
    runner.addJSFailure("entity.submit button should be visible");
  if (getElement(runner, "cmd_page1_discard")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("entity_discard command should be enabled");
  if (getElement(runner, "cmd_page1_submit")
      .getAttribute("disabled") == "true")
    runner.addJSFailure("entity_submit command should be enabled");
}

function entity_discard_test(runner)
{
  getElement(runner,treeId).view.selection.select(1);
  runner.doCommand("cmd_page1_discard");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function entity_submit_test(runner)
{
  if (this.item < 0)
    this.item = getElement(runner, "page1.tree").view.rowCount + this.item;
  getElement(runner,"page1.tree").view.selection.select(this.item);
  runner.doCommand("cmd_page1_submit");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function flow_entity_test(runner)
{
  runner.doCommand("cmd_flow_entity");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function flow_take_test(runner)
{
  runner.doCommand("cmd_flow_take");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function rule_check(runner)
{
  var result = true;

  var tree = getElement(runner,"page1.tree");
  if (!tree) {
    runner.addJSFailure("rule page: tree is null");
    result = false;
  }

  // XXX 403202 'Unflexing' the tree speeds up reflow significantly
  if (tree && tree.hasAttribute("flex"))
    tree.removeAttribute("flex");

  if (getAnnoElement(runner, "header", "header.discard")
      .getAttribute("collapsed") == "true")
  {
    runner.addJSFailure("rule page: discard button should be visible");
    result = false;
  }
  if (getAnnoElement(runner, "header", "header.submit")
      .getAttribute("collapsed") == "true")
  {
    runner.addJSFailure("rule page: submit button should be visible");
    result = false;
  }
  if (getElement(runner, "cmd_page1_discard")
      .getAttribute("disabled") == "true")
  {
    runner.addJSFailure("rule page: discard command should be enabled");
    result = false;
  }
  if (getElement(runner, "cmd_page1_submit")
      .getAttribute("disabled") == "true")
  {
    runner.addJSFailure("rule page: submit command should be enabled");
    result = false;
  }

  return result;
}


EXPORTED_SYMBOLS = [
  "jsdump",
  "objdump",
  "getFrame",
  "getElement",
  "getAnnoElement",
  "session_buffer_check",
  "flow_check",
  "resourceTagId",
  "resource_details_check",
  "resource_check",
  "resource_select_check",
  "resource_discard_test",
  "resource_submit_test",
  "resourceTypeId",
  "chooseTypeSymbol",
  "resource_init_page",
  "entityTagId",
  "entity_details_check",
  "entity_check",
  "entity_select_check",
  "entity_discard_test",
  "entity_submit_test",
  "flow_entity_test",
  "flow_take_test",
  "rule_check",
  ""];

