/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_TRANSPORT_UTILS_H
#define AA_TRANSPORT_UTILS_H

#include "nsIPipe.h"
#include "nsIAsyncOutputStream.h"
#include "nsIAsyncInputStream.h"

#include "nsComponentManagerUtils.h"

inline nsresult
AA_CreatePipe(nsIInputStream **pipeIn,
              nsIOutputStream **pipeOut,
              PRBool nonBlockingIn = PR_TRUE)
{
  nsresult rv;

  nsCOMPtr<nsIAsyncInputStream> in;
  nsCOMPtr<nsIAsyncOutputStream> out;
  nsCOMPtr<nsIPipe> pipe
    = do_CreateInstance("@mozilla.org/pipe;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = pipe->Init(nonBlockingIn, PR_FALSE, 0, 0, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = pipe->GetInputStream(getter_AddRefs(in));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = pipe->GetOutputStream(getter_AddRefs(out));
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*pipeIn = in);
  NS_IF_ADDREF(*pipeOut = out);
  return NS_OK;
}

#endif //AA_TRANSPORT_UTILS_H

