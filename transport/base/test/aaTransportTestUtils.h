/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_TRANSPORT_TEST_UTILS_H
#define AA_TRANSPORT_TEST_UTILS_H

#include "nsIFile.h"

#include "aaIManager.h"

#include "nsComponentManagerUtils.h"
#include "nsNetUtil.h"
#include "nsStringAPI.h"

inline nsresult
AA_NewTempFile(nsIFile **aResult, const nsACString& aName,
    PRBool aReplace = PR_FALSE)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aResult);

  nsCOMPtr<aaIManager> manager = 
    do_CreateInstance(AA_MANAGER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> file;
  rv = manager->TmpFileFromCString(aName, aReplace,
      getter_AddRefs(file));
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*aResult = file);
  return NS_OK;
}

inline nsresult
AA_NewFileURIWithSql(nsIURI **aResult, nsIFile *aFile, const nsACString& sql)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aResult);
  NS_ENSURE_ARG_POINTER(aFile);

  nsCOMPtr<nsIURI> fileURI;
  rv = NS_NewFileURI(getter_AddRefs(fileURI), aFile);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIURI> dbURI;
  nsCAutoString sqlQuery("?sql=");
  sqlQuery.Append(sql);
  rv = NS_NewURI(getter_AddRefs(dbURI), sqlQuery, nsnull, fileURI);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*aResult = dbURI);
  return NS_OK;
}

#endif //AA_TRANSPORT_TEST_UTILS_H

