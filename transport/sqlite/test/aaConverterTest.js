/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=javascript: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

const nsCI                   = Components.interfaces;
const moduleName             = "aaConverterTest";
const moduleCID              = "{30e4bf20-504a-44fd-9473-3dc0294f4827}";
const moduleContractID       = "@aasii.org/transport/sqlite/test/converter;1";

Components.utils.import("resource:///modules/aaTestVC.jsm");

/*
 * Module entry point
 * The NSGetModule function is the magic entry point that XPCOM uses to find
 * what XPCOM components this module provides
 */
function NSGetModule(comMgr, fileSpec)
{
  var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"]
    .getService(Components.interfaces.mozIJSSubScriptLoader);
  loader.loadSubScript("resource:///modules/nsTestFrame.jsm");

  var aaVCTestModule = JSTestModule();
  aaVCTestModule.init = ModuleInit;
  aaVCTestModule.init();
  return aaVCTestModule;
}

function ModuleInit()
{
  this._name = moduleName;
  this._CID = Components.ID(moduleCID);
  this._contractID = moduleContractID;

  this._add(select_sqlite_table_CID, select_sqlite_table_contractID,
      select_sqlite_table_name, select_sqlite_table_test,
      select_sqlite_table_check);
}

function doLoadURI(runner, uri)
{
  runner.testWindow.document.getElementById("content").docShell
    .QueryInterface(nsCI.nsIWebNavigation)
    .loadURI(uri, nsCI.nsIWebNavigation.LOAD_FLAGS_NONE, null, null, null);
}

const select_sqlite_table_contractID =
  "@aasii.org/transport/sqlite/test/converter/select-sqlite-table;1";
const select_sqlite_table_name = "aaConverterTestSelectSqliteTable";
const select_sqlite_table_CID =
  Components.ID("{fca3d21e-3e35-40d4-a086-1ff9bd44b72e}");

function select_sqlite_table_test(runner)
{
  doLoadURI(runner, "sqlite:///tmp/transport.sqlite?sql" +
      "=SELECT id,tag FROM test");
  runner.watchWindow = getFrame(runner).contentWindow;
}

function firstChildIsResult(elem) {
  if (elem.firstChild.nodeValue.trim() == "1" ||
      elem.firstChild.nodeValue.trim() == "hello") {
    return true;
  }
  return false;
}

function select_sqlite_table_check(runner)
{
  var doc = runner.testWindow.document;
  var web = doc.getElementById("content").docShell
    .QueryInterface(nsCI.nsIWebNavigation);
  var result = false;
  // check
  var td = web.document.documentElement.getElementsByTagName("td");
  if (td.length == 2) {
    if (firstChildIsResult(td[0]) ||
        firstChildIsResult(td[1])) {
      result = true;
    }
  }
  if (!result) {
    runner.addJSFailure("aaConverterTest - Not correct select sqlite data");
  }
}

