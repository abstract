/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLITELOADENTITYTEST_H
#define AASQLITELOADENTITYTEST_H

#define AA_SQLITE_LOAD_ENTITY_CID \
{0xabc68f07, 0x93bc, 0x488a, {0x9f, 0xfe, 0xba, 0x3d, 0xa7, 0x97, 0x97, 0xa4}}
#define AA_SQLITE_LOAD_ENTITY_CONTRACT \
"@aasii.org/transport/sqlite/test/load-entity;1"

#define AA_SQLITE_LOAD_ENTITY_QUERY "\
SELECT id FROM entity"

#define AA_SQLITE_LOAD_ENTITY_RESULT "\
<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\
<result>\n\
  <row number=\"0\">\n\
    <id>1</id>\n\
  </row>\n\
  <row number=\"1\">\n\
    <id>2</id>\n\
  </row>\n\
  <row number=\"2\">\n\
    <id>3</id>\n\
  </row>\n\
  <row number=\"3\">\n\
    <id>4</id>\n\
  </row>\n\
  <row number=\"4\">\n\
    <id>5</id>\n\
  </row>\n\
</result>\n"

#endif /* AASQLITELOADENTITYTEST_H */
