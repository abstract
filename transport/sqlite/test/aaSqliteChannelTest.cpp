/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsDirectoryServiceDefs.h"
#include "nsIProperties.h"
#include "nsIFile.h"
#include "nsStringAPI.h"
#include "nsIStreamListener.h"
#include "nsNetUtil.h"

/* Unfrozen API */
#include "nsAppDirectoryServiceDefs.h"
#include "nsIBinaryInputStream.h"

/* Project includes */
#include "nsTestUtils.h"
#include "aaTransportTestUtils.h"
#include "nsITestRunner.h"

#include "aaSqliteChannel.h"

#include "aaSqliteChannelTest.h"


class aaTestObserver : public nsIStreamListener
{
public:
  aaTestObserver(nsITest *parent, const char *result)
    :mParent(parent), mResult(result), mState(eNone), mOffset(0) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIREQUESTOBSERVER
  NS_DECL_NSISTREAMLISTENER

private:
  ~aaTestObserver() {}
private:
  nsITest *mParent;
  enum {
    eNone = 0
    ,eStart
    ,eData
    ,eStop
  };

  const char *mResult;
  PRUint32 mState;
  PRUint32 mOffset;
  nsCAutoString mBuffer;
};

NS_IMPL_ISUPPORTS2(aaTestObserver,
                   nsIRequestObserver,
                   nsIStreamListener)

NS_IMETHODIMP
aaTestObserver::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  mState = eStart;
  return NS_OK;
}

NS_IMETHODIMP
aaTestObserver::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext,
    nsresult aStatusCode)
{
  nsresult rv;
  nsCOMPtr<nsITestRunner> cxxUnitTestRunner
    = do_GetService(NS_TESTRUNNER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  (void) cxxUnitTestRunner->MarkTestEnd(mParent);

  NS_TEST_ASSERT_MSG(mState == eData, "sqlite channel: wrong state");
  mState = eStop;
  NS_TEST_ASSERT_MSG(mBuffer.EqualsLiteral(mResult),
      (mBuffer.Insert("sqlite channel: wrong query result:\n",0),
       mBuffer.AppendLiteral(mResult),
       mBuffer.get()));

  return NS_OK;
}

NS_IMETHODIMP
aaTestObserver::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext,
    nsIInputStream *aInputStream, PRUint32 aOffset, PRUint32 aCount)
{
  nsresult rv;
  nsCOMPtr<nsITestRunner> cxxUnitTestRunner
    = do_GetService(NS_TESTRUNNER_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(mState == eData || mState == eStart,
      "sqlite channel: wrong state");
  NS_TEST_ASSERT_MSG(mOffset == aOffset, "sqlite channel: wrong offset");

  nsCAutoString buffer;
  PRUint32 readCount;
  mState = eData;
  rv = aInputStream->Read(buffer.BeginWriting(aCount), aCount, &readCount);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  buffer.EndWriting();
  
  mBuffer += buffer;
  mOffset += aCount;

  return NS_OK;
}

/*
 * *** aaSqliteChannelTest implementation ***
 */

aaSqliteChannelTest::aaSqliteChannelTest(const char *query, const char *result)
  :mQuery(query),
  mResult(result)
{
}

NS_IMPL_ISUPPORTS1(aaSqliteChannelTest,
                   nsITest)

NS_IMETHODIMP
aaSqliteChannelTest::Test(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsCOMPtr<nsIFile> file;
  rv = AA_NewTempFile(getter_AddRefs(file), NS_LITERAL_CSTRING("storage.sqlite"));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), file,
      nsDependentCString(mQuery));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsRefPtr<aaSqliteChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> iChannel;
  rv = channel->QueryInterface(NS_GET_IID(nsIChannel),
      getter_AddRefs(iChannel));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsRefPtr<aaTestObserver> observer = new aaTestObserver(this, mResult);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aTestRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aTestRunner->MarkTestEnd(this);
  }
  return NS_OK;
}
