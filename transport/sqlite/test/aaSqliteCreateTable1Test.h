/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLITECREATETABLE1TEST_H
#define AASQLITECREATETABLE1TEST_H

#define AA_SQLITE_CREATE_TABLE1_CID \
{0x655e2b3e, 0xd1dc, 0x4377, {0x86, 0xca, 0x45, 0x29, 0xf3, 0x41, 0xf2, 0xb9}}
#define AA_SQLITE_CREATE_TABLE1_CONTRACT \
"@aasii.org/transport/sqlite/test/create-table;1"

#define AA_SQLITE_CREATE_TABLE1_QUERY "\
CREATE TABLE t1(id INTEGER PRIMARY KEY AUTOINCREMENT, \
                name VARCHAR(35))"

#define AA_SQLITE_CREATE_TABLE1_RESULT "\
<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\
<result>\n\
  <message>Query OK: 0 row affected</message>\n\
</result>\n"

#endif /* AASQLITECREATETABLE1TEST_H */
