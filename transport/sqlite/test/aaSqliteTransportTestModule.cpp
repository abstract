/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaSqliteChannelTest.h"
#include "aaSqliteLoadEntityTest.h"
#include "aaSqliteCreateTable1Test.h"

#define AA_SQLITE_TRANSPORT_TEST_MODULE_CID \
{0x8ebe8443, 0xe59b, 0x4ffc, {0xa5, 0x40, 0x93, 0xf5, 0x26, 0xee, 0x1d, 0x5b}}
#define AA_SQLITE_TRANSPORT_TEST_MODULE_CONTRACT \
"@aasii.org/transport/sqlite/unit;1"

class aaSqliteTransportTestModule: public nsITest,
                                   public nsIUTF8StringEnumerator
{
public:
  aaSqliteTransportTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  ~aaSqliteTransportTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaSqliteTransportTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaSqliteTransportTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  AA_SQLITE_CREATE_TABLE1_CONTRACT
  ,"@aasii.org/transport/xml/sqlite;1"
  ,"@aasii.org/transport/sqlite/test/converter;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaSqliteTransportTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteTransportTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(subtests[mSubtest++]);
  return NS_OK;
}

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqliteTransportTestModule)

static NS_IMETHODIMP
aaSqliteLoadEntityTestConstructor(nsISupports *aOuter, REFNSIID aIID,
                                  void **aResult)
{
    nsresult rv;
    aaSqliteChannelTest *inst;

    *aResult = NULL;
    NS_ENSURE_TRUE(NULL == aOuter, NS_ERROR_NO_AGGREGATION);

    inst = new aaSqliteChannelTest(AA_SQLITE_LOAD_ENTITY_QUERY,
        AA_SQLITE_LOAD_ENTITY_RESULT);
    NS_ENSURE_TRUE(NULL != inst, NS_ERROR_OUT_OF_MEMORY);

    NS_ADDREF(inst);
    rv = inst->QueryInterface(aIID, aResult);
    NS_RELEASE(inst);

    return rv;
}

static NS_IMETHODIMP
aaSqliteCreateTable1TestConstructor(nsISupports *aOuter, REFNSIID aIID,
                                  void **aResult)
{
    nsresult rv;
    aaSqliteChannelTest *inst;

    *aResult = NULL;
    NS_ENSURE_TRUE(NULL == aOuter, NS_ERROR_NO_AGGREGATION);

    inst = new aaSqliteChannelTest(AA_SQLITE_CREATE_TABLE1_QUERY,
        AA_SQLITE_CREATE_TABLE1_RESULT);
    NS_ENSURE_TRUE(NULL != inst, NS_ERROR_OUT_OF_MEMORY);

    NS_ADDREF(inst);
    rv = inst->QueryInterface(aIID, aResult);
    NS_RELEASE(inst);

    return rv;
}

static const nsModuleComponentInfo kComponents[] =
{
  { "Sqlite Transport Module Test Container",
    AA_SQLITE_TRANSPORT_TEST_MODULE_CID,
    AA_SQLITE_TRANSPORT_TEST_MODULE_CONTRACT,
    aaSqliteTransportTestModuleConstructor}
  ,{"Sqlite Channel Load Entity Test",
    AA_SQLITE_LOAD_ENTITY_CID,
    AA_SQLITE_LOAD_ENTITY_CONTRACT,
    aaSqliteLoadEntityTestConstructor}
  ,{"Sqlite Channel Create Table Test",
    AA_SQLITE_CREATE_TABLE1_CID,
    AA_SQLITE_CREATE_TABLE1_CONTRACT,
    aaSqliteCreateTable1TestConstructor}
};
NS_IMPL_NSGETMODULE(aaSqliteTransportTest, kComponents)
