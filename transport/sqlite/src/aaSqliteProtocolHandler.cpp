/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008-2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "aaSqliteProtocolHandler.h"

#include "nsStringAPI.h"
#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsIURI.h"
#include "nsIStandardURL.h"
#include "nsIScriptSecurityManager.h"
#include "nsNetCID.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"

#include "aaSqlitePump.h"
#include "aaSqliteChannel.h"

NS_IMPL_ISUPPORTS1(aaSqliteProtocolHandler, nsIProtocolHandler)

/* readonly attribute ACString scheme; */
NS_IMETHODIMP
aaSqliteProtocolHandler::GetScheme(nsACString & aScheme)
{
  aScheme.AssignLiteral(AA_SQLITE_PROTOCOL);
  return NS_OK;
}

/* readonly attribute long defaultPort; */
NS_IMETHODIMP
aaSqliteProtocolHandler::GetDefaultPort(PRInt32 *aDefaultPort)
{
  NS_ENSURE_ARG_POINTER(aDefaultPort);
  *aDefaultPort = -1;
  return NS_OK;
}

/* readonly attribute unsigned long protocolFlags; */
NS_IMETHODIMP
aaSqliteProtocolHandler::GetProtocolFlags(PRUint32 *aProtocolFlags)
{
  NS_ENSURE_ARG_POINTER(aProtocolFlags);
  *aProtocolFlags = URI_STD | URI_IS_LOCAL_FILE;
  return NS_OK;
}

/* nsIURI newURI (in AUTF8String aSpec, in string aOriginCharset, in nsIURI aBaseURI); */
NS_IMETHODIMP
aaSqliteProtocolHandler::NewURI(const nsACString & aSpec,
  const char *aOriginCharset, nsIURI *aBaseURI, nsIURI **_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG(!aSpec.IsEmpty());
  NS_ENSURE_ARG_POINTER(_retval);
  nsresult rv = NS_OK;

  nsCOMPtr<nsIURI> uri = do_CreateInstance(NS_STANDARDURL_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIStandardURL> surl = do_QueryInterface(uri, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = surl->Init(nsIStandardURL::URLTYPE_STANDARD, -1, aSpec, aOriginCharset,
		  aBaseURI);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = surl->SetMutable(PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_IF_ADDREF(*_retval = uri);
  return NS_OK;
}

/* nsIChannel newChannel (in nsIURI aURI); */
NS_IMETHODIMP
aaSqliteProtocolHandler::NewChannel(nsIURI *aURI, nsIChannel **_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(aURI);
  NS_ENSURE_ARG_POINTER(_retval);

  nsresult rv = NS_OK;
  PRBool sqliteScheme = PR_FALSE;
  rv = aURI->SchemeIs(AA_SQLITE_PROTOCOL, &sqliteScheme);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(sqliteScheme, NS_ERROR_FAILURE);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(aURI);
  NS_ENSURE_TRUE(channel, NS_ERROR_OUT_OF_MEMORY);

  nsCOMPtr<nsIScriptSecurityManager> scrSecMngr =
    do_GetService(NS_SCRIPTSECURITYMANAGER_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIPrincipal> owner;
  rv = scrSecMngr->GetSystemPrincipal(getter_AddRefs(owner));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->SetOwner(owner);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*_retval = channel);

  return NS_OK;
}

/* boolean allowPort (in long port, in string scheme); */
NS_IMETHODIMP
aaSqliteProtocolHandler::AllowPort(PRInt32 port, const char *scheme,
  PRBool *_retval NS_OUTPARAM)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

