/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"
#include "nsCOMPtr.h"
#include "nsServiceManagerUtils.h"
#include "nsICategoryManager.h"

/* Unfrozen API */

/* Project includes */
#include "aaSqliteProtocolHandler.h"
#include "aaDBXMLtoXMLConverter.h"

#define DUMMY_CID \
{0x2f3a5eaf, 0x9af4, 0x4979, {0x8d, 0x34, 0xb9, 0x99, 0x84, 0x7a, 0xf4, 0x0d}}
#define DUMMY_CONTRACT "@aasii.org/dummy;1"

class Dummy: public nsISupports
{
public:
  Dummy() {}
  NS_DECL_ISUPPORTS
private:
  ~Dummy() {}
};

NS_IMPL_ISUPPORTS0(Dummy)

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(Dummy)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaSqliteProtocolHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaDBXMLtoXMLConverter)

static NS_METHOD
RegisterStreamConverters(nsIComponentManager *aCompMgr, nsIFile *aPath,
    const char *registryLocation, const char *componentType,
    const nsModuleComponentInfo *info)
{
  nsCOMPtr<nsICategoryManager> catmgr =
    do_GetService(NS_CATEGORYMANAGER_CONTRACTID);
  NS_ENSURE_STATE(catmgr);

  catmgr->AddCategoryEntry(NS_ISTREAMCONVERTER_KEY, DBXML_TO_XML, "", PR_TRUE,
      PR_TRUE, nsnull);
  return NS_OK;
}

static NS_METHOD
UnregisterStreamConverters(nsIComponentManager *aCompMgr, nsIFile *aPath,
    const char *registryLocation, const nsModuleComponentInfo *info) {
  nsCOMPtr<nsICategoryManager> catmgr =
    do_GetService(NS_CATEGORYMANAGER_CONTRACTID);
  NS_ENSURE_STATE(catmgr);

  catmgr->DeleteCategoryEntry(NS_ISTREAMCONVERTER_KEY, DBXML_TO_XML, PR_TRUE);
  return NS_OK;
}

static const nsModuleComponentInfo kComponents[] =
{
  { "Dummy Component",
    DUMMY_CID,
    DUMMY_CONTRACT,
    DummyConstructor},
  { AA_SQLITE_PROTOCOL_HANDLER_CLASS_NAME,
    AA_SQLITE_PROTOCOL_HANDLER_CID,
    AA_SQLITE_PROTOCOL_HANDLER_CONTRACTID,
    aaSqliteProtocolHandlerConstructor},
  { AA_DBXML_TO_XML_CONV_CLASS_NAME,
    AA_DBXML_TO_XML_CONV_CID,
    NS_ISTREAMCONVERTER_KEY DBXML_TO_XML,
    aaDBXMLtoXMLConverterConstructor,
    RegisterStreamConverters,
    UnregisterStreamConverters }
};
NS_IMPL_NSGETMODULE(aaModuleSqliteTransport, kComponents)

