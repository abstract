/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "aaDBXMLtoXMLConverter.h"

#include "nsIChannel.h"
#include "nsILoadGroup.h"
#include "nsIStringStream.h"

#include "nsMimeTypes.h"
#include "nsComponentManagerUtils.h"
#include "nsMemory.h"

NS_IMPL_ISUPPORTS3(aaDBXMLtoXMLConverter,
    nsIStreamConverter,
    nsIStreamListener,
    nsIRequestObserver)

aaDBXMLtoXMLConverter::aaDBXMLtoXMLConverter()
  : mListener(nsnull)
  , mHasHead(PR_FALSE)
  , mHasEnd(PR_FALSE)
{
}

aaDBXMLtoXMLConverter::~aaDBXMLtoXMLConverter()
{
}

NS_IMETHODIMP
aaDBXMLtoXMLConverter::Convert(nsIInputStream *aFromStream,
    const char *aFromType, const char *aToType, nsISupports *aCtxt,
    nsIInputStream **_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaDBXMLtoXMLConverter::AsyncConvertData(const char *aFromType,
    const char *aToType, nsIStreamListener *aListener, nsISupports *ctxt)
{
  NS_ENSURE_ARG_POINTER(aListener);
  mListener = aListener;
  return NS_OK; 
}

// nsIStreamListener method
/* This method handles asyncronous conversion of data. */
NS_IMETHODIMP
aaDBXMLtoXMLConverter::OnDataAvailable(nsIRequest* request, nsISupports *aContext, 
    nsIInputStream *aInStream, PRUint32 aSourceOffset, PRUint32 aCount)
{
  nsresult rv = NS_OK;

  NS_ENSURE_STATE(mListener);
  NS_ENSURE_ARG_POINTER(aInStream);

  if (mHasEnd)
    return mListener->OnDataAvailable(request, aContext, aInStream,
      aSourceOffset, aCount);

  PRUint32 avail = 0;
  rv = aInStream->Available(&avail);
  NS_ENSURE_SUCCESS(rv, rv);

  if (avail > aCount) avail = aCount;
  NS_ENSURE_TRUE(avail > 0, NS_ERROR_INVALID_ARG);

  char *buf = nsnull;
  nsCAutoString data;
  buf = (char*)nsMemory::Alloc(sizeof(char) * (avail + 1));
  NS_ENSURE_TRUE(buf, NS_ERROR_OUT_OF_MEMORY);

  PRUint32 readCount = 0;
  rv = aInStream->Read(buf, avail, &readCount);
  if (NS_SUCCEEDED(rv))
  {
    buf[readCount] = 0;
    data.Truncate();
    data.AssignLiteral(buf);
  }
  nsMemory::Free(buf);

  PRInt32 headPos = 0;
  if (!mHasHead)
    headPos = FindHead(data);
  if (mHasHead && !mHasEnd)
  {
    PRInt32 endPos = FindEnd(data, headPos);
    if (mHasEnd)
    {
      data.Insert("<?xml-stylesheet type=\"text/xsl\" \
            href=\"chrome://transport/content/respage.xsl\" ?>", endPos);
    }
  }

  nsCOMPtr<nsIStringInputStream> ss(
      do_CreateInstance("@mozilla.org/io/string-input-stream;1", &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = ss->ShareData(data.get(), data.Length());
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIInputStream> inStream(do_QueryInterface(ss, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  return mListener->OnDataAvailable(request, aContext, inStream,
      0, data.Length());
}

// nsIRequestObserver methods
/* These methods just pass through directly to the mListener */
NS_IMETHODIMP
aaDBXMLtoXMLConverter::OnStartRequest(nsIRequest* request, nsISupports *ctxt)
{
  nsresult rv = NS_OK;
  NS_ENSURE_STATE(mListener);
  NS_ENSURE_ARG_POINTER(request);

  nsCOMPtr<nsIChannel> channel =
    do_QueryInterface(request, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->SetContentType(NS_LITERAL_CSTRING(TEXT_XML));
  NS_ENSURE_SUCCESS(rv, rv);

  return mListener->OnStartRequest(request, ctxt);
}

NS_IMETHODIMP
aaDBXMLtoXMLConverter::OnStopRequest(nsIRequest* request, nsISupports *ctxt, 
    nsresult aStatus)
{
  NS_ENSURE_STATE(mListener);
  return mListener->OnStopRequest(request, ctxt, aStatus);
}

PRInt32
aaDBXMLtoXMLConverter::FindHead(const nsACString& data)
{
  if (!mBuffer.IsEmpty())
  {
    PRInt32 lossLength = 6 - mBuffer.Length();
    if ((PRInt32) data.Length() < lossLength)
    {
      mBuffer.Append(data);
      return data.Length();
    }
    mBuffer.Append(Substring(data, 0, lossLength));
    if (mBuffer.EqualsLiteral("<?xml "))
    {
      mHasHead = PR_TRUE;
      mBuffer.Truncate();
      return lossLength;
    }
    else
      mBuffer.Truncate();
  }

  PRInt32 anglePos = -1;
  while ((anglePos = data.FindChar('<', (PRUint32) (anglePos + 1))) > -1)
  {
    if (((PRInt32) data.Length()) - 6 >= anglePos)
    {
      if (Substring(data, anglePos, 6).EqualsLiteral("<?xml "))
      {
        mHasHead = PR_TRUE;
        return anglePos + 6;
      }
    }
    else
    {
      mBuffer.Assign(Substring(data, anglePos));
    }
  }
  return data.Length();
}

PRInt32
aaDBXMLtoXMLConverter::FindEnd(const nsACString& data, PRInt32 aStartPos)
{
  if (!mBuffer.IsEmpty())
  {
    mBuffer.Append(Substring(data, 0, 1));
    if (mBuffer.EqualsLiteral("?>"))
    {
      mHasEnd = PR_TRUE;
      mBuffer.Truncate();
      return 1;
    }
    else
      mBuffer.Truncate();
  }

  PRInt32 anglePos = aStartPos - 1;
  while ((anglePos = data.FindChar('?', (PRUint32) (anglePos + 1))) > -1)
  {
    if (((PRInt32) data.Length()) - 2 >= anglePos)
    {
      if (Substring(data, anglePos, 2).EqualsLiteral("?>"))
      {
        mHasEnd = PR_TRUE;
        return anglePos + 2;
      }
    }
    else
    {
      mBuffer.Assign(Substring(data, anglePos));
    }
  }
  return data.Length();
}

