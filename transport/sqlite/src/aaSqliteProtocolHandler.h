/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008-2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_SQLITE_PROTOCOL_HANDLER_H
#define AA_SQLITE_PROTOCOL_HANDLER_H

#include "nsIProtocolHandler.h"

#define AA_SQLITE_PROTOCOL \
  "sqlite"

#define AA_SQLITE_PROTOCOL_HANDLER_CID \
{0xba95b99, 0xc340, 0x424c, {0x9f, 0x3, 0x97, 0xd3, 0xd4, 0xa8, 0xf0, 0xa3}}

#define AA_SQLITE_PROTOCOL_HANDLER_CONTRACTID \
  NS_NETWORK_PROTOCOL_CONTRACTID_PREFIX AA_SQLITE_PROTOCOL

#define AA_SQLITE_PROTOCOL_HANDLER_CLASS_NAME \
  "Sqlite Protocol Handler"

class aaSqliteProtocolHandler : public nsIProtocolHandler
{
public:
  aaSqliteProtocolHandler() {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIPROTOCOLHANDLER
private:
  ~aaSqliteProtocolHandler() {}
};

#endif //AA_SQLITE_PROTOCOL_HANDLER_H

