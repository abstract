/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "sqlite3.h"
#include "nsCOMPtr.h"

/* Unfrozen API */

/* Project includes */

#include "aaSqliteStmt.h"

DBWrapper::~DBWrapper()
{
  if (mStmt)
    sqlite3_finalize(mStmt);

  if (mDB)
    sqlite3_close(mDB);
}

nsresult
DBWrapper::Connect(const char *file)
{
  int rc;
	rc = sqlite3_open(file, &mDB);
  if (rc)
    return NS_ERROR_FAILURE;
  return NS_OK;
}

nsresult
DBWrapper::Create(const char *query)
{
  int rc;
  if (!mDB)
    return NS_ERROR_NOT_INITIALIZED;
  rc = sqlite3_prepare_v2(mDB, query, strlen(query), &mStmt, NULL);
  if (rc)
    return NS_ERROR_FAILURE;
  return NS_OK;
}

nsresult
DBWrapper::Changes(PRInt32 *aChanges)
{
  NS_ENSURE_ARG_POINTER(aChanges);
  if (!mDB)
    return NS_ERROR_NOT_INITIALIZED;
  *aChanges = sqlite3_changes(mDB);
  return NS_OK;
}

nsresult
DBWrapper::ErrorCode(PRInt32 *aCode)
{
  NS_ENSURE_ARG_POINTER(aCode);
  if (!mDB)
    return NS_ERROR_NOT_INITIALIZED;
  *aCode = sqlite3_errcode(mDB);
  return NS_OK;
}

nsresult
DBWrapper::ErrorMsg(const char **aMsg)
{
  NS_ENSURE_ARG_POINTER(aMsg);
  if (!mDB)
    return NS_ERROR_NOT_INITIALIZED;
  *aMsg = sqlite3_errmsg(mDB);
  return NS_OK;
}

