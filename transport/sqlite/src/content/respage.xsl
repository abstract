<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/result">
    <html>
      <head>
      </head>
      <body>
        <xsl:choose>
	  <xsl:when test="child::row[1] and child::row[1]/*">
            <table id="res_table" border="1">
              <tr>
                <xsl:for-each select="child::row[1]/*">
                  <th>
                    <xsl:value-of select="name(.)"/>
                  </th>
                </xsl:for-each>
              </tr>
              <xsl:for-each select="row">
                <tr>
                  <xsl:for-each select="child::*">
                    <td>
                      <xsl:value-of select="text()" />
                    </td>
                  </xsl:for-each>
                </tr>
              </xsl:for-each>
            </table>
          </xsl:when>
        </xsl:choose>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>

