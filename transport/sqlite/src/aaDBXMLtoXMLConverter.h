/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_DBXML_TO_XML_CONV
#define AA_DBXML_TO_XML_CONV

#include "nsIStreamConverter.h"
#include "nsCOMPtr.h"
#include "nsStringAPI.h"

#define AA_DBXML_TO_XML_CONV_CLASS_NAME \
    "aaDBXMLToXMLConverter"
#define AA_DBXML_TO_XML_CONV_CID \
  { 0xc5912b65, 0x7f6c, 0x4d0b,  \
    { 0x84, 0xbf, 0x10, 0x59, 0xc9, 0xc2, 0xaf, 0x36 } }
#define DBXML_TO_XML "?from=text/dbxml&to=*/*"

class aaDBXMLtoXMLConverter : public nsIStreamConverter {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_NSIREQUESTOBSERVER
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSISTREAMCONVERTER

  aaDBXMLtoXMLConverter();
private:
  ~aaDBXMLtoXMLConverter();
  friend class aaChannelTest;

  PRInt32 FindHead(const nsACString& data);
  PRInt32 FindEnd(const nsACString& data, PRInt32 aStartPos);

  nsCOMPtr<nsIStreamListener> mListener;
  nsCAutoString mBuffer;
  PRPackedBool mHasHead;
  PRPackedBool mHasEnd;
};

#endif //AA_DBXML_TO_XML_CONV

