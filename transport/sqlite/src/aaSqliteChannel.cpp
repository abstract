/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsNetCID.h"
#include "nsIStreamListener.h"
#include "nsIURL.h"
#include "nsIFileURL.h"
#include "nsINetUtil.h"
#include "nsComponentManagerUtils.h"
#include "nsNetUtil.h"

/* Unfrozen API */
#include "nsThreadUtils.h"
#include "nsIRunnable.h"
#include "nsIThread.h"

/* Project includes */

#include "aaSqliteChannel.h"
#include "aaSqlitePump.h"

aaSqliteChannel::aaSqliteChannel(nsIURI *aURI)
  : mURI(aURI)
  , mContentType("text/dbxml")
  , mStarted(PR_FALSE)
  , mHadStart(PR_FALSE)
  , mLoadFlags(0)
  , mStatus(NS_OK)
  , mCancelled(PR_FALSE)
{}

NS_IMPL_THREADSAFE_ISUPPORTS4(aaSqliteChannel,
                              nsIRequest,
                              nsIChannel,
                              nsIStreamListener,
                              nsIRequestObserver)

/* nsIRequest */
NS_IMETHODIMP
aaSqliteChannel::GetName(nsACString & aName)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::IsPending(PRBool *_retval NS_OUTPARAM)
{
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = !!mObserver;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetStatus(nsresult *aStatus)
{
  NS_ENSURE_ARG_POINTER(aStatus);
  *aStatus = mStatus;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::Cancel(nsresult aStatus)
{
  NS_ENSURE_STATE(mObserver);
  if (NS_UNLIKELY(mCancelled)) return NS_ERROR_UNEXPECTED;

  mStatus = aStatus;
  mCancelled = PR_TRUE;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::Suspend()
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::Resume()
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::GetLoadGroup(nsILoadGroup * *aLoadGroup)
{
  NS_ENSURE_ARG_POINTER(aLoadGroup);
  NS_IF_ADDREF(*aLoadGroup = mLoadGroup);
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetLoadGroup(nsILoadGroup * aLoadGroup)
{
  mLoadGroup = aLoadGroup;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetLoadFlags(nsLoadFlags *aLoadFlags)
{
  NS_ENSURE_ARG_POINTER(aLoadFlags);
  *aLoadFlags = mLoadFlags;
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetLoadFlags(nsLoadFlags aLoadFlags)
{
  mLoadFlags = aLoadFlags;
  return NS_OK;
}

/* nsIChannel */
NS_IMETHODIMP
aaSqliteChannel::GetOriginalURI(nsIURI * *aOriginalURI)
{
  NS_ENSURE_ARG_POINTER(aOriginalURI);
  NS_IF_ADDREF(*aOriginalURI = mURI);
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetOriginalURI(nsIURI * aOriginalURI)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::GetURI(nsIURI * *aURI)
{
  NS_ENSURE_ARG_POINTER(aURI);
  NS_IF_ADDREF(*aURI = mURI);
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetOwner(nsISupports * *aOwner)
{
  NS_ENSURE_ARG_POINTER(aOwner);
  NS_IF_ADDREF(*aOwner = mOwner);
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetOwner(nsISupports * aOwner)
{
  mOwner = aOwner;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetNotificationCallbacks(nsIInterfaceRequestor *
    *aNotificationCallbacks)
{
  NS_ENSURE_ARG_POINTER(aNotificationCallbacks);
  NS_IF_ADDREF(*aNotificationCallbacks = mCallbacks);
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetNotificationCallbacks(nsIInterfaceRequestor *
    aNotificationCallbacks)
{
  mCallbacks = aNotificationCallbacks;
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetSecurityInfo(nsISupports * *aSecurityInfo)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::GetContentType(nsACString & aContentType)
{
  aContentType.Assign(mContentType);
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetContentType(const nsACString & aContentType)
{
  nsresult rv = NS_OK;
  if(NS_UNLIKELY(mStarted && !mHadStart))
    return NS_ERROR_UNEXPECTED;

  if (mHadStart)
  {
    nsCAutoString contentCharset;
    rv = NS_ParseContentType(aContentType, mContentType, contentCharset);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::GetContentCharset(nsACString & aContentCharset)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaSqliteChannel::SetContentCharset(const nsACString & aContentCharset)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::GetContentLength(PRInt32 *aContentLength)
{
  NS_ENSURE_ARG_POINTER(aContentLength);
  *aContentLength = -1;
  return NS_OK;
}
NS_IMETHODIMP
aaSqliteChannel::SetContentLength(PRInt32 aContentLength)
{
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::Open(nsIInputStream **_retval NS_OUTPARAM)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaSqliteChannel::AsyncOpen(nsIStreamListener *aListener, nsISupports *aContext)
{
  NS_ENSURE_ARG_POINTER(aListener);
  NS_ENSURE_STATE(mURI);
  nsresult rv;

  if (NS_UNLIKELY(mStarted)) return NS_ERROR_ALREADY_OPENED;

  nsCOMPtr<nsIEventTarget> worker =
    do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  mObserver = aListener;

  nsCOMPtr<nsIURL> url = do_QueryInterface(mURI, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString query;
  rv = url->GetQuery(query);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_ENSURE_TRUE(StringBeginsWith(query, NS_LITERAL_CSTRING("sql=")),
      NS_ERROR_INVALID_ARG);
  const nsACString& sqlQuery = Substring(query, 4);

  nsCOMPtr<nsINetUtil> netUtil = do_CreateInstance(NS_NETUTIL_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString sql;
  rv = netUtil->UnescapeString(sqlQuery, nsINetUtil::ESCAPE_XALPHAS, sql);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIFile> db;
  PRBool schemeIsSqlite = PR_FALSE;
  rv = mURI->SchemeIs("sqlite", &schemeIsSqlite);
  NS_ENSURE_SUCCESS(rv, rv);
  if (schemeIsSqlite)
  {
    nsCOMPtr<nsIURL> url = do_QueryInterface(mURI, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCAutoString filePath;
    rv = url->GetFilePath(filePath);
    NS_ENSURE_SUCCESS(rv, rv);

    nsCOMPtr<nsILocalFile> localFile =
      do_CreateInstance("@mozilla.org/file/local;1", &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = localFile->InitWithNativePath(filePath);
    NS_ENSURE_SUCCESS(rv, rv);

    db = do_QueryInterface(localFile, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else
  {
    nsCOMPtr<nsIFileURL> fileURL = do_QueryInterface(mURI, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = fileURL->GetFile(getter_AddRefs(db));
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsRefPtr<aaSqlitePump> pump = new aaSqlitePump(db, sql.get(), this);
  NS_ENSURE_TRUE(pump, NS_ERROR_OUT_OF_MEMORY);

  rv = worker->Dispatch(pump, NS_DISPATCH_NORMAL);
  NS_ENSURE_SUCCESS(rv, rv);

  mStarted = PR_TRUE;

  if (mLoadGroup) mLoadGroup->AddRequest(this, nsnull);
  return NS_OK;
}

NS_IMETHODIMP
aaSqliteChannel::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  NS_ENSURE_STATE(mObserver && !mHadStart);
  mHadStart = PR_TRUE;

  nsresult rv = NS_OK;
  rv = mObserver->OnStartRequest(this, aContext);
  if (NS_FAILED(rv) && !IsCancelled())
  {
    Cancel(rv);
  }
  return rv;
}

NS_IMETHODIMP
aaSqliteChannel::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext,
    nsresult aStatusCode)
{
  NS_ENSURE_STATE(mObserver && mHadStart);
  nsresult rv = NS_OK;

  if (NS_SUCCEEDED(mStatus))
    mStatus = aStatusCode;
  rv = mObserver->OnStopRequest(this, aContext, mStatus);
  mObserver = nsnull;

  if (mLoadGroup) mLoadGroup->RemoveRequest(this, nsnull, mStatus);
  mCallbacks = nsnull;
  return rv;
}

NS_IMETHODIMP
aaSqliteChannel::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext,
    nsIInputStream *aInputStream, PRUint32 aOffset, PRUint32 aCount)
{
  NS_ENSURE_STATE(mObserver && mHadStart);

  nsresult rv = NS_OK;
  rv = mObserver->OnDataAvailable(this, aContext, aInputStream,
      aOffset, aCount);
  if (NS_FAILED(rv) && !IsCancelled())
  {
    Cancel(rv);
  }
  return rv;
}

