/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLITESTMT_H
#define AASQLITESTMT_H

typedef struct sqlite3 sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;

class DBWrapper
{
public:
  DBWrapper() {}
  ~DBWrapper();

  nsresult Connect(const char *file);
  nsresult Create(const char *query);
  inline sqlite3_stmt* GetStmt() const { return mStmt; }
  nsresult Changes(PRInt32 *aChanges);
  nsresult ErrorCode(PRInt32 *aCode);
  nsresult ErrorMsg(const char **aMsg);
private:
	sqlite3 *mDB;
	sqlite3_stmt *mStmt;
};

#endif /* AASQLITESTMT_H */
