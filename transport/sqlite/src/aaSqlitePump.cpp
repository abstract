/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "sqlite3.h"
#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsNetCID.h"
#include "nsIAsyncInputStream.h"
#include "nsIAsyncOutputStream.h"
#include "nsIPipe.h"
#include "nsIStreamListener.h"

/* Unfrozen API */
#include "nsThreadUtils.h"
#include "nsIRunnable.h"

#include "aaSqlitePump.h"
#include "aaSqliteChannel.h"
#include "aaTransportXML.h"
#include "aaTransportUtils.h"

class aaStartEvent : public nsIRunnable
{
public:
  aaStartEvent(aaSqliteChannel *channel)
    :mChannel(channel) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsRefPtr<aaSqliteChannel> mChannel;
private:
  ~aaStartEvent() {}
};

NS_IMPL_THREADSAFE_ISUPPORTS1(aaStartEvent,
                              nsIRunnable)

NS_IMETHODIMP
aaStartEvent::Run()
{
  NS_ENSURE_STATE(mChannel);
  return mChannel->OnStartRequest(nsnull, nsnull);
}

class aaDataEvent : public nsIRunnable
{
public:
  aaDataEvent(aaSqliteChannel *channel, nsIInputStream *stream, PRUint32
      offset, PRUint32 count)
    :mChannel(channel), mStream(stream), mOffset(offset), mCount(count) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsRefPtr<aaSqliteChannel> mChannel;
  nsCOMPtr<nsIInputStream> mStream;
  PRUint32 mOffset;
  PRUint32 mCount;
private:
  ~aaDataEvent() {}
};

NS_IMPL_THREADSAFE_ISUPPORTS1(aaDataEvent,
                              nsIRunnable)

NS_IMETHODIMP
aaDataEvent::Run()
{
  NS_ENSURE_STATE(mChannel);
  if (mChannel->IsCancelled())
    return NS_OK;

  return mChannel->OnDataAvailable(nsnull, nsnull, mStream,
      mOffset, mCount);
}

class aaStopEvent : public nsIRunnable
{
public:
  aaStopEvent(aaSqliteChannel *channel, nsresult status)
    :mChannel(channel), mStatus(status) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsRefPtr<aaSqliteChannel> mChannel;
  nsresult mStatus;
private:
  ~aaStopEvent() {}
};

NS_IMPL_THREADSAFE_ISUPPORTS1(aaStopEvent,
                              nsIRunnable)

NS_IMETHODIMP
aaStopEvent::Run()
{
  NS_ENSURE_STATE(mChannel);
  return mChannel->OnStopRequest(nsnull, nsnull, mStatus);
}

aaSqlitePump::aaSqlitePump(nsIFile *dbFile, const char *query, aaSqliteChannel
    *channel)
  :mDBFile(dbFile), mQuery(query), mChannel(channel), mOffset(0), mCount(0),
  mRowIndex(0)
{
  mTarget = do_GetCurrentThread();
}

NS_IMPL_THREADSAFE_ISUPPORTS1(aaSqlitePump,
                              nsIRunnable)

NS_IMETHODIMP
aaSqlitePump::Run()
{
  nsresult rv;

  {
    nsRefPtr<aaStartEvent> event = new aaStartEvent(mChannel);
    mTarget->Dispatch(event, NS_DISPATCH_NORMAL);
  }

  rv = start();

  PRInt32 code = 0;
  db.ErrorCode(&code);
  if (NS_FAILED(rv) && SQLITE_OK != code)
  {
    feed(kTransportXmlHeader);
    feedError();
    feed(kTransportXmlFooter);
  }

  if (NS_SUCCEEDED(rv))
    rv = read();

  {
    nsRefPtr<aaStopEvent> event = new aaStopEvent(mChannel, rv);
    rv = mTarget->Dispatch(event, NS_DISPATCH_NORMAL);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

nsresult
aaSqlitePump::write(const char *data)
{
  return writen(data, strlen(data));
}

nsresult
aaSqlitePump::writen(const char *data, PRUint32 length)
{
  NS_ENSURE_STATE(mSink);
  nsresult rv;

  PRUint32 writtenCount;
  rv = mSink->Write(data, length, &writtenCount);
  NS_ENSURE_SUCCESS(rv, rv);

  mCount += writtenCount;
  return NS_OK;
}

static const char kRaw[] = "<>&";
static const char* kEscaped[] =
{
  "&lt;"
  ,"&rt;"
  ,"&amp;"
};

nsresult
aaSqlitePump::writeAndEscape(const char *data)
{
  nsresult rv;

  PRUint32 i = 0;
  const char *last = data;

  while (NS_LIKELY(0 != last[i++]));
  {
    PRUint32 y = 0;
    for (; y < 3; y++)
    {
      if (NS_UNLIKELY(kRaw[y] == last[i - 1]))
      {
        if (i > 1) {
          rv = writen(last, i - 1);
          NS_ENSURE_SUCCESS(rv, rv);
        }
        rv = write(kEscaped[y]);
        NS_ENSURE_SUCCESS(rv, rv);

        last = &last[i];
        i = 0;
      }
    }
  }

  rv = writen(last, i - 1);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSqlitePump::notifyListener()
{
  NS_ENSURE_STATE(mTarget);
  nsresult rv;

  nsRefPtr<aaDataEvent> event = new aaDataEvent(mChannel, mFeeder, mOffset,
      mCount);
  NS_ENSURE_TRUE(event, NS_ERROR_OUT_OF_MEMORY);

  rv = mTarget->Dispatch(event, NS_DISPATCH_NORMAL);
  NS_ENSURE_SUCCESS(rv, rv);

  mOffset += mCount;
  mCount = 0;
  return NS_OK;
}

nsresult
aaSqlitePump::feed(const char *data)
{
  nsresult rv;

  rv = write(data);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = notifyListener();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSqlitePump::start()
{
  NS_ENSURE_STATE(mDBFile);
  nsresult rv;

  nsAutoString path;
  rv = mDBFile->GetPath(path);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = AA_CreatePipe(getter_AddRefs(mFeeder),
                  getter_AddRefs(mSink));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = db.Connect(NS_ConvertUTF16toUTF8(path).get());
  if (NS_SUCCEEDED(rv))
  {
    rv = db.Create(mQuery.get());
  }

  return rv;
}

nsresult
aaSqlitePump::read()
{
  nsresult rv;

  rv = feed(kTransportXmlHeader);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = feedResult();
  feed(kTransportXmlFooter);

  return rv;
}

nsresult
aaSqlitePump::feedResult()
{
  nsresult rv;
  int rc;

  PRBool isItemCollected = PR_FALSE;
  do {
    rc = sqlite3_step(db.GetStmt());
    if (rc != SQLITE_ROW) {
      if (rc == SQLITE_DONE || rc == SQLITE_OK)
        break;
      return feedError();
    }

    rv = feedItem();
    NS_ENSURE_SUCCESS(rv, rv);

    mRowIndex++;
    isItemCollected = PR_TRUE;
  } while (PR_TRUE);
  if (!isItemCollected)
  {
    feedChanges();
  }

  return NS_OK;
}

nsresult
aaSqlitePump::feedItem()
{
  nsresult rv;
  PRUint32 i, count = sqlite3_column_count(db.GetStmt());
  nsCAutoString xml(kTransportRowHeader);

  xml.AppendInt(mRowIndex);
  xml.Append("\">\n");
  rv = write(xml.get());
  NS_ENSURE_SUCCESS(rv, rv);

  for (i = 0; i < count; i++) {
    rv = feedColumn(i);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = write(kTransportRowFooter);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = notifyListener();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSqlitePump::feedColumn(PRUint32 index)
{
  nsresult rv;
  const char *name = (const char *) sqlite3_column_name(db.GetStmt(), index);
  const char *text = (const char *) sqlite3_column_text(db.GetStmt(), index);

  rv = write("    <");
  NS_ENSURE_SUCCESS(rv, rv);

  rv = writeAndEscape(name);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = write(">");
  NS_ENSURE_SUCCESS(rv, rv);

  rv = writeAndEscape(text);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = write("</");
  NS_ENSURE_SUCCESS(rv, rv);

  rv = writeAndEscape(name);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = write(">\n");
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSqlitePump::feedChanges()
{
  nsresult rv;
  nsCAutoString xml(kTransportMessageHeader);

  xml.AppendLiteral("Query OK: ");

  PRInt32 changes = 0;
  rv = db.Changes(&changes);
  NS_ENSURE_SUCCESS(rv, rv);

  xml.AppendInt(changes);
  xml.AppendLiteral(" row affected");
  xml.AppendLiteral(kTransportMessageFooter);

  rv = write(xml.get());
  NS_ENSURE_SUCCESS(rv, rv);

  rv = notifyListener();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaSqlitePump::feedError()
{
  nsresult rv;
  nsCAutoString xml(kTransportErrorHeader);

  xml.AppendLiteral("ERROR ");

  PRInt32 code = 0;
  rv = db.ErrorCode(&code);
  NS_ENSURE_SUCCESS(rv, rv);

  xml.AppendInt(code);
  xml.AppendLiteral(": ");

  const char *msg = nsnull;
  rv = db.ErrorMsg(&msg);
  NS_ENSURE_SUCCESS(rv, rv);

  xml.AppendLiteral(msg);
  xml.AppendLiteral(kTransportErrorFooter);

  rv = write(xml.get());
  NS_ENSURE_SUCCESS(rv, rv);

  rv = notifyListener();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

