/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASQLITEPUMP_H
#define AASQLITEPUMP_H

#include "nsIRunnable.h"
#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsIFile.h"
#include "nsIInputStream.h"
#include "nsIOutputStream.h"
#include "nsIThread.h"

#include "aaSqliteStmt.h"

class aaSqliteChannel;

class aaSqlitePump : public nsIRunnable
{
public:
  aaSqlitePump(nsIFile *dbFile, const char *query, aaSqliteChannel *channel);
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  ~aaSqlitePump() {}
  nsresult write(const char *data);
  nsresult writen(const char *data, PRUint32 length);
  nsresult writeAndEscape(const char *data);
  nsresult notifyListener();
  nsresult feed(const char *data);
  nsresult start();
  nsresult read();
  nsresult feedResult();
  nsresult feedItem();
  nsresult feedColumn(PRUint32 index);
  nsresult feedChanges();
  nsresult feedError();

private:
  nsCOMPtr<nsIFile> mDBFile;
  nsCAutoString mQuery;
  nsRefPtr<aaSqliteChannel> mChannel;
  nsCOMPtr<nsIThread> mTarget;
  DBWrapper db;
  nsCOMPtr<nsIInputStream> mFeeder;
  nsCOMPtr<nsIOutputStream> mSink;
  PRUint32 mOffset;
  PRUint32 mCount;
  PRUint32 mRowIndex;
};

#endif /* AASQLITEPUMP_H */

