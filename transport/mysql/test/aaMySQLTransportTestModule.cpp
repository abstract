/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "nsTestUtils.h"
#include "aaMySQLChannelTest.h"

#define AA_MYSQL_TRANSPORT_TEST_MODULE_CID \
{0x5204199b, 0x8f92, 0x4a56, {0xb6, 0x0b, 0xca, 0x48, 0x06, 0xe2, 0xe5, 0x56}}
#define AA_MYSQL_TRANSPORT_TEST_MODULE_CONTRACT \
"@aasii.org/transport/mysql/unit;1"

class aaMySQLTransportTestModule: public nsITest,
                                   public nsIUTF8StringEnumerator
{
public:
  aaMySQLTransportTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  ~aaMySQLTransportTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaMySQLTransportTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaMySQLTransportTestModule::Test(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  "@aasii.org/transport/mysql/unit-channel;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaMySQLTransportTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaMySQLTransportTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(subtests[mSubtest++]);
  return NS_OK;
}

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaMySQLTransportTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaMySQLChannelTest)

static const nsModuleComponentInfo kComponents[] =
{
  { "MySQL Transport Module Test Container",
    AA_MYSQL_TRANSPORT_TEST_MODULE_CID,
    AA_MYSQL_TRANSPORT_TEST_MODULE_CONTRACT,
    aaMySQLTransportTestModuleConstructor}
  ,{"MySQL Channel Test",
    AA_MYSQL_CHANNEL_TEST_CID,
    AA_MYSQL_CHANNEL_TEST_CONTRACT,
    aaMySQLChannelTestConstructor}
};
NS_IMPL_NSGETMODULE(aaMySQLTransportTest, kComponents)
