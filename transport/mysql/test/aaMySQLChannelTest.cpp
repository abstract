/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsAutoPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */

/* Project includes */
#include "nsTestUtils.h"
#include "nsITestRunner.h"

#include "aaMySQLChannel.h"

#include "aaMySQLChannelTest.h"

/*
 * *** aaMySQLChannelTest implementation ***
 */

aaMySQLChannelTest::aaMySQLChannelTest() {}

NS_IMPL_ISUPPORTS1(aaMySQLChannelTest,
                   nsITest)

static const char *kHost = "localhost";
static const char *kUser = "abstract";
static const char *kPass = "abstract";
static const char *kDB = "mysql";
static const char *kQuery = "SHOW TABLES";

NS_IMETHODIMP
aaMySQLChannelTest::Test(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN(aTestRunner);
  nsresult rv;

  nsRefPtr<aaMySQLChannel> channel = new aaMySQLChannel(kHost, kUser, kPass,
      kDB, kQuery);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(channel->getMy(), "mysql channel: failed to init db");
  return NS_OK;
}
