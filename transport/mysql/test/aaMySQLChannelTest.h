/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAMYSQLCHANNELTEST_H
#define AAMYSQLCHANNELTEST_H

/* Unfrozen API */
#include "nsITest.h"

class nsITestRunner;

#define AA_MYSQL_CHANNEL_TEST_CID \
{0x1968df5d, 0xa67d, 0x4396, {0x85, 0xce, 0x95, 0xf9, 0x47, 0xd3, 0x75, 0x86}}
#define AA_MYSQL_CHANNEL_TEST_CONTRACT \
"@aasii.org/transport/mysql/unit-channel;1"

class aaMySQLChannelTest: public nsITest
{
public:
  aaMySQLChannelTest();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  ~aaMySQLChannelTest() {;}
};

#endif /* AAMYSQLCHANNELTEST_H */
