/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAMYSQLCHANNEL_H
#define AAMYSQLCHANNEL_H

#pragma GCC visibility push(default)
#include "mysql/mysql.h"
#pragma GCC visibility pop
#include "nsISupports.h"

/* Unfrozen API */

/* Project includes */

#ifdef DEBUG
#endif

class aaMySQLChannel : public nsISupports
{
public:
  aaMySQLChannel(const char *host, const char *user, const char *pass, const
      char *db, const char *query);
  NS_DECL_ISUPPORTS
  
  MYSQL* getMy() { return mMy; }
private:
  ~aaMySQLChannel();

private:
  MYSQL *mMy;
  const char *mHost;
  const char *mUser;
  const char *mPass;
  const char *mDB;
  const char *mQuery;
};

#endif /* AAMYSQLCHANNEL_H */
