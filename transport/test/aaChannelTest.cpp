/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "aaChannelTest.h"

#include "nsIFile.h"
#include "nsIThreadManager.h"
#include "nsIThread.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsMimeTypes.h"
#include "nsILoadGroup.h"

#include "aaTransportTestUtils.h"
#include "nsTestUtils.h"
#include "nsITestRunner.h"
#include "aaSqliteChannel.h"

#include "aaDBXMLtoXMLConverter.h"

static const nsCAutoString kCreateTestTable = NS_LITERAL_CSTRING(
    "CREATE TABLE test(id INTEGER PRIMARY KEY AUTOINCREMENT, "
    "tag CHAR(20) UNIQUE)"
    );
static const nsCAutoString kInsertTestRow1 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row1')"
    );
static const nsCAutoString kInsertTestRow2 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row2')"
    );
static const nsCAutoString kInsertTestRow3 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row3')"
    );
static const nsCAutoString kInsertTestRow4 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row4')"
    );
static const nsCAutoString kInsertTestRow5 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row5')"
    );
static const nsCAutoString kInsertTestRow6 = NS_LITERAL_CSTRING(
    "INSERT INTO test(tag) VALUES('row6')"
    );


static const nsCAutoString kTransportContentType =
  NS_LITERAL_CSTRING("text/dbxml");
static const nsCAutoString kTestContentType =
  NS_LITERAL_CSTRING(TEXT_XML);

/********************aaChannelObserver*****************************/

class aaChannelObserver : public nsIStreamListener
{
public:
  aaChannelObserver(nsITestRunner* aRunner, nsIRequest *aRequest)
    : mState(DEFAULT)
    , mRunner(aRunner)
    , mRequest(aRequest) {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIREQUESTOBSERVER
  NS_DECL_NSISTREAMLISTENER

  enum EState {
    START = 0x00,
    DATA,
    STOP,
    DEFAULT = 0xFF
  };
  nsresult Wait(EState aEventType = STOP);
protected:
  virtual ~aaChannelObserver() {}

  nsITestRunner* PickRunner() { return mRunner; }
private:
  
  EState mState;
  nsCOMPtr<nsITestRunner> mRunner;
  nsIRequest *mRequest;
};

NS_IMPL_ISUPPORTS2(aaChannelObserver,
                   nsIRequestObserver,
                   nsIStreamListener)

NS_IMETHODIMP
aaChannelObserver::OnStartRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  NS_TEST_BEGIN(mRunner);
  NS_TEST_ASSERT_MSG(DEFAULT == mState,
      "[channel test] onStartRequest is not called first");
  NS_TEST_ASSERT_MSG(aRequest == mRequest,
      "[channel test] bad request");
  mState = START;
  return NS_OK;
}

NS_IMETHODIMP
aaChannelObserver::OnStopRequest(nsIRequest *aRequest, nsISupports *aContext,
    nsresult aStatusCode)
{
  NS_TEST_BEGIN(mRunner);
  NS_TEST_ASSERT_MSG(DATA == mState || START == mState,
      "[channel test] onStopRequest is not called last");
  NS_TEST_ASSERT_MSG(aRequest == mRequest,
      "[channel test] bad request");
  mState = STOP;
  return NS_OK;
}

NS_IMETHODIMP
aaChannelObserver::OnDataAvailable(nsIRequest *aRequest, nsISupports *aContext,
    nsIInputStream *aInputStream, PRUint32 aOffset, PRUint32 aCount)
{
  NS_TEST_BEGIN(mRunner);
  NS_TEST_ASSERT_MSG(DATA == mState || START == mState,
      "[channel test] onDataAvailable is not called after onStartRequest");
  NS_TEST_ASSERT_MSG(aRequest == mRequest,
      "[channel test] bad request");
  mState = DATA;
  return NS_OK;
}

nsresult
aaChannelObserver::Wait(EState aEventType)
{
  nsresult rv = NS_OK;

  nsCOMPtr<nsIThreadManager> thManager =
    do_GetService("@mozilla.org/thread-manager;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIThread> currThread;
  rv = thManager->GetCurrentThread(getter_AddRefs(currThread));
  NS_ENSURE_SUCCESS(rv, rv);

  while (aEventType != mState)
  {
    PRBool fProcessed = PR_FALSE;
    rv = currThread->ProcessNextEvent(PR_TRUE, &fProcessed);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

/********************aaAttributeCheckObserver*****************************/
class aaAttributeCheckObserver : public aaChannelObserver
{
public:
  aaAttributeCheckObserver(nsITestRunner *aRunner, nsISupports *aOwner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest)
    , mOwner(aOwner) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaAttributeCheckObserver() {}
  nsresult checkAttributes(nsIRequest *aRequest);

  nsCOMPtr<nsISupports> mOwner;
};

NS_IMPL_ISUPPORTS_INHERITED0(aaAttributeCheckObserver,
    aaChannelObserver)

NS_IMETHODIMP
aaAttributeCheckObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  checkAttributes(aRequest);
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaAttributeCheckObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  checkAttributes(aRequest);
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaAttributeCheckObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  checkAttributes(aRequest);
  return aaChannelObserver::OnDataAvailable(aRequest, aContext, aInputStream,
      aOffset, aCount);
}

nsresult
aaAttributeCheckObserver::checkAttributes(nsIRequest *aRequest)
{
  NS_TEST_BEGIN(PickRunner());

  nsCOMPtr<nsIChannel> channel = do_QueryInterface(aRequest);
  NS_TEST_ASSERT_MSG(channel, "[channel test] request is not a channel");

  if (channel)
  {
    nsresult rv = NS_OK;
    nsCOMPtr<nsISupports> channelOwner;
    rv = channel->GetOwner(getter_AddRefs(channelOwner));
    NS_TEST_ASSERT_OK(rv);

    NS_TEST_ASSERT_MSG(mOwner.get() == channelOwner.get(),
        "[channel test] owner is not strong refferenced");

    nsCAutoString contentType;
    rv = channel->GetContentType(contentType);
    NS_TEST_ASSERT_OK(rv);

    NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
        "[channel test] content type is not text/dbxml");
  }

  return NS_OK;
}

/********************aaContentTypeObserver*****************************/
class aaContentTypeObserver : public aaChannelObserver
{
public:
  aaContentTypeObserver(nsITestRunner *aRunner,
      const nsACString& aNewContentType
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest)
    , mNewContentType(aNewContentType) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaContentTypeObserver() {}
  nsresult checkContentType(nsIRequest *aRequest,
      const nsACString& aContentType);

  nsCAutoString mNewContentType;
};

NS_IMPL_ISUPPORTS_INHERITED0(aaContentTypeObserver,
    aaChannelObserver)

NS_IMETHODIMP
aaContentTypeObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  checkContentType(aRequest, kTransportContentType);
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaContentTypeObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  checkContentType(aRequest, mNewContentType);
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaContentTypeObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  checkContentType(aRequest, mNewContentType);
  return aaChannelObserver::OnDataAvailable(aRequest, aContext, aInputStream,
      aOffset, aCount);
}

nsresult
aaContentTypeObserver::checkContentType(nsIRequest *aRequest,
    const nsACString& aContentType)
{
  NS_TEST_BEGIN(PickRunner());

  nsCOMPtr<nsIChannel> channel = do_QueryInterface(aRequest);
  NS_TEST_ASSERT_MSG(channel, "[channel test] request is not a channel");

  if (channel)
  {
    nsresult rv = NS_OK;
    nsCAutoString contentType;
    rv = channel->GetContentType(contentType);
    NS_TEST_ASSERT_OK(rv);

    NS_TEST_ASSERT_MSG(contentType.Equals(aContentType),
        "[channel test] content type is not right");
  }

  return NS_OK;
}

/********************aaCancelObserver*****************************/
class aaCancelObserver : public aaChannelObserver
{
public:
  aaCancelObserver(nsITestRunner *aRunner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaCancelObserver() {}
};

NS_IMPL_ISUPPORTS_INHERITED0(aaCancelObserver, aaChannelObserver)

NS_IMETHODIMP
aaCancelObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaCancelObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(aStatusCode == NS_BINDING_ABORTED,
      "[channel test] OnStopRequest status code is not failed");
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaCancelObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(PR_FALSE,
      "[channel test] OnDataAvailable is raised");
  return NS_OK;
}

/********************aaCancelOnStartObserver*****************************/
class aaCancelOnStartObserver : public aaChannelObserver
{
public:
  aaCancelOnStartObserver(nsITestRunner *aRunner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaCancelOnStartObserver() {}
};

NS_IMPL_ISUPPORTS_INHERITED0(aaCancelOnStartObserver, aaChannelObserver)

NS_IMETHODIMP
aaCancelOnStartObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  nsCOMPtr<nsIChannel> channel = do_QueryInterface(aRequest);
  if (channel)
  {
    channel->Cancel(NS_BINDING_ABORTED);
  }
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaCancelOnStartObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(NS_FAILED(aStatusCode),
      "[channel test] OnStopRequest status code is not failed");
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaCancelOnStartObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(PR_FALSE,
      "[channel test] OnDataAvailable is raised");
  return NS_OK;
}

/********************aaCancelOnDataObserver*****************************/
class aaCancelOnDataObserver : public aaChannelObserver
{
public:
  aaCancelOnDataObserver(nsITestRunner *aRunner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaCancelOnDataObserver() {}
};

NS_IMPL_ISUPPORTS_INHERITED0(aaCancelOnDataObserver, aaChannelObserver)

NS_IMETHODIMP
aaCancelOnDataObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaCancelOnDataObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(NS_FAILED(aStatusCode),
      "[channel test] OnStopRequest status code is not failed");
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaCancelOnDataObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  nsCOMPtr<nsIChannel> channel = do_QueryInterface(aRequest);
  if (channel)
  {
    nsresult res;
    channel->GetStatus(&res);
    if (NS_FAILED(res))
    {
      NS_TEST_BEGIN(PickRunner());
      NS_TEST_ASSERT_MSG(PR_FALSE,
          "[channel test] OnDataAvailable is raised");
    }
    else
    {
      channel->Cancel(NS_BINDING_ABORTED);
    }
  }
  else
  {
    NS_TEST_BEGIN(PickRunner());
    NS_TEST_ASSERT_MSG(PR_FALSE,
        "[channel test] OnDataAvailable is raised");
  }
  return NS_ERROR_FAILURE;
}

/********************aaOnDataExceptObserver*****************************/
class aaOnDataExceptObserver : public aaChannelObserver
{
public:
  aaOnDataExceptObserver(nsITestRunner *aRunner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest)
    , mExceptionRaised(PR_FALSE) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaOnDataExceptObserver() {}
  PRBool mExceptionRaised;
};

NS_IMPL_ISUPPORTS_INHERITED0(aaOnDataExceptObserver, aaChannelObserver)

NS_IMETHODIMP
aaOnDataExceptObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  return aaChannelObserver::OnStartRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaOnDataExceptObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(NS_FAILED(aStatusCode),
      "[channel test] OnStopRequest status code is not failed");
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaOnDataExceptObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  nsCOMPtr<nsIChannel> channel = do_QueryInterface(aRequest);
  if (!mExceptionRaised)
  {
    mExceptionRaised = PR_TRUE;
  }
  else
  {
    NS_TEST_BEGIN(PickRunner());
    NS_TEST_ASSERT_MSG(PR_FALSE,
        "[channel test] OnDataAvailable is raised");
  }
  return NS_BINDING_ABORTED;
}

/********************aaOnStartExceptObserver*****************************/
class aaOnStartExceptObserver : public aaChannelObserver
{
public:
  aaOnStartExceptObserver(nsITestRunner *aRunner
      ,nsIRequest *aRequest)
    : aaChannelObserver(aRunner, aRequest) {}
  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_NSISTREAMLISTENER
  NS_DECL_NSIREQUESTOBSERVER
private:
  virtual ~aaOnStartExceptObserver() {}
};

NS_IMPL_ISUPPORTS_INHERITED0(aaOnStartExceptObserver, aaChannelObserver)

NS_IMETHODIMP
aaOnStartExceptObserver::OnStartRequest(nsIRequest *aRequest,
    nsISupports *aContext)
{
  aaChannelObserver::OnStartRequest(aRequest, aContext);
  return NS_BINDING_ABORTED;
}

NS_IMETHODIMP
aaOnStartExceptObserver::OnStopRequest(nsIRequest *aRequest,
    nsISupports *aContext, nsresult aStatusCode)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(NS_BINDING_ABORTED == aStatusCode,
      "[channel test] OnStopRequest status code is not failed");
  return aaChannelObserver::OnStopRequest(aRequest, aContext, aStatusCode);
}

NS_IMETHODIMP
aaOnStartExceptObserver::OnDataAvailable(nsIRequest *aRequest,
    nsISupports *aContext, nsIInputStream *aInputStream,
    PRUint32 aOffset, PRUint32 aCount)
{
  NS_TEST_BEGIN(PickRunner());
  NS_TEST_ASSERT_MSG(PR_FALSE,
      "[channel test] OnDataAvailable is raised");
  return NS_OK;
}

/********************aaTestLoadGroup***************************/
class aaTestLoadGroup : public nsILoadGroup
{
public:
  aaTestLoadGroup() {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSILOADGROUP
  NS_DECL_NSIREQUEST

  nsIRequest* PickRequest() { return mRequest.get(); }
private:
  ~aaTestLoadGroup() {}

  nsCOMPtr<nsIRequest> mRequest;
};

NS_IMPL_ISUPPORTS2(aaTestLoadGroup,
    nsILoadGroup, nsIRequest)

/* attribute nsIRequestObserver groupObserver; */
NS_IMETHODIMP
aaTestLoadGroup::GetGroupObserver(nsIRequestObserver **aGroupObserver)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaTestLoadGroup::SetGroupObserver(nsIRequestObserver *aGroupObserver)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute nsIRequest defaultLoadRequest; */
NS_IMETHODIMP
aaTestLoadGroup::GetDefaultLoadRequest(nsIRequest **aDefaultLoadRequest)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaTestLoadGroup::SetDefaultLoadRequest(nsIRequest *aDefaultLoadRequest)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void addRequest (in nsIRequest aRequest, in nsISupports aContext); */
NS_IMETHODIMP
aaTestLoadGroup::AddRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  NS_ENSURE_ARG_POINTER(aRequest);
  mRequest = aRequest;
  return NS_OK;
}

/* void removeRequest (in nsIRequest aRequest, in nsISupports aContext, in
 * nsresult aStatus); */
NS_IMETHODIMP
aaTestLoadGroup::RemoveRequest(nsIRequest *aRequest, nsISupports *aContext,
  nsresult aStatus)
{
  NS_ENSURE_ARG_POINTER(aRequest);
  NS_ENSURE_TRUE(aRequest == mRequest.get(), NS_ERROR_FAILURE);
  mRequest = nsnull;
  return NS_OK;
}

/* readonly attribute nsISimpleEnumerator requests; */
NS_IMETHODIMP
aaTestLoadGroup::GetRequests(nsISimpleEnumerator **aRequests)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute unsigned long activeCount; */
NS_IMETHODIMP
aaTestLoadGroup::GetActiveCount(PRUint32 *aActiveCount)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute nsIInterfaceRequestor notificationCallbacks; */
NS_IMETHODIMP
aaTestLoadGroup::GetNotificationCallbacks(
  nsIInterfaceRequestor **aNotificationCallbacks)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaTestLoadGroup::SetNotificationCallbacks(
    nsIInterfaceRequestor *aNotificationCallbacks)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute AUTF8String name; */
NS_IMETHODIMP
aaTestLoadGroup::GetName(nsACString & aName)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* boolean isPending (); */
NS_IMETHODIMP
aaTestLoadGroup::IsPending(PRBool *_retval NS_OUTPARAM)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute nsresult status; */
NS_IMETHODIMP
aaTestLoadGroup::GetStatus(nsresult *aStatus)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void cancel (in nsresult aStatus); */
NS_IMETHODIMP
aaTestLoadGroup::Cancel(nsresult aStatus)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void suspend (); */
NS_IMETHODIMP
aaTestLoadGroup::Suspend()
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void resume (); */
NS_IMETHODIMP
aaTestLoadGroup::Resume()
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute nsILoadGroup loadGroup; */
NS_IMETHODIMP
aaTestLoadGroup::GetLoadGroup(nsILoadGroup * *aLoadGroup)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaTestLoadGroup::SetLoadGroup(nsILoadGroup * aLoadGroup)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute nsLoadFlags loadFlags; */
NS_IMETHODIMP
aaTestLoadGroup::GetLoadFlags(nsLoadFlags *aLoadFlags)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaTestLoadGroup::SetLoadFlags(nsLoadFlags aLoadFlags)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/********************aaTestInterfaceRequestor*****************************/
class aaTestInterfaceRequestor : public nsIInterfaceRequestor
{
public:
  aaTestInterfaceRequestor() {}
  NS_DECL_ISUPPORTS
  NS_IMETHOD GetInterface(const nsIID& aIID, void **aResult)
  { return NS_ERROR_NOT_IMPLEMENTED; }
private:
  ~aaTestInterfaceRequestor() {}
};

NS_IMPL_ISUPPORTS1(aaTestInterfaceRequestor,
                   nsIInterfaceRequestor)

/********************aaChannelTest*****************************/

aaChannelTest::aaChannelTest()
{
}

NS_IMPL_ISUPPORTS1(aaChannelTest, nsITest)

NS_IMETHODIMP
aaChannelTest::Test(nsITestRunner *aRunner)
{
  nsresult rv = NS_OK;
  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIFile> file;
  rv = AA_NewTempFile(getter_AddRefs(file),
      NS_LITERAL_CSTRING("transport.sqlite"), PR_TRUE);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelAttributes(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelContentType(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelCancelAAO(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelCancelOS(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelCancelOD(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelExceptionOS(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testChannelExceptionOD(aRunner, file);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = testDBXMLtoXMLConverter(aRunner);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelAttributes(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kCreateTestTable);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsRefPtr<aaSqliteChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> iChannel;
  rv = channel->QueryInterface(NS_GET_IID(nsIChannel),
      getter_AddRefs(iChannel));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIURI> channelURI;
  rv = iChannel->GetURI(getter_AddRefs(channelURI));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  PRBool fEqual = PR_FALSE;
  rv = channelURI->Equals(dbURI, &fEqual);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(fEqual,
      "[channel test] channel uri is not equal to db uri");

  rv = iChannel->GetOriginalURI(getter_AddRefs(channelURI));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channelURI->Equals(dbURI, &fEqual);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(fEqual,
      "[channel test] channel original uri is not equal to db uri");

  nsCOMPtr<nsISupports> owner = do_QueryInterface(this, &rv);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->SetOwner(owner);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupports> channelOwner;
  rv = iChannel->GetOwner(getter_AddRefs(channelOwner));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(owner.get() == channelOwner.get(),
      "[channel test] owner is not strong refferenced");

  nsCOMPtr<nsIInterfaceRequestor> requestor =
    new aaTestInterfaceRequestor();
  NS_ENSURE_TRUE(requestor, NS_ERROR_OUT_OF_MEMORY);

  nsCOMPtr<nsIInterfaceRequestor> channelRequestor;
  rv = iChannel->GetNotificationCallbacks(getter_AddRefs(channelRequestor));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(!channelRequestor,
      "[channel test] notification callbacks are not empty");

  rv = iChannel->SetNotificationCallbacks(requestor);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->GetNotificationCallbacks(getter_AddRefs(channelRequestor));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(channelRequestor == requestor,
      "[channel test] notification callbacks are wrong");

  nsCAutoString contentType;
  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
    "[channel test] content type is not equal to text/dbxml");

  nsRefPtr<aaAttributeCheckObserver> observer =
    new aaAttributeCheckObserver(aRunner, owner, iChannel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  PRInt32 contentLength;
  rv = iChannel->GetContentLength(&contentLength);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(-1 == contentLength,
      "[channel test] channel length is not equal -1");

  nsrefcnt channelRefCount = iChannel->AddRef() - 1;
  rv = iChannel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }
 
  rv = iChannel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_MSG(NS_FAILED(rv),
      "[channel test] channel can be reopenned");
  rv = NS_OK;
 
  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
      "[channel test] content type is not text/dbxml");

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(channelRefCount == iChannel->Release(),
      "[channel test] invalid channel life support");

  rv = iChannel->GetOwner(getter_AddRefs(channelOwner));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(owner.get() == channelOwner.get(),
      "[channel test] owner is not strong refferenced");

  rv = iChannel->GetNotificationCallbacks(getter_AddRefs(channelRequestor));
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  NS_TEST_ASSERT_MSG(!channelRequestor,
      "[channel test] notification callbacks are not empty");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelContentType(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow1);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> iChannel = new aaSqliteChannel(dbURI);
  rv = iChannel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  
  rv = iChannel->SetLoadFlags(nsIChannel::LOAD_FROM_CACHE);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsLoadFlags flags;
  rv = iChannel->GetLoadFlags(&flags);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(flags == nsIChannel::LOAD_FROM_CACHE,
      "[channel test] content type is not empty");

  nsCAutoString contentType;
  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
    "[channel test] content type does not equal to text/dbxml");

  rv = iChannel->SetContentType(kTestContentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
    "[channel test] content type does not equal to text/dbxml");

  nsRefPtr<aaContentTypeObserver> observer =
    new aaContentTypeObserver(aRunner, kTestContentType, iChannel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaTestLoadGroup> loadGroup = new aaTestLoadGroup();
  rv = loadGroup ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->SetLoadGroup(loadGroup);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  PRBool isPending = PR_FALSE;
  rv = iChannel->IsPending(&isPending);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(!isPending,
      "[channel test] channel is pending");

  rv = iChannel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  nsresult channelStatus;
  rv = iChannel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_OK(channelStatus);

  rv = iChannel->IsPending(&isPending);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(isPending,
      "[channel test] channel is not pending");
  NS_TEST_ASSERT_MSG(loadGroup->PickRequest(),
      "[channel test] ichannel is not set to load group");
  
  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTransportContentType),
      "[channel test] content type is not text/dbxml");

  rv = iChannel->SetContentType(kTestContentType);
  NS_TEST_ASSERT_MSG(NS_FAILED(rv),
      "[channel test] content type can be set before OnStartRequest");
  rv = NS_OK;

  rv = observer->Wait(aaChannelObserver::START);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->SetContentType(kTestContentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = iChannel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_OK(channelStatus);

  NS_TEST_ASSERT_MSG(!loadGroup->PickRequest(),
      "[channel test] ichannel is not cleared to load group");

  rv = iChannel->GetContentType(contentType);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(contentType.Equals(kTestContentType),
      "[channel test] content type is not text/xml");

  rv = iChannel->IsPending(&isPending);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(!isPending,
      "[channel test] channel is pending");
  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelCancelAAO(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow2);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
 
  nsRefPtr<aaCancelObserver> observer =
    new aaCancelObserver(aRunner, channel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = channel->Cancel(NS_BINDING_ABORTED);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->Cancel(NS_BINDING_ABORTED);
  NS_TEST_ASSERT_MSG(NS_OK != rv,
      "[channel test] second Cancel() succeeded");

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsresult channelStatus = NS_OK;
  rv = channel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(channelStatus == NS_BINDING_ABORTED,
      "[channel test] status is not failed");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelCancelOS(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow3);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
 
  nsRefPtr<aaCancelOnStartObserver> observer =
    new aaCancelOnStartObserver(aRunner, channel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsresult channelStatus = NS_OK;
  rv = channel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(channelStatus == NS_BINDING_ABORTED,
      "[channel test] status is not failed");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelCancelOD(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow4);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
 
  nsRefPtr<aaCancelOnDataObserver> observer =
    new aaCancelOnDataObserver(aRunner, channel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsresult channelStatus = NS_OK;
  rv = channel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(channelStatus == NS_BINDING_ABORTED,
      "[channel test] status is not failed");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelExceptionOS(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow5);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
 
  nsRefPtr<aaOnStartExceptObserver> observer =
    new aaOnStartExceptObserver(aRunner, channel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsresult channelStatus = NS_OK;
  rv = channel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(NS_BINDING_ABORTED == channelStatus,
      "[channel test] status is not failed");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testChannelExceptionOD(nsITestRunner *aRunner, nsIFile *aFile)
{
  nsresult rv = NS_OK;
  NS_ENSURE_ARG_POINTER(aFile);

  NS_TEST_BEGIN(aRunner);

  nsCOMPtr<nsIURI> dbURI;
  rv = AA_NewFileURIWithSql(getter_AddRefs(dbURI), aFile, kInsertTestRow6);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIChannel> channel = new aaSqliteChannel(dbURI);
  rv = channel ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
 
  nsRefPtr<aaOnDataExceptObserver> observer =
    new aaOnDataExceptObserver(aRunner, channel);
  rv = observer ? NS_OK : NS_ERROR_OUT_OF_MEMORY;
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = aRunner->ArmTimer(500);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  rv = channel->AsyncOpen(observer, nsnull);
  NS_TEST_ASSERT_OK(rv);
  if (NS_FAILED(rv)) {
    (void) aRunner->MarkTestEnd(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = observer->Wait();
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);

  nsresult channelStatus = NS_OK;
  rv = channel->GetStatus(&channelStatus);
  NS_TEST_ASSERT_OK(rv); NS_ENSURE_SUCCESS(rv, rv);
  NS_TEST_ASSERT_MSG(NS_FAILED(channelStatus),
      "[channel test] status is not failed");

  (void) aRunner->MarkTestEnd(this);

  return NS_OK;
}

nsresult
aaChannelTest::testDBXMLtoXMLConverter(nsITestRunner *aRunner)
{
  NS_TEST_BEGIN(aRunner);

  nsRefPtr<aaDBXMLtoXMLConverter> converter = new aaDBXMLtoXMLConverter();
  NS_TEST_ASSERT_MSG(converter, "");
  NS_ENSURE_TRUE(converter, NS_ERROR_OUT_OF_MEMORY);

  nsCAutoString xml("<?xml version=\"1.0\" ?><sometag></sometag>");

  PRInt32 headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(6 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(converter->mHasHead, "[converter] invalid head flag");

  PRInt32 endPos = converter->FindEnd(xml, headPos);
  NS_TEST_ASSERT_MSG(22 == endPos, "[converter] invalid end position");
  NS_TEST_ASSERT_MSG(converter->mHasEnd, "[converter] invalid end flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.IsEmpty(),
      "[converter] invalid buffer");

  converter->mHasHead = PR_FALSE;
  converter->mHasEnd = PR_FALSE;

  xml.AssignLiteral("<!-- 1 --><?");
  headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(12 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(!converter->mHasHead, "[converter] invalid head flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.EqualsLiteral("<?"),
      "[converter] invalid buffer");

  xml.AssignLiteral("x");
  headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(1 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(!converter->mHasHead, "[converter] invalid head flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.EqualsLiteral("<?x"),
      "[converter] invalid buffer");

  xml.AssignLiteral("ml ?");
  headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(3 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(converter->mHasHead, "[converter] invalid head flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.IsEmpty(),
      "[converter] invalid buffer");
  
  endPos = converter->FindEnd(xml, headPos);
  NS_TEST_ASSERT_MSG(4 == endPos, "[converter] invalid end position");
  NS_TEST_ASSERT_MSG(!converter->mHasEnd, "[converter] invalid end flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.EqualsLiteral("?"),
      "[converter] invalid buffer");

  xml.AssignLiteral("><asd>");
  endPos = converter->FindEnd(xml, headPos);
  NS_TEST_ASSERT_MSG(1 == endPos, "[converter] invalid end position");
  NS_TEST_ASSERT_MSG(converter->mHasEnd, "[converter] invalid end flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.IsEmpty(),
      "[converter] invalid buffer");

  converter->mHasHead = PR_FALSE;

  xml.AssignLiteral("<!-- 1 --><?");
  headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(12 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(!converter->mHasHead, "[converter] invalid head flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.EqualsLiteral("<?"),
      "[converter] invalid buffer");

  xml.AssignLiteral("xml-1");
  headPos = converter->FindHead(xml);
  NS_TEST_ASSERT_MSG(5 == headPos, "[converter] invalid head position");
  NS_TEST_ASSERT_MSG(!converter->mHasHead, "[converter] invalid head flag");
  NS_TEST_ASSERT_MSG(converter->mBuffer.IsEmpty(),
      "[converter] invalid buffer");

  return NS_OK;
}

