/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsNetCID.h"
#include "nsIAsyncInputStream.h"
#include "nsIAsyncOutputStream.h"
#include "nsIPipe.h"

/* Unfrozen API */
#include "nsThreadUtils.h"
#include "nsIRunnable.h"
#include "nsIThread.h"
#include "nsIBinaryInputStream.h"
#include "nsIBinaryOutputStream.h"

/* Project includes */
#include "aaTransportUtils.h"
#include "nsTestUtils.h"
#include "nsITestRunner.h"

#include "aaPipeTest.h"

class aaPipeObserver : public nsISupports
{
public:
  aaPipeObserver(nsITestRunner *testRunner, nsITest *parent);
  NS_DECL_ISUPPORTS

  nsresult Notify(nsIInputStream *stream, PRUint32 offset);
private:
  nsITestRunner *cxxUnitTestRunner;
  nsCOMPtr<nsITest> mParent;
private:
  ~aaPipeObserver() {}
  nsresult consume(nsIInputStream *stream, PRUint32 count);
};

aaPipeObserver::aaPipeObserver(nsITestRunner *testRunner, nsITest *parent)
  :cxxUnitTestRunner(testRunner), mParent(parent) {}

NS_IMPL_THREADSAFE_ISUPPORTS0(aaPipeObserver)

nsresult
aaPipeObserver::Notify(nsIInputStream *stream, PRUint32 offset)
{
  NS_TEST_ASSERT_MSG(offset < 2, "pipe: wrong notification count");
  NS_ENSURE_TRUE(offset < 2, NS_ERROR_INVALID_ARG);
  nsresult rv;
  if (offset == 0) {
    rv = consume(stream, 10000);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  cxxUnitTestRunner->MarkTestEnd(mParent);
  mParent = nsnull;
  return NS_OK;
}

nsresult
aaPipeObserver::consume(nsIInputStream *stream, PRUint32 count)
{
  nsresult rv;
  nsCOMPtr<nsIBinaryInputStream> biStream
    = do_CreateInstance("@mozilla.org/binaryinputstream;1", &rv);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = biStream->SetInputStream(stream);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString hello;
  nsCAutoString sample("Hello world!");
  PRUint32 i;
  for (i = 0; i < count; i++) {
    rv = biStream->ReadCString(hello);
    NS_TEST_ASSERT_OK(rv);
    NS_ENSURE_SUCCESS(rv, rv);
    NS_TEST_ASSERT_MSG(sample.Equals(hello), "pipe: wrong string read");
  }
  return NS_OK;
}

class aaPipeEvent : public nsIRunnable
{
public:
  aaPipeEvent(aaPipeObserver *target, nsIInputStream *stream, PRUint32 offset);
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsRefPtr<aaPipeObserver> mTarget;
  nsCOMPtr<nsIInputStream> mStream;
  PRUint32 mOffset;
private:
  ~aaPipeEvent() {}
};

aaPipeEvent::aaPipeEvent(aaPipeObserver *target, nsIInputStream *stream,
    PRUint32 offset)
:mTarget(target), mStream(stream), mOffset(offset) {}

NS_IMPL_THREADSAFE_ISUPPORTS1(aaPipeEvent,
                              nsIRunnable)

NS_IMETHODIMP
aaPipeEvent::Run()
{
  NS_ENSURE_STATE(mTarget);
  return mTarget->Notify(mStream, mOffset);
}

class aaPipeRequest : public nsIRunnable
{
public:
  aaPipeRequest();
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
  
  nsresult AsyncOpen(aaPipeObserver *observer);
private:
  nsCOMPtr<nsIThread> mTarget;
  nsRefPtr<aaPipeObserver> mObserver;

private:
  ~aaPipeRequest() {}
  nsresult feed(PRUint32 offset, PRUint32 count);
};

aaPipeRequest::aaPipeRequest() {}

NS_IMPL_THREADSAFE_ISUPPORTS1(aaPipeRequest,
                              nsIRunnable)

nsresult
aaPipeRequest::AsyncOpen(aaPipeObserver *observer)
{
  nsresult rv;

  nsCOMPtr<nsIEventTarget> worker =
    do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  mObserver = observer;
  mTarget = do_GetCurrentThread();

  rv = worker->Dispatch(this, NS_DISPATCH_NORMAL);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaPipeRequest::Run()
{
  nsresult rv;
  rv = feed(0, 10000);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaPipeRequest::feed(PRUint32 offset, PRUint32 count)
{
  NS_ENSURE_STATE(mTarget);
  nsresult rv;

  nsCOMPtr<nsIInputStream> feeder;
  nsCOMPtr<nsIOutputStream> sink;
  rv = AA_CreatePipe(getter_AddRefs(feeder),
                  getter_AddRefs(sink));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIBinaryOutputStream> boStream
    = do_CreateInstance("@mozilla.org/binaryoutputstream;1", &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = boStream->SetOutputStream(sink);
  NS_ENSURE_SUCCESS(rv, rv);

  nsRefPtr<aaPipeEvent> event = new aaPipeEvent(mObserver, feeder, offset);
  rv = mTarget->Dispatch(event, NS_DISPATCH_NORMAL);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCAutoString hello("Hello world!");
  PRUint32 i;
  for (i = 0; i < count; i++) {
    rv = boStream->WriteStringZ(hello.get());
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

/*
 * aaPipeTest
 */

aaPipeTest::aaPipeTest() {}

NS_IMPL_ISUPPORTS1(aaPipeTest,
                   nsITest)

NS_IMETHODIMP
aaPipeTest::Test(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN(aTestRunner);
  nsRefPtr<aaPipeObserver> observer = new aaPipeObserver(aTestRunner, this);

  nsRefPtr<aaPipeRequest> request = new aaPipeRequest();
  NS_ENSURE_TRUE(request, NS_ERROR_OUT_OF_MEMORY);

  rv = request->AsyncOpen(observer);
  NS_TEST_ASSERT_OK(rv);
  NS_ENSURE_SUCCESS(rv, rv);

  aTestRunner->ArmTimer(1000);

  return NS_OK;
}
