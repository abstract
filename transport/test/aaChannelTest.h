/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_CHANNEL_TEST_H
#define AA_CHANNEL_TEST_H

/* Unfrozen API */
#include "nsITest.h"

class nsITestRunner;

#define AA_CHANNEL_TEST_CID \
{0x75a192d7, 0x90bd, 0x4f5e, {0x80, 0x90, 0x1b, 0xa3, 0xa0, 0x6a, 0x63, 0x6}}
#define AA_CHANNEL_TEST_CONTRACT "@aasii.org/transport/test/channel;1"

class aaChannelTest: public nsITest
{
public:
  aaChannelTest();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  nsresult testChannelAttributes(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelContentType(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelCancelAAO(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelCancelOS(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelCancelOD(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelExceptionOS(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testChannelExceptionOD(nsITestRunner *aRunner, nsIFile *aFile);
  nsresult testDBXMLtoXMLConverter(nsITestRunner *aRunner);
private:
  ~aaChannelTest() {;}
};

#endif /* AA_CHANNEL_TEST_H */

