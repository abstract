/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIStringEnumerator.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaPipeTest.h"
#include "aaChannelTest.h"

#define AA_TRANSPORT_TEST_MODULE_CID \
{0x9cc69955, 0xc48c, 0x42b2, {0xa3, 0xdb, 0x17, 0x15, 0x7e, 0xf6, 0xf2, 0xd5}}
#define AA_TRANSPORT_TEST_MODULE_CONTRACT "@aasii.org/transport/unit;1"

class aaTransportTestModule: public nsITest,
                       public nsIUTF8StringEnumerator
{
public:
  aaTransportTestModule() :mSubtest(0) {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_NSIUTF8STRINGENUMERATOR
private:
  ~aaTransportTestModule() {;}
  PRUint32 mSubtest;
};

NS_IMPL_ISUPPORTS2(aaTransportTestModule,
                   nsITest,
                   nsIUTF8StringEnumerator)

NS_IMETHODIMP
aaTransportTestModule::Test(nsITestRunner *aTestRunner)
{
  return NS_OK;
}

/* nsIUTF8StringEnumerator */
static const char* subtests[] =
{
  "@aasii.org/transport/pipe;1"
  ,AA_CHANNEL_TEST_CONTRACT
  ,"@aasii.org/transport/sqlite/unit;1"
  ,"@aasii.org/transport/mysql/unit;1"
};
#define subtestCount (sizeof(subtests) / sizeof(char*))

NS_IMETHODIMP
aaTransportTestModule::HasMore(PRBool *aHasMore)
{
  NS_ENSURE_ARG_POINTER(aHasMore);
  *aHasMore = (mSubtest < subtestCount);
  return NS_OK;
}

NS_IMETHODIMP
aaTransportTestModule::GetNext(nsACString &aContractID)
{
  NS_ENSURE_TRUE(mSubtest < subtestCount, NS_ERROR_FAILURE);
  aContractID.Assign(subtests[mSubtest++]);
  return NS_OK;
}

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTransportTestModule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaPipeTest)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaChannelTest)

static const nsModuleComponentInfo kComponents[] =
{
  { "Transport Module Test Container",
    AA_TRANSPORT_TEST_MODULE_CID,
    AA_TRANSPORT_TEST_MODULE_CONTRACT,
    aaTransportTestModuleConstructor}
  ,{"Pipe Test",
    AA_PIPE_TEST_CID,
    AA_PIPE_TEST_CONTRACT,
    aaPipeTestConstructor}
  ,{"Channel Test",
    AA_CHANNEL_TEST_CID,
    AA_CHANNEL_TEST_CONTRACT,
    aaChannelTestConstructor}
};
NS_IMPL_NSGETMODULE(aaTransportTest, kComponents)
