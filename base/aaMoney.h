/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAMONEY_H
#define AAMONEY_H 1

#define AA_MONEY_CID \
{0xcffd4531, 0xf2f7, 0x46e6, {0xba, 0x27, 0x3a, 0x80, 0x5c, 0xb2, 0x44, 0xae}}

#include "aaIMoney.h"

class aaMoney : public aaIMoney
{
public:
  aaMoney();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIRESOURCE
  NS_DECL_AAIMONEY

private:
  ~aaMoney();
  friend class aaBaseTest;

  PRInt64 mId;
  nsAutoString mTag;
  PRBool mRead;
  PRBool mEdited;
};

#endif /* AAMONEY_H */
