/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Project includes */
#include "aaIHandler.h"
#include "aaMoney.h"

aaMoney::aaMoney()
  :mId(0), mTag((const PRUnichar *) nsnull), mRead(PR_FALSE), mEdited(PR_FALSE)
{
}

aaMoney::~aaMoney()
{
}

NS_IMPL_ISUPPORTS3_CI(aaMoney,
                      aaIDataNode,
                      aaIResource,
                      aaIMoney)
/* aaIDataNode */
NS_IMETHODIMP
aaMoney::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return aQuery->HandleMoney(this);
}

NS_IMETHODIMP
aaMoney::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = mEdited;
  return NS_OK;
}

NS_IMETHODIMP
aaMoney::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaMoney::GetType(PRInt32 *aType)
{
  NS_ENSURE_ARG_POINTER(aType);
  *aType = aaIAsset::TYPE_MONEY;
  return NS_OK;
}
PRInt32
aaMoney::PickType()
{
  return aaIAsset::TYPE_MONEY;
}

NS_IMETHODIMP
aaMoney::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaMoney::SetId(PRInt64 aId)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaMoney::SetNumCode(PRInt64 aNumCode)
{
  mId = aNumCode;
  return NS_OK;
}
PRInt64
aaMoney::PickId()
{
  return mId;
}

NS_IMETHODIMP
aaMoney::GetTag(nsAString & aTag)
{
  aTag.Assign(mTag);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaMoney::SetAlphaCode(const nsAString & aAlphaCode)
{
  if (mTag.Equals(aAlphaCode))
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mTag.Assign(aAlphaCode);
  return NS_OK;
}
