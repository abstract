/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATRANSACTION_H
#define AATRANSACTION_H 1

#define AA_TRANSACTION_CID \
{0x527a04a3, 0xaff7, 0x4d56, {0xba, 0x75, 0x64, 0xf3, 0xcf, 0x10, 0x04, 0x53}}

#include "aaITransaction.h"

class aaIFact;
class aaIBalance;
#ifdef DEBUG
#include "aaIFact.h"
#include "aaIBalance.h"
#endif

class aaTransaction : public aaITransaction
{
public:
  aaTransaction();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAITRANSACTION

private:
  ~aaTransaction() {}
  friend class aaBaseTest;

  nsCOMPtr<aaIFact> mFact;
  nsCOMPtr<aaIBalance> mBalanceFrom;
  nsCOMPtr<aaIBalance> mBalanceTo;
  nsCOMPtr<aaIBalance> mIncome;
  double mValue;
  double mEarnings;

  PRPackedBool mHasBalanceFrom;
  PRPackedBool mHasBalanceTo;
  PRPackedBool mHasEarnings;
  PRPackedBool mBroadcasting;

  nsresult tryValueCalculation();
  nsresult doValueCalculation();
};

#endif /* AATRANSACTION_H */
