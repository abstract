/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Project includes */
#include "aaIRule.h"
#include "aaIFact.h"
#include "aaIHandler.h"
#include "aaIStorager.h"
#include "aaIStorager.h"
#include "aaEventBase.h"
#include "aaEvent.h"

aaEvent::aaEvent()
{
  mState = new aaEventBase(this);
}

aaEvent::~aaEvent()
{
}

NS_IMPL_ISUPPORTS6_CI(aaEvent,
                      nsISupportsWeakReference,
                      aaIDataNode,
                      aaIListNode,
                      aaIEvent,
                      nsIArray,
                      nsIMutableArray)

void
aaEvent::SetState(aaEventBase *aState)
{
  mState = aState;
}
