/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAASSET_H
#define AAASSET_H 1

#define AA_ASSET_CID \
{0x063badbd, 0xe607, 0x4265, {0x84, 0x90, 0x06, 0x2a, 0x70, 0xa5, 0x27, 0x38}}

#include "aaIResource.h"

class aaAsset : public aaIAsset
{
public:
  aaAsset();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIRESOURCE
  NS_DECL_AAIASSET

private:
  virtual ~aaAsset();
  friend class aaBaseTest;

  PRInt64 mId;
  nsAutoString mTag;
  PRBool mRead;
  PRBool mEdited;
};

#endif /* AAASSET_H */
