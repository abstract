/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAFLOW_H
#define AAFLOW_H 1

#define AA_FLOW_CID \
{0x6dfc0a28, 0x610a, 0x4b30, {0x88, 0xa9, 0xd9, 0xee, 0x58, 0x24, 0xa6, 0x4e}}

#include "nsWeakReference.h"
#include "nsCOMArray.h"
#include "nsIArray.h"
#include "aaIFlow.h"

class aaIEntity;
class aaIResource;
class aaIRule;

#ifdef DEBUG
#include "aaIRule.h"
#endif

class aaFlow : public aaIFlow,
               public nsIArray,
               public nsSupportsWeakReference
{
public:
  aaFlow();
  NS_DECL_ISUPPORTS
  NS_DECL_NSIARRAY
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIFLOW

private:
  ~aaFlow();
  friend class aaBaseTest;

  PRInt64 mId;
  nsAutoString mTag;
  nsCOMPtr<aaIEntity> mEntity;
  nsCOMPtr<aaIResource> mGiveResource;
  nsCOMPtr<aaIResource> mTakeResource;
  PRBool mRead;
  PRBool mEdited;
  double mRate;
  double mLimit;
  PRBool mIsOffBalance;
  nsCOMArray<aaIRule> mRules;
};

#endif /* AAFLOW_H */

