/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AABASETESTUTILS_H
#define AABASETESTUTILS_H

class aaIBalance;
class aaITransaction;

XPCOM_API(PRBool)
testFact(nsITestRunner *cxxUnitTestRunner, aaIFact *node, PRInt64 fromFlowId,
    PRInt64 toFlowId, double sum);

XPCOM_API(PRBool)
testTxn(nsITestRunner *cxxUnitTestRunner, aaITransaction *txn,
    PRInt64 fromFlowId, PRInt64 toFlowId, double amount, double value,
    PRBool status = 0, double earnings = 0.0);

XPCOM_API(PRBool)
testBalance(nsITestRunner *cxxUnitTestRunner, aaIBalance *node, PRInt64 flowId,
    PRInt64 resourceId, double amount, double value, PRBool aSide);

XPCOM_API(PRBool)
testIncomeState(nsITestRunner *cxxUnitTestRunner, aaIBalance *node,
    double value, PRBool aSide);

XPCOM_API(PRBool)
testState(nsITestRunner *cxxUnitTestRunner, aaIState *node, PRInt64 flowId,
    PRInt64 resourceId, double sum);

XPCOM_API(PRBool)
testRule(nsITestRunner *cxxUnitTestRunner, aaIRule *node, PRInt64 fromFlowId,
    PRInt64 toFlowId, PRBool factSide, PRBool changeSide, double rate,
    const nsAString &tag);

#endif /* AABASETESTUTILS_H */
