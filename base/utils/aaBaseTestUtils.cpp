/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#define _IMPL_NS_COM

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaAmountUtils.h"
#include "nsStringUtils.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIRule.h"
#include "aaITransaction.h"
#include "aaIBalance.h"

XPCOM_API(PRBool)
testFact(nsITestRunner *cxxUnitTestRunner, aaIFact *node, PRInt64 fromFlowId,
    PRInt64 toFlowId, double sum)
 {
  nsresult rv;
  PRBool equals = PR_TRUE;
 
  NS_TEST_ASSERT_MSG(node, "  'fact' is null");
  if (NS_UNLIKELY( ! node ))
    return PR_FALSE;
  nsCOMPtr<aaIFlow> flow;
  PRInt64 id;
  node->GetTakeFrom(getter_AddRefs( flow ));
  if ( flow ) {
    flow->GetId( &id );
    if (NS_UNLIKELY( id != fromFlowId )) {
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, "  'fact.takeFrom.id' is wrong" );
    }
  } else if (NS_UNLIKELY( fromFlowId != 0 )) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'fact.takeFrom.id' is wrong" );
   }

  node->GetGiveTo(getter_AddRefs( flow ));
  if ( flow ) {
    flow->GetId( &id );
    if (NS_UNLIKELY( id != toFlowId )) {
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, "  'fact.giveTo.id' is wrong" );
    }
  } else if (NS_UNLIKELY( toFlowId != 0 )) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'fact.giveTo.id' is wrong" );
  }

  double amount, diff;
  rv = node->GetAmount(&amount);
  diff = amount - sum;
  if (diff > 0.0001 || diff < -0.0001) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'fact.amount' is wrong" );
  }

  return equals;
}

XPCOM_API(PRBool)
testTxn(nsITestRunner *cxxUnitTestRunner, aaITransaction *txn,
    PRInt64 fromFlowId, PRInt64 toFlowId, double amount, double value,
    PRBool status = 0, double earnings = 0.0)
{
  PRBool equals = PR_TRUE;
 
  NS_TEST_ASSERT_MSG(txn, "  'txn' is null");
  if (NS_UNLIKELY( ! txn ))
    return PR_FALSE;
  if (! testFact(cxxUnitTestRunner, txn->PickFact(), fromFlowId, toFlowId,
        amount))
    return PR_FALSE;

  double sum, diff;
  txn->GetValue(&sum);
  diff = value - sum;
  if (! isZero(diff)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'txn.value' is wrong" );
  }

  PRBool b;
  txn->GetHasEarnings(&b);
  if (b != status) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'txn.hasEarnings' is wrong" );
  }

  if (!b)
    return equals;

  return equals;
}

XPCOM_API(PRBool)
testBalance(nsITestRunner *cxxUnitTestRunner, aaIBalance *node, PRInt64 flowId,
    PRInt64 resourceId, double amount, double value, PRBool aSide)
{ 
  nsresult rv;
  PRBool equals = PR_TRUE;
  if (NS_UNLIKELY( ! node )) {
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'balance' is null");
    return PR_FALSE;
  }
  nsCOMPtr<aaIFlow> flow;
  rv = node->GetFlow(getter_AddRefs( flow ));
  NS_TEST_ASSERT_MSG(flow, "  'balance.flow' is null");
  PRInt64 id;
  if (NS_LIKELY( flow )) {
    flow->GetId(&id);
    if (NS_UNLIKELY( id != flowId) ) {
      nsCAutoString msg("  'balance.flow.id' is wrong - ");
      AppendInt(msg, id);
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
    }
  } else {
    equals = PR_FALSE;
  }

  nsCOMPtr<aaIResource> resource;
  rv = node->GetResource(getter_AddRefs( resource ));
  NS_TEST_ASSERT_MSG(resource, "  'balance.resource' is null" );
  if (NS_LIKELY( resource )) {
    resource->GetId(&id);
    if (NS_UNLIKELY( id != resourceId) ) {
      nsCAutoString msg("  'balance.resource.id' is wrong - ");
      AppendInt(msg, id);
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
    }
  } else {
    equals = PR_FALSE;
  }

  double sum, diff;
  node->GetAmount(&sum);
  diff = amount - sum;
  if (NS_UNLIKELY(diff > 0.0001 || diff < -0.0001)) {
    equals = PR_FALSE;
    nsCAutoString msg("  'balance.amount' is wrong: ");
    AppendDouble(msg, sum);
    NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
  }

  node->GetValue(&sum);
  diff = value - sum;
  if (NS_UNLIKELY(diff > 0.0001 || diff < -0.0001)) {
    equals = PR_FALSE;
    nsCAutoString msg("  'balance.value' is wrong: ");
    AppendDouble(msg, sum);
    NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
  }

  PRBool side;
  node->GetSide(&side);
  if (NS_UNLIKELY( side != aSide)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'balance.side' is wrong");
  }
  return equals;
}

XPCOM_API(PRBool)
testIncomeState(nsITestRunner *cxxUnitTestRunner, aaIBalance *node,
    double value, PRBool aSide)
{ 
  nsresult rv;
  PRBool equals = PR_TRUE;
  if (NS_UNLIKELY( ! node )) {
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'income state' is null");
    return PR_FALSE;
  }
  nsCOMPtr<aaIFlow> flow;
  rv = node->GetFlow(getter_AddRefs( flow ));
  PRInt64 id;
  if (NS_LIKELY( flow )) {
    flow->GetId(&id);
    nsCAutoString msg("  'income state.flow' is wrong - ");
    AppendInt(msg, id);
    if (NS_UNLIKELY( id )) {
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
    }
  } else {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'income state.flow' is null");
  }

  nsCOMPtr<aaIResource> resource;
  rv = node->GetResource(getter_AddRefs( resource ));
  if (NS_UNLIKELY( resource )) {
    resource->GetId(&id);
    nsCAutoString msg("  'income state.resource' is not null -" );
    AppendInt(msg, id);
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
  }

  double sum, diff;
  node->GetAmount(&sum);
  diff = sum;
  if (!isZero(diff)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'income state.amount' is wrong");
  }

  node->GetValue(&sum);
  diff = value - sum;
  if (NS_UNLIKELY(diff > 0.0001 || diff < -0.0001)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'income state.value' is wrong");
  }

  PRBool side;
  node->GetSide(&side);
  if (NS_UNLIKELY( side != aSide)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'income state.side' is wrong");
  }
  return equals;
}

XPCOM_API(PRBool)
testState(nsITestRunner *cxxUnitTestRunner, aaIState *node, PRInt64 flowId,
    PRInt64 resourceId, double sum)
{
  NS_TEST_ASSERT_MSG(node, " 'state' is null");
  if (!node)
    return PR_FALSE;
  nsresult rv;
  PRBool equals = PR_TRUE;
  nsCOMPtr<aaIFlow> flow;
  rv = node->GetFlow(getter_AddRefs( flow ));
  NS_TEST_ASSERT_MSG(flow, " 'state.flow' is null");
  PRInt64 id;
  nsAutoString tag;
  if (NS_LIKELY( flow )) {
    flow->GetId(&id);
    if (NS_UNLIKELY( id != flowId) ) {
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, " 'state.flow.id' is wrong");
    }
  } else {
    equals = PR_FALSE;
  }

  nsCOMPtr<aaIResource> resource;
  rv = node->GetResource(getter_AddRefs( resource ));
  NS_TEST_ASSERT_MSG(resource, " 'state.resource' is null" );
  if (NS_LIKELY( resource )) {
    resource->GetId(&id);
    if (NS_UNLIKELY( id != resourceId) ) {
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, " 'state.resource.id' is wrong");
    }
  } else {
    equals = PR_FALSE;
  }

  double amount, diff;
  rv = node->GetAmount(&amount);
  diff = amount - sum;
  if (! isZero(diff)) {
    equals = PR_FALSE;
    nsCAutoString msg(" 'state.amount' is wrong: ");
    AppendDouble(msg, amount);
    NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
  }

  return equals;
}

XPCOM_API(PRBool)
testRule(nsITestRunner *cxxUnitTestRunner, aaIRule *node, PRInt64 fromFlowId,
    PRInt64 toFlowId, PRBool aFactSide, PRBool aChangeSide, double rate,
    const nsAString &tag)
{
  NS_TEST_ASSERT_MSG(node, " 'rule' is null");
  if (!node)
    return PR_FALSE;
  PRBool equals = PR_TRUE;
  nsCOMPtr<aaIFlow> flow;
  PRInt64 id;

  node->GetTakeFrom(getter_AddRefs(flow));
  if (flow) {
    flow->GetId(&id);
    if (NS_UNLIKELY(id != fromFlowId)) {
      nsCAutoString msg("  'rule.takeFrom.id' is wrong - ");
      AppendInt(msg, id);
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
    }
  }
  else if (NS_UNLIKELY(fromFlowId != 0)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'rule.takeFrom' is null");
  }

  node->GetGiveTo(getter_AddRefs(flow));
  if (flow) {
    flow->GetId(&id);
    if (NS_UNLIKELY(id != toFlowId)) {
      nsCAutoString msg("  'rule.giveTo.id' is wrong - ");
      AppendInt(msg, id);
      equals = PR_FALSE;
      NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
    }
  }
  else if (NS_UNLIKELY(toFlowId != 0)) {
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, "  'rule.giveTo' is null");
  }

  double value;
  node->GetRate(&value);
  if (! isZero(value - rate)) {
    nsCAutoString msg("  'rule.rate' is wrong - ");
    AppendDouble(msg, value);
    equals = PR_FALSE;
    NS_TEST_ASSERT_MSG(PR_FALSE, msg.get());
  }

  PRBool factSide, changeSide;
  node->GetFactSide(&factSide);
  node->GetChangeSide(&changeSide);
  if (NS_UNLIKELY(factSide != aFactSide)) {
    NS_TEST_ASSERT_MSG(factSide == aFactSide, " 'rule' wrong fact side");
    equals = PR_FALSE;
  }
  if (NS_UNLIKELY(changeSide != aChangeSide)) {
    NS_TEST_ASSERT_MSG(changeSide == aChangeSide, " 'rule' wrong change side");
    equals = PR_FALSE;
  }

  nsAutoString nodeTag;
  node->GetTag(nodeTag);
  if (NS_UNLIKELY(nodeTag != tag)) {
    NS_TEST_ASSERT_MSG(nodeTag == tag, " 'rule' wrong tag state");
    equals = PR_FALSE;
  }

  return equals;
}
