/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIGenericFactory.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"
#include "nsIClassInfoImpl.h"

/* Project includes */
#include "aaParamFactory.h"
#include "aaTimeFrame.h"
#include "aaEntity.h"
#include "aaAsset.h"
#include "aaMoney.h"
#include "aaFlow.h"
#include "aaFact.h"
#include "aaState.h"
#include "aaEvent.h"
#include "aaRule.h"
#include "aaQuote.h"
#include "aaTransaction.h"
#include "aaIncomeFlow.h"
#include "aaIncome.h"
#include "aaBaseKeys.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(aaTimeFrame)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaAsset)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaMoney)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaFact)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaState)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaEvent)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaRule)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaQuote)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaTransaction)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncomeFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR(aaIncome)

NS_DECL_CLASSINFO(aaAsset)
NS_DECL_CLASSINFO(aaMoney)
NS_DECL_CLASSINFO(aaEvent)
NS_DECL_CLASSINFO(aaRule)
NS_DECL_CLASSINFO(aaQuote)
NS_DECL_CLASSINFO(aaIncome)

static const nsModuleComponentInfo components[] =
{
  { "Time Frame",
    AA_TIMEFRAME_CID,
    AA_TIMEFRAME_CONTRACT,
    aaTimeFrameConstructor}
  ,{
    "Entity",
    AA_ENTITY_CID,
    AA_ENTITY_CONTRACT,
    aaEntityConstructor
  }
  ,{
    "Asset",
    AA_ASSET_CID,
    AA_ASSET_CONTRACT,
    aaAssetConstructor
    ,NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaAsset),
    NULL,
    &NS_CLASSINFO_NAME(aaAsset)
  }
  ,{
    "Money",
    AA_MONEY_CID,
    AA_MONEY_CONTRACT,
    aaMoneyConstructor
    ,NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaMoney),
    NULL,
    &NS_CLASSINFO_NAME(aaMoney)
  }
  ,{
    "Flow",
    AA_FLOW_CID,
    AA_FLOW_CONTRACT,
    aaFlowConstructor
  }
  ,{
    "Fact",
    AA_FACT_CID,
    AA_FACT_CONTRACT,
    aaFactConstructor
  }
  ,{
    "State",
    AA_STATE_CID,
    AA_STATE_CONTRACT,
    aaStateConstructor
  }
  ,{
    "Event",
    AA_EVENT_CID,
    AA_EVENT_CONTRACT,
    aaEventConstructor,
    NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaEvent),
    NULL,
    &NS_CLASSINFO_NAME(aaEvent)
  }
  ,{
    "Rule",
    AA_RULE_CID,
    AA_RULE_CONTRACT,
    aaRuleConstructor,
    NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaRule),
    NULL,
    &NS_CLASSINFO_NAME(aaRule)
  }
  ,{
    "Quote",
    AA_QUOTE_CID,
    AA_QUOTE_CONTRACT,
    aaQuoteConstructor
    ,NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaQuote),
    NULL,
    &NS_CLASSINFO_NAME(aaQuote)
  }
  ,{
    "Transaction",
    AA_TRANSACTION_CID,
    AA_TRANSACTION_CONTRACT,
    aaTransactionConstructor
  }
  ,{
    "Income Flow",
    AA_INCOMEFLOW_CID,
    AA_INCOMEFLOW_CONTRACT,
    aaIncomeFlowConstructor
  }
  ,{
    "Income",
    AA_INCOME_CID,
    AA_INCOME_CONTRACT,
    aaIncomeConstructor
    ,NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaIncome),
    NULL,
    &NS_CLASSINFO_NAME(aaIncome)
  }
};
NS_IMPL_NSGETMODULE(aabase, components)

