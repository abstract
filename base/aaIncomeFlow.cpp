/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsServiceManagerUtils.h"

/* Unfrozen API */
#include "nsIStringBundle.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIHandler.h"
#include "aaIncomeFlow.h"

aaIncomeFlow::aaIncomeFlow()
{
}

aaIncomeFlow::~aaIncomeFlow()
{
}

NS_IMPL_ISUPPORTS3(aaIncomeFlow,
                   aaIDataNode,
                   aaIListNode,
                   aaIFlow)
/* aaIDataNode */
NS_IMETHODIMP
aaIncomeFlow::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = 0;
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetId(PRInt64 aId)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaIncomeFlow::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return aQuery->HandleFlow(this);
}

NS_IMETHODIMP
aaIncomeFlow::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = PR_FALSE;
  return NS_OK;
}

NS_IMETHODIMP
aaIncomeFlow::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIFlow */
PRInt64
aaIncomeFlow::PickId()
{
  return 0;
}

NS_IMETHODIMP
aaIncomeFlow::GetTag(nsAString & aTag)
{
  nsresult rv;
  nsCOMPtr<nsIStringBundleService> heap = do_GetService(
      NS_STRINGBUNDLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIStringBundle> bundle;
  rv = heap->CreateBundle("chrome://abstract/locale/abstract.properties",
      getter_AddRefs( bundle ));
  NS_ENSURE_SUCCESS(rv, rv);

  nsAutoString w;
  rv = bundle->GetStringFromName(NS_LITERAL_STRING("terms.profits").get(), 
      getter_Copies(w));
  NS_ENSURE_SUCCESS(rv, rv);

  aTag.Assign(w);
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetTag(const nsAString & aTag)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaIncomeFlow::GetEntity(aaIEntity ** aEntity)
{
  NS_ENSURE_ARG_POINTER(aEntity);
  *aEntity = nsnull;
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetEntity(aaIEntity * aEntity)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaIncomeFlow::GetGiveResource(aaIResource ** aGiveResource)
{
  NS_ENSURE_ARG_POINTER(aGiveResource);
  *aGiveResource = nsnull;
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetGiveResource(aaIResource * aGiveResource)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
aaIResource*
aaIncomeFlow::PickGiveResource()
{
  return nsnull;
}

NS_IMETHODIMP
aaIncomeFlow::GetTakeResource(aaIResource ** aTakeResource)
{
  NS_ENSURE_ARG_POINTER(aTakeResource);
  *aTakeResource = nsnull;
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetTakeResource(aaIResource * aTakeResource)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
aaIResource*
aaIncomeFlow::PickTakeResource()
{
  return nsnull;
}

NS_IMETHODIMP
aaIncomeFlow::GetRate(double *aRate)
{
  NS_ENSURE_ARG_POINTER(aRate);
  *aRate = 1.0;
  return NS_OK;
}
NS_IMETHODIMP
aaIncomeFlow::SetRate(double aRate)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaIncomeFlow::GetLimit(double *aLimit)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaIncomeFlow::SetLimit(double aLimit)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaIncomeFlow::GetIsOffBalance(PRBool *aIsOffBalance)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaIncomeFlow::SetIsOffBalance(PRBool aIsOffBalance)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
PRBool
aaIncomeFlow::PickIsOffBalance()
{
  return PR_TRUE;
}

NS_IMETHODIMP
aaIncomeFlow::StoreRule(aaIRule *aRule)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
