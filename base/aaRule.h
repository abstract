/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AARULE_H
#define AARULE_H 1

#define AA_RULE_CID \
{0x0c14aebd, 0x2b15, 0x4bda, {0x9d, 0x33, 0x95, 0xf0, 0x1b, 0xb2, 0xaf, 0x56}}

#include "nsIWeakReferenceUtils.h"
#include "aaIRule.h"

class aaIFlow;
#ifdef DEBUG
#include "aaIFlow.h"
#endif

class aaRule : public aaIRule
{
public:
  aaRule();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIRULE

private:
  ~aaRule() {}
  friend class aaBaseTest;

  PRBool mEdited;
  nsWeakPtr mFlow;
  PRInt64 mId;
  PRBool mChangeSide;
  PRBool mFactSide;
  nsCOMPtr<aaIFlow> mTakeFrom;
  nsCOMPtr<aaIFlow> mGiveTo;
  double mRate;
  nsAutoString mTag;
};

#endif /* AARULE_H */

