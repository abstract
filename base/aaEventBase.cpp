/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"

/* Project includes */
#include "aaIRule.h"
#include "aaIFact.h"
#include "aaIHandler.h"
#include "aaIStorager.h"

#include "aaEventBase.h"
#include "aaEvent.h"


aaEventBase::aaEventBase(aaEvent *aParent)
  :mParent(aParent), mId(0), mTag((const PRUnichar *) nsnull),
  mHasTime(PR_FALSE), mRead(PR_FALSE), mEdited(PR_FALSE)
{
}

aaEventBase::~aaEventBase()
{
}

NS_IMPL_ISUPPORTS5(aaEventBase,
                   aaIDataNode,
                   aaIListNode,
                   aaIEvent,
                   nsIArray,
                   nsIMutableArray)
/* aaIDataNode */
NS_IMETHODIMP
aaEventBase::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaEventBase::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}
PRInt64
aaEventBase::PickId()
{
  return mId;
}

NS_IMETHODIMP
aaEventBase::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  nsresult rv;
  rv = aQuery->HandleEvent(this);
  if (NS_FAILED(rv)) return rv;

  PRInt32 i;
  for (i = 0; i < mFacts.Count(); i++) {
    rv = mFacts[i]->Accept(aQuery);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
aaEventBase::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = mEdited;
  return NS_OK;
}

NS_IMETHODIMP
aaEventBase::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  nsresult rv;
  if (mId) {
    rv = aStorager->Delete(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = aStorager->Insert(aChannel);
    if (NS_FAILED(rv)) return rv;
  }

  return NS_OK;
}

/* aaIEvent */
NS_IMETHODIMP
aaEventBase::GetTag(nsAString & aTag)
{
  aTag.Assign(mTag);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaEventBase::SetTag(const nsAString & aTag)
{
  if (mTag.Equals(aTag))
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mTag.Assign(aTag);
  return NS_OK;
}

NS_IMETHODIMP
aaEventBase::GetTime(PRTime *aTime)
{
  NS_ENSURE_TRUE( mHasTime, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER( aTime );
  *aTime = mTime;
  return NS_OK;
}
NS_IMETHODIMP
aaEventBase::SetTime(PRTime aTime)
{
  PRExplodedTime tm;
  PR_ExplodeTime(aTime, &PR_LocalTimeParameters, &tm);
  tm.tm_params.tp_gmt_offset = 0;
  tm.tm_params.tp_dst_offset = 0;
  tm.tm_hour = 12;
  tm.tm_min = 0;
  tm.tm_sec = 0;
  tm.tm_usec = 0;
  mTime = PR_ImplodeTime(&tm);
  mHasTime = PR_TRUE;
  return NS_OK;
}

/* nsIArray */
NS_IMETHODIMP
aaEventBase::GetLength(PRUint32 *aLength)
{
  NS_ENSURE_ARG_POINTER( aLength );
  *aLength = mFacts.Count();
  return NS_OK;
}

NS_IMETHODIMP
aaEventBase::QueryElementAt(PRUint32 index, const nsIID & uuid, void * *result)
{
  NS_ENSURE_TRUE( index < (PRUint32) mFacts.Count(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_ARG_POINTER( result );
  return mFacts[index]->QueryInterface(uuid, result);
}

NS_IMETHODIMP
aaEventBase::IndexOf(PRUint32 startIndex, nsISupports *element, PRUint32 *_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaEventBase::Enumerate(nsISimpleEnumerator **_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* nsIMutableArray */
NS_IMETHODIMP
aaEventBase::AppendElement(nsISupports *element, PRBool weak)
{
  NS_ENSURE_TRUE(!weak, NS_ERROR_NOT_IMPLEMENTED);
  nsCOMPtr<aaIFact> fact = do_QueryInterface(element);
  NS_ENSURE_TRUE(fact, NS_ERROR_INVALID_ARG);

  nsresult rv;
  rv = fact->SetEvent(mParent);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool res = mFacts.AppendObject(fact);
  NS_ENSURE_TRUE(res, NS_ERROR_FAILURE);

  return NS_OK;
}

NS_IMETHODIMP
aaEventBase::RemoveElementAt(PRUint32 index)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaEventBase::InsertElementAt(nsISupports *element, PRUint32 index, PRBool weak)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaEventBase::ReplaceElementAt(nsISupports *element, PRUint32 index, PRBool weak)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaEventBase::Clear()
{
  mFacts.Clear();
  return NS_OK;
}

