/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"

/* Project includes */
#include "aaTimeFrame.h"

aaTimeFrame::aaTimeFrame()
  :mStatus(0)
{
  mStart = mEnd = PR_Now();
}

aaTimeFrame::~aaTimeFrame()
{
}

NS_IMPL_ISUPPORTS1(aaTimeFrame,
                   aaITimeFrame)

/* aaITimeFrame */
NS_IMETHODIMP
aaTimeFrame::GetStart(PRTime *aStart)
{
  NS_ENSURE_ARG_POINTER(aStart);
  *aStart = mStart;
  return NS_OK;
}
NS_IMETHODIMP
aaTimeFrame::SetStart(PRTime aStart)
{
  mStart = aStart;
  mStatus |= HAS_START;
  if (mEnd < mStart) {
    mEnd = mStart;
    mStatus &= ~HAS_END;
  }
  return NS_OK;
}
PRTime
aaTimeFrame::PickStart()
{
  return mStart;
}

NS_IMETHODIMP
aaTimeFrame::GetEnd(PRTime *aEnd)
{
  NS_ENSURE_ARG_POINTER(aEnd);
  *aEnd = mEnd;
  return NS_OK;
}
NS_IMETHODIMP
aaTimeFrame::SetEnd(PRTime aEnd)
{
  mEnd = aEnd;
  mStatus |= HAS_END;
  if (mEnd < mStart) {
    mStart = mEnd;
    mStatus &= ~HAS_START;
  }
  return NS_OK;
}
PRTime
aaTimeFrame::PickEnd()
{
  return mEnd;
}

NS_IMETHODIMP
aaTimeFrame::GetStatus(PRUint32 *aStatus)
{
  NS_ENSURE_ARG_POINTER(aStatus);
  *aStatus = mStatus;
  return NS_OK;
}
NS_IMETHODIMP
aaTimeFrame::SetStatus(PRUint32 aStatus)
{
  mStatus = aStatus;
  if (mStatus < mStart)
    mStart = mStatus;
  return NS_OK;
}
PRUint32
aaTimeFrame::PickStatus()
{
  return mStatus;
}
