/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAENTITY_H
#define AAENTITY_H 1

#define AA_ENTITY_CID \
{0xa8d4d720, 0xf998, 0x4e32, {0x90, 0xed, 0x23, 0xcc, 0x75, 0xc9, 0x38, 0x4c}}

#include "aaIEntity.h"

class aaEntity : public aaIEntity
{
public:
  aaEntity();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIENTITY

private:
  virtual ~aaEntity();
  friend class aaBaseTest;

  PRInt64 mId;
  nsAutoString mTag;
  PRBool mRead;
  PRBool mEdited;
};

#endif /* AAENTITY_H */

