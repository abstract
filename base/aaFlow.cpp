/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsStringAPI.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIResource.h"
#include "aaIHandler.h"
#include "aaIRule.h"
#include "aaFlow.h"

aaFlow::aaFlow()
  : mId(-1)
  , mTag((const PRUnichar *) nsnull)
  , mRead(PR_FALSE)
  , mEdited(PR_FALSE)
  , mRate(1.0)
  , mLimit(0.0)
  , mIsOffBalance(PR_FALSE)
{
}

aaFlow::~aaFlow()
{
}

NS_IMPL_ISUPPORTS5(aaFlow,
                   nsISupportsWeakReference,
                   nsIArray,
                   aaIDataNode,
                   aaIListNode,
                   aaIFlow)
/* aaIDataNode */
NS_IMETHODIMP
aaFlow::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  nsresult rv;
  rv =  aQuery->HandleFlow(this);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt32 i;
  for (i = 0; i < mRules.Count(); i++) {
    rv = mRules[i]->Accept(aQuery);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
aaFlow::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = mEdited;
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIFlow */
PRInt64
aaFlow::PickId()
{
  return mId;
}

NS_IMETHODIMP
aaFlow::GetTag(nsAString & aTag)
{
  aTag.Assign(mTag);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetTag(const nsAString & aTag)
{
  if (mTag.Equals(aTag))
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mTag.Assign(aTag);
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::GetEntity(aaIEntity ** aEntity)
{
  NS_ENSURE_ARG_POINTER(aEntity);
  NS_IF_ADDREF(*aEntity = mEntity);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetEntity(aaIEntity * aEntity)
{
  if (mEntity == aEntity)
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mEntity = aEntity;
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::GetGiveResource(aaIResource ** aGiveResource)
{
  NS_ENSURE_ARG_POINTER(aGiveResource);
  NS_IF_ADDREF(*aGiveResource = mGiveResource);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetGiveResource(aaIResource * aGiveResource)
{
  if (mGiveResource == aGiveResource)
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mGiveResource = aGiveResource;
  return NS_OK;
}
aaIResource*
aaFlow::PickGiveResource()
{
  return mGiveResource;
}

NS_IMETHODIMP
aaFlow::GetTakeResource(aaIResource ** aTakeResource)
{
  NS_ENSURE_ARG_POINTER(aTakeResource);
  NS_IF_ADDREF(*aTakeResource = mTakeResource);
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetTakeResource(aaIResource * aTakeResource)
{
  if (mTakeResource == aTakeResource)
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mTakeResource = aTakeResource;
  return NS_OK;
}
aaIResource*
aaFlow::PickTakeResource()
{
  return mTakeResource;
}

NS_IMETHODIMP
aaFlow::GetRate(double *aRate)
{
  NS_ENSURE_ARG_POINTER(aRate);
  *aRate = mRate;
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetRate(double aRate)
{
  if (mRate == aRate)
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mRate = aRate;
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::GetLimit(double *aLimit)
{
  NS_ENSURE_ARG_POINTER(aLimit);
  *aLimit = mLimit;
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetLimit(double aLimit)
{
  if (mLimit == aLimit)
    return NS_OK;
  if (mRead)
    mEdited = PR_TRUE;
  mLimit = aLimit;
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::GetIsOffBalance(PRBool *aIsOffBalance)
{
  NS_ENSURE_ARG_POINTER(aIsOffBalance);
  *aIsOffBalance = mIsOffBalance;
  mRead = PR_TRUE;
  return NS_OK;
}
NS_IMETHODIMP
aaFlow::SetIsOffBalance(PRBool aIsOffBalance)
{
  if (mIsOffBalance == aIsOffBalance)
    return NS_OK;
  if (mIsOffBalance)
    mIsOffBalance = PR_TRUE;
  mIsOffBalance = aIsOffBalance;
  return NS_OK;
}
PRBool
aaFlow::PickIsOffBalance()
{
  return mIsOffBalance;
}

NS_IMETHODIMP
aaFlow::StoreRule(aaIRule *aRule)
{
  nsresult rv;
  nsCOMPtr<aaIFlow> flow;

  rv = aRule->GetFlow(getter_AddRefs(flow));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(!flow || flow == (aaIFlow *) this, NS_ERROR_INVALID_ARG);

  if (!flow) {
    rv = aRule->SetFlow(this);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  if (!aRule->PickId()) {
    rv = mRules.AppendObject(aRule);
    NS_ENSURE_TRUE(rv, NS_ERROR_FAILURE);
    rv = aRule->SetId(mRules.Count());
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    rv = mRules.ReplaceObjectAt(aRule, (PRUint32) aRule->PickId() - 1);
    NS_ENSURE_TRUE(rv, NS_ERROR_FAILURE);
  }

  return NS_OK;
}

/* nsIArray */
NS_IMETHODIMP
aaFlow::GetLength(PRUint32 *aLength)
{
  NS_ENSURE_ARG_POINTER( aLength );
  *aLength = mRules.Count();
  return NS_OK;
}

NS_IMETHODIMP
aaFlow::QueryElementAt(PRUint32 index, const nsIID & uuid, void * *result)
{
  NS_ENSURE_TRUE( index < (PRUint32) mRules.Count(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_ARG_POINTER( result );
  return mRules[index]->QueryInterface(uuid, result);
}

NS_IMETHODIMP
aaFlow::IndexOf(PRUint32 startIndex, nsISupports *element, PRUint32 *_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaFlow::Enumerate(nsISimpleEnumerator **_retval)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

