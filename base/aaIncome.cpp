/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIFlow.h"
#include "aaIResource.h"
#include "aaIFact.h"
#include "aaIQuote.h"
#include "aaITransaction.h"
#include "aaIStorager.h"
#include "aaBaseKeys.h"
#include "aaIncome.h"

aaIncome::aaIncome()
  :mSide(0), mCreditDiff(0.0), mDebitDiff(0.0), mStatus(PR_FALSE),
  mStart(LL_MININT), mHasPrevTime(PR_FALSE)
{
  mFlow = do_CreateInstance(AA_INCOMEFLOW_CONTRACT);
}

aaIncome::~aaIncome()
{
}

NS_IMPL_ISUPPORTS4_CI(aaIncome,
                      aaIDataNode,
                      aaIState,
                      aaIBalance,
                      aaIIncome)
/* aaIDataNode */
NS_IMETHODIMP
aaIncome::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}

NS_IMETHODIMP
aaIncome::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaIncome::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaIncome::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  nsresult rv;

  if (mStatus < NEW)
    return NS_ERROR_NOT_INITIALIZED;

  if (mStatus == REPLACE) {
    rv = aStorager->Close(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else if (mStatus == UPDATE) {
    if (isZero(PickValue()))
      return aStorager->Delete(aChannel);
    else
      return aStorager->Update(aChannel);
  }

  if (!isZero(PickValue())) {
    rv = aStorager->Insert(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

/* aaIIncome */
NS_IMETHODIMP
aaIncome::GetFlow(aaIFlow* *aFlow)
{
  NS_ENSURE_ARG_POINTER(aFlow);
  NS_IF_ADDREF(*aFlow = mFlow);
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetFlow(aaIFlow* aFlow)
{
  return NS_ERROR_NOT_AVAILABLE;
}
aaIFlow*
aaIncome::PickFlow()
{
  return mFlow;
}

NS_IMETHODIMP
aaIncome::GetResource(aaIResource* *aResource)
{
  NS_ENSURE_ARG_POINTER(aResource);
  *aResource = nsnull;
  return NS_OK;
}

NS_IMETHODIMP
aaIncome::GetSide(PRBool *aSide)
{
  NS_ENSURE_ARG_POINTER(aSide);
  *aSide = mSide;
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetSide(PRBool aSide)
{
  mSide = aSide;
  return NS_OK;
}
PRBool
aaIncome::PickSide()
{
  return mSide;
}

NS_IMETHODIMP
aaIncome::GetAmount(double *aAmount)
{
  NS_ENSURE_ARG_POINTER(aAmount);
  *aAmount = PickAmount();
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetAmount(double aAmount)
{
  NS_ENSURE_TRUE(isZero(aAmount), NS_ERROR_INVALID_ARG);
  return NS_OK;
}
double
aaIncome::PickAmount()
{
  return 0.0;
}

NS_IMETHODIMP
aaIncome::GetStart(PRTime *aStart)
{
  NS_ENSURE_ARG_POINTER(aStart);
  *aStart = PickStart();
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetStart(PRTime aStart)
{
  mStart = aStart;
  mStatus = LOADED;
  return NS_OK;
}
PRTime
aaIncome::PickStart()
{
  return mStart;
}

NS_IMETHODIMP
aaIncome::GetEnd(PRTime *aEnd)
{
  NS_ENSURE_ARG_POINTER(aEnd);
  *aEnd = mEnd;
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetEnd(PRTime aEnd)
{
  mEnd = aEnd;
  mStatus = PAID;
  return NS_OK;
}
PRTime
aaIncome::PickEnd()
{
  return mEnd;
}

NS_IMETHODIMP
aaIncome::GetPrevTime(PRTime *aPrevTime )
{ 
  NS_ENSURE_ARG_POINTER(aPrevTime);
  NS_ENSURE_TRUE(mHasPrevTime, NS_ERROR_NOT_INITIALIZED);
  *aPrevTime = mPrevTime;
  return NS_OK;
}

NS_IMETHODIMP
aaIncome::GetIsPaid(PRBool *aIsPaid)
{
  NS_ENSURE_ARG_POINTER(aIsPaid);
  *aIsPaid = PickIsPaid();
  return NS_OK;
}
PRBool
aaIncome::PickIsPaid()
{
  return (mStatus == PAID);
}

NS_IMETHODIMP
aaIncome::ApplyFact(aaIFact *aFact)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaIncome::GetEventId(PRInt64 *aEventId)
{
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::GetFactId(PRInt64 *aFactId)
{
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::GetFactSide(PRBool *aFactSide)
{
  return NS_OK;
}

NS_IMETHODIMP
aaIncome::GetValue(double *aValue)
{
  NS_ENSURE_ARG_POINTER(aValue);
  *aValue = PickValue();
  return NS_OK;
}
NS_IMETHODIMP
aaIncome::SetInitValue(double aValue)
{
  mValue = aValue;
  return NS_OK;
}
double
aaIncome::PickValue()
{
  if (!mFlow || !mFlow->PickId())
    return mValue;

  return mValue + PickCreditDiff() + PickDebitDiff();
}

NS_IMETHODIMP
aaIncome::GetCreditDiff(double *aDiff)
{
  NS_ENSURE_ARG_POINTER(aDiff);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  *aDiff = PickCreditDiff();
  return NS_OK;
}
double
aaIncome::PickCreditDiff()
{
  return mCreditDiff;
}
NS_IMETHODIMP
aaIncome::SetCreditDiff(double aCreditDiff)
{
  mCreditDiff = aCreditDiff;
  return NS_OK;
}


NS_IMETHODIMP
aaIncome::GetDebitDiff(double *aDiff)
{
  NS_ENSURE_ARG_POINTER(aDiff);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  *aDiff = PickDebitDiff();
  return NS_OK;
}
double
aaIncome::PickDebitDiff()
{
  return mDebitDiff;
}
NS_IMETHODIMP
aaIncome::SetDebitDiff(double aDebitDiff)
{
  mDebitDiff = aDebitDiff;
  return NS_OK;
}

NS_IMETHODIMP
aaIncome::ApplyQuote(aaIQuote *aQuote)
{
  return update(aQuote->PickTime(), aQuote->PickDiff());
}

NS_IMETHODIMP
aaIncome::ApplyTransaction(aaITransaction *aTransaction, double *aChange,
    PRBool *aHasChange)
{
  NS_ENSURE_TRUE(aTransaction->PickFact(), NS_ERROR_INVALID_ARG);

  nsresult rv;
  double earnings;
  rv = aTransaction->GetEarnings(&earnings);
  NS_ENSURE_SUCCESS(rv, rv);

  return update(aTransaction->PickFact()->PickTime(), earnings);
}

nsresult
aaIncome::update(PRTime time, double change)
{
  if (NS_UNLIKELY(mStatus == PAID || mStart > time))
    return NS_ERROR_INVALID_ARG;

  if (!mStatus) {
    mStatus = NEW;
    mStart = time;
    if (change > 0.0) {
      mValue = change;
      mSide = 0;
    }
    else {
      mValue = -change;
      mSide = 1;
    }

    return NS_OK;
  }

  mValue += mSide ? -change : change;
  if (mValue < 0.0) {
    mValue = -mValue;
    mSide = !mSide;
  }

  if (mStart == time) {
    mStatus = UPDATE;
    return NS_OK;
  }

  mStatus = REPLACE;
  mPrevTime = mStart;
  mHasPrevTime = PR_TRUE;
  mStart = time;
  return NS_OK;
}
