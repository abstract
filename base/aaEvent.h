/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAEVENT_H
#define AAEVENT_H 1

#define AA_EVENT_CID \
{0x77b7edfe, 0x09ef, 0x4990, {0x8d, 0xb3, 0x6e, 0x74, 0x59, 0x22, 0x12, 0xa4}}

#include "nsWeakReference.h"
#include "nsAutoPtr.h"
#include "aaIEvent.h"

class aaEventBase;
#include "aaEventBase.h"

class aaEvent : public aaIEvent,
                public nsIMutableArray,
                public nsSupportsWeakReference
{
public:
  aaEvent();
  NS_DECL_ISUPPORTS
  NS_FORWARD_SAFE_AAIDATANODE(mState)
  NS_FORWARD_SAFE_AAILISTNODE(mState)
  NS_FORWARD_SAFE_AAIEVENT(mState)
  NS_FORWARD_SAFE_NSIARRAY(mState)
  NS_FORWARD_SAFE_NSIMUTABLEARRAY(mState)

  void SetState(aaEventBase *aState);
private:
  ~aaEvent();
  friend class aaBaseTest;

  nsRefPtr<aaEventBase> mState;
};

#endif /* AAEVENT_H */
