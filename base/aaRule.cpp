/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsMemory.h"
#include "nsIClassInfoImpl.h"
#include "nsCOMPtr.h"
#include "nsIWeakReference.h"
#include "nsStringAPI.h"

/* Project includes */
#include "aaIFlow.h"
#include "aaIHandler.h"
#include "aaRule.h"

aaRule::aaRule()
  : mEdited(PR_FALSE)
  , mId(0)
  , mChangeSide(PR_FALSE)
  , mFactSide(PR_FALSE)
  , mRate(0.0)
  , mTag((const PRUnichar*)nsnull)
{
}

NS_IMPL_ISUPPORTS3_CI(aaRule,
                      aaIDataNode,
                      aaIListNode,
                      aaIRule)
/* aaIDataNode */
NS_IMETHODIMP
aaRule::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetId(PRInt64 aId)
{
  NS_ENSURE_TRUE(aId, NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(!mId, NS_ERROR_ALREADY_INITIALIZED);
  mId = aId;
  mEdited = PR_FALSE;
  return NS_OK;
}
PRInt64
aaRule::PickId()
{
  return mId;
}

NS_IMETHODIMP
aaRule::Accept(aaIHandler* aQuery)
{
  return aQuery->HandleRule(this);
}

NS_IMETHODIMP
aaRule::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = mEdited;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_AVAILABLE;
}

/* aaIRule */
NS_IMETHODIMP aaRule::GetTag(nsAString & aTag)
{
  aTag.Assign(mTag);
  return NS_OK;
}
NS_IMETHODIMP aaRule::SetTag(const nsAString & aTag)
{
  if (mTag.Equals(aTag))
    return NS_OK;
  mEdited = PR_TRUE;
  mTag.Assign(aTag);
  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetFlow(aaIFlow * * aFlow)
{
  NS_ENSURE_ARG_POINTER(aFlow);
  if (!mFlow) {
    *aFlow = nsnull;
    return NS_OK;
  }

  nsresult rv;
  nsCOMPtr<aaIFlow> flow = do_QueryReferent(mFlow, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_IF_ADDREF(*aFlow = flow);
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetFlow(aaIFlow *aFlow)
{
  nsresult rv;
  mFlow = do_GetWeakReference(aFlow, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetChangeSide(PRBool *aChangeSide)
{
  NS_ENSURE_ARG_POINTER(aChangeSide);
  *aChangeSide = mChangeSide;
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetChangeSide(PRBool aChangeSide)
{
  if (!mEdited && mChangeSide != aChangeSide)
    mEdited = PR_TRUE;
  mChangeSide = aChangeSide;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetFactSide(PRBool *aFactSide)
{
  NS_ENSURE_ARG_POINTER(aFactSide);
  *aFactSide = mFactSide;
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetFactSide(PRBool aFactSide)
{
  if (!mEdited && mFactSide != aFactSide)
    mEdited = PR_TRUE;
  mFactSide = aFactSide;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetTakeFrom(aaIFlow * * aTakeFrom)
{
  NS_ENSURE_ARG_POINTER(aTakeFrom);
  NS_IF_ADDREF(*aTakeFrom = mTakeFrom);
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetTakeFrom(aaIFlow *aTakeFrom)
{
  if (!mEdited && mTakeFrom != aTakeFrom)
    mEdited = PR_TRUE;
  mTakeFrom = aTakeFrom;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetGiveTo(aaIFlow * * aGiveTo)
{
  NS_ENSURE_ARG_POINTER(aGiveTo);
  NS_IF_ADDREF(*aGiveTo = mGiveTo);
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetGiveTo(aaIFlow *aGiveTo)
{
  if (!mEdited && mGiveTo != aGiveTo)
    mEdited = PR_TRUE;
  mGiveTo = aGiveTo;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::GetRate(double *aRate)
{
  NS_ENSURE_ARG_POINTER(aRate);
  *aRate = mRate;
  return NS_OK;
}
NS_IMETHODIMP
aaRule::SetRate(double aRate)
{
  if (!mEdited && mRate != aRate)
    mEdited = PR_TRUE;
  mRate = aRate;
  return NS_OK;
}

NS_IMETHODIMP
aaRule::Validate(void)
{
  nsresult rv;
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  nsCOMPtr<aaIFlow> flow = do_QueryReferent(mFlow, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  NS_ENSURE_TRUE(flow->PickId(), NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(mId, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(mRate != 0.0, NS_ERROR_NOT_INITIALIZED);

  return NS_OK;
}
