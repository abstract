/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include <stdio.h>
#include "nsStringAPI.h"
#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"
#include "nsIMutableArray.h"
#include "nsCOMArray.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Unfrozen API */
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaIHandler.h"
#include "aaITransaction.h"
#include "aaAsset.h"
#include "aaMoney.h"
#include "aaEntity.h"
#include "aaFlow.h"
#include "aaFact.h"
#include "aaState.h"
#include "aaEvent.h"
#include "aaRule.h"
#include "aaIncomeFlow.h"
#include "aaBaseKeys.h"

#define AA_BASE_TEST_CID \
{0x502c6e1e, 0x42c4, 0x46b1, {0xa4, 0x7b, 0x19, 0x42, 0x8a, 0xd6, 0x75, 0x58}}
#define AA_BASE_TEST_CONTRACT "@aasii.org/base/unit;1"

NS_DECL_CLASSINFO(aaAsset)
NS_DECL_CLASSINFO(aaEvent)

class aaBaseTest: public nsITest,
                  public aaIHandler
{
public:
  aaBaseTest() {;}
  virtual ~aaBaseTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
  NS_DECL_AAIHANDLER
private:
#define AA_BASE_TEST_ENTITY   1
#define AA_BASE_TEST_RESOURCE 2
#define AA_BASE_TEST_ASSET    4
#define AA_BASE_TEST_FLOW     8
#define AA_BASE_TEST_FACT    16
#define AA_BASE_TEST_EVENT   32
#define AA_BASE_TEST_ACCOUNT 64
#define AA_BASE_TEST_MONEY  128
  PRUint32 mPackQueries;
  PRUint32 mPackParam;

  nsresult testEntity(nsITestRunner *aTestRunner);
  nsresult testAsset(nsITestRunner *aTestRunner);
  nsresult testMoney(nsITestRunner *aTestRunner);
  nsresult testFlow(nsITestRunner *aTestRunner);
  nsresult testFact(nsITestRunner *aTestRunner);
  nsresult testState(nsITestRunner *aTestRunner);
  nsresult testEvent(nsITestRunner *aTestRunner);
  nsresult testRule(nsITestRunner *aTestRunner);
  nsresult testIncomeFlow(nsITestRunner *aTestRunner);
};

NS_IMETHODIMP
aaBaseTest::Test(nsITestRunner *aTestRunner)
{
  nsITestRunner *cxxUnitTestRunner = aTestRunner;
  NS_TEST_ASSERT_OK(testEntity(aTestRunner));
  NS_TEST_ASSERT_OK(testAsset(aTestRunner));
  NS_TEST_ASSERT_OK(testMoney(aTestRunner));
  NS_TEST_ASSERT_OK(testFlow(aTestRunner));
  NS_TEST_ASSERT_OK(testEvent(aTestRunner));
  NS_TEST_ASSERT_OK(testFact(aTestRunner));
  NS_TEST_ASSERT_OK(testState(aTestRunner));
  NS_TEST_ASSERT_OK(testRule(aTestRunner));
  NS_TEST_ASSERT_OK(testIncomeFlow(aTestRunner));
  return NS_OK;
} 

NS_IMPL_ISUPPORTS1(aaBaseTest, nsITest)

NS_GENERIC_FACTORY_CONSTRUCTOR(aaBaseTest)

static const nsModuleComponentInfo components[] =
{
  { "Base Module Unit Test",
    AA_BASE_TEST_CID,
    AA_BASE_TEST_CONTRACT,
    aaBaseTestConstructor }
};

NS_IMPL_NSGETMODULE(aabaset, components)

/* aaIHandler */
NS_IMETHODIMP
aaBaseTest::HandleNode(aaIListNode *aNode)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaBaseTest::HandleEntity(aaIEntity *aEntity)
{
  mPackQueries |= AA_BASE_TEST_ENTITY;
  if (aEntity)
    mPackParam |= AA_BASE_TEST_ENTITY;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleResource(aaIResource *aResource)
{
  mPackQueries |= AA_BASE_TEST_RESOURCE;
  if (aResource)
    mPackParam |= AA_BASE_TEST_RESOURCE;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleMoney(aaIMoney *aMoney)
{
  mPackQueries |= AA_BASE_TEST_MONEY;
  if (aMoney)
    mPackParam |= AA_BASE_TEST_MONEY;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleAsset(aaIAsset *aAsset)
{
  mPackQueries |= AA_BASE_TEST_ASSET;
  if (aAsset)
    mPackParam |= AA_BASE_TEST_ASSET;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleFlow(aaIFlow *aFlow)
{
  mPackQueries |= AA_BASE_TEST_FLOW;
  if (aFlow)
    mPackParam |= AA_BASE_TEST_FLOW;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleRule(aaIRule *aRule)
{
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleState(aaIState *aState)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaBaseTest::HandleFact(aaIFact *aFact)
{
  mPackQueries |= AA_BASE_TEST_FACT;
  if (aFact)
    mPackParam |= AA_BASE_TEST_FACT;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleEvent(aaIEvent *aEvent)
{
  mPackQueries |= AA_BASE_TEST_EVENT;
  if (aEvent)
    mPackParam |= AA_BASE_TEST_EVENT;
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleQuote(aaIQuote *aQuote)
{
  return NS_OK;
}

NS_IMETHODIMP
aaBaseTest::HandleTransaction(aaITransaction *aTransaction)
{
  return NS_OK;
}

/* Private methods */
nsresult
aaBaseTest::testEntity(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIEntity> entity(do_CreateInstance(AA_ENTITY_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(entity, "aaEntity instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(entity, &rv));
  NS_TEST_ASSERT_MSG(item, "aaEntity aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_ENTITY,
      "aaEntity->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_ENTITY),
      "aaEntity->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_ENTITY,
      "aaEntity->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  aaEntity* raw = static_cast<aaEntity *>(entity.get());
  nsAutoString wstr(NS_LITERAL_STRING("init"));
  NS_TEST_ASSERT_MSG(! raw->mEdited, "blank aaEntity marked edited");
  entity->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "hidden aaEntity marked edited");
  NS_TEST_ASSERT_MSG(! raw->mRead, "hidden aaEntity marked read");
  entity->GetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "read aaEntity marked edited");
  NS_TEST_ASSERT_MSG(raw->mRead, "read aaEntity not marked read");
  entity->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "unchanged aaEntity marked edited");
  entity->SetTag(NS_LITERAL_STRING("edit"));
  NS_TEST_ASSERT_MSG(raw->mEdited, "changed aaEntity not marked edited");

  return NS_OK;
}

nsresult
aaBaseTest::testAsset(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIAsset> resource(do_CreateInstance(AA_ASSET_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(resource, "aaAsset instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(resource, &rv));
  NS_TEST_ASSERT_MSG(item, "aaAsset aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_ASSET,
      "aaAsset->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_ASSET),
      "aaAsset->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_ASSET,
      "aaAsset->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  aaAsset* raw = static_cast<aaAsset*>(resource.get());
  nsAutoString wstr(NS_LITERAL_STRING("init"));
  NS_TEST_ASSERT_MSG(! raw->mEdited, "blank aaAsset marked edited");
  resource->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "hidden aaAsset marked edited");
  NS_TEST_ASSERT_MSG(! raw->mRead, "hidden aaAsset marked read");
  resource->GetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "read aaAsset marked edited");
  NS_TEST_ASSERT_MSG(raw->mRead, "read aaAsset not marked read");
  resource->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "unchanged aaAsset marked edited");
  resource->SetTag(NS_LITERAL_STRING("edit"));
  NS_TEST_ASSERT_MSG(raw->mEdited, "changed aaAsset not marked edited");

  return NS_OK;
}

nsresult
aaBaseTest::testMoney(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIMoney> money(do_CreateInstance(AA_MONEY_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(money, "aaMoney instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(money, &rv));
  NS_TEST_ASSERT_MSG(item, "aaMoney aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_MONEY,
      "aaMoney->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_MONEY),
      "aaMoney->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_MONEY,
      "aaMoney->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  return NS_OK;
}

nsresult
aaBaseTest::testFlow(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIFlow> resource(do_CreateInstance(AA_FLOW_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(resource, "aaFlow instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(resource, &rv));
  NS_TEST_ASSERT_MSG(item, "aaFlow aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_FLOW,
      "aaFlow->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_FLOW),
      "aaFlow->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_FLOW,
      "aaFlow->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  aaFlow* raw = static_cast<aaFlow*>(resource.get());
  nsAutoString wstr(NS_LITERAL_STRING("init"));
  NS_TEST_ASSERT_MSG(! raw->mEdited, "blank aaFlow marked edited");
  raw->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "hidden aaFlow marked edited");
  NS_TEST_ASSERT_MSG(! raw->mRead, "hidden aaFlow marked read");
  raw->GetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "read aaFlow marked edited");
  NS_TEST_ASSERT_MSG(raw->mRead, "read aaFlow not marked read");
  raw->SetTag(wstr);
  NS_TEST_ASSERT_MSG(! raw->mEdited, "unchanged aaFlow marked edited");
  raw->SetTag(NS_LITERAL_STRING("edit"));
  NS_TEST_ASSERT_MSG(raw->mEdited, "changed aaFlow not marked edited");

  return NS_OK;
}

nsresult
aaBaseTest::testEvent(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIEvent> item(do_CreateInstance(AA_EVENT_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(item, "aaEvent instance creation" );

  nsCOMPtr<nsIMutableArray> array(do_QueryInterface(item));
  NS_TEST_ASSERT_MSG(array, "aaEvent 'mutableArray' interface" );

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_EVENT,
      "aaEvent->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_EVENT),
      "aaEvent->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_EVENT,
      "aaEvent->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  return NS_OK;
}

nsresult
aaBaseTest::testFact(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIFact> item(do_CreateInstance(AA_FACT_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(item, "[fact] instance creation" );

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_FACT,
      "[fact] accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_FACT),
      "[fact] accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_FACT,
      "[fact] accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  PRInt64 eventId=0;
  PRTime time=0;
  rv = item->GetEventId( &eventId );
  NS_TEST_ASSERT_MSG(rv == NS_ERROR_NOT_INITIALIZED,
      "[fact] uninited eventId returned");
  rv = item->GetTime( &time );
  NS_TEST_ASSERT_MSG(rv == NS_ERROR_NOT_INITIALIZED,
      "[fact] uninited time returned");

  nsCOMPtr<aaIEvent> event = do_CreateInstance(AA_EVENT_CONTRACT);
  NS_ENSURE_TRUE(event, NS_ERROR_UNEXPECTED);

  /* Set time to 2007-08-29 */
  PRExplodedTime tm = {0,0,0,12,29,7,2007};
  event->SetTime(PR_ImplodeTime(&tm));
  event->SetId(1);

  item->SetEvent(event);
  rv = item->GetEventId( &eventId );
  NS_TEST_ASSERT_MSG(eventId == 1, "[fact] wrong eventId returned");
  rv = item->GetTime( &time );
  NS_TEST_ASSERT_MSG(time == PR_ImplodeTime(&tm), "[fact] wrong time returned");

  return NS_OK;
}

nsresult
aaBaseTest::testState(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIState> item(do_CreateInstance(AA_STATE_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(item, "aaState instance creation" );

  return NS_OK;
}

nsresult
aaBaseTest::testRule(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIRule> item(do_CreateInstance(AA_RULE_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(item, "rule instance creation" );

  return NS_OK;
}

nsresult
aaBaseTest::testIncomeFlow(nsITestRunner *aTestRunner)
{
  NS_TEST_BEGIN( aTestRunner );
  nsresult rv;
  nsCOMPtr<aaIFlow> flow(do_CreateInstance(AA_INCOMEFLOW_CONTRACT, &rv));
  NS_TEST_ASSERT_MSG(flow, "[income flow] instance creation" );
  NS_ENSURE_SUCCESS(rv, rv);
  nsCOMPtr<aaIDataNode> item(do_QueryInterface(flow, &rv));
  NS_TEST_ASSERT_MSG(item, "[income flow] aaIDataNode interface" );
  NS_ENSURE_SUCCESS(rv, rv);

  mPackParam = mPackQueries = 0;
  rv = item->Accept(this);
  NS_TEST_ASSERT_MSG(mPackQueries & AA_BASE_TEST_FLOW,
      "[income flow]->Accept doesn't return query" );
  NS_TEST_ASSERT_MSG(!(mPackQueries & ~AA_BASE_TEST_FLOW),
      "[income flow]->Accept returns incorrect query" );
  NS_TEST_ASSERT_MSG(mPackParam & AA_BASE_TEST_FLOW,
      "[income flow]->Accept doesn't provide valid parameter" );
  NS_TEST_ASSERT_OK(rv);

  nsAutoString wstr;
  rv = flow->GetTag(wstr);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! flow->PickId(), "[income flow] wrong tag");
  return NS_OK;
}
