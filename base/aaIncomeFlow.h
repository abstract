/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINCOMEFLOW_H
#define AAINCOMEFLOW_H 1

#define AA_INCOMEFLOW_CID \
{0x4b6105a0, 0x4ea9, 0x477d, {0xb8, 0x68, 0xd0, 0x3a, 0xf3, 0x3f, 0x6c, 0xe6}}

#include "aaIFlow.h"

class aaIEntity;
class aaIResource;

class aaIncomeFlow : public aaIFlow
{
public:
  aaIncomeFlow();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAILISTNODE
  NS_DECL_AAIFLOW

private:
  ~aaIncomeFlow();
  friend class aaBaseTest;
};

#endif /* AAINCOMEFLOW_H */

