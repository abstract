/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaIRule.h"
#include "aaIResource.h"
#include "aaIQuote.h"
#include "aaITransaction.h"
#include "aaIStorager.h"

#include "aaState.h"

aaState::aaState()
  :mSide(0), mAmount(0.0), mValue(0.0), mCreditRate(0.0), mDebitRate(0.0),
  mStatus(PR_FALSE), mStart(LL_MININT), mHasPrevTime(PR_FALSE), mEventId(0),
  mRate(1.0), mDiff0(0.0), mDiff1(0.0)
{
}

aaState::~aaState()
{
}

NS_IMPL_ISUPPORTS3(aaState,
                   aaIDataNode,
                   aaIState,
                   aaIBalance)
/* aaIDataNode */
NS_IMETHODIMP
aaState::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}

NS_IMETHODIMP
aaState::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaState::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  return NS_ERROR_NOT_AVAILABLE;
}

NS_IMETHODIMP
aaState::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  nsresult rv;

  NS_ENSURE_TRUE(mStatus >= NEW, NS_ERROR_NOT_INITIALIZED);
  if (mStatus < NEW)
    return NS_ERROR_NOT_INITIALIZED;

  if (mStatus == REPLACE) {
    rv = aStorager->Close(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else if (mStatus == UPDATE) {
    if (isZero(PickAmount())) {
      rv = aStorager->Delete(aChannel);
      NS_ENSURE_SUCCESS(rv, rv);

      return NS_OK;
    }
    else {
      rv = aStorager->Update(aChannel);
      NS_ENSURE_SUCCESS(rv, rv);

      return NS_OK;
    }
  }

  if (!isZero(PickAmount())) {
    rv = aStorager->Insert(aChannel);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

/* aaIState */
NS_IMETHODIMP
aaState::GetFlow(aaIFlow* *aFlow)
{
  NS_ENSURE_ARG_POINTER(aFlow);
  NS_IF_ADDREF(*aFlow = mFlow);
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetFlow(aaIFlow* aFlow)
{
  mFlow = aFlow;
  return NS_OK;
}
aaIFlow*
aaState::PickFlow()
{
  return mFlow;
}

NS_IMETHODIMP
aaState::GetResource(aaIResource* *aResource)
{
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  if (mSide)
    return mFlow->GetTakeResource(aResource);

  return mFlow->GetGiveResource(aResource);
}

NS_IMETHODIMP
aaState::GetSide(PRBool *aSide)
{
  NS_ENSURE_ARG_POINTER(aSide);
  *aSide = mSide;
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetSide(PRBool aSide)
{
  mSide = aSide;
  return NS_OK;
}
PRBool
aaState::PickSide()
{
  return mSide;
}

NS_IMETHODIMP
aaState::GetAmount(double *aAmount)
{
  NS_ENSURE_ARG_POINTER(aAmount);
  *aAmount = mAmount;
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetAmount(double aAmount)
{
  mAmount = aAmount;
  return NS_OK;
}
double
aaState::PickAmount()
{
  return mAmount;
}

NS_IMETHODIMP
aaState::GetStart(PRTime *aStart)
{
  NS_ENSURE_ARG_POINTER(aStart);
  *aStart = mStart;
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetStart(PRTime aStart)
{
  mStart = aStart;
  mStatus = LOADED;
  return NS_OK;
}
PRTime
aaState::PickStart()
{
  return mStart;
}

NS_IMETHODIMP
aaState::GetEnd(PRTime *aEnd)
{
  NS_ENSURE_ARG_POINTER(aEnd);
  *aEnd = mEnd;
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetEnd(PRTime aEnd)
{
  NS_ENSURE_TRUE(mStatus == LOADED, NS_ERROR_NOT_INITIALIZED);
  mEnd = aEnd;
  mStatus = PAID;
  return NS_OK;
}
PRTime
aaState::PickEnd()
{
  return mEnd;
}

NS_IMETHODIMP
aaState::GetIsPaid(PRBool *aIsPaid)
{
  NS_ENSURE_ARG_POINTER(aIsPaid);
  *aIsPaid = PickIsPaid();
  return NS_OK;
}
PRBool
aaState::PickIsPaid()
{
  return (mStatus == PAID);
}

NS_IMETHODIMP
aaState::GetPrevTime(PRTime *aPrevTime )
{ 
  NS_ENSURE_ARG_POINTER(aPrevTime);
  NS_ENSURE_TRUE(mHasPrevTime, NS_ERROR_NOT_INITIALIZED);
  *aPrevTime = mPrevTime;
  return NS_OK;
}

NS_IMETHODIMP
aaState::ApplyFact(aaIFact *aFact)
{
  NS_ENSURE_TRUE(mFlow && mFlow->PickId(), NS_ERROR_NOT_INITIALIZED);

  nsresult rv;

  rv = setFactSide(aFact);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = updateTime(aFact->PickTime());
  NS_ENSURE_SUCCESS(rv, rv);

  mEventId = aFact->PickEventId();
  mFactId = aFact->PickId();

  rv = useRules(aFact);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
NS_IMETHODIMP
aaState::GetEventId(PRInt64 *aEventId)
{
  NS_ENSURE_ARG_POINTER(aEventId);
  NS_ENSURE_TRUE(mEventId, NS_ERROR_NOT_INITIALIZED);
  *aEventId = mEventId;
  return NS_OK;
}
NS_IMETHODIMP
aaState::GetFactId(PRInt64 *aFactId)
{
  NS_ENSURE_ARG_POINTER(aFactId);
  NS_ENSURE_TRUE(mEventId, NS_ERROR_NOT_INITIALIZED);
  *aFactId = mFactId;
  return NS_OK;
}
NS_IMETHODIMP
aaState::GetFactSide(PRBool *aFactSide)
{
  NS_ENSURE_ARG_POINTER(aFactSide);
  NS_ENSURE_TRUE(mEventId, NS_ERROR_NOT_INITIALIZED);
  *aFactSide = mFactSide;
  return NS_OK;
}

NS_IMETHODIMP
aaState::GetValue(double *aValue)
{
  NS_ENSURE_ARG_POINTER(aValue);
  *aValue = PickValue();
  return NS_OK;
}
NS_IMETHODIMP
aaState::SetInitValue(double aValue)
{
  mValue = aValue;
  return NS_OK;
}
double
aaState::PickValue()
{
  if (!mFlow || !mFlow->PickId())
    return mValue;

  if (mSide)
    return mDebitRate ? normVal(mAmount * mDebitRate) : mValue;

  return mCreditRate ? normVal(mAmount * mCreditRate) : mValue;
}

NS_IMETHODIMP
aaState::GetCreditDiff(double *aDiff)
{
  NS_ENSURE_ARG_POINTER(aDiff);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  *aDiff = PickCreditDiff();
  return NS_OK;
}
double
aaState::PickCreditDiff()
{
  if (!mFlow)
    return 0.0;
  if (!mFlow->PickId())
    return mCreditRate;
  return (!mSide && mCreditRate) ? normVal(mAmount * mCreditRate) - mValue : 0.0;
}

NS_IMETHODIMP
aaState::GetDebitDiff(double *aDiff)
{
  NS_ENSURE_ARG_POINTER(aDiff);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  *aDiff = PickDebitDiff();
  return NS_OK;
}
double
aaState::PickDebitDiff()
{
  if (!mFlow)
    return 0.0;
  if (!mFlow->PickId())
    return mDebitRate;
  return (mSide && mDebitRate) ? normVal(mAmount * mDebitRate) - mValue : 0.0;
}

NS_IMETHODIMP
aaState::ApplyQuote(aaIQuote *aQuote)
{
  NS_ENSURE_ARG_POINTER(aQuote);
  NS_ENSURE_TRUE(mFlow, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER(aQuote->PickResource());

  nsresult rv;
  PRInt64 quoteId = aQuote->PickResource()->PickId();
  nsCOMPtr<aaIResource> resource;
  rv = mFlow->GetTakeResource(getter_AddRefs(resource));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(resource, NS_ERROR_NOT_INITIALIZED);

  if (quoteId == resource->PickId()) {
    mDebitRate = aQuote->PickRate();
    return NS_OK;
  }

  rv = mFlow->GetGiveResource(getter_AddRefs(resource));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(resource, NS_ERROR_NOT_INITIALIZED);

  NS_ENSURE_TRUE(quoteId == resource->PickId(), NS_ERROR_INVALID_ARG);

  mCreditRate = aQuote->PickRate();
  return NS_OK;
}

NS_IMETHODIMP
aaState::ApplyTransaction(aaITransaction *aTransaction, double *aChange,
    PRBool *aHasChange)
{
  NS_ENSURE_ARG_POINTER(aTransaction);
  NS_ENSURE_TRUE(mFlow && mFlow->PickId(), NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(aTransaction->PickFact(), NS_ERROR_INVALID_ARG);

  nsresult rv;
  rv = setFactSide(aTransaction->PickFact());
  NS_ENSURE_SUCCESS(rv, rv);

  rv = updateTime(aTransaction->PickFact()->PickTime());
  NS_ENSURE_SUCCESS(rv, rv);

  double change = 0.0;
  rv = updateValue(aTransaction->PickValue(), &change);
  NS_ENSURE_SUCCESS(rv, rv);

  PRBool hasChange = !isZero(change);
  if (!mFactSide && mDebitRate && mFlow->PickTakeResource()->PickId() !=
      mFlow->PickGiveResource()->PickId())
    hasChange = PR_TRUE;

  if (aChange)
    *aChange = change;

  if  (aHasChange)
    *aHasChange = hasChange;

  return NS_OK;
}

/* Private methods */
nsresult
aaState::setFactSide(aaIFact *aFact)
{
  NS_ENSURE_ARG_POINTER(aFact);

  if (aFact->PickTakeFrom() &&
      mFlow->PickId() == aFact->PickTakeFrom()->PickId())
  {
    mFactSide = 1;
  }
  else if (aFact->PickGiveTo() &&
      mFlow->PickId() == aFact->PickGiveTo()->PickId())
  {
    mFactSide = 0;
  }
  else {
    return NS_ERROR_INVALID_ARG;
  }

  nsresult rv;
  mOldAmount = mAmount; mOldValue = PickValue();

  rv = mFlow->GetRate(&mRate);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mSide == mFactSide) {
    mDiff0 = aFact->PickAmount();
    mAmount -= mDiff0;
  }
  else {
    mDiff1 = aFact->PickAmount() * (mSide ? mRate : 1 / mRate);
    mAmount += mDiff1;
  }

  if (!isZero(mAmount) && mAmount < 0.0) {
    mSide = !mSide;
    mAmount *= -1 * (mSide ? mRate : 1 / mRate);
    mDiff0 = mOldAmount;
    mDiff1 = mAmount;
    mOldValue = -mOldValue;
  }

  mAmount = normVal(mAmount);
  mDiff0 = normVal(mDiff0);
  mDiff1 = normVal(mDiff1);

  return NS_OK;
}

nsresult
aaState::updateTime(PRTime time)
{
  NS_ENSURE_TRUE(mStatus != PAID, NS_ERROR_NOT_AVAILABLE);
  NS_ENSURE_TRUE(!mStatus || mStart <= time, NS_ERROR_INVALID_ARG);

  if (!mStatus) {
    mStatus = NEW;
    mStart = time;
  }
  else if (mStart == time) {
    mStatus = UPDATE;
  }
  else {
    mStatus = REPLACE;
    mPrevTime = mStart;
    mHasPrevTime = PR_TRUE;
    mStart = time;
  }

  return NS_OK;
}

nsresult
aaState::updateValue(double param, double *_retval)
{
  if (mFactSide) {
    if (mSide) {
      if (mDebitRate) {
        mValue = normVal(mAmount * mDebitRate);
      }
      else {
        NS_ENSURE_TRUE(mOldValue >= 0.0, NS_ERROR_NOT_AVAILABLE);
        NS_ENSURE_TRUE(mOldAmount != 0.0, NS_ERROR_FAILURE);
        mValue = normVal(mOldValue * mAmount / mOldAmount);
      }
      *_retval = mOldValue - mValue;
    }
    else {
      if (mCreditRate) {
        mValue = normVal(mAmount * mCreditRate);
      }
      else {
        NS_ENSURE_TRUE(mDebitRate, NS_ERROR_INVALID_ARG);
        mValue = normVal(mAmount * mDebitRate * mRate);
      }
      *_retval = mValue - mOldValue;
    }
  }
  else {
    if (mSide) {
      if (mDebitRate) {
        mValue = normVal(mAmount * mDebitRate);
        *_retval = mValue - mOldValue - param;
      }
      else {
        NS_ENSURE_TRUE(param != 0.0, NS_ERROR_INVALID_ARG);
        mValue = mOldValue + param;
      }
    }
    else {
      if (mCreditRate) {
        mValue = normVal(mAmount * mCreditRate);
      }
      else {
        NS_ENSURE_TRUE(mOldValue > 0.0, NS_ERROR_NOT_AVAILABLE);
        NS_ENSURE_TRUE(mOldAmount != 0.0, NS_ERROR_FAILURE);
        mValue = normVal(mOldValue * mAmount / mOldAmount);
      }
      *_retval = mOldValue - mValue - param;
    }
  }

  return NS_OK;
}

nsresult
aaState::useRules(aaIFact *aFact)
{
  if (aFact->PickAmount() < 0.0)
    return NS_OK;

  nsresult rv;
  nsCOMPtr<nsIArray> ruleSet = do_QueryInterface(mFlow, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  PRUint32 i, count;
  rv = ruleSet->GetLength(&count);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIRule> rule;
  for (i = 0; i < count; i++) {
    rule = do_QueryElementAt(ruleSet, i, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    PRBool factSide, changeSide;
    rv = rule->GetFactSide(&factSide);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = rule->GetChangeSide(&changeSide);
    NS_ENSURE_SUCCESS(rv, rv);

    if (factSide == mFactSide) {
      if (!isZero(mDiff0) && !changeSide) {
        rv = aFact->UseRule(rule, mDiff0);
        NS_ENSURE_SUCCESS(rv, rv);
      }
      else if (!isZero(mDiff1) && changeSide) {
        rv = aFact->UseRule(rule, mDiff1);
        NS_ENSURE_SUCCESS(rv, rv);
      }
    }
  }

  return NS_OK;
}
