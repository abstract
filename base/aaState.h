/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASTATE_H
#define AASTATE_H 1

#define AA_STATE_CID \
{0xadce33d6, 0x848e, 0x4d1b, {0xbe, 0xe4, 0xef, 0x23, 0x31, 0x48, 0xf7, 0xb2}}

#include "aaIBalance.h"

class aaIFlow;
class aaIResource;

class aaState : public aaIBalance
{
public:
  aaState();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAISTATE
  NS_DECL_AAIBALANCE

private:
  ~aaState();
  friend class aaBaseTest;

  enum {
    LOADED = 1
    ,PAID
    ,NEW
    ,UPDATE
    ,REPLACE
  };

  PRInt64 mId;
  nsCOMPtr<aaIFlow> mFlow;
  PRBool mSide;
  double mAmount;
  double mValue;
  double mCreditRate;
  double mDebitRate;
  PRUint32 mStatus;
  PRTime mStart;
  PRTime mEnd;
  PRBool mHasPrevTime;
  PRTime mPrevTime;
  PRInt64 mEventId;
  PRInt64 mFactId;
  PRBool mFactSide;
  double mOldAmount;
  double mOldValue;
  double mRate;
  double mDiff0;
  double mDiff1;

  nsresult setFactSide(aaIFact *aFact);
  nsresult updateTime(PRTime time);
  nsresult updateValue(double param, double *_retval);
  nsresult useRules(aaIFact *aFact);
};

#endif /* AASTATE_H */
