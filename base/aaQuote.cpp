/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsServiceManagerUtils.h"
#include "nsComponentManagerUtils.h"
#include "nsIObserverService.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Project includes */
#include "aaIResource.h"
#include "aaIBalance.h"
#include "aaIHandler.h"
#include "aaBaseKeys.h"

#include "aaQuote.h"

aaQuote::aaQuote()
  :mEdited(0), mHasTime(PR_FALSE), mRate(1.0), mDiff(0.0)
  
{
}

aaQuote::~aaQuote()
{
}

NS_IMPL_ISUPPORTS2_CI(aaQuote,
                      aaIDataNode,
                      aaIQuote)
/* aaIDataNode */
NS_IMETHODIMP
aaQuote::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP
aaQuote::SetId(PRInt64 aId)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaQuote::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return aQuery->HandleQuote(this);
}

NS_IMETHODIMP
aaQuote::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = mEdited;
  return NS_OK;
}

NS_IMETHODIMP
aaQuote::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIQuote */
NS_IMETHODIMP
aaQuote::GetResource(aaIResource ** aResource)
{
  NS_ENSURE_ARG_POINTER(aResource);
  NS_IF_ADDREF(*aResource = mResource);
  return NS_OK;
}
NS_IMETHODIMP
aaQuote::SetResource(aaIResource * aResource)
{
  if (mResource == aResource)
    return NS_OK;
  mResource = aResource;
  mEdited = PR_TRUE;
  nsCOMPtr<nsIObserverService> broadcaster = do_GetService(
      "@mozilla.org/observer-service;1");
  NS_ENSURE_TRUE(broadcaster, NS_OK);
  broadcaster->NotifyObservers(this, "quote-resource-change", nsnull);
  return NS_OK;
}
aaIResource*
aaQuote::PickResource()
{
  return mResource;
}

NS_IMETHODIMP
aaQuote::GetTime(PRTime *aTime)
{
  NS_ENSURE_TRUE( mHasTime, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_ARG_POINTER( aTime );
  *aTime = mTime;
  return NS_OK;
}
NS_IMETHODIMP
aaQuote::SetTime(PRTime aTime)
{
  mTime = aTime;
  mHasTime = PR_TRUE;
  mEdited = PR_TRUE;
  return NS_OK;
}
PRInt64
aaQuote::PickTime()
{
  return mTime;
}

NS_IMETHODIMP
aaQuote::GetRate(double *aRate)
{
  NS_ENSURE_ARG_POINTER(aRate);
  *aRate = mRate;
  return NS_OK;
}
NS_IMETHODIMP
aaQuote::SetRate(double aRate)
{
  mRate = aRate;
  mEdited = PR_TRUE;
  return NS_OK;
}
double
aaQuote::PickRate()
{
  return mRate;
}

NS_IMETHODIMP
aaQuote::GetDiff(double *aDiff)
{
  NS_ENSURE_ARG_POINTER(aDiff);
  *aDiff = mDiff;
  return NS_OK;
}
NS_IMETHODIMP
aaQuote::SetDiff(double aDiff)
{
  mDiff = aDiff;
  return NS_OK;
}
double
aaQuote::PickDiff()
{
  return mDiff;
}

NS_IMETHODIMP
aaQuote::InitIncome(aaIBalance *aIncome)
{
  nsresult rv;
  if (NS_UNLIKELY(mIncome)) return NS_ERROR_ALREADY_INITIALIZED;

  nsCOMPtr<aaIBalance> income = aIncome;
  if (!income) {
    income = do_CreateInstance(AA_INCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = income->ApplyQuote(this);
  NS_ENSURE_SUCCESS(rv, rv);

  mIncome = income;
  return NS_OK;
}
NS_IMETHODIMP
aaQuote::GetIncome(aaIBalance * *aIncome)
{
  NS_ENSURE_ARG_POINTER(aIncome);
  NS_IF_ADDREF(*aIncome = mIncome);
  return NS_OK;
}
