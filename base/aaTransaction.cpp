/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"

/* Project includes */
#include "aaIFact.h"
#include "aaIFlow.h"
#include "aaIHandler.h"
#include "aaIBalance.h"
#include "aaBaseKeys.h"

#include "aaTransaction.h"

aaTransaction::aaTransaction()
  :mValue(0.0),
  mEarnings(0.0),
  mHasBalanceFrom(PR_FALSE),
  mHasBalanceTo(PR_FALSE),
  mHasEarnings(PR_FALSE),
  mBroadcasting(PR_FALSE)
{
}

NS_IMPL_ISUPPORTS2(aaTransaction,
                   aaIDataNode,
                   aaITransaction)

/* aaIDataNode */
NS_IMETHODIMP
aaTransaction::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return aQuery->HandleTransaction(this);
}

NS_IMETHODIMP
aaTransaction::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = PR_FALSE;
  return NS_OK;
}

NS_IMETHODIMP
aaTransaction::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaITransaction */
NS_IMETHODIMP
aaTransaction::GetFact(aaIFact * *aFact)
{
  NS_ENSURE_ARG_POINTER( aFact );
  NS_IF_ADDREF( *aFact = mFact );
  return NS_OK;
}
NS_IMETHODIMP
aaTransaction::SetFact(aaIFact *aFact)
{
  mFact = aFact;
  return NS_OK;
}
aaIFact *
aaTransaction::PickFact()
{
  return mFact;
}

NS_IMETHODIMP
aaTransaction::GetValue(double *aValue)
{
  NS_ENSURE_ARG_POINTER( aValue );
  *aValue = mValue;
  return NS_OK;
}
NS_IMETHODIMP
aaTransaction::SetValue(double aValue)
{
  mValue = aValue;
  return NS_OK;
}
double
aaTransaction::PickValue()
{
  return mValue;
}

NS_IMETHODIMP
aaTransaction::GetHasEarnings(PRBool *aHasEarnings)
{
  NS_ENSURE_ARG_POINTER(aHasEarnings);
  *aHasEarnings = mHasEarnings;
  return NS_OK;
}
PRBool
aaTransaction::PickHasEarnings()
{
  return mHasEarnings;
}

NS_IMETHODIMP
aaTransaction::GetEarnings(double *aEarnings)
{
  NS_ENSURE_ARG_POINTER(aEarnings);
  NS_ENSURE_TRUE(mHasEarnings, NS_ERROR_NOT_INITIALIZED);
  *aEarnings = mEarnings;
  return NS_OK;
}
NS_IMETHODIMP
aaTransaction::SetEarnings(double aEarnings)
{
  mHasEarnings = PR_TRUE;
  mEarnings = aEarnings;
  return NS_OK;
}
double
aaTransaction::PickEarnings()
{
  return mEarnings;
}

NS_IMETHODIMP
aaTransaction::InitBalanceFrom(aaIBalance *aBalanceFrom)
{
  NS_ENSURE_TRUE(mFact, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(!mHasBalanceFrom, NS_ERROR_ALREADY_INITIALIZED);

  if (!aBalanceFrom) {
    NS_ENSURE_TRUE(!mFact->PickTakeFrom() || !mFact->PickTakeFrom()->PickId(),
        NS_ERROR_INVALID_ARG);

    mHasBalanceFrom = PR_TRUE;
    return tryValueCalculation();
  }

  NS_ENSURE_TRUE(aBalanceFrom->PickFlow() &&
      aBalanceFrom->PickFlow()->PickId(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(mFact->PickTakeFrom() && mFact->PickTakeFrom()->PickId(),
      NS_ERROR_UNEXPECTED);
  NS_ENSURE_TRUE(aBalanceFrom->PickFlow()->PickId() ==
      mFact->PickTakeFrom()->PickId(), NS_ERROR_INVALID_ARG);

  mBalanceFrom = aBalanceFrom;
  mHasBalanceFrom = PR_TRUE;
  return tryValueCalculation();
}
NS_IMETHODIMP
aaTransaction::InitBalanceTo(aaIBalance *aBalanceTo)
{
  NS_ENSURE_TRUE(mFact, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(!mHasBalanceTo, NS_ERROR_ALREADY_INITIALIZED);

  if (!aBalanceTo) {
    NS_ENSURE_TRUE(!mFact->PickGiveTo()
        || mFact->PickGiveTo()->PickIsOffBalance(),
        NS_ERROR_INVALID_ARG);

    mHasBalanceTo = PR_TRUE;
    return tryValueCalculation();
  }

  NS_ENSURE_TRUE(aBalanceTo->PickFlow() &&
      aBalanceTo->PickFlow()->PickId(), NS_ERROR_INVALID_ARG);
  NS_ENSURE_TRUE(mFact->PickGiveTo() && mFact->PickGiveTo()->PickId(),
      NS_ERROR_UNEXPECTED);
  NS_ENSURE_TRUE(aBalanceTo->PickFlow()->PickId() ==
      mFact->PickGiveTo()->PickId(), NS_ERROR_INVALID_ARG);

  mBalanceTo = aBalanceTo;
  mHasBalanceTo = PR_TRUE;
  return tryValueCalculation();
}
NS_IMETHODIMP
aaTransaction::InitIncome(aaIBalance *aIncome)
{
  if (!mHasBalanceFrom || !mHasBalanceTo) {
    mIncome = aIncome;
    return NS_OK;
  }
  NS_ENSURE_TRUE(!mIncome, NS_ERROR_ALREADY_INITIALIZED);
  nsresult rv;
  mIncome = aIncome;
  if (!mIncome) {
    mIncome = do_CreateInstance(AA_INCOME_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = mIncome->ApplyTransaction(this, nsnull, nsnull);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaTransaction::GetBalanceFrom(aaIBalance * * aBalanceFrom)
{
  NS_ENSURE_ARG_POINTER(aBalanceFrom);
  NS_ENSURE_TRUE(mHasBalanceFrom && mHasBalanceTo, NS_ERROR_NOT_INITIALIZED);
  NS_IF_ADDREF(*aBalanceFrom = mBalanceFrom);
  return NS_OK;
}
NS_IMETHODIMP
aaTransaction::GetBalanceTo(aaIBalance * * aBalanceTo)
{
  NS_ENSURE_ARG_POINTER(aBalanceTo);
  NS_ENSURE_TRUE(mHasBalanceFrom && mHasBalanceTo, NS_ERROR_NOT_INITIALIZED);
  NS_IF_ADDREF(*aBalanceTo = mBalanceTo);
  return NS_OK;
}
NS_IMETHODIMP
aaTransaction::GetIncome(aaIBalance * *aIncome)
{
  NS_ENSURE_ARG_POINTER(aIncome);
  NS_ENSURE_TRUE(mHasBalanceFrom && mHasBalanceTo, NS_ERROR_NOT_INITIALIZED);
  if (!mHasEarnings) {
    *aIncome = nsnull;
    return NS_OK;
  }
  NS_ENSURE_TRUE(mIncome, NS_ERROR_NOT_INITIALIZED);
  NS_IF_ADDREF(*aIncome = mIncome);
  return NS_OK;
}

/* Private methods */
nsresult
aaTransaction::tryValueCalculation()
{
  if (!mHasBalanceFrom || !mHasBalanceTo)
    return NS_OK;

  return doValueCalculation();
}

nsresult
aaTransaction::doValueCalculation()
{
  nsresult rv;

  mValue = 0.0; mEarnings = 0.0;
  if (mBalanceFrom) {
    rv = mBalanceFrom->ApplyTransaction(this, &mValue, nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else if (!mBalanceTo) {
    return NS_OK;
  }

  if (mBalanceTo) {
    PRBool hasEarnings;
    rv = mBalanceTo->ApplyTransaction(this, &mEarnings, &hasEarnings);
    NS_ENSURE_SUCCESS(rv, rv);

    mHasEarnings = (PRPackedBool) hasEarnings;
  }
  else {
    mHasEarnings = PR_TRUE;
    mEarnings = -mValue;
  }

  if (mHasEarnings && mIncome) {
    rv = mIncome->ApplyTransaction(this, nsnull, nsnull);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}
