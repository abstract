/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATIMEFRAME_H
#define AATIMEFRAME_H 1

#define AA_TIMEFRAME_CID \
{0xa03ff4ff, 0xcff9, 0x4a6f, {0x9d, 0xd9, 0x2f, 0x05, 0xd3, 0xee, 0xa7, 0x0c}}

#include "aaITimeFrame.h"

class aaTimeFrame : public aaITimeFrame
{
public:
  aaTimeFrame();
  NS_DECL_ISUPPORTS
  NS_DECL_AAITIMEFRAME

private:
  ~aaTimeFrame();
  friend class aaBaseTest;

  PRUint32 mStatus;
  PRTime mStart;
  PRTime mEnd;
};

#endif /* AATIMEFRAME_H */

