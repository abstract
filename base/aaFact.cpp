/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsIObserverService.h"
#include "nsIMutableArray.h"

/* Project includes */
#include "aaIFlow.h"
#include "aaIResource.h"
#include "aaIRule.h"
#include "aaIEvent.h"
#include "aaIState.h"
#include "aaIHandler.h"
#include "aaBaseKeys.h"

#include "aaFact.h"

aaFact::aaFact()
  :mAmount(0.0), mBroadcasting(PR_FALSE)
{
}

aaFact::~aaFact()
{
}

NS_IMPL_ISUPPORTS3(aaFact,
                   aaIDataNode,
                   aaIListNode,
                   aaIFact)

/* aaIListNode */
NS_IMETHODIMP
aaFact::GetId(PRInt64 *aId)
{
  NS_ENSURE_ARG_POINTER(aId);
  *aId = mId;
  return NS_OK;
}
NS_IMETHODIMP
aaFact::SetId(PRInt64 aId)
{
  mId = aId;
  return NS_OK;
}
PRInt64
aaFact::PickId()
{
  return mId;
}

/* aaIDataNode */
NS_IMETHODIMP
aaFact::Accept(aaIHandler* aQuery)
{
  NS_ENSURE_ARG_POINTER(aQuery);
  return aQuery->HandleFact(this);
}

NS_IMETHODIMP
aaFact::GetEdited(PRBool* aEdited)
{
  NS_ENSURE_ARG_POINTER(aEdited);
  *aEdited = PR_FALSE;
  return NS_OK;
}

NS_IMETHODIMP
aaFact::Sync(aaIStorager *aStorager, aaISqliteChannel *aChannel)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIFact */
NS_IMETHODIMP
aaFact::GetEventId(PRInt64 *aEventId)
{
  NS_ENSURE_ARG_POINTER( aEventId );
  if (NS_UNLIKELY( !mEvent ))
    return NS_ERROR_NOT_INITIALIZED;
  *aEventId = PickEventId();
  return NS_OK;
}
PRInt64
aaFact::PickEventId()
{
  if (!mEvent)
    return 0;
  nsCOMPtr<aaIEvent> event = do_QueryReferent(mEvent);
  if (event)
    event->GetId(&mEventId);
  return mEventId;
}


NS_IMETHODIMP
aaFact::GetTime(PRTime *aTime)
{
  NS_ENSURE_ARG_POINTER( aTime );
  if (NS_UNLIKELY( !mEvent ))
    return NS_ERROR_NOT_INITIALIZED;
  *aTime = mTime;
  return NS_OK;
}
PRTime
aaFact::PickTime()
{
  if (!mEvent)
    return 0;
  nsCOMPtr<aaIEvent> event = do_QueryReferent(mEvent);
  if (event)
    event->GetTime(&mTime);
  return mTime;
}

NS_IMETHODIMP
aaFact::SetEvent(aaIEvent *aEvent)
{
  if (NS_UNLIKELY( ! aEvent )) {
    mEvent = nsnull;
    return NS_OK;
  }
  nsresult rv;

  rv = aEvent->GetId( &mEventId );
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aEvent->GetTime( &mTime );
  NS_ENSURE_SUCCESS(rv, rv);

  mEvent = do_GetWeakReference(aEvent, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaFact::GetTakeFrom(aaIFlow * *aTakeFrom)
{
  NS_ENSURE_ARG_POINTER( aTakeFrom );
  NS_IF_ADDREF( *aTakeFrom = mTakeFrom );
  return NS_OK;
}
NS_IMETHODIMP
aaFact::SetTakeFrom(aaIFlow *aTakeFrom)
{
  NS_ENSURE_TRUE(!mStateFrom, NS_ERROR_ALREADY_INITIALIZED);
  mTakeFrom = aTakeFrom;
  if (! mBroadcasting)
    return NS_OK;
  nsCOMPtr<nsIObserverService> broadcaster = do_GetService(
      "@mozilla.org/observer-service;1");
  NS_ENSURE_TRUE(broadcaster, NS_OK);
  broadcaster->NotifyObservers(this, "fact-from-flow-change", nsnull);
  return NS_OK;
}
aaIFlow *
aaFact::PickTakeFrom()
{
  return mTakeFrom;
}

NS_IMETHODIMP
aaFact::GetGiveTo(aaIFlow * *aGiveTo)
{
  NS_ENSURE_ARG_POINTER( aGiveTo );
  NS_IF_ADDREF( *aGiveTo = mGiveTo );
  return NS_OK;
}
NS_IMETHODIMP
aaFact::SetGiveTo(aaIFlow *aGiveTo)
{
  NS_ENSURE_TRUE(!mStateTo, NS_ERROR_ALREADY_INITIALIZED);
  mGiveTo = aGiveTo;
  if (! mBroadcasting)
    return NS_OK;
  nsCOMPtr<nsIObserverService> broadcaster = do_GetService(
      "@mozilla.org/observer-service;1");
  NS_ENSURE_TRUE(broadcaster, NS_OK);
  broadcaster->NotifyObservers(this, "fact-to-flow-change", nsnull);
  return NS_OK;
}
aaIFlow *
aaFact::PickGiveTo()
{
  return mGiveTo;
}

NS_IMETHODIMP
aaFact::GetAmount(double *aAmount)
{
  NS_ENSURE_ARG_POINTER( aAmount );
  *aAmount = mAmount;
  return NS_OK;
}
NS_IMETHODIMP
aaFact::SetAmount(double aAmount)
{
  mAmount = aAmount;
  return NS_OK;
}
double
aaFact::PickAmount()
{
  return mAmount;
}

NS_IMETHODIMP
aaFact::InitStateFrom(aaIState *aStateFrom)
{
  NS_ENSURE_TRUE(!mStateFrom, NS_ERROR_ALREADY_INITIALIZED);
  if ((!mTakeFrom || !mTakeFrom->PickId()) && !aStateFrom)
    return NS_OK;
  nsresult rv;
  nsCOMPtr<aaIState> stateFrom = aStateFrom;
  if (stateFrom) {
    NS_ENSURE_TRUE(mTakeFrom, NS_ERROR_INVALID_ARG);
    NS_ENSURE_ARG_POINTER(aStateFrom->PickFlow());
    NS_ENSURE_TRUE(mTakeFrom->PickId() == aStateFrom->PickFlow()->PickId(),
        NS_ERROR_INVALID_ARG);
    if (mStateTo && mStateTo->PickFlow()->PickId()) {
      NS_ENSURE_TRUE(mStateTo->PickFlow()->PickGiveResource(),
          NS_ERROR_INVALID_ARG);
      NS_ENSURE_TRUE(aStateFrom->PickFlow()->PickTakeResource(),
          NS_ERROR_INVALID_ARG);
      if (mStateTo->PickFlow()->PickGiveResource()->PickId() !=
         aStateFrom->PickFlow()->PickTakeResource()->PickId())
        return NS_ERROR_INVALID_ARG;
    }
  }
  else {
    stateFrom = do_CreateInstance(AA_STATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = stateFrom->SetFlow(mTakeFrom);
  NS_ENSURE_SUCCESS(rv, rv);
  
  rv = stateFrom->ApplyFact(this);
  NS_ENSURE_SUCCESS(rv, rv);

  mStateFrom = stateFrom;
  return NS_OK;
}

NS_IMETHODIMP
aaFact::InitStateTo(aaIState *aStateTo)
{
  NS_ENSURE_TRUE(!mStateTo, NS_ERROR_ALREADY_INITIALIZED);
  if ((!mGiveTo || !mGiveTo->PickId()) && !aStateTo)
    return NS_OK;
  nsresult rv;
  nsCOMPtr<aaIState> stateTo = aStateTo;
  if (stateTo) {
    NS_ENSURE_TRUE(mGiveTo, NS_ERROR_INVALID_ARG);
    NS_ENSURE_ARG_POINTER(aStateTo->PickFlow());
    NS_ENSURE_TRUE(mGiveTo->PickId() == aStateTo->PickFlow()->PickId(),
        NS_ERROR_INVALID_ARG);
    if (mStateFrom && mStateFrom->PickFlow()->PickId()) {
      NS_ENSURE_TRUE(mStateFrom->PickFlow()->PickTakeResource(),
          NS_ERROR_INVALID_ARG);
      NS_ENSURE_TRUE(aStateTo->PickFlow()->PickGiveResource(),
          NS_ERROR_INVALID_ARG);
      if (mStateFrom->PickFlow()->PickTakeResource()->PickId() !=
         aStateTo->PickFlow()->PickGiveResource()->PickId())
        return NS_ERROR_INVALID_ARG;
    }
  }
  else {
    stateTo = do_CreateInstance(AA_STATE_CONTRACT, &rv);
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = stateTo->SetFlow(mGiveTo);
  NS_ENSURE_SUCCESS(rv, rv);
  
  rv = stateTo->ApplyFact(this);
  NS_ENSURE_SUCCESS(rv, rv);

  mStateTo = stateTo;
  return NS_OK;
}

NS_IMETHODIMP
aaFact::GetStateFrom(aaIState * * aStateFrom)
{
  NS_ENSURE_ARG_POINTER(aStateFrom);
  NS_IF_ADDREF(*aStateFrom = mStateFrom);
  return NS_OK;
}

NS_IMETHODIMP
aaFact::GetStateTo(aaIState * * aStateTo)
{
  NS_ENSURE_ARG_POINTER(aStateTo);
  NS_IF_ADDREF(*aStateTo = mStateTo);
  return NS_OK;
}

NS_IMETHODIMP
aaFact::GetBroadcasting(PRBool *aBroadcasting)
{
  NS_ENSURE_ARG_POINTER(aBroadcasting);
  *aBroadcasting = mBroadcasting;
  return NS_OK;
}
NS_IMETHODIMP
aaFact::SetBroadcasting(PRBool aBroadcasting)
{
  mBroadcasting = aBroadcasting;
  return NS_OK;
}

NS_IMETHODIMP
aaFact::UseRule(aaIRule *aRule, double aAmount)
{
  nsresult rv;
  nsRefPtr<aaFact> newFact = new aaFact();
  double rate;
  rv = aRule->GetRate(&rate);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = newFact->SetAmount(rate * aAmount);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIFlow> takeFrom, giveTo;
  rv = aRule->GetTakeFrom(getter_AddRefs(takeFrom));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = newFact->SetTakeFrom(takeFrom);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = aRule->GetGiveTo(getter_AddRefs(giveTo));
  NS_ENSURE_SUCCESS(rv, rv);

  rv = newFact->SetGiveTo(giveTo);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIMutableArray> factSet = do_QueryReferent(mEvent);
  rv = factSet->AppendElement(newFact, PR_FALSE);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}
