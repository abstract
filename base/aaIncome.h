/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAINCOME_H
#define AAINCOME_H 1

#define AA_INCOME_CID \
{0x586b3332, 0xa372, 0x421c, {0xa6, 0x6b, 0x02, 0x10, 0xa3, 0x00, 0x65, 0x3c}}

#include "aaIIncome.h"

class aaIFlow;
class aaIResource;

class aaIncome : public aaIIncome
{
public:
  aaIncome();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAISTATE
  NS_DECL_AAIBALANCE
  NS_DECL_AAIINCOME

private:
  ~aaIncome();
  friend class aaBaseTest;

  enum {
    LOADED = 1
    ,PAID
    ,NEW
    ,UPDATE
    ,REPLACE
  };

  PRInt64 mId;
  nsCOMPtr<aaIFlow> mFlow;
  PRBool mSide;
  double mValue;
  double mCreditDiff;
  double mDebitDiff;
  PRUint32 mStatus;
  PRTime mStart;
  PRTime mEnd;
  PRBool mHasPrevTime;
  PRTime mPrevTime;

  nsresult update(PRTime time, double change);
};

#endif /* AAINCOME_H */
