/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAQUOTE_H
#define AAQUOTE_H 1

#define AA_QUOTE_CID \
{0xda135703, 0xc181, 0x4e7d, {0x9f, 0x97, 0xe4, 0x3c, 0x50, 0xa6, 0xb5, 0x1a}}

#include "aaIQuote.h"

class aaIResource;

class aaQuote : public aaIQuote
{
public:
  aaQuote();
  NS_DECL_ISUPPORTS
  NS_DECL_AAIDATANODE
  NS_DECL_AAIQUOTE

private:
  virtual ~aaQuote();
  friend class aaBaseTest;

  PRBool mEdited;
  nsCOMPtr<aaIResource> mResource;
  PRTime mTime;
  PRBool mHasTime;
  double mRate;
  double mDiff;
  nsCOMPtr<aaIBalance> mIncome;
};

#endif /* AAQUOTE_H */
