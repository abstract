/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsAutoPtr.h"
#include "nsIIOService.h"
#include "nsISHistory.h"
#include "nsIURI.h"
#include "nsIComponentManager.h"
#include "nsComponentManagerUtils.h"
#include "nsMemory.h"
#include "nsStringAPI.h"
#include "nsIClassInfoImpl.h"
#include "nsIServiceManager.h"

/* Unfrozen API */
#include "nsIDocShell.h"
#include "nsISHistoryInternal.h"
#include "nsITreeView.h"
#include "nsIWebNavigation.h"

/* Project includes */
#include "aaIPageView.h"
#include "aaLoadGroup.h"
#include "aaSessionHistory.h"

aaSessionHistory::aaSessionHistory()
  :mChoosing(PR_FALSE)
{
  mSH = do_CreateInstance(NS_SHISTORY_CONTRACTID);
  if (!mSH)
    return;
  mSHI = do_QueryInterface(mSH);
  if (!mSHI)
    return;
  mWN = do_QueryInterface(mSH);
  if (!mWN)
    return;
}

aaSessionHistory::~aaSessionHistory()
{
}

NS_IMPL_ISUPPORTS5_CI(aaSessionHistory,
                      nsISHistory,
                      nsISHistoryInternal,
                      nsIWebNavigation,
                      aaISHistory,
                      aaISession)

/* nsISHistoryInternal */
NS_IMETHODIMP
aaSessionHistory::AddEntry(nsISHEntry *aEntry, PRBool aPersist)
{
  nsCOMPtr<aaIPageView> pageView( do_CreateInstance(
        "@aasii.org/view/page-view;1", aEntry));
  pageView->SetChoosing(mChoosing);
  if (mChoosing)
    mChoosing = PR_FALSE;
  return mSHI->AddEntry(pageView, aPersist);
}

NS_IMETHODIMP
aaSessionHistory::GetRootTransaction(nsISHTransaction * *aRootTransaction)
{
  return mSHI->GetRootTransaction(aRootTransaction);
}

NS_IMETHODIMP
aaSessionHistory::GetRootDocShell(nsIDocShell * *aRootDocShell)
{
  return mSHI->GetRootDocShell( aRootDocShell );
}
NS_IMETHODIMP
aaSessionHistory::SetRootDocShell(nsIDocShell * aRootDocShell)
{
  return mSHI->SetRootDocShell( aRootDocShell );
}

NS_IMETHODIMP
aaSessionHistory::UpdateIndex()
{
  return mSHI->UpdateIndex();
}

NS_IMETHODIMP
aaSessionHistory::ReplaceEntry(PRInt32 aIndex, nsISHEntry *aReplaceEntry)
{
  if (!aReplaceEntry->GetIID().Equals(NS_GET_IID(aaIPageView))) {
    nsCOMPtr<aaIPageView> pageView(do_CreateInstance("@aasii.org/view/page-view;1", aReplaceEntry));
    pageView->SetChoosing(mChoosing);
    return mSHI->ReplaceEntry(aIndex, pageView);
  }
  return mSHI->ReplaceEntry(aIndex, aReplaceEntry);
}

NS_IMETHODIMP
aaSessionHistory::GetListener(nsISHistoryListener * *aListener)
{
  return mSHI->GetListener(aListener);
}

NS_IMETHODIMP
aaSessionHistory::EvictContentViewers(PRInt32 previousIndex, PRInt32 index)
{
  return mSHI->EvictContentViewers(previousIndex, index);
}

NS_IMETHODIMP
aaSessionHistory::EvictExpiredContentViewerForEntry(nsISHEntry *aEntry)
{
  return mSHI->EvictExpiredContentViewerForEntry(aEntry);
}

NS_IMETHODIMP
aaSessionHistory::EvictAllContentViewers()
{
  return mSHI->EvictAllContentViewers();
}

/* nsIWebNavigation */
NS_IMETHODIMP
aaSessionHistory::LoadURI(const PRUnichar *aURI, PRUint32 aLoadFlags,
    nsIURI *aReferrer, nsIInputStream *aPostData, nsIInputStream *aHeaders)
{
  nsresult rv;
  nsIDocShell *docShell;
  rv = mSHI->GetRootDocShell(&docShell);
  NS_ENSURE_SUCCESS(rv, rv);

  nsRefPtr<aaLoadGroup> loader = new aaLoadGroup(docShell);
  rv = loader->Init(aURI, aLoadFlags, aReferrer, aPostData, aHeaders);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* aaISHistory */
NS_IMETHODIMP
aaSessionHistory::GetChoosing(PRBool *aChoosing)
{
  nsresult rv;
  nsCOMPtr<aaIPageView> pv1 = getter_AddRefs( getDestination(0, rv) );
  NS_ENSURE_TRUE(pv1, rv);

  return pv1->GetChoosing(aChoosing);
}

NS_IMETHODIMP
aaSessionHistory::BeginQuery()
{
  if ( mChoosing )
    return NS_ERROR_ALREADY_INITIALIZED;
  mChoosing = PR_TRUE;
  return NS_OK;
}

NS_IMETHODIMP
aaSessionHistory::SetFile(nsIFile *file)
{ 
  mFile = file;
  if (!mFile)
    mSession = nsnull;

  return NS_OK;
}

NS_IMETHODIMP
aaSessionHistory::GetQueryBuffer(nsISupports * *aQueryBuffer)
{
  nsresult rv;
  nsCOMPtr<aaIPageView> pv1 = getter_AddRefs( getDestination(0, rv) );
  NS_ENSURE_TRUE(pv1, rv);

  return pv1->GetQueryChoice(aQueryBuffer);
}

NS_IMETHODIMP
aaSessionHistory::GetChoice(nsISupports * *aChoice)
{
  nsresult rv;
  nsCOMPtr<aaIPageView> pv0 = getter_AddRefs( getDestination(-1, rv) );
  NS_ENSURE_TRUE(pv0, rv);

  return pv0->GetQueryChoice(aChoice);
}
NS_IMETHODIMP
aaSessionHistory::SetChoice(nsISupports * aChoice)
{
  nsresult rv;
  nsCOMPtr<aaIPageView> pv0 = getter_AddRefs( getDestination(-1, rv) );
  NS_ENSURE_TRUE(pv0, rv);

  return pv0->SetQueryChoice(aChoice);
}

NS_IMETHODIMP
aaSessionHistory::CreateTreeView(const char *aContract, nsITreeView * *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  nsCOMPtr<nsIComponentManager> compMgr;
  nsresult rv = NS_GetComponentManager(getter_AddRefs(compMgr));
  if (compMgr)
    rv = compMgr->CreateInstanceByContractID(aContract, get_mSession(),
        NS_GET_IID(nsITreeView), (void **) _retval);

  return rv;
}

/* Private methods */
aaIPageView *
aaSessionHistory::getDestination(PRInt32 offset, nsresult &rv)
{
  PRInt32 index;
  rv = mSH->GetIndex(&index);
  NS_ENSURE_SUCCESS(rv, nsnull);

  nsCOMPtr<nsIHistoryEntry> he0;
  rv = mSH->GetEntryAtIndex(index + offset, PR_FALSE, getter_AddRefs( he0 ));
  NS_ENSURE_SUCCESS(rv, nsnull);

  aaIPageView *pv0;
  rv = CallQueryInterface(he0, &pv0);
  return pv0;
}

aaISession *
aaSessionHistory::get_mSession()
{ 
  nsresult rv;
  if  (!mSession) {
    mSession = do_CreateInstance("@aasii.org/storage/session;1", mFile, &rv);
    NS_ENSURE_SUCCESS(rv, nsnull);
  }
  return (mSession);
}
