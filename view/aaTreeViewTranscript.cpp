/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsIAtom.h"
#include "nsIAtomService.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIResource.h"
#include "aaIFact.h"
#include "aaIBalance.h"
#include "aaITransaction.h"
#include "aaISqlTransaction.h"
#include "aaITranscript.h"
#include "aaReportKeys.h"
#include "aaViewUtils.h"
#include "aaTreeViewTranscript.h"

aaTreeViewTranscript::aaTreeViewTranscript()
{
  mComplexLoader = do_CreateInstance(AA_TRANSCRIPT_CONTRACT);
}

aaTreeViewTranscript::~aaTreeViewTranscript()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewTranscript, aaTreeView)

/* Helpers */
PRBool
chooseSide(aaIFact *fact, PRInt64 ownId)
{
  if (! fact)
    return PR_FALSE;

  if (ownId) {
    if (fact->PickTakeFrom() && fact->PickTakeFrom()->PickId() == ownId)
      return PR_FALSE;
    else
      return PR_TRUE;
  }

  if (fact->PickGiveTo() && fact->PickGiveTo()->PickId())
    return PR_FALSE;
  return PR_TRUE;
}

nsresult
aaTreeViewTranscript::getColumn0(aaITransaction *txn, PRInt64 flowId,
    PRUint32 flags, nsAString & _retval)
{
  if (! txn->PickFact())
    return NS_OK;

  assignDate(_retval, txn->PickFact()->PickTime());
  return NS_OK;
}

nsresult
aaTreeViewTranscript::getColumn1(aaITransaction *txn, PRInt64 flowId,
    PRUint32 flags, nsAString & _retval)
{
  if (! txn->PickFact())
    return NS_OK;

  aaIFlow *flow = nsnull;
  if (chooseSide(txn->PickFact(), flowId)) {
    flow = txn->PickFact()->PickTakeFrom();
  } else {
    flow = txn->PickFact()->PickGiveTo();
  }

  if (flow)
    flow->GetTag(_retval);

  return NS_OK;
}

nsresult
aaTreeViewTranscript::getColumn2(aaITransaction *txn, PRInt64 flowId,
    PRUint32 flags, nsAString & _retval)
{
  if (! txn->PickFact())
    return NS_OK;

  if (flowId) {
    if (chooseSide(txn->PickFact(), flowId)) {
      if (flags)
        assignDouble(_retval, txn->PickValue() + (txn->PickHasEarnings() ?
              txn->PickEarnings() : 0.0));
      else
        assignDouble(_retval, txn->PickFact()->PickAmount());
    }
  } else if (txn->PickEarnings() < 0.0) {
      assignDouble(_retval, -txn->PickEarnings());
  }

  return NS_OK;
}

nsresult
aaTreeViewTranscript::getColumn3(aaITransaction *txn, PRInt64 flowId,
    PRUint32 flags, nsAString & _retval)
{
  if (! txn->PickFact())
    return NS_OK;

  if (flowId) {
    if (! chooseSide(txn->PickFact(), flowId)) {
      if (flags)
        assignDouble(_retval, txn->PickValue());
      else
        assignDouble(_retval, txn->PickFact()->PickAmount());
    }
  } else if (txn->PickEarnings() > 0.0) {
      assignDouble(_retval, txn->PickEarnings());
  }

  return NS_OK;
}

nsresult (*const aaTreeViewTranscript::colFuncs[])(aaITransaction *,
    PRInt64 flowId, PRUint32 flags, nsAString &) =
{
  &aaTreeViewTranscript::getColumn0
  ,&aaTreeViewTranscript::getColumn1
  ,&aaTreeViewTranscript::getColumn2
  ,&aaTreeViewTranscript::getColumn3
};

/* Private functions */
nsresult
aaTreeViewTranscript::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  nsresult rv;
  _retval.Truncate();

  PRInt32 ci = getColumnIndex(col, sizeof(colFuncs));
  NS_ENSURE_TRUE(ci >= 0, NS_OK);

  nsCOMPtr<aaITransaction> txn(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(txn, NS_OK);

  nsCOMPtr<aaITranscript> transcript(do_QueryInterface(mDataSet, &rv));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIFlow> flow;
  rv = transcript->GetFlow(getter_AddRefs(flow));
  NS_ENSURE_TRUE(flow, rv);

  if (!flow)
    return NS_OK;
  return colFuncs[ci](txn,flow->PickId(), mFlags, _retval);
}
