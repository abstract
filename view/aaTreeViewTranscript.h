/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWTRANSCRIPT_H
#define AATREEVIEWTRANSCRIPT_H 1

#define AA_TREEVIEWTRANSCRIPT_CID \
{0x46950acc, 0xb1b4, 0x4d04, {0xa4, 0x49, 0xf0, 0x63, 0x06, 0xaa, 0x9e, 0x68}}
#define AA_TREEVIEWTRANSCRIPT_CONTRACT "@aasii.org/view/tree-transcript;1"

#include "aaTreeView.h"

class aaITransaction;
class nsAString;

class aaTreeViewTranscript : public aaTreeView
{
public:
  aaTreeViewTranscript();
  virtual ~aaTreeViewTranscript();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }

private:
  friend class aaViewTest;

  static nsresult (*const colFuncs[])(aaITransaction *, PRInt64 flowId,
      PRUint32 flags, nsAString &);
  static nsresult getColumn0(aaITransaction *txn, PRInt64 flowId,
      PRUint32 flags, nsAString & _retval);
  static nsresult getColumn1(aaITransaction *txn, PRInt64 flowId,
      PRUint32 flags, nsAString & _retval);
  static nsresult getColumn2(aaITransaction *txn, PRInt64 flowId,
      PRUint32 flags, nsAString & _retval);
  static nsresult getColumn3(aaITransaction *txn, PRInt64 flowId,
      PRUint32 flags, nsAString & _retval);

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
};

#endif /* AATREEVIEWTRANSCRIPT_H */

