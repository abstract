/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AALOADGROUP_H
#define AALOADGROUP_H 1

#include "nsCOMPtr.h"
#include "nsWeakPtr.h"
#include "nsILoadGroup.h"
#include "nsIDocShell.h"
#include "nsIRunnable.h"
#include "nsWeakReference.h"

#define NS_FORWARD_NSILOADGROUP_R(_to) \
  NS_SCRIPTABLE NS_IMETHOD GetGroupObserver(nsIRequestObserver * *aGroupObserver) { return _to GetGroupObserver(aGroupObserver); } \
  NS_SCRIPTABLE NS_IMETHOD SetGroupObserver(nsIRequestObserver * aGroupObserver) { return _to SetGroupObserver(aGroupObserver); } \
  NS_SCRIPTABLE NS_IMETHOD GetDefaultLoadRequest(nsIRequest * *aDefaultLoadRequest) { return _to GetDefaultLoadRequest(aDefaultLoadRequest); } \
  NS_SCRIPTABLE NS_IMETHOD SetDefaultLoadRequest(nsIRequest * aDefaultLoadRequest) { return _to SetDefaultLoadRequest(aDefaultLoadRequest); } \
  NS_SCRIPTABLE NS_IMETHOD GetRequests(nsISimpleEnumerator * *aRequests) { return _to GetRequests(aRequests); } \
  NS_SCRIPTABLE NS_IMETHOD GetActiveCount(PRUint32 *aActiveCount) { return _to GetActiveCount(aActiveCount); } \
  NS_SCRIPTABLE NS_IMETHOD GetNotificationCallbacks(nsIInterfaceRequestor * *aNotificationCallbacks) { return _to GetNotificationCallbacks(aNotificationCallbacks); } \
  NS_SCRIPTABLE NS_IMETHOD SetNotificationCallbacks(nsIInterfaceRequestor * aNotificationCallbacks) { return _to SetNotificationCallbacks(aNotificationCallbacks); } 

class aaLoadGroup : public nsILoadGroup,
                    public nsSupportsWeakReference
{ 
public:
  aaLoadGroup(nsIDocShell *docShell);
  NS_DECL_ISUPPORTS
  NS_FORWARD_NSIREQUEST(mContained->)
  NS_FORWARD_NSILOADGROUP_R(mContained->)
  NS_SCRIPTABLE NS_IMETHOD AddRequest(nsIRequest *aRequest,
      nsISupports *aContext);
  NS_SCRIPTABLE NS_IMETHOD RemoveRequest(nsIRequest *aRequest,
      nsISupports *aContext, nsresult aStatus);

  nsresult Init(const PRUnichar *aURI, PRUint32 aLoadFlags, nsIURI *aReferrer,
      nsIInputStream *aPostData, nsIInputStream *aHeaders);
private:
  virtual ~aaLoadGroup() {}
  friend class aaViewTest;

  nsWeakPtr mDocShell;
  nsCOMPtr<nsILoadGroup> mContained;
  nsCOMPtr<nsIRunnable> mTask;

  PRInt32 mCount;
  nsresult mStatus;

  void checkPresentation(nsIDocShell *docShell);
};

#endif /* AALOADGROUP_H */

