/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsIStringBundle.h"

/* Project includes */
#include "aaIResource.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"
#include "aaIOrderCondition.h"
#include "aaConditionKeys.h"
#include "aaISqlFilter.h"
#include "aaTreeViewResource.h"

aaTreeViewResource::aaTreeViewResource()
{
  mDataLoader = do_CreateInstance(AA_LOADRESOURCE_CONTRACT);
}

aaTreeViewResource::~aaTreeViewResource()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewResource, aaTreeView)

/* Private functions */
nsresult
aaTreeViewResource::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  _retval.Truncate();
  nsresult rv;
  NS_ENSURE_TRUE(col, NS_ERROR_INVALID_ARG);

  PRInt32 ci;
  rv = col->GetIndex(&ci);
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_SUCCESS(ci <= 1, NS_OK);

  nsCOMPtr<aaIResource> resource(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  if (!resource)
    return NS_OK;

  switch (ci) {
  case 0:
    return getTypeColumnText(resource, _retval);
  case 1:
    return resource->GetTag(_retval);
  default:
    break;
  }

  return NS_OK;
}

nsresult
aaTreeViewResource::getTypeColumnText(aaIResource *aResource,
    nsAString & _retval)
{
  nsresult rv;
  rv = ensureState();
  NS_ENSURE_SUCCESS(rv, rv);

  nsAutoString w, key;
  switch (aResource->PickType()) {
  case aaIResource::TYPE_MONEY:
    key = NS_LITERAL_STRING("resource.type.money");
    break;
  case aaIResource::TYPE_ASSET:
    key = NS_LITERAL_STRING("resource.type.asset");
    break;
  default:
    break;
  }

  rv = mBundle->GetStringFromName(key.get(), getter_Copies(w));
  NS_ENSURE_SUCCESS(rv, rv);

  _retval.Assign(w);
  return NS_OK;
}

nsresult
aaTreeViewResource::ensureState()
{
  nsresult rv;
  if (!mBundle) {
    nsCOMPtr<nsIStringBundleService> heap = do_GetService(
        NS_STRINGBUNDLE_CONTRACTID, &rv);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = heap->CreateBundle("chrome://abstract/locale/resource.properties",
        getter_AddRefs(mBundle));
    NS_ENSURE_SUCCESS(rv, rv);
  }

  return NS_OK;
}

NS_IMETHODIMP
  aaTreeViewResource::convertSortColumnToString(nsITreeColumn* col, nsACString& name)
{
  nsresult rc = NS_OK;
  PRInt32 idx = -1;

  NS_ENSURE_ARG_POINTER(col);

  name.Truncate();

  rc = col->GetIndex(&idx);
  NS_ENSURE_SUCCESS(rc, rc);

  switch (idx) {
  case 0:
    name = NS_LITERAL_CSTRING("resource.type");
    break;
  case 1:
    name = NS_LITERAL_CSTRING("resource.tag");
    break;
  default:
    name.Truncate();
    return NS_ERROR_INVALID_ARG;
  }
  return rc;
}
