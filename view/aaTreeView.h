/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEW_H
#define AATREEVIEW_H 1

#include "aaIDataTreeView.h"
#include "aaITreeViewFilters.h"
#include "nsCOMPtr.h"

class nsITreeBoxObject;
class nsITreeSelection;
class aaISession;
class aaISqlRequest;
class aaISqlTransaction;
class nsIArray;
class nsIDOMDocument;

class aaIOrderCondition;
class aaIFilterCondition;
class aaISqlFilter;

class aaTreeView
  : public aaIDataTreeView
  , public aaITreeFilterObserver
{
public:
  aaTreeView();
  NS_DECL_ISUPPORTS
  NS_DECL_NSITREEVIEW
  NS_DECL_AAIDATATREEVIEW
  NS_DECL_AAITREEFILTEROBSERVER

  nsresult Init(nsISupports *aParam);
protected:
  ~aaTreeView();

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col, nsAString & _retval) = 0;
  virtual nsresult postLoadHandler();
  PRInt32 getColumnIndex(nsITreeColumn *col, PRUint32 colPtrSz);

  NS_IMETHOD GetOldRowCount(PRInt32* aRowCount);
  NS_IMETHOD RowCountChanged(PRInt32 aOldRowCount, PRInt32 aNewRowCount);

  //filtering
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name) = 0;
  NS_IMETHOD buildFilter(nsISupports* aParam);
  nsresult createOrderCondition(nsITreeColumn* col, const nsAString& param, aaIOrderCondition** _retval);
  nsresult createFilterCondition(nsITreeColumn* col, const nsAString& param, aaIFilterCondition** _retval);
  //end filtering

  //events
  NS_IMETHOD dataLoadSuccess();
  //end events
protected:
  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<nsITreeBoxObject> mTree;
  nsCOMPtr<nsITreeSelection> mSelection;
  nsCOMPtr<aaISqlTransaction> mComplexLoader;
  nsCOMPtr<aaISqlRequest> mDataLoader;
  nsCOMPtr<nsIArray> mDataSet;
  PRUint32 mFlags;
  PRInt32 mOldRowCount;
  nsCOMPtr<aaITreeViewFilters> mFilters;
  nsCOMPtr<aaISqlFilter> mFilter;
};

#endif /* AATREEVIEW_H */

