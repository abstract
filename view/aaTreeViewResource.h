/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWRESOURCE_H
#define AATREEVIEWRESOURCE_H 1

#define AA_TREEVIEWRESOURCE_CID \
{0x9432ddea, 0x9448, 0x426f, {0xa4, 0x35, 0xa3, 0x50, 0x2b, 0x4a, 0x0c, 0xb0}}
#define AA_TREEVIEWRESOURCE_CONTRACT "@aasii.org/view/tree-resource;1"

#include "aaTreeView.h"

class aaIResource;
class nsIStringBundle;

class aaTreeViewResource : public aaTreeView
{
public:
  aaTreeViewResource();
  virtual ~aaTreeViewResource();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name);
private:
  friend class aaViewTest;

  nsCOMPtr<nsIStringBundle> mBundle;

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
  nsresult ensureState();
  nsresult getTypeColumnText(aaIResource *aResource, nsAString & _retval);
};

#endif /* AATREEVIEWRESOURCE_H */

