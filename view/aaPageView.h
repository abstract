/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AAPAGEVIEW_H
#define AAPAGEVIEW_H 1

#define AA_PAGEVIEW_CID \
{0xf6f1bb39, 0xab1d, 0x409b, {0x80, 0x7d, 0x4d, 0x16, 0x01, 0x4f, 0x49, 0xf9}}
#define AA_PAGEVIEW_CONTRACT "@aasii.org/view/page-view;1"

#include "aaIPageView.h"

class aaIDataNode;
#ifdef DEBUG
#include "aaIDataNode.h"
#endif

class aaPageView : public aaIPageView,
                   public nsISHContainer
{ 
public:
  aaPageView() :mIndex(-1), mChoosingSet(PR_FALSE), mChoosing(PR_FALSE) {}
  NS_DECL_ISUPPORTS
  NS_FORWARD_NSIHISTORYENTRY(mSHE->)
  NS_FORWARD_NSISHENTRY(mSHE->)
  NS_FORWARD_NSISHCONTAINER(mSHC->)
  NS_DECL_AAIPAGEVIEW

  nsresult Init(nsISupports *aOuter);
private:
  virtual ~aaPageView() {}
  friend class aaViewTest;

  nsCOMPtr<nsISHEntry> mSHE;
  nsCOMPtr<nsISHContainer> mSHC;

  nsCOMPtr<aaIDataNode> mBuffer;
  PRInt32 mIndex;
  nsCOMPtr<nsISupports> mQueryChoice;
  PRBool mChoosingSet;
  PRBool mChoosing;
};

#endif /* AAPAGEVIEW_H */

