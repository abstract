/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsStringAPI.h"
#include "nsIDOMElement.h"
#include "nsIDOMNode.h"
#include "nsIDOMDocument.h"
#include "nsIDOMXULElement.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsISHistory.h"
#include "nsServiceManagerUtils.h"
#include "nsIObserverService.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */
#include "nsITreeSelection.h"
#include "nsITreeColumns.h"
#include "nsITreeBoxObject.h"
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsClassHashtable.h"

/* Project includes */
#include "aaTreeView.h"
#include "aaISession.h"
#include "aaISqlTransaction.h"
#include "aaISqlRequest.h"
#include "aaBaseLoaders.h"

#include "aaIOrderCondition.h"
#include "aaIFilterCondition.h"
#include "aaConditionKeys.h"
#include "aaISqlFilter.h"

/*******************************************************************/
class CTreeViewFilters : public aaITreeViewFilters
{
public:
  class CTreeViewFilter
  {
  public:
    CTreeViewFilter(nsITreeColumn* col, const nsAString& param)
      : mColumn(col)
      , mParam(param)
    {
    }
    ~CTreeViewFilter() {
    }
  public:
    NS_IMETHOD GetColumn(nsITreeColumn** col) {
      NS_ENSURE_ARG_POINTER(col);
      NS_IF_ADDREF(*col = mColumn);
      return NS_OK;
    }
    NS_IMETHOD GetParam(nsAString& param) {
      param.Truncate();
      param = mParam;
      return NS_OK;
    }
  protected:
    nsCOMPtr<nsITreeColumn> mColumn;
    nsAutoString mParam;
  };
public:
  CTreeViewFilters();
  NS_DECL_ISUPPORTS
  NS_DECL_AAITREEVIEWFILTERS
protected:
  ~CTreeViewFilters();
protected:
  typedef nsClassHashtable<nsUint32HashKey, CTreeViewFilter> aaFilterHashtable;
  aaFilterHashtable mContainer;
};

NS_IMPL_ISUPPORTS1(CTreeViewFilters, aaITreeViewFilters)

CTreeViewFilters::CTreeViewFilters() {
  mContainer.Init();
}

CTreeViewFilters::~CTreeViewFilters() {
}

NS_IMETHODIMP
  CTreeViewFilters::AddFilter(PRInt16 type, nsITreeColumn* col, const nsAString& param)
{
  NS_ENSURE_ARG_POINTER(col);
  if (!mContainer.Put(PRUint32(type), new CTreeViewFilter(col, param))) {
    return NS_ERROR_FAILURE;
  }
  return NS_OK;
}

NS_IMETHODIMP
  CTreeViewFilters::Clear()
{
  mContainer.Clear();
  return NS_OK;
}

PLDHashOperator EnumFunc(const PRUint32& type, nsAutoPtr<CTreeViewFilters::CTreeViewFilter>& filter, void* userArg)
{
  if (nsnull != userArg) {
    nsCOMPtr<nsITreeColumn> col;
    nsAutoString param;
    aaITreeFilterObserver* pObserver = (aaITreeFilterObserver*)userArg;
    if (NS_SUCCEEDED(filter->GetColumn(getter_AddRefs(col))) && NS_SUCCEEDED(filter->GetParam(param))) {
      PRBool moveNext = PR_TRUE;
      if (NS_SUCCEEDED(pObserver->Next(PRInt16(type), col, param, &moveNext))) {
        return moveNext ? PL_DHASH_NEXT : PL_DHASH_STOP;
      }
    }
  }
  return PL_DHASH_STOP;
}

NS_IMETHODIMP
  CTreeViewFilters::Observe(aaITreeFilterObserver *observer)
{
  NS_ENSURE_ARG_POINTER(observer);
  mContainer.Enumerate((aaFilterHashtable::EnumFunction)&EnumFunc, observer);
  return NS_OK;
}

NS_IMETHODIMP
  CTreeViewFilters::IsExist(PRInt16 type, PRBool *_retval NS_OUTPARAM)
{
  nsresult rc = NS_OK;
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = PR_FALSE;
  CTreeViewFilter* pFilter = nsnull;
  if (mContainer.Get(type, &pFilter)) {
    *_retval = PR_TRUE;
  }
  return rc;
}

NS_IMETHODIMP
  CTreeViewFilters::GetFilter(PRInt16 type, nsITreeColumn **col NS_OUTPARAM, nsAString & param NS_OUTPARAM)
{
  nsresult rc = NS_OK;
  CTreeViewFilter* pFilter = nsnull;
  NS_ENSURE_ARG_POINTER(col);
  if (!mContainer.Get(type, &pFilter)) {
    return NS_ERROR_INVALID_ARG;
  }
  param.Truncate();
  NS_ENSURE_SUCCESS(rc = pFilter->GetColumn(col), rc);
  NS_ENSURE_SUCCESS(rc = pFilter->GetParam(param), rc);
  return rc;
}
/*******************************************************************/

aaTreeView::aaTreeView()
  : mFlags(0)
  , mOldRowCount(0)
  , mFilters(nsnull)
{
}

aaTreeView::~aaTreeView()
{
}

nsresult
aaTreeView::Init(nsISupports *aParam)
{
  nsresult rv;
  mSession = do_QueryInterface(aParam, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMPL_ISUPPORTS3(aaTreeView,
                   aaIDataTreeView,
                   nsITreeView,
                   aaITreeFilterObserver)

NS_IMETHODIMP
aaTreeView::GetRowCount(PRInt32 *aRowCount)
{
  NS_ENSURE_ARG_POINTER(aRowCount);
  *aRowCount = 0;
  if (! mDataSet)
    return NS_OK;

  return mDataSet->GetLength((PRUint32 *) aRowCount);
}

NS_IMETHODIMP
aaTreeView::GetSelection(nsITreeSelection * *aSelection)
{
  NS_ENSURE_ARG_POINTER( aSelection );
  NS_IF_ADDREF(*aSelection = mSelection);
  return NS_OK;
}
NS_IMETHODIMP
aaTreeView::SetSelection(nsITreeSelection * aSelection)
{
  mSelection = aSelection;

  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::GetRowProperties(PRInt32 index, nsISupportsArray *properties)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetCellProperties(PRInt32 row, nsITreeColumn *col, nsISupportsArray *properties)
{
  return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetColumnProperties(nsITreeColumn *col, nsISupportsArray *properties)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::IsContainer(PRInt32 index, PRBool *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = PR_FALSE;
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::IsContainerOpen(PRInt32 index, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::IsContainerEmpty(PRInt32 index, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::IsSeparator(PRInt32 index, PRBool *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = PR_FALSE;
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::IsSorted(PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::CanDrop(PRInt32 index, PRInt32 orientation,
    nsIDOMDataTransfer *dataTransfer, PRBool *_retval NS_OUTPARAM)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::Drop(PRInt32 row, PRInt32 orientation,
    nsIDOMDataTransfer *dataTransfer)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetParentIndex(PRInt32 rowIndex, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::HasNextSibling(PRInt32 rowIndex, PRInt32 afterIndex, PRBool *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  nsresult rv;
  PRInt32 count;
  rv = GetRowCount(&count);
  NS_ENSURE_SUCCESS(rv, rv);
  if (rowIndex == afterIndex && rowIndex < count) {
    *_retval = PR_TRUE;
  } else {
    *_retval = PR_FALSE;
  }
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::GetLevel(PRInt32 index, PRInt32 *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = 0;
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::GetImageSrc(PRInt32 row, nsITreeColumn *col, nsAString & _retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetProgressMode(PRInt32 row, nsITreeColumn *col, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetCellValue(PRInt32 row, nsITreeColumn *col, nsAString & _retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::GetCellText(PRInt32 row, nsITreeColumn *col, nsAString & _retval)
{
  /** 
   * There may be a bug in <tree> implementation. This function is
   * occasionally called after mTree is detached by SetTree(nsnull);
   */
  NS_ENSURE_TRUE(mDataSet, NS_ERROR_NOT_INITIALIZED);
  NS_ENSURE_TRUE(row >= 0, NS_ERROR_INVALID_ARG);
  return doGetCellText(row, col, _retval);
}

NS_IMETHODIMP
aaTreeView::SetTree(nsITreeBoxObject *tree)
{
  mTree = tree;
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::ToggleOpenState(PRInt32 index)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::CycleHeader(nsITreeColumn *col)
{
  //when column clicked, method was called by treecol handler
  return NS_OK;//NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::SelectionChanged()
{
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::CycleCell(PRInt32 row, nsITreeColumn *col)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::IsEditable(PRInt32 row, nsITreeColumn *col, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::IsSelectable(PRInt32 row, nsITreeColumn *col, PRBool *_retval)
{
  NS_ENSURE_ARG_POINTER(_retval);
  *_retval = PR_TRUE;
  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::SetCellValue(PRInt32 row, nsITreeColumn *col, const nsAString & value)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::SetCellText(PRInt32 row, nsITreeColumn *col, const nsAString & value)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::PerformAction(const PRUnichar *action)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::PerformActionOnRow(const PRUnichar *action, PRInt32 row)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP
aaTreeView::PerformActionOnCell(const PRUnichar *action, PRInt32 row, nsITreeColumn *col)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* aaIDataTreeView */
NS_IMETHODIMP
aaTreeView::GetDataSet(nsIArray * * aDataSet)
{
  NS_ENSURE_ARG_POINTER(aDataSet);
  NS_IF_ADDREF(*aDataSet = mDataSet);
  return NS_OK;
}

NS_IMETHODIMP aaTreeView::GetOldRowCount(PRInt32* aRowCount) {
  *aRowCount = mOldRowCount;
  return NS_OK;
}

NS_IMETHODIMP aaTreeView::RowCountChanged(PRInt32 aOldRowCount, PRInt32 aNewRowCount) {
  if (mOldRowCount == aOldRowCount) {
    mOldRowCount = aNewRowCount;
    return NS_OK;
  }
  return NS_ERROR_INVALID_ARG;
}

NS_IMETHODIMP
aaTreeView::Update(nsISupports *aParam = 0)
{
  NS_ENSURE_TRUE(mDataLoader || mComplexLoader, NS_ERROR_NOT_INITIALIZED);
  nsresult rv;
  PRInt32 oldCount = 0;
  PRInt32 newRowCount = 0;


  rv = GetOldRowCount(&oldCount);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mTree) {
    mTree->RowCountChanged(0, - oldCount);
  }

  rv = buildFilter(aParam);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mDataLoader) {
    rv = mDataLoader->SetFilter(mFilter);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Load(mDataLoader, getter_AddRefs(mDataSet));
    NS_ENSURE_SUCCESS(rv, rv);

    rv = postLoadHandler();
    NS_ENSURE_SUCCESS(rv, rv);
  }
  else {
    nsCOMPtr<nsISupports> supReturn;

    rv = mComplexLoader->SetParam(mFilter);
    NS_ENSURE_SUCCESS(rv, rv);

    rv = mSession->Execute(mComplexLoader, getter_AddRefs(supReturn));
    NS_ENSURE_SUCCESS(rv, rv);
    NS_ENSURE_TRUE(supReturn, NS_ERROR_UNEXPECTED);

    rv = supReturn->QueryInterface(NS_GET_IID(nsIArray), getter_AddRefs(mDataSet));
    NS_ENSURE_SUCCESS(rv, rv);
  }

  rv = GetRowCount(&newRowCount);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = RowCountChanged(oldCount, newRowCount);
  NS_ENSURE_SUCCESS(rv, rv);

  if (mTree) {
    mTree->RowCountChanged(0, newRowCount);
  }

  nsCOMPtr<nsIObserverService> broadcaster = do_GetService("@mozilla.org/observer-service;1");
  NS_ENSURE_TRUE(broadcaster, NS_OK);
  broadcaster->NotifyObservers(reinterpret_cast<nsISupports*>(this), "tree-view-load", nsnull);

  rv = dataLoadSuccess();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

NS_IMETHODIMP
aaTreeView::GetFlags(PRUint32 *aFlags)
{
  NS_ENSURE_ARG_POINTER(aFlags);
  *aFlags = mFlags;
  return NS_OK;
}
NS_IMETHODIMP
aaTreeView::SetFlags(PRUint32 aFlags)
{
  PRBool needUpdate = (aFlags != mFlags);
  mFlags = aFlags;
  if (NS_LIKELY( needUpdate && mTree )) {
    mTree->Invalidate();
    nsCOMPtr<nsIObserverService> broadcaster = do_GetService(
        "@mozilla.org/observer-service;1");
    NS_ENSURE_TRUE(broadcaster, NS_OK);
    broadcaster->NotifyObservers(reinterpret_cast<nsISupports*>(this), "tree-flags-change", nsnull);
  }
  return NS_OK;
}

/* Protected methods */
nsresult
aaTreeView::postLoadHandler()
{
  return NS_OK;
}

PRInt32
aaTreeView::getColumnIndex(nsITreeColumn *col, PRUint32 colPtrSz)
{
  nsresult rv;
  NS_ENSURE_TRUE(col, -1);

  PRInt32 ci;
  rv = col->GetIndex(&ci);
  NS_ENSURE_SUCCESS(rv, -1);

  NS_ENSURE_TRUE(ci >= 0 && ci < (PRInt32) (colPtrSz / sizeof(int *)), -1);

  return ci;
}

NS_IMETHODIMP
  aaTreeView::GetFilters(aaITreeViewFilters **aFilters)
{
  NS_ENSURE_ARG_POINTER(aFilters);
  if (!mFilters) {
    //mFilters
    (new CTreeViewFilters)->QueryInterface(NS_GET_IID(aaITreeViewFilters), getter_AddRefs(mFilters));
    NS_ENSURE_TRUE(mFilters, NS_ERROR_NULL_POINTER);
  }
  if (NS_IF_ADDREF(mFilters)) {
    *aFilters = mFilters;
  } else {
    *aFilters = nsnull;
    return NS_ERROR_FAILURE;
  }
  return NS_OK;
}

NS_IMETHODIMP
  aaTreeView::Next(PRInt16 type, nsITreeColumn *col, const nsAString & param, PRBool *_retval)
{
  nsresult rc = NS_OK;
  NS_ENSURE_ARG_POINTER(col);
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_TRUE(mFilter, NS_ERROR_NOT_INITIALIZED);
  *_retval = PR_FALSE;
  if (PRInt16(aaITreeViewFilters::FILTER_SORT) == type) {
    nsCOMPtr<aaIOrderCondition> order;
    nsCAutoString name;
    //do create order condition
    rc = createOrderCondition(col, param, getter_AddRefs(order));
    NS_ENSURE_SUCCESS(rc, rc);
    //do get condition name
    rc = order->GetConditionName(name);
    NS_ENSURE_SUCCESS(rc, rc);
    //do add order to filter
    rc = mFilter->AddParam(name, order);
    NS_ENSURE_SUCCESS(rc, rc);
  } else if(PRInt16(aaITreeViewFilters::FILTER_FILTER) == type) {
    nsCOMPtr<aaIFilterCondition> filter;
    nsCAutoString name;
    //do create order condition
    rc = createFilterCondition(col, param, getter_AddRefs(filter));
    NS_ENSURE_SUCCESS(rc, rc);
    //do get condition name
    rc = filter->GetConditionName(name);
    NS_ENSURE_SUCCESS(rc, rc);
    //do add order to filter
    rc = mFilter->AddParam(name, filter);
    NS_ENSURE_SUCCESS(rc, rc);
  } else {
    return NS_ERROR_INVALID_ARG;
  }
  *_retval = PR_TRUE;
  return rc;
}

NS_IMETHODIMP
  aaTreeView::buildFilter(nsISupports* aParam)
{
  nsresult rc = NS_OK;
  if (mFilter) {
    mFilter = nsnull;
  }

  if (mFilters || aParam) {
    mFilter = do_CreateInstance(AA_SQLFILTER_CONTRACT, &rc);
    NS_ENSURE_SUCCESS(rc, rc);
  }

  if (mFilters) {
    nsCOMPtr<aaITreeFilterObserver> pObserver;
    this->QueryInterface(NS_GET_IID(aaITreeFilterObserver), getter_AddRefs(pObserver));
    NS_ENSURE_SUCCESS(rc, rc);
    //do observe params
    rc = mFilters->Observe(pObserver);
    NS_ENSURE_SUCCESS(rc, rc);
  }

  if (aParam) {
    //add param to filter
    rc = mFilter->SetInterface(aParam->GetIID(), aParam);
    NS_ENSURE_SUCCESS(rc, rc);
  }
  return rc;
}

nsresult
  aaTreeView::createOrderCondition(nsITreeColumn* col, const nsAString& param, aaIOrderCondition** _retval)
{
  nsresult rc = NS_OK;
  nsCAutoString column;
  nsCOMPtr<aaIOrderCondition> order;

  NS_ENSURE_ARG_POINTER(col);
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_ARG_POINTER(!param.IsEmpty());

  order = do_CreateInstance(AA_ORDER_CONDITION_CONTRACTID, &rc);
  NS_ENSURE_SUCCESS(rc, rc);

  rc = convertSortColumnToString(col, column);
  NS_ENSURE_SUCCESS(rc, rc);

  rc = order->AddOrder(column, (param.Compare(NS_LITERAL_STRING("ascending")) == 0 ? PRInt16(aaIOrderCondition::SORT_ASC) : PRInt16(aaIOrderCondition::SORT_DESC)));
  NS_ENSURE_SUCCESS(rc, rc);

  if (NS_IF_ADDREF(order)) {
    *_retval = order;
  } else {
    *_retval = nsnull;
    return NS_ERROR_UNEXPECTED;
  }
  return rc;
}

nsresult
  aaTreeView::createFilterCondition(nsITreeColumn* col, const nsAString& param, aaIFilterCondition** _retval)
{
  nsresult rc = NS_OK;
  nsCAutoString column;
  nsCOMPtr<aaIFilterCondition> filter;

  NS_ENSURE_ARG_POINTER(col);
  NS_ENSURE_ARG_POINTER(_retval);
  NS_ENSURE_ARG_POINTER(!param.IsEmpty());

  filter = do_CreateInstance(AA_FILTER_CONDITION_CONTRACTID, &rc);
  NS_ENSURE_SUCCESS(rc, rc);

  rc = convertSortColumnToString(col, column);
  NS_ENSURE_SUCCESS(rc, rc);

  rc = filter->AddFilter(column, NS_ConvertUTF16toUTF8(param));
  NS_ENSURE_SUCCESS(rc, rc);

  if (NS_IF_ADDREF(filter)) {
    *_retval = filter;
  } else {
    *_retval = nsnull;
    return NS_ERROR_UNEXPECTED;
  }
  return rc;
}

NS_IMETHODIMP
  aaTreeView::dataLoadSuccess()
{
  return NS_OK;
}
