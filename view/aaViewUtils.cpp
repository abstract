/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "prtime.h"
#include "nsTextFormatter.h"

#include "aaViewUtils.h"

void
TruncateResult(nsAString &aStr)
{
  PRUint32 len = aStr.Length();
  if (! aStr[len - 1])
    aStr.SetLength(len - 1);
}

void
assignDouble(nsAString &aStr, double val)
{
  const PRUnichar *fmt = (PRUnichar *) L"%.2f";
  nsTextFormatter::ssprintf(aStr, fmt, val);
  TruncateResult(aStr);
}

void
assignSignificantDouble(nsAString &aStr, double val)
{
  const PRUnichar *fmt = (PRUnichar *) L"%f";
  nsTextFormatter::ssprintf(aStr, fmt, val);

  PRUint32 k=aStr.Length(), dot = 0, last = 0;
  while (--k) 
  {
    if (aStr[k]=='\0')
      continue;
    if (aStr[k]!='0' && !last)
      last = k;
    if (aStr[k]=='.')
     dot = k;
  }
  if (dot && last) {
    aStr.SetLength(last + 1);
  } else {
    TruncateResult(aStr);
  }
}

void
assignDate(nsAString &aStr, PRTime time)
{
  const PRUnichar *fmt = (PRUnichar *) L"%04u-%02u-%02u";
  PRExplodedTime tm;
  PR_ExplodeTime(time, &PR_LocalTimeParameters, &tm);
  nsTextFormatter::ssprintf(aStr, fmt, tm.tm_year, tm.tm_month + 1, tm.tm_mday);
  TruncateResult(aStr);
}
