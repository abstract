/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWENTITY_H
#define AATREEVIEWENTITY_H 1

#define AA_TREEVIEWENTITY_CID \
{0xcfe81592, 0xcfc5, 0x423f, {0xa0, 0xd7, 0x88, 0xae, 0x67, 0x3a, 0xc5, 0xee}}
#define AA_TREEVIEWENTITY_CONTRACT "@aasii.org/view/tree-entity;1"

#include "aaTreeView.h"

class aaTreeViewEntity : public aaTreeView
{
public:
  aaTreeViewEntity();
  virtual ~aaTreeViewEntity();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name);

  //events
  NS_IMETHOD dataLoadSuccess();
  //end events
private:
  friend class aaViewTest;

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
};

#endif /* AATREEVIEWENTITY_H */

