/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWLEDGER_H
#define AATREEVIEWLEDGER_H 1

#define AA_TREEVIEWLEDGER_CID \
{0xe4baa3c6, 0xf605, 0x48e4, {0xb2, 0xbc, 0x2c, 0x04, 0x2e, 0x99, 0x37, 0xd2}}
#define AA_TREEVIEWLEDGER_CONTRACT "@aasii.org/view/tree-general-ledger;1"

#include "aaTreeView.h"

class aaITransaction;
class nsACString;

class aaTreeViewLedger : public aaTreeView
{
public:
  aaTreeViewLedger();
  NS_DECL_ISUPPORTS_INHERITED

  NS_IMETHOD GetRowCount(PRInt32 *aRowCount);

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
private:
  virtual ~aaTreeViewLedger();
  friend class aaViewTest;

  static nsresult (*const colFuncs[])(aaITransaction *, PRUint32 flags,
      nsAString &);
  static nsresult getColumn0(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn1(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn2(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn3(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn4(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn5(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn6(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn7(aaITransaction *transaction, PRUint32 flags,
      nsAString & _retval);

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
  PRInt32 getIndexFromRow(PRInt32 row) const { return row / 2; }
  PRInt32 getRowFromIndex(PRInt32 row) const { return row * 2; }
  PRInt32 getOffsetFromRow(PRInt32 row) const {
    return row - getRowFromIndex(getIndexFromRow(row)); }
};

#endif /* AATREEVIEWLEDGER_H */

