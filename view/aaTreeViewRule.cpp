/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "aaTreeViewRule.h"
#include "aaBaseLoaders.h"
#include "aaIRule.h"
#include "aaIFlow.h"

#include "nsComponentManagerUtils.h"
#include "nsCOMPtr.h"
#include "nsStringAPI.h"
#include "aaISqlTransaction.h"
#include "aaViewUtils.h"
#include "nsCOMArray.h"
#include "nsIObserverService.h"
#include "nsServiceManagerUtils.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsIMutableArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsISupportsPrimitives.h"

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewRule, aaTreeView)

aaTreeViewRule::aaTreeViewRule() {
  mComplexLoader = do_CreateInstance(AA_LOADRULE_CONTRACT);
}

aaTreeViewRule::~aaTreeViewRule() {
}

nsresult
aaTreeViewRule::doGetCellText(PRInt32 row, nsITreeColumn *col, nsAString & _retval)
{
  nsresult rv;
  NS_ENSURE_TRUE(col, NS_ERROR_INVALID_ARG);

  PRInt32 ci;
  rv = col->GetIndex(&ci);
  NS_ENSURE_SUCCESS(rv, rv);
  if (ci > 3) {
    _retval.Truncate();
    return NS_OK;
  }

  nsCOMPtr<aaIRule> rule(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  switch(ci) {
    case 0:
      return rule->GetTag(_retval);
    case 1:
      {
        nsCOMPtr<aaIFlow> flowGive;
        rv = rule->GetGiveTo(getter_AddRefs(flowGive));
        NS_ENSURE_SUCCESS(rv, rv);
        if (!flowGive){
          _retval.Truncate();
          return NS_OK;
        }
        return flowGive->GetTag(_retval);
      }
    case 2:
      {
        nsCOMPtr<aaIFlow> flowTake;
        rv = rule->GetTakeFrom(getter_AddRefs(flowTake));
        NS_ENSURE_SUCCESS(rv, rv);
        if (!flowTake) {
          _retval.Truncate();
          return NS_OK;
        }
        return flowTake->GetTag(_retval);
      }
    case 3:
      {
        double rate;
        rv = rule->GetRate(&rate);
        NS_ENSURE_SUCCESS(rv, rv);
        assignSignificantDouble(_retval, rate);
        return NS_OK;
      }
  }

  _retval.Truncate();
  return NS_OK;
}

NS_IMETHODIMP
  aaTreeViewRule::convertSortColumnToString(nsITreeColumn* col, nsACString& name)
{
  nsresult rc = NS_OK;
  PRInt32 idx = -1;

  NS_ENSURE_ARG_POINTER(col);

  name.Truncate();

  rc = col->GetIndex(&idx);
  NS_ENSURE_SUCCESS(rc, rc);

  switch (idx) {
  case 0:
    name = NS_LITERAL_CSTRING("rule.tag");
    break;
  case 1:
    name = NS_LITERAL_CSTRING("rule.giveto.tag");
    break;
  case 2:
    name = NS_LITERAL_CSTRING("rule.takefrom.tag");
    break;
  case 3:
    name = NS_LITERAL_CSTRING("rule.rate");
    break;
  default:
    name.Truncate();
    return NS_ERROR_INVALID_ARG;
  }

  return rc;
}

NS_IMETHODIMP
  aaTreeViewRule::Update(nsISupports *aParam)
{
  nsresult rc = NS_OK;
  nsCOMPtr<aaIFlow> flow;
  NS_ENSURE_ARG_POINTER(aParam);

  flow = do_QueryInterface(aParam, &rc);
  NS_ENSURE_SUCCESS(rc, rc);
  if (mFilters && flow->PickId() < 0) {
    PRInt32 oldCount = 0;
    PRInt32 newRowCount = 0;

    rc = GetOldRowCount(&oldCount);
    NS_ENSURE_SUCCESS(rc, rc);

    if (mTree) {
      mTree->RowCountChanged(0, - oldCount);
    }
    {
      PRBool exist = PR_FALSE;
      nsCOMPtr<nsIArray> fArr;
      nsCOMPtr<nsIArray> sArr;
      //////////////////////////////////////////////////////////////////
      fArr = do_QueryInterface(flow, &rc);
      NS_ENSURE_SUCCESS(rc, rc);
      //////////////////////////////////////////////////////////////////
      rc = mFilters->IsExist(aaITreeViewFilters::FILTER_FILTER, &exist);
      NS_ENSURE_SUCCESS(rc, rc);
      if (exist) {
        nsCOMPtr<nsITreeColumn> column;
        nsAutoString param;
        //get filter
        rc = mFilters->GetFilter(aaITreeViewFilters::FILTER_FILTER, getter_AddRefs(column), param);
        NS_ENSURE_SUCCESS(rc, rc);
        //do filter
        rc = doFilterRules(fArr, column, param, getter_AddRefs(sArr));
        NS_ENSURE_SUCCESS(rc, rc);
        fArr = sArr;
      }
      rc = mFilters->IsExist(aaITreeViewFilters::FILTER_SORT, &exist);
      NS_ENSURE_SUCCESS(rc, rc);
      if (exist) {
        nsCOMPtr<nsITreeColumn> column;
        nsAutoString param;
        //get filter
        rc = mFilters->GetFilter(aaITreeViewFilters::FILTER_SORT, getter_AddRefs(column), param);
        NS_ENSURE_SUCCESS(rc, rc);
        //do sort
        rc = doSortRules(fArr, column, param, getter_AddRefs(sArr));
        NS_ENSURE_SUCCESS(rc, rc);
        fArr = sArr;
      }
      mDataSet = fArr;
    }
    rc = GetRowCount(&newRowCount);
    NS_ENSURE_SUCCESS(rc, rc);

    rc = RowCountChanged(oldCount, newRowCount);
    NS_ENSURE_SUCCESS(rc, rc);

    if (mTree) {
      mTree->RowCountChanged(0, newRowCount);
    }

    nsCOMPtr<nsIObserverService> broadcaster = do_GetService("@mozilla.org/observer-service;1");
    NS_ENSURE_TRUE(broadcaster, NS_OK);
    broadcaster->NotifyObservers(reinterpret_cast<nsISupports*>(this), "tree-view-load", nsnull);
  } else {
    return aaTreeView::Update(aParam);
  }
  return rc;
}

struct SSortInformation
{
  enum ESortType {
    E_ASC = 0,
    E_DESC = 1
  };
  ESortType mType;
  PRInt32 mSortColumn;
};

int comparator(aaIRule* aElement1, aaIRule* aElement2, void* aData)
{
  NS_ENSURE_TRUE(aElement1, 0);
  NS_ENSURE_TRUE(aElement2, 0);
  if (nsnull != aData) {
    SSortInformation* info = static_cast<SSortInformation*>(aData);
    switch (info->mSortColumn) {
      case 0:
        {
          nsAutoString tag1;
          nsAutoString tag2;
          NS_ENSURE_SUCCESS(aElement1->GetTag(tag1), 0);
          NS_ENSURE_SUCCESS(aElement2->GetTag(tag2), 0);
          return SSortInformation::E_ASC == info->mType ? tag1.Compare(tag2) : tag2.Compare(tag1);
        }
      case 1:
        {
          nsCOMPtr<aaIFlow> flow1;
          nsCOMPtr<aaIFlow> flow2;
          nsAutoString tag1;
          nsAutoString tag2;
          NS_ENSURE_SUCCESS(aElement1->GetGiveTo(getter_AddRefs(flow1)), 0);
          NS_ENSURE_SUCCESS(aElement2->GetGiveTo(getter_AddRefs(flow2)), 0);
          NS_ENSURE_SUCCESS(flow1->GetTag(tag1), 0);
          NS_ENSURE_SUCCESS(flow2->GetTag(tag2), 0);
          return SSortInformation::E_ASC == info->mType ? tag1.Compare(tag2) : tag2.Compare(tag1);
        }
      case 2:
        {
          nsCOMPtr<aaIFlow> flow1;
          nsCOMPtr<aaIFlow> flow2;
          nsAutoString tag1;
          nsAutoString tag2;
          NS_ENSURE_SUCCESS(aElement1->GetTakeFrom(getter_AddRefs(flow1)), 0);
          NS_ENSURE_SUCCESS(aElement2->GetTakeFrom(getter_AddRefs(flow2)), 0);
          NS_ENSURE_SUCCESS(flow1->GetTag(tag1), 0);
          NS_ENSURE_SUCCESS(flow2->GetTag(tag2), 0);
          return SSortInformation::E_ASC == info->mType ? tag1.Compare(tag2) : tag2.Compare(tag1);
        }
      case 3:
        {
          double rate1;
          double rate2;
          NS_ENSURE_SUCCESS(aElement1->GetRate(&rate1), 0);
          NS_ENSURE_SUCCESS(aElement2->GetRate(&rate2), 0);
          return SSortInformation::E_ASC == info->mType ? (rate1 >= rate2 ? 1 : -1) : (rate1 <= rate2 ? 1 : -1);
        }
    }
  }
  return 0;
}

nsresult
  aaTreeViewRule::doSortRules(nsIArray* pInput, nsITreeColumn* pColumn, const nsAString& param, nsIArray** _retval)
{
  nsresult rc = NS_OK;
  SSortInformation info;
  NS_ENSURE_ARG_POINTER(pInput);
  NS_ENSURE_ARG_POINTER(pColumn);
  NS_ENSURE_ARG_POINTER(_retval);
  if ((param.Compare(NS_LITERAL_STRING("ascending")) == 0 ? info.mType = SSortInformation::E_ASC,PR_TRUE : PR_FALSE) ||
    (param.Compare(NS_LITERAL_STRING("descending")) == 0 ? info.mType = SSortInformation::E_DESC,PR_TRUE: PR_FALSE)) {
    PRInt32 idx = -1;
    PRUint32 length = 0;
    nsCOMArray<aaIRule> rules;
    nsCOMPtr<nsIMutableArray> marr;
    nsCOMPtr<aaIRule> rule;

    rc = pColumn->GetIndex(&idx);
    NS_ENSURE_SUCCESS(rc, rc);
    //check idx
    NS_ENSURE_TRUE(idx >= 0 || idx <= 3, NS_ERROR_INVALID_ARG);
    //set idx
    info.mSortColumn = idx;
    //get length
    rc = pInput->GetLength(&length);
    NS_ENSURE_SUCCESS(rc, rc);
    if (length > 1) {
      for (PRUint32 i = 0;  i < length; ++ i) {
        rule = do_QueryElementAt(pInput, i, &rc);
        NS_ENSURE_SUCCESS(rc, rc);
        //append to com array
        if (!rules.AppendObject(rule)) {
          return NS_ERROR_UNEXPECTED;
        }
      }
      //do sort
      rules.Sort(nsCOMArray<aaIRule>::nsCOMArrayComparatorFunc(&comparator), &info);
      //do copy to array
      marr = do_CreateInstance("@mozilla.org/array;1", &rc);
      NS_ENSURE_SUCCESS(rc, rc);
      for (PRUint32 i = 0;  i < length; ++ i) {
        rc = marr->AppendElement(rules[i], PR_FALSE);
        NS_ENSURE_SUCCESS(rc, rc);
      }
      rc = marr->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
      NS_ENSURE_SUCCESS(rc, rc);
    } else {
      rc = pInput->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
      NS_ENSURE_SUCCESS(rc, rc);
    }
  } else {
    rc = pInput->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
    NS_ENSURE_SUCCESS(rc, rc);
  }
  return rc;
}

PRBool match(PRInt32 idx, aaIRule* pLeft, const nsACString& prm)
{
  nsCAutoString cUpperString;
  //to upper
  NS_ENSURE_TRUE(pLeft, PR_FALSE);
  if (!prm.IsEmpty()) {
    switch (idx) {
      case 0:
        {
          nsAutoString tag;
          nsCAutoString utf8Tag;
          NS_ENSURE_SUCCESS(pLeft->GetTag(tag), PR_FALSE);
          ToUpperCase(NS_ConvertUTF16toUTF8(tag), utf8Tag);
          return utf8Tag.Compare(prm) == 0 ? PR_TRUE : PR_FALSE;
        }
      case 1:
        {
          nsCOMPtr<aaIFlow> flow;
          nsAutoString tag;
          nsCAutoString utf8Tag;
          NS_ENSURE_SUCCESS(pLeft->GetGiveTo(getter_AddRefs(flow)), PR_FALSE);
          NS_ENSURE_SUCCESS(flow->GetTag(tag), PR_FALSE);
          ToUpperCase(NS_ConvertUTF16toUTF8(tag), utf8Tag);
          return utf8Tag.Compare(prm) == 0 ? PR_TRUE : PR_FALSE;
        }
      case 2:
        {
          nsCOMPtr<aaIFlow> flow;
          nsAutoString tag;
          nsCAutoString utf8Tag;
          NS_ENSURE_SUCCESS(pLeft->GetTakeFrom(getter_AddRefs(flow)), PR_FALSE);
          ToUpperCase(NS_ConvertUTF16toUTF8(tag), utf8Tag);
          return utf8Tag.Compare(prm) == 0 ? PR_TRUE : PR_FALSE;
        }
      case 3:
        {
          double rate;
          nsCAutoString utf8Tag;
          nsresult rc = NS_OK;
          static const int size = 32;
          char buf[size];
          nsCOMPtr<nsISupportsDouble> supDouble = do_CreateInstance("@mozilla.org/supports-double;1", &rc);
          NS_ENSURE_SUCCESS(rc, PR_FALSE);
          NS_ENSURE_SUCCESS(pLeft->GetRate(&rate), PR_FALSE);
          NS_ENSURE_SUCCESS(supDouble->SetData(rate), PR_FALSE);
          NS_ENSURE_SUCCESS(supDouble->ToString((char**)&buf), PR_FALSE);
          return prm.Compare(buf) == 0 ? PR_TRUE : PR_FALSE;
        }
    }
  }
  return PR_FALSE;
}

nsresult
  aaTreeViewRule::doFilterRules(nsIArray* pInput, nsITreeColumn* pColumn, const nsAString& param, nsIArray** _retval)
{
  nsresult rc = NS_OK;
  NS_ENSURE_ARG_POINTER(pInput);
  NS_ENSURE_ARG_POINTER(pColumn);
  NS_ENSURE_ARG_POINTER(_retval);
  if (!param.IsEmpty()) {
    PRInt32 idx = -1;
    PRUint32 length = 0;
    //arrays
    nsCOMPtr<nsIMutableArray> marr;
    nsCOMPtr<aaIRule> rule;
    //UTF8 param
    nsCAutoString utfParam;
    //convert param
    ToUpperCase(NS_ConvertUTF16toUTF8(param), utfParam);
    //get index
    rc = pColumn->GetIndex(&idx);
    NS_ENSURE_SUCCESS(rc, rc);
    //check idx
    NS_ENSURE_TRUE(idx >= 0 || idx <= 3, NS_ERROR_INVALID_ARG);
    //get length
    rc = pInput->GetLength(&length);
    NS_ENSURE_SUCCESS(rc, rc);
    if (length > 0) {
      marr = do_CreateInstance("@mozilla.org/array;1", &rc);
      NS_ENSURE_SUCCESS(rc, rc);
      for (PRUint32 i = 0;  i < length; ++ i) {
        rule = do_QueryElementAt(pInput, i, &rc);
        NS_ENSURE_SUCCESS(rc, rc);
        if (match(idx, rule, utfParam)) {
          rc = marr->AppendElement(rule, PR_FALSE);
          NS_ENSURE_SUCCESS(rc, rc);
        }
      }
      rc = marr->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
      NS_ENSURE_SUCCESS(rc, rc);
    } else {
      rc = pInput->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
      NS_ENSURE_SUCCESS(rc, rc);
    }
  } else {
    rc = pInput->QueryInterface(NS_GET_IID(nsIArray), (void**)_retval);
    NS_ENSURE_SUCCESS(rc, rc);
  }
  return rc;
}
