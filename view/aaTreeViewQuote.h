/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWQUOTE_H
#define AATREEVIEWQUOTE_H 1

#define AA_TREEVIEWQUOTE_CID \
{0x268d7f4b, 0x9f31, 0x49b2, {0xa2, 0xc3, 0x48, 0xb3, 0xbd, 0x24, 0xcf, 0xeb}}
#define AA_TREEVIEWQUOTE_CONTRACT "@aasii.org/view/tree-quote;1"

#include "aaTreeView.h"

class aaIQuote;
class nsACString;

class aaTreeViewQuote : public aaTreeView
{
public:
  aaTreeViewQuote();
  virtual ~aaTreeViewQuote();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name);
private:
  friend class aaViewTest;

  static nsresult (*const colFuncs[])(aaIQuote *, PRUint32 flags,
      nsAString &);
  static nsresult getColumn0(aaIQuote *quote, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn1(aaIQuote *quote, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn2(aaIQuote *quote, PRUint32 flags,
      nsAString & _retval);

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
};

#endif /* AATREEVIEWQUOTE_H */

