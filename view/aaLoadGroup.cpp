/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2010 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"
#include "nsISimpleEnumerator.h"
#include "nsIWebNavigation.h"
#include "nsIInterfaceRequestor.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsIRequest.h"
#include "nsILoadGroup.h"
#include "nsIURI.h"
#include "nsIInputStream.h"
#include "nsIRunnable.h"

/* Unfrozen API */
#include "nsThreadUtils.h"

/* Project includes */
#include "aaLoadGroup.h"

class aaLoadEvent : public nsIRunnable
{
public:
  aaLoadEvent(nsIWeakReference *weakDocShell, const PRUnichar *aURI,
      PRUint32 aLoadFlags, nsIURI *aReferrer, nsIInputStream *aPostData,
      nsIInputStream *aHeaders)
    :mDocShell(weakDocShell), mURI(aURI), mLoadFlags(aLoadFlags)
     ,mReferrer(aReferrer), mPostData(aPostData), mHeaders(aHeaders)
     {}
  NS_DECL_ISUPPORTS
  NS_DECL_NSIRUNNABLE
private:
  nsWeakPtr mDocShell;
  const PRUnichar *mURI;
  PRUint32 mLoadFlags;
  nsCOMPtr<nsIURI> mReferrer;
  nsCOMPtr<nsIInputStream> mPostData;
  nsCOMPtr<nsIInputStream> mHeaders;
private:
  ~aaLoadEvent() {}
};


NS_IMPL_ISUPPORTS1(aaLoadEvent,
                   nsIRunnable)

NS_IMETHODIMP
aaLoadEvent::Run()
{
  nsresult rv;
  nsCOMPtr<nsIWebNavigation> webNav = do_QueryReferent(mDocShell, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = webNav->LoadURI(mURI, mLoadFlags, mReferrer, mPostData, mHeaders);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

aaLoadGroup::aaLoadGroup(nsIDocShell *docShell)
  :mCount(0)
{
  mDocShell = do_GetWeakReference(docShell, &mStatus);
  if (NS_FAILED(mStatus)) return;

  checkPresentation(docShell);
}

NS_IMPL_ISUPPORTS3(aaLoadGroup,
                   nsILoadGroup,
                   nsIRequest,
                   nsISupportsWeakReference)
/* nsILoadGroup */
NS_IMETHODIMP
aaLoadGroup::AddRequest(nsIRequest *aRequest, nsISupports *aContext)
{
  mCount++;
  return mContained->AddRequest(aRequest, aContext);
}

NS_IMETHODIMP
aaLoadGroup::RemoveRequest(nsIRequest *aRequest, nsISupports *aContext,
    nsresult aStatus)
{
  NS_ENSURE_TRUE(mCount, NS_ERROR_UNEXPECTED);
  nsresult rv;

  rv = mContained->RemoveRequest(aRequest, aContext, aStatus);
  NS_ENSURE_SUCCESS(rv, rv);

  if (--mCount > 0)
    return NS_OK;

  rv = NS_DispatchToCurrentThread(mTask);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

/* Private methods */
nsresult
aaLoadGroup::Init(const PRUnichar *aURI, PRUint32 aLoadFlags,
    nsIURI *aReferrer, nsIInputStream *aPostData, nsIInputStream *aHeaders)
{
  NS_ENSURE_SUCCESS(mStatus, mStatus);
  nsresult rv;

  nsCOMPtr<nsIRunnable> task  = new aaLoadEvent(mDocShell, aURI, aLoadFlags,
      aReferrer, aPostData, aHeaders);
  NS_ENSURE_TRUE(task, NS_ERROR_OUT_OF_MEMORY);

  if (mContained) {
    mTask = task;
    return NS_OK;
  }

  rv = task->Run();
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

void
aaLoadGroup::checkPresentation(nsIDocShell *docShell)
{
  nsresult rv;

  if (!docShell)
    return;

  nsCOMPtr<nsIInterfaceRequestor> rq = do_QueryInterface(docShell, &rv);
  if (!rq || NS_FAILED(rv))
    return;

  nsCOMPtr<nsILoadGroup> loadGroup = do_GetInterface(rq, &rv);
  if (!loadGroup || NS_FAILED(rv))
    return;

  nsCOMPtr<nsISimpleEnumerator> requests;
  rv = loadGroup->GetRequests(getter_AddRefs(requests));
  if (!requests || NS_FAILED(rv))
    return;

  PRBool hasMore = PR_FALSE;

  while (NS_SUCCEEDED(requests->HasMoreElements(&hasMore)) && hasMore) {
    nsCOMPtr<nsISupports> elem;
    requests->GetNext(getter_AddRefs(elem));

    nsCOMPtr<nsIRequest> request = do_QueryInterface(elem);
    if (request) {
      mStatus = request->SetLoadGroup(this);
      if (NS_FAILED(mStatus))
        return;

      mCount++;
    }
  }
  if (mCount)
    mContained = loadGroup;
}

