/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsTextFormatter.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsIAtom.h"
#include "nsIAtomService.h"
#include "nsIStringBundle.h"

/* Project includes */
#include "aaAmountUtils.h"
#include "aaIResource.h"
#include "aaIFlow.h"
#include "aaIFact.h"
#include "aaITransaction.h"
#include "aaISqlTransaction.h"
#include "aaReportKeys.h"
#include "aaViewUtils.h"
#include "aaTreeViewLedger.h"

aaTreeViewLedger::aaTreeViewLedger()
{
  mComplexLoader = do_CreateInstance(AA_GENERALLEDGER_CONTRACT);
}

aaTreeViewLedger::~aaTreeViewLedger()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewLedger, aaTreeView)

/* Virtual overrides */
NS_IMETHODIMP
aaTreeViewLedger::GetRowCount(PRInt32 *aRowCount)
{
  NS_ENSURE_ARG_POINTER(aRowCount);
  *aRowCount = 0;
  if (! mDataSet)
    return NS_OK;

  nsresult rv;
  rv = mDataSet->GetLength((PRUint32 *) aRowCount);
  NS_ENSURE_SUCCESS(rv, rv);

  *aRowCount *= 2;
  return NS_OK;
}

/* Helpers */
nsresult
aaTreeViewLedger::getColumn0(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (flags)
    return NS_OK;

  assignDate(_retval, txn->PickFact()->PickTime());
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn1(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (flags)
    return NS_OK;

  nsCOMPtr<aaIResource> resource;
  aaIFlow *flow = txn->PickFact()->PickTakeFrom();
  if (flow && flow->PickId()) {
      txn->PickFact()->PickTakeFrom()->GetGiveResource(
          getter_AddRefs(resource));
  } else {
      txn->PickFact()->PickGiveTo()->GetTakeResource(
          getter_AddRefs(resource));
  }

  if (resource)
    return resource->GetTag(_retval);

  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn2(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (flags)
    return NS_OK;

  assignDouble(_retval, txn->PickFact()->PickAmount());
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn3(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  nsresult rv;
  nsCOMPtr<nsIStringBundleService> heap = do_GetService(
      NS_STRINGBUNDLE_CONTRACTID, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIStringBundle> bundle;
  rv = heap->CreateBundle("chrome://abstract/locale/abstract.properties",
      getter_AddRefs( bundle ));
  NS_ENSURE_SUCCESS(rv, rv);

  PRUnichar *w;
  if (flags)
    rv = bundle->GetStringFromName(NS_LITERAL_STRING("terms.debit").get(),
        &w);
  else
    rv = bundle->GetStringFromName(NS_LITERAL_STRING("terms.credit").get(),
        &w);

  _retval.Assign(w);
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn4(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (!flags) {
    if (txn->PickFact()->PickTakeFrom())
      txn->PickFact()->PickTakeFrom()->GetTag(_retval);
  } else {
    if (txn->PickFact()->PickGiveTo())
      txn->PickFact()->PickGiveTo()->GetTag(_retval);
  }
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn5(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (isZero(txn->PickFact()->PickAmount()))
    return NS_OK;

  if (flags) {
    assignDouble(_retval, (txn->PickValue() + txn->PickEarnings())
        / txn->PickFact()->PickAmount());
  } else {
    assignDouble(_retval, (txn->PickValue())
        / txn->PickFact()->PickAmount());
  }
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn6(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (!flags)
    return NS_OK;

  assignDouble(_retval, txn->PickValue() + txn->PickEarnings());
  return NS_OK;
}

nsresult
aaTreeViewLedger::getColumn7(aaITransaction *txn, PRUint32 flags,
    nsAString & _retval)
{
  if (flags)
    return NS_OK;

  assignDouble(_retval, txn->PickValue());
  return NS_OK;
}

nsresult (*const
aaTreeViewLedger::colFuncs[])(aaITransaction *, PRUint32 flags,
  nsAString &) = {
  &aaTreeViewLedger::getColumn0,
  &aaTreeViewLedger::getColumn1,
  &aaTreeViewLedger::getColumn2,
  &aaTreeViewLedger::getColumn3,
  &aaTreeViewLedger::getColumn4,
  &aaTreeViewLedger::getColumn5,
  &aaTreeViewLedger::getColumn6,
  &aaTreeViewLedger::getColumn7
};

/* Private functions */
nsresult
aaTreeViewLedger::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  nsresult rv;
  _retval.Truncate();

  PRInt32 ci = getColumnIndex(col, sizeof(colFuncs));
  NS_ENSURE_TRUE(ci >= 0, NS_OK);

  nsCOMPtr<aaITransaction> transaction(do_QueryElementAt(mDataSet,
        getIndexFromRow(row), &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(transaction, NS_OK);

  return colFuncs[ci](transaction, getOffsetFromRow(row), _retval);
}
