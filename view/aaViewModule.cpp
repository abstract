/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsIGenericFactory.h"
#include "nsISHistory.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"

/* Unfrozen API */
#include "nsISHistoryInternal.h"
#include "nsIWebNavigation.h"
#include "nsISHEntry.h"
#include "nsISHContainer.h"

/* Project includes */
#include "aaParamFactory.h"
#include "aaSessionHistory.h"
#include "aaTreeView.h"
#include "aaTreeViewEntity.h"
#include "aaTreeViewResource.h"
#include "aaTreeViewFlow.h"
#include "aaTreeViewLedger.h"
#include "aaTreeViewBalance.h"
#include "aaTreeViewTranscript.h"
#include "aaTreeViewQuote.h"
#include "aaTreeViewRule.h"
#include "aaPageView.h"

NS_GENERIC_FACTORY_CONSTRUCTOR_INIT(aaSessionHistory, Init)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewEntity)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewResource)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewFlow)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewLedger)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewBalance)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewTranscript)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewQuote)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaTreeViewRule)
NS_GENERIC_FACTORY_CONSTRUCTOR_PARAM(aaPageView)

NS_DECL_CLASSINFO(aaSessionHistory)

static const nsModuleComponentInfo kComponents[] =
{
  {
    "SessionHistory",
    AA_SESSIONHISTORY_CID,
    AA_SESSIONHISTORY_CONTRACT,
    aaSessionHistoryConstructor
    ,NULL, NULL, NULL,
    NS_CI_INTERFACE_GETTER_NAME(aaSessionHistory),
    NULL,
    &NS_CLASSINFO_NAME(aaSessionHistory)
  }
  ,{
    "TreeViewEntity",
    AA_TREEVIEWENTITY_CID,
    AA_TREEVIEWENTITY_CONTRACT,
    aaTreeViewEntityConstructor
  }
  ,{
    "TreeViewResource",
    AA_TREEVIEWRESOURCE_CID,
    AA_TREEVIEWRESOURCE_CONTRACT,
    aaTreeViewResourceConstructor
  }
  ,{
    "TreeViewFlow",
    AA_TREEVIEWFLOW_CID,
    AA_TREEVIEWFLOW_CONTRACT,
    aaTreeViewFlowConstructor
  }
  ,{
    "TreeViewLedger",
    AA_TREEVIEWLEDGER_CID,
    AA_TREEVIEWLEDGER_CONTRACT,
    aaTreeViewLedgerConstructor
  }
  ,{
    "TreeViewBalance",
    AA_TREEVIEWBALANCE_CID,
    AA_TREEVIEWBALANCE_CONTRACT,
    aaTreeViewBalanceConstructor
  }
  ,{
    "TreeViewTranscript",
    AA_TREEVIEWTRANSCRIPT_CID,
    AA_TREEVIEWTRANSCRIPT_CONTRACT,
    aaTreeViewTranscriptConstructor
  }
  ,{
    "TreeViewQuote",
    AA_TREEVIEWQUOTE_CID,
    AA_TREEVIEWQUOTE_CONTRACT,
    aaTreeViewQuoteConstructor
  }
  ,{
    "TreeViewRule",
    AA_TREEVIEWRULE_CID,
    AA_TREEVIEWRULE_CONTRACT,
    aaTreeViewRuleConstructor
  }
  ,{
    "Page View",
    AA_PAGEVIEW_CID,
    AA_PAGEVIEW_CONTRACT,
    aaPageViewConstructor
  }
};
NS_IMPL_NSGETMODULE(aaview, kComponents)

