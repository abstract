/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AA_TREEVIEWRULE_H
#define AA_TREEVIEWRULE_H

#define AA_TREEVIEWRULE_CID \
	{ 0x29e2cb02, 0x53e1, 0x48cd, { 0x87, 0x65, 0x32, 0xd7, 0xf3, 0xcb, 0xee, 0xfa } }
#define AA_TREEVIEWRULE_CONTRACT "@aasii.org/view/tree-rule;1"

#include "aaTreeView.h"

class aaIFlow;

class aaTreeViewRule : public aaTreeView {
public:
  aaTreeViewRule();
  virtual ~aaTreeViewRule();
  NS_DECL_ISUPPORTS_INHERITED

public:
  //override
  NS_IMETHOD Update(nsISupports *aParam);

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name);
protected:
  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col, nsAString & _retval);
private:
  nsresult doSortRules(nsIArray* pInput, nsITreeColumn* pColumn, const nsAString& param, nsIArray** _retval);
  nsresult doFilterRules(nsIArray* pInput, nsITreeColumn* pColumn, const nsAString& param, nsIArray** _retval);
};

#endif //AA_TREEVIEWRULE_H
