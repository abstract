/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWFLOW_H
#define AATREEVIEWFLOW_H 1

#define AA_TREEVIEWFLOW_CID \
{0x0da145d9, 0x024d, 0x4628, {0xb6, 0x60, 0x5b, 0xe6, 0xcd, 0x2e, 0xcd, 0x58}}
#define AA_TREEVIEWFLOW_CONTRACT "@aasii.org/view/tree-flow;1"

#include "aaTreeView.h"

class aaTreeViewFlow : public aaTreeView
{
public:
  aaTreeViewFlow();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name);
private:
  ~aaTreeViewFlow();
  friend class aaViewTest;

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
  virtual nsresult postLoadHandler();
};

#endif /* AATREEVIEWFLOW_H */
