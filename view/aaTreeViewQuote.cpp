/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsTextFormatter.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsIAtom.h"
#include "nsIAtomService.h"

/* Project includes */
#include "aaIResource.h"
#include "aaIQuote.h"
#include "aaISqlRequest.h"
#include "aaAccountLoaders.h"
#include "aaViewUtils.h"
#include "aaTreeViewQuote.h"

aaTreeViewQuote::aaTreeViewQuote()
{
  mDataLoader = do_CreateInstance(AA_LOADQUOTE_CONTRACT);
}

aaTreeViewQuote::~aaTreeViewQuote()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewQuote, aaTreeView)

/* Helpers */
nsresult
aaTreeViewQuote::getColumn0(aaIQuote *quote, PRUint32 flags,
    nsAString & _retval)
{
  nsresult rv;

  nsCOMPtr<aaIResource> resource;
  rv = quote->GetResource(getter_AddRefs(resource));
  NS_ENSURE_TRUE(resource, NS_OK);

  return resource->GetTag(_retval);
}

nsresult
aaTreeViewQuote::getColumn1(aaIQuote *quote, PRUint32 flags,
    nsAString & _retval)
{
  NS_ENSURE_TRUE(quote, NS_OK);

  assignDate(_retval, quote->PickTime());
  return NS_OK;
}

nsresult
aaTreeViewQuote::getColumn2(aaIQuote *quote, PRUint32 flags,
    nsAString & _retval)
{
  NS_ENSURE_TRUE(quote, NS_OK);

  assignSignificantDouble(_retval, quote->PickRate());
  return NS_OK;
}

nsresult (*const
aaTreeViewQuote::colFuncs[])(aaIQuote *, PRUint32 flags,
  nsAString &) = {
  &aaTreeViewQuote::getColumn0,
  &aaTreeViewQuote::getColumn1,
  &aaTreeViewQuote::getColumn2
};

/* Private functions */
nsresult
aaTreeViewQuote::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  nsresult rv;
  _retval.Truncate();

  PRInt32 ci = getColumnIndex(col, sizeof(colFuncs));
  NS_ENSURE_TRUE(ci >= 0, NS_OK);

  nsCOMPtr<aaIQuote> quote(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(quote, NS_OK);

  return colFuncs[ci](quote, mFlags, _retval);
}

NS_IMETHODIMP
  aaTreeViewQuote::convertSortColumnToString(nsITreeColumn* col, nsACString& name)
{
  nsresult rc = NS_OK;
  PRInt32 idx = -1;

  NS_ENSURE_ARG_POINTER(col);

  name.Truncate();

  rc = col->GetIndex(&idx);
  NS_ENSURE_SUCCESS(rc, rc);

  switch (idx) {
  case 0:
    name = NS_LITERAL_CSTRING("quote.tag");
    break;
  case 1:
    name = NS_LITERAL_CSTRING("quote.date");
    break;
  case 2:
    name = NS_LITERAL_CSTRING("quote.rate");
    break;
  default:
    name.Truncate();
    return NS_ERROR_INVALID_ARG;
  }

  return rc;
}
