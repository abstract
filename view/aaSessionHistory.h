/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AASESSIONHISTORY_H
#define AASESSIONHISTORY_H 1

#define AA_SESSIONHISTORY_CID \
{0xceee0f16, 0x7194, 0x4831, {0xac, 0x37, 0x3c, 0xd2, 0x5f, 0xb1, 0x09, 0x6b}}
#define AA_SESSIONHISTORY_CONTRACT "@aasii.org/view/session-history;1"

#include "nsISHistory.h"
#include "nsISHistoryInternal.h"
#include "nsIWebNavigation.h"
#include "aaISHistory.h"
#include "aaISession.h"


#define NS_FORWARD_NSIWEBNAVIGATION_R(_to) \
  NS_SCRIPTABLE NS_IMETHOD GetCanGoBack(PRBool *aCanGoBack) { return _to GetCanGoBack(aCanGoBack); } \
  NS_SCRIPTABLE NS_IMETHOD GetCanGoForward(PRBool *aCanGoForward) { return _to GetCanGoForward(aCanGoForward); } \
  NS_SCRIPTABLE NS_IMETHOD GoBack(void) { return _to GoBack(); } \
  NS_SCRIPTABLE NS_IMETHOD GoForward(void) { return _to GoForward(); } \
  NS_SCRIPTABLE NS_IMETHOD GotoIndex(PRInt32 index) { return _to GotoIndex(index); } \
  NS_SCRIPTABLE NS_IMETHOD Reload(PRUint32 aReloadFlags) { return _to Reload(aReloadFlags); } \
  NS_SCRIPTABLE NS_IMETHOD Stop(PRUint32 aStopFlags) { return _to Stop(aStopFlags); } \
  NS_SCRIPTABLE NS_IMETHOD GetDocument(nsIDOMDocument * *aDocument) { return _to GetDocument(aDocument); } \
  NS_SCRIPTABLE NS_IMETHOD GetCurrentURI(nsIURI * *aCurrentURI) { return _to GetCurrentURI(aCurrentURI); } \
  NS_SCRIPTABLE NS_IMETHOD GetReferringURI(nsIURI * *aReferringURI) { return _to GetReferringURI(aReferringURI); } \
  NS_SCRIPTABLE NS_IMETHOD GetSessionHistory(nsISHistory * *aSessionHistory) { return _to GetSessionHistory(aSessionHistory); } \
  NS_SCRIPTABLE NS_IMETHOD SetSessionHistory(nsISHistory * aSessionHistory) { return _to SetSessionHistory(aSessionHistory); } 

class nsISHistory;
class nsISHistoryInternal;
class nsIWebNavigation;
class aaISession;
class aaIPageView;

class aaSessionHistory : public nsISHistory,
                         public nsISHistoryInternal,
                         public nsIWebNavigation,
                         public aaISHistory,
                         public aaISession
{
public:
  aaSessionHistory();
  virtual ~aaSessionHistory();
  NS_DECL_ISUPPORTS
  NS_FORWARD_NSISHISTORY(mSH->)
  NS_DECL_NSISHISTORYINTERNAL
  NS_FORWARD_NSIWEBNAVIGATION_R(mWN->)
  NS_SCRIPTABLE NS_IMETHOD LoadURI(const PRUnichar *aURI, PRUint32 aLoadFlags,
      nsIURI *aReferrer, nsIInputStream *aPostData, nsIInputStream *aHeaders);
  NS_FORWARD_AAISAVEQUERY(mSession->)
  NS_FORWARD_SAFE_AAISESSION(get_mSession())
  NS_DECL_AAISHISTORY

  nsresult Init() {return mWN ? NS_OK : NS_ERROR_FAILURE;}
private:
  friend class aaViewTest;

  nsCOMPtr<aaISession> mSession;
  nsCOMPtr<nsISHistory> mSH;
  nsCOMPtr<nsISHistoryInternal> mSHI;
  nsCOMPtr<nsIWebNavigation> mWN;
  nsCOMPtr<nsIFile> mFile;

  PRBool mChoosing;
  nsCOMPtr<nsISupports> mQueryBuffer;

  aaIPageView* getDestination(PRInt32 offset, nsresult &rv);
  aaISession* get_mSession();
}; 

#endif /* AASESSIONHISTORY_H */

/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
