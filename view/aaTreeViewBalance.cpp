/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsServiceManagerUtils.h"
#include "nsStringAPI.h"
#include "nsTextFormatter.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"
#include "nsIAtom.h"
#include "nsIAtomService.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaIResource.h"
#include "aaIBalance.h"
#include "aaISqlTransaction.h"
#include "aaReportKeys.h"
#include "aaViewUtils.h"
#include "aaTreeViewBalance.h"

aaTreeViewBalance::aaTreeViewBalance()
{
  mComplexLoader = do_CreateInstance(AA_BALANCESHEET_CONTRACT);
}

aaTreeViewBalance::~aaTreeViewBalance()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewBalance, aaTreeView)

/* Helpers */
nsresult
aaTreeViewBalance::getColumn0(aaIBalance *balance, PRUint32 flags,
    nsAString & _retval)
{
  nsresult rv;

  nsCOMPtr<aaIFlow> flow;
  rv = balance->GetFlow(getter_AddRefs( flow ));
  NS_ENSURE_TRUE(flow, NS_OK);

  return flow->GetTag(_retval);
}

nsresult
aaTreeViewBalance::getColumn1(aaIBalance *balance, PRUint32 flags,
    nsAString & _retval)
{
  nsresult rv;

  nsCOMPtr<aaIFlow> flow;
  rv = balance->GetFlow(getter_AddRefs( flow ));
  NS_ENSURE_TRUE(flow, NS_OK);

  nsCOMPtr<aaIEntity> entity;
  rv = flow->GetEntity(getter_AddRefs( entity ));
  if (!entity)
    return NS_OK;

  return entity->GetTag(_retval);
}

nsresult
aaTreeViewBalance::getColumn2(aaIBalance *balance, PRUint32 flags,
    nsAString & _retval)
{
  nsresult rv;

  nsCOMPtr<aaIResource> resource;
  rv = balance->GetResource(getter_AddRefs( resource ));
  if (!resource)
    return NS_OK;

  return resource->GetTag(_retval);
}

nsresult
aaTreeViewBalance::getColumn3(aaIBalance *balance, PRUint32 flags,
    nsAString & _retval)
{
  PRBool side;
  double value;
  balance->GetSide(&side);
  if (! side)
    return NS_OK;
  if (flags)
    balance->GetValue(&value);
  else
    balance->GetAmount(&value);
  assignDouble(_retval, value);
  return NS_OK;
}

nsresult
aaTreeViewBalance::getColumn4(aaIBalance *balance, PRUint32 flags,
    nsAString & _retval)
{
  PRBool side;
  double value;
  balance->GetSide(&side);
  if (side)
    return NS_OK;
  if (flags)
    balance->GetValue(&value);
  else
    balance->GetAmount(&value);
  assignDouble(_retval, value);
  return NS_OK;
}

nsresult (*const
aaTreeViewBalance::colFuncs[])(aaIBalance *, PRUint32 flags,
  nsAString &) = {
  &aaTreeViewBalance::getColumn0,
  &aaTreeViewBalance::getColumn1,
  &aaTreeViewBalance::getColumn2,
  &aaTreeViewBalance::getColumn3,
  &aaTreeViewBalance::getColumn4
};

/* Private functions */
nsresult
aaTreeViewBalance::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  nsresult rv;
  _retval.Truncate();

  PRInt32 ci = getColumnIndex(col, sizeof(colFuncs));
  NS_ENSURE_TRUE(ci >= 0, NS_OK);

  nsCOMPtr<aaIBalance> balance(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  NS_ENSURE_TRUE(balance, NS_OK);

  return colFuncs[ci](balance, mFlags, _retval);
}
