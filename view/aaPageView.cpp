/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"

/* Unfrozen API */
#include "nsISHEntry.h"
#include "nsISHContainer.h"

/* Project includes */
#include "aaIDataNode.h"
#include "aaPageView.h"

nsresult
aaPageView::Init(nsISupports *aOuter)
{
  nsresult rv;
  mSHE = do_QueryInterface(aOuter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  mSHC = do_QueryInterface(aOuter, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  return NS_OK;
}

NS_IMPL_ISUPPORTS4(aaPageView,
                   nsIHistoryEntry,
                   nsISHEntry,
                   aaIPageView,
                   nsISHContainer)
/* aaIPageView */
NS_IMETHODIMP
aaPageView::GetBuffer(aaIDataNode * *aBuffer)
{
  NS_ENSURE_ARG_POINTER(aBuffer);
  NS_IF_ADDREF(*aBuffer = mBuffer);  
  return NS_OK;
}
NS_IMETHODIMP
aaPageView::SetBuffer(aaIDataNode *aBuffer)
{
  mBuffer = aBuffer;
  return NS_OK;
}

NS_IMETHODIMP
aaPageView::GetIndex(PRInt32 *aIndex)
{
  NS_ENSURE_ARG_POINTER( aIndex );
  *aIndex = mIndex;  
  return NS_OK;
}
NS_IMETHODIMP
aaPageView::SetIndex(PRInt32 aIndex)
{
  mIndex = aIndex;
  return NS_OK;
}

NS_IMETHODIMP
aaPageView::GetQueryChoice(nsISupports * *aQueryChoice)
{
  NS_ENSURE_ARG_POINTER(aQueryChoice);
  NS_IF_ADDREF(*aQueryChoice = mQueryChoice);  
  return NS_OK;
}
NS_IMETHODIMP
aaPageView::SetQueryChoice(nsISupports *aQueryChoice)
{
  mQueryChoice = aQueryChoice;
  return NS_OK;
}

NS_IMETHODIMP
aaPageView::GetChoosing(PRBool *aChoosing)
{
  NS_ENSURE_ARG_POINTER( aChoosing );
  *aChoosing = mChoosing;  
  return NS_OK;
}
NS_IMETHODIMP
aaPageView::SetChoosing(PRBool aChoosing)
{
  if (mChoosingSet)
    return NS_ERROR_ALREADY_INITIALIZED;
  mChoosingSet = PR_TRUE;
  mChoosing = aChoosing;
  return NS_OK;
}
