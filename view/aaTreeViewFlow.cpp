/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsStringAPI.h"

/* Unfrozen API */
#include "nsIArray.h"
#include "nsArrayUtils.h"
#include "nsITreeColumns.h"

/* Project includes */
#include "aaIEntity.h"
#include "aaIFlow.h"
#include "aaBaseKeys.h"
#include "aaBaseLoaders.h"
#include "aaISqlRequest.h"
#include "aaTreeViewFlow.h"

aaTreeViewFlow::aaTreeViewFlow()
{
  mDataLoader = do_CreateInstance(AA_LOADFLOW_CONTRACT);
}

aaTreeViewFlow::~aaTreeViewFlow()
{
}

NS_IMPL_ISUPPORTS_INHERITED0(aaTreeViewFlow, aaTreeView)

/* Private functions */
nsresult
aaTreeViewFlow::postLoadHandler()
{
  if (!mFlags)
    return NS_OK;

  nsresult rv;
  nsCOMPtr<aaILoadObserver> resultSet = do_QueryInterface(mDataSet, &rv);
  NS_ENSURE_SUCCESS(rv, rv);
  
  nsCOMPtr<aaIFlow> pnl = do_CreateInstance(AA_INCOMEFLOW_CONTRACT, &rv);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = resultSet->ObserveLoad(pnl);
  NS_ENSURE_SUCCESS(rv, rv);

  return NS_OK;
}

nsresult
aaTreeViewFlow::doGetCellText(PRInt32 row, nsITreeColumn *col,
    nsAString & _retval)
{
  nsresult rv;
  NS_ENSURE_TRUE(col, NS_ERROR_INVALID_ARG);

  PRInt32 ci;
  rv = col->GetIndex(&ci);
  NS_ENSURE_SUCCESS(rv, rv);
  _retval.Truncate();
  if (ci > 1) {
    return NS_OK;
  }

  nsCOMPtr<aaIFlow> flow(do_QueryElementAt(mDataSet, row, &rv));
  NS_ENSURE_SUCCESS(rv, rv);
  if (! flow)
    return NS_OK;

  switch (ci) {
  case 0:
    return flow->GetTag(_retval);
    break;
  case 1:
    {
      nsCOMPtr<aaIEntity> entity;
      flow->GetEntity(getter_AddRefs( entity ));
      if (! entity)
        return NS_OK;
      return entity->GetTag(_retval);
    }
    break;
  }
  return NS_OK;
}

NS_IMETHODIMP
  aaTreeViewFlow::convertSortColumnToString(nsITreeColumn* col, nsACString& name)
{
  nsresult rc = NS_OK;
  PRInt32 idx = -1;

  NS_ENSURE_ARG_POINTER(col);

  name.Truncate();

  rc = col->GetIndex(&idx);
  NS_ENSURE_SUCCESS(rc, rc);

  switch (idx) {
  case 0:
    name = NS_LITERAL_CSTRING("flow.tag");
    break;
  case 1:
    name = NS_LITERAL_CSTRING("flow.entity.tag");
    break;
  default:
    name.Truncate();
    return NS_ERROR_INVALID_ARG;
  }

  return rc;
}
