/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef AATREEVIEWBALANCE_H
#define AATREEVIEWBALANCE_H 1

#define AA_TREEVIEWBALANCE_CID \
{0x47715c90, 0x3ab0, 0x4964, {0x93, 0x35, 0x9f, 0x1a, 0x07, 0xdd, 0x85, 0x31}}
#define AA_TREEVIEWBALANCE_CONTRACT "@aasii.org/view/tree-balance;1"

#include "aaTreeView.h"

class aaIBalance;
class nsACString;

class aaTreeViewBalance : public aaTreeView
{
public:
  aaTreeViewBalance();
  virtual ~aaTreeViewBalance();
  NS_DECL_ISUPPORTS_INHERITED

protected:
  NS_IMETHOD convertSortColumnToString(nsITreeColumn* col, nsACString& name) {
    return NS_ERROR_NOT_IMPLEMENTED;
  }
private:
  friend class aaViewTest;

  static nsresult (*const colFuncs[])(aaIBalance *, PRUint32 flags,
      nsAString &);
  static nsresult getColumn0(aaIBalance *balance, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn1(aaIBalance *balance, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn2(aaIBalance *balance, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn3(aaIBalance *balance, PRUint32 flags,
      nsAString & _retval);
  static nsresult getColumn4(aaIBalance *balance, PRUint32 flags,
      nsAString & _retval);

  virtual nsresult doGetCellText(PRInt32 row, nsITreeColumn *col,
      nsAString & _retval);
};

#endif /* AATREEVIEWBALANCE_H */

