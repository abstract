/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set ts=2 sw=2 sts=2 et cindent tw=79 ft=cpp: */
/*
 * Copyright (C) 2007,2008 Sergey Yanovich <ynvich@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "xpcom-config.h"

#include "nsIGenericFactory.h"
#include "nsComponentManagerUtils.h"
#include "nsISHistory.h"
#include "nsMemory.h"
#include "nsIClassInfoImpl.h"
#include "nsXPCOMCID.h"
#include "nsISupportsPrimitives.h"

/* Unfrozen API */
#include "nsISHistoryInternal.h"
#include "nsIWebNavigation.h"
#include "nsISHEntry.h"
#include "nsISHContainer.h"
#include "nsTestUtils.h"
#include "nsITest.h"
#include "nsITestRunner.h"

/* Project includes */
#include "aaSessionHistory.h"
#include "aaPageView.h"
#include "aaTreeViewQuote.h"

#define AA_VIEW_TEST_CID \
{0x39010bed, 0x1087, 0x4363, {0x99, 0x19, 0xf9, 0xe7, 0x1a, 0x74, 0xce, 0x9b}}
#define AA_VIEW_TEST_CONTRACT "@aasii.org/view/unit;1"

class aaViewTest: public nsITest {
public:
  aaViewTest() {;}
  virtual ~aaViewTest() {;}
  NS_DECL_ISUPPORTS
  NS_DECL_NSITEST
private:
  nsresult testSH(nsITestRunner *aTestRunner);
  nsresult testSHE(nsITestRunner *aTestRunner);
  nsresult testSHChoice(nsITestRunner *aTestRunner);
  nsresult testQuoteView(nsITestRunner *aTestRunner);
};

/* Module and Factory code */
NS_GENERIC_FACTORY_CONSTRUCTOR(aaViewTest)

NS_DECL_CLASSINFO(aaSessionHistory)

static const nsModuleComponentInfo kComponents[] =
{
  {
    "View Module Unit Test",
    AA_VIEW_TEST_CID,
    AA_VIEW_TEST_CONTRACT,
    aaViewTestConstructor
  }
};
NS_IMPL_NSGETMODULE(aaviewt, kComponents)

NS_IMPL_ISUPPORTS1(aaViewTest, nsITest)

/* nsITest */
NS_IMETHODIMP
aaViewTest::Test(nsITestRunner *aTestRunner)
{
  nsITestRunner *cxxUnitTestRunner = aTestRunner;
  NS_TEST_ASSERT_OK( testSH( aTestRunner ) );
  NS_TEST_ASSERT_OK( testSHE( aTestRunner ) );
  NS_TEST_ASSERT_OK( testSHChoice( aTestRunner ) );
  return NS_OK;
}

/* Private methods */
nsresult
aaViewTest::testSH(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<nsISHistory> sh( do_CreateInstance(
        AA_SESSIONHISTORY_CONTRACT) );
  NS_TEST_ASSERT_MSG(sh, "aaSessionHistory intance creation");
  
  nsCOMPtr<nsIWebNavigation> wn = do_QueryInterface(sh);
  NS_TEST_ASSERT_MSG(wn, "aaSessionHistory WebNavigation interface");

  PRInt32 count = 1;
  rv = sh->GetCount( &count );
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(! count, "aaSessionHistory wrong item count");

  nsCOMPtr<nsISHistoryInternal> shi = do_QueryInterface(sh);
  NS_TEST_ASSERT_MSG(shi, "aaSessionHistory SHInternal interface");
  nsCOMPtr<nsISHEntry> entry(do_CreateInstance(NS_SHENTRY_CONTRACTID));
  NS_ENSURE_TRUE(entry, NS_ERROR_UNEXPECTED);
  
  rv = shi->AddEntry(entry, PR_TRUE);
  NS_TEST_ASSERT_OK(rv);

  rv = sh->GetCount( &count );
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 1, "aaSessionHistory wrong item count");

  PRInt32 index = 0;
  rv = sh->GetIndex( &index );
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(count == 1, "aaSessionHistory wrong index");

  nsCOMPtr<aaISHistory> ash = do_QueryInterface(sh);
  PRBool choosing = PR_TRUE;
  rv = ash->GetChoosing( &choosing );
  NS_TEST_ASSERT_OK( rv );
  NS_TEST_ASSERT_MSG(! choosing, "ash should not be in choice mode");

  return NS_OK;
}

nsresult
aaViewTest::testSHE(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  nsCOMPtr<nsISHEntry> wrapped( do_CreateInstance(
        NS_SHENTRY_CONTRACTID, &rv) );
  NS_ENSURE_TRUE(wrapped, rv);

  nsCOMPtr<nsISHEntry> she( do_CreateInstance(
        AA_PAGEVIEW_CONTRACT, wrapped));
  NS_TEST_ASSERT_MSG(she, "[aaPageView] instance creation");
  
  nsCOMPtr<nsISHContainer> shc = do_QueryInterface(she);
  NS_TEST_ASSERT_MSG(shc, "[aaPageView] SHContainer interface");

  nsCOMPtr<nsIHistoryEntry> he = do_QueryInterface(she);
  NS_TEST_ASSERT_MSG(he, "[aaPageView] HistoryEntry interface");

  nsCOMPtr<aaIPageView> pv = do_QueryInterface(she);
  NS_TEST_ASSERT_MSG(pv, "[aaPageView] PageView interface");
  NS_ENSURE_TRUE(pv, rv);
  
  rv = pv->SetChoosing(PR_TRUE);
  NS_TEST_ASSERT_OK(rv);
  rv = pv->SetChoosing(PR_TRUE);
  NS_TEST_ASSERT_MSG(NS_FAILED(rv),
      "[aaPageView] 'choosing' must be write-once");

  return NS_OK;
}

nsresult
aaViewTest::testSHChoice(nsITestRunner *aTestRunner)
{
  nsresult rv;
  NS_TEST_BEGIN( aTestRunner );

  /* Prepare test situation */
  nsCOMPtr<aaISHistory> ash = do_CreateInstance(AA_SESSIONHISTORY_CONTRACT,
      &rv);
  NS_ENSURE_TRUE(ash, rv);

  nsCOMPtr<nsISHistory> sh = do_QueryInterface(ash, &rv);
  NS_ENSURE_TRUE(sh, rv);

  nsCOMPtr<nsISHistoryInternal> shi = do_QueryInterface(ash, &rv);
  NS_ENSURE_TRUE(shi, rv);

  nsCOMPtr<nsISHEntry> wrapped = do_CreateInstance(NS_SHENTRY_CONTRACTID, &rv);
  NS_ENSURE_TRUE(wrapped, rv);

  rv = shi->AddEntry(wrapped, PR_TRUE);
  NS_ENSURE_SUCCESS(rv, rv);

  PRInt32 index;
  rv = sh->GetIndex(&index);
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsIHistoryEntry> stored;
  rv = sh->GetEntryAtIndex(index, PR_FALSE, getter_AddRefs( stored ));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIPageView> p1 = do_QueryInterface(stored, &rv);
  NS_ENSURE_TRUE(p1, rv);

  wrapped = do_CreateInstance(NS_SHENTRY_CONTRACTID, &rv);
  NS_ENSURE_TRUE(wrapped, rv);

  rv = ash->BeginQuery();
  NS_ENSURE_SUCCESS(rv, rv);

  rv = shi->AddEntry(wrapped, PR_TRUE);
  NS_ENSURE_SUCCESS(rv, rv);

  /* Test PR_TRUE trip to destination */
  nsCOMPtr<nsISupportsPRBool> test1 = do_CreateInstance(
      NS_SUPPORTS_PRBOOL_CONTRACTID, &rv);
  NS_ENSURE_TRUE(test1, rv);
  
  rv = test1->SetData(PR_TRUE);
  NS_ENSURE_SUCCESS(rv, rv);

  rv = ash->SetChoice(test1);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv), "[session history] setting choice");
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<nsISupports> s;
  rv = p1->GetQueryChoice(getter_AddRefs( s ));
  NS_TEST_ASSERT_MSG(s, "[session history] reading choice from entry");
  NS_ENSURE_TRUE(s, rv);

  nsCOMPtr<nsISupportsPRBool> test2 = do_QueryInterface(s, &rv);
  NS_ENSURE_TRUE(test2, rv);
  PRBool b = PR_FALSE; test2->GetData(&b);
  NS_TEST_ASSERT_MSG(b, "[session history] choice value is wrong");

  /* Test PR_TRUE round-trip */
  rv = ash->GetChoice(getter_AddRefs( s ));
  NS_TEST_ASSERT_MSG(s, "[session history] reading choice");
  NS_ENSURE_TRUE(s, rv);

  test2 = do_QueryInterface(s, &rv);
  NS_ENSURE_TRUE(test2, rv);
  b = PR_FALSE; test2->GetData(&b);
  NS_TEST_ASSERT_MSG(b, "[session history] choice value is wrong");

  /* Test 'choosing' state at destination */
  rv = sh->GetEntryAtIndex(index + 1, PR_FALSE, getter_AddRefs( stored ));
  NS_ENSURE_SUCCESS(rv, rv);

  nsCOMPtr<aaIPageView> p2 = do_QueryInterface(stored, &rv);
  NS_ENSURE_TRUE(p2, rv);

  b = PR_FALSE;
  rv = p2->GetChoosing(&b);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(b, "[session history] entry not in 'choosing' mode");
  
  /* Test multiple 'session.choosing' */
  rv = ash->GetChoosing(&b);
  NS_TEST_ASSERT_OK(rv);
  NS_TEST_ASSERT_MSG(b, "[session history] not in 'choosing' mode");
  
  rv = ash->GetChoosing(&b);
  NS_TEST_ASSERT_MSG(NS_SUCCEEDED(rv),
      "[session history] re-reading 'choosing'");
  return NS_OK;
}
